$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(".employer-section").on("click", ".btn-searchEmployer", function(e) {

	var $this = $(this);
	var $employer_section = $(".employer-section");

	var value = $this.closest(".form-group").find("input[name=employer_name]").val();

	if($.trim(value) != "") {
		$.ajax({
			type: "POST",
			url: BASE_URL + "/admin-panel/search-employer",
			data: {name: value},
			dataType: "json",
			beforeSend: function() {
				$this.text('Searching...').attr("disabled", true);
			},
			success: function(Response) {
				if (Response['status'] == true) {
					var employer = Response.employer;
					var html = '';

					html += '<tr>';
					html += '<td class="text-left">';
					html += '<label class="option block mn" style="width: 55px;">';
					html += '<input type="checkbox" name="check[]" class="check" value="' + employer.employer_id + '">';
					html += '<span class="checkbox mn"></span>';
                    html += '</label>';
                    html += '</td>';        
                    html += '<td class=""> ' + employer.employer_name + ' </td>';
                    html += '<td class=""> ' + employer.employer_name + ' </td>';
                    html += '<td class=""> ' + employer.employer_name + ' </td>';
                    html += '<td class=""> ' + employer.employer_name + ' </td>';
                    html += '<td class=""> ' + employer.employer_name + ' </td>';
                    html += '<td class="text-right">';
                    html += '<a title="Edit" href="' + employer.url + '" style="text-transform: capitalize;">';
                    html += '<button type="button" class="btn btn-success  br2 btn-xs fs12 dropdown-toggle"> Edit </button>';
                    html += '</a>';
                    html += '</td>';
                    html += '<td class="text-right">';
                    html += '<a href="" class="" title="Hide">'; 
                    html += '<button type="button" class="btn btn-success  br2 btn-xs fs12 dropdown-toggle"> Active';
                    html += '</button>';
                    html += '</a>';
                    html += '</td>';
					html += '</tr>';

					$employer_section.find(".table-employer-list tbody").html(html);
				}
			},
			complete: function () {
				$this.text('Search').removeAttr("disabled");
			}
		});
	}
});


// $(".employer-section2").on("click", ".btn-searchEmployer2", function(e) {

// });

// $(document).on('click','.role_status',function(){
//         var id = $(this).attr('id');
        
//         alert("hello");

//         $.ajax({
//             data:{ id=id, _token: '{!! crfs_toekn() !!}'},
//             url: '',
//             type: 'POST',
//             dataType: 'json'

//             success:function()
//             {

//             }
//        });
//     });
