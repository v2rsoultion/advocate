<?php $__env->startSection('content'); ?>
<style type="text/css">
  /*.admin-form .select, .admin-form .gui-input, .admin-form .select > select, .admin-form .select-multiple select{
    height: 28px !important;
  }*/
  .admin-form .append-icon .field-icon, .admin-form .prepend-icon .field-icon{
    line-height: 28px !important;
  }
  .admin-form .gui-textarea {
    line-height: 7px !important;
  }

  .form-control {
    height: 28px !important;
    /*padding: 0px 12px !important;*/
  }
  .admin-form .button{
    height: 28px !important;
    line-height: 1px !important;
  }
  .btn {
    height: 29px !important;
    line-height: 1px !important;  }

 .down{
  margin-top: 70px;
 }

 .close{
  margin-top: -6px !important;
  font-size: 30px !important;
  width: 30px !important;
}

</style>
  <!-- Start: Content-Wrapper -->
  <section id="content_wrapper">
    <header id="topbar">
      <div class="topbar-left down">
        <ol class="breadcrumb">
          <li class="crumb-active">
            <a href="<?php echo e(url('/advocate-panel/add-class-code')); ?>">Add Class Code</a>
          </li>
          <li class="crumb-icon">
            <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">
              <span class="glyphicon glyphicon-home"></span>
            </a>
          </li>
          <li class="crumb-link">
            <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">Home</a>
          </li>
          <li class="crumb-trail">View Class Code </li>
        </ol>
      </div>
    </header>

    <div class="" style="margin-top:10px;">
      <div class="col-md-12">
        <div class="panel panel-primary panel-border top mb70">  
          <div class="panel-heading">
            <div class="panel-title hidden-xs">
              <span class="glyphicon glyphicon-tasks"></span> View Class Code</div>
          </div>

          <div class="panel-menu admin-form theme-primary" style="padding: 5px 20px;">
            <div class="row">
              <?php echo Form::open(['url'=>'/advocate-panel/view-class-code' ,'autocomplete'=>'off']); ?>

                
                <div class="col-md-3">
                    <label for="level" class="field prepend-icon">  
                      <select class=" form-control" name="subtype_case" id="level"  style="padding: 0px 12px; color: black;">
                        <option value="">Choose Case Type</option>
                          <?php $__currentLoopData = $case_type_entry; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $case_type_entrys): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                            <option value="<?php echo e($case_type_entrys->case_id); ?>" <?php echo e($case_type_entrys->case_id == $subtype_case ? 'selected="selected"' : ''); ?> ><?php echo e($case_type_entrys->case_name); ?> </option>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </select>
                    </label>
                </div>

                <div class="col-md-3">
                  <label for="pincode" class="field prepend-icon">
                    <?php echo Form::text('sub_type_name','',array('class' => 'form-control ','placeholder' => 'Class Code Name', 'autocomplete' => 'off' )); ?>

                    <label for="pincode" class="field-icon">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>
                <!-- <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon">
                    <?php echo Form::text('subtype_code','',array('class' => 'form-control ','placeholder' => 'Code', 'autocomplete' => 'off' )); ?>

                    <label for="pincode" class="field-icon">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>
                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon">
                    <?php echo Form::text('subtype_short_name','',array('class' => 'form-control','placeholder' => 'Short Name', 'autocomplete' => 'off' )); ?>

                    <label for="pincode" class="field-icon">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div> -->

                <div class="col-md-1 pull-right mr15">
                  <button type="submit" name="search" class="button btn-primary"> Search </button>
                </div>
              <?php echo Form::close(); ?>             
                <div class="col-md-1">
                   <a href="<?php echo e(url('/advocate-panel/view-class-code/')); ?>"><?php echo Form::submit('Default', array('class' => 'btn btn-primary', 'id' => 'maskedKey')); ?></a>
                </div>
                
                <div class="col-md-2 ">
                   <a href="<?php echo e(url('/advocate-panel/view-class-code/all')); ?>"><?php echo Form::submit('Show All Records', array('class' => 'btn btn-primary', 'id' => 'maskedKey')); ?></a>
                </div>

            </div>
          </div>

          <div class="panel-body pn">
              <?php echo Form::open(['url'=>'/advocate-panel/view-class-code','name'=>'form' ,'autocomplete'=>'off']); ?>


              <div class="table-responsive">
                <table class="table admin-form table-bordered table-striped theme-warning tc-checkbox-1 fs13" id="datatable2">
                  <thead>
                    <tr class="bg-light">
                      <th style="width:90px !important;" class="text-left">
                        <label class="option block mn" style="width:90px !important;">
                          <input type="checkbox" id="check_all"> 
                          <span class="checkbox mn"></span>
                          Select All
                        </label>
                      </th>
                      <th class="">Case Type</th>
                      <th class="">Class Code Name</th>
                      <!-- <th class="">Code</th>
                      <th class="">Short Name</th>
                      <th class="">Description</th> -->
                      <th class="text-right">Status</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php $__currentLoopData = $get_record; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_records): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>               
                    <tr>
                      <td class="text-left" style="padding-left: 18px;">
                        <label class="option block mn">
                          <input type="checkbox" name="check[]" class="check" value="<?php echo e($get_records->classcode_id); ?>">
                          <span class="checkbox mn"></span>
                        </label>
                      </td>
                      <td class="" style="padding-left:20px"> <?php if($get_records->case_name != ""): ?> <?php echo e($get_records->case_name); ?> <?php else: ?> ----- <?php endif; ?> </td>
                      <td class="text-left" style="padding-left:20px"> <?php if($get_records->classcode_name != ""): ?> <?php echo e($get_records->classcode_name); ?> <?php else: ?> ----- <?php endif; ?></td>
                     <!--  <td class="text-left" style="padding-left:20px"> <?php echo e($get_records->classcode_code); ?> </td>
                      <td class="text-left" style="padding-left:20px"> <?php echo e($get_records->classcode_short_name); ?> </td> -->

                      <!-- <td class="" style="padding-left:20px"> 
                        <a href="#" style="text-transform: capitalize; text-decoration:none;" data-toggle="modal" data-target="#view_message<?php echo e($get_records->classcode_id); ?>" > View Description </a>
                        
                        <div id="view_message<?php echo e($get_records->classcode_id); ?>" class="modal fade in" role="dialog">
                          <div class="modal-dialog" style="width:700px; margin-top:100px;">
                            <div class="modal-content">
                              <div class="modal-header" style="padding-bottom: 35px;">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title pull-left"> Description </h4>
                              </div>
                              <div class="modal-body">
                                <section style="">
                                  <div class="row">
                                    <div class="col-md-12 text-justify">
                                      <?php echo e($get_records->classcode_description); ?>

                                    </div>
                                  </div>
                                </section>
                              </div>
                              <div class="clearfix"></div>
                            </div> 
                          </div>
                        </div>
                      </td> -->

                      <td class="text-right">
                        <div class="btn-group text-left">
                          <button type="button" class="btn <?php echo e($get_records->classcode_status == 1 ? 'btn-success' : 'btn-danger'); ?>  br2 btn-xs fs12 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> <?php echo e($get_records->classcode_status == 1 ? 'Active' : 'Deactive'); ?>

                            <span class="caret ml5"></span>
                          </button>
                          <ul class="dropdown-menu" role="menu" style="min-width:130px; left: <?php echo e($get_records->classcode_status == 1 ? '-67px' : '-54px'); ?> !important;">
                            <li>
                              <a href="<?php echo e(url('/advocate-panel/add-class-code')); ?>/<?php echo e(sha1($get_records->classcode_id)); ?>">Edit</a>
                            </li>
                            <div class="divider"></div>                            
                            <li class="<?php echo e($get_records->classcode_status == 1 ? 'active' : ''); ?>">
                              <a href="<?php echo e(url('/advocate-panel/change-class-code-status')); ?>/<?php echo e(sha1($get_records->classcode_id)); ?>/1">Active</a>
                            </li>
                            <li class=" <?php echo e($get_records->classcode_status == 0 ? 'active' : ''); ?> ">
                              <a href="<?php echo e(url('/advocate-panel/change-class-code-status')); ?>/<?php echo e(sha1($get_records->classcode_id)); ?>/0">Deactive</a>
                            </li>
                          </ul>
                        </div>
                      </td> 
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                  
                  </tbody>
                </table>
              </div>
              <?php echo Form::close(); ?>

          </div>
          <div class="panel-body pn">
            <div class="table-responsive">
              <table class="table admin-form theme-warning tc-checkbox-1 fs13">                                
                <tbody>
                  <tr class="">
                     <th class="text-left">
                      <button type="button" class="btn btn-primary" onclick="go_delete()"><i class="glyphicon glyphicon-trash"></i> Delete Multiple </button>
                    </th>
                    <th class="text-right">
                      <?php echo e($get_record->links()); ?>

                    </th>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


<style type="text/css">
.dt-panelfooter{
  display: none !important;
}
</style>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('advocate_admin/layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>