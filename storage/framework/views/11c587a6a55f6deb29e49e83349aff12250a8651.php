<?php $__env->startSection('content'); ?>

<style type="text/css">
  /*.admin-form .select, .admin-form .gui-input, .admin-form .select > select, .admin-form .select-multiple select{
    height: 28px !important;
  }*/
  .admin-form .append-icon .field-icon, .admin-form .prepend-icon .field-icon{
    line-height: 28px !important;
  }
  .admin-form .gui-textarea {
    line-height: 7px !important;
  }

  .form-control {
    height: 28px !important;
  }
  .admin-form .button{
    height: 28px !important;
    line-height: 1px !important;
  }
  .btn {
    height: 29px !important;
    line-height: 1px !important;  }

 .down{
  margin-top: 70px;
 }
 .close{
  margin-top: -6px !important;
  font-size: 30px !important;
  width: 30px !important;
}

</style>
  <!-- Start: Content-Wrapper -->
  <section id="content_wrapper">
    <header id="topbar">
      <div class="topbar-left down">
        <ol class="breadcrumb">
          <li class="crumb-active">
            <a href="<?php echo e(url('/advocate-panel/add-advocate')); ?>">Add Advocate</a>
          </li>
          <li class="crumb-icon">
            <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">
              <span class="glyphicon glyphicon-home"></span>
            </a>
          </li>
          <li class="crumb-link">
            <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">Home</a>
          </li>
          <li class="crumb-trail">View Advocate </li>
        </ol>
      </div>
    </header>

    <div class="" style="margin-top:10px;">
      <div class="col-md-12">
        <div class="panel panel-primary panel-border top mb70">  
          <div class="panel-heading">
            <div class="panel-title hidden-xs">
              <span class="glyphicon glyphicon-tasks"></span> View Advocate</div>
          </div>

          <div class="panel-menu admin-form theme-primary" style="padding: 5px 20px;">
            <div class="row">
              <?php echo Form::open(['url'=>'/advocate-panel/view-advocate' ,'autocomplete'=>'off']); ?>


                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon">
                    <?php echo Form::text('admin_name','',array('class' => 'form-control ','placeholder' => 'Name', 'autocomplete' => 'off' )); ?>

                    <label for="pincode" class="field-icon">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>
                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon">
                    <?php echo Form::text('admin_email','',array('class' => 'form-control ','placeholder' => 'Email Id', 'autocomplete' => 'off' )); ?>

                    <label for="pincode" class="field-icon">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>
                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon">
                    <?php echo Form::text('admin_number','',array('class' => 'form-control ','placeholder' => 'Mobile No', 'autocomplete' => 'off' )); ?>

                    <label for="pincode" class="field-icon">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>

                <div class="col-md-1">
                  <button type="submit" name="search" class="button btn-primary"> Search </button>
                </div>
              <?php echo Form::close(); ?>             
                
                <div class="col-md-2 ">
                   <a href="<?php echo e(url('/advocate-panel/view-advocate/all')); ?>"><?php echo Form::submit('Show All Records', array('class' => 'btn btn-primary', 'id' => 'maskedKey')); ?></a>
                </div>

                <div class="col-md-1 pull-right">
                   <a href="<?php echo e(url('/advocate-panel/view-advocate/')); ?>"><?php echo Form::submit('Default', array('class' => 'btn btn-primary', 'id' => 'maskedKey')); ?></a>
                </div>
              
            </div>
          </div>

          <div class="panel-body pn">
              <?php echo Form::open(['url'=>'/advocate-panel/view-advocate','name'=>'form' ,'autocomplete'=>'off']); ?>


              <div class="table-responsive">
                <table class="table admin-form table-bordered table-striped theme-warning tc-checkbox-1 fs13" id="datatable2">
                  <thead>
                    <tr class="bg-light">
                      <th class="text-left">
                        <label class="option block mn" style="width:90px !important;">
                          <input type="checkbox" id="check_all"> 
                          <span class="checkbox mn"></span>
                          Select All
                        </label>
                      </th>
                      <th class="">Name</th>
                      <th class="">Email Id</th>
                      <th class="">Mobile No</th>
                      <!-- <th class="">Address</th>
                      <th class="">Start Date</th>
                      <th class="">End date</th> -->
                      <th class=""> Special Permission </th>
                      <th class=""> Other Details </th>
                      <!-- <th class="">Image</th> -->
                      <th class="text-right">Status</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php $__currentLoopData = $get_record; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_records): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>               
                    <tr>
                      <td class="text-left" style="padding-left: 18px;">
                        <label class="option block mn">
                          <input type="checkbox" name="check[]" class="check" value="<?php echo e($get_records->admin_id); ?>">
                          <span class="checkbox mn"></span>
                        </label>
                      </td>
                      <td class="text-left" style="padding-left:20px"> <?php if($get_records->admin_name != ""): ?> <?php echo e($get_records->admin_name); ?> <?php else: ?> ----- <?php endif; ?> </td>
                      <td class="text-left" style="padding-left:20px"> <?php if($get_records->admin_email != ""): ?> <?php echo e($get_records->admin_email); ?> <?php else: ?> ----- <?php endif; ?> </td>
                      <td class="text-left" style="padding-left:20px"> <?php if($get_records->admin_number != ""): ?> <?php echo e($get_records->admin_number); ?> <?php else: ?> ----- <?php endif; ?> </td>
                      <!-- <td class="text-left" style="padding-left:20px"> <?php if($get_records->admin_address != ""): ?> <?php echo e($get_records->admin_address); ?> <?php else: ?> ----- <?php endif; ?> </td>
                      <td class="text-left" style="padding-left:20px"><?php echo e(date('d F Y',strtotime($get_records->start_date))); ?></td>
                      <td class="text-left" style="padding-left:20px"><?php echo e(date('d F Y',strtotime($get_records->end_date))); ?></td> -->
                      
                      <td class="" style="padding-left:20px"> 
                        <a href="#" style="text-transform: capitalize; text-decoration:none;" onclick="view_permission(<?php echo e($get_records->admin_id); ?>)" > Special Permission </a>

                        <!-- Sign In model -->
                        <div id="view_permission<?php echo e($get_records->admin_id); ?>" class="modal fade in" role="dialog" style="overflow: scroll;">
                          <div class="modal-dialog" style="width:700px; margin-top:80px;">
                            <div class="modal-content">
                              <div class="modal-header" style="padding-bottom: 35px;">
                                <button type="button" class="close" data-dismiss="modal" onclick="close_button_permission(<?php echo e($get_records->admin_id); ?>)" >&times;</button>
                                <h4 class="modal-title pull-left">Special Permission</h4>
                              </div>
                              <div class="modal-body">
                                <section style="background:#efefe9;">
                                  <div class="row">
                                    <div class="table-responsive">
                                      <table class="table admin-form theme-warning tc-checkbox-1 fs13" id="datatable">
                                        <thead>
                                          <tr class="bg-light">
                                            <th class="text-left">Model Name</th>
                                            <th class="text-left">Access</th>
                                          </tr>
                                        </thead> 
        
                                        <tbody>
                                          <tr>
                                            <td class="text-left" style="text-transform: capitalize; padding-left:20px"> Case Registration </td>
                                            <td class="text-left" style="padding-left:20px"> <?php if(isset(json_decode($get_records->special_permission)->case_registration)): ?> <i class="fa fa-check"></i>  <?php else: ?>  <i class="fa fa-times"></i>  <?php endif; ?> </td>
                                          </tr>

                                          <tr>
                                            <td class="text-left" style="text-transform: capitalize; padding-left:20px"> Peshi / Cause List Entry </td>
                                            <td class="text-left" style="padding-left:20px"> <?php if(isset(json_decode($get_records->special_permission)->pessi_cause_list)): ?>  <i class="fa fa-check"></i>  <?php else: ?>  <i class="fa fa-times"></i>  <?php endif; ?> </td>
                                          </tr>

                                          <tr>
                                            <td class="text-left" style="text-transform: capitalize; padding-left:20px"> Daily Diary </td>
                                            <td class="text-left" style="padding-left:20px"> <?php if(isset(json_decode($get_records->special_permission)->daily_cause_list)): ?>  <i class="fa fa-check"></i>  <?php else: ?>  <i class="fa fa-times"></i>  <?php endif; ?> </td>
                                          </tr>

                                          <tr>
                                            <td class="text-left" style="text-transform: capitalize; padding-left:20px"> SMS to Clients </td>
                                            <td class="text-left" style="padding-left:20px"> <?php if(isset(json_decode($get_records->special_permission)->sms_to_clients)): ?>  <i class="fa fa-check"></i>  <?php else: ?>  <i class="fa fa-times"></i>  <?php endif; ?> </td>
                                          </tr>

                                          <tr>
                                            <td class="text-left" style="text-transform: capitalize; padding-left:20px"> Order / Judgement Upload </td>
                                            <td class="text-left" style="padding-left:20px"> <?php if(isset(json_decode($get_records->special_permission)->order_judgement_upload)): ?>  <i class="fa fa-check"></i>  <?php else: ?>  <i class="fa fa-times"></i>  <?php endif; ?> </td>
                                          </tr>

                                          <tr>
                                            <td class="text-left" style="text-transform: capitalize; padding-left:20px"> Complaince </td>
                                            <td class="text-left" style="padding-left:20px"> <?php if(isset(json_decode($get_records->special_permission)->complaince)): ?>  <i class="fa fa-check"></i>  <?php else: ?>  <i class="fa fa-times"></i>  <?php endif; ?> </td>
                                          </tr>

                                          <tr>
                                            <td class="text-left" style="text-transform: capitalize; padding-left:20px"> Calendar </td>
                                            <td class="text-left" style="padding-left:20px"> <?php if(isset(json_decode($get_records->special_permission)->calendar)): ?>  <i class="fa fa-check"></i>  <?php else: ?>  <i class="fa fa-times"></i>  <?php endif; ?> </td>
                                          </tr>

                                          <tr>
                                            <td class="text-left" style="text-transform: capitalize; padding-left:20px"> Undated Case </td>
                                            <td class="text-left" style="padding-left:20px"> <?php if(isset(json_decode($get_records->special_permission)->undated_case)): ?>  <i class="fa fa-check"></i>  <?php else: ?>  <i class="fa fa-times"></i>  <?php endif; ?> </td>
                                          </tr>

                                          <tr>
                                            <td class="text-left" style="text-transform: capitalize; padding-left:20px"> Due Course </td>
                                            <td class="text-left" style="padding-left:20px"> <?php if(isset(json_decode($get_records->special_permission)->due_course)): ?>  <i class="fa fa-check"></i>  <?php else: ?>  <i class="fa fa-times"></i>  <?php endif; ?> </td>
                                          </tr>

                                          <tr>
                                            <td class="text-left" style="text-transform: capitalize; padding-left:20px"> Reporting </td>
                                            <td class="text-left" style="padding-left:20px"> <?php if(isset(json_decode($get_records->special_permission)->reporting)): ?>  <i class="fa fa-check"></i>  <?php else: ?>  <i class="fa fa-times"></i>  <?php endif; ?> </td>
                                          </tr>

                                          <tr>
                                            <td class="text-left" style="text-transform: capitalize; padding-left:20px"> Master Modules </td>
                                            <td class="text-left" style="padding-left:20px"> <?php if(isset(json_decode($get_records->special_permission)->master_modules)): ?>  <i class="fa fa-check"></i>  <?php else: ?>  <i class="fa fa-times"></i>  <?php endif; ?> </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </div>
                                  </div>
                                </section>
                              </div>
                              <div class="clearfix"></div>
                            </div> 
                          </div>
                        </div>
                      </td>



                      <td class="" style="padding-left:20px"> 
                        <a href="#" style="text-transform: capitalize; text-decoration:none;" onclick="other_details(<?php echo e($get_records->admin_id); ?>)" > Other Details </a>

                        <!-- Sign In model -->
                        <div id="other_details<?php echo e($get_records->admin_id); ?>" class="modal fade in" role="dialog" style="overflow: scroll;">
                          <div class="modal-dialog" style="width:700px; margin-top:80px;">
                            <div class="modal-content">
                              <div class="modal-header" style="padding-bottom: 35px;">
                                <button type="button" class="close" data-dismiss="modal" onclick="close_button_other(<?php echo e($get_records->admin_id); ?>)" >&times;</button>
                                <h4 class="modal-title pull-left"> Other Details </h4>
                              </div>
                              <div class="modal-body">
                                <section style="background:#efefe9;">
                                  <div class="row">
                                    <div class="table-responsive">
                                      <table class="table table-bordered admin-form theme-warning tc-checkbox-1 fs13" id="datatable">
                                        
        
                                        <tbody>
                                          <tr>
                                            <td class="text-left" style="text-transform: capitalize; padding-left:20px"> Address </td>
                                            <td class="text-left" style="padding-left:20px"> <?php if(isset($get_records->admin_address)): ?> <?php echo $get_records->admin_address; ?> <?php else: ?>  -----  <?php endif; ?> </td>
                                          </tr>

                                          <tr>
                                            <td class="text-left" style="text-transform: capitalize; padding-left:20px"> Firmname </td>
                                            <td class="text-left" style="padding-left:20px"> <?php if(isset($get_records->admin_firmname)): ?> <?php echo e($get_records->admin_firmname); ?> <?php else: ?>  -----  <?php endif; ?> </td>
                                          </tr>

                                          <tr>
                                            <td class="text-left" style="text-transform: capitalize; padding-left:20px"> Advocates </td>
                                            <td class="text-left" style="padding-left:20px"> <?php if(isset($get_records->admin_advocates)): ?> <?php echo e($get_records->admin_advocates); ?> <?php else: ?>  -----  <?php endif; ?> </td>
                                          </tr>

                                          <tr>
                                            <td class="text-left" style="text-transform: capitalize; padding-left:20px"> Start Date </td>
                                            <td class="text-left" style="padding-left:20px"> <?php if(isset($get_records->start_date)): ?> <?php echo e(date('d F Y',strtotime($get_records->start_date))); ?> <?php else: ?>  -----  <?php endif; ?> </td>
                                          </tr>

                                          <tr>
                                            <td class="text-left" style="text-transform: capitalize; padding-left:20px"> End Date </td>
                                            <td class="text-left" style="padding-left:20px"> <?php if(isset($get_records->end_date)): ?> <?php echo e(date('d F Y',strtotime($get_records->end_date))); ?> <?php else: ?>  -----  <?php endif; ?> </td>
                                          </tr>

                                          <tr>
                                            <td class="text-left" style="text-transform: capitalize; padding-left:20px"> Degree </td>
                                            <td class="text-left" style="padding-left:20px"> <?php if(isset($get_records->admin_degree)): ?> <?php echo e($get_records->admin_degree); ?> <?php else: ?>  -----  <?php endif; ?> </td>
                                          </tr>

                                          <tr>
                                            <td class="text-left" style="text-transform: capitalize; padding-left:20px"> Court </td>
                                            <td class="text-left" style="padding-left:20px"> <?php if(isset($get_records->admin_court)): ?> <?php echo e($get_records->admin_court); ?> <?php else: ?>  -----  <?php endif; ?> </td>
                                          </tr>

                                        </tbody>
                                      </table>
                                    </div>
                                  </div>
                                </section>
                              </div>
                              <div class="clearfix"></div>
                            </div> 
                          </div>
                        </div>
                      </td>


                      <!-- <td class="text-left" style="padding-left:20px">
                        <?php if($get_records->admin_image == ""): ?>
                            No Image Available
                        <?php else: ?> 
                          <?php echo Html::image($get_records->admin_image, '', array('class' => 'data_photo')); ?>   
                        <?php endif; ?>
                      </td> -->
                      
                      <td class="text-right">
                        <div class="btn-group text-left">
                          <button type="button" class="btn <?php echo e($get_records->admin_status   == 1 ? 'btn-success' : 'btn-danger'); ?>  br2 btn-xs fs12 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> <?php echo e($get_records->admin_status  == 1 ? 'Active' : 'Deactive'); ?>

                            <span class="caret ml5"></span>
                          </button>
                          <ul class="dropdown-menu" role="menu" style="min-width:130px; left: <?php echo e($get_records->admin_status == 1 ? '-67px' : '-54px'); ?> !important;">
                            <!-- <li>
                              <a href="<?php echo e(url('/advocate-panel/user-login')); ?>/<?php echo e($get_records->admin_id); ?>">User Login</a>
                            </li> -->
                            <li>
                              <a href="<?php echo e(url('/advocate-panel/add-advocate')); ?>/<?php echo e($get_records->admin_id); ?>">Edit</a>
                            </li>
                            <div class="divider"></div>                            
                            <li class="<?php echo e($get_records->admin_status   == 1 ? 'active' : ''); ?>">
                              <a href="<?php echo e(url('/advocate-panel/change-advocate-status')); ?>/<?php echo e($get_records->admin_id); ?>/1">Active</a>
                            </li>
                            <li class=" <?php echo e($get_records->admin_status  == 0 ? 'active' : ''); ?> ">
                              <a href="<?php echo e(url('/advocate-panel/change-advocate-status')); ?>/<?php echo e($get_records->admin_id); ?>/0">Deactive</a>
                            </li>
                          </ul>
                        </div>
                      </td>

                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                 
                  </tbody>
                </table>
              </div>
              <?php echo Form::close(); ?>

          </div>
          <div class="panel-body pn">
            <div class="table-responsive">
              <table class="table admin-form theme-warning tc-checkbox-1 fs13">                                
                <tbody>
                  <tr class="">
                     <th class="text-left">
                      <button type="button" class="btn btn-primary" onclick="go_delete()"><i class="glyphicon glyphicon-trash"></i> Delete Multiple </button>
                    </th>
                    <th>
                      <?php echo e($get_record->links()); ?>

                    </th>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

<style type="text/css">
.view a{
  color:#fff !important;
}
</style>
<style type="text/css">
.dt-panelfooter{
  display: none !important;
}

.dataTables_filter{
  float: left;
}
</style>

<script type="text/javascript">
  
  function view_permission(admin_id){
    $('body').css('overflow','hidden');
    $("html, body").animate({ scrollTop: 0 }, "slow");
    document.getElementById('view_permission'+admin_id).style.display='block';
  }

  function close_button_permission(admin_id){
    $('body').css('overflow','auto', 'important');
    document.getElementById('view_permission'+admin_id).style.display='none';
  }

  function other_details(admin_id){
    $('body').css('overflow','hidden');
    $("html, body").animate({ scrollTop: 0 }, "slow");
    document.getElementById('other_details'+admin_id).style.display='block';
  }

  function close_button_other(admin_id){
    $('body').css('overflow','auto', 'important');
    document.getElementById('other_details'+admin_id).style.display='none';
  }

</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('advocate_admin/layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>