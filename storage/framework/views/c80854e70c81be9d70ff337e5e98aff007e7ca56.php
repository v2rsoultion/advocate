<!-- include header -->
<?php echo $__env->make('header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<!-- case section start-->
<div class="container case" id="about-us">
	<div class="row case-in2">
		<div class="col-md-6 col-xs-12 col-sm-6 casein  wow bounceInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
		  <img src="public/images/mobile.jpg" class="img-responsive" alt="img mobile">
		</div>
		<div class="col-md-6  col-xs-12 col-sm-6  wow fadeInRight" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
			<h2>A Single Space For All Your Cases</h2>
			<p>See all your cases at one glance arranged in order of its next hearing, automatically!</p>
			<ul>
			   <li><img src="public/images/arrow.png" alt="img arrow">Interactive Client Directory</li>
			   <li><img src="public/images/arrow.png" alt="img arrow">Personalized Case Diary</li>
			   <li><img src="public/images/arrow.png" alt="img arrow">Maintain Accounts</li>
			   <li><img src="public/images/arrow.png" alt="img arrow">Digital Library</li>
			   <li><img src="public/images/arrow.png" alt="img arrow">Research Database(Cases/Judgments)</li>
			   <li><img src="public/images/arrow.png" alt="img arrow">Completely Secured Data With Ssl.</li>
			</ul>
		</div>
	</div>
	       <div class="clearfix"></div>
</div>

<div class="container case" id="product">
	<div class="row case-in2">
		<div class="col-md-6 col-xs-12 col-sm-6 wow fadeInRight" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
			<h2>Every Case Detail In One Click</h2>
			<p>One tap to read recent orders or to know the bench and item number for your next hearing.</p>
			<ul>
			   <li><img src="public/images/arrow.png" alt="img arrow">Ideal Judgment/Research notes! </li>
			   <li><img src="public/images/arrow.png" alt="img arrow">Pre-set formates of all kinds of Notices</li>
			   <li><img src="public/images/arrow.png" alt="img arrow">Get the total Compliance of any case on one click</li>
			</ul>
			<div class="clearfix"></div>
		</div>

		<div class="col-md-6 col-xs-12 col-sm-6 casein wow bounceInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
		    <img src="public/images/case2.jpg" height="404px" class="img-responsive" class="dis" alt="img case2">
		</div>
	</div>
	<div class="clearfix"></div>
</div>

<div class="container case">
	<div class="row case-in2">
		<div class="col-md-6 col-xs-12 col-sm-6  casein wow bounceInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
			<img src="public/images/alerts.jpg" class="img-responsive" alt="img alerts">
		</div>
		<div class="col-md-6 col-xs-12 col-sm-6 wow fadeInRight" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
			<h2>Easy Alerts For Appeals</h2>
			<p>See all your cases at one glance arranged in order of its next hearing, automatically!</p>
			<ul>
			   <li><img src="public/images/arrow.png" alt="img arrow">Auto-updation of case records</li>
			   <li><img src="public/images/arrow.png" alt="img arrow">Store all your pleadings/drafts </li>
			   <li><img src="public/images/arrow.png" alt="img arrow">Your personal Accountant!</li>
			</ul>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<!-- case section end-->
<!-- press mentions section start-->
<div class="press">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-xs-12 col-sm-12 wow fadeInLeft" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10"">
				<h2>Press Mentions</h2>
				<!-- slider start -->
				    <div class="owl-carousel owl-theme" id="mainslider">
				       	<div class="item"> 
							<img src="public/images/quotation.png" alt="img quotation">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
						</div>
					    <div class="item"> 
							<img src="public/images/quotation.png" alt="img quotation">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
						</div>
			        </div>
			</div>
		</div>
	</div>
</div>
<!-- press mentions section end-->
<!-- linking section start -->
<div class="links">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-xs-6 col-sm-3">
				<div class="suprem">
					<a href="https://www.sci.gov.in/"  title="Supreme court" target="_blank"><h1>SUPREME COURT</h1> 
					<i class="fa fa-chevron-right"></i></a>
				</div>
			</div>
			<div class="col-md-3 col-xs-6 col-sm-3">
				<div class="suprem">
					<a href="http://hcraj.nic.in/" target="_blank" title="Rajasthan high court"><h1>RAJASTHAN HIGH COURT</h1> 
					<i class="fa fa-chevron-right"></i></a>
				</div>
			</div>
			<div class="col-md-3 col-xs-6 col-sm-3">
				<div class="suprem">
					<a href="https://districts.ecourts.gov.in/" title="District/trial court" target="_blank"><h1>DISTRICT /TRIAL COURT</h1> 
					<i class="fa fa-chevron-right"></i></a>
				</div>
			</div>
			<div class="col-md-3 col-xs-6 col-sm-3">
				<div class="suprem">
					<a href="https://services.ecourts.gov.in/ecourtindia_v5.1/" title="Ecourt of india" target="_blank"><h1>ECOURT OF INDIA</h1> 
					<i class="fa fa-chevron-right"></i></a>
				</div>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-3">
			    <div class="suprem">
					<a href="http://njdg.ecourts.gov.in/njdg_public/" title="National judicial date grid"  target="_blank"><h1>NATIONAL JUDICIAL DATE GRID</h1> 
					<i class="fa fa-chevron-right"></i></a>
				</div>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-3">
			    <div class="suprem">
					<a href="https://www.india.gov.in/"  title="Govt. of India"  target="_blank"><h1>GOVT. OF INDIA</h1> 
					<i class="fa fa-chevron-right"></i></a>
				</div>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-3">
				<div class="suprem">
					<a href="https://presidentofindia.nic.in/" title="President of india" target="_blank"><h1>PRESIDENT OF INDIA</h1> 
					<i class="fa fa-chevron-right"></i></a>
				</div>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-3">
			    <div class="suprem">
					<a href="http://www.eci.gov.in/" title="Election commission of india" target="_blank"><h1>ELECTION COMMISSION OF INDIA</h1> 
					<i class="fa fa-chevron-right"></i></a>
				</div>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-3">
				<div class="suprem">
					<a href="https://portal1.passportindia.gov.in/AppOnlineProject/welcomeLink" title="Passport sewa" target="_blank"><h1>PASSPORT SEWA</h1> 
					<i class="fa fa-chevron-right"></i></a>
				</div>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-3">
			    <div class="suprem">
					<a href="https://www.sbi.co.in/" target="_blank" title="State bank of india"><h1>STATE BANK OF INDIA</h1> 
					<i class="fa fa-chevron-right"></i></a>
				</div>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-3">
				<div class="suprem">
					<a href="https://www.aiims.edu/en.html" target="_blank" title="All india ins. medical sci."><h1>ALL INDIA INS. MEDICAL SCI.</h1> 
					<i class="fa fa-chevron-right"></i></a>
				</div>
            </div>
            <div class="col-md-3 col-xs-6 col-sm-3">
				<div class="suprem">
					<a href="https://www.irctc.co.in/nget/" target="_blank" title="Indian railway"><h1>INDIAN RAILWAY </h1> 
					<i class="fa fa-chevron-right"></i></a>
				</div>
            </div>
		</div>
	</div>
</div>

<!--  -->


<!-- get section end-->
<!-- footer section include -->

<?php echo $__env->make('footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>








