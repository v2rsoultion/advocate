<!DOCTYPE html>
<html>
  <head>
  <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>Confirmation | AdminPanel</title>
    <meta name="keywords" content="AdminPanel" />
    <meta name="description" content="AdminPanel">
    <meta name="author" content="AdminPanel">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Font CSS (Via CDN) -->
    <?php echo Html::style('public/admin/css/css.css'); ?>

    <!-- Admin Forms CSS -->
    <?php echo Html::style('public/admin/css/admin-forms.css'); ?>

    <!-- Theme CSS -->
    <?php echo Html::style('public/admin/css/theme.css'); ?>

    <?php echo Html::style('public/admin/css/fonts/glyphicons-pro/glyphicons-pro.css'); ?>

    <?php echo Html::style('public/admin/css/fonts/iconsweets/iconsweets.css'); ?>

    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo e(asset('public/admin/img/1.png')); ?>">
  </head>

  <body class="external-page sb-l-c sb-r-c">
    <div id="main" class="animated fadeIn">
      <section id="content_wrapper">
        <div id="canvas-wrapper">
          <canvas id="demo-canvas"></canvas>
        </div>
        <section id="content">
          <div class="admin-form theme-info" id="login1">
            <div class="center-block mt70" style="max-width: 625px">
              <div class="row table-layout">
                <div class="col-xs-12 pln">
                   <h2 class="text-dark mbn confirmation-header"><i class="fa fa-check text-success"></i> Account Confirmed.</h2>
                </div>
              </div>
              <div class="panel mt15">
                <div class="panel-body">          
                  <p class="lead">Hello <?php echo e($admin_name); ?>,</p>
                  <hr class="alt short ">
                    <p class="lh25 text-muted fs15">Your password reset link has been successfully sent to your email. Please check your email id for changing the password.</p>
                  <p class="text-right mt20"><a href="<?php echo e(url('advocate-panel')); ?>" class="btn btn-primary btn-rounded ph40">SIGN IN</a></p>
                </div>
              </div>
            </div>          
          </div>
        </section>
      </section>
    </div>
          
  <?php echo HTML::script('public/admin/js/jquery/jquery-1.11.1.min.js'); ?>

  <?php echo HTML::script('public/admin/js/jquery/jquery_ui/jquery-ui.min.js'); ?>

  <?php echo HTML::script('public/admin/js/plugins/canvasbg/canvasbg.js'); ?>

  <?php echo HTML::script('public/admin/js/utility/utility.js'); ?>

  <?php echo HTML::script('public/admin/js/demo/demo.js'); ?>

  <?php echo HTML::script('public/admin/js/main.js'); ?>

 

  <script>
$(document).ready(function() {
  $('img').on('dragstart', function(event) { event.preventDefault(); });
});
</script>
  
  <!-- Page Javascript -->
  <script type="text/javascript">
  jQuery(document).ready(function() {

    "use strict";
    // Init Theme Core      
    Core.init();

    // Init Demo JS
    Demo.init();

    // Init CanvasBG and pass target starting location
    CanvasBG.init({
      Loc: {
        x: window.innerWidth / 2,
        y: window.innerHeight / 3.3
      },
    });

  });
  </script>

  <!-- END: PAGE SCRIPTS -->

</body>
</html>



