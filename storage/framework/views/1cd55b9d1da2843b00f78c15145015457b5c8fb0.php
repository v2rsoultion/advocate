
<!-- Start : Header -->  

    <?php echo $__env->make('advocate_admin/include/header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<!-- End : Header -->	


<!-- Start: Menu -->

    <?php echo $__env->make('advocate_admin/include/sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<!-- End: Menu -->

<?php echo $__env->yieldContent('content'); ?>

<!-- Start: Footer -->
  
    <?php echo $__env->make('advocate_admin/include/footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<!-- End: Footer -->