<?php $__env->startSection('content'); ?>

<style type="text/css">
  .admin-form .select, .admin-form .gui-input, .admin-form .select > select, .admin-form .select-multiple select{
    height: 28px !important;
  }
  .admin-form a.button, .admin-form span.button, .admin-form label.button
  {
    line-height: 28px !important;
  }
  .admin-form .append-icon .field-icon, .admin-form .prepend-icon .field-icon{
    line-height: 28px !important;
  }
  .admin-form .gui-textarea {
    line-height: 7px !important;
  }
/*  .gui-textarea{
    height: 100px !important;
  }*/
/* .admin-form .gui-input {
  padding: 5px;
 }*/
 /*.admin-form .prepend-icon .field-icon {
  top: 6px;
 }*/
 .admin-form .button{
    height: 28px !important;
    line-height: 1px !important;
  }
 .btn {
    height: 29px !important;
    line-height: 1px !important;  }

 .down{
  margin-top: 70px;
 }
 .account_setting {
  margin: 0px 20px !important;
  margin-top: 25px !important;
  /*padding: 5px 0px !important;*/
 }
</style>
  <!-- Start: Content-Wrapper -->
  <section id="content_wrapper">
    <header id="topbar">
      <div class="topbar-left down">
        <ol class="breadcrumb">
          <li class="crumb-icon">
            <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">
              <span class="glyphicon glyphicon-home"></span>
            </a>
          </li>
          <li class="crumb-link">
            <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">Home</a>
          </li>
          <li class="crumb-trail"> Guideline Software</li>
        </ol>
      </div>
    </header>


    <div class="row">
      <div class="col-md-12">
        <?php if(\Session::has('success')): ?>
          <div class="alert alert-success account_setting" style="padding: 10px; border: 0px; text-align: center; margin: 10px;">
              <?php echo \Session::get('success'); ?>

          </div>
        <?php endif; ?>
      </div>
    </div>


    <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center">
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-heading">
                <span class="panel-title text-capitalize"> <?php echo e($get_record['pages_title']); ?> </span>
              </div>
              <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                  <div id="tab1_1" class="tab-pane active">
                    
                  <?php if($errors->any()): ?>
                    <div id="log_error" class="alert alert-danger"><i class="fa fa-thumbs-o-down"> <?php echo e($errors->first()); ?> </i></div>
                  <?php endif; ?>

                    <?php echo Form::open(['url'=>'advocate-panel/update-guideline-software' , 'name'=>'form', 'enctype' => 'multipart/form-data' , 'id' => 'validation' ,'autocomplete'=>'off' ] ); ?>

                    
                    <?php echo Form::hidden('pages_id',$get_record['pages_id'], array('class' => 'event-name gui-input br-light light focusss','placeholder' => '' )); ?>


                    <div class="row">
                      <div class="col-md-12">
                        <div class="col-md-12">
                          <div class="section">
                            <label for="pages_title" class="field-label" style="font-weight:600;" > Title </label>
                              <label for="pages_title" class="field prepend-icon">
                                <?php echo Form::text('pages_title',$get_record['pages_title'], array('class' => 'event-name gui-input br-light light focusss','placeholder' => '', 'disabled' => 'disabled' )); ?>

                                <label for="store-currency" class="field-icon">
                                  <i class="glyphicons glyphicons-edit"></i>
                                </label>
                              </label>
                          </div>
                        </div>
<!-- 
                        <div class="col-md-12">
                          <div class="section">
                            <label for="pages_excerpt" class="field-label" style="font-weight:600;" > Excerpt </label>
                              <label for="pages_excerpt" class="field prepend-icon">
                                <?php echo Form::textarea('pages_excerpt',$get_record['pages_excerpt'], array('class' => 'gui-textarea accc','placeholder' => '' )); ?>

                                <label for="store-currency" class="field-icon">
                                  <i class="fa fa-comments"></i>
                                </label>
                              </label>
                          </div>
                        </div> -->

                        <!-- <div class="col-md-12">
                          <div class="section">
                            <label for="pages_description" class="field-label" style="font-weight:600;" > Description </label>
                              <label for="pages_description" class="field prepend-icon">
                                <?php echo Form::textarea('pages_description',$get_record['pages_description'], array('class' => 'gui-textarea accc jqte-test','id' => 'pages_description' )); ?>

                              </label>
                            <div id="descrption_error" class="" ></div>
                          </div>
                        </div> -->

                        <div class="col-md-12">
                          <div class="field" align="left">
                            <label for="service_category_description" class="field prepend-icon" style="font-weight:600;" > <span style="float: left;margin-bottom: 10px;"> Upload PDF in English</span> <?php if($get_record['pages_english_pdf'] != ""): ?> <span id="cleardiv" style="float: right;"><a style="cursor: pointer;" href="<?php echo e(asset($get_record['pages_english_pdf'])); ?>" target="_Blank">View PDF </a></span> <?php endif; ?> </label>
                            <input type="file" id="english_pdf" name="english_pdf" style="padding: 3px;border: 1px solid #ccc;width: 100%;margin-bottom: 20px;" />
                          </div>
                        </div>

                        <div class="col-md-12">
                          <div class="field" align="left">
                            <label for="service_category_description" class="field-label" style="font-weight:600;" > <span style="float: left;margin-bottom: 10px;"> Upload PDF in Hindi</span> <?php if($get_record['pages_hindi_pdf'] != ""): ?> <span id="cleardiv" style="float: right;"><a style="cursor: pointer;" href="<?php echo e(asset($get_record['pages_hindi_pdf'])); ?>" target="_Blank">View PDF </a></span> <?php endif; ?> </label>
                            <input type="file" id="hindi_pdf" name="hindi_pdf" style="padding: 3px;border: 1px solid #ccc;width: 100%;margin-bottom: 20px;" />
                          </div>
                        </div>


                      </div>
                    </div>

                    <div class="panel-footer text-right">
                      <button type="button" class="button btn-primary mysave" onclick="update_record_pages()"> Update </button>
                      <?php echo Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')); ?>

                    </div>
                    
                    <?php echo Form::close(); ?>


                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>


  </section>


<style type="text/css">
.dt-panelfooter{
  display: none !important;
}
.state-error{
    margin-top: -10px;
    margin-bottom: 10px;
}
</style>


<style>
    
  .jqte_editor{
    height: auto !important;
    min-height: 300px!important;
  }
  .jqte_source{
    height: auto !important;
    min-height: 300px!important;
  }
</style>



<script type="text/javascript">
  jQuery(document).ready(function() {
  
    /* @custom  validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#validation").validate({

      /* @validation  states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation  rules 
      ------------------------------------------ */

      rules: {
        pages_title: {
          required: true,
        },

        pages_excerpt: {
          required: true,
        },

        pages_description: {
          required: true,
        },


        english_pdf: {
          extension: 'pdf',
        },

        hindi_pdf: {
          extension: 'pdf',
        },
       
      },

      /* @validation  error messages 
      ---------------------------------------------- */

      messages: {
        pages_title: {
          required: 'Enter Pages Title'
        },

        pages_excerpt: {
          required: 'Enter pages Excerpt'
        },

        pages_description: {
          required: 'Enter Pages Description'
        },

        english_pdf: {
          extension: 'File must be in .pdf format only'
        },

        hindi_pdf: {
          extension: 'File must be in .pdf format only'
        },

      },

      /* @validation  highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });

  function update_record_pages() {
     var pages_description = $("#pages_description").val();
     if(pages_description == ""){
      $('#descrption_error').html('<div class="state-error"></div><em for="services_excerpt" class="state-error">Please enter page description.</em>');
      $(".jqte").css({"border": "1px solid #de888a"});
      $(".jqte_editor").css({"background": "#fee9ea"});
      return false;
     } else {
        if(confirm("Are you sure to want update records?")) {
          $("[name=form]").submit();    
        }
      }
   }

  setInterval(function(){ 
    var pages_description = $("#pages_description").val();
    if (pages_description!="") {
      $('#descrption_error').html('');
      $(".jqte").css({"border": "1px solid #ddd"});
      $(".jqte_editor").css({"background": "#fff"});
    }
  }, 500);


  </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('advocate_admin/layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>