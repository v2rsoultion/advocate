<?php $__env->startSection('content'); ?>
<style type="text/css">
  /*.admin-form .select, .admin-form .gui-input, .admin-form .select > select, .admin-form .select-multiple select{
    height: 28px !important;
  }*/
  .admin-form .append-icon .field-icon, .admin-form .prepend-icon .field-icon{
    line-height: 28px !important;
  }
  .admin-form .gui-textarea {
    line-height: 7px !important;
  }

  .form-control {
    height: 28px !important;
    /*padding: 0px 12px !important;*/
  }
  .admin-form .button{
    height: 28px !important;
    line-height: 1px !important;
  }
  .btn {
    height: 29px !important;
    line-height: 1px !important;  }

 .down{
  margin-top: 70px;
 }

</style>
  <!-- Start: Content-Wrapper -->
  <section id="content_wrapper">
    <header id="topbar">
      <div class="topbar-left down">
        <ol class="breadcrumb">
          <li class="crumb-active">
            <a href="<?php echo e(url('/advocate-panel/add-referred')); ?>">Add Referred</a>
          </li>
          <li class="crumb-icon">
            <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">
              <span class="glyphicon glyphicon-home"></span>
            </a>
          </li>
          <li class="crumb-link">
            <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">Home</a>
          </li>
          <li class="crumb-trail">View Referred </li>
        </ol>
      </div>
    </header>

    <div class="" style="margin-top:10px;">
      <div class="col-md-12">
        <div class="panel panel-primary panel-border top mb70">  
          <div class="panel-heading">
            <div class="panel-title hidden-xs">
              <span class="glyphicon glyphicon-tasks"></span> View Referred</div>
          </div>

          <div class="panel-menu admin-form theme-primary" style="padding: 5px 20px;">
            <div class="row">
              <?php echo Form::open(['url'=>'/advocate-panel/view-referred' ,'autocomplete'=>'off']); ?>


                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon">
                    <?php echo Form::text('ref_advocate_name','',array('class' => 'form-control ','placeholder' => 'Advocate Name', 'autocomplete' => 'off' )); ?>

                    <label for="pincode" class="field-icon">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>
                
                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon">
                    <?php echo Form::text('ref_mobile_number','',array('class' => 'form-control ','placeholder' => 'Mobile No', 'autocomplete' => 'off' )); ?>

                    <label for="pincode" class="field-icon">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>
                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon">
                    <?php echo Form::text('ref_place','',array('class' => 'form-control ','placeholder' => 'Place', 'autocomplete' => 'off' )); ?>

                    <label for="pincode" class="field-icon">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>
                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon">
                    <?php echo Form::text('ref_address','',array('class' => 'form-control ','placeholder' => 'Address', 'autocomplete' => 'off' )); ?>

                    <label for="pincode" class="field-icon">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>

                <div class="col-md-1 pull-right mr15">
                  <button type="submit" name="search" class="button btn-primary"> Search </button>
                </div>
              <?php echo Form::close(); ?>             
                <div class="col-md-1">
                   <a href="<?php echo e(url('/advocate-panel/view-referred/')); ?>"><?php echo Form::submit('Default', array('class' => 'btn btn-primary', 'id' => 'maskedKey')); ?></a>
                </div>

                <div class="col-md-1 ">
                   <a href="<?php echo e(url('/advocate-panel/view-referred/all')); ?>"><?php echo Form::submit('Show All Records', array('class' => 'btn btn-primary', 'id' => 'maskedKey')); ?></a>
                </div>
              
            </div>
          </div>

          <div class="panel-body pn">
              <?php echo Form::open(['url'=>'/advocate-panel/view-referred','name'=>'form' ,'autocomplete'=>'off']); ?>


              <div class="table-responsive">
                <table class="table admin-form table-bordered table-striped theme-warning tc-checkbox-1 fs13" id="datatable2">
                  <thead>
                    <tr class="bg-light">
                      <th class="text-left">
                        <label class="option block mn" style="width:90px !important;">
                          <input type="checkbox" id="check_all"> 
                          <span class="checkbox mn"></span>
                          Select All
                        </label>
                      </th>
                      <th class=""> Advocate Name</th>
                      <th class=""> Email</th>
                      <th class=""> Mobile No</th>
                      <th class=""> Place</th>
                      <th class=""> Address</th>
                      <th class="text-right"> Status</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php $__currentLoopData = $get_record; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_records): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                              
                    <tr>
                      <td class="text-left" style="padding-left: 18px;">
                        <label class="option block mn">
                          <input type="checkbox" name="check[]" class="check" value="<?php echo e($get_records->ref_id); ?>">
                          <span class="checkbox mn"></span>
                        </label>
                      </td>
                      <td class="" style="padding-left:20px"><?php if($get_records->ref_advocate_name != ""): ?> <?php echo e($get_records->ref_advocate_name); ?> <?php else: ?> ----- <?php endif; ?> </td>
                      <td class="" style="padding-left:20px"><?php if($get_records->ref_advocate_email != ""): ?> <?php echo e($get_records->ref_advocate_email); ?> <?php else: ?> ----- <?php endif; ?> </td>
                      <td class="" style="padding-left:20px"><?php if($get_records->ref_mobile_number != ""): ?> <?php echo e($get_records->ref_mobile_number); ?> <?php else: ?> ----- <?php endif; ?> </td>
                      <td class="" style="padding-left:20px"><?php if($get_records->ref_place != ""): ?> <?php echo e($get_records->ref_place); ?> <?php else: ?> ----- <?php endif; ?> </td>
                      <td class="" style="padding-left:20px"><?php if($get_records->ref_address != ""): ?> <?php echo e($get_records->ref_address); ?> <?php else: ?> ----- <?php endif; ?> </td>
                      <td class="text-right">
                        <div class="btn-group text-left">
                          <button type="button" class="btn <?php echo e($get_records->ref_status   == 1 ? 'btn-success' : 'btn-danger'); ?>  br2 btn-xs fs12 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> <?php echo e($get_records->ref_status  == 1 ? 'Active' : 'Deactive'); ?>

                            <span class="caret ml5"></span>
                          </button>
                          <ul class="dropdown-menu" role="menu" style="min-width:130px; left: <?php echo e($get_records->ref_status == 1 ? '-67px' : '-54px'); ?> !important;">
                            <li>
                              <a href="<?php echo e(url('/advocate-panel/add-referred')); ?>/<?php echo e(sha1($get_records->ref_id)); ?>">Edit</a>
                            </li>
                            <div class="divider"></div>                            
                            <li class="<?php echo e($get_records->ref_status   == 1 ? 'active' : ''); ?>">
                              <a href="<?php echo e(url('/advocate-panel/change-referred-status')); ?>/<?php echo e(sha1($get_records->ref_id)); ?>/1">Active</a>
                            </li>
                            <li class=" <?php echo e($get_records->ref_status  == 0 ? 'active' : ''); ?> ">
                              <a href="<?php echo e(url('/advocate-panel/change-referred-status')); ?>/<?php echo e(sha1($get_records->ref_id)); ?>/0">Deactive</a>
                            </li>
                          </ul>
                        </div>
                      </td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                 
                  </tbody>
                </table>
              </div>
              <?php echo Form::close(); ?>

          </div>
          <div class="panel-body pn">
            <div class="table-responsive">
              <table class="table admin-form theme-warning tc-checkbox-1 fs13">                                
                <tbody>
                  <tr class="">
                     <th class="text-left">
                      <button type="button" class="btn btn-primary" onclick="go_delete()"><i class="glyphicon glyphicon-trash"></i> Delete Multiple </button>
                    </th>
                    <th class="text-right">
                      <?php echo e($get_record->links()); ?>

                    </th>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


<style type="text/css">
.dt-panelfooter{
  display: none !important;
}
</style>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('advocate_admin/layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>