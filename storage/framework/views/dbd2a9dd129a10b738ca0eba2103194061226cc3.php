<?php $__env->startSection('content'); ?>

<style type="text/css">
  .admin-form .select, .admin-form .gui-input, .admin-form .select > select, .admin-form .select-multiple select{
    height: 28px !important;
  }
  .admin-form a.button, .admin-form span.button, .admin-form label.button
  {
    line-height: 28px !important;
  }
  .admin-form .append-icon .field-icon, .admin-form .prepend-icon .field-icon{
    line-height: 28px !important;
  }
  .admin-form .gui-textarea {
    line-height: 7px !important;
  }
/*  .gui-textarea{
    height: 100px !important;
  }*/
/* .admin-form .gui-input {
  padding: 5px;
 }*/
 /*.admin-form .prepend-icon .field-icon {
  top: 6px;
 }*/
 .admin-form .button{
    height: 28px !important;
    line-height: 1px !important;
  }
 .btn {
    height: 29px !important;
    line-height: 1px !important;  }

 .down{
  margin-top: 70px;
 }
.account_setting {
  margin: 0px 20px !important;
  margin-top: 25px !important;
  /*padding: 5px 0px !important;*/
 }
</style>
  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left down">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="<?php echo e(url('/advocate-panel/view-case-type')); ?>">View Case Type</a>
            </li>
            <li class="crumb-icon">
              <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">Home</a>
            </li>
            <li class="crumb-trail">Add Case Type</li>
          </ol>
        </div>
      </header>

      <div class="row">
        <div class="col-md-12">
          <?php if(\Session::has('success')): ?>
          <div class="alert alert-success account_setting" >
            <?php echo \Session::get('success'); ?>

          </div>
          <?php endif; ?>
        </div>
      </div>
      <!-- Begin: Content -->

      <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center">
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-heading">
                <span class="panel-title">Add Case Type</span>
              </div>
              <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                <!-- IF any error found -->
                <div class="col-md-12">
                <?php if($errors->any()): ?>
                  <div id="log_error" class="alert alert-danger" style='font-family: josefin_sansregular !important;'>
                  <?php echo e($errors->first()); ?>  </i></div>
                <?php endif; ?>
                </div>
                <!-- IF any error found -->
                <div id="tab1_1" class="tab-pane active">
                    <?php echo Form::open(['name'=>'form_validation','url'=>'advocate-panel/insert-case-type/'.$get_record[0]->case_id,'id'=>'form_validation' ,'autocomplete'=>'off']); ?>

                    <div class="row">
                      
                      <div class="col-md-12">
                        <div class="section" style="margin-bottom: 40px;">
                          <label for="level_name" class="field-label" style="font-weight:600;" > Category :  </label>  
                          <div class="col-md-2">
                            <label class="option" style="font-size:12px;">

                              <?php if($get_record[0]->case_category != ""): ?>
                                <?php echo Form::radio('c_name','1',$get_record[0]->case_category == 1 ? 'checked' : '', array( 'class' => 'check','onclick'=>'court_type_trail(1)','id' => 'check1' )); ?>

                              <?php else: ?> 
                                <?php echo Form::radio('c_name','1',checked, array( 'class' => 'check' , 'class' => 'check', 'onclick'=>'court_type_trail(1)' ,'id' => 'check1' )); ?>

                              <?php endif; ?>
                              <span class="radio"></span> Criminal
                            </label>
                          </div>

                          <div class="col-md-2">
                            <label class="option" style="font-size:12px;">
                              <?php echo Form::radio('c_name','2',$get_record[0]->case_category == 2 ? 'checked' : '', array( 'class' => 'check', 'onclick'=>'court_type_trail(2)' ,'id' => 'check2'  )); ?>

                              <span class="radio"></span> Civil
                            </label>
                          </div>
                        </div>

                        <div class="section"></div>
                        <div class="clearfix"></div>
                      </div>
                      <input type="hidden" name="case_id" id="case_id" value="<?php echo e($get_record[0]->case_id); ?>">

                      <input type="hidden" name="case_category_value" id="case_category_value" value="1">

                      <div class="col-md-12">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;" >Type Name :  </label>  
                          <label for="level_name" class="field prepend-icon">
                            <?php echo Form::text('type_name',$get_record[0]->case_name,array('class' => 'gui-input','placeholder' => '' , 'id' => 'type_name' )); ?>

                              <label for="Account Mobile" class="field-icon">
                              <i class="fa fa-pencil"></i>
                            </label>                           
                          </label>
                        </div>
                        </div>
                        <!-- <div class="col-md-12">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;" >short Name :  </label>  
                          <label for="level_name" class="field prepend-icon">
                            <?php echo Form::text('short_name',$get_record[0]->case_short_name,array('class' => 'gui-input','placeholder' => '' )); ?>

                              <label for="Account Mobile" class="field-icon">
                              <i class="fa fa-pencil"></i>
                            </label>                           
                          </label>
                        </div>
                        </div> -->
                        
                      </div>
                    </div>
                  <div class="panel-footer text-right">
                      <?php echo Form::submit('Save', array('class' => 'button btn-primary', 'id' => 'maskedKey')); ?>

                      <?php echo Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')); ?>

                  </div>   
                    <?php echo Form::close(); ?>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

<script type="text/javascript">

  function court_type_trail(case_category) {

    if(case_category == 1){
      $("#case_category_value").val(case_category);

      $("#type_name").removeData("previousValue"); //clear cache
      $("#form_validation").data('validator').element('#type_name'); //retrigger remote call
      $('#type_name').blur();

    } else {
      $("#case_category_value").val(case_category);

      $("#type_name").removeData("previousValue"); //clear cache
      $("#form_validation").data('validator').element('#type_name'); //retrigger remote call
      $('#type_name').blur();
    }

  }

  jQuery(document).ready(function() {
  
    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "This field contains alphabets only");

    jQuery.validator.addMethod("checkemail", function(value, element) 
    {
    return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
    }, "Enter a VALID email address"); 

    $.validator.addMethod('minStrict', function (value, el, param) {
     return value > param; 
    });
    $.validator.addMethod('maxStrict', function (value, el, param) {
     return value < param; 
    });


    /* @custom  validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#form_validation").validate({

      /* @validation  states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation  rules 
      ------------------------------------------ */

      rules: {
        type_name: {
          // required: true,
          remote: {
              url: "<?php echo e(url('advocate-panel/check-case-type-name')); ?>",
              type: "post",
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              data: {
                // affiliate_id: function() {
                //   return $( "#affiliate_id" ).val();
                // },
                type_name: function() {
                  return $( "#type_name" ).val();
                },
                // case_category: function() {
                //   return $( "#case_category_value" ).val();
                // },
                case_id: function() {
                  return $( "#case_id" ).val();
                }
                
              }
          }
      },

        // type_name: {
        //   required: true,
        //   lettersonly: true
        // },
        // short_name: {
        //   required: true,
        //   lettersonly: true
        // },
      },

      /* @validation  error messages 
      ---------------------------------------------- */

      messages: {
        type_name: {
          remote: ' Name already exists, please try with other name.'
        },
        c_name: {
          required: 'Please Fill Required Category'
        },
        short_name: {
          required: 'Please Fill Required Short Name'
        },        
      },

      /* @validation  highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });


  if($( "#case_id" ).val() != ''){
    $("#type_name").removeData("previousValue"); //clear cache
    $("#form_validation").data('validator').element('#type_name'); //retrigger remote call
    $('#type_name').blur();
  }


  </script>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('advocate_admin/layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>