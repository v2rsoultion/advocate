  <footer id="content-footer" id="topbar" class="affix" style="z-index: 11">
    <div class="row">
      <div class="col-md-6">
        <span class="footer-legal">© <?php echo date("Y"); ?> AdminPanel</span>
      </div>
      
      <div class="col-md-6 text-right">
        <span class="footer-meta">Designed & Developed by <b><a href="http://www.v2rsolution.com/" style="text-decoration: none;" title="Affordable Webdesign Company, Website Development Company, Creative Webdesign Company, Website Services, Website Packages, Hosting" target="_blank">V2R Solution</a></b> </span>
        <a href="#content" class="footer-return-top"> <span class="fa fa-arrow-up"></span> </a>
      </div>
    </div>
  </footer>
</section>
 </div>   

 <style type="text/css">
.dt-panelfooter{
  display: none !important;
}

.dataTables_filter{
  float: left;
}
</style>

<!-- jQuery -->
<?php echo Html::script('public/admin/js/jquery/jquery-1.11.1.min.js'); ?>

<?php echo Html::script('public/admin/js/jquery/jquery_ui/jquery-ui.min.js'); ?>

<?php echo Html::script('public/admin/js/jquery-te-1.4.0.min.js'); ?>

<?php echo Html::script('public/admin/js/admin-forms/js/jquery-ui-datepicker.min.js'); ?>

<!-- <?php echo Html::script('public/admin/js/admin-forms/js/jquery-ui-monthpicker.min.js'); ?> -->
<?php echo Html::script('public/admin/js/admin-forms/js/mycustom_form.js'); ?>

<?php echo Html::script('public/admin/js/plugins/select2/select2.min.js'); ?>

<?php echo Html::script('public/admin/js/plugins/magnific/jquery.magnific-popup.js'); ?>

<!-- FileUpload JS -->
<?php echo Html::script('public/admin/js/plugins/fileupload/fileupload.js'); ?>

<?php echo Html::script('public/admin/js/plugins/holder/holder.min.js'); ?>

  <!-- Datatables -->
<?php echo Html::script('public/admin/js/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js'); ?>

<?php echo Html::script('public/admin/js/plugins/datatables/media/js/jquery.dataTables.js'); ?>

<?php echo Html::script('public/admin/js/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js'); ?>

<?php echo Html::script('public/admin/js/plugins/datatables/media/js/dataTables.bootstrap.js'); ?>

<!-- Tagmanager JS -->
<?php echo Html::script('public/admin/js/plugins/tagsinput/tagsinput.min.js'); ?>

<?php echo Html::script('public/admin/js/bootstrap-formhelpers.min.js'); ?>

<!-- AdminForms Date/Month Pickers -->
<?php echo Html::script('public/admin/js/admin-forms/js/jquery-ui-monthpicker.min.js'); ?>

<?php echo Html::script('public/admin/js/admin-forms/js/jquery-ui-datepicker.min.js'); ?>

  <!-- FullCalendar Plugin + Moment Dependency -->
<?php echo Html::script('public/admin/js/plugins/fullcalendar/lib/moment.min.js'); ?>

<?php echo Html::script('public/admin/js/plugins/fullcalendar/fullcalendar.min.js'); ?>

<!-- jQuery Validate Plugin-->
<?php echo Html::script('public/admin/js/admin-forms/js/jquery.validate.min.js'); ?>

<?php echo Html::script('public/admin/js/admin-forms/js/additional-methods.min.js'); ?>

<!-- Theme Javascript -->
<?php echo Html::script('public/admin/js/utility/utility.js'); ?>

<?php echo Html::script('public/admin/js/demo/demo.js'); ?>

<?php echo Html::script('public/admin/js/main.js'); ?>

<?php echo Html::script('public/admin/js/app.js'); ?>





<!-- AdminForms JS -->

<script>
$(".select2-single").select2();
$("#switch-modal").bootstrapSwitch();
$("#switch-modal2").bootstrapSwitch();
$("#switch-modal3").bootstrapSwitch();


function close_button(){
    $('body').css('overflow','auto', 'important');
    document.getElementById('share_email').style.display='none';
  }

</script>

<script>

$('.class_numeric').keypress(function(event) {
    var $this = $(this);
    if ((event.which != 46 || $this.val().indexOf('.') != -1) && ((event.which < 48 || event.which > 57) && (event.which != 0 && event.which != 8))) {
        event.preventDefault();
    }
    var text = $(this).val();
    if ((event.which == 46) && (text.indexOf('.') == -1)) {
        setTimeout(function() {
            if ($this.val().substring($this.val().indexOf('.')).length > 3) {
                $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
            }
        }, 1);
    }
    if ((text.indexOf('.') != -1) && (text.substring(text.indexOf('.')).length > 2) && (event.which != 0 && event.which != 8) && ($(this)[0].selectionStart >= text.length - 2)) {
        event.preventDefault();
    }
});
$('.class_numeric').bind("paste", function(e) {
    var text = e.originalEvent.clipboardData.getData('Text');
    if ($.isNumeric(text)) {
        if ((text.substring(text.indexOf('.')).length > 3) && (text.indexOf('.') > -1)) {
            e.preventDefault();
            $(this).val(text.substring(0, text.indexOf('.') + 3));
        }
    } else {
        e.preventDefault();
    }
});
  
  $('.jqte-test').jqte();
  var jqteStatus = true;
  $(".status").click(function()
  {
    jqteStatus = jqteStatus ? false : true;
    $('.jqte-test').jqte({"status" : jqteStatus})
  });
</script>

<script>
$(document).ready(function() {
  $('img').on('dragstart', function(event) { event.preventDefault(); });
});
</script>
<script type="text/javascript">

$(function(){
  $('.numeric').on('input', function (event) { 
      this.value = this.value.replace(/[^0-9]/g, '');
  });
});

  jQuery(document).ready(function() {
    /* @time  picker
     ------------------------------------------------------------------ */
    $('.inline-tp').timepicker({
                    format: 'LT'
                }
    );

    $('#timepicker1').timepicker({
      beforeShow: function(input, inst) {
        var newclass = 'admin-form';
        var themeClass = $(this).parents('.admin-form').attr('class');
        var smartpikr = inst.dpDiv.parent();
        if (!smartpikr.hasClass(themeClass)) {
          inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
        }
      }
    });

    $('#timepicker2').timepicker({
      showOn: 'both',
      buttonText: '<i class="fa fa-clock-o"></i>',
      beforeShow: function(input, inst) {
        var newclass = 'admin-form';
        var themeClass = $(this).parents('.admin-form').attr('class');
        var smartpikr = inst.dpDiv.parent();
        if (!smartpikr.hasClass(themeClass)) {
          inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
        }
      }
    });

    $('#timepicker3').timepicker({
      showOn: 'both',
      disabled: true,
      buttonText: '<i class="fa fa-clock-o"></i>',
      beforeShow: function(input, inst) {
        var newclass = 'admin-form';
        var themeClass = $(this).parents('.admin-form').attr('class');
        var smartpikr = inst.dpDiv.parent();
        if (!smartpikr.hasClass(themeClass)) {
          inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
        }
      }
    });
    
    /* @date  time picker
    ------------------------------------------------------------------ */
    $('#datetimepicker1').datetimepicker({
    
    });
    
    $('#datetimepicker2').datetimepicker({
      showOn: 'both',
      buttonText: '<i class="fa fa-calendar-o"></i>',
      prevText: '<i class="fa fa-chevron-left"></i>',
      nextText: '<i class="fa fa-chevron-right"></i>',
      beforeShow: function(input, inst) {
        var newclass = 'admin-form';
        var themeClass = $(this).parents('.admin-form').attr('class');
        var smartpikr = inst.dpDiv.parent();
        if (!smartpikr.hasClass(themeClass)) {
          inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
        }
      }
    }); 

    $( "#datepicker" ).datepicker( {
      dateFormat: 'd-m-yy',
      buttonText: '<i class="fa fa-calendar-o"></i>',
      prevText: '<i class="fa fa-chevron-left"></i>',
      nextText: '<i class="fa fa-chevron-right"></i>',
    });
    $( "#registerdate" ).datepicker( {
      buttonText: '<i class="fa fa-calendar-o"></i>',
      prevText: '<i class="fa fa-chevron-left"></i>',
      nextText: '<i class="fa fa-chevron-right"></i>',
    });
    // $( "#datefuther" ).datepicker( {
    //   dateFormat: 'd-m-yy',
    //   buttonText: '<i class="fa fa-calendar-o"></i>',
    //   prevText: '<i class="fa fa-chevron-left"></i>',
    //   nextText: '<i class="fa fa-chevron-right"></i>',
    // });


    $("#datefuthernew").datepicker({
     dateFormat: 'd-m-yy', 
     maxDate: '0', 
    // minDate: '0',
     buttonText: '<i class="fa fa-calendar-o"></i>',
     prevText: '<i class="fa fa-chevron-left"></i>',
     nextText: '<i class="fa fa-chevron-right"></i>',

     onSelect: function (selected) {
        $(".datefuthernew").addClass("state-success");
        $(".datefuthernew").removeClass("state-error");
        $(".section_date .state-error").html("");
      }
      
    });


    $("#end_date").datepicker({
     dateFormat: 'd-m-yy', 
  //   maxDate: '0', 
     minDate: '0',
     buttonText: '<i class="fa fa-calendar-o"></i>',
     prevText: '<i class="fa fa-chevron-left"></i>',
     nextText: '<i class="fa fa-chevron-right"></i>',
    });


    $("#onlyyear").datepicker({
        format: "yyyy",
        viewMode: "years", 
        minViewMode: "years"
    });



    $(".fromdate").datepicker({
        dateFormat: 'd-m-yy',
      //  minDate: '0',
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>',  
        changeMonth: true,
        changeYear: true,
        numberOfMonths: 1,
        yearRange: "-50:+30",
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() + 1);
            $(".todate").datepicker("option", "minDate", dt);
            $(".fromdate").addClass("state-success");
            $(".fromdate").removeClass("state-error");
            $(".section_from .state-error").html("");
        }
    });

    $(".reg_from").datepicker({
        dateFormat: 'd-m-yy',
        maxDate: '0',
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>',  
        numberOfMonths: 1,
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() + 1);
            $(".reg_to").datepicker("option", "minDate", dt);
            $(".reg_from").addClass("state-success");
            $(".reg_from").removeClass("state-error");
            $(".section_from .state-error").html("");
        }
    });

    $(".reg_to").datepicker({
        dateFormat: 'd-m-yy',  
        minDate: '0',
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>',
        numberOfMonths: 1,
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() - 1);
            $(".reg_from").datepicker("option", "maxDate", dt);
            $(".reg_to").addClass("state-success");
            $(".reg_to").removeClass("state-error");
            $(".section_to .state-error").html("");
        }
    });

    $(".disposal_from").datepicker({
        dateFormat: 'd-m-yy',
      //  maxDate: '0',
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>',  
        changeMonth: true,
        changeYear: true,
        numberOfMonths: 1,
        yearRange: "-50:+30",
        onSelect: function (selected) {
            // var dt = new Date(selected);
            // dt.setDate(dt.getDate() + 1);
            // $(".disposal_to").datepicker("option", "minDate", dt);
            // $(".disposal_from").addClass("state-success");
            // $(".disposal_from").removeClass("state-error");
            // $(".section_from .state-error").html("");
        }
    });

    $(".disposal_to").datepicker({
        dateFormat: 'd-m-yy',  
     //   minDate: '0',
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>',
        changeMonth: true,
        changeYear: true,
        numberOfMonths: 1,
        yearRange: "-50:+30",
        onSelect: function (selected) {

          $('.mysave').removeAttr('disabled');
          $('.disposal_to').next().next().html("");
          $('.disposal_to').next().next().css("color", "#DE888A");
          $('.disposal_to').parent().addClass("state-success");
          
            // var dt = new Date(selected);
            // dt.setDate(dt.getDate() - 1);
            // $(".disposal_from").datepicker("option", "maxDate", dt);
            // $(".disposal_to").addClass("state-success");
            // $(".disposal_to").removeClass("state-error");
            // $(".section_to .state-error").html("");
        }
    });

    $(".fromdate_search").datepicker({
        dateFormat: 'd-m-yy',
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>',  
        changeMonth: true,
        changeYear: true,
        numberOfMonths: 1,
        yearRange: "-50:+30",
        onSelect: function (selected) {
            // var dt = new Date(selected);
            // dt.setDate(dt.getDate() + 1);
            // $(".todate").datepicker("option", "minDate", dt);
            // $(".fromdate_search").addClass("state-success");
            // $(".fromdate_search").removeClass("state-error");
            // $(".section_from .state-error").html("");
        }
    });

    $(".fromdate_search_new").datepicker({
        dateFormat: 'd-m-yy',
        minDate: '0',
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>',  
        changeMonth: true,
        changeYear: true,
        numberOfMonths: 1,
        yearRange: "-50:+30",
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() + 1);
            $(".todate").datepicker("option", "minDate", dt);
            // $(".fromdate_search").addClass("state-success");
            // $(".fromdate_search").removeClass("state-error");
            // $(".section_from .state-error").html("");
        }
    });

    $(".todate").datepicker({
        dateFormat: 'd-m-yy',  
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>',
        changeMonth: true,
        changeYear: true,
        numberOfMonths: 1,
        yearRange: "-50:+30",
        onSelect: function (selected) {
            // var dt = new Date(selected);
            // dt.setDate(dt.getDate() - 1);
            // $(".fromdate_search").datepicker("option", "maxDate", dt);
            $(".todate").addClass("state-success");
            $(".todate").removeClass("state-error");
            $(".section_to .state-error").html("");
        }
    });


    $(".fromdate_certified").datepicker({
        dateFormat: 'd-m-yy',
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>',  
        changeMonth: true,
        changeYear: true,
        numberOfMonths: 1,
        yearRange: "-50:+30",
        onSelect: function (selected) {
            // var dt = new Date(selected);
            // dt.setDate(dt.getDate() + 1);
            // $(".todate_certified").datepicker("option", "minDate", dt);
            // $(".fromdate_certified").addClass("state-success");
            // $(".fromdate_certified").removeClass("state-error");
            // $(".section_from .state-error").html("");
        }
    });

    $(".todate_certified").datepicker({
        dateFormat: 'd-m-yy',  
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>',
        changeMonth: true,
        changeYear: true,
        numberOfMonths: 1,
        yearRange: "-50:+30",
        onSelect: function (selected) {
            // var dt = new Date(selected);
            // dt.setDate(dt.getDate() - 1);
            // $(".fromdate_certified").datepicker("option", "maxDate", dt);
            // $(".todate_certified").addClass("state-success");
            // $(".todate_certified").removeClass("state-error");
            // $(".section_to .state-error").html("");
        }
    });

    


    $(".todate_new").datepicker({
        dateFormat: 'd-m-yy',  
        minDate: '0',
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>',
        numberOfMonths: 1,
        // onSelect: function (selected) {
        //     var dt = new Date(selected);
        //     dt.setDate(dt.getDate() - 1);
        //     $(".fromdate").datepicker("option", "maxDate", dt);
        //     $(".todate_new").addClass("state-success");
        //     $(".todate_new").removeClass("state-error");
        //     $(".section_to .state-error").html("");
        // }
    });
  

    

    $("#datefuther").datepicker({
     dateFormat: 'd-m-yy', 
     // maxDate: '0', 
      minDate: '0',
      changeMonth: true,
      changeYear: true,
      numberOfMonths: 1,
      yearRange: "-50:+30",
     buttonText: '<i class="fa fa-calendar-o"></i>',
     prevText: '<i class="fa fa-chevron-left"></i>',
     nextText: '<i class="fa fa-chevron-right"></i>',
    });

    $("#datepicker_further").datepicker({
     dateFormat: 'd-m-yy', 
     // maxDate: '0', 
     minDate: '0',
     buttonText: '<i class="fa fa-calendar-o"></i>',
     prevText: '<i class="fa fa-chevron-left"></i>',
     nextText: '<i class="fa fa-chevron-right"></i>',
    });

    $(".fromdate_notice").datepicker({
        dateFormat: 'd-m-yy',
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>',  
        changeMonth: true,
        changeYear: true,
        numberOfMonths: 1,
        yearRange: "-50:+30",
        onSelect: function (selected) {

          $('.mysave').removeAttr('disabled');
          $('.fromdate_notice').next().next().html("");
          $('.fromdate_notice').parent().next().html("");
          $('.fromdate_notice').next().next().css("color", "#DE888A");
          $('.fromdate_notice').parent().addClass("state-success");

            // var dt = new Date(selected);
            // dt.setDate(dt.getDate() + 1);
            // $(".todate_notice").datepicker("option", "minDate", dt);
            // $(".fromdate_notice").addClass("state-success");
            // $(".fromdate_notice").removeClass("state-error");
            //$(".section_from .state-error").html("");
        },
    });


    $(".todate_notice_new").datepicker({
        dateFormat: 'd-m-yy',  
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>',
        changeMonth: true,
        changeYear: true,
        numberOfMonths: 1,
        yearRange: "-50:+30",
        onSelect: function (selected) {          

            var date = $('#date_picker').val();

          value=selected.split("-");
          var newDate_notice =value[1]+"/"+value[0]+"/"+value[2];
          var notice_status_date = new Date(newDate_notice).getTime();
          
          var notice_issue_date = $('#date_picker').val();
          notice_issue_date = notice_issue_date.split("-");
          var newDate_notice_issue = notice_issue_date[1]+"/"+notice_issue_date[0]+"/"+notice_issue_date[2];
          var notice_issue_date = new Date(newDate_notice_issue).getTime();


            if(notice_status_date < notice_issue_date){
              $('.todate_notice_new').next().next().html("Enter date in dd-mm-yyyy format.");
              $('.todate_notice_new').next().next().css("color", "#DE888A");
              $('#maskedKey_submit').attr('disabled','disabled');
              
            } else {
            
            $('.todate_notice_new').next().next().html("");
            $('.todate_notice_new').parent().next().html("");
            $('.todate_notice_new').next().next().css("color", "#DE888A");
            $('.todate_notice_new').parent().addClass("state-success");
            $('#maskedKey_submit').removeAttr('disabled');

          }
           
            // $(".fromdate_notice").datepicker("option", "maxDate", dt);
            // $(".todate_notice").addClass("state-success");
            // $(".todate_notice").removeClass("state-error");
            //$(".section_to .state-error").html("");
        }
    });


    $(".todate_notice").datepicker({
        dateFormat: 'd-m-yy',  
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>',
        changeMonth: true,
        changeYear: true,
        numberOfMonths: 1,
        yearRange: "-50:+30",
        onSelect: function (selected) {          

            $('.todate_notice').next().next().html("");
            $('.todate_notice').parent().next().html("");
            $('.todate_notice').next().next().css("color", "#DE888A");
            $('.todate_notice').parent().addClass("state-success");

            // var dt = new Date(selected);
            // dt.setDate(dt.getDate() - 1);
            // $(".fromdate_notice").datepicker("option", "maxDate", dt);
            // $(".todate_notice").addClass("state-success");
            // $(".todate_notice").removeClass("state-error");
            //$(".section_to .state-error").html("");
        }
    });




// $('.class_numeric').keypress(function(event) {
//     var $this = $(this);
//     if ((event.which != 46 || $this.val().indexOf('.') != -1) && ((event.which < 48 || event.which > 57) && (event.which != 0 && event.which != 8))) {
//         event.preventDefault();
//     }
//     var text = $(this).val();
//     if ((event.which == 46) && (text.indexOf('.') == -1)) {
//         setTimeout(function() {
//             if ($this.val().substring($this.val().indexOf('.')).length > 3) {
//                 $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
//             }
//         }, 1);
//     }
//     if ((text.indexOf('.') != -1) && (text.substring(text.indexOf('.')).length > 2) && (event.which != 0 && event.which != 8) && ($(this)[0].selectionStart >= text.length - 2)) {
//         event.preventDefault();
//     }
// });
// $('.class_numeric').bind("paste", function(e) {
//     var text = e.originalEvent.clipboardData.getData('Text');
//     if ($.isNumeric(text)) {
//         if ((text.substring(text.indexOf('.')).length > 3) && (text.indexOf('.') > -1)) {
//             e.preventDefault();
//             $(this).val(text.substring(0, text.indexOf('.') + 3));
//         }
//     } else {
//         e.preventDefault();
//     }
// });
  
//   $('.jqte-test').jqte();
//   var jqteStatus = true;
//   $(".status").click(function()
//   {
//     jqteStatus = jqteStatus ? false : true;
//     $('.jqte-test').jqte({"status" : jqteStatus})
//   });
//


// $(document).ready(function() {
//   $('img').on('dragstart', function(event) { event.preventDefault(); });
// });
// 


// $(function(){
//   $('.numeric').on('input', function (event) { 
//       this.value = this.value.replace(/[^0-9]/g, '');
//   });
// });


    

    // $( "#issuedate" ).datepicker(
    // {
    //     minDate: new Date(),
    //     changeMonth: true,
    //     numberOfMonths: 1,
    //     onClose: function( selectedDate )
    //     {
    //         $( "#expdate" ).datepicker( "option", "minDate", selectedDate );
    //     }
    // });

    // $( "#expdate" ).datepicker(
    // {
    //     minDate: new Date(),
    //     changeMonth: true,
    //     numberOfMonths: 1,
    //     onClose: function( selectedDate )
    //     {
    //         $( "#issuedate" ).datepicker( "option", "maxDate", selectedDate );
    //     }
    // });

    // $("#notice_issue_date").datepicker({
    //  dateFormat: 'd-m-yy', 
    //  buttonText: '<i class="fa fa-calendar-o"></i>',
    //  prevText: '<i class="fa fa-chevron-left"></i>',
    //  nextText: '<i class="fa fa-chevron-right"></i>',
    // });


    




    $("#compliance_date").datepicker({
     dateFormat: 'd-m-yy', 
     buttonText: '<i class="fa fa-calendar-o"></i>',
     prevText: '<i class="fa fa-chevron-left"></i>',
     nextText: '<i class="fa fa-chevron-right"></i>',
    });


    $("#certified_copy_date").datepicker({
     dateFormat: 'd-m-yy', 
     buttonText: '<i class="fa fa-calendar-o"></i>',
     prevText: '<i class="fa fa-chevron-left"></i>',
     nextText: '<i class="fa fa-chevron-right"></i>',
    });
    

    $('#datetimepicker3').datetimepicker({
      showOn: 'both',
      buttonText: '<i class="fa fa-calendar-o"></i>',
      disabled: true,
      prevText: '<i class="fa fa-chevron-left"></i>',
      nextText: '<i class="fa fa-chevron-right"></i>',
      beforeShow: function(input, inst) {
        var newclass = 'admin-form';
        var themeClass = $(this).parents('.admin-form').attr('class');
        var smartpikr = inst.dpDiv.parent();
        if (!smartpikr.hasClass(themeClass)) {
          inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
        }
      }
    });

    $('.inline-dtp').datetimepicker({
      prevText: '<i class="fa fa-chevron-left"></i>',
      nextText: '<i class="fa fa-chevron-right"></i>',
    });

    /* @date  picker
    ------------------------------------------------------------------ */
    $("#datepicker1").datepicker({
      prevText: '<i class="fa fa-chevron-left"></i>',
      nextText: '<i class="fa fa-chevron-right"></i>',
      showButtonPanel: false,
      beforeShow: function(input, inst) {
        var newclass = 'admin-form';
        var themeClass = $(this).parents('.admin-form').attr('class');
        var smartpikr = inst.dpDiv.parent();
        if (!smartpikr.hasClass(themeClass)) {
          inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
        }
      }
    });
    
    $("#datepicker").datepicker({
      minDate: '0',
      prevText: '<i class="fa fa-chevron-left"></i>',
      nextText: '<i class="fa fa-chevron-right"></i>',
      showButtonPanel: false,
      beforeShow: function(input, inst) {
        var newclass = 'admin-form';
        var themeClass = $(this).parents('.admin-form').attr('class');
        var smartpikr = inst.dpDiv.parent();
        if (!smartpikr.hasClass(themeClass)) {
          inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
        }
      }
    });

    $('#datepicker2').datepicker({
      numberOfMonths: 3,
      showOn: 'both',
      buttonText: '<i class="fa fa-calendar-o"></i>',
      prevText: '<i class="fa fa-chevron-left"></i>',
      nextText: '<i class="fa fa-chevron-right"></i>',
      beforeShow: function(input, inst) {
        var newclass = 'admin-form';
        var themeClass = $(this).parents('.admin-form').attr('class');
        var smartpikr = inst.dpDiv.parent();
        if (!smartpikr.hasClass(themeClass)) {
          inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
        }
      }
    });

    $('#datepicker3').datepicker({
      showOn: 'both',
      disabled: true,
      buttonText: '<i class="fa fa-calendar-o"></i>',
      prevText: '<i class="fa fa-chevron-left"></i>',
      nextText: '<i class="fa fa-chevron-right"></i>',
      beforeShow: function(input, inst) {
        var newclass = 'admin-form';
        var themeClass = $(this).parents('.admin-form').attr('class');
        var smartpikr = inst.dpDiv.parent();
        if (!smartpikr.hasClass(themeClass)) {
          inst.dpDiv.wrap('<div class="' + themeClass + '"></div>');
        }
      }
    });

    $('.inline-dp').datepicker({
      numberOfMonths: 1,
      prevText: '<i class="fa fa-chevron-left"></i>',
      nextText: '<i class="fa fa-chevron-right"></i>',
      showButtonPanel: false
    });
  });
    </script>

<script type="text/javascript">
jQuery(document).ready(function() {

"use strict";

// Init Theme Core    
Core.init();

// Init Demo JS    
Demo.init();        

// Init Boostrap Multiselects
$('#multiselect2').multiselect({
  includeSelectAllOption: true
});

// Init Boostrap Multiselects
$('#multiselect3').multiselect({
  includeSelectAllOption: true
}); 

// Init Boostrap Multiselects
$('#multiselect4').multiselect({
  includeSelectAllOption: true
}); 
 

// select dropdowns - placeholder like creation
    var selectList = $('.admin-form select');
    selectList.each(function(i, e) {
      $(e).on('change', function() {
        if ($(e).val() == "0") $(e).addClass("empty");
        else $(e).removeClass("empty")
      });
    });
    selectList.each(function(i, e) {
      $(e).change();
    });

    // Init tagsinput plugin
    $("input#tagsinput").tagsinput({
      tagClass: function(item) {
        return 'label label-default';
      }
    });

     // Init DataTables
    $('#datatable').dataTable({
      "sDom": 't<"dt-panelfooter clearfix"ip>',
      "displayLength": -1,
    });

    $('#datatable_1').dataTable({
      "sDom": 't<"dt-panelfooter clearfix"ip>',
      "displayLength": -1,
    });

    $('#datatable_2').dataTable({
      "sDom": 't<"dt-panelfooter clearfix"ip>',
      "displayLength": -1,
    });



    $('#datatable2').dataTable({
      "aoColumnDefs": [{
        'bSortable': false,
        'aTargets': [-1]
      }],
      "oLanguage": {
        "oPaginate": {
          "sPrevious": "",
          "sNext": ""
        }
      },
      "iDisplayLength": -1,
      
      "sDom": '<"dt-panelmenu clearfix"lfr>t<"dt-panelfooter clearfix"ip>',
      "oTableTools": {
        "sSwfPath": "vendor/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
      }
    });

    $('.dataTables_filter input').attr("placeholder", "Enter Terms...");
    $('div.dataTables_filter input').focus();
    $('#datatable2_length').hide();



});
</script>

<script type="text/javascript">
// To delete Selected Records
function go_delete() {
    if($(".check:checked").length) {
        if(confirm("Are you sure to want delete selected records?")) {
            $("[name=form]").submit();
        }
    } else {
        alert("Please select atleast one record to delete");
    }
}


function selectall(){
  if($('#selcheck').prop("checked") == true){
    $('.check').prop('checked', true);
  }else{
    $('.check').prop('checked', false);
  }
}

function checkakk(){
  var flag = ( $(".check:checked").length == $(".check").length ) ? true : false
  $("#selcheck").prop("checked", flag);
}

$(function() {
    $("#check_all").on("click", function() {

        $(".check").prop("checked",$(this).prop("checked"));
        // var get_all_records = $("#get_all_records").val();

        // alert(get_all_records);
      //  $("#all_reg_id").prop("checked", flag);

    });

    $(".check").on("click", function() {
        var flag = ( $(".check:checked").length == $(".check").length ) ? true : false
        $("#check_all").prop("checked", flag);
    });
});

function selected() {
    if($(".check:checked").length) {
        $("[name=form]").submit();
    } else {
        alert("Please select atleast one record to download");
    }
}

function update_record() {
    if(confirm("Are you sure to want update records?")) {
      $("[name=form]").submit();    
    }
}

</script>


<script type="text/javascript">
  function generate_code() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    for(var i = 0; i < 6; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    $("#discount_code").val(text);
  }
</script>




</body>
</html><!---index page ui stop -->
