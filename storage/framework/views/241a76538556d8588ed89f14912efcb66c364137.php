<?php $__env->startSection('content'); ?>

<style type="text/css">
  /*.admin-form .select, .admin-form .gui-input, .admin-form .select > select, .admin-form .select-multiple select{
    height: 28px !important;
  }*/
  .admin-form .append-icon .field-icon, .admin-form .prepend-icon .field-icon{
    line-height: 28px !important;
  }
  .admin-form .gui-textarea {
    line-height: 7px !important;
  }

  .form-control {
    height: 28px !important;
  }
  .admin-form .button{
    height: 28px !important;
    line-height: 1px !important;
  }
  .btn {
    height: 29px !important;
    line-height: 1px !important;  }

 .down{
  margin-top: 70px;
 }
  .select-text {
    padding: 0px 12px !important;
}

 .close{
  margin-top: -6px !important;
  font-size: 30px !important;
  width: 30px !important;
}

</style>
  <!-- Start: Content-Wrapper -->
  <section id="content_wrapper">
    <header id="topbar">
      <div class="topbar-left down">
        <ol class="breadcrumb">
          <li class="crumb-active">
            <a href="<?php echo e(url('/advocate-panel/add-client')); ?>">Add Client</a>
          </li>
          <li class="crumb-icon">
            <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">
              <span class="glyphicon glyphicon-home"></span>
            </a>
          </li>
          <li class="crumb-link">
            <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">Home</a>
          </li>
          <li class="crumb-trail">View Client </li>
        </ol>
      </div>
    </header>

    <div class="" style="margin-top:10px;">
      <div class="col-md-12">
        <div class="panel panel-primary panel-border top mb70">  
          <div class="panel-heading">
            <div class="panel-title hidden-xs">
              <span class="glyphicon glyphicon-tasks"></span> View Client</div>
          </div>

          <div class="panel-menu admin-form theme-primary" style="padding: 5px 20px;">
            
            <?php echo Form::open(['url'=>'/advocate-panel/view-client' ,'autocomplete'=>'off']); ?>

              
              <div class="row">
        
                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon">
                    <?php echo Form::text('cl_group_name','',array('class' => 'form-control','placeholder' => 'Name', 'autocomplete' => 'off' )); ?>

                    <label for="pincode" class="field-icon">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>
                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon">
                    <?php echo Form::text('cl_father_name','',array('class' => 'form-control','placeholder' => 'Guardian Name', 'autocomplete' => 'off' )); ?>

                    <label for="pincode" class="field-icon">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>
                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon">
                    <?php echo Form::text('cl_group_email_id','',array('class' => 'form-control','placeholder' => 'Email Id', 'autocomplete' => 'off' )); ?>

                    <label for="pincode" class="field-icon">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>
                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon">
                    <?php echo Form::text('cl_group_place','',array('class' => 'form-control','placeholder' => 'Place', 'autocomplete' => 'off' )); ?>

                    <label for="pincode" class="field-icon">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>
                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon">
                    <?php echo Form::text('cl_group_mobile_no','',array('class' => 'form-control','placeholder' => 'Mobile No', 'autocomplete' => 'off' )); ?>

                    <label for="pincode" class="field-icon">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>

                <div class="col-md-2 mb10">
                  <label for="pincode" class="field prepend-icon">
                    <select class="form-control select-text" id="cl_group_type" name="cl_group_type" style="color: black;">
                      <option value=''>Select Type</option>    
                      <option value="1" <?php echo e($cl_group_type == 1 ? 'selected="selected"' : ''); ?> > Individual </option>          
                      <option value="2" <?php echo e($cl_group_type == 2 ? 'selected="selected"' : ''); ?> > Organisation </option>
                    </select>
                    <i class="arrow double"></i>
                  </label>
                </div>
                          
              </div>

              <div class="row">
                <div class="col-md-1">
                   <a href="<?php echo e(url('/advocate-panel/view-client')); ?>"><?php echo Form::button('Default', array('class' => 'btn btn-primary', 'id' => 'maskedKey')); ?></a>
                </div>

                <div class="col-md-2 ">
                   <a href="<?php echo e(url('/advocate-panel/view-client/all')); ?>"><?php echo Form::button('Show All Records', array('class' => 'btn btn-primary', 'id' => 'maskedKey')); ?></a>
                </div>


                <div class="col-md-1  pull-right mr15">
                  <button type="submit" name="search" class="button btn-primary"> Search </button>
                </div>
              </div>

            <?php echo Form::close(); ?> 
          </div>

          <div class="panel-body pn">
              <?php echo Form::open(['url'=>'/advocate-panel/view-client','name'=>'form' ,'autocomplete'=>'off']); ?>


              <div class="table-responsive">
                <table class="table admin-form table-bordered table-striped theme-warning tc-checkbox-1 fs13" id="datatable2">
                  <thead>
                    <tr class="bg-light">
                      <th style="width:90px !important;" class="text-left">
                        <label class="option block mn" style="width:90px !important;">
                          <input type="checkbox" id="check_all"> 
                          <span class="checkbox mn"></span>
                          Select All
                        </label>
                      </th>
                      <th class="">Name</th>
                      <th class="">Prefix</th>
                      <th class="">Guardian Name</th>
                      <!-- <th class="">Email Id</th> -->
                      <th class="">Mobile No</th>
                      <th class="">Place</th>
                      <th class="">Address</th>
                      <th class="">Type</th>
                      <th class="">Sub Group</th>
                      <th class="text-right">Status</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php $__currentLoopData = $get_record; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_records): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>               
                    <tr>
                      <td class="text-left" style="padding-left: 18px;">
                        <label class="option block mn">
                          <input type="checkbox" name="check[]" class="check" value="<?php echo e($get_records->cl_id); ?>">
                          <span class="checkbox mn"></span>
                        </label>
                      </td>
                      <td class="" style="padding-left:20px"> <?php if($get_records->cl_group_name != ""): ?> <?php echo e($get_records->cl_group_name); ?> <?php else: ?> ----- <?php endif; ?> </td>

                      <td class="" style="padding-left:20px"> <?php if($get_records->cl_name_prefix != ""): ?> <?php echo e($get_records->cl_name_prefix); ?> <?php else: ?> ----- <?php endif; ?> </td>

                      <td class="" style="padding-left:20px"> <?php if($get_records->cl_father_name != ""): ?> <?php echo e($get_records->cl_father_name); ?> <?php else: ?> ----- <?php endif; ?> </td>

                      <!-- <td class="text-left" style="padding-left:20px"> <?php if($get_records->cl_group_email_id != ""): ?> <?php echo e($get_records->cl_group_email_id); ?> <?php else: ?> ----- <?php endif; ?> </td> -->
                      <td class="text-left" style="padding-left:20px"> <?php if($get_records->cl_group_mobile_no != ""): ?> <?php echo e($get_records->cl_group_mobile_no); ?> <?php else: ?> ----- <?php endif; ?> </td>
                      <td class="text-left" style="padding-left:20px"> <?php if($get_records->cl_group_place != ""): ?> <?php echo e($get_records->cl_group_place); ?> <?php else: ?> ----- <?php endif; ?> </td>
                      <td class="text-left" style="padding-left:20px"> <?php if($get_records->cl_group_address != ""): ?> <?php echo e($get_records->cl_group_address); ?> <?php else: ?> ----- <?php endif; ?> </td>
                      <td class="text-left" style="padding-left:20px"> <?php if($get_records->cl_group_type == 1): ?> Individual <?php elseif($get_records->cl_group_type == 2): ?> Organisation <?php endif; ?> </td>

                      <td class="text-center" style="padding-left:20px">
                        
                        <button type="button" class="btn btn-primary view btn-xs" onclick="view_image(<?php echo e($get_records->cl_id); ?>)">
                          <a href="#" style="text-transform: capitalize; text-decoration:none;"> View </a>
                        </button>
                      </td>
                      <td class="text-right">
                        <div class="btn-group text-left">
                          <button type="button" class="btn <?php echo e($get_records->cl_status   == 1 ? 'btn-success' : 'btn-danger'); ?>  br2 btn-xs fs12 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> <?php echo e($get_records->cl_status  == 1 ? 'Active' : 'Deactive'); ?>

                            <span class="caret ml5"></span>
                          </button>
                          <ul class="dropdown-menu" role="menu" style="min-width:130px; left: <?php echo e($get_records->cl_status == 1 ? '-67px' : '-54px'); ?> !important;">
                            <li>
                              <a href="<?php echo e(url('/advocate-panel/add-client')); ?>/<?php echo e(sha1($get_records->cl_id)); ?>">Edit</a>
                            </li>
                            <div class="divider"></div>                            
                            <li class="<?php echo e($get_records->cl_status   == 1 ? 'active' : ''); ?>">
                              <a href="<?php echo e(url('/advocate-panel/change-client-status')); ?>/<?php echo e(sha1($get_records->cl_id)); ?>/1">Active</a>
                            </li>
                            <li class=" <?php echo e($get_records->cl_status  == 0 ? 'active' : ''); ?> ">
                              <a href="<?php echo e(url('/advocate-panel/change-client-status')); ?>/<?php echo e(sha1($get_records->cl_id)); ?>/0">Deactive</a>
                            </li>
                          </ul>
                        </div>
                      </td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                 
                  </tbody>
                </table>
              </div>
              <?php echo Form::close(); ?>

          </div>
          <div class="panel-body pn">
            <div class="table-responsive">
              <table class="table admin-form theme-warning tc-checkbox-1 fs13">                                
                <tbody>
                  <tr class="">
                     <th class="text-left">
                      <button type="button" class="btn btn-primary" onclick="go_delete()"><i class="glyphicon glyphicon-trash"></i> Delete Multiple </button>
                    </th>
                    <th class="text-right">
                      <?php echo e($get_record->links()); ?>

                    </th>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


<?php $__currentLoopData = $get_record; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_records): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

<div id="view_image<?php echo e($get_records->cl_id); ?>" class="modal fade in" role="dialog" style="overflow: scroll;">
  <div class="modal-dialog" style="width:1000px; margin-top:80px;">
    <div class="modal-content">
      <div class="modal-header" style="padding-bottom: 35px;">
        <button type="button" class="close" data-dismiss="modal" onclick="close_button_image(<?php echo e($get_records->cl_id); ?>)" >&times;</button>
          <h4 class="modal-title pull-left">
            Sub Group
          </h4>
      </div>
      <div class="modal-body">
        <?php
          $sub_client =  \App\Model\Sub_Client\Sub_Client::where(['client_id' => $get_records->cl_id ])->get();
        ?>

        <?php if(count($sub_client) != 0): ?>
        <table class="table table-bordered mbn">
          <thead>
            <tr class="bg-light">
            <th class="text-left" style="padding-left: 10px !important;">Name</th>
            <th class="text-left" style="padding-left: 10px !important;">Prefix</th>
            <th class="text-left" style="padding-left: 10px !important;">Guardian Name</th>
            <th class="text-left" style="padding-left: 10px !important;">Email Id</th>
            <th class="text-left" style="padding-left: 10px !important;">Mobile No</th>
            <th class="text-left" style="padding-left: 10px !important;">Place</th>
            <th class="text-left" style="padding-left: 10px !important;">Address</th>
          </tr>
        </thead>
        <tbody>            
          
          <?php $__currentLoopData = $sub_client; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub_clients): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr class="">
              <td class="text-left"> <?php if($sub_clients->sub_client_name != ""): ?> <?php echo e($sub_clients->sub_client_name); ?> <?php else: ?> ----- <?php endif; ?> </td>
              <td class="text-left"> <?php if($sub_clients->sub_name_prefix != ""): ?> <?php echo e($sub_clients->sub_name_prefix); ?> <?php else: ?> ----- <?php endif; ?> </td>
              <td class="text-left"> <?php if($sub_clients->sub_guardian_name != ""): ?> <?php echo e($sub_clients->sub_guardian_name); ?> <?php else: ?> ----- <?php endif; ?> </td>
              <td class="text-left"> <?php if($sub_clients->sub_client_email_id != ""): ?> <?php echo e($sub_clients->sub_client_email_id); ?> <?php else: ?> ----- <?php endif; ?> </td>
              <td class="text-left"> <?php if($sub_clients->sub_client_mobile_no != ""): ?> <?php echo e($sub_clients->sub_client_mobile_no); ?> <?php else: ?> ----- <?php endif; ?> </td>
              <td class="text-left"> <?php if($sub_clients->sub_client_place != ""): ?> <?php echo e($sub_clients->sub_client_place); ?> <?php else: ?> ----- <?php endif; ?> </td>
              <td class="text-left"> <?php if($sub_clients->sub_client_address != ""): ?> <?php echo e($sub_clients->sub_client_address); ?> <?php else: ?> ----- <?php endif; ?> </td>
            </tr>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
      </table>
      <?php else: ?> 

      <div>No Record Found !!</div>

      <?php endif; ?>
      </div>
      <div class="clearfix"></div>
    </div> 
  </div>
</div>

<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

<style type="text/css">
.view a{
  color:#fff !important;
}
</style>
<style type="text/css">
.dt-panelfooter{
  display: none !important;
}
</style>

<script type="text/javascript">
  
  function view_image(cl_id){
    $('body').css('overflow','hidden');
    $("html, body").animate({ scrollTop: 0 }, "slow");  
    document.getElementById('view_image'+cl_id).style.display='block';
  }

  function close_button_image(cl_id){
    $('body').css('overflow','auto', 'important');
    document.getElementById('view_image'+cl_id).style.display='none';
  }


</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('advocate_admin/layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>