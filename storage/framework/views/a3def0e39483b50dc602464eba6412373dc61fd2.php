<?php $__env->startSection('content'); ?>

  <!-- Start: Content-Wrapper -->
  <section id="content_wrapper">
    <header id="topbar">
      <div class="topbar-left">
        <ol class="breadcrumb">
          <li class="crumb-active">
            <a href="<?php echo e(url('/advocate-panel/add-date')); ?>">Add Date</a>
          </li>
          <li class="crumb-icon">
            <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">
              <span class="glyphicon glyphicon-home"></span>
            </a>
          </li>
          <li class="crumb-link">
            <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">Home</a>
          </li>
          <li class="crumb-trail">View Date</li>
        </ol>
      </div>
    </header>

    <div class="" style="margin-top:10px;">
      <div class="col-md-12">
        <div class="panel panel-primary panel-border top mb35">  
          <div class="panel-heading">
            <div class="panel-title hidden-xs">
              <span class="glyphicon glyphicon-tasks"></span>View Date</div>
          </div>

          <div class="panel-menu admin-form theme-primary">
            <div class="row">
              <?php echo Form::open(['url'=>'/advocate-panel/view-date']); ?>


                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon">
                    <?php echo Form::text('cdate_case_no','',array('class' => 'form-control product','placeholder' => 'Case No', 'autocomplete' => 'off' )); ?>

                    <label for="pincode" class="field-icon">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>
                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon">
                    <?php echo Form::text('cdate_title','',array('class' => 'form-control product','placeholder' => 'Title', 'autocomplete' => 'off' )); ?>

                    <label for="pincode" class="field-icon">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>
                <div class="col-md-2">
                    <label for="level" class="field prepend-icon">
                      <select class=" form-control" name="cdate_type" id="level" >
                        <option value="">Choose Type</option>
                            <option value="1">long date</option>
                            <option value="2">short date</option> 
                      </select>
                    </label>
                </div>
                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon">
                    <?php echo Form::text('cdate_date','',array('class' => 'form-control product','placeholder' => 'Date', 'autocomplete' => 'off' )); ?>

                    <label for="pincode" class="field-icon">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>

                <div class="col-md-1">
                  <button type="submit" name="search" class="button btn-primary"> Search </button>
                </div>
              <?php echo Form::close(); ?>             
                <div class="col-md-1 pull-right">
                   <a href="<?php echo e(url('/advocate-panel/view-date/')); ?>"><?php echo Form::submit('Default', array('class' => 'btn btn-primary', 'id' => 'maskedKey')); ?></a>
                </div>
              </div>
            </div>

          <div class="panel-body pn">
              <?php echo Form::open(['url'=>'/advocate-panel/view-date','name'=>'form']); ?>

              <div class="table-responsive">
                <table class="table admin-form theme-warning tc-checkbox-1 fs13" id="datatable">
                  <thead>
                    <tr class="bg-light">
                      <th style="width:90px !important;" class="text-left">
                        <label class="option block mn">
                          <input type="checkbox" id="check_all"> 
                          <span class="checkbox mn"></span>
                          Select All
                        </label>
                      </th>
                      <th class="">Case No</th>
                      <th class="">Title</th>
                      <th class="">Date Type</th>
                      <th class="">Date</th>
                      <th class="">Status</th>
                      <th class="text-right">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php $__currentLoopData = $get_record; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_records): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>               
                    <tr>
                      <td class="text-left" style="padding-left: 18px;">
                        <label class="option block mn">
                          <input type="checkbox" name="check[]" class="check" value="<?php echo e($get_records->cdate_id); ?>">
                          <span class="checkbox mn"></span>
                        </label>
                      </td>
                      <td class="" style="padding-left:20px">
                        <?php
                          $sub_client = App\Model\Case_Registration\Case_Registration::where(['reg_id' => $get_records->cdate_case_no ])->first();
                        ?>
                        <?php echo e($sub_client->reg_case_number); ?>

                      </td>
                      <td class="text-left" style="padding-left:20px">
                        <?php
                          $sub_client = App\Model\Case_Registration\Case_Registration::where(['reg_id' => $get_records->cdate_case_no ])->first();
                        ?>
                        <?php echo e($sub_client->reg_petitioner); ?> v/s
                        <?php echo e($sub_client->reg_respondent); ?>

                      </td>
                      <td class="text-left" style="padding-left:20px">
                        <?php if($get_records->cdate_type == 2): ?>
                               Filled (<?php echo e(date('d M Y',strtotime($get_records->cdate_pic))); ?>)
                        <?php elseif($get_records->cdate_type == 1): ?>
                               Pending
                        <?php endif; ?>
                      </td>
                      <td class="text-left" style="padding-left:20px"><?php echo e($get_records->cdate_date); ?></td>
                      <td class="text-left" style="padding-left:20px">
                        <?php if($get_records->cstatus_date == 2): ?>
                               Filled
                        <?php elseif($get_records->cstatus_date == 1): ?>
                               Pending
                        <?php endif; ?>
                      </td>
                      <td class="text-right">
                        <div class="btn-group text-left">
                          <button type="button" class="btn <?php echo e($get_records->cdate_status   == 1 ? 'btn-success' : 'btn-danger'); ?>  br2 btn-xs fs12 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> <?php echo e($get_records->cdate_status  == 1 ? 'Active' : 'Deactive'); ?>

                            <span class="caret ml5"></span>
                          </button>
                          <ul class="dropdown-menu" role="menu" style="min-width:125px;">
                            <li>
                              <a href="<?php echo e(url('/advocate-panel/add-date')); ?>/<?php echo e(sha1($get_records->cdate_id)); ?>">Edit</a>
                            </li>
                            <div class="divider"></div>                            
                            <li class="<?php echo e($get_records->cdate_status   == 1 ? 'active' : ''); ?>">
                              <a href="<?php echo e(url('/advocate-panel/change-date-status')); ?>/<?php echo e($get_records->cdate_id); ?>/1">Active</a>
                            </li>
                            <li class=" <?php echo e($get_records->cdate_status  == 0 ? 'active' : ''); ?> ">
                              <a href="<?php echo e(url('/advocate-panel/change-date-status')); ?>/<?php echo e($get_records->cdate_id); ?>/0">Deactive</a>
                            </li>
                          </ul>
                        </div>
                      </td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                 
                  </tbody>
                </table>
              </div>
              <?php echo Form::close(); ?>

          </div>
          <div class="panel-body pn">
            <div class="table-responsive">
              <table class="table admin-form theme-warning tc-checkbox-1 fs13">                                
                <tbody>
                  <tr class="">
                     <th class="text-left">
                      <button type="button" class="btn btn-primary" onclick="go_delete()"><i class="glyphicon glyphicon-trash"></i> Delete Multiple </button>
                    </th>
                    <th>
                      <?php echo e($get_record->links()); ?>

                    </th>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


<style type="text/css">
.dt-panelfooter{
  display: none !important;
}
</style>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('advocate_admin/layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>