<?php $__env->startSection('content'); ?>

  <!-- Start: Content-Wrapper -->
  <section id="content_wrapper">
    <header id="topbar" >
      <div class="topbar-left">
        <ol class="breadcrumb">
          <li class="crumb-active">
            <a href="<?php echo e(url('/advocate-panel/view-calendar')); ?>">Calendar</a>
          </li>
          <li class="crumb-icon">
            <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">
              <span class="glyphicon glyphicon-home"></span>
            </a>
          </li>
          <li class="crumb-link">
            <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">Home</a>
          </li>
          <li class="crumb-trail"> Calendar </li>
        </ol>
      </div>
    </header>

    <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">

        <!-- begin: .tray-left -->
        <aside class="tray tray-left tray290" data-tray-mobile="#content > .tray-center">


          <!-- Demo HTML - Via JS we insert a cloned fullcalendar title -->
          <div class="fc-title-clone"></div>

          <div class="section admin-form theme-primary">
            <div class="inline-mp minimal-mp center-block"></div>
          </div>

          <div class="admin-form theme-primary popup-basic popup-lg mfp-with-anim" id="">
            <div class="panel">
              <!-- <div class="panel-heading">
                <span class="panel-title" style="font-size: 20px;">
                  <i class="fa fa-pencil-square"></i>Add Note
                </span>
              </div> -->
              
              <?php echo Form::open(['name'=>'form_add_question','url'=>'advocate-panel/insert-note','id'=>'form_add_question' ,'autocomplete'=>'off']); ?>


                <div class="panel-body p25">
                  <div class="section-divider mt10 mb40">
                    <span>Add Note</span>
                  </div>

                  <div class="section section_from_new">
                    <label class="field prepend-icon" id="discount_date_from_lb">
                      <?php echo Form::text('further_date',$get_record->reg_nxt_further_date,array('class' => 'gui-input fromdate','placeholder' => 'Select Date','id'=>'datefuther' )); ?>

                      <label for="comment" class="field-icon">
                        <i class="fa fa-calendar"></i>
                      </label>
                    </label>
                  </div>

                  <div class="section">
                    <label class="field prepend-icon">
                      <textarea class="gui-textarea" id="comment" name="comment" placeholder="Note Description"></textarea>
                      <label for="comment" class="field-icon">
                        <i class="fa fa-comments"></i>
                      </label>
                    </label>
                  </div>

                  
                </div>



                <div class="panel-footer text-right">
                  <button type="submit" class="button btn-primary"> Add Note</button>
                </div>
              <?php echo Form::close(); ?>

            </div>
          </div>

        </aside>
        <!-- end: .tray-left -->

        <!-- begin: .tray-center -->
        <div class="tray tray-center">

          <!-- Calendar -->
          <div id='calendar' class="admin-theme"></div>

        </div>
        <!-- end: .tray-center -->

      </section>
      <!-- End: Content -->

  </section>





<style type="text/css">
.dt-panelfooter{
  display: none !important;
}

#content-footer.affix{
  display: none;
}
.fc-more-popover{
  /*top: auto !important;*/
  /*bottom: 0px !important; */
}
</style>
<script type="text/javascript">
  
  $(document).ready(function() {

    $('#calendar').fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay,listWeek'
      },
    //  defaultDate: '2018-03-12',
      navLinks: true, // can click day/week names to navigate views
      editable: false,
      eventLimit: true, // allow "more" link when too many events
      events: [
        <?php $__currentLoopData = $progress_case_high; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $progress_case_highs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            {
              title: "<?php echo e($progress_case_highs->count_pessi); ?> Case in High Court",
              start: "<?php echo e($progress_case_highs->pessi_further_date); ?>",
              url: "view-peshi/<?php echo e($progress_case_highs->count_pessi); ?>/<?php echo e($progress_case_highs->pessi_further_date); ?>/2"
            },   
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


        <?php $__currentLoopData = $progress_case_trail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $progress_case_trails): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            {
              title: "<?php echo e($progress_case_trails->count_pessi); ?> Case in Trail Court",
              start: "<?php echo e($progress_case_trails->pessi_further_date); ?>",
              url: "view-peshi/<?php echo e($progress_case_trails->count_pessi); ?>/<?php echo e($progress_case_trails->pessi_further_date); ?>/1"
            },   
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        <?php $__currentLoopData = $notes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $notes_all): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            {
              title: "<?php echo e($notes_all->notes_description); ?>",
              start: "<?php echo e($notes_all->notes_date); ?>",
            },   
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        <?php $__currentLoopData = $holiday; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $holidays): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            {
              title: "<?php echo e($holidays->holiday_title); ?>",
              start: "<?php echo e($holidays->holiday_date); ?>",
              backgroundColor: '#70ca63 !important',
              textColor: 'white'
            },   
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        
      ],


      eventRender: function(event, element) {
        // create event tooltip using bootstrap tooltips
        $(element).attr("data-original-title", event.title);
        $(element).tooltip({
          container: 'body',
          delay: {
            "show": 100,
            "hide": 200
          }
        });
        // create a tooltip auto close timer  
        $(element).on('show.bs.tooltip', function() {
          var autoClose = setTimeout(function() {
            $('.tooltip').fadeOut();
          }, 3500);
        });
      }
    });

  });
</script>
<style type="text/css">
  .fc-bg tr td:first-child{
    background: #ff2e003d !important;
    color: white !important;
  }

  .fc-content-skeleton tr td:first-child{
    color: white !important;
    font-weight: bold;
  }

  .fc-day-grid div:nth-child(2) .fc-bg tr td:last-child{
    background: #ff2e003d !important;
    color: white !important;
  }

  .fc-day-grid div:nth-child(4) .fc-bg tr td:last-child{
    background: #ff2e003d !important;
    color: white !important;
  }

  .fc-day-grid div:nth-child(2) .fc-content-skeleton tr td:last-child{
    color: white !important;
    font-weight: bold;
  }

  .fc-day-grid div:nth-child(4) .fc-content-skeleton tr td:last-child{
    color: white !important;
    font-weight: bold;
  }

  
</style>

<script type="text/javascript">

  jQuery(document).ready(function() {
  
    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "This field contains alphabets only");

    jQuery.validator.addMethod("checkemail", function(value, element) 
    {
    return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
    }, "Enter a VALID email address"); 

    $.validator.addMethod('minStrict', function (value, el, param) {
     return value > param; 
    });
    $.validator.addMethod('maxStrict', function (value, el, param) {
     return value < param; 
    });


    /* @custom  validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#form_add_question").validate({

      /* @validation  states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation  rules 
      ------------------------------------------ */

      rules: {
        // further_date: {
        //   required: true
        // },
        // comment: {
        //   required: true
        // }
      },

      /* @validation  error messages 
      ---------------------------------------------- */

      messages: {
        comment: {
          required: 'Please Enter Notes Description'
        },
        further_date: {
          required: 'Please Select Date'
        }
      },

      /* @validation  highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });

  setInterval(function(){ 
    var dt_val = $("#datefuther").val();
    if (dt_val!="") {
        $("#discount_date_from_lb").addClass("state-success");
        $("#discount_date_from_lb").removeClass("state-error");
        $(".section_from_new .state-error").html("");
    }
  }, 500);

  </script>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('advocate_admin/layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>