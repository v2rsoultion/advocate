<?php $__env->startSection('content'); ?>

<style type="text/css">
  .admin-form .select, .admin-form .gui-input, .admin-form .select > select, .admin-form .select-multiple select{
    height: 28px !important;
  }
  .admin-form a.button, .admin-form span.button, .admin-form label.button
  {
    line-height: 28px !important;
  }
  .admin-form .append-icon .field-icon, .admin-form .prepend-icon .field-icon{
    line-height: 28px !important;
  }
/*  .admin-form .gui-textarea {
    line-height: 7px !important;
  }*/
/*  .gui-textarea{
    height: 100px !important;
  }*/
/* .admin-form .gui-input {
  padding: 5px;
 }*/
 /*.admin-form .prepend-icon .field-icon {
  top: 6px;
 }*/
 .client_result{
  position: absolute; max-height: 280px; overflow: auto; width: 96%; z-index: 99999999999;
 }
 .admin-form .button{
    height: 28px !important;
    line-height: 1px !important;
  }
 .btn {
    height: 29px !important;
    line-height: 1px !important;  }

 .down{
  margin-top: 70px;
 }
.account_setting {
  margin: 0px 20px !important;
  margin-top: 25px !important;
  /*padding: 5px 0px !important;*/
 }
 .select-text {
    padding: 0px 12px !important;
}
.admin-form .select .arrow{
  top: 0px;
}
</style>

  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left down">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="<?php echo e(url('/advocate-panel/view-client')); ?>">View Client</a>
            </li>
            <li class="crumb-icon">
              <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">Home</a>
            </li>
            <li class="crumb-trail">Add Client</li>
          </ol>
        </div>
      </header>

       <div class="row">
        <div class="col-md-12">
          <?php if(\Session::has('success')): ?>
          <div class="alert alert-success account_setting" >
            <?php echo \Session::get('success'); ?>

          </div>
          <?php endif; ?>

          <?php if(\Session::has('failure')): ?>
          <div class="alert alert-danger account_setting" >
            <?php echo \Session::get('failure'); ?>

          </div>
          <?php endif; ?>
        </div>
      </div>

      <!-- Begin: Content -->

      <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center">
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-heading">
                <span class="panel-title"> Add Client </span>
              </div>
              <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                <!-- IF any error found -->
                <div class="col-md-12">
                <?php if($errors->any()): ?>
                  <div id="log_error" class="alert alert-danger" style='font-family: josefin_sansregular !important;'>
                  <?php echo e($errors->first()); ?>  </i></div>
                <?php endif; ?>
                </div>
                <!-- IF any error found -->
                  <div id="tab1_1" class="tab-pane active">
                    <?php echo Form::open(['name'=>'form_add_question','url'=>'advocate-panel/insert-client/'.$get_record[0]->cl_id,'id'=>'form_add_question' ,'autocomplete'=>'off']); ?>

                    
                    <div class="">
                      <!-- <div class="">
                        <span class="sub_group"> Group </span>
                      </div>                                
                      <div class="divider"></div> -->
                      
                      <div class="col-md-12">
                        <div class="section" style="margin-bottom: 40px;">
                        
                          <div class="col-md-2">
                            <label class="option" style="font-size:12px;">

                              <?php if($get_record[0]->client_type != ""): ?>
                                <?php echo Form::radio('client_type','1',$get_record[0]->cl_group_type == 1 ? 'checked' : '', array( 'id' => 'check1' , 'onclick' => 'hide_sub_group()' )); ?>

                              <?php else: ?> 
                                <?php echo Form::radio('client_type','1',checked, array('id' => 'check1' , 'onclick' => 'hide_sub_group()' )); ?>

                              <?php endif; ?>
                              <span class="radio"></span> Individual
                            </label>
                          </div>

                          <div class="col-md-2">
                            <label class="option" style="font-size:12px;">
                              <?php echo Form::radio('client_type','2',$get_record[0]->cl_group_type == 2 ? 'checked' : '', array( 'id' => 'check2' , 'onclick' => 'show_sub_group()' )); ?>

                              <span class="radio"></span> Organisation
                            </label>
                          </div>
                        </div>

                        <!-- <input type="hidden" name="client_type" value="<?php echo e($get_record[0]->cl_group_type); ?>" id="client_type"> -->

                        <div class="section"></div>
                        <div class="clearfix"></div>
                      </div>


                      <div class="col-md-3">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;" >Name :  </label>  
                          <label for="level_name" class="field prepend-icon">
                            <?php echo Form::text('name',$get_record[0]->cl_group_name,array('class' => 'gui-input cl_group_name_search','id' => 'cl_group_name' , 'autocomplete' => 'off' )); ?>

                              <label for="Account Mobile" class="field-icon">
                              <i class="fa fa-pencil"></i>
                            </label>                            
                          </label>
                          <ul class="list-group client_result" id="client_result" style="position: absolute; max-height: 280px; overflow: auto; width: 96%; z-index: 99999999999;"></ul>
                        </div>
                      </div>

                      <div class="col-md-2">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;" >Choose Prefix : </label>
                            <label for="artist_state" class="field select">
                              <select class="form-control select-text" id="" name="cl_name_prefix">
                                <option value="">Select</option>
                                <option value="S/O" <?php echo e($get_record[0]->cl_name_prefix == "S/O" ? 'selected="selected"' : ''); ?> >S/O</option>
                                <option value="F/O" <?php echo e($get_record[0]->cl_name_prefix == "F/O" ? 'selected="selected"' : ''); ?> >F/O</option>
                                <option value="D/O" <?php echo e($get_record[0]->cl_name_prefix == "D/O" ? 'selected="selected"' : ''); ?> >D/O</option>
                                <option value="C/O" <?php echo e($get_record[0]->cl_name_prefix == "C/O" ? 'selected="selected"' : ''); ?>>C/O</option>
                                <option value="W/O" <?php echo e($get_record[0]->cl_name_prefix == "W/O" ? 'selected="selected"' : ''); ?> >W/O</option>
                                <option value="Thr/O" <?php echo e($get_record[0]->cl_name_prefix == "Thr/O" ? 'selected="selected"' : ''); ?> >Thr/O</option>                           
                              </select>
                              <i class="arrow"></i>
                            </label>
                        </div>
                      </div>


                      <div class="col-md-3">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;" > Guardian Name :  </label>  
                          <label for="level_name" class="field prepend-icon">
                            <?php echo Form::text('father_name',$get_record[0]->cl_father_name,array('class' => 'gui-input','id' => 'cl_group_name' , 'autocomplete' => 'off' )); ?>

                              <label for="Account Mobile" class="field-icon">
                              <i class="fa fa-pencil"></i>
                            </label>                            
                          </label>
                        </div>
                      </div>

                      <div class="col-md-4">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;" >Email Id :  </label>  
                          <label for="level_name" class="field prepend-icon">
                            <?php echo Form::text('email',$get_record[0]->cl_group_email_id,array('class' => 'gui-input','placeholder' => '' )); ?>

                              <label for="Account Mobile" class="field-icon">
                              <i class="fa fa-pencil"></i>
                            </label>                           
                          </label>
                        </div>
                      </div>

                      <div class="clearfix"></div>
                      
                       <div class="col-md-6">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;" >Mobile No :  </label>  
                          <label for="level_name" class="field prepend-icon">
                            <?php echo Form::text('mobile',$get_record[0]->cl_group_mobile_no,array('class' => 'gui-input numeric','placeholder' => '' )); ?>

                              <label for="Account Mobile" class="field-icon">
                              <i class="fa fa-pencil"></i>
                            </label>                           
                          </label>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;" >Place :  </label>  
                          <label for="level_name" class="field prepend-icon">
                            <?php echo Form::text('place',$get_record[0]->cl_group_place,array('class' => 'gui-input','placeholder' => '' )); ?>

                              <label for="Account Mobile" class="field-icon">
                              <i class="fa fa-pencil"></i>
                            </label>                           
                          </label>
                        </div>
                      </div>

                    </div>

                      <div class=""> 
                      <div class="col-md-12">
                        <div class="section" id="textasrea">
                          <label for="level_name" class="field-label" style="font-weight:600;" >Address :  </label>  
                          <label for="level_name" class="field prepend-icon">
                            <?php echo Form::textarea('address',$get_record[0]->cl_group_address,array('class' => 'gui-textarea','placeholder' => '' )); ?>

                            <label for="Account Mobile" class="field-icon">
                              <i class="fa fa-pencil"></i>
                            </label>                           
                          </label>
                        </div>
                        </div>
                     </div>

                  <!-- show group -->
                  <div class="clearfix"></div>
                
                <div id="hide_dynamic_sub_group">
                <?php if($get_record[0]->cl_group_type == 2): ?>  
                  <?php
                  $counter = 0;
                    $sub_client =  \App\Model\Sub_Client\Sub_Client::where(['client_id' => $get_record[0]->cl_id ])->get();
                  ?>           

                  <div class="divider"></div>
                      <div class="">
                      <span class="sub_group"> Sub Group </span>
                      </div>
                      
                  <?php $__currentLoopData = $sub_client; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub_clients): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php $counter++; ?>


                    <div class="" > 
                                
                      <div class="divider"></div>

                      <div class="questionrow" id="remove_questiondiv_<?php echo e($sub_clients->sub_client_id); ?>">
                        <div class="questiondiv" id="questiondiv">     
                          <div class="col-md-3">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Name :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              
                              <?php echo Form::hidden('sub_fild['.$counter.'][sub_id]',$sub_clients->sub_client_id,array('class' => 'gui-input','placeholder' => '' )); ?>


                              <?php echo Form::text('sub_fild['.$counter.'][sub_name]',$sub_clients->sub_client_name,array('class' => 'gui-input cl_group_name_search','placeholder' => '' )); ?>

                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-pencil"></i>
                              </label>                          
                            </label>
                            <ul class="list-group client_result" id="client_result" style="position: absolute; max-height: 280px; overflow: auto; width: 96%; z-index: 99999999999;"></ul>
                          </div>
                          </div>

                          <div class="col-md-2">
                            <div class="section">
                              <label for="level_name" class="field-label" style="font-weight:600;" >Choose Prefix : </label>
                                <label for="artist_state" class="field select">
                                  <select class="form-control select-text" id="" name="sub_fild[<?php echo e($counter); ?>][sub_name_prefix]">
                                    <option value="">Select</option>
                                    <option value="S/O" <?php echo e($sub_clients->sub_name_prefix == "S/O" ? 'selected="selected"' : ''); ?> >S/O</option>
                                    <option value="F/O" <?php echo e($sub_clients->sub_name_prefix == "F/O" ? 'selected="selected"' : ''); ?> >F/O</option>
                                    <option value="D/O" <?php echo e($sub_clients->sub_name_prefix == "D/O" ? 'selected="selected"' : ''); ?> >D/O</option>
                                    <option value="C/O" <?php echo e($sub_clients->sub_name_prefix == "C/O" ? 'selected="selected"' : ''); ?>>C/O</option>
                                    <option value="W/O" <?php echo e($sub_clients->sub_name_prefix == "W/O" ? 'selected="selected"' : ''); ?> >W/O</option>
                                    <option value="Thr/O" <?php echo e($sub_clients->sub_name_prefix == "Thr/O" ? 'selected="selected"' : ''); ?> >Thr/O</option>                           
                                  </select>
                                  <i class="arrow"></i>
                                </label>
                            </div>
                          </div>

                          <div class="col-md-3">
                            <div class="section">
                              <label for="level_name" class="field-label" style="font-weight:600;" > Guardian Name :  </label>  
                              <label for="level_name" class="field prepend-icon">
                                <?php echo Form::text('sub_fild['.$counter.'][sub_guardian_name]',$sub_clients->sub_guardian_name,array('class' => 'gui-input','placeholder' => '' )); ?>

                                  <label for="Account Mobile" class="field-icon">
                                  <i class="fa fa-pencil"></i>
                                </label>                            
                              </label>
                            </div>
                          </div>

                          <div class="col-md-4">
                            <div class="section">
                              <label for="level_name" class="field-label" style="font-weight:600;" >Email Id :  </label>
                              <label for="level_name" class="field prepend-icon">
                                <?php echo Form::text('sub_fild['.$counter.'][sub_email]',$sub_clients->sub_client_email_id,array('class' => 'gui-input','placeholder' => '' )); ?>

                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-pencil"></i>
                              </label>                           
                            </label>
                          </div>
                        </div>
                        <div class="col-md-6">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;" >Mobile No :  </label>  
                          <label for="level_name" class="field prepend-icon">
                            <?php echo Form::text('sub_fild['.$counter.'][sub_mobile]',$sub_clients->sub_client_mobile_no,array('class' => 'gui-input numeric','placeholder' => '' )); ?>

                              <label for="Account Mobile" class="field-icon">
                              <i class="fa fa-pencil"></i>
                            </label>                           
                          </label>
                        </div>
                        </div>

                        <div class="col-md-6">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;" >Place :  </label>  
                          <label for="level_name" class="field prepend-icon">
                            <?php echo Form::text('sub_fild['.$counter.'][sub_place]',$sub_clients->sub_client_place,array('class' => 'gui-input','placeholder' => '' )); ?>

                              <label for="Account Mobile" class="field-icon">
                              <i class="fa fa-pencil"></i>
                            </label>                           
                          </label>
                        </div>
                        </div>

                        <div class="col-md-12">
                          <div class="section" id="textasrea">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Address :  </label>
                            <label for="level_name" class="field prepend-icon">
                              <?php echo Form::textarea('sub_fild['.$counter.'][sub_address]',$sub_clients->sub_client_address,array('class' => 'gui-textarea','placeholder' => '' )); ?>

                              <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-pencil"></i>
                              </label>
                            </label>
                          </div>
                        </div>

                        <div class="section row mb6" id="hide_button" >
                            <div class="">
                              <div class="col-md-12"> 
                                <button type="button" onclick="delete_group('<?php echo e($sub_clients->sub_client_id); ?>');" name="add" id="" class="button btn-danger pull-right mr15"> <i class="fa fa-trash-o" aria-hidden="true"></i> &nbsp; Delete Sub Group </button>
                              </div>
                            </div>
                        </div>

                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              <?php endif; ?>
              </div>
                
                  <div class="clearfix"></div>
                  
                  <?php if($edit_id != ""): ?>
                  <div id="sub_hide">
                    <div class="questionrow" id="questionrow">
                        <div class="questiondiv" id="questiondiv"> </div>
                    </div>
                  </div>
                  <?php endif; ?>
                  
                  <div class="section row mb6" id="hide_button" style="<?php echo e($get_record[0]->cl_group_type  == 2 ? '' : 'display: none;'); ?>" >
                      <div class="">
                        <div class="col-md-12"> 
                          <button type="button" onclick="addRecords();" name="add" id="add" class="button btn-primary pull-right mr15"> <i class="fa fa-plus" aria-hidden="true"></i> &nbsp; Add Fields </button>
                        </div>
                      </div>
                  </div>
                  <div class="clearfix"></div>
                  <div id="show_sub_group" style="display: none;">
                  <!-- <?php if($edit_id ==""): ?> -->
                  <div class="">
                      <div class="divider"></div>
                      <div class="">
                      <span class="sub_group"> Sub Group </span>
                      </div>                                
                      <div class="divider"></div>
                      <div class="questionrow" id="questionrow">
                        <div class="questiondiv" id="questiondiv">     
                          <div class="col-md-3">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Name :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              <?php echo Form::text('sub_fild[1][sub_name]',$get_record[0]->sub_client_name,array('class' => 'gui-input commen_text cl_group_name_search','placeholder' => '' )); ?>

                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-pencil"></i>
                              </label>                          
                            </label>
                            <ul class="list-group client_result" id="client_result" style="position: absolute; max-height: 280px; overflow: auto; width: 96%; z-index: 99999999999;"></ul>
                          </div>
                          </div>

                          <div class="col-md-2">
                            <div class="section">
                              <label for="level_name" class="field-label" style="font-weight:600;" >Choose Prefix : </label>
                                <label for="artist_state" class="field select">
                                  <select class="form-control select-text" id="" name="sub_fild[1][sub_name_prefix]">
                                    <option value="">Select</option>
                                    <option value="S/O" <?php echo e($sub_clients->sub_name_prefix == "S/O" ? 'selected="selected"' : ''); ?> >S/O</option>
                                    <option value="F/O" <?php echo e($sub_clients->sub_name_prefix == "F/O" ? 'selected="selected"' : ''); ?> >F/O</option>
                                    <option value="D/O" <?php echo e($sub_clients->sub_name_prefix == "D/O" ? 'selected="selected"' : ''); ?> >D/O</option>
                                    <option value="C/O" <?php echo e($sub_clients->sub_name_prefix == "C/O" ? 'selected="selected"' : ''); ?>>C/O</option>
                                    <option value="W/O" <?php echo e($sub_clients->sub_name_prefix == "W/O" ? 'selected="selected"' : ''); ?> >W/O</option>
                                    <option value="Thr/O" <?php echo e($sub_clients->sub_name_prefix == "Thr/O" ? 'selected="selected"' : ''); ?> >Thr/O</option>                           
                                  </select>
                                  <i class="arrow"></i>
                                </label>
                            </div>
                          </div>

                          <div class="col-md-3">
                            <div class="section">
                              <label for="level_name" class="field-label" style="font-weight:600;" > Guardian Name :  </label>  
                              <label for="level_name" class="field prepend-icon">
                                <?php echo Form::text('sub_fild[1][sub_guardian_name]',$sub_clients->sub_guardian_name,array('class' => 'gui-input','placeholder' => '' )); ?>

                                  <label for="Account Mobile" class="field-icon">
                                  <i class="fa fa-pencil"></i>
                                </label>                            
                              </label>
                            </div>
                          </div>

                          <div class="col-md-4">
                            <div class="section">
                              <label for="level_name" class="field-label" style="font-weight:600;" >Email Id :  </label>
                              <label for="level_name" class="field prepend-icon">
                                <?php echo Form::text('sub_fild[1][sub_email]',$get_record[0]->sub_client_email_id,array('class' => 'gui-input commen_text','placeholder' => '' )); ?>

                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-pencil"></i>
                              </label>                           
                            </label>
                          </div>
                        </div>
                        <div class="col-md-6">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;" >Mobile No :  </label>  
                          <label for="level_name" class="field prepend-icon">
                            <?php echo Form::text('sub_fild[1][sub_mobile]',$get_record[0]->sub_client_mobile_no,array('class' => 'gui-input numeric commen_text','placeholder' => '' )); ?>

                              <label for="Account Mobile" class="field-icon">
                              <i class="fa fa-pencil"></i>
                            </label>                           
                          </label>
                        </div>
                        </div>

                        <div class="col-md-6">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;" > Place :  </label>  
                          <label for="level_name" class="field prepend-icon">
                            <?php echo Form::text('sub_fild[1][sub_place]',$get_record[0]->sub_client_place,array('class' => 'gui-input commen_text','placeholder' => '' )); ?>

                              <label for="Account Mobile" class="field-icon">
                              <i class="fa fa-pencil"></i>
                            </label>                           
                          </label>
                        </div>
                        </div>

                        <div class="col-md-12">
                          <div class="section" id="textasrea">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Address :  </label>
                            <label for="level_name" class="field prepend-icon">
                              <?php echo Form::textarea('sub_fild[1][sub_address]',$get_record[0]->sub_client_address,array('class' => 'gui-textarea commen_text','placeholder' => '' )); ?>

                              <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-pencil"></i>
                              </label>
                            </label>
                          </div>
                        </div>
                        <div class="clearfix"></div>         
                      </div>
                    </div>
                  </div>
                  <!-- <?php endif; ?> -->
                  <div class="section row mb6">
                    <div class="">
                      <div class="col-md-12"> 
                        <button type="button" onclick="addRecords();" name="add" id="add" class="button btn-primary pull-right mr15"> <i class="fa fa-plus" aria-hidden="true"></i> &nbsp; Add Fields </button>
                      </div>
                    </div>
                  </div>
                </div>
                  <input type="hidden" name="countid1" id="countid1" value="<?php echo e(count($sub_client + 1)); ?>"> <br>
                  <input type="hidden" name="countid2" id="countid2" value="1">
              
                  <div class="panel-footer text-right">
                    <?php echo Form::submit('Save', array('class' => 'button btn-primary', 'id' => 'maskedKey')); ?>

                    <?php echo Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')); ?>

                  </div>
                  <?php echo Form::close(); ?>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


<script type="text/javascript">

  jQuery(document).ready(function() {
  
    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "This field contains alphabets only");

    jQuery.validator.addMethod("checkemail", function(value, element) 
    {
    return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
    }, "Enter a VALID email address"); 

    $.validator.addMethod('minStrict', function (value, el, param) {
     return value > param; 
    });
    $.validator.addMethod('maxStrict', function (value, el, param) {
     return value < param; 
    });


    /* @custom  validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#form_add_question").validate({

      /* @validation  states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation  rules 
      ------------------------------------------ */

      rules: {
        // cl_name_prefix: {
        //   required: true
        // },
        // email: {
        //   required: true,
        //   checkemail: true
        // },
        // address: {
        //   required: true
        // },
        // place: {
        //   required: true
        // },
        // mobile: {
        //   required: true,
        //   minlength: 10,
        //   maxlength: 12,
        // },
      },

      /* @validation  error messages 
      ---------------------------------------------- */

      messages: {
        cl_name_prefix: {
          required: 'Please select prefix'
        },
        email: {
          required: 'Please Fill Required Email Id'
        },
        address: {
          required: 'Please Fill Required Address'
        },
        place: {
          required: 'Please Fill Required Place'
        },
        mobile: {
          required: 'Please Fill Required Mobile No',
          minlength:'Please enter valid mobile number',
          maxlength:'Please enter valid mobile number',
        },        
      },

      /* @validation  highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });

  function addRecords(){

    //document.getElementById("sub_hide").style.display = "block";
    // document.getElementById("show_sub_group").style.display = "block";
    // document.getElementById("hide_button").style.display = "none";
    
    var count1 = $('#countid1').val();
    var count2 = $('#countid2').val();

    var counter1 = parseInt(count1) + 1;
    var counter2 = parseInt(count2) + 1;
    $('#countid1').val(counter1);
    $('#countid2').val(counter2);


    if(count2 >= 0){
      $('#questionrow').append('<div class="questiondiv" id="questiondiv'+counter1+'"><div class="divider"></div><div class="col-md-3"><div class="section"><label for="level_name" class="field-label" style="font-weight:600;" >Name : </label><label for="level_name" class="field prepend-icon"><input type="text" name="sub_fild['+counter1+'][sub_name]" value="" class="gui-input commen_text cl_group_name_search" placeholder=""><label for="Account Mobile" class="field-icon"><i class="fa fa-pencil"></i></label></label><ul class="list-group client_result" id="client_result"></ul></div></div><div class="col-md-2"><div class="section"><label for="level_name" class="field-label" style="font-weight:600;" >Choose Prefix : </label><label for="artist_state" class="field select"><select class="form-control select-text" id="" name="sub_fild['+counter1+'][sub_name_prefix]"><option value="">Select</option><option value="S/O">S/O</option><option value="F/O">F/O</option><option value="D/O">D/O</option><option value="C/O">C/O</option><option value="W/O">W/O</option><option value="Thr/O">Thr/O</option></select><i class="arrow"></i></label></div></div><div class="col-md-3"><div class="section"><label for="level_name" class="field-label" style="font-weight:600;" > Guardian Name :  </label><label for="level_name" class="field prepend-icon"><input type="text" name="sub_fild['+counter1+'][sub_guardian_name]" value="" class="gui-input commen_text" placeholder=""><label for="Account Mobile" class="field-icon"><i class="fa fa-pencil"></i></label></label></div></div><div class="col-md-4"><div class="section"><label for="level_name" class="field-label" style="font-weight:600;" >Email Id : </label><label for="level_name" class="field prepend-icon"><input type="text" name="sub_fild['+counter1+'][sub_email]" value="" class="gui-input commen_text" placeholder=""><label for="Account Mobile" class="field-icon"><i class="fa fa-pencil"></i></label></label></div></div><div class="col-md-6"><div class="section"><label for="level_name" class="field-label" style="font-weight:600;" >Mobile : </label><label for="level_name" class="field prepend-icon"><input type="text" name="sub_fild['+counter1+'][sub_mobile]" value="" class="gui-input commen_text" placeholder=""><label for="Account Mobile" class="field-icon"><i class="fa fa-pencil"></i></label></label></div></div><div class="col-md-6"><div class="section"><label for="level_name" class="field-label" style="font-weight:600;" >Place : </label><label for="level_name" class="field prepend-icon"><input type="text" name="sub_fild['+counter1+'][sub_place]" value="" class="gui-input commen_text" placeholder=""><label for="Account Mobile" class="field-icon"><i class="fa fa-pencil"></i></label></label></div></div><div class="col-md-12"><div class="section" id="textasrea"><label for="level_name" class="field-label" style="font-weight:600;" >Address : </label><label for="level_name" class="field prepend-icon"><textarea type="textarea" name="sub_fild['+counter1+'][sub_address]" value="" class="gui-textarea" placeholder=""></textarea><label for="Account Mobile" class="field-icon"><i class="fa fa-pencil"></i></label></label></div></div> <div class="section row mb5 customClasss"><div class=""> <button type="button" onclick="removeRecords('+counter1+');" name="add" id="remove" class="button btn-danger pull-right mr30"> <i class="fa fa-trash-o" aria-hidden="true"></i> &nbsp; Remove Fields </button></div></div></div></div>');
       // $('#countid').val(count);
    }
  }

  function removeRecords(countnew){
    $( "#questiondiv"+countnew+"" ).remove();
    var count2 = $('#countid2').val();
    var counter2 = parseInt(count2) - 1;
    $('#countid2').val(counter2);
  }

  function hide_sub_group(){
    $(".commen_text").val('');
    $("#client_type").val('1');
    document.getElementById("show_sub_group").style.display = "none";
    document.getElementById("hide_dynamic_sub_group").style.display = "none";
    document.getElementById("hide_button").style.display = "none";
    document.getElementById("sub_hide").style.display = "none";
  }

  function show_sub_group(){
    $("#client_type").val('2');
    document.getElementById("show_sub_group").style.display = "block";
    document.getElementById("hide_dynamic_sub_group").style.display = "block";
    document.getElementById("hide_button").style.display = "none";
    document.getElementById("sub_hide").style.display = "block";
  }


  function delete_group(sub_client_id) {
      
      //alert(client_id);
      if(confirm("Are you sure to want delete sub group?")) {
          
        BASE_URL = '<?php echo e(url('/')); ?>';
        $.ajax({
          url:BASE_URL+"/advocate-panel/delete-sub-group/"+sub_client_id,
          success: function(result){
            $( "#remove_questiondiv_"+sub_client_id+"" ).remove(); 
          }
        });

      }
      
  }


  $(document).ready(function(){
    $.ajaxSetup({ cache: false });
    $('.cl_group_name_search').keyup(function(){
        var searchField = this.value;
        var n=$(this);

        n.parent().next().html('');

        if (document.getElementById('check1').checked) {
          client_type = document.getElementById('check1').value;
        }

        if (document.getElementById('check2').checked) {
          client_type = document.getElementById('check2').value;
        }
         
        var expression = new RegExp(searchField, "i");

        $.ajax({
            url: BASE_URL+"/get-all-clients?client_name="+searchField+'&client_type='+client_type,       
            success: function(result) {
              n.parent().next().html(result);
            }
        });
    });
    $('.client_result').on('click', 'li', function() {
        var click_text  = $(this).text();
            // var name_1      = click_text[0];
            // var name_2      = click_text[1];
            // var name_3      = name_2.substr(0,3);
            //$('#cl_group_name').val($.trim(click_text));
        $(".client_result").html('');
    });
  });



  </script>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('advocate_admin/layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>