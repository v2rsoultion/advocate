<?php $__env->startSection('content'); ?>

  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="<?php echo e(url('/advocate-panel/view-pessi')); ?>">View Pessi</a>
            </li>
            <li class="crumb-icon">
              <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">Home</a>
            </li>
            <li class="crumb-trail">Add Pessi</li>
          </ol>
        </div>
      </header>

      <!-- Begin: Content -->

      <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center">
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-heading">
                <span class="panel-title">Add Pessi</span>
              </div>
              <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                <!-- IF any error found -->
                <div class="col-md-12">
                <?php if($errors->any()): ?>
                  <div id="log_error" class="alert alert-danger" style='font-family: josefin_sansregular !important;'>
                  <?php echo e($errors->first()); ?>  </i></div>
                <?php endif; ?>
                </div>
                <!-- IF any error found -->
                  <div id="tab1_1" class="tab-pane active">
                    
                    <?php echo Form::open(['name'=>'form_add_question','url'=>'advocate-panel/insert-pessi/'.$get_record->reg_id,'id'=>'form_add_question','files'=>'ture']); ?>


                    <div class="row">
                        
                        <div class="col-md-4">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Case No : </label>
                              <label for="artist_state" class="field">
                                <select class="select2-single form-control" id="case_no" name="case_no" onchange="get_detail(this.value)">
                                  <option value='0'>Select Case No.</option>    
                                  <?php $__currentLoopData = $get_case_regestered; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_case_regestereds): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($get_case_regestereds->reg_id); ?>" > <?php echo e($get_case_regestereds->reg_case_number); ?></option>
                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                           
                                </select>
                                <i class="arrow double"></i>
                              </label>
                              <div id="descrption_error" class="" ></div>
                          </div>
                        </div>

                        <div class="col-md-4">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" > File No : </label>
                              <label for="artist_state" class="field">
                                <select class="select2-single form-control" id="file_no" name="file_no" onchange="get_detail(this.value)">
                                  <option value='0'>File No.</option>    
                                  <?php $__currentLoopData = $get_case_regestered; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_case_regestereds): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($get_case_regestereds->reg_id); ?>" > <?php echo e($get_case_regestereds->reg_file_no); ?></option>
                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                           
                                </select>
                                <i class="arrow double"></i>
                              </label>
                              <div id="descrption_error_file" class="" ></div>
                          </div>
                        </div>


                        <div class="col-md-4">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" > NCV : </label>
                              <label for="artist_state" class="field">
                                <select class="select2-single form-control" id="ncv_no" name="ncv_no" onchange="get_detail(this.value)">
                                  <option value='0'>Select NCV</option>    
                                  <?php $__currentLoopData = $get_case_regestered; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_case_regestereds): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($get_case_regestereds->reg_id); ?>" > <?php echo e($get_case_regestereds->reg_vcn_number); ?></option>
                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                           
                                </select>
                                <i class="arrow double"></i>
                              </label>
                              <div id="descrption_error_ncv" class="" ></div>
                          </div>
                        </div>

<!--                         <div class="clearfix"></div>
                        <div class="col-md-6"> </div>
                        <div class="clearfix"></div> -->

                        <div class="col-md-3">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Type of Case : </label>
                            <label for="artist_state" class="field select">
                              <?php echo Form::text('case_type',$get_record->case_type,array('class' => 'gui-input','id' => 'case_type' , disabled )); ?>

                            </label>
                          </div>
                        </div>
                        
                        <div class="col-md-4">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;"> Title of Case : </label>
                              <label for="artist_state" class="field select">
                                <?php echo Form::text('case_title',$get_record->case_title,array('class' => 'gui-input','id' => 'case_title' , disabled )); ?>

                              </label>
                          </div>
                        </div>

                        <div class="col-md-2">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;"> Previous Date :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              <?php echo Form::text('previous_date',$get_record->reg_nxt_further_date,array('class' => 'gui-input','placeholder' => '','id'=>'previous_date' , readonly )); ?>

                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-calendar"></i>
                              </label>                           
                            </label>
                          </div>
                        </div>

                        <div class="col-md-3">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;"> Select Stage : </label>
                              <label for="artist_state" class="field select">
                                <select class="form-control" name="reg_stage" id="reg_stage">
                                  <option value=''>Select Stage</option>
                                    <?php $__currentLoopData = $stage; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $stages): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                      <option value="<?php echo e($stages->stage_id); ?>" <?php echo e($stages->stage_id == $get_record->reg_stage_id ? 'selected="selected"' : ''); ?> ><?php echo e($stages->stage_name); ?> </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                
                                </select>
                                <i class="arrow double"></i>
                              </label>
                          </div>
                        </div>

                        <div class="col-md-12">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;"> Order Sheet :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              <?php echo Form::textarea('order_sheet',$get_record->order_sheet,array('class' => 'gui-textarea accc1','placeholder' => '','id'=>'' )); ?>

                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-comment"></i>
                              </label                           
                            </label>
                          </div>
                        </div>

                        <div class="col-md-12">
                          <div class="section" style="margin-bottom: 40px;">
                            <div class="col-md-3">
                              <label class="option block mn" style="font-size:12px;">
                                <?php echo Form::radio('choose_type','0',$get_record['user_gender'] == 0 ? 'checked' : '', array('onclick' => 'show_further()', 'class' => 'check' )); ?>

                                <span class="checkbox mn"></span> Further Date
                              </label>
                            </div>

                            <div class="col-md-3">
                              <label class="option block mn" style="font-size:12px;">
                                <?php echo Form::radio('choose_type','1',$get_record['user_gender'] == 1 ? 'checked' : '', array('onclick' => 'show_decide()', 'class' => 'check' )); ?>

                                <span class="checkbox mn"></span> Decide
                              </label>
                            </div>

                            <div class="col-md-3">
                              <label class="option block mn" style="font-size:12px;">
                                <?php echo Form::radio('choose_type','2',$get_record['user_gender'] == 2 ? 'checked' : '', array('onclick' => 'show_due_course()', 'class' => 'check' )); ?>

                                <span class="checkbox mn"></span> Due Course
                              </label>
                            </div>
                          </div>

                          <div class="section"></div>
                          <div class="clearfix"></div>
                        </div>



                        

                        <div class="col-md-4" style="display:;" id="show_further_date" >
                          <div class="section section_from">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Next Further Date :  </label>  
                            <label for="level_name" class="field prepend-icon" id="discount_date_from_lb">
                              <?php echo Form::text('further_date',$get_record->reg_nxt_further_date,array('class' => 'gui-input','placeholder' => '','id'=>'datefuther' )); ?>

                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-calendar"></i>
                              </label                           
                            </label>
                          </div>
                        </div>

                        <div class="col-md-4" style="display: none;" id="show_decide">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;"> Decide : </label>
                              <label for="artist_state" class="field select">
                                <select class="form-control" id="decide" name="decide" >
                                  <option value=''>Select Decide </option>
                                  <option value='1'>Case in fever </option>                               
                                  <option value='2'>Case in against </option>
                                  <option value='3'> Withdraw </option>
                                  <option value='4'> None </option>
                                </select>
                                <i class="arrow double"></i>
                              </label>
                          </div>
                        </div>

                        <div class="col-md-2" style="display: none;" id="show_due_course">
                          <button type="button" class="btn btn-success br2 btn-xs field select form-control" style="margin-top: 23px; margin-bottom: 22px;" > Due Course </button>
                        </div>
                      
                     </div>

                  <div class="panel-footer text-right">
                      <?php echo Form::submit('Save', array('class' => 'button btn-primary mysave', 'id' => 'maskedKey')); ?>

                      <!-- <?php echo Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')); ?> -->
                  </div>   
                    <?php echo Form::close(); ?>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>


<script type="text/javascript">


  function show_further(){
    document.getElementById("show_further_date").style.display = "block";
    document.getElementById("show_decide").style.display = "none";
    document.getElementById("show_due_course").style.display = "none";
  }


  function show_decide(){
    document.getElementById("show_further_date").style.display = "none";
    document.getElementById("show_decide").style.display = "block";
    document.getElementById("show_due_course").style.display = "none";
  }


  function show_due_course(){
    document.getElementById("show_further_date").style.display = "none";
    document.getElementById("show_decide").style.display = "none";
  //  document.getElementById("show_due_course").style.display = "block";
  }


  function get_detail(reg_id){

    if(reg_id != 0){

      $('#descrption_error').html('');
      $('#descrption_error_file').html('');
      $('#descrption_error_ncv').html('');
      $(".select2-selection").css({"border": "1px solid #A5D491"});
      $(".select2-selection").css({"background": "#F0FEE9"});

      BASE_URL = '<?php echo e(url('/')); ?>';
      $.ajax({
        url:BASE_URL+"/advocate-panel/get-case-registeration/"+reg_id,
        success: function(result){
            $("#case_type").val(result.case_name);
            $("#case_title").val(result.reg_respondent+' v/s '+result.reg_petitioner); 
            $("#previous_date").val(result.previous_date);
            $("#reg_stage").val(result.pessi_statge_id);
            $("#case_no").val(result.reg_case_no);
            $("#file_no").val(result.reg_file_no);
            $("#ncv_no").val(result.reg_vcn_number);
        }
      });

    }
    
  }

</script>

<script type="text/javascript">

  

  jQuery(document).ready(function() {

    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "This field contains alphabets only");

    jQuery.validator.addMethod("checkemail", function(value, element) 
    {
    return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
    }, "Enter a VALID email address"); 

    $.validator.addMethod('minStrict', function (value, el, param) {
     return value > param; 
    });
    $.validator.addMethod('maxStrict', function (value, el, param) {
     return value < param; 
    });


    /* @custom  validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#form_add_question").validate({

      /* @validation  states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation  rules 
      ------------------------------------------ */

      rules: {
        case_no: {
          required: true
        },
        reg_stage: {
          required: true
        },
        order_sheet: {
          required: true
        },
        choose_type: {
          required: true
        },
        further_date: {
          required: true
        },
        decide: {
          required: true
        }
      },
      /* @validation  error messages 
      ---------------------------------------------- */

      messages: {
        reg_stage: {
          required: 'Please select stage'
        },
        order_sheet: {
          required: 'Please enter order sheet'
        },
        type: {
          required: 'Please Fill Required Type'
        },
        further_date: {
          required: 'Please select further date'
        },
        decide: {
          required: 'Please select decide'
        },
      },

      /* @validation  highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });


  $(document).on('click','.mysave',function(){

    var case_no = $("#case_no").val();

//     alert(case_no);
    
    if(case_no == 0){
      $('#descrption_error').html('<div class="state-error"></div><em for="services_excerpt" class="state-error">Please select case no.</em>');
      $('#descrption_error_file').html('<div class="state-error"></div><em for="services_excerpt" class="state-error">Please select file no.</em>');
      $('#descrption_error_ncv').html('<div class="state-error"></div><em for="services_excerpt" class="state-error">Please select NCV</em>');
      $(".select2-selection").css({"border": "1px solid #de888a"});
      $(".select2-selection").css({"background": "#fee9ea"});
      return false;
    }
  })



  setInterval(function(){ 
    var dt_val = $("#datefuther").val();
    if (dt_val!="") {
        $("#discount_date_from_lb").addClass("state-success");
        $("#discount_date_from_lb").removeClass("state-error");
        $(".section_from .state-error").html("");
    }
  }, 500);


  </script>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('advocate_admin/layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>