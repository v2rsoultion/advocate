<?php if(count($sub_client) != 0): ?>
<div class="section">
    <label for="level_name" class="field-label" style="font-weight:600;" > Sub Client : </label>
      <label for="artist_state" class="field">
        <select id="framework_2" name="sub_client[]" multiple class="form-control" >
            <?php $__currentLoopData = $sub_client; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub_clients): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <option value="<?php echo e($sub_clients->sub_client_id); ?>"> <?php echo e($sub_clients->sub_client_name); ?> <?php if($sub_clients->sub_guardian_name != ""): ?> <?php echo e($sub_clients->sub_name_prefix); ?> <?php echo e($sub_clients->sub_guardian_name); ?> <?php endif; ?>  <?php if($sub_clients->sub_client_mobile_no != ""): ?> (<?php echo e($sub_clients->sub_client_mobile_no); ?>) <?php endif; ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </select>
        <i class="arrow double"></i>
      </label>
  </div>
<?php endif; ?>


<script type="text/javascript">
	

	$('#framework_2').multiselect({
      nonSelectedText: 'Select Sub Client',
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
      buttonWidth:'400px'
     });
	
</script>