<?php $__env->startSection('content'); ?>

<style type="text/css">
  .admin-form .select, .admin-form .gui-input, .admin-form .select > select, .admin-form .select-multiple select{
    height: 28px !important;
  }
  .admin-form a.button, .admin-form span.button, .admin-form label.button
  {
    line-height: 28px !important;
  }
  .admin-form .append-icon .field-icon, .admin-form .prepend-icon .field-icon{
    line-height: 28px !important;
  }
  .admin-form .gui-textarea {
    line-height: 7px !important;
  }
  .select2-container .select2-selection--single {
    height: 28px !important;
  }
  .select2-container--default .select2-selection--single .select2-selection__rendered {
    line-height: 24px !important;
  }
  .select2-container .select2-selection--multiple {
    min-height: 30px !important;
  }
  .select2-container--default .select2-selection--single .select2-selection__arrow {
    top: -4px !important;
  }
/*  .gui-textarea{
    height: 100px !important;
  }*/
/* .admin-form .gui-input {
  padding: 5px;
 }*/
 /*.admin-form .prepend-icon .field-icon {
  top: 6px;
 }*/
 .form-control {
    height: 28px !important;
    /*padding: 0px 12px !important;*/
  }
 .admin-form .button{
    height: 28px !important;
    line-height: 1px !important;
  }
 .btn {
    height: 29px !important;
    line-height: 1px !important;  }

 .down{
  margin-top: 70px;
 }
.account_setting {
  margin: 0px 20px !important;
  margin-top: 25px !important;
  /*padding: 5px 0px !important;*/
 }
 .select-text{
  padding: 0px 12px;
 }

   .multiselect-container li a label{
    border: 0px !important;
    background: none !important;
  }
  .multiselect-container {
    margin-top: 5px !important;
  }

  .btn.multiselect .caret{
    display: none;
  }
  .multiselect{
    text-align: left;
  }
  .input-group-addon {
    min-width: 30px;
    padding: 5px 6px;
  }
  .multiselect-search{
    height: 29px !important;
  }

</style>
  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left down">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="#"> Reporting </a>
            </li>
            <li class="crumb-icon">
              <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">Home</a>
            </li>
            <li class="crumb-trail"> Reporting </li>
          </ol>
        </div>
      </header>

      <!-- Begin: Content -->

      <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center">
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-heading">
                <span class="panel-title"> Reporting </span>
              </div>
              <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                <!-- IF any error found -->
                <div class="col-md-12">
                <?php if($errors->any()): ?>
                  <div id="log_error" class="alert alert-danger" style='font-family: josefin_sansregular !important;'>
                  <?php echo e($errors->first()); ?>  </i></div>
                <?php endif; ?>
                </div>
                <!-- IF any error found -->
                  <div id="tab1_1" class="tab-pane active">
                    <?php echo Form::open(['name'=>'form_add_question','url'=>'advocate-panel/filter-reporting' ,'id'=>'form_add_question','files'=>'true' ,'autocomplete'=>'off']); ?>

                    <div class="row">
                      
                      <div class="col-md-12">
                        <div class="section" style="margin-bottom: 40px;">
                          <label for="level_name" class="field-label" style="font-weight:600;" > Category :  </label>  
                          <div class="col-md-2">
                            <label class="option" style="font-size:12px;">
                                <?php echo Form::radio('case_category','1','', array( 'class' => 'check' , 'onclick' => 'change_case(this.value)' , 'class' => 'check' )); ?>

                              <span class="radio"></span> Criminal
                            </label>
                          </div>

                          <div class="col-md-2">
                            <label class="option" style="font-size:12px;">
                                <?php echo Form::radio('case_category','2','', array( 'class' => 'check' ,'onclick' => 'change_case(this.value)' )); ?>

                              <span class="radio"></span> Civil
                            </label>
                          </div>

                          <div class="col-md-2">
                            <label class="option" style="font-size:12px;">
                                <?php echo Form::radio('case_category','0','', array( 'class' => 'check' ,'onclick' => 'change_case(this.value)' )); ?>

                              <span class="radio"></span> Both
                            </label>
                          </div>
                        </div>
                        <div class="section"></div>
                        <div class="clearfix"></div>
                      </div>

                        
                        <div class="col-md-3">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;"> Type of Case : </label>
                            <label for="artist_state" class="field">
                              <!-- <select class="form-control" id="choose_case_type" name="choose_case_type" onchange="get_type_of_case(this.value)"> -->
                              <select class="select2-single form-control"  id="choose_case_type" name="choose_case_type[]" onchange="get_type_of_case(this.value)" multiple> 
                                <!-- <option value=''>Select Type Of Case</option> -->
                                <?php $__currentLoopData = $case_type_entry; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $case_type_entrys): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                   <option value="<?php echo e($case_type_entrys->case_id); ?>"><?php echo e($case_type_entrys->case_name); ?> </option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                              </select>
                              <i class="arrow double"></i>
                            </label>
                          </div>
                        </div>

                        <div class="col-md-4">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Case No : </label>
                              <label for="artist_state" class="field">
                                <select class="select2-single form-control" id="case_no" name="case_no">
                                  <option value='0'>Select Case No.</option>    
                                    <?php $__currentLoopData = $get_case_regestered; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_case_regestereds): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                      <option value="<?php echo e($get_case_regestereds->reg_id); ?>"> <?php echo e($get_case_regestereds->reg_case_number); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                                <i class="arrow double"></i>
                              </label>
                          </div>
                        </div>

                        <div class="col-md-4">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" > File No : </label>
                              <label for="artist_state" class="field">
                                <select class="select2-single form-control" id="file_no" name="file_no">
                                  <option value='0'>Select File No.</option>    
                                    <?php $__currentLoopData = $get_case_regestered; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_case_regestereds): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                      <option value="<?php echo e($get_case_regestereds->reg_id); ?>"> <?php echo e($get_case_regestereds->reg_file_no); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                                <i class="arrow double"></i>
                              </label>
                          </div>
                        </div>

                        <div class="clearfix"></div>

                        <div class="col-md-6">
                          <div class="section" style="margin-bottom: 40px;">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Status :  </label>  
                            
                            <div class="col-md-4">
                              <label class="option" style="font-size:12px;">
                                  <?php echo Form::radio('case_status','','', array( 'class' => 'check' , 'class' => 'check' )); ?>

                                <span class="radio"></span> All
                              </label>
                            </div>

                            <div class="col-md-4">
                              <label class="option" style="font-size:12px;">
                                  <?php echo Form::radio('case_status','1','', array( 'class' => 'check' , 'class' => 'check' )); ?>

                                <span class="radio"></span> Pending
                              </label>
                            </div>

                            <div class="col-md-4">
                              <label class="option" style="font-size:12px;">
                                  <?php echo Form::radio('case_status','2','', array( 'class' => 'check'  )); ?>

                                <span class="radio"></span> Disposal
                              </label>
                            </div>
                          </div>
                          <div class="section"></div>
                          <div class="clearfix"></div>
                        </div>

                        <div class="col-md-6">
                          <div class="section" style="margin-bottom: 40px;">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Type of Court :  </label>  
                            <div class="col-md-5">
                              <label class="option" style="font-size:12px;">
                                  <?php echo Form::radio('court_type','1','', array( 'class' => 'check' , 'class' => 'check', 'onclick' => 'change_court_type(this.value)' )); ?>

                                <span class="radio"></span> Trial Court
                              </label>
                            </div>

                            <div class="col-md-5">
                              <label class="option" style="font-size:12px;">
                                  <?php echo Form::radio('court_type','2','', array( 'class' => 'check' , 'onclick' => 'change_court_type(this.value)'  )); ?>

                                <span class="radio"></span> High Court
                              </label>
                            </div>
                          </div>
                          <div class="section"></div>
                          <div class="clearfix"></div>
                        </div>

                        <div class="col-md-12">
                          <div class="section" style="margin-bottom: 40px;">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Power :  </label>  
                            <div class="col-md-2">
                              <label class="option" style="font-size:12px;">
                                <?php echo Form::radio('power','','', array( 'class' => 'check' , 'class' => 'check' )); ?>

                                <span class="radio"></span> All
                              </label>
                            </div>

                            <div class="col-md-2">
                              <label class="option" style="font-size:12px;">
                                <?php echo Form::radio('power','1','', array( 'class' => 'check' , 'class' => 'check' )); ?>

                                <span class="radio"></span> Petitioner
                              </label>
                            </div>

                            <div class="col-md-2">
                              <label class="option" style="font-size:12px;">
                                <?php echo Form::radio('power','2','', array( 'class' => 'check'  )); ?>

                                <span class="radio"></span> Respondent
                              </label>
                            </div>

                            <div class="col-md-2">
                              <label class="option" style="font-size:12px;">
                                <?php echo Form::radio('power','3','', array( 'class' => 'check'  )); ?>

                                <span class="radio"></span> Caveat
                              </label>
                            </div>

                            <div class="col-md-2">
                              <label class="option" style="font-size:12px;">
                                <?php echo Form::radio('power','4','', array( 'class' => 'check'  )); ?>

                                <span class="radio"></span> None
                              </label>
                            </div>

                          </div>
                          <div class="section"></div>
                          <div class="clearfix"></div>
                        </div>

                        <div class="col-md-3">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Court Name : </label>
                              <!-- <label for="artist_state" class="field select"> -->
                                <label for="level" class="field prepend-icon"> 
                                <select class="form-control select-text" name="court_name" id="court_name">
                                  <option value=''>Select Court</option>

                                    <?php $__currentLoopData = $get_court; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_courts): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                       <option value="<?php echo e($get_courts->court_id); ?>"><?php echo e($get_courts->court_name); ?> </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                </select>
                                <i class="arrow double"></i>
                              </label>
                          </div>
                        </div>


                        <div class="col-md-3">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Stage : </label>
                              <!-- <label for="artist_state" class="field select"> -->
                                <label for="level" class="field prepend-icon"> 
                                <select class="form-control select-text" name="reg_stage">
                                  <option value=''>Select Stage</option>
                                    <?php $__currentLoopData = $stage; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $stages): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                      <option value="<?php echo e($stages->stage_id); ?>"><?php echo e($stages->stage_name); ?> </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                
                                </select>
                                <i class="arrow double"></i>
                              </label>
                          </div>
                        </div>
                        
                        <!-- <div class="col-md-12">
                          <div class="section" style="margin-bottom: 40px;">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Power :  </label>  
                            <div class="col-md-2">
                              <label class="option" style="font-size:12px;">
                                <?php echo Form::radio('power','','', array( 'class' => 'check' , 'class' => 'check' )); ?>

                                <span class="radio"></span> All
                              </label>
                            </div>

                            <div class="col-md-2">
                              <label class="option" style="font-size:12px;">
                                <?php echo Form::radio('power','1','', array( 'class' => 'check' , 'class' => 'check' )); ?>

                                <span class="radio"></span> Petitioner
                              </label>
                            </div>

                            <div class="col-md-2">
                              <label class="option" style="font-size:12px;">
                                <?php echo Form::radio('power','2','', array( 'class' => 'check'  )); ?>

                                <span class="radio"></span> Respondent
                              </label>
                            </div>

                            <div class="col-md-2">
                              <label class="option" style="font-size:12px;">
                                <?php echo Form::radio('power','3','', array( 'class' => 'check'  )); ?>

                                <span class="radio"></span> Caveat
                              </label>
                            </div>

                            <div class="col-md-2">
                              <label class="option" style="font-size:12px;">
                                <?php echo Form::radio('power','4','', array( 'class' => 'check'  )); ?>

                                <span class="radio"></span> None
                              </label>
                            </div>

                          </div>
                          <div class="section"></div>
                          <div class="clearfix"></div>
                        </div> -->
                        
                        <!-- <div class="clearfix"></div> -->
 
                        <div class="col-md-3">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Assigned : </label>
                              <!-- <label for="artist_state" class="field select"> -->
                                <label for="level" class="field prepend-icon"> 
                                <select class="form-control select-text" id="" name="assigned">
                                  <option value=''> Select Assigned</option>
                                  
                                   <?php $__currentLoopData = $assigned; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $assigneds): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                     <option value="<?php echo e($assigneds->assign_id); ?>"><?php echo e($assigneds->assign_advocate_name); ?> </option>
                                   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                   
                                </select>
                                <i class="arrow double"></i>
                              </label>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Act : </label>
                              <!-- <label for="artist_state" class="field select"> -->
                                <label for="level" class="field prepend-icon"> 
                                <select class="form-control select-text" name="act" onchange="act_type_change_ajax(this.value)">
                                  <option value=''> Select Act</option>
                                  <?php $__currentLoopData = $act; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $acts): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                     <option value="<?php echo e($acts->act_id); ?>"><?php echo e($acts->act_name); ?> </option>
                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                  
                                </select>
                                <i class="arrow double"></i>
                              </label>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Section : </label>

                              <label for="artist_state" class="field" style="height: 28px;" id="show_act_sub_type_section">
                                <select id="framework_3" name="section[]" multiple class="form-control">
                                    <?php $__currentLoopData = $section; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sections): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 

                                    <?php
                                      $arr = explode(',', $get_record[0]->reg_section_id);
                                      $sel1 = '';
                                      if ( in_array( $sections->section_id, $arr) ) {
                                        $sel1 = 'selected="selected"';  
                                      } 
                                    ?>

                                      <option value="<?php echo e($sections->section_id); ?>" <?php echo e($sel1); ?> ><?php echo e($sections->section_name); ?> </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
                                </select>
                                <i class="arrow double"></i>
                            </label>
                          </div>
                        </div>
                        

                        <div class="col-md-3">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Referred By : </label>
                              <!-- <label for="artist_state" class="field select"> -->
                                <label for="level" class="field prepend-icon"> 
                                <select class="form-control select-text" id="" name="reffered">
                                  <option value=''>Select Referred By</option>
                                  <?php $__currentLoopData = $reffered; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $reffereds): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($reffereds->ref_id); ?>"><?php echo e($reffereds->ref_advocate_name); ?> </option>
                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                 
                                </select>
                                <i class="arrow double"></i>
                              </label>
                          </div>
                        </div>

                        <div class="col-md-3">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Place : </label>
                              <!-- <label for="artist_state" class="field select"> -->
                                <label for="level" class="field prepend-icon"> 
                                <select class="form-control select-text" name="client_place" id="client_place">
                                  <option value=''> Select Place</option>
                                  <?php $__currentLoopData = $place; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $places): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                     <option value="<?php echo e($places->cl_group_place); ?>"><?php echo e($places->cl_group_place); ?> </option>
                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                 
                                </select>
                                <i class="arrow double"></i>
                              </label>
                          </div>
                        </div>

                        <div class="col-md-12">
                          <div class="section" style="margin-bottom: 40px;">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Group Type :  </label>  
                            <div class="col-md-2">
                              <label class="option" style="font-size:12px;">
                                  <?php echo Form::radio('group_type','1','', array( 'class' => 'check' , 'class' => 'check','onclick' => 'change_group(this.value)' )); ?>

                                <span class="radio"></span> Individual
                              </label>
                            </div>

                            <div class="col-md-2">
                              <label class="option" style="font-size:12px;">
                                  <?php echo Form::radio('group_type','2','', array( 'class' => 'check' ,'onclick' => 'change_group(this.value)' )); ?>

                                <span class="radio"></span> Organisation
                              </label>
                            </div>
                          </div>
                          <div class="section"></div>
                          <div class="clearfix"></div>
                        </div>

                        <div class="col-md-3">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Client Group/Clients : </label>
                              <!-- <label for="artist_state" class="field select"> -->
                                <label for="level" class="field prepend-icon"> 
                                <select class="form-control select-text" name="client_group" id="client_group" onchange="sub_group_change_ajax(this.value)">
                                  <option value=''> Select Client Group/Clients </option>
                                  <?php $__currentLoopData = $client; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $clients): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                     <option value="<?php echo e($clients->cl_id); ?>"><?php echo e($clients->cl_group_name); ?> <?php if($clients->cl_father_name != ""): ?> <?php echo e($clients->cl_name_prefix); ?> <?php echo e($clients->cl_father_name); ?> <?php endif; ?> </option>
                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                 
                                </select>
                                <i class="arrow double"></i>
                              </label>
                          </div>
                        </div>
                        
                        
                        <div class="col-md-3" id="show_client_sub_group" style="display: none;">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Client Sub Group : </label>
                              <!-- <label for="artist_state" class="field select"> -->
                                <label for="level" class="field prepend-icon"> 
                                <select class="form-control select-text" id="sub_group_change" name="client_sub_group">
                                  <option value=''>Select Client Sub Group</option>  
                                  <?php $__currentLoopData = $sub_client; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub_clients): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                     <option value="<?php echo e($sub_clients->sub_client_id); ?>"><?php echo e($sub_clients->sub_client_name); ?> </option>
                                   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                              
                                </select>
                                <i class="arrow double"></i>
                              </label>
                          </div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="col-md-3">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Peshi Date (Next date) From :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              <?php echo Form::text('next_date_from','',array('class' => 'gui-input fromdate_certified','placeholder' => '','id'=>'next_date_from' )); ?>

                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-calendar"></i>
                              </label                           
                            </label>
                          </div>
                        </div>

                        <div class="col-md-3">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Peshi Date (Next date) To :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              <?php echo Form::text('next_date_to', '' ,array('class' => 'gui-input todate_certified','placeholder' => '','id'=>'next_date_to' )); ?>

                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-calendar"></i>
                              </label                           
                            </label>
                          </div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="col-md-3">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Registration From :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              <?php echo Form::text('reg_date_from','',array('class' => 'gui-input fromdate_notice','placeholder' => '','id'=>'reg_date_from' )); ?>

                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-calendar"></i>
                              </label                           
                            </label>
                          </div>
                        </div>

                        <div class="col-md-3">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Registration To :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              <?php echo Form::text('reg_date_to', '' ,array('class' => 'gui-input todate_notice','placeholder' => '','id'=>'reg_date_to' )); ?>

                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-calendar"></i>
                              </label                           
                            </label>
                          </div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="col-md-3">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Undated From :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              <?php echo Form::text('disposal_from','',array('class' => 'gui-input disposal_from','placeholder' => '','id'=>'disposal_from' )); ?>

                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-calendar"></i>
                              </label                           
                            </label>
                          </div>
                        </div>

                        <div class="col-md-3">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Undated To :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              <?php echo Form::text('disposal_to','',array('class' => 'gui-input disposal_to','placeholder' => '','id'=>'disposal_to' )); ?>

                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-calendar"></i>
                              </label                           
                            </label>
                          </div>
                        </div>
                        <div class="clearfix"></div>


                        <div class="col-md-3">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Decided From :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              <?php echo Form::text('from_date','',array('class' => 'gui-input fromdate_notice','placeholder' => '','id'=>'from_date' )); ?>

                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-calendar"></i>
                              </label                           
                            </label>
                          </div>
                        </div>

                        <div class="col-md-3">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Decided To :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              <?php echo Form::text('to_date','',array('class' => 'gui-input todate_notice','placeholder' => '','id'=>'to_date' )); ?>

                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-calendar"></i>
                              </label                           
                            </label>
                          </div>
                        </div>
                        <div class="clearfix"></div>
                        
                        <div class="col-md-3">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Reporting Heading :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              <?php echo Form::text('reporting_heading','',array('class' => 'gui-input','placeholder' => '','id'=>'reporting_heading' )); ?>

                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-pencil"></i>
                              </label                           
                            </label>
                          </div>
                        </div>

                     </div>

                      

                  <div class="panel-footer text-right">
                      <?php echo Form::submit('Save', array('class' => 'button btn-primary', 'id' => 'maskedKey_submit')); ?>

                      <?php echo Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')); ?>

                  </div>   
                    <?php echo Form::close(); ?>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>


<style type="text/css">

.divider{
  display: none;
}
#form_add_question .field-icon {
    margin-top: 0px !important;
}

</style>

<script type="text/javascript">

   jQuery(document).ready(function() {

    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "This field contains alphabets only");

    jQuery.validator.addMethod("checkemail", function(value, element) 
    {
    return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
    }, "Enter a VALID email address"); 

    $.validator.addMethod('minStrict', function (value, el, param) {
     return value > param; 
    });
    $.validator.addMethod('maxStrict', function (value, el, param) {
     return value < param; 
    });

    jQuery.validator.addMethod("checkdate", function(value, element) 
    {
    return this.optional(element) || /^([0]?[1-9]|[1|2][0-9]|[3][0|1])[-]([0]?[1-9]|[1][0-2])[-]([0-9]{4}|[0-9]{4})$/.test(value);
    }, "Enter date in dd-mm-yyyy format");




    jQuery.validator.addMethod("greaterThan_next", function(value, element, params) {
      if(value != ""){
        if (!/Invalid|NaN/.test(value)) {
        value=value.split("-");
        var newDate_notice =value[1]+"/"+value[0]+"/"+value[2];
        var notice_status_date = new Date(newDate_notice).getTime();
        var notice_issue_date = $('#next_date_from').val();
        notice_issue_date=notice_issue_date.split("-");
        var newDate_notice_issue =notice_issue_date[1]+"/"+notice_issue_date[0]+"/"+notice_issue_date[2];
        var notice_issue_date = new Date(newDate_notice_issue).getTime();
        return notice_status_date >= notice_issue_date;     
        }
      }
      return isNaN(notice_status_date) && isNaN($(params).val()) 
      || (Number(notice_status_date) > Number($(params).val())); 
    },'To date must be greater than from date.');


    jQuery.validator.addMethod("greaterThan_reg", function(value, element, params) {
      if(value != ""){
        if (!/Invalid|NaN/.test(value)) {
        value=value.split("-");
        var newDate_notice =value[1]+"/"+value[0]+"/"+value[2];
        var notice_status_date = new Date(newDate_notice).getTime();
        var notice_issue_date = $('#reg_date_from').val();
        notice_issue_date=notice_issue_date.split("-");
        var newDate_notice_issue =notice_issue_date[1]+"/"+notice_issue_date[0]+"/"+notice_issue_date[2];
        var notice_issue_date = new Date(newDate_notice_issue).getTime();
        return notice_status_date >= notice_issue_date;     
        }
      }
      return isNaN(notice_status_date) && isNaN($(params).val()) 
      || (Number(notice_status_date) > Number($(params).val())); 
    },'To date must be greater than from date.');


    jQuery.validator.addMethod("greaterThan_disposal", function(value, element, params) {
      if(value != ""){
        if (!/Invalid|NaN/.test(value)) {
        value=value.split("-");
        var newDate_notice =value[1]+"/"+value[0]+"/"+value[2];
        var notice_status_date = new Date(newDate_notice).getTime();
        var notice_issue_date = $('#disposal_from').val();
        notice_issue_date=notice_issue_date.split("-");
        var newDate_notice_issue =notice_issue_date[1]+"/"+notice_issue_date[0]+"/"+notice_issue_date[2];
        var notice_issue_date = new Date(newDate_notice_issue).getTime();
        return notice_status_date >= notice_issue_date;     
        }
      }
      return isNaN(notice_status_date) && isNaN($(params).val()) 
      || (Number(notice_status_date) > Number($(params).val())); 
    },'To date must be greater than from date.');


    jQuery.validator.addMethod("greaterThan_todate", function(value, element, params) {
      if(value != ""){
        if (!/Invalid|NaN/.test(value)) {
        value=value.split("-");
        var newDate_notice =value[1]+"/"+value[0]+"/"+value[2];
        var notice_status_date = new Date(newDate_notice).getTime();
        var notice_issue_date = $('#from_date').val();
        notice_issue_date=notice_issue_date.split("-");
        var newDate_notice_issue =notice_issue_date[1]+"/"+notice_issue_date[0]+"/"+notice_issue_date[2];
        var notice_issue_date = new Date(newDate_notice_issue).getTime();
        return notice_status_date >= notice_issue_date;     
        }
      }
      return isNaN(notice_status_date) && isNaN($(params).val()) 
      || (Number(notice_status_date) > Number($(params).val())); 
    },'To date must be greater than from date.');





    /* @custom  validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#form_add_question").validate({

      /* @validation  states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation  rules 
      ------------------------------------------ */

      rules: {
        next_date_from: {
          checkdate: true,
        },
        next_date_to: {
          checkdate: true,
          greaterThan_next: true,
        },
        reg_date_from: {
          checkdate: true,
        },
        reg_date_to: {
          checkdate: true,
          greaterThan_reg: true,
        },
        disposal_from: {
          checkdate: true,
        },
        disposal_to: {
          checkdate: true,
          greaterThan_disposal: true,
        },
        from_date: {
          checkdate: true,
        },
        to_date: {
          checkdate: true,
          greaterThan_todate: true,
        }
      },
      /* @validation  error messages 
      ---------------------------------------------- */
      messages: {
      },

      /* @validation  highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });

  // case category

  function change_case(compliance_category){

    BASE_URL = '<?php echo e(url('/')); ?>';
      $.ajax({
        url:BASE_URL+"/advocate-panel/get-type-case/"+compliance_category,
        success: function(result){
            $("#choose_case_type").html(result);
        }
      });
  }

  // type of case

  function get_type_of_case(){

    var compliance_category = $("#choose_case_type").val();

    BASE_URL = '<?php echo e(url('/')); ?>';
      $.ajax({
        url:BASE_URL+"/advocate-panel/get-case-registeration-category/"+compliance_category,
        success: function(result){
            $("#case_no").html(result);
        }
      });

      $.ajax({
        url:BASE_URL+"/advocate-panel/get-case-registeration-file/"+compliance_category,
        success: function(result){
            $("#file_no").html(result);
        }
      });
  }

  // COUrt category

  function change_court_type(court_type){

    BASE_URL = '<?php echo e(url('/')); ?>';
      $.ajax({
        url:BASE_URL+"/advocate-panel/court-type/"+court_type,
        success: function(result){
            $("#court_name").html(result);
        }
      });
  }


  // change_group

  function change_group(group_type){

    if(group_type == 1){
      document.getElementById("show_client_sub_group").style.display = "none";
    } else {
      document.getElementById("show_client_sub_group").style.display = "block"; 
    }

    BASE_URL = '<?php echo e(url('/')); ?>';
      $.ajax({
        url:BASE_URL+"/advocate-panel/change-group/"+group_type,
        success: function(result){
            $("#client_group").html(result);
        }
      });
  }


  

  // court_type_trail

  function court_type_trail() {
    
    document.getElementById("hide_show_class_code").style.display = "none";
    document.getElementById("hide_show_ncv").style.display = "block";

  }

  // court_type_high

  function court_type_high() {
    
    document.getElementById("hide_show_ncv").style.display = "none";
    document.getElementById("hide_show_class_code").style.display = "block";

  }

  // choose_sub_type

  function choose_sub_type(client_id){

    if (client_id != ''){
      BASE_URL = '<?php echo e(url('/')); ?>';
      $.ajax({
        url:BASE_URL+"/advocate-panel/change-client-sub-type-ajax/"+client_id,
        success: function(result){
            $("#hide_choose_sub_type").html(result);
        }
      });

      
      $.ajax({
        url:BASE_URL+"/advocate-panel/get-case-category/"+client_id,
        success: function(result){
            if(result.status == true){
              $("#reg_case_type_category").val(1);
              document.getElementById("fir_show").style.display = "block";
            } else {
              $("#reg_case_type_category").val(2);
              document.getElementById("fir_show").style.display = "none";
            }
        }
      });
    }
  }

  // check case no

  function check_case_no(case_number){

  //  alert(case_number);

    BASE_URL = '<?php echo e(url('/')); ?>';
    var reg_case_type_category = $("#reg_case_type_category").val();
    var case_type = $("#case_type").val();
    
    $.ajax({
      url:BASE_URL+"/advocate-panel/check-case-number/"+case_number+"/"+case_type+"/"+reg_case_type_category,
      success: function(result){
        if(result.status == true){
          $("#case_number_error").html('<div class="state-error"></div><em for="services_excerpt" class="state-error"> This case number already exits </em>');
          $(".case_number_error").removeClass("state-success");
          $(".case_number_error").addClass("state-error");
          $('#maskedKey_submit').prop('disabled', true);
          return false;
          
        } else {
          $('#maskedKey_submit').prop('disabled', false);
          $(".case_number_error").removeClass("state-error");
          $(".case_number_error").addClass("state-success");
          $("#case_number_error").html("");
        }
      }
    });
  }

// function type_change_ajax_id(type_id){

//   if (type_id != ''){
//     BASE_URL = '<?php echo e(url('/')); ?>';
//     $.ajax({
//       url:BASE_URL+"/advocate-panel/change-type-fir-change/"+type_id,
//       success: function(result){
//           $("#type_change_ajax").html(result); 
//       }
//     });
//   }

// }


function act_type_change_ajax(act_type_id){

  if (act_type_id != ''){
    BASE_URL = '<?php echo e(url('/')); ?>';
    $.ajax({
      url:BASE_URL+"/advocate-panel/change-act-sub-type-section-ajax/"+act_type_id,
      success: function(result){
          $("#show_act_sub_type_section").html(result); 
      }
    });
  }
}



function sub_group_change_ajax(sub_group_change_id){

  if (sub_group_change_id != ''){
    BASE_URL = '<?php echo e(url('/')); ?>';
    $.ajax({
      url:BASE_URL+"/advocate-panel/change-sub-group-ajax/"+sub_group_change_id,
      success: function(result){
          
          if(result.status == true){
            document.getElementById("show_client_sub_group").style.display = "none";
          } else {
            document.getElementById("show_client_sub_group").style.display = "block";
            $("#sub_group_change").html(result); 
          }

      }
    });

  }

}

</script>


<script>

$(document).ready(function() {


  $('#framework_3').multiselect({
    nonSelectedText: 'Select Section',
    enableFiltering: true,
    enableCaseInsensitiveFiltering: true,
    buttonWidth:'400px'
   });
});


function myFunction() {
    var x = document.getElementById("myDIV");
    if (x.style.display === "none") {
        x.style.display = "block";
    }else{
        x.style.display = "block";
    }
}
</script>

<style>
#myDIV{
    width: 100%;
    padding: 50px 0;
    text-align: left !important;
    margin-top: 20px;
    display:none !important;
}
</style>
<script>

</script>
<script>

    // function showhide()
    //  {
    // var div = document.getElementById("myDIV");
    // if (div.style.display !== "none") {
    //     div.style.display = "block";
    // }
    //   div.style.display = "none";
    // }
     
</script>

<style>
#subtype {
    width: 100%;
    padding: 50px 0;
    text-align: left !important;
    margin-top: 20px;
    display:none;
}
</style>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('advocate_admin/layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>