<?php $__env->startSection('content'); ?>
<!-- <style type="text/css">
 
.admin-form .select, .admin-form .gui-input, .admin-form .gui-textarea, .admin-form .select > select, .admin-form .select-multiple select {
  height: 28px !important;
}
</style> -->
<style type="text/css">
  .admin-form .select, .admin-form .gui-input, .admin-form .select > select, .admin-form .select-multiple select{
    height: 28px !important;
  }
  .admin-form a.button, .admin-form span.button, .admin-form label.button
  {
    line-height: 28px !important;
  }
  .admin-form .append-icon .field-icon, .admin-form .prepend-icon .field-icon{
    line-height: 28px !important;
  }
  .admin-form .gui-textarea {
    line-height: 15px !important;
  }
  .mysave{
    width: 75px;
    padding: 0 5px !important;
  }
/*  .gui-textarea{
    height: 100px !important;
  }*/
/* .admin-form .gui-input {
  padding: 5px;
 }*/
 /*.admin-form .prepend-icon .field-icon {
  top: 6px;
 }*/
 .form-control {
    height: 28px !important;
    /*padding: 0px 12px !important;*/
  }
 .admin-form .button{
    height: 28px !important;
    line-height: 1px !important;
  }
 .btn {
    height: 29px !important;
    line-height: 1px !important;  }

 .down{
  margin-top: 70px;
 }
.account_setting {
  margin: 0px 20px !important;
  margin-top: 25px !important;
  /*padding: 5px 0px !important;*/
 }
 .select-text{
  padding: 0px 0px;
 }

 .multiselect-container li a label{
  border: 0px !important;
  background: none !important;
}
.multiselect-container {
  margin-top: 5px !important;
}
.multiselect {
    text-align: left;
}
.btn.multiselect .caret {
    display: none;
}
.multiselect-search{
  height: 36px !important;
}

.multiselect-container.dropdown-menu {
    max-height: 200px;
    overflow-x: auto;
}

</style>
  <!-- Start: Content-Wrapper -->
  <section id="content_wrapper">
    <header id="topbar">
      <div class="topbar-left down">
        <ol class="breadcrumb">
          <li class="crumb-active">
            <a href="#">Daily Diary</a>
          </li>
          <li class="crumb-icon">
            <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">
              <span class="glyphicon glyphicon-home"></span>
            </a>
          </li>
          <li class="crumb-link">
            <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">Home</a>
          </li>
          <li class="crumb-trail">Daily Diary </li>
        </ol>
      </div>
    </header>

    <div class="" style="margin-top:10px;">
      <div class="col-md-12">
        <div class="panel panel-primary panel-border top mb70">  
          <div class="panel-heading">
            <div class="panel-title hidden-xs">
              <span class="glyphicon glyphicon-tasks"></span> Daily Diary </div>
          </div>

          <div class="panel-menu admin-form theme-primary" style="padding: 5px 20px;">
            <div class="row">
              <?php echo Form::open(['url'=>'/advocate-panel/daily-diary' ,'autocomplete'=>'off']); ?>


                <div class="col-md-2">
                    <label for="level" class="field prepend-icon select_outer" style="margin-top:10px !important;">
                      <select class=" form-control new_select select-text" name="court_type" id="level" style="color: black;">
                        <option value="">Court Type</option>
                            <option value="1" <?php echo e($court_type == 1 ? 'selected="selected"' : ''); ?> >Trial Court</option>
                            <option value="2" <?php echo e($court_type == 2 ? 'selected="selected"' : ''); ?> >High Court</option>
                            <!-- <option value="3" <?php echo e($court_type == 3 ? 'selected="selected"' : ''); ?> >Both</option>  -->
                      </select>
                    </label>
                </div>

                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 10px;">
                    <?php echo Form::text('further_date_search',date('d-m-Y'),array('class' => 'form-control new_text disposal_to' ,'placeholder' => 'Cause List Date', 'autocomplete' => 'off', 'id' => '' )); ?>

                    <label for="pincode" class="field-icon search_new">
                      <i class="fa fa-calendar"></i>
                    </label>
                  </label>
                </div>

                <div class="col-md-1 pull-right mr15" style="margin-top:10px !important;">
                  <button type="submit" name="search" class="button btn-primary new_button"> Search </button>
                </div>
              <?php echo Form::close(); ?>             
                <div class="col-md-1" style="margin-top:10px !important;">
                   <a href="<?php echo e(url('/advocate-panel/daily-diary/')); ?>"><?php echo Form::submit('Default', array('class' => 'btn btn-primary new_button_2', 'id' => 'maskedKey')); ?></a>
                </div>

                <div class="col-md-2" style="margin-top:10px !important;">
                   <a href="<?php echo e(url('/advocate-panel/print-daily-diary/')); ?>/<?php echo e($further_date); ?>/<?php echo e($court_type); ?>"><?php echo Form::submit('Print Daily Diary', array('class' => 'btn btn-primary new_button_2', 'id' => 'maskedKey')); ?></a>
                </div>

                <div class="col-md-2 mt10">
                   <a href="<?php echo e(url('/advocate-panel/daily-diary/all')); ?>"><?php echo Form::button('Show All Records', array('class' => 'btn btn-primary', 'id' => 'maskedKey')); ?></a>
                </div>

            </div>
          </div>

          <div class="panel-body pn">
              <!-- <?php echo Form::open(['url'=>'/advocate-panel/daily-diary','name'=>'form']); ?> -->

              <?php echo Form::open(['name'=>'form_add_pessi','url'=>'advocate-panel/insert-daily-diary/'.$get_records->pessi_id,'id'=>'form_add_pessi' ,'autocomplete'=>'off']); ?>


              <div class="table-responsive">
                <table class="table admin-form table-bordered table-striped theme-warning tc-checkbox-1 fs13" id="datatable2">
                  <thead>
                    <tr class="bg-light">
                      <!-- <th style="width:90px !important;" class="text-left">
                        <label class="option block mn">
                          <input type="checkbox" id="check_all"> 
                          <span class="checkbox mn"></span>
                          Select All
                        </label>
                      </th> -->
                      <th> S.No</th>
                      <th class="text-left">Case Type</th>
                      <th class="text-left">Case No.</th>
                      <th class="text-left">Title</th>
                      <th class="text-left">Stage</th>
                      <th class="text-left">Order Sheet</th>
                      <th class="text-left">Next Date</th>
                      <th class="text-left">Result</th>                     
                      <!-- <th class="text-left">SMS</th> -->
                      <th class="text-left">Save</th>
                    </tr>
                  </thead>
                  <tbody>

                  <?php $sno = 0; ?>
                  <?php $__currentLoopData = $get_record; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_records): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>       

                  <?php $sno = $sno+1; ?>         
                    <tr>
                      <!-- <td class="text-left" style="padding-left: 18px;">
                        <label class="option block mn">
                          <input type="checkbox" name="check[]" class="check" value="<?php echo e($get_records->pessi_id); ?>">
                          <span class="checkbox mn"></span>
                        </label>
                      </td> -->
                      <td> <?php echo e($sno); ?> </td>
                      <td class="text-left" style="padding-left: 20px;">  <?php if($get_records->case_name == ""): ?> ----- <?php else: ?> <?php echo e($get_records->case_name); ?> <?php endif; ?> </td>

                      <td class="text-left" style="padding-left: 20px;">  <?php if($get_records->reg_case_number == ""): ?> ----- <?php else: ?> <?php echo e($get_records->reg_case_number); ?> <?php endif; ?> </td>

                      <td class="text-left" style="padding-left: 20px;">  <?php echo e($get_records->reg_petitioner); ?> v/s <?php echo e($get_records->reg_respondent); ?>  </td>

                      <td class="text-left" style="">
                        <!-- <label for="artist_state" class="field select"> -->
                          <label for="level" class="field prepend-icon">
                          <select class="form-control select-text" name="reg_stage" id="reg_stage_<?php echo e($get_records->pessi_id); ?>" style="width: 100px;">
                            <!-- <option value=''>Select Stage</option> -->
                              <?php $__currentLoopData = $get_stage; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $stages): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                <option value="<?php echo e($stages->stage_id); ?>" <?php echo e($stages->stage_id == $get_records->pessi_statge_id ? 'selected="selected"' : ''); ?> ><?php echo e($stages->stage_name); ?> </option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                
                          </select>
                        </label>
                      </td>

                      <td class="text-left" style="">
                        <label for="level_name" class="field prepend-icon">
                          <?php echo Form::textarea('order_sheet',$get_records->pessi_order_sheet,array('class' => 'gui-textarea accc1 padding_less','placeholder' => '','id'=>'order_sheet_'.$get_records->pessi_id )); ?>

                                                       
                        </label>
                      </td>
                      <?php echo Form::hidden('current_date',date('d-m-Y'),array('class' => 'gui-input current_date','placeholder' => '','id'=>'current_date'  )); ?>


                      <td class="text-left" style="">

                        <label for="level_name" class="field prepend-icon" id="discount_date_from_lb" style="width: 90px;">
                                                      
                          <?php if($get_records->pessi_further_date == "" || $get_records->pessi_further_date == "1970-01-01"): ?>

                          <?php echo Form::text('further_date', '' ,array('class' => 'gui-input padding_less fromdate_search_new','placeholder' => '','id'=>'further_date_'.$get_records->pessi_id , $get_records->pessi_status == 0 ? 'disabled' : '' )); ?>


                          <?php else: ?> 

                          <?php echo Form::text('further_date', date('d-m-Y',strtotime($get_records->pessi_further_date)) ,array('class' => 'gui-input  padding_less fromdate_search_new','placeholder' => '','id'=>'further_date_'.$get_records->pessi_id , $get_records->pessi_status == 0 ? 'disabled' : ''  )); ?>


                          <?php endif; ?>
                          
                          <div></div>

                          <?php if($get_records->pessi_status == 0): ?>
                            <div style="color: rgb(222, 136, 138);"> Next Date is available you can't edit this date. </div>
                          <?php endif; ?>
                        </label>
                      </td>

                      <td class="text-left w100" style="">
                        <div class="col-md-12" style="width: 135px;">

                          <label class="option" style="font-size:12px;">
                            <?php echo Form::radio('choose_type'.$get_records->pessi_id,'0',$get_records->pessi_choose_type == 0 ? 'checked' : '', array('onclick' => "show_due_course('$get_records->pessi_id')", 'class' => 'check' , 'id'=>'choose_type_pending_'.$get_records->pessi_id , $get_records->pessi_status == 0 ? 'disabled' : '' )); ?>

                            <span class="radio" style="border: 2px solid #4a89dc !important;"></span> Pending
                          </label>
                        </div>

                        <div class="col-md-12" style="width: 135px;">
                          <label class="option" style="font-size:12px;">
                            <?php echo Form::radio('choose_type'.$get_records->pessi_id,'2',$get_records->pessi_choose_type == 2 ? 'checked' : '', array('onclick' => "show_due_course('$get_records->pessi_id')", 'class' => 'check' , 'id'=>'choose_type_due_course_'.$get_records->pessi_id , $get_records->pessi_status == 0 ? 'disabled' : '' )); ?>

                            <span class="radio" style="border: 2px solid #4a89dc !important;"></span> Due Course
                          </label>
                        </div>

                        <div class="col-md-12" style="width: 135px;">
                          <label class="option" style="font-size:12px;">
                            <?php echo Form::radio('choose_type'.$get_records->pessi_id,'1',$get_records->pessi_choose_type == 1 ? 'checked' : '', array('onclick' => "show_decide('$get_records->pessi_id')", 'class' => 'check','id'=>'choose_type_decide_'.$get_records->pessi_id , $get_records->pessi_status == 0 ? 'disabled' : '' )); ?>

                            <span class="radio" style="border: 2px solid #4a89dc !important;"></span> Decide
                          </label>
                        </div>

                        <div class="col-md-12" style="display: none;"  id="show_decide_new_<?php echo e($get_records->pessi_id); ?>">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;"> Decide : </label>
                              <!-- <label for="artist_state" class="field select"> -->
                                <label for="level" class="field prepend-icon">
                                <select class="form-control select-text" id="decide_<?php echo e($get_records->pessi_id); ?>" name="decide" >
                                  <option value=''>Select Decide </option>
                                  <option value='1' <?php echo e($get_records->pessi_decide_id == 1 ? 'selected="selected"' : ''); ?> >Case In Favour </option>      
                                  <option value='2' <?php echo e($get_records->pessi_decide_id == 2 ? 'selected="selected"' : ''); ?> >Case in against </option>
                                  <option value='3' <?php echo e($get_records->pessi_decide_id == 3 ? 'selected="selected"' : ''); ?> > Withdraw </option>
                                  <option value='4' <?php echo e($get_records->pessi_decide_id == 4 ? 'selected="selected"' : ''); ?> > None </option>
                                </select>
                              </label>
                          </div>
                        </div>

                      </td>


                      <!-- <td class="text-left" style="">
                        <label for="level_name" class="field prepend-icon" id="discount_date_from_lb" style="width: 125px;">
                                
                          <?php echo Form::text('sms_text', '' ,array('class' => 'gui-input padding_less','placeholder' => '', 'id'=>'sms_text_'.$get_records->pessi_id   )); ?>


                        </label>

                      </td> -->

                      <td style="padding-left: 20px;">
                        <?php if($get_records->pessi_update == 0): ?>
                          <button type="button" class="button btn-primary mysave" id="maskedKey_<?php echo e($get_records->pessi_id); ?>" onclick="update_daily(<?php echo e($get_records->pessi_id); ?>)" > Save </button>
                        <?php else: ?> 
                          <button type="button" class="button btn-primary mysave" id="maskedKey_<?php echo e($get_records->pessi_id); ?>" onclick="update_daily(<?php echo e($get_records->pessi_id); ?>)" > <span class="fa fa-check" aria-hidden="true"></span>  Saved </button>
                        <?php endif; ?>

                        <?php if($get_records->pessi_status == 0): ?>
                          <div style="margin-top: 20px;">
                            <button type="button" class="button btn-primary mysave" id="maskedKey_<?php echo e($get_records->pessi_id); ?>" > SMS </button>
                          </div>
                        <?php else: ?>
                          <div style="margin-top: 20px;"> <a href="#" class="button btn-primary" style="text-transform: capitalize; text-decoration:none;background-color: #4a89dc;color: white;" onclick="edit_pessi(<?php echo e($get_records->pessi_id); ?>)" > SMS </a> <div>
                        <?php endif; ?>  
                      </td>



                      <!-- <td class="text-left" style="padding-left: 20px;"> <a href="<?php echo e(url('/advocate-panel/case-detail')); ?>/<?php echo e(sha1($get_records->reg_id)); ?>"> <?php if($get_records->reg_file_no == ""): ?> ----- <?php else: ?> <?php echo e($get_records->reg_file_no); ?> <?php endif; ?> </a> </td> -->

                      <!-- <td class="text-left" style="padding-left: 20px;">
                        <?php if($get_records->reg_court == 2): ?>
                               High Court
                        <?php elseif($get_records->reg_court == 1): ?>
                               Trial Court
                        <?php endif; ?>
                      </td> -->

                      <!-- <td style="padding-left: 20px;"> -->
                        <!-- <a href="#" style="text-transform: capitalize; text-decoration:none;" data-toggle="modal" data-target="#edit_pessi<?php echo e($get_records->pessi_id); ?>" > Edit </a> -->
                        <div id="edit_pessi<?php echo e($get_records->pessi_id); ?>" class="modal fade in" role="dialog" style="overflow: scroll;"> 
                          <div class="modal-dialog" style="width:700px; margin-top:80px;">
                            <div class="modal-content">
                              <div class="modal-header" style="padding-bottom: 35px;">
                                <button type="button" class="close" data-dismiss="modal" onclick="close_button_edit_pessi(<?php echo e($get_records->pessi_id); ?>)" >&times;</button>
                                <h4 class="modal-title pull-left"> Send SMS </h4>
                              </div>
                              <section id="content" class="table-layout animated fadeIn">
                                <div>
                                  <div class="center-block">
                                    <div class="panel panel-primary panel-border top mb35">
                                      <div class="panel-body bg-light dark">
                                        <div class="tab-content pn br-n admin-form">
                                          <div id="tab1_1" class="tab-pane active">
                                            
                                            <?php echo Form::open(['name'=>'form_add_pessi','url'=>'advocate-panel/insert-daily-diary/'.$get_records->pessi_id,'id'=>'form_daily_sms_'.$get_records->pessi_id ,'autocomplete'=>'off' ]); ?>


                                            <div class="row">
                                              
                                              <!-- <div class="col-md-12" id="">
                                                <div class="section">
                                                  <label for="level_name" class="field-label" style="font-weight:600;" > Assigned : </label>
                                                    <label for="artist_state" class="field">
                                                      <select id="assigned<?php echo e($get_records->pessi_id); ?>" name="assigned[]" multiple class="form-control">
                                                          <?php $__currentLoopData = $assigned; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $assigneds): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option value="<?php echo e($assigneds->assign_id); ?>"> <?php echo e($assigneds->assign_advocate_name); ?> <?php if($assigneds->assign_mobile_number != ""): ?> (<?php echo e($assigneds->assign_mobile_number); ?>) <?php endif; ?> </option>
                                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                      </select>
                                                    </label>
                                                </div>
                                              </div> -->


                                              <?php if($get_records->assign_mobile_number != ""): ?>
                                              <div class="col-md-12">
                                                <div class="section" style="margin-bottom: 40px;">
                                                  <label for="level_name" class="field-label" style="font-weight:600;" > Assign :  </label>  
                                                  
                                                  <div class="col-md-12">
                                                    <label class="option block mn" style="font-size:12px;">
                                                      <?php echo Form::checkbox('assigned[]',$get_records->assign_id,'', array( 'class' => 'check', 'id' => 'assigned'.$get_records->pessi_id  )); ?>

                                                      <span class="checkbox mn" style="border: 2px solid #4a89dc !important;"></span> <?php echo e($get_records->assign_advocate_name); ?> ( <?php echo e($get_records->assign_mobile_number); ?>)
                                                    </label>
                                                  </div>
                                                </div>
                                                <div class="section"></div>
                                                <div class="clearfix"></div>
                                              </div>
                                              <?php endif; ?>

                                              <?php if($get_records->ref_mobile_number != ""): ?>
                                              <div class="col-md-12">
                                                <div class="section" style="margin-bottom: 40px;">
                                                  <label for="level_name" class="field-label" style="font-weight:600;" > Referred :  </label>  
                                                  
                                                  <div class="col-md-12">
                                                    <label class="option block mn" style="font-size:12px;">
                                                      <?php echo Form::checkbox('referred[]',$get_records->ref_id,'', array( 'class' => 'check', 'id' => 'referred'.$get_records->pessi_id  )); ?>

                                                      <span class="checkbox mn" style="border: 2px solid #4a89dc !important;"></span> <?php echo e($get_records->ref_advocate_name); ?> ( <?php echo e($get_records->ref_mobile_number); ?>)
                                                    </label>
                                                  </div>
                                                </div>
                                                <div class="section"></div>
                                                <div class="clearfix"></div>
                                              </div>
                                              <?php endif; ?>

                                              <!-- <div class="col-md-12" id="">
                                                <div class="section">
                                                  <label for="level_name" class="field-label" style="font-weight:600;" > Referred : </label>
                                                    <label for="artist_state" class="field">
                                                      <select id="referred<?php echo e($get_records->pessi_id); ?>" name="referred[]" multiple class="form-control">
                                                          <?php $__currentLoopData = $referred; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $referreds): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option value="<?php echo e($referreds->ref_id); ?>"> <?php echo e($referreds->ref_advocate_name); ?> <?php if($referreds->ref_mobile_number != ""): ?> (<?php echo e($referreds->ref_mobile_number); ?>) <?php endif; ?> </option>
                                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                      </select>
                                                    </label>
                                                </div>
                                              </div> -->

                                              <!-- <div class="col-md-12">
                                                <div class="section" style="margin-bottom: 40px;">
                                                  <label for="level_name" class="field-label" style="font-weight:600;" > Send SMS :  </label>  
                                                  <div class="col-md-2">
                                                    <label class="option" style="font-size:12px;">
                                                        <?php echo Form::radio('send_sms','1',true, array( 'class' => 'check' , 'onclick' => 'send_sms_all(1)' )); ?>

                                                      <span class="radio"></span> All
                                                    </label>
                                                  </div>

                                                  <div class="col-md-3">
                                                    <label class="option" style="font-size:12px;">
                                                        <?php echo Form::radio('send_sms','2','', array( 'class' => 'check' , 'onclick' => 'send_sms_all(2)' )); ?>

                                                      <span class="radio"></span> Pending
                                                    </label>
                                                  </div>
                                                </div>
                                                <div class="section"></div>
                                                <div class="clearfix"></div>
                                              </div> -->

                                              <!-- <div class="col-md-12" id="hide_framework">
                                                <div class="section">
                                                  <label for="level_name" class="field-label" style="font-weight:600;" > Client : </label>
                                                    <label for="artist_state" class="field">
                                                      <select id="framework<?php echo e($get_records->pessi_id); ?>" name="client[]" multiple class="form-control"  > 
                                                          <?php $__currentLoopData = $client; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $clients): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option value="<?php echo e($clients->cl_id); ?>"> <?php echo e($clients->cl_group_name); ?> <?php if($clients->cl_father_name != ""): ?> <?php echo e($clients->cl_name_prefix); ?> <?php echo e($clients->cl_father_name); ?> <?php endif; ?> <?php if($clients->cl_group_mobile_no != ""): ?> (<?php echo e($clients->cl_group_mobile_no); ?>) <?php endif; ?> </option>
                                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                      </select>
                                                    </label>
                                                </div>
                                              </div> -->

                                              <?php if($get_records->cl_group_mobile_no != ""): ?>
                                              <div class="col-md-12">
                                                <div class="section" style="margin-bottom: 40px;">
                                                  <label for="level_name" class="field-label" style="font-weight:600;" > Client :  </label>  
                                                  
                                                  <div class="col-md-12">
                                                    <label class="option block mn" style="font-size:12px;">
                                                      <?php echo Form::checkbox('client[]',$get_records->cl_id,'', array( 'class' => 'check', 'id' => 'framework'.$get_records->pessi_id  )); ?>

                                                      <span class="checkbox mn" style="border: 2px solid #4a89dc !important;"></span> <?php echo e($get_records->cl_group_name); ?> ( <?php echo e($get_records->cl_group_mobile_no); ?>)
                                                    </label>
                                                  </div>
                                                </div>
                                                <div class="section"></div>
                                                <div class="clearfix"></div>
                                              </div>
                                              <?php endif; ?>

                                              <?php if($get_records->sub_client_mobile_no != ""): ?>
                                              <div class="col-md-12">
                                                <div class="section" style="margin-bottom: 40px;">
                                                  <label for="level_name" class="field-label" style="font-weight:600;" > Sub Client :  </label>  
                                                  
                                                  <div class="col-md-12">
                                                    <label class="option block mn" style="font-size:12px;">
                                                      <?php echo Form::checkbox('sub_client[]',$get_records->sub_client_id,'', array( 'class' => 'check', 'id' => 'framework_2'.$get_records->pessi_id  )); ?>

                                                      <span class="checkbox mn" style="border: 2px solid #4a89dc !important;"></span> <?php echo e($get_records->sub_client_name); ?> ( <?php echo e($get_records->sub_client_mobile_no); ?>)
                                                    </label>
                                                  </div>
                                                </div>
                                                <div class="section"></div>
                                                <div class="clearfix"></div>
                                              </div>
                                              <?php endif; ?>



                                              <!-- <div class="col-md-12" id="framework_sms"></div>
                                              <div class="col-md-12" id="sub_client_view">
                                                <div class="section">
                                                  <label for="level_name" class="field-label" style="font-weight:600;" > Sub Client : </label>
                                                    <label for="artist_state" class="field">
                                                      <select id="framework_2<?php echo e($get_records->pessi_id); ?>" name="sub_client[]" multiple class="form-control" >
                                                          <?php $__currentLoopData = $sub_client; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub_clients): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option value="<?php echo e($sub_clients->sub_client_id); ?>"> <?php echo e($sub_clients->sub_client_name); ?> <?php if($sub_clients->sub_guardian_name != ""): ?> <?php echo e($sub_clients->sub_name_prefix); ?> <?php echo e($sub_clients->sub_guardian_name); ?> <?php endif; ?>  <?php if($sub_clients->sub_client_mobile_no != ""): ?> (<?php echo e($sub_clients->sub_client_mobile_no); ?>) <?php endif; ?></option>
                                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                      </select>
                                                      <i class="arrow double"></i>
                                                    </label>
                                                </div>
                                              </div> -->
                                                     
                                              <div class="col-md-12">
                                                <div class="section">
                                                  <label for="level_name" class="field-label" style="font-weight:600;"> Text :  </label>
                                                  <label for="level_name" class="field prepend-icon">
                                                    <?php echo Form::text('reg_sms_text', '' ,array('class' => 'gui-input','id' => 'reg_sms_text'.$get_records->pessi_id )); ?>

                                                      <label for="Account Mobile" class="field-icon">
                                                      <i class="fa fa-pencil"></i>
                                                    </label>                           
                                                  </label>
                                                </div>
                                              </div>
                                           </div>

                                          <div class="panel-footer text-right">

                                              <button type="button" class="button btn-primary mysave" id="maskedKey_daily_<?php echo e($get_records->pessi_id); ?>" onclick="send_sms_daily_new(<?php echo e($get_records->pessi_id); ?>)"> Send </button>
                                              <!-- <?php echo Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')); ?> -->
                                          </div>   


                                          <!-- <script type="text/javascript">
                                            
                                            jQuery(document).ready(function() {

                                              $('#assigned'+<?php echo e($get_records->pessi_id); ?>).multiselect({
                                                nonSelectedText: 'Select Assigned',
                                                enableFiltering: true,
                                                enableCaseInsensitiveFiltering: true,
                                                buttonWidth:'400px'
                                               });

                                              $('#referred'+<?php echo e($get_records->pessi_id); ?>).multiselect({
                                                nonSelectedText: 'Select Referred',
                                                enableFiltering: true,
                                                enableCaseInsensitiveFiltering: true,
                                                buttonWidth:'400px'
                                               });

                                              $('#framework'+<?php echo e($get_records->pessi_id); ?>).multiselect({
                                                nonSelectedText: 'Select Client',
                                                enableFiltering: true,
                                                enableCaseInsensitiveFiltering: true,
                                                buttonWidth:'400px'
                                               });

                                              $('#framework_2'+<?php echo e($get_records->pessi_id); ?>).multiselect({
                                                nonSelectedText: 'Select Sub Client',
                                                enableFiltering: true,
                                                enableCaseInsensitiveFiltering: true,
                                                buttonWidth:'400px'
                                               });

                                              $('#framework_3'+<?php echo e($get_records->pessi_id); ?>).multiselect({
                                                nonSelectedText: 'Select Client',
                                                enableFiltering: true,
                                                enableCaseInsensitiveFiltering: true,
                                                buttonWidth:'400px'
                                               });
                                            });

                                          </script> -->
                                            <?php echo Form::close(); ?>

                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </section>
                              <div class="clearfix"></div>
                            </div> 
                          </div>
                        </div>
                      <!-- </td> -->

                      <!-- <td style="padding-left: 20px;">
                        <?php if($get_records->pessi_choose_type == 0): ?>
                          Next Date:-  <?php if($get_records->pessi_further_date != "1970-01-01"): ?> <?php echo e(date('d F Y',strtotime($get_records->pessi_further_date))); ?> <?php else: ?> ----- <?php endif; ?>
                        <?php elseif($get_records->pessi_choose_type == 1): ?>
                          Decide :- <?php if($get_records->pessi_decide_id == 1): ?> Case In Favour <?php elseif($get_records->pessi_decide_id == 2): ?> Case in against <?php elseif($get_records->pessi_decide_id == 3): ?> Withdraw <?php elseif($get_records->pessi_decide_id == 4): ?> None <?php endif; ?>
                        <?php else: ?>
                          Due Course
                        <?php endif; ?>
                      </td> -->

                    </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                  <?php $sno = count($get_record); ?>
                  <?php $__currentLoopData = $garbage_data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $garbages): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                  <?php $sno = $sno+1; ?>     

                    <tr>
                      
                      <td> <?php echo e($sno); ?> </td>
                      <td class="text-left" style="padding-left: 20px;">  <?php if($garbages->case_name == ""): ?> ----- <?php else: ?> <?php echo e($garbages->case_name); ?> <?php endif; ?> </td>

                      <td class="text-left" style="padding-left: 20px;">  <?php if($garbages->reg_case_number == ""): ?> ----- <?php else: ?> <?php echo e($garbages->reg_case_number); ?> <?php endif; ?> </td>

                      <td class="text-left" style="padding-left: 20px;">  <?php echo e($garbages->reg_petitioner); ?> v/s <?php echo e($garbages->reg_respondent); ?>  </td>

                      <td class="text-left" style="">
                          <label for="level" class="field prepend-icon">
                          <select class="form-control select-text" name="reg_stage" id="reg_stage_<?php echo e($garbages->pessi_id); ?>" style="width: 100px;">
                              <?php $__currentLoopData = $get_stage; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $stages): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                <option value="<?php echo e($stages->stage_id); ?>" <?php echo e($stages->stage_id == $garbages->pessi_statge_id ? 'selected="selected"' : ''); ?> ><?php echo e($stages->stage_name); ?> </option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                
                          </select>
                        </label>
                      </td>

                      <td class="text-left" style="">
                        <label for="level_name" class="field prepend-icon">
                          <?php echo Form::textarea('order_sheet',$garbages->pessi_order_sheet,array('class' => 'gui-textarea accc1 padding_less','placeholder' => '','id'=>'order_sheet_'.$garbages->pessi_id )); ?>

                        </label>
                      </td>
                      <?php echo Form::hidden('current_date',date('d-m-Y'),array('class' => 'gui-input current_date','placeholder' => '','id'=>'current_date'  )); ?>


                      <td class="text-left" style="">
                        <label for="level_name" class="field prepend-icon" id="discount_date_from_lb" style="width: 90px;"> 
                          <?php if($garbages->pessi_further_date == "" || $garbages->pessi_further_date == "1970-01-01"): ?>
                            <?php echo Form::text('further_date', '' ,array('class' => 'gui-input padding_less fromdate_search_new','placeholder' => '','id'=>'further_date_'.$garbages->pessi_id , $garbages->pessi_status == 0 ? 'disabled' : '' )); ?>

                          <?php else: ?> 
                            <?php echo Form::text('further_date', date('d-m-Y',strtotime($garbages->pessi_further_date)) ,array('class' => 'gui-input  padding_less fromdate_search_new','placeholder' => '','id'=>'further_date_'.$garbages->pessi_id , $garbages->pessi_status == 0 ? 'disabled' : '' )); ?>

                          <?php endif; ?>
                          <div></div>
                          <?php if($garbages->pessi_status == 0): ?>
                            <div style="color: rgb(222, 136, 138);"> Next Date is available you can't edit this date. </div>
                          <?php endif; ?>
                        </label>
                      </td>

                      <td class="text-left w100" style="">
                        
                        <div class="col-md-12" style="width: 135px;">
                          <label class="option" style="font-size:12px;">
                            <?php echo Form::radio('choose_type'.$garbages->pessi_id ,'0',$garbages->pessi_choose_type == 0 ? 'checked' : '', array('onclick' => "show_due_course('$garbages->pessi_id')", 'class' => 'check' , 'id'=>'choose_type_pending_'.$garbages->pessi_id , $garbages->pessi_status == 0 ? 'disabled' : '')); ?>

                            <span class="radio" style="border: 2px solid #4a89dc !important;"></span> Pending
                          </label>
                        </div>

                        <div class="col-md-12" style="width: 135px;">
                          <label class="option" style="font-size:12px;">
                            <?php echo Form::radio('choose_type'.$garbages->pessi_id ,'2',$garbages->pessi_choose_type == 2 ? 'checked' : '', array('onclick' => "show_due_course('$garbages->pessi_id')", 'class' => 'check' , 'id'=>'choose_type_due_course_'.$garbages->pessi_id , $garbages->pessi_status == 0 ? 'disabled' : '' )); ?>

                            <span class="radio" style="border: 2px solid #4a89dc !important;"></span> Due Course
                          </label>
                        </div>

                        <div class="col-md-12" style="width: 135px;">
                          <label class="option" style="font-size:12px;">
                            <?php echo Form::radio('choose_type'.$garbages->pessi_id ,'1',$garbages->pessi_choose_type == 1 ? 'checked' : '', array('onclick' => "show_decide('$garbages->pessi_id')", 'class' => 'check','id'=>'choose_type_decide_'.$garbages->pessi_id , $garbages->pessi_status == 0 ? 'disabled' : '' )); ?>

                            <span class="radio" style="border: 2px solid #4a89dc !important;"></span> Decide
                          </label>
                        </div>

                        <div class="col-md-12" style="display: none;" id="show_decide_new_<?php echo e($garbages->pessi_id); ?>">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;"> Decide : </label>
                              <!-- <label for="artist_state" class="field select"> -->
                                <label for="level" class="field prepend-icon">
                                <select class="form-control select-text" id="decide_<?php echo e($garbages->pessi_id); ?>" name="decide" >
                                  <option value=''>Select Decide </option>
                                  <option value='1' <?php echo e($garbages->pessi_decide_id == 1 ? 'selected="selected"' : ''); ?> >Case In Favour </option>      
                                  <option value='2' <?php echo e($garbages->pessi_decide_id == 2 ? 'selected="selected"' : ''); ?> >Case in against </option>
                                  <option value='3' <?php echo e($garbages->pessi_decide_id == 3 ? 'selected="selected"' : ''); ?> > Withdraw </option>
                                  <option value='4' <?php echo e($garbages->pessi_decide_id == 4 ? 'selected="selected"' : ''); ?> > None </option>
                                </select>
                              </label>
                          </div>
                        </div>
                      </td>

                      <td style="padding-left: 20px;">
                        <?php if($garbages->pessi_update == 0): ?>
                          <button type="button" class="button btn-primary mysave" id="maskedKey_<?php echo e($garbages->pessi_id); ?>" onclick="update_daily(<?php echo e($garbages->pessi_id); ?>)"  > Save </button>
                        <?php else: ?> 
                          <button type="button" class="button btn-primary mysave" id="maskedKey_<?php echo e($garbages->pessi_id); ?>" onclick="update_daily(<?php echo e($garbages->pessi_id); ?>)" > <span class="fa fa-check" aria-hidden="true"></span>  Saved </button>
                        <?php endif; ?>

                        <?php if($garbages->pessi_status == 0): ?>
                          <div style="margin-top: 20px;">
                            <button type="button" class="button btn-primary mysave" id="maskedKey_<?php echo e($garbages->pessi_id); ?>" > SMS </button>
                          </div>
                        <?php else: ?>
                          <div style="margin-top: 20px;"> <a href="#" class="button btn-primary" style="text-transform: capitalize; text-decoration:none;background-color: #4a89dc;color: white;" onclick="edit_pessi(<?php echo e($garbages->pessi_id); ?>)" > SMS </a> <div>
                        <?php endif; ?>
                        
                      </td>

                        <div id="edit_pessi<?php echo e($garbages->pessi_id); ?>" class="modal fade in" role="dialog" style="overflow: scroll;">
                          <div class="modal-dialog" style="width:700px; margin-top:80px;">
                            <div class="modal-content">
                              <div class="modal-header" style="padding-bottom: 35px;">
                                <button type="button" class="close" data-dismiss="modal" onclick="close_button_edit_pessi(<?php echo e($garbages->pessi_id); ?>)">&times;</button>
                                <h4 class="modal-title pull-left"> Send SMS </h4>
                              </div>
                              <section id="content" class="table-layout animated fadeIn">
                                <div>
                                  <div class="center-block">
                                    <div class="panel panel-primary panel-border top mb35">
                                      <div class="panel-body bg-light dark">
                                        <div class="tab-content pn br-n admin-form">
                                          <div id="tab1_1" class="tab-pane active">
                                            
                                            <?php echo Form::open(['name'=>'form_add_pessi','url'=>'advocate-panel/insert-daily-diary/'.$garbages->pessi_id,'id'=>'form_daily_sms_'.$garbages->pessi_id ,'autocomplete'=>'off' ]); ?>


                                            <div class="row">
                                              
                                              <!-- <div class="col-md-12" id="">
                                                <div class="section">
                                                  <label for="level_name" class="field-label" style="font-weight:600;" > Assigned : </label>
                                                    <label for="artist_state" class="field">
                                                      <select id="assigned<?php echo e($garbages->pessi_id); ?>" name="assigned[]" multiple class="form-control">
                                                          <?php $__currentLoopData = $assigned; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $assigneds): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option value="<?php echo e($assigneds->assign_id); ?>"> <?php echo e($assigneds->assign_advocate_name); ?> <?php if($assigneds->assign_mobile_number != ""): ?> (<?php echo e($assigneds->assign_mobile_number); ?>) <?php endif; ?> </option>
                                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                      </select>
                                                    </label>
                                                </div>
                                              </div>

                                              <div class="col-md-12" id="">
                                                <div class="section">
                                                  <label for="level_name" class="field-label" style="font-weight:600;" > Referred : </label>
                                                    <label for="artist_state" class="field">
                                                      <select id="referred<?php echo e($garbages->pessi_id); ?>" name="referred[]" multiple class="form-control">
                                                          <?php $__currentLoopData = $referred; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $referreds): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option value="<?php echo e($referreds->ref_id); ?>"> <?php echo e($referreds->ref_advocate_name); ?> <?php if($referreds->ref_mobile_number != ""): ?> (<?php echo e($referreds->ref_mobile_number); ?>) <?php endif; ?> </option>
                                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                      </select>
                                                    </label>
                                                </div>
                                              </div> -->

                                              <!-- <div class="col-md-12">
                                                <div class="section" style="margin-bottom: 40px;">
                                                  <label for="level_name" class="field-label" style="font-weight:600;" > Send SMS :  </label>  
                                                  <div class="col-md-2">
                                                    <label class="option" style="font-size:12px;">
                                                        <?php echo Form::radio('send_sms','1',true, array( 'class' => 'check' , 'onclick' => 'send_sms_all(1)' )); ?>

                                                      <span class="radio"></span> All
                                                    </label>
                                                  </div>

                                                  <div class="col-md-3">
                                                    <label class="option" style="font-size:12px;">
                                                        <?php echo Form::radio('send_sms','2','', array( 'class' => 'check' , 'onclick' => 'send_sms_all(2)' )); ?>

                                                      <span class="radio"></span> Pending
                                                    </label>
                                                  </div>
                                                </div>
                                                <div class="section"></div>
                                                <div class="clearfix"></div>
                                              </div> -->

                                              <!-- <div class="col-md-12" id="hide_framework">
                                                <div class="section">
                                                  <label for="level_name" class="field-label" style="font-weight:600;" > Client : </label>
                                                    <label for="artist_state" class="field">
                                                      <select id="framework<?php echo e($garbages->pessi_id); ?>" name="client[]" multiple class="form-control"  > 
                                                          <?php $__currentLoopData = $client; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $clients): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option value="<?php echo e($clients->cl_id); ?>"> <?php echo e($clients->cl_group_name); ?> <?php if($clients->cl_father_name != ""): ?> <?php echo e($clients->cl_name_prefix); ?> <?php echo e($clients->cl_father_name); ?> <?php endif; ?> <?php if($clients->cl_group_mobile_no != ""): ?> (<?php echo e($clients->cl_group_mobile_no); ?>) <?php endif; ?> </option>
                                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                      </select>
                                                    </label>
                                                </div>
                                              </div>

                                              <div class="col-md-12" id="framework_sms"></div>
                                              <div class="col-md-12" id="sub_client_view">
                                                <div class="section">
                                                  <label for="level_name" class="field-label" style="font-weight:600;" > Sub Client : </label>
                                                    <label for="artist_state" class="field">
                                                      <select id="framework_2<?php echo e($garbages->pessi_id); ?>" name="sub_client[]" multiple class="form-control" >
                                                          <?php $__currentLoopData = $sub_client; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub_clients): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option value="<?php echo e($sub_clients->sub_client_id); ?>"> <?php echo e($sub_clients->sub_client_name); ?> <?php if($sub_clients->sub_guardian_name != ""): ?> <?php echo e($sub_clients->sub_name_prefix); ?> <?php echo e($sub_clients->sub_guardian_name); ?> <?php endif; ?>  <?php if($sub_clients->sub_client_mobile_no != ""): ?> (<?php echo e($sub_clients->sub_client_mobile_no); ?>) <?php endif; ?></option>
                                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                      </select>
                                                      <i class="arrow double"></i>
                                                    </label>
                                                </div>
                                              </div> -->

                                              <?php if($garbages->assign_mobile_number != ""): ?>
                                              <div class="col-md-12">
                                                <div class="section" style="margin-bottom: 40px;">
                                                  <label for="level_name" class="field-label" style="font-weight:600;" > Assign :  </label>  
                                                  
                                                  <div class="col-md-12">
                                                    <label class="option block mn" style="font-size:12px;">
                                                      <?php echo Form::checkbox('assigned[]',$garbages->assign_id,'', array( 'class' => 'check', 'id' => 'assigned'.$garbages->pessi_id  )); ?>

                                                      <span class="checkbox mn" style="border: 2px solid #4a89dc !important;"></span> <?php echo e($garbages->assign_advocate_name); ?> ( <?php echo e($garbages->assign_mobile_number); ?>)
                                                    </label>
                                                  </div>
                                                </div>
                                                <div class="section"></div>
                                                <div class="clearfix"></div>
                                              </div>
                                              <?php endif; ?>

                                              <?php if($garbages->ref_mobile_number != ""): ?>
                                              <div class="col-md-12">
                                                <div class="section" style="margin-bottom: 40px;">
                                                  <label for="level_name" class="field-label" style="font-weight:600;" > Referred :  </label>  
                                                  
                                                  <div class="col-md-12">
                                                    <label class="option block mn" style="font-size:12px;">
                                                      <?php echo Form::checkbox('referred[]',$garbages->ref_id,'', array( 'class' => 'check', 'id' => 'referred'.$garbages->pessi_id  )); ?>

                                                      <span class="checkbox mn" style="border: 2px solid #4a89dc !important;"></span> <?php echo e($garbages->ref_advocate_name); ?> ( <?php echo e($garbages->ref_mobile_number); ?>)
                                                    </label>
                                                  </div>
                                                </div>
                                                <div class="section"></div>
                                                <div class="clearfix"></div>
                                              </div>
                                              <?php endif; ?>

                                              <?php if($garbages->cl_group_mobile_no != ""): ?>
                                              <div class="col-md-12">
                                                <div class="section" style="margin-bottom: 40px;">
                                                  <label for="level_name" class="field-label" style="font-weight:600;" > Client :  </label>  
                                                  
                                                  <div class="col-md-12">
                                                    <label class="option block mn" style="font-size:12px;">
                                                      <?php echo Form::checkbox('client[]',$garbages->cl_id,'', array( 'class' => 'check', 'id' => 'framework'.$garbages->pessi_id  )); ?>

                                                      <span class="checkbox mn" style="border: 2px solid #4a89dc !important;"></span> <?php echo e($garbages->cl_group_name); ?> ( <?php echo e($garbages->cl_group_mobile_no); ?>)
                                                    </label>
                                                  </div>
                                                </div>
                                                <div class="section"></div>
                                                <div class="clearfix"></div>
                                              </div>
                                              <?php endif; ?>

                                              <?php if($garbages->sub_client_mobile_no != ""): ?>
                                              <div class="col-md-12">
                                                <div class="section" style="margin-bottom: 40px;">
                                                  <label for="level_name" class="field-label" style="font-weight:600;" > Sub Client :  </label>  
                                                  
                                                  <div class="col-md-12">
                                                    <label class="option block mn" style="font-size:12px;">
                                                      <?php echo Form::checkbox('sub_client[]',$garbages->sub_client_id,'', array( 'class' => 'check', 'id' => 'framework_2'.$garbages->pessi_id  )); ?>

                                                      <span class="checkbox mn" style="border: 2px solid #4a89dc !important;"></span> <?php echo e($garbages->sub_client_name); ?> ( <?php echo e($garbages->sub_client_mobile_no); ?>)
                                                    </label>
                                                  </div>
                                                </div>
                                                <div class="section"></div>
                                                <div class="clearfix"></div>
                                              </div>
                                              <?php endif; ?>
                                                     
                                              <div class="col-md-12">
                                                <div class="section">
                                                  <label for="level_name" class="field-label" style="font-weight:600;"> Text :  </label>
                                                  <label for="level_name" class="field prepend-icon">
                                                    <?php echo Form::text('reg_sms_text', '' ,array('class' => 'gui-input','id' => 'reg_sms_text'.$garbages->pessi_id )); ?>

                                                      <label for="Account Mobile" class="field-icon">
                                                      <i class="fa fa-pencil"></i>
                                                    </label>                           
                                                  </label>
                                                </div>
                                              </div>
                                           </div>

                                          <div class="panel-footer text-right">

                                              <button type="button" class="button btn-primary mysave" id="maskedKey_daily_<?php echo e($garbages->pessi_id); ?>" onclick="send_sms_daily_new(<?php echo e($garbages->pessi_id); ?>)"> Send </button>
                                              <!-- <?php echo Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')); ?> -->
                                          </div>   


                                          <!-- <script type="text/javascript">
                                            
                                            jQuery(document).ready(function() {

                                              $('#assigned'+<?php echo e($garbages->pessi_id); ?>).multiselect({
                                                nonSelectedText: 'Select Assigned',
                                                enableFiltering: true,
                                                enableCaseInsensitiveFiltering: true,
                                                buttonWidth:'400px'
                                               });

                                              $('#referred'+<?php echo e($garbages->pessi_id); ?>).multiselect({
                                                nonSelectedText: 'Select Referred',
                                                enableFiltering: true,
                                                enableCaseInsensitiveFiltering: true,
                                                buttonWidth:'400px'
                                               });

                                              $('#framework'+<?php echo e($garbages->pessi_id); ?>).multiselect({
                                                nonSelectedText: 'Select Client',
                                                enableFiltering: true,
                                                enableCaseInsensitiveFiltering: true,
                                                buttonWidth:'400px'
                                               });

                                              $('#framework_2'+<?php echo e($garbages->pessi_id); ?>).multiselect({
                                                nonSelectedText: 'Select Sub Client',
                                                enableFiltering: true,
                                                enableCaseInsensitiveFiltering: true,
                                                buttonWidth:'400px'
                                               });

                                              $('#framework_3'+<?php echo e($garbages->pessi_id); ?>).multiselect({
                                                nonSelectedText: 'Select Client',
                                                enableFiltering: true,
                                                enableCaseInsensitiveFiltering: true,
                                                buttonWidth:'400px'
                                               });
                                            });

                                          </script> -->
                                            <?php echo Form::close(); ?>

                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </section>
                              <div class="clearfix"></div>
                            </div> 
                          </div>
                        </div>
                      <!-- </td> -->

                    </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                  </tbody>
                </table>
              </div>
              <?php echo Form::close(); ?>

          </div>
          <div class="panel-body pn">
            <div class="table-responsive">
              <table class="table admin-form theme-warning tc-checkbox-1 fs13">                                
                <tbody>
                  <tr class="">
                     <!-- <th class="text-left">
                      <button type="button" class="btn btn-primary" onclick="go_delete()"><i class="glyphicon glyphicon-trash"></i> Delete Multiple </button>
                    </th> -->
                    <th class="text-right">
                      <?php echo e($get_record->links()); ?>

                    </th>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

<style type="text/css">
.view a{
  color:#fff !important;
}
.padding_less{
  padding-left: 5px !important;
}
</style>
<style type="text/css">
.dt-panelfooter{
  display: none !important;
}

 .close{
  margin-top: -6px !important;
  font-size: 30px !important;
  width: 30px !important;
}
</style>
  
<script type="text/javascript">


  $(document).on("keyup",".fromdate_search_new",function(){
    //  $('.mysave').removeAttr('disabled');

      var dateid = $(this).attr('id');
      var suffix = dateid.match(/\d+/);

      var  currentdate = this.value;
      var n=$(this);    

      var checkdate = /^([0]?[1-9]|[1|2][0-9]|[3][0|1])[-]([0]?[1-9]|[1][0-2])[-]([0-9]{4}|[0-9]{4})$/.test(currentdate);
      
      if(checkdate == true) {

        value=currentdate.split("-");
        var newDate_notice =value[1]+"/"+value[0]+"/"+value[2];
        var notice_status_date = new Date(newDate_notice).getTime();
        
        var notice_issue_date = $('#current_date').val();
        notice_issue_date = notice_issue_date.split("-");
        var newDate_notice_issue = notice_issue_date[1]+"/"+notice_issue_date[0]+"/"+notice_issue_date[2];
        var notice_issue_date = new Date(newDate_notice_issue).getTime();


        if(notice_status_date > notice_issue_date){
          n.next().html("");
          n.next().css("color", "#DE888A");
          n.parent().addClass("state-success");
          $('#maskedKey_'+suffix).removeAttr('disabled');
        } else {
          n.next().html("Date must be greater than current date.");
          n.next().css("color", "#DE888A");
          n.parent().addClass("state-error");
          n.parent().removeClass("state-success");
          $(this).closest('.mysave').html('');
          $('#maskedKey_'+suffix).attr('disabled','disabled');
          return false;
        }

        
      } else {
        n.next().html("Enter date in dd-mm-yyyy format.");
        n.next().css("color", "#DE888A");
        n.parent().addClass("state-error");
        n.parent().removeClass("state-success");
        $(this).closest('.mysave').html('');
        $('#maskedKey_'+suffix).attr('disabled','disabled');
        return false;
      }
  });


  function send_sms_daily_new(id){


    var assigned = $("#assigned"+id).val();
    var referred = $("#referred"+id).val();
    var client = $("#framework"+id).val();
    var sub_clients = $("#framework_2"+id).val();
    var text = $("#reg_sms_text"+id).val();  


    $.ajax({
      type: "POST",
      url: BASE_URL+"/advocate-panel/send-sms-daily?pessi_id="+id+'&assigned='+assigned+'&referred='+referred+'&client='+client+'&sub_clients='+sub_clients+'&sms_text='+text,     
          
      success: function(result) {
        $("#maskedKey_daily_"+id).html('<span class="fa fa-check" aria-hidden="true"></span>  Send'); 
      }
    });


    // var formEl = document.forms.form_daily_sms_+id;

    // alert(formEl);
    // $.ajax({
    //     type: "POST",
    //     url: BASE_URL+"/send-sms-daily",   
    //     data: $('#form_daily_sms_'+id).serialize(),
    //     success: function(result) {
    //       // if(result.status == true){
    //       //     $('#newsletter').bootstrapValidator('resetForm', true);
    //       //     $("#subscribe_submit").html('submit');
    //       //     $("#subcriber_messsage").html('Please check  your email account for verification link.');
    //       // } else {
    //       //     $("#subscribe_submit").html('submit');
    //       //     $('#subscribe_submit').prop('disabled', false);
    //       //     $("#subcriber_messsage").html('You are already registered with this email id.');
    //       // }
    //     }
    //   });

  }


  function update_daily(id){
    
     var stage = $("#reg_stage_"+id).val();
     var order_sheet = $("#order_sheet_"+id).val();
     var further_date = $("#further_date_"+id).val();

     var checkdate = /^([0]?[1-9]|[1|2][0-9]|[3][0|1])[-]([0]?[1-9]|[1][0-2])[-]([0-9]{4}|[0-9]{4})$/.test(further_date);
      
      if(checkdate == true) {
      } else {
        alert("Enter date in dd-mm-yyyy format.");
        return false;
      }

      var choose_type = 0;

     var choose_type_decide = document.getElementById("choose_type_decide_"+id).checked;
      
      if(choose_type_decide == true){
        var choose_type = 1;
      }

      var choose_type_due_course = document.getElementById("choose_type_due_course_"+id).checked;
      if(choose_type_due_course == true){
        var choose_type = 2;
      }

      var choose_type_pending = document.getElementById("choose_type_pending_"+id).checked;
      if(choose_type_pending == true){
        var choose_type = 0;
      }
      

    var decide = $("#decide_"+id).val();
    var sms_text = $("#sms_text_"+id).val();

    $.ajax({
      type: "POST",
      url: BASE_URL+"/advocate-panel/insert-daily-diary?pessi_id="+id+'&stage='+stage+'&order_sheet='+order_sheet+'&further_date='+further_date+'&choose_type='+choose_type+'&decide='+decide+'&sms_text='+sms_text,     
          
      success: function(result) {
        $("#maskedKey_"+id).html('<span class="fa fa-check" aria-hidden="true"></span>  Saved'); 
        alert('Daily Dairy updated successfully !!');
      }
    });

  }



  // sms type

  function reg_sms_type(){

    if ($('#sms_type').is(":checked")){
      $('#show_other_mobile').show();
    } else {
      $('#show_other_mobile').hide();      
    }

  }


  function selected_fields(){


    var new_selected = $("#multiselect2").val();

    if(new_selected == null){
      $('#case_no_hide').css('display','none');
      $('#year_hide').css('display','none');      
      $('#ncv_no_hide').css('display','none');
      $('#stage_hide').css('display','none');
      $('#from_date_hide').css('display','none');
      $('#to_date_hide').css('display','none');
      $('#next_date_hide').css('display','none');
    }    

    var selected = $("#multiselect2").val().toString();

    if(selected != null){
      var values   = selected.split(",");      
    }


    if(selected.includes("1") == true){  
      $('#case_no_hide').css('display','block');  
    } else {  
      $('#case_no_hide').css('display','none');  
    }

    if(selected.includes("2") == true){
      $('#year_hide').css('display','block');
    } else {
      $('#year_hide').css('display','none');
    }

    if(selected.includes("3") == true){
      $('#ncv_no_hide').css('display','block');
    } else {
      $('#ncv_no_hide').css('display','none');
    }

    if(selected.includes("4") == true){
      $('#stage_hide').css('display','block');
    } else {
      $('#stage_hide').css('display','none');
    }

    if(selected.includes("5") == true){
      $('#from_date_hide').css('display','block');
    } else {
      $('#from_date_hide').css('display','none');
    }

    if(selected.includes("6") == true){
      $('#to_date_hide').css('display','block');
    } else {
      $('#to_date_hide').css('display','none');
    }

    if(selected.includes("7") == true){
      $('#next_date_hide').css('display','block');
    } else {
      $('#next_date_hide').css('display','none');
    }


    
    // alert(values[0]);
    // alert(values[1]);
    // alert(values[2]);
    // alert(values[3]);
    // alert(values[4]);
    // alert(values[5]);
    // alert(values[6]);

  }
  
  function show_further(pessi_id){
  //  document.getElementById("show_further_date"+pessi_id).style.display = "block";
    document.getElementById("show_decide_new_"+pessi_id).style.display = "none";
    document.getElementById("show_due_course"+pessi_id).style.display = "none";
  }


  function show_decide(pessi_id){
    document.getElementById("show_decide_new_"+pessi_id).style.display = "block";
    document.getElementById("show_due_course"+pessi_id).style.display = "none";
  //  document.getElementById("show_further_date"+pessi_id).style.display = "none";
  }


  function show_due_course(pessi_id){
  //  document.getElementById("show_further_date"+pessi_id).style.display = "none";
    document.getElementById("show_decide_new_"+pessi_id).style.display = "none";
  //  document.getElementById("show_due_course").style.display = "block";
  }

</script>

<script type="text/javascript">


  function get_client(client_id){

    var new_selected = $("#framework").val();

    var new_new = $("#framework_3").val()


    if(new_selected == null){
      var new_selected = $("#framework_3").val();
    }

    BASE_URL = '<?php echo e(url('/')); ?>';
    $.ajax({
      url:BASE_URL+"/advocate-panel/get-sub-client-ajax/"+new_selected,
      success: function(result){
          $("#sub_client_view").html(result); 
      }
    });
   
  }

  // send sms

  function send_sms_all(sms){
  //  alert(sms);

    BASE_URL = '<?php echo e(url('/')); ?>';
    $.ajax({
      url:BASE_URL+"/advocate-panel/send-sms-client/"+sms,
      success: function(result){
          $('#hide_framework').hide();  
          $("#framework_sms").html(result); 
      }
    });
   
  }


  jQuery(document).ready(function() {

    // $('#assigned').multiselect({
    //   nonSelectedText: 'Select Assigned',
    //   enableFiltering: true,
    //   enableCaseInsensitiveFiltering: true,
    //   buttonWidth:'400px'
    //  });

    // $('#referred').multiselect({
    //   nonSelectedText: 'Select Referred',
    //   enableFiltering: true,
    //   enableCaseInsensitiveFiltering: true,
    //   buttonWidth:'400px'
    //  });

    // $('#framework').multiselect({
    //   nonSelectedText: 'Select Client',
    //   enableFiltering: true,
    //   enableCaseInsensitiveFiltering: true,
    //   buttonWidth:'400px'
    //  });

    // $('#framework_2').multiselect({
    //   nonSelectedText: 'Select Sub Client',
    //   enableFiltering: true,
    //   enableCaseInsensitiveFiltering: true,
    //   buttonWidth:'400px'
    //  });

    // $('#framework_3').multiselect({
    //   nonSelectedText: 'Select Client',
    //   enableFiltering: true,
    //   enableCaseInsensitiveFiltering: true,
    //   buttonWidth:'400px'
    //  });



    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "This field contains alphabets only");

    jQuery.validator.addMethod("checkemail", function(value, element) 
    {
    return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
    }, "Enter a VALID email address"); 

    $.validator.addMethod('minStrict', function (value, el, param) {
     return value > param; 
    });
    $.validator.addMethod('maxStrict', function (value, el, param) {
     return value < param; 
    });

    /* @custom  validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#form_add_pessi").validate({

      /* @validation  states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation  rules 
      ------------------------------------------ */

      rules: {
        // case_no: {
        //   required: true
        // },
        // reg_stage: {
        //   required: true
        // },
        // order_sheet: {
        //   required: true
        // },
        // choose_type: {
        //   required: true
        // },
        // further_date: {
        //   required: true
        // },
        // decide: {
        //   required: true
        // }
      },
      /* @validation  error messages 
      ---------------------------------------------- */

      messages: {
        // reg_stage: {
        //   required: 'Please select stage'
        // },
        // order_sheet: {
        //   required: 'Please enter order sheet'
        // },
        // type: {
        //   required: 'Please Fill Required Type'
        // },
        // further_date: {
        //   required: 'Please select further date'
        // },
        // decide: {
        //   required: 'Please select decide'
        // },
      },

      /* @validation  highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });


  setInterval(function(){ 
    var dt_val = $("#datepicker_further").val();
    if (dt_val!="") {
        $("#discount_date_from_lb").addClass("state-success");
        $("#discount_date_from_lb").removeClass("state-error");
        $(".section_from .state-error").html("");
    }
  }, 500);



  jQuery(document).ready(function() {

    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "This field contains alphabets only");

    jQuery.validator.addMethod("letternumeric", function(value, element) 
    {
    return this.optional(element) || /^[a-z,0-9," "]+$/i.test(value);
    }, "This field contains only alphabets and numeric only");

    jQuery.validator.addMethod("checkemail", function(value, element) 
    {
    return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
    }, "Enter a VALID email address"); 

    $.validator.addMethod('minStrict', function (value, el, param) {
     return value > param; 
    });
    $.validator.addMethod('maxStrict', function (value, el, param) {
     return value < param; 
    });


    /* @custom  validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#form_add_question").validate({

      /* @validation  states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation  rules 
      ------------------------------------------ */

      rules: {
        // case_no: {
        //   required: true
        // },
        // reg_stage: {
        //   required: true
        // },
        // honarable_justice: {
        //   required: true
        // },
        // court_no: {
        //   required: true,
        //   letternumeric: true
        // },
        // serial_no: {
        //   required: true,
        //   letternumeric: true
        // },
        // page_no: {
        //   required: true,
        //   letternumeric: true
        // }
      },
      /* @validation  error messages 
      ---------------------------------------------- */

      messages: {
        // reg_stage: {
        //   required: 'Please select stage'
        // },
        // honarable_justice: {
        //   required: 'Please select honarable justice'
        // },
        // court_no: {
        //   required: 'Please enter court no'
        // },
        // serial_no: {
        //   required: 'Please enter serial no'
        // },
        // page_no: {
        //   required: 'Please enter page no'
        // },
      },

      /* @validation  highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });


  function edit_pessi(pessi_id){
    $('body').css('overflow','hidden');
    $("html, body").animate({ scrollTop: 0 }, "slow");
    document.getElementById('edit_pessi'+pessi_id).style.display='block';
  }

  function close_button_edit_pessi(pessi_id){
    $('body').css('overflow','auto', 'important');
    document.getElementById('edit_pessi'+pessi_id).style.display='none';
  }


  </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('advocate_admin/layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>