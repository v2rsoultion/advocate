<?php $__env->startSection('content'); ?>

  <!-- Start: Content-Wrapper -->
  <section id="content_wrapper">
    <header id="topbar">
      <div class="topbar-left">
        <ol class="breadcrumb">
          <li class="crumb-active">
            <a href="<?php echo e(url('/advocate-panel/add-course-list')); ?>">Add Course List</a>
          </li>
          <li class="crumb-icon">
            <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">
              <span class="glyphicon glyphicon-home"></span>
            </a>
          </li>
          <li class="crumb-link">
            <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">Home</a>
          </li>
          <li class="crumb-trail">View Course List </li>
        </ol>
      </div>
    </header>

    <div class="" style="margin-top:10px;">
      <div class="col-md-12">
        <div class="panel panel-primary panel-border top mb35">  
          <div class="panel-heading">
            <div class="panel-title hidden-xs">
              <span class="glyphicon glyphicon-tasks"></span> View Course List </div>
          </div>

          <div class="panel-menu admin-form theme-primary">
            <div class="row">
              <?php echo Form::open(['url'=>'/advocate-panel/view-course-list']); ?>


                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon">
                    <select class="select2-single form-control" id="case_no" name="case_no">
                      <option value='0'>Select Case No.</option>    
                      <?php $__currentLoopData = $get_case_regestered; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_case_regestereds): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($get_case_regestereds->reg_id); ?>" <?php echo e($case_no == $get_case_regestereds->reg_id ? 'selected="selected"' : ''); ?> > <?php echo e($get_case_regestereds->reg_case_number); ?></option>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                           
                    </select>
                    <i class="arrow double"></i>
                  </label>
                </div>
                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon">
                    <select class="select2-single form-control" id="case_name" name="case_name">
                      <option value='0'>Type Of Case</option>    
                      <?php $__currentLoopData = $type_case; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type_cases): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($type_cases->case_id); ?>" <?php echo e($case_name == $type_cases->case_id ? 'selected="selected"' : ''); ?> > <?php echo e($type_cases->case_name); ?></option>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                           
                    </select>
                    <i class="arrow double"></i>
                  </label>
                </div>
                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon">
                    <select class="select2-single form-control" id="stage_name" name="stage_name">
                      <option value='0'>Select Stage</option>    
                      <?php $__currentLoopData = $get_stage; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_stages): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($get_stages->stage_id); ?>" <?php echo e($stage_name == $get_stages->stage_id ? 'selected="selected"' : ''); ?> > <?php echo e($get_stages->stage_name); ?></option>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                           
                    </select>
                    <i class="arrow double"></i>
                  </label>
                </div>
                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon">
                    <select class="select2-single form-control" id="honarable_justice" name="honarable_justice">
                      <option value='0'>Select Justice</option>    
                      <?php $__currentLoopData = $get_judge; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_judges): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($get_judges->judge_id); ?>" <?php echo e($honarable_justice == $get_judges->judge_id ? 'selected="selected"' : ''); ?> > <?php echo e($get_judges->judge_name); ?></option>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                           
                    </select>
                    <i class="arrow double"></i>
                  </label>
                </div>

                <div class="col-md-2">
                  <label for="services_name" class="field prepend-icon">
                    <?php echo Form::text('previous_date','',array('class' => 'gui-input','placeholder' => 'Registration Date','id'=>'datefuthernew' , readonly )); ?>

                    <label for="blog_title" class="field-icon">
                      <i class="fa fa-calendar"></i>
                    </label>
                  </label>
                </div>


                <div class="col-md-1">
                  <button type="submit" name="search" class="button btn-primary"> Search </button>
                </div>
              <?php echo Form::close(); ?>             
                <div class="col-md-1 pull-right">
                   <a href="<?php echo e(url('/advocate-panel/view-course-list/')); ?>"><?php echo Form::submit('Default', array('class' => 'btn btn-primary', 'id' => 'maskedKey')); ?></a>
                </div>     
            </div>
          </div>

          <div class="panel-body pn">
              <?php echo Form::open(['url'=>'/advocate-panel/view-course-list','name'=>'form']); ?>

              <div class="table-responsive">
                <table class="table admin-form theme-warning tc-checkbox-1 fs13" id="datatable">
                  <thead>
                    <tr class="bg-light">
                      <th style="width:90px !important;" class="text-left">
                        <label class="option block mn">
                          <input type="checkbox" id="check_all"> 
                          <span class="checkbox mn"></span>
                          Select All
                        </label>
                      </th>
                      <th class="">Case Number</th>
                      <th class="">Registration Date</th>
                      <th class="">File No.</th>
                      <th class="">Type of Case</th>
                      <th class="">Title of Case</th>
                      <th class="">Previous Date</th>
                      <th class="">Stage</th>
                      <th class="">Details</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php $__currentLoopData = $get_record; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_records): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>              
                    <tr>
                      <td class="text-left" style="padding-left: 18px;">
                        <label class="option block mn">
                          <input type="checkbox" name="check[]" class="check" value="<?php echo e($get_records->list_id); ?>">
                          <span class="checkbox mn"></span>
                        </label>
                      </td>

                      <td class="text-left" style="padding-left:20px"> <?php echo e($get_records->reg_case_number); ?> </td>
                      <td class="text-left" style="padding-left:20px"> <?php echo e(date('d F Y',strtotime($get_records->reg_date))); ?> </td>
                      <td class="text-left" style="padding-left:20px"> <?php echo e($get_records->reg_file_no); ?> </td>
                      <td class="text-left" style="padding-left:20px">  <?php echo e($get_records->case_name); ?>  </td>
                      <td class="text-left" style="padding-left:20px">  <?php echo e($get_records->reg_respondent); ?> v/s <?php echo e($get_records->reg_petitioner); ?>  </td>
                      <td class="text-left" style="padding-left:20px">  
                        <?php if($get_records->list_previous_date == "1970-01-01"): ?>
                          -----
                        <?php else: ?>
                          <?php echo e(date('d F Y',strtotime($get_records->list_previous_date))); ?>

                        <?php endif; ?>

                      </td>
                      <td class="text-left" style="padding-left:20px">  <?php echo e($get_records->stage_name); ?>  </td>

                      <td class="" style="padding-left:20px"> 
                        <a href="#" style="text-transform: capitalize; text-decoration:none;" data-toggle="modal" data-target="#view_message<?php echo e($get_records->list_id); ?>" > View Details </a>
                        <!-- Sign In model -->
                        <div id="view_message<?php echo e($get_records->list_id); ?>" class="modal fade in" role="dialog">
                          <div class="modal-dialog" style="width:700px; margin-top:100px;">
                            <div class="modal-content">
                              <div class="modal-header" style="padding-bottom: 35px;">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title pull-left"> View Details </h4>
                              </div>
                              <div class="modal-body">
                                <table class="table table-bordered mbn">
                                  <tbody>

                                      <tr>
                                        <td class="text-left"> Honarable Justice </td>
                                        <td> <?php echo e($get_records->judge_name); ?>  </td>
                                      </tr>
                                      <tr>
                                        <td class="text-left"> Court No. </td>
                                        <td> <?php echo e($get_records->list_court_no); ?>  </td>
                                      </tr>
                                      <tr>
                                        <td class="text-left"> Serial No. </td>
                                        <td> <?php echo e($get_records->list_serial_no); ?>  </td>
                                      </tr>
                                      <tr>
                                        <td class="text-left"> Page No. </td>
                                        <td> <?php echo e($get_records->list_page_no); ?>  </td>
                                      </tr>
                                      
                                  </tbody>
                                </table>
                              </div>
                              <div class="clearfix"></div>
                            </div> 
                          </div>
                        </div>
                      </td>

                    </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </tbody>
                </table>
              </div>
              <?php echo Form::close(); ?>

          </div>
          <div class="panel-body pn">
            <div class="table-responsive">
              <table class="table admin-form theme-warning tc-checkbox-1 fs13">                                
                <tbody>
                  <tr class="">
                     <th class="text-left">
                      <button type="button" class="btn btn-primary" onclick="go_delete()"><i class="glyphicon glyphicon-trash"></i> Delete Multiple </button>
                    </th>
                    <th>
                      <?php echo e($get_record->links()); ?>

                    </th>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

<style type="text/css">
.view a{
  color:#fff !important;
}
</style>
<style type="text/css">
.dt-panelfooter{
  display: none !important;
}
</style>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('advocate_admin/layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>