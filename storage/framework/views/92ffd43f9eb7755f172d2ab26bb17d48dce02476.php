<?php $__env->startSection('content'); ?>

<style type="text/css">
  .admin-form .select, .admin-form .gui-input, .admin-form .select > select, .admin-form .select-multiple select{
    height: 28px !important;
  }
  .admin-form a.button, .admin-form span.button, .admin-form label.button
  {
    line-height: 28px !important;
  }
  .admin-form .append-icon .field-icon, .admin-form .prepend-icon .field-icon{
    line-height: 28px !important;
  }
  /*.admin-form .gui-textarea {
    line-height: 7px !important;
  }*/
/*  .gui-textarea{
    height: 100px !important;
  }*/
/* .admin-form .gui-input {
  padding: 5px;
 }*/
 /*.admin-form .prepend-icon .field-icon {
  top: 6px;
 }*/
 .admin-form .button{
    height: 28px !important;
    line-height: 1px !important;
  }
 .btn {
    height: 29px !important;
    line-height: 1px !important;  }

 .down{
  margin-top: 70px;
 }
 .account_setting {
  margin: 0px 20px !important;
  margin-top: 25px !important;
  /*padding: 5px 0px !important;*/
 }

</style>

  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left down">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="<?php echo e(url('/advocate-panel/view-holiday')); ?>">View Holiday</a>
            </li>
            <li class="crumb-icon">
              <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">Home</a>
            </li>
            <li class="crumb-trail">Add Holiday</li>
          </ol>
        </div>
      </header>

      <div class="row">
        <div class="col-md-12">
          <?php if(\Session::has('success')): ?>
          <div class="alert alert-success account_setting" style="margin: 20px 25px;">
            <?php echo \Session::get('success'); ?>

          </div>
          <?php endif; ?>
        </div>
      </div>

      <!-- Begin: Content -->

      <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center">
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-heading">
                <span class="panel-title"> Add Holiday </span>
              </div>
              <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                
                <!-- IF any error found -->
                <!-- <div class="col-md-12">
                  <?php if(\Session::has('danger')): ?>
                    <div class="alert alert-danger account_setting">
                      <?php echo \Session::get('danger'); ?>

                    </div>
                  <?php endif; ?>
                </div> -->
                
                <!-- IF any error found -->
                  <div id="tab1_1" class="tab-pane active">
                    <?php echo Form::open(['url'=>'advocate-panel/insert-holiday/'.$get_record->holiday_id,'id'=>'validation' ,  'enctype' => 'multipart/form-data' ]); ?>

                    <div class="row"> 
                      <div class="col-md-12">
                      
                        <div class="col-md-6">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Title :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              <?php echo Form::text('holiday_title',$get_record->holiday_title,array('class' => 'gui-input','placeholder' => '' )); ?>

                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-pencil"></i>
                              </label>                            
                            </label>
                          </div>
                        <!-- </div> -->
                        <!-- <div class="col-md-6"> -->
                          <div class="section section_from">
                            <label for="level_name" class="field-label" style="font-weight:600;">Date :  </label>  
                            <label for="level_name" class="field prepend-icon fromdate">

                            <?php if($get_record->holiday_date != ""): ?>
                              <?php echo Form::text('holiday_date',date('d F Y',strtotime($get_record->holiday_date)) ,array('class' => 'gui-input fromdate','placeholder' => '','id'=>'' , readonly )); ?>

                            <?php else: ?> 
                              <?php echo Form::text('holiday_date','',array('class' => 'gui-input fromdate','placeholder' => '','id'=>'' , readonly )); ?>

                            <?php endif; ?>
                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-calendar"></i>
                              </label>                           
                            </label>
                          </div>
                        </div>
                       
        

                        <div class="col-md-6">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Description :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              <?php echo Form::textarea('holiday_description',$get_record->holiday_description,array('class' => 'gui-textarea accc1','placeholder' => '' )); ?>

                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-map-marker"></i>
                              </label>                           
                            </label>
                          </div>
                        </div>


                          
                        
                      </div>      
                    </div>
              
                  <div class="panel-footer text-right">
                    <?php echo Form::submit('Save', array('class' => 'button btn-primary', 'id' => 'maskedKey')); ?>

                    <?php echo Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')); ?>

                  </div>
                  <?php echo Form::close(); ?>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


<script type="text/javascript">

  jQuery(document).ready(function() {
  
    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "This field contains alphabets only");

    jQuery.validator.addMethod("checkemail", function(value, element) 
    {
    return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
    }, "Enter a VALID email address"); 

    $.validator.addMethod('minStrict', function (value, el, param) {
     return value > param; 
    });
    $.validator.addMethod('maxStrict', function (value, el, param) {
     return value < param; 
    });


    /* @custom  validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#validation").validate({

      /* @validation  states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation  rules 
      ------------------------------------------ */

      rules: {
        // holiday_title: {
        //   required: true
        // },
        // holiday_date: {
        //   required: true
        // },
        // number: {
        //   required: true,
        //   minlength: 10,
        //   maxlength: 10,
        // },
        // holiday_description: {
        //   required: true
        // },
        // confirm_password: {
        //   required: true,
        //   minlength: 6,
        //   equalTo: '#password'
        // },
        // address: {  
        //   required: true,
        // },
        // start_date: {
        //   required: true
        // },
        // end_date: {
        //   required: true
        // },
        // admin_image: {
        // //  required: true,
          // extension: 'jpeg,jpg,png',
        //   type: 'image/jpeg,image/png',
        // //  maxSize: 2097152,   // 2048 * 1024
       //    message: 'The selected file is not valid'
         // },
      },

      /* @validation  error messages 
      ---------------------------------------------- */

      messages: {
        holiday_title: {
          required: 'Please enter title for holiday'
        },
        holiday_date: {
          required: 'Please select date'
        },
        // address: {
        //   required: 'Please enter address'
        // },
        holiday_description: {
          required: 'Please Fill Description'
        },
        end_date: {
          required: 'Please select end date'
        },
        // number: {
        //   required: 'Please enter mobile no',
        //   minlength:'Enter at least 10 digit phone numbers',
        //   maxlength:'Enter 10 digit phone numbers',
        // },        
        admin_image: {
          extension: 'Image Should be in .jpg,.jpeg and .png format only'
        },
        password:{
          required: 'Please enter password',
          minlength:'Please enter at least 6 characters'
        },
        confirm_password: {
          required: 'Please confirm your password, again',
          equalTo: 'New Password and Confirm Password do not match. Please try again !'
        },
      },

      /* @validation  highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });

  </script>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('advocate_admin/layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>