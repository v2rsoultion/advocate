<?php $__env->startSection('content'); ?>
<style type="text/css">
  /*.admin-form .select, .admin-form .gui-input, .admin-form .select > select, .admin-form .select-multiple select{
    height: 28px !important;
  }*/
  .admin-form .append-icon .field-icon, .admin-form .prepend-icon .field-icon{
    line-height: 28px !important;
  }
  .admin-form .gui-textarea {
    line-height: 7px !important;
  }

  .form-control {
    height: 28px !important;
    /*padding: 0px 12px !important;*/
  }
  .admin-form .button{
    height: 28px !important;
    line-height: 1px !important;
  }
  .btn {
    height: 29px !important;
    line-height: 1px !important;  }

 .down{
  margin-top: 70px;
 }
.select-text {
  padding: 0px 12px !important;
}
 .close{
  margin-top: -6px !important;
  font-size: 30px !important;
  width: 30px !important;
}

</style>
  <!-- Start: Content-Wrapper -->
  <section id="content_wrapper">
    <header id="topbar">
      <div class="topbar-left down">
        <ol class="breadcrumb">
          <li class="crumb-active">
            <a href="<?php echo e(url('/advocate-panel/add-compliance')); ?>">Add Compliance</a>
          </li>
          <li class="crumb-icon">
            <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">
              <span class="glyphicon glyphicon-home"></span>
            </a>
          </li>
          <li class="crumb-link">
            <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">Home</a>
          </li>
          <li class="crumb-trail">View Compliance</li>
        </ol>
      </div>
    </header>

    <div class="" style="margin-top:10px;">
      <div class="col-md-12">
        <div class="panel panel-primary panel-border top mb70">  
          <div class="panel-heading">
            <div class="panel-title hidden-xs">
              <span class="glyphicon glyphicon-tasks"></span>View Compliance</div>
          </div>

          <div class="panel-menu admin-form theme-primary" style="padding: 5px 20px;">
            <?php echo Form::open(['url'=>'/advocate-panel/view-compliance' ,'autocomplete'=>'off']); ?>

            
              <div class="row">
              

                <div class="col-md-2">
                  <div class="section" style="margin-top:5px !important;">
                      <label for="level" class="field prepend-icon">
                      <select class="form-control select-text" name="court_type" onchange="choose_court(this.value)" id="court_type" style="color: black;">
                        <option value="">Court Type</option>
                        <option value="1" <?php echo e($court_type == 1 ? 'selected="selected"' : ''); ?> >Trial Court</option>
                        <option value="2" <?php echo e($court_type == 2 ? 'selected="selected"' : ''); ?> >High Court</option>
                      </select>
                      <i class="arrow double"></i>
                    </label>
                  </div>
                </div>

                <div class="col-md-2">
                  <div class="section" style="margin-top:5px !important;">
                      <label for="level" class="field prepend-icon">
                      <select class="form-control select-text" name="court_name" onchange="choose_court_name(this.value)" id="court_name" style="color: black;">
                        <option value="">Court Name</option>
                          
                          <?php $__currentLoopData = $court_all; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $court_alls): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                            <option value="<?php echo e($court_alls->court_id); ?>" <?php echo e($court_alls->court_id == $court_name ? 'selected="selected"' : ''); ?> > <?php echo e($court_alls->court_name); ?> </option>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                      </select>
                      <i class="arrow double"></i>
                    </label>
                  </div>
                </div>

                <div class="col-md-2">
                  <div class="section" style="margin-top:5px !important;">
                    <!-- <label for="artist_state" class="field select"> -->
                      <label for="level" class="field prepend-icon">
                      <select class="form-control select-text" name="type" onclick="get_type_of_case(this.value)" style="color: black;">
                        <option value=''>Select Case Type</option> 
                        
                        <?php $__currentLoopData = $case_type_entry; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $case_type_entrys): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                        <option value="<?php echo e($case_type_entrys->case_id); ?>" <?php echo e($case_type_entrys->case_id == $reg_case_type_id ? 'selected="selected"' : ''); ?> ><?php echo e($case_type_entrys->case_name); ?> </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        
                      </select>
                      <i class="arrow double"></i>
                    </label>
                  </div>
                </div>

                <div class="col-md-2">
                  <div class="section" style="margin-top:5px !important;">
                    <!-- <label for="artist_state" class="field select"> -->
                      <label for="level" class="field prepend-icon">
                      <select class="form-control select-text" name="case_no" id="case_no" style="color: black;" onchange="choose_case_no(this.value)">
                        <option value=''>Select Case No.</option> 
                        
                        <?php $__currentLoopData = $get_case_regestered; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_case_regestereds): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                          <?php if($get_case_regestereds->reg_case_number != ""): ?>
                            <option value="<?php echo e($get_case_regestereds->reg_id); ?>" <?php echo e($get_case_regestereds->reg_id == $case_no ? 'selected="selected"' : ''); ?> > <?php echo e($get_case_regestereds->reg_case_number); ?></option>
                          <?php endif; ?>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        
                      </select>
                      <i class="arrow double"></i>
                    </label>
                  </div>
                </div>

                <div class="col-md-2">
                  <div class="section" style="margin-top:5px !important;">
                    <!-- <label for="artist_state" class="field select"> -->
                      <label for="level" class="field prepend-icon">
                      <select class="form-control select-text" name="reg_file_no" id="reg_file_no" style="color: black;">
                        <option value=''>Select File No.</option> 
                        
                        <?php $__currentLoopData = $get_case_regestered; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_case_regestereds): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                          <?php if($get_case_regestereds->reg_file_no != ""): ?>
                            <option value="<?php echo e($get_case_regestereds->reg_id); ?>" <?php echo e($get_case_regestereds->reg_id == $file_no ? 'selected="selected"' : ''); ?> > <?php echo e($get_case_regestereds->reg_file_no); ?></option>
                          <?php endif; ?>
                          
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        
                      </select>
                      <i class="arrow double"></i>
                    </label>
                  </div>
                </div>

                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 5px;">
                    <?php echo Form::text('respondent','',array('class' => 'form-control','placeholder' => 'Resp. Name', 'autocomplete' => 'off' )); ?>

                    <label for="pincode" class="field-icon">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 5px;">
                    <?php echo Form::text('petitioner_name','',array('class' => 'form-control','placeholder' => 'Pet. Name', 'autocomplete' => 'off' )); ?>

                    <label for="pincode" class="field-icon">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>

                <div class="col-md-2">
                  <div class="section" style="margin-top:5px !important;">
                    <!-- <label for="artist_state" class="field select"> -->
                      <label for="level" class="field prepend-icon">
                      <select class="form-control select-text" name="compliance_type" style="color: black;">
                        <option value=''>Compliance Type</option> 
                        <option value='1' <?php echo e($compliance_type_notice == 1 ? 'selected="selected"' : ''); ?> > Notice Issue </option>
                        <option value='2' <?php echo e($compliance_type_date == 2 ? 'selected="selected"' : ''); ?> > Date </option>
                        <option value='3' <?php echo e($compliance_type_defect_case == 3 ? 'selected="selected"' : ''); ?> > Defect Case </option>
                        <option value='4' <?php echo e($compliance_type_reply == 4 ? 'selected="selected"' : ''); ?> > Reply </option>
                        <option value='5' <?php echo e($compliance_type_certified == 5 ? 'selected="selected"' : ''); ?> > Apply Date </option>
                        <option value='6' <?php echo e($compliance_type_other == 6 ? 'selected="selected"' : ''); ?> > Other </option>
                        <option value='7' <?php echo e($compliance_type_other == 7 ? 'selected="selected"' : ''); ?> > All </option>
                      </select>

                      <i class="arrow double"></i>
                    </label>
                  </div>
                </div>

                <div class="col-md-2">
                  <div class="section" >
                    <!-- <label for="artist_state" class="field select"> -->
                      <label for="level" class="field prepend-icon">
                      <select class="form-control select-text" name="compliance_status" style="color: black;">
                        <option value=''> Select Status </option>
                        <option value='1' <?php echo e($compliance_status == 1 ? 'selected="selected"' : ''); ?> > Pending </option>
                        <option value='2' <?php echo e($compliance_status == 2 ? 'selected="selected"' : ''); ?> > Complete </option>
                      </select>

                      <i class="arrow double"></i>
                    </label>
                  </div>
                </div>

                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon">
                    <?php echo Form::text('compliance_from','',array('class' => 'form-control fromdate_search','placeholder' => 'Compliance From', 'autocomplete' => 'off' )); ?>

                    <label for="pincode" class="field-icon">
                      <i class="fa fa-calendar"></i>
                    </label>
                  </label>
                </div>

                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon">
                    <?php echo Form::text('compliance_to','',array('class' => 'form-control todate','placeholder' => 'Compliance To', 'autocomplete' => 'off' )); ?>

                    <label for="pincode" class="field-icon">
                      <i class="fa fa-calendar"></i>
                    </label>
                  </label>
                </div>

                <div class="col-md-1  pull-right mr15">
                  <button type="submit" name="search" class="button btn-primary"> Search </button>
                </div>
                <div class="clearfix"></div>

                <div class="col-md-1 mb10">
                   <a href="<?php echo e(url('/advocate-panel/view-compliance/')); ?>"><?php echo Form::button('Default', array('class' => 'btn btn-primary', 'id' => 'maskedKey')); ?></a>
                </div>

                <div  class="col-md-2 mb10">
                  <a href="<?php echo e(url('advocate-panel/download-compliance/xls')); ?>?&court_type=<?php echo e($court_type); ?>&court_name=<?php echo e($court_name); ?>&regg_case_type_id=<?php echo e($reg_case_type_id); ?>&case_no=<?php echo e($case_no); ?>&file_no=<?php echo e($file_no); ?>&compliance_type_notice=<?php echo e($compliance_type_notice); ?>&respondent=<?php echo e($respondent); ?>&petitioner_name=<?php echo e($petitioner_name); ?>&compliance_type_date=<?php echo e($compliance_type_date); ?>&compliance_type_defect_case=<?php echo e($compliance_type_defect_case); ?>&compliance_type_reply=<?php echo e($compliance_type_reply); ?>&compliance_type_certified=<?php echo e($compliance_type_certified); ?>&compliance_type_other=<?php echo e($compliance_type_other); ?>&compliance_status=<?php echo e($compliance_status); ?>&compliance_from=<?php echo e($compliance_from); ?>&compliance_to=<?php echo e($compliance_to); ?>"><button type="button" class="btn btn-primary"> Download All </button></a>
                </div>

                <div class="col-md-2 mb10">
                   <a href="<?php echo e(url('/advocate-panel/view-compliance/all')); ?>"><?php echo Form::button('Show All Records', array('class' => 'btn btn-primary', 'id' => 'maskedKey')); ?></a>
                </div>

              </div>

            <?php echo Form::close(); ?>  
          </div>

          <div class="panel-body pn">
              <?php echo Form::open(['url'=>'/advocate-panel/view-compliance','name'=>'form' ,'autocomplete'=>'off']); ?>

              <div class="table-responsive">
                <table class="table admin-form table-bordered table-striped theme-warning tc-checkbox-1 fs13" id="datatable">
                  <thead>
                    <tr class="bg-light">
                      <th style="width:90px !important;" class="text-left">
                        <label class="option block mn" style="width:90px !important;">
                          <input type="checkbox" id="check_all"> 
                          <span class="checkbox mn"></span>
                          Select All
                        </label>
                      </th>
                      <!-- <th class="">Category</th> -->
                      <th class="">Court Name</th>
                      <th class="">Type of Case</th>
                      <th class="">Case No</th>
<!--                       <th class="">File Number</th> -->
                      <th class="">Title of Case</th>
                      <th class="w200">List</th>
                      <!-- <th class="">Created</th> -->
                      <!-- <th class="">View Compliance</th> -->
                      
                      <!-- <th class="">Notice Issue</th> -->
                      <!-- <th class="">Date</th> -->
                      <!-- <th class="">Defect Case</th>
                      <th class="">Reply</th>
                      <th class="">Apply Date</th>
                      <th class="">Other Complaince</th> -->
                      <th class="">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php $__currentLoopData = $get_record; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_records): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>               
                    <tr>
                      <td class="text-left" style="padding-left: 18px;">
                        <label class="option block mn">
                          <input type="checkbox" name="check[]" class="check" value="<?php echo e($get_records->compliance_id); ?>">
                          <span class="checkbox mn"></span>
                        </label>
                      </td>
                      
                      <!-- <td class="text-left" style="padding-left:20px">
                        <?php if($get_records->compliance_category == 1): ?>
                               Criminal
                        <?php elseif($get_records->compliance_category == 2): ?>
                               Civil
                        <?php endif; ?>
                      </td> -->

                      <td class="text-left" style="padding-left: 20px;"> 
                        <?php if($get_records->court_name != ""): ?> <?php echo e($get_records->court_name); ?> <?php else: ?> ------- <?php endif; ?>
                      </td>
                      <td class="text-left" style="padding-left:20px">
                        <?php if($get_records->case_name != ""): ?> <?php echo e($get_records->case_name); ?> <?php else: ?> ------- <?php endif; ?>
                      </td>
                      <td class="text-left" style="padding-left:20px">
                        <?php if($get_records->reg_case_number != ""): ?> <?php echo e($get_records->reg_case_number); ?> <?php else: ?> ------- <?php endif; ?>
                      </td>

                      <!-- <td class="text-left" style="padding-left: 20px;"> <?php if($get_records->reg_file_no == ""): ?> ----- <?php else: ?> <?php echo e($get_records->reg_file_no); ?> <?php endif; ?> </td> -->

                      <td class="text-left" style="padding-left:20px">                        
                        <?php echo e($get_records->reg_petitioner); ?> v/s <?php echo e($get_records->reg_respondent); ?>

                      </td>

                      <td class="text-left" style="padding-left:20px">
                        <?php if($get_records->compliance_type_notice == 1): ?> Notice Details ,   <?php endif; ?>

                        <?php if($get_records->compliance_type_date == 1): ?> Date , <?php endif; ?>
                               
                        <?php if($get_records->compliance_type_defect_case == 1): ?> Defect Case , <?php endif; ?>
                              
                        <?php if($get_records->compliance_type_reply == 1): ?> Reply ,  <?php endif; ?>

                        <?php if($get_records->compliance_type_certified == 1): ?> Apply Date ,  <?php endif; ?>

                        <?php if($get_records->compliance_type_other == 1): ?> Other  <?php endif; ?>

                        <?php if($get_records->compliance_type_notice != 1 && $get_records->compliance_type_date != 1 && $get_records->compliance_type_defect_case != 1 && $get_records->compliance_type_reply != 1 && $get_records->compliance_type_certified != 1 && $get_records->compliance_type_other != 1): ?> -----  <?php endif; ?>

                      </td>
                      
                      <!-- <td class="text-left"><?php echo e(date('d F Y',strtotime($get_records->compliance_created))); ?></td> -->

                      <td class="text-right">
                        <div class="btn-group text-left">
                          <button type="button" class="btn btn-success  br2 btn-xs fs12 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> Edit
                            <span class="caret ml5"></span>
                          </button>
                          <ul class="dropdown-menu" role="menu" style="min-width:130px; left: -80px !important;">
                            <li>
                              <a href="<?php echo e(url('/advocate-panel/add-compliance')); ?>/<?php echo e(sha1($get_records->compliance_id)); ?>">Edit</a>
                            </li>

                            <?php if($get_records->compliance_type_notice != 1 && $get_records->compliance_type_date != 1 && $get_records->compliance_type_defect_case != 1 && $get_records->compliance_type_reply != 1 && $get_records->compliance_type_certified != 1 && $get_records->compliance_type_other != 1): ?>

                            <?php else: ?>

                            <li onclick="function_view_compliance(<?php echo e($get_records->compliance_id); ?>)">
                              <a href="#"> View Compliance</a>
                            </li>

                            <div id="view_compliance<?php echo e($get_records->compliance_id); ?>" class="modal fade in" role="dialog" style="overflow: scroll;">
                              <div class="modal-dialog" style="width:500px; margin-top:80px;">
                                <div class="modal-content">
                                  <div class="modal-header" style="padding-bottom: 35px;">
                                    <button type="button" class="close" data-dismiss="modal" onclick="close_button_compliance(<?php echo e($get_records->compliance_id); ?>)"  >&times;</button>
                                      <h4 class="modal-title pull-left">
                                        View Compliance
                                      </h4>
                                  </div>

                                  <?php if($get_records->compliance_type_notice == 1): ?> 
                                    <div class="modal-header" style="padding-bottom: 35px;">
                                        <h4 class="modal-title pull-left">  Notice Details  </h4>
                                    </div>

                                    <div class="modal-body">
                                      <table class="table table-bordered mbn">
                                        <thead>
                                          <tr class="bg-light">
                                            <th class="text-left" style="padding-left: 10px !important;">Issue Date</th>
                                            <th class="text-left" style="padding-left: 10px !important;">Status</th>
                                          </tr>
                                        </thead>
                                        <tbody>            
                                          <tr class="">
                                            <td class="text-left"> <?php if($get_records->notice_issue_date != "1970-01-01"): ?> <?php echo e(date('d F Y',strtotime($get_records->notice_issue_date))); ?> <?php else: ?> ----- <?php endif; ?></td>
                                            <td class="text-left"><?php if($get_records->notice_status == 1): ?> Pending <?php elseif($get_records->notice_status == 2): ?>  Complete ( <?php echo e(date('d F Y',strtotime($get_records->notice_status_date))); ?> ) <?php else: ?> ----- <?php endif; ?> </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </div>
                                  <?php endif; ?>
                                  <div class="clearfix"></div>

                                  <?php if($get_records->compliance_type_date == 1): ?> 
                                    <div class="divider" style="margin: 0px;"></div>
                                    <div class="modal-header" style="padding-bottom: 35px;">
                                        <h4 class="modal-title pull-left">  Date  </h4>
                                    </div>

                                    <div class="modal-body">
                                      <table class="table table-bordered mbn">
                                        <thead>
                                          <tr class="bg-light">
                                            <th class="text-left" style="padding-left: 10px !important;">Date Type</th>
                                            <th class="text-left" style="padding-left: 10px !important;">Date</th>
                                            <th class="text-left" style="padding-left: 10px !important;">Status</th>
                                          </tr>
                                        </thead>
                                        <tbody>            
                                          <tr class="">
                                            <td class="text-left"> <?php if($get_records->compliance_date_type == 1): ?> Short Date <?php else: ?> Long Date <?php endif; ?></td>
                                            
                                            <td class="text-left"> <?php if($get_records->compliance_date != "1970-01-01"): ?> <?php echo e(date('d F Y',strtotime($get_records->compliance_date))); ?> <?php else: ?> ----- <?php endif; ?> </td>

                                            <td class="text-left"> <?php if($get_records->compliance_status == 1): ?> Pending <?php elseif($get_records->compliance_status == 2): ?> Complete ( <?php echo e(date('d F Y',strtotime($get_records->compliance_date_status))); ?> ) <?php else: ?> ----- <?php endif; ?> </td>
                                          
                                          </tr>
                                        </tbody>
                                      </table>
                                    </div>
                                  <?php endif; ?>

                                  <?php if($get_records->compliance_type_defect_case == 1): ?> 
                                    <div class="divider" style="margin: 0px;"></div>
                                    <div class="modal-header" style="padding-bottom: 35px;">
                                        <h4 class="modal-title pull-left">  Defect Case  </h4>
                                    </div>

                                    <div class="modal-body">
                                      <table class="table table-bordered mbn">
                                        <thead>
                                          <tr class="bg-light">
                                            <!-- <th class="text-left" style="padding-left: 10px !important;">Act Reason</th> -->
                                            <th class="text-left" style="padding-left: 10px !important;">Status</th>
                                          </tr>
                                        </thead>
                                        <tbody>            
                                          <tr class="">
                                            <!-- <td class="text-left"> <?php echo e($get_records->compliance_act_reason); ?> </td> -->
                                            <td class="text-left"><?php if($get_records->compliance_defect_status == 1): ?> Pending <?php elseif($get_records->compliance_defect_status == 2): ?> Complete ( <?php echo e(date('d F Y',strtotime($get_records->compliance_defect_date_status))); ?> ) <?php else: ?> ----- <?php endif; ?> </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </div>
                                  <?php endif; ?>

                                  <?php if($get_records->compliance_type_reply == 1): ?> 
                                    <div class="divider" style="margin: 0px;"></div>
                                    <div class="modal-header" style="padding-bottom: 35px;">
                                        <h4 class="modal-title pull-left">  Reply  </h4>
                                    </div>

                                    <div class="modal-body">
                                      <table class="table table-bordered mbn">
                                        <thead>
                                          <tr class="bg-light">
                                            <th class="text-left" style="padding-left: 10px !important;">Reply</th>
                                            <th class="text-left" style="padding-left: 10px !important;">Status</th>
                                          </tr>
                                        </thead>
                                        <tbody>            
                                          <tr class="">
                                            <td class="text-left"> <?php if($get_records->compliance_reply != ""): ?> <?php echo e($get_records->compliance_reply); ?> <?php else: ?> ----- <?php endif; ?> </td>
                                            <td class="text-left"><?php if($get_records->compliance_reply_status == 1): ?> Pending <?php elseif($get_records->compliance_reply_status == 2): ?> Complete ( <?php echo e(date('d F Y',strtotime($get_records->compliance_reply_date_status))); ?> ) <?php else: ?> ----- <?php endif; ?> </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </div>
                                  <?php endif; ?>

                                  <?php if($get_records->compliance_type_certified == 1): ?> 
                                    <div class="divider" style="margin: 0px;"></div>
                                    <div class="modal-header" style="padding-bottom: 35px;">
                                        <h4 class="modal-title pull-left">  Apply Date  </h4>
                                    </div>

                                    <div class="modal-body">
                                      <table class="table table-bordered mbn">
                                        <thead>
                                          <tr class="bg-light">
                                            <th class="text-left" style="padding-left: 10px !important;">Apply Date</th>
                                            <th class="text-left" style="padding-left: 10px !important;">Status</th>
                                          </tr>
                                        </thead>
                                        <tbody>            
                                          <tr class="">
                                            <td class="text-left"><?php if($get_records->certified_copy_date != "1970-01-01"): ?> <?php echo e(date('d F Y',strtotime($get_records->certified_copy_date))); ?> <?php else: ?> ----- <?php endif; ?></td>
                                            <td class="text-left"><?php if($get_records->certified_copy_status == 1): ?> Pending <?php elseif($get_records->certified_copy_status == 2): ?> Complete ( <?php echo e(date('d F Y',strtotime($get_records->certified_copy_date_status))); ?> ) <?php else: ?> ----- <?php endif; ?> </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </div>
                                  <?php endif; ?>

                                  <?php if($get_records->compliance_type_other == 1): ?> 
                                    <div class="divider" style="margin: 0px;"></div>
                                    <div class="modal-header" style="padding-bottom: 35px;">
                                        <h4 class="modal-title pull-left">  Other  </h4>
                                    </div>

                                    <div class="modal-body">
                                      <table class="table table-bordered mbn">
                                        <thead>
                                          <tr class="bg-light">
                                            <th class="text-left" style="padding-left: 10px !important;">Other</th>
                                            <th class="text-left" style="padding-left: 10px !important;">Status</th>
                                          </tr>
                                        </thead>
                                        <tbody>            
                                          <tr class="">
                                            <td class="text-left"> <?php if($get_records->compliance_other != ""): ?> <?php echo e($get_records->compliance_other); ?><?php else: ?> ----- <?php endif; ?></td>
                                            <td class="text-left"> <?php if($get_records->compliance_other_status == 1): ?> Pending <?php elseif($get_records->compliance_other_status == 2): ?> Complete ( <?php echo e(date('d F Y',strtotime($get_records->compliance_other_date_status))); ?> ) <?php else: ?> ----- <?php endif; ?> </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </div>
                                  <?php endif; ?>

                                </div> 
                              </div> 
                            </div>

                            <?php endif; ?>

                            
                            
                          </ul>
                        </div>
                      </td>

                      

                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                 
                  </tbody>
                </table>
              </div>
              <?php echo Form::close(); ?>

          </div>



<div class="modal fade" id="myModal" role="dialog" style="overflow: scroll;">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
          <div class="panel-body pn">
            <div class="table-responsive">
              <table class="table admin-form theme-warning tc-checkbox-1 fs13">                                
                <tbody>
                  <tr class="">
                     <th class="text-left">
                      <button type="button" class="btn btn-primary" onclick="go_delete()"><i class="glyphicon glyphicon-trash"></i> Delete Multiple </button>
                    </th>
                    <th>
                      <?php echo e($get_record->links()); ?>

                    </th>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


  <script type="text/javascript">
    
      function choose_court(court_type) { 
    BASE_URL = '<?php echo e(url('/')); ?>';
      $.ajax({
        url:BASE_URL+"/advocate-panel/compliance-court-type/"+court_type,
        success: function(result){
            $("#court_name").html(result);
        }
      });
  }
  
     function get_type_of_case(compliance_category){

    BASE_URL = '<?php echo e(url('/')); ?>';
      $.ajax({
        url:BASE_URL+"/advocate-panel/get-case-registeration-category/"+compliance_category,
        success: function(result){
            $("#case_no").html(result);
 //           $("#reg_file_no").html(result);
        }
      });
  }


  function choose_case_no(case_no) { 

   // if(case_no != ""){

    BASE_URL = '<?php echo e(url('/')); ?>';

      $.ajax({
        url:BASE_URL+"/advocate-panel/ajax-case-no/"+case_no,
        success: function(result){
            $("#reg_file_no").html(result);
        }
      });
   // }
  }


  // function function_view_compliance(compliance_id){
  //   $('#view_compliance'+compliance_id).modal('show');    
  // }


  function function_view_compliance(compliance_id){
    $('body').css('overflow','hidden');
    $("html, body").animate({ scrollTop: 0 }, "slow");
    document.getElementById('view_compliance'+compliance_id).style.display='block';
  }

  function close_button_compliance(compliance_id){
    $('body').css('overflow','auto', 'important');
    document.getElementById('view_compliance'+compliance_id).style.display='none';
  }


  jQuery(document).ready(function() {

    "use strict";

    $('#datatable').dataTable({
      "aoColumnDefs": [{
        'bSortable': false,
        'aTargets': [-1]
      }],
      "oLanguage": {
        "oPaginate": {
          "sPrevious": "",
          "sNext": ""
        }
      },
      "iDisplayLength": -1,
      
      "sDom": '<"dt-panelmenu clearfix"lfr>t<"dt-panelfooter clearfix"ip>',
      "oTableTools": {
        "sSwfPath": "vendor/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
      }
    });

    $('.dataTables_filter input').attr("placeholder", "Enter Terms...");
    $('div.dataTables_filter input').focus();
    $('#datatable_length').hide();

  });

  </script>


<style type="text/css">
.dt-panelfooter{
  display: none !important;
}

 .close{
  margin-top: -6px !important;
  font-size: 30px !important;
  width: 30px !important;
}
</style>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('advocate_admin/layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>