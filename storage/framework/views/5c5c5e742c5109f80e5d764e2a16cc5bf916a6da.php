<div class="section">
<label for="level_name" class="field-label" style="font-weight:600;" > Client : </label>
  <label for="artist_state" class="field">
    <select id="framework_3" name="client[]" multiple class="form-control" onchange="get_client(this.value)" >
        <?php $__currentLoopData = $client; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $clients): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <option value="<?php echo e($clients->cl_id); ?>"> <?php echo e($clients->cl_group_name); ?> <?php if($clients->cl_father_name != ""): ?> <?php echo e($clients->cl_name_prefix); ?> <?php echo e($clients->cl_father_name); ?> <?php endif; ?>  <?php if($clients->cl_group_email_id != ""): ?> (<?php echo e($clients->cl_group_email_id); ?>) <?php endif; ?> </option>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>
    <i class="arrow double"></i>
  </label>
</div>


<script type="text/javascript">
	

	$('#framework_3').multiselect({
      nonSelectedText: 'Select Client',
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
      buttonWidth:'400px'
     });
	
</script>
