<?php $__env->startSection('content'); ?>
<style type="text/css">
  .admin-form .select, .admin-form .gui-input, .admin-form .select > select, .admin-form .select-multiple select{
    height: 28px !important;
  }
  .admin-form a.button, .admin-form span.button, .admin-form label.button
  {
    line-height: 28px !important;
  }
  .admin-form .append-icon .field-icon, .admin-form .prepend-icon .field-icon{
    line-height: 28px !important;
  }
/*  .admin-form .gui-textarea {
    line-height: 7px !important;
  }*/
/*  .gui-textarea{
    height: 100px !important;
  }*/
/* .admin-form .gui-input {
  padding: 5px;
 }*/
 /*.admin-form .prepend-icon .field-icon {
  top: 6px;
 }*/
 .admin-form .button{
    height: 28px !important;
    line-height: 1px !important;
  }
 .btn {
    height: 29px !important;
    line-height: 1px !important;  }

 .down{
  margin-top: 70px;
 }
.account_setting {
  margin: 0px 20px !important;
  margin-top: 25px !important;
  /*padding: 5px 0px !important;*/
 }
</style>



  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left down">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="<?php echo e(url('/advocate-panel/view-advocate')); ?>">View Advocate</a>
            </li>
            <li class="crumb-icon">
              <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">Home</a>
            </li>
            <li class="crumb-trail">Add Advocate</li>
          </ol>
        </div>
      </header>
      <div class="row">
        <div class="col-md-12">
          <?php if(\Session::has('success')): ?>
          <div class="alert alert-success account_setting" >
            <?php echo \Session::get('success'); ?>

          </div>
          <?php endif; ?>
        </div>
      </div>

      <!-- Begin: Content -->

      <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center">
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-heading">
                <span class="panel-title"> Add Advocate </span>
              </div>
              <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                
                <!-- IF any error found -->

               <!--  <div class="col-md-12">
                  <?php if(\Session::has('success')): ?>
                    <div class="alert alert-danger">
                      <?php echo \Session::get('success'); ?>

                    </div>
                  <?php endif; ?>
                </div> -->
                
                <!-- IF any error found -->
                  <div id="tab1_1" class="tab-pane active">
                    <?php echo Form::open(['url'=>'advocate-panel/insert-advocate/'.$get_record->admin_id,'id'=>'validation' ,  'enctype' => 'multipart/form-data' ,'autocomplete'=>'off' ]); ?>

                    <div class="row"> 
                      <div class="col-md-8">
                      
                        <div class="col-md-4">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Name :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              <?php echo Form::text('name',$get_record->admin_name,array('class' => 'gui-input','placeholder' => '' )); ?>

                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-pencil"></i>
                              </label>                            
                            </label>
                          </div>
                          </div>
                        <div class="col-md-4">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Email Id :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              <?php echo Form::text('email',$get_record->admin_email,array('class' => 'gui-input','placeholder' => '', $get_record  == "" ? '' : 'disabled'  )); ?>

                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-envelope"></i>
                              </label>                           
                            </label>
                          </div>
                          </div>
                          
                          <div class="col-md-4">
                            <div class="section">
                              <label for="level_name" class="field-label" style="font-weight:600;" >Mobile No :  </label>  
                              <label for="level_name" class="field prepend-icon">
                                <?php echo Form::text('number',$get_record->admin_number,array('class' => 'gui-input numeric','placeholder' => '' )); ?>

                                  <label for="Account Mobile" class="field-icon">
                                  <i class="fa fa-phone"></i>
                                </label>                           
                              </label>
                            </div>
                          </div>


                          <div class="col-md-12">
                            <div class="section">
                              <label for="level_name" class="field-label" style="font-weight:600;" > Address :  </label>  
                              <label for="level_name" class="field prepend-icon">
                                <?php echo Form::textarea('address',$get_record->admin_address,array('class' => 'gui-textarea accc1','placeholder' => '' )); ?>

                                  <label for="Account Mobile" class="field-icon">
                                  <i class="fa fa-map-marker"></i>
                                </label>                           
                              </label>
                            </div>
                          </div>


                          <?php if($get_record == ""): ?>
                          <div class="col-md-3">
                            <div class="section">
                              <label for="level_name" class="field-label" style="font-weight:600;" >Password :  </label>  
                              <label for="level_name" class="field prepend-icon">
                                <?php echo Form::password('password',array('class' => 'gui-input','id' => 'password' )); ?>

                                  <label for="Account Mobile" class="field-icon">
                                  <i class="fa fa-lock"></i>
                                </label>                           
                              </label>
                            </div>
                          </div>

                          <div class="col-md-3">
                            <div class="section">
                              <label for="level_name" class="field-label" style="font-weight:600;" >Confirm Password :  </label>  
                              <label for="level_name" class="field prepend-icon">
                                <?php echo Form::password('confirm_password',array('class' => 'gui-input','placeholder' => '' )); ?>

                                  <label for="Account Mobile" class="field-icon">
                                  <i class="fa fa-lock"></i>
                                </label>                           
                              </label>
                            </div>
                          </div>

                          <?php endif; ?>

                          <?php if($get_record->start_date == ""): ?>

                          <div class="col-md-3">
                            <div class="section section_from">
                              <label for="level_name" class="field-label" style="font-weight:600;"> Start Date :  </label>  
                              <label for="level_name" class="field prepend-icon fromdate">
                                <?php echo Form::text('start_date', '' ,array('class' => 'gui-input fromdate','placeholder' => '','id'=>'start_date' , readonly )); ?>

                                  <label for="Account Mobile" class="field-icon">
                                  <i class="fa fa-calendar"></i>
                                </label>                           
                              </label>
                            </div>
                          </div>

                          <?php else: ?>
                          <div class="col-md-3">
                            <div class="section section_from">
                              <label for="level_name" class="field-label" style="font-weight:600;"> Start Date :  </label>  
                              <label for="level_name" class="field prepend-icon fromdate">
                                <?php echo Form::text('start_date', date('d-m-Y',strtotime($get_record->start_date)) ,array('class' => 'gui-input fromdate','placeholder' => '','id'=>'start_date' , readonly )); ?>

                                  <label for="Account Mobile" class="field-icon">
                                  <i class="fa fa-calendar"></i>
                                </label>                           
                              </label>
                            </div>
                          </div>

                          <?php endif; ?>

                          <?php if($get_record->end_date == ""): ?>
                            <div class="col-md-3">
                            <div class="section section_to">
                              <label for="level_name" class="field-label" style="font-weight:600;"> End Date :  </label>  
                              <label for="level_name" class="field prepend-icon todate">
                                <?php echo Form::text('end_date', '' ,array('class' => 'gui-input todate','placeholder' => '','id'=>'' , readonly )); ?>

                                  <label for="Account Mobile" class="field-icon">
                                  <i class="fa fa-calendar"></i>
                                </label>                           
                              </label>
                            </div>
                          </div>

                          <?php else: ?>
                          <div class="col-md-3">
                            <div class="section section_to">
                              <label for="level_name" class="field-label" style="font-weight:600;"> End Date :  </label>  
                              <label for="level_name" class="field prepend-icon todate">
                                <?php echo Form::text('end_date', date('d-m-Y',strtotime($get_record->end_date)) ,array('class' => 'gui-input todate_new','placeholder' => '','id'=>'' , readonly )); ?>

                                  <label for="Account Mobile" class="field-icon">
                                  <i class="fa fa-calendar"></i>
                                </label>                           
                              </label>
                            </div>
                          </div>
                          <?php endif; ?>

                          </div>
                      
                      
                          <div class="col-md-4" style="  margin-top: 25px;">
                            <div class="fileupload fileupload-new admin-form" data-provides="fileupload">

                            <?php if($get_record['admin_image'] != ""): ?>

                              <div class="fileupload-preview thumbnail mb15" style="line-height: 136px !important; height: 200px !important;">
                                <?php echo Html::image($get_record['admin_image'], '100%x147'); ?>

                              </div>
                              <span class="button btn-system btn-file btn-block ph5" style="margin-bottom:10px">
                                <span class="fileupload-new">Update Profile</span>
                                <span class="fileupload-exists">Update Profile</span>
                                <?php echo Form::file('admin_image'); ?>

                              </span>

                            <?php else: ?>

                              <div class="fileupload-preview thumbnail mb15" style="padding: 3px;outline: 1px dashed #d9d9d9;height: 200px !important;">
                                <img data-src="holder.js/100%x200" alt="100%x200" src="" data-holder-rendered="true"  style="height: 200px !important; width: 100%; display: block;">
                              </div>
                              <span class="button btn-system btn-file btn-block ph5" style="margin-bottom:10px">
                                <span class="fileupload-new" >Photo Upload</span>
                                <span class="fileupload-exists">Photo Upload</span>
                                <?php echo Form::file('admin_image'); ?>

                              </span>

                            <?php endif; ?>

                            </div>
                        </div>
                      </div>


                      <div class="col-md-12">
                        <div class="section">
                          <label for="user_number" class="field-label" style="font-weight:600;" > Special Permission </label>
                            <label for="user_number" class="field prepend-icon">
                              <div class="row section">
                                  
                                <div class="col-md-3" style="padding: 10px">
                                  <label class="option block mn" style="font-size:12px;">
                                    <?php echo Form::checkbox('case_registration','1',isset(json_decode($get_record['special_permission'])->case_registration), array('id' => '', 'class' => 'check' )); ?>

                                    <span class="checkbox mn"></span> Case Registration
                                  </label>
                                </div>

                                <div class="col-md-3" style="padding: 10px">
                                  <label class="option block mn" style="font-size:12px;">
                                    <?php echo Form::checkbox('pessi_cause_list','1',isset(json_decode($get_record['special_permission'])->pessi_cause_list), array('id' => '', 'class' => 'check' )); ?>

                                    <span class="checkbox mn"></span> Peshi / Cause List Entry
                                  </label>
                                </div>

                                <div class="col-md-3" style="padding: 10px">
                                  <label class="option block mn" style="font-size:12px;">
                                    <?php echo Form::checkbox('daily_cause_list','1',isset(json_decode($get_record['special_permission'])->daily_cause_list), array('id' => '', 'class' => 'check' )); ?>

                                    <span class="checkbox mn"></span> Daily Diary
                                  </label>
                                </div>

                                <div class="col-md-3" style="padding: 10px">
                                  <label class="option block mn" style="font-size:12px;">
                                    <?php echo Form::checkbox('sms_to_clients','1',isset(json_decode($get_record['special_permission'])->sms_to_clients), array('id' => '', 'class' => 'check' )); ?>

                                    <span class="checkbox mn"></span> SMS to Clients
                                  </label>
                                </div>

                                <div class="clearfix"></div>

                                <div class="col-md-3" style="padding: 10px">
                                  <label class="option block mn" style="font-size:12px;">
                                    <?php echo Form::checkbox('order_judgement_upload','1',isset(json_decode($get_record['special_permission'])->order_judgement_upload), array('id' => '', 'class' => 'check' )); ?>

                                    <span class="checkbox mn"></span> Order / Judgement Upload
                                  </label>
                                </div>

                                <div class="col-md-3" style="padding: 10px">
                                  <label class="option block mn" style="font-size:12px;">
                                    <?php echo Form::checkbox('complaince','1',isset(json_decode($get_record['special_permission'])->complaince), array('id' => '', 'class' => 'check' )); ?>

                                    <span class="checkbox mn"></span> Complaince
                                  </label>
                                </div>

                                <div class="col-md-3" style="padding: 10px">
                                  <label class="option block mn" style="font-size:12px;">
                                    <?php echo Form::checkbox('calendar','1',isset(json_decode($get_record['special_permission'])->calendar), array('id' => '', 'class' => 'check' )); ?>

                                    <span class="checkbox mn"></span> Calendar
                                  </label>
                                </div>

                                <div class="col-md-3" style="padding: 10px">
                                  <label class="option block mn" style="font-size:12px;">
                                    <?php echo Form::checkbox('undated_case','1',isset(json_decode($get_record['special_permission'])->undated_case), array('id' => '', 'class' => 'check' )); ?>

                                    <span class="checkbox mn"></span> Undated Case
                                  </label>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-3" style="padding: 10px">
                                  <label class="option block mn" style="font-size:12px;">
                                    <?php echo Form::checkbox('due_course','1',isset(json_decode($get_record['special_permission'])->due_course), array('id' => '', 'class' => 'check' )); ?>

                                    <span class="checkbox mn"></span> Due Course
                                  </label>
                                </div>

                                <div class="col-md-3" style="padding: 10px">
                                  <label class="option block mn" style="font-size:12px;">
                                    <?php echo Form::checkbox('reporting','1',isset(json_decode($get_record['special_permission'])->reporting), array('id' => '', 'class' => 'check' )); ?>

                                    <span class="checkbox mn"></span> Reporting
                                  </label>
                                </div>

                                <div class="col-md-3" style="padding: 10px">
                                  <label class="option block mn" style="font-size:12px;">
                                    <?php echo Form::checkbox('master_modules','1',isset(json_decode($get_record['special_permission'])->master_modules), array('id' => '', 'class' => 'check' )); ?>

                                    <span class="checkbox mn"></span> Master Modules
                                  </label>
                                </div>

                              </div>
                            </label>
                          </div>
                        </div>
                      </div>

                      <div class="clearfix"></div>

              
                  <div class="panel-footer text-right">
                    <?php echo Form::submit('Save', array('class' => 'button btn-primary', 'id' => 'maskedKey')); ?>

                    <?php echo Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')); ?>

                  </div>
                  <?php echo Form::close(); ?>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


<script type="text/javascript">

  jQuery(document).ready(function() {
  
    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "This field contains alphabets only");

    jQuery.validator.addMethod("checkemail", function(value, element) 
    {
    return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
    }, "Enter a VALID email address"); 

    $.validator.addMethod('minStrict', function (value, el, param) {
     return value > param; 
    });
    $.validator.addMethod('maxStrict', function (value, el, param) {
     return value < param; 
    });


    jQuery.validator.addMethod("greaterThan_reg", function(value, element, params) {
      if (!/Invalid|NaN/.test(value)) {

      //var myDate_notice = value;
      value=value.split("-");
      var newDate_notice =value[1]+"/"+value[0]+"/"+value[2];
      var notice_status_date = new Date(newDate_notice).getTime();

      
      var notice_issue_date = $('#start_date').val();
      notice_issue_date=notice_issue_date.split("-");
      var newDate_notice_issue =notice_issue_date[1]+"/"+notice_issue_date[0]+"/"+notice_issue_date[2];
      var notice_issue_date = new Date(newDate_notice_issue).getTime();


      return notice_status_date >= notice_issue_date;
     
      }
      return isNaN(notice_status_date) && isNaN($(params).val()) 
      || (Number(notice_status_date) > Number($(params).val())); 
    },'End date must be greater than start date.');


    /* @custom  validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#validation").validate({

      /* @validation  states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation  rules 
      ------------------------------------------ */

      rules: {
        name: {
          required: true,
          lettersonly: true,
        },
        email: {
          required: true,
          checkemail: true
        },
        // number: {
        //   required: true,
        //   minlength: 10,
        //   maxlength: 10,
        // },
        password: {
          required: true,
          minlength: 6,
        },
        confirm_password: {
          required: true,
          minlength: 6,
          equalTo: '#password'
        },
        // address: {  
        //   required: true,
        // },
        start_date: {
          required: true
        },
        end_date: {
          required: true,
          greaterThan_reg: true,
        },
        admin_image: {
        // //  required: true,
          extension: 'jpeg,jpg,png',
        //   type: 'image/jpeg,image/png',
        // //  maxSize: 2097152,   // 2048 * 1024
       //    message: 'The selected file is not valid'
         },
      },

      /* @validation  error messages 
      ---------------------------------------------- */

      messages: {
        name: {
          required: 'Please enter name'
        },
        email: {
          required: 'Please enter Email Id'
        },
        // address: {
        //   required: 'Please enter address'
        // },
        start_date: {
          required: 'Please select start date'
        },
        end_date: {
          required: 'Please select end date'
        },
        // number: {
        //   required: 'Please enter mobile no',
        //   minlength:'Enter at least 10 digit phone numbers',
        //   maxlength:'Enter 10 digit phone numbers',
        // },        
        admin_image: {
          extension: 'Image Should be in .jpg,.jpeg and .png format only'
        },
        password:{
          required: 'Please enter password',
          minlength:'Please enter at least 6 characters'
        },
        confirm_password: {
          required: 'Please confirm your password, again',
          equalTo: 'New Password and Confirm Password do not match. Please try again !'
        },
      },

      /* @validation  highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });

  </script>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('advocate_admin/layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>