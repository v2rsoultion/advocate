<?php $__env->startSection('content'); ?>

<style type="text/css">
  .admin-form .select, .admin-form .gui-input, .admin-form .select > select, .admin-form .select-multiple select{
    height: 28px !important;
  }
  .admin-form a.button, .admin-form span.button, .admin-form label.button
  {
    line-height: 28px !important;
  }
  .admin-form .append-icon .field-icon, .admin-form .prepend-icon .field-icon{
    line-height: 28px !important;
  }
  .admin-form .gui-textarea {
    line-height: 7px !important;
  }
 .admin-form .button{
    height: 28px !important;
    line-height: 1px !important;
  }
 .btn {
    height: 29px !important;
    line-height: 1px !important;  }

 .down{
  margin-top: 70px;
 }
.account_setting {
  margin: 0px 0px !important;
  margin-top: 15px !important;
 }
</style>
  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left down">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="<?php echo e(url('/advocate-panel/sms-text')); ?>">SMS Text</a>
            </li>
            <li class="crumb-icon">
              <a href="<?php echo e(url('/advocate-panel/dashboard')); ?>">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="<?php echo e(url('/advocate-panel/dashboard')); ?>">Home</a>
            </li>
            <li class="crumb-trail">SMS Text</li>
          </ol>
        </div>
      </header>
      <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center" style="height:100% !important;">
          <div class="mw1100 center-block">
            
            <div class="row">
            <div class="col-md-12">
              <?php if(\Session::has('success')): ?>
              <div class="alert alert-success account_setting">
                <?php echo \Session::get('success'); ?>

              </div>
              <?php endif; ?>
            </div>
          </div>
            <div class="panel panel-primary panel-border top mt20 mb35">
              <div class="panel-heading">
                <span class="panel-title">SMS Text</span>
              </div>            
              <div class="panel-body bg-light dark">
                <div class="admin-form">
                  <?php echo Form::open(['url'=>'advocate-panel/insert-sms-text' , 'enctype' => 'multipart/form-data' , 'id' => 'admin-form', 'name' => 'form' ,'autocomplete'=>'off' ]); ?>


                    <!-- <div class="mb15" >
                      <span class="sub_group_new"> Sign Up Text </span>
                    </div>

                    <?php
                      $sign_up =  \App\Model\SmsText\SmsText::where(['sms_content_type' => 1 ,'user_admin_id' => session('admin_id') ])->first();
                    ?>

                    <div class="col-md-6">
                      <div class="section">
                        <label for="level_name" class="field-label" style="font-weight:600;"> Sign Up Before Text :  </label>  
                        <label for="level_name" class="field prepend-icon">
                          <input class="gui-input" id="" autocomplete="off" name="sign_up_before_text" type="text" value="<?php echo e($sign_up->sms_content_before); ?>">
                            <label for="Account Mobile" class="field-icon">
                            <i class="fa fa-pencil"></i>
                          </label>                            
                        </label>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="section">
                        <label for="level_name" class="field-label" style="font-weight:600;"> Sign Up After Text :  </label>  
                        <label for="level_name" class="field prepend-icon">
                          <input class="gui-input" id="" autocomplete="off" name="sign_up_after_text" type="text" value="<?php echo e($sign_up->sms_content_after); ?>">
                            <label for="Account Mobile" class="field-icon">
                            <i class="fa fa-pencil"></i>
                          </label>                            
                        </label>
                      </div>
                    </div> -->


                    <div class="mb15" >
                      <span class="sub_group_new"> Case Registration </span>
                    </div>

                    <?php
                      $case_registration =  \App\Model\SmsText\SmsText::where(['sms_content_type' => 2 ,'user_admin_id' => session('admin_id') ])->first();
                    ?>

                    <div class="col-md-6">
                      <div class="section">
                        <label for="level_name" class="field-label" style="font-weight:600;"> Case Registration Before Text :  </label>  
                        <label for="level_name" class="field prepend-icon">
                          <input class="gui-input" id="" autocomplete="off" name="case_registration_before_text" type="text" value="<?php echo e($case_registration->sms_content_before); ?>">
                            <label for="Account Mobile" class="field-icon">
                            <i class="fa fa-pencil"></i>
                          </label>                            
                        </label>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="section">
                        <label for="level_name" class="field-label" style="font-weight:600;"> Case Registration After Text :  </label>  
                        <label for="level_name" class="field prepend-icon">
                          <input class="gui-input" id="" autocomplete="off" name="case_registration_after_text" type="text" value="<?php echo e($case_registration->sms_content_after); ?>">
                            <label for="Account Mobile" class="field-icon">
                            <i class="fa fa-pencil"></i>
                          </label>                            
                        </label>
                      </div>
                    </div>


                    <div class="mb15" >
                      <span class="sub_group_new"> Peshi / Cause List Entry   </span>
                    </div>

                    <?php
                      $Pessi_cause =  \App\Model\SmsText\SmsText::where(['sms_content_type' => 3 ,'user_admin_id' => session('admin_id') ])->first();
                    ?>


                    <div class="col-md-6">
                      <div class="section">
                        <label for="level_name" class="field-label" style="font-weight:600;"> Peshi / Cause List Entry  Before Text :  </label>  
                        <label for="level_name" class="field prepend-icon">
                          <input class="gui-input" id="" autocomplete="off" name="pessi_before_text" type="text" value="<?php echo e($Pessi_cause->sms_content_before); ?>">
                            <label for="Account Mobile" class="field-icon">
                            <i class="fa fa-pencil"></i>
                          </label>                            
                        </label>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="section">
                        <label for="level_name" class="field-label" style="font-weight:600;"> Peshi / Cause List Entry  After Text :  </label>  
                        <label for="level_name" class="field prepend-icon">
                          <input class="gui-input" id="" autocomplete="off" name="pessi_after_text" type="text" value="<?php echo e($Pessi_cause->sms_content_after); ?>">
                            <label for="Account Mobile" class="field-icon">
                            <i class="fa fa-pencil"></i>
                          </label>                            
                        </label>
                      </div>
                    </div>


                    <div class="mb15" >
                      <span class="sub_group_new"> Daily Diary </span>
                    </div>

                    <?php
                      $daily_diary =  \App\Model\SmsText\SmsText::where(['sms_content_type' => 4 ,'user_admin_id' => session('admin_id') ])->first();
                    ?>

                    <div class="col-md-6">
                      <div class="section">
                        <label for="level_name" class="field-label" style="font-weight:600;"> Daily Diary Before Text :  </label>  
                        <label for="level_name" class="field prepend-icon">
                          <input class="gui-input" id="" autocomplete="off" name="daily_diary_before_text" type="text" value="<?php echo e($daily_diary->sms_content_before); ?>">
                            <label for="Account Mobile" class="field-icon">
                            <i class="fa fa-pencil"></i>
                          </label>                            
                        </label>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="section">
                        <label for="level_name" class="field-label" style="font-weight:600;"> Daily Diary After Text :  </label>  
                        <label for="level_name" class="field prepend-icon">
                          <input class="gui-input" id="" autocomplete="off" name="daily_diary_after_text" type="text" value="<?php echo e($daily_diary->sms_content_after); ?>">
                            <label for="Account Mobile" class="field-icon">
                            <i class="fa fa-pencil"></i>
                          </label>                            
                        </label>
                      </div>
                    </div>

                    <div class="mb15" >
                      <span class="sub_group_new"> Sms to Client   </span>
                    </div>

                    <?php
                      $sms_client =  \App\Model\SmsText\SmsText::where(['sms_content_type' => 5 ,'user_admin_id' => session('admin_id') ])->first();
                    ?>

                    <div class="col-md-6">
                      <div class="section">
                        <label for="level_name" class="field-label" style="font-weight:600;"> Sms to Client Before Text :  </label>  
                        <label for="level_name" class="field prepend-icon">
                          <input class="gui-input" id="" autocomplete="off" name="sms_client_before_text" type="text" value="<?php echo e($sms_client->sms_content_before); ?>">
                            <label for="Account Mobile" class="field-icon">
                            <i class="fa fa-pencil"></i>
                          </label>                            
                        </label>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="section">
                        <label for="level_name" class="field-label" style="font-weight:600;"> Sms to Client After Text :  </label>  
                        <label for="level_name" class="field prepend-icon">
                          <input class="gui-input" id="" autocomplete="off" name="sms_client_after_text" type="text" value="<?php echo e($sms_client->sms_content_after); ?>">
                            <label for="Account Mobile" class="field-icon">
                            <i class="fa fa-pencil"></i>
                          </label>                            
                        </label>
                      </div>
                    </div>

                    <!-- <div class="mb15" >
                      <span class="sub_group_new"> Undated Case   </span>
                    </div>

                    <?php
                      $undated_case =  \App\Model\SmsText\SmsText::where(['sms_content_id' => 6 ])->first();
                    ?>

                    <div class="col-md-6">
                      <div class="section">
                        <label for="level_name" class="field-label" style="font-weight:600;"> Undated Case Before Text :  </label>  
                        <label for="level_name" class="field prepend-icon">
                          <input class="gui-input" id="" autocomplete="off" name="undated_case_before_text" type="text" value="<?php echo e($undated_case->sms_content_before); ?>">
                            <label for="Account Mobile" class="field-icon">
                            <i class="fa fa-pencil"></i>
                          </label>                            
                        </label>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="section">
                        <label for="level_name" class="field-label" style="font-weight:600;"> Undated Case After Text :  </label>  
                        <label for="level_name" class="field prepend-icon">
                          <input class="gui-input" id="" autocomplete="off" name="undated_case_after_text" type="text" value="<?php echo e($undated_case->sms_content_after); ?>">
                            <label for="Account Mobile" class="field-icon">
                            <i class="fa fa-pencil"></i>
                          </label>                            
                        </label>
                      </div>
                    </div> -->

                    <!-- <div class="mb15" >
                      <span class="sub_group_new"> Due Course  </span>
                    </div>

                    <?php
                      $due_course =  \App\Model\SmsText\SmsText::where(['sms_content_id' => 7 ])->first();
                    ?>

                    <div class="col-md-6">
                      <div class="section">
                        <label for="level_name" class="field-label" style="font-weight:600;"> Due Course Before Text :  </label>  
                        <label for="level_name" class="field prepend-icon">
                          <input class="gui-input" id="" autocomplete="off" name="due_course_before_text" type="text" value="<?php echo e($due_course->sms_content_before); ?>">
                            <label for="Account Mobile" class="field-icon">
                            <i class="fa fa-pencil"></i>
                          </label>                            
                        </label>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="section">
                        <label for="level_name" class="field-label" style="font-weight:600;"> Sign Up After Text :  </label>  
                        <label for="level_name" class="field prepend-icon">
                          <input class="gui-input" id="" autocomplete="off" name="due_course_after_text" type="text" value="<?php echo e($due_course->sms_content_after); ?>">
                            <label for="Account Mobile" class="field-icon">
                            <i class="fa fa-pencil"></i>
                          </label>                            
                        </label>
                      </div>
                    </div> -->

                    <div class="form-group">
                    <div class="col-lg-12">
                      <div class="input-group" style="float: right; width: 220px;  ">
                      <?php echo Form::button('Update', array('class' => 'button btn-primary btn-file btn-block ph5', 'id' => 'maskedKey' , 'onclick' => 'update_record()' )); ?>


                      </div>
                    </div>
                    </div>
                  <?php echo Form::close(); ?>

                </div>
              </div>
            </div>
          </div>
        </div>
      </section>






<?php $__env->stopSection(); ?>

<?php echo $__env->make('advocate_admin/layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>