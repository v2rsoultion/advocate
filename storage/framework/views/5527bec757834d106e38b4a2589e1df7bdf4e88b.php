<!DOCTYPE html>
<html>
  <head>
  <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>Forgot Password | AdminPanel</title>
    <meta name="keywords" content="AdminPanel" />
    <meta name="description" content="AdminPanel">
    <meta name="author" content="AdminPanel">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <!-- Font CSS (Via CDN) -->
    <?php echo Html::style('public/admin/css/css.css'); ?>

    <!-- Admin Forms CSS -->
    <?php echo Html::style('public/admin/css/admin-forms.css'); ?>

    <!-- Theme CSS -->
    <?php echo Html::style('public/admin/css/theme.css'); ?>

    <?php echo Html::style('public/admin/css/fonts/glyphicons-pro/glyphicons-pro.css'); ?>

    <?php echo Html::style('public/admin/css/fonts/iconsweets/iconsweets.css'); ?>

    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo e(asset('public/admin/img/1.png')); ?>">
  </head>

  <body class="external-page sb-l-c sb-r-c">
    <!-- Start: Main -->
    <div id="main" class="animated fadeIn">
      <!-- Start: Content-Wrapper -->
      <section id="content_wrapper">
        <!-- begin canvas animation bg -->
        <div id="canvas-wrapper">
          <canvas id="demo-canvas"></canvas>
        </div>
        <!-- Begin: Content -->
        <section id="content">
          <div class="admin-form theme-info" id="login1">
            <div class="row mb15 table-layout">     
              <div class="col-xs-4 va-m pln"></div>
              <div class="col-xs-4 va-m pln">
                  <img src="<?php echo e(asset($account[0]['account_logo'])); ?>"  height="43"  title="Admin Logo" class="img-responsive">
              </div>

              <div class="col-xs-4 text-right va-b pr5">
                <a href="<?php echo e(url('advocate-panel')); ?>" class="back_textss"><span>Login</span></a>
              </div>
            </div>

            <div class="panel panel-info mt10 br-n">
              <div style="border-top:5px solid #3bafda;">
                <?php echo Form::open(['url'=>'advocate-panel/forgot-password', 'id' => 'login_form' ,'autocomplete'=>'off']); ?>


                <?php if($error_msg != ""): ?>
                  <div class="panel-body p15 pt25" id="email_msg">
                    <div class="alert alert-micro alert-border-left alert-info pastel alert-dismissable mn">
                      <i class="fa fa-info pr10"></i> Entered <b>Email Id</b> Is Invalid!
                    </div>
                  </div>
                <?php endif; ?>

                <?php if($status_msg != ""): ?>
                  <div class="panel-body p15 pt25" id="email_msg">
                    <div class="alert alert-micro alert-border-left alert-info pastel alert-dismissable mn">
                      <i class="fa fa-danger pr10"></i> <?php echo e($status_msg); ?>

                    </div>
                  </div>
                <?php endif; ?>

                <div class="panel-footer p25 pv25">
                  <div class="section mn">
                    <div class="smart-widget sm-right smr-80">
                      <label for="email" class="field prepend-icon">
                        <?php echo Form::email('admin_email', '', array('class' => 'gui-input','placeholder' => 'your Email Address', 'required' => 'required'  )); ?>

                        <label for="email" class="field-icon">
                          <i class="fa fa-envelope-o"></i>
                        </label>
                      </label>
                      <label><input type="submit" name="login" id="login" class="button" value="Reset" ></label>
                    </div>
                  </div>
                </div> 
                <?php echo Form::close(); ?>

              </div>
            </div>
          </div>
        </section>
      </section>
    </div>
          
  <?php echo HTML::script('public/admin/js/jquery/jquery-1.11.1.min.js'); ?>

  <?php echo HTML::script('public/admin/js/jquery/jquery_ui/jquery-ui.min.js'); ?>

  <?php echo HTML::script('public/admin/js/plugins/canvasbg/canvasbg.js'); ?>

  <?php echo HTML::script('public/admin/js/utility/utility.js'); ?>

  <?php echo HTML::script('public/admin/js/demo/demo.js'); ?>

  <?php echo HTML::script('public/admin/js/main.js'); ?>

 

  <script>
$(document).ready(function() {
  $('img').on('dragstart', function(event) { event.preventDefault(); });
});
</script>
  
  <!-- Page Javascript -->
  <script type="text/javascript">
  jQuery(document).ready(function() {

    "use strict";
    // Init Theme Core      
    Core.init();

    // Init Demo JS
    Demo.init();

    // Init CanvasBG and pass target starting location
    CanvasBG.init({
      Loc: {
        x: window.innerWidth / 2,
        y: window.innerHeight / 3.3
      },
    });

  });
  </script>

  <!-- END: PAGE SCRIPTS -->

</body>
</html>
