<?php $__env->startSection('content'); ?>
<style type="text/css">
  .admin-form .select, .admin-form .gui-input, .admin-form .select > select, .admin-form .select-multiple select{
    height: 28px !important;
  }
  .admin-form a.button, .admin-form span.button, .admin-form label.button
  {
    line-height: 28px !important;
  }
  .admin-form .append-icon .field-icon, .admin-form .prepend-icon .field-icon{
    line-height: 28px !important;
  }
  .admin-form .gui-textarea {
    line-height: 7px !important;
  }
/*  .gui-textarea{
    height: 100px !important;
  }*/
/* .admin-form .gui-input {
  padding: 5px;
 }*/
 /*.admin-form .prepend-icon .field-icon {
  top: 6px;
 }*/
 .admin-form .button{
    height: 28px !important;
    line-height: 1px !important;
  }
 .btn {
    height: 29px !important;
    line-height: 1px !important;  }

 .down{
  margin-top: 70px;
 }
.account_setting {
  margin: 0px 20px !important;
  margin-top: 25px !important;
  /*padding: 5px 0px !important;*/
 }
</style>
  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left down">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="<?php echo e(url('/advocate-panel/view-act')); ?>">View Acts</a>
            </li>
            <li class="crumb-icon">
              <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">Home</a>
            </li>
            <li class="crumb-trail">Add Acts</li>
          </ol>
        </div>
      </header>

      <div class="row">
        <div class="col-md-12">
          <?php if(\Session::has('success')): ?>
          <div class="alert alert-success account_setting" >
            <?php echo \Session::get('success'); ?>

          </div>
          <?php endif; ?>
        </div>
      </div>
      <!-- Begin: Content -->

      <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center">
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-heading">
                <span class="panel-title">Add Acts</span>
              </div>
              <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                <!-- IF any error found -->
                <div class="col-md-12">
                <?php if($errors->any()): ?>
                  <div id="log_error" class="alert alert-danger" style='font-family: josefin_sansregular !important;'>
                  <?php echo e($errors->first()); ?>  </i></div>
                <?php endif; ?>
                </div>
                <!-- IF any error found -->
                  <div id="tab1_1" class="tab-pane active">
                    <?php echo Form::open(['name'=>'form_add_question','url'=>'advocate-panel/insert-act/'.$get_record[0]->act_id,'id'=>'form_add_question' ,'autocomplete'=>'off']); ?>

                    <div class="row">
                      <div class="col-md-12">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;" >Act Name :  </label>  
                          <label for="level_name" class="field prepend-icon">
                            <?php echo Form::text('type_name',$get_record[0]->act_name,array('class' => 'gui-input','placeholder' => '' )); ?>

                              <label for="Account Mobile" class="field-icon">
                              <i class="fa fa-pencil"></i>
                            </label>                           
                          </label>
                        </div>
                      </div>


                      <div class="col-md-12">

                        <div class="">
                          <label for="level_name" class="field-label" style="font-weight:600; font-size: 18px;" >Add Section :  </label>
                        </div>

                        <?php
                          $counterss  = 0;
                          $section =  \App\Model\Section\Section::where(['section_act_id' => $get_record[0]->act_id ])->where('section_name', '!=' ,'')->get();
                        ?>

                        <?php if(count($section) != 0): ?>
                        <?php $__currentLoopData = $section; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sections): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                          <?php $counterss++; ?>
                          
                          <div id="hide_resp<?php echo e($counterss); ?>">
                            <?php echo Form::hidden('section_add['.$counterss.'][section_id]',$sections->section_id,array('class' => 'gui-input','placeholder' => '','id'=>'' )); ?>

                            <div class="" style="margin-top: 10px;">
                              <div class="section">
                                <label for="level_name" class="field-label" style="font-weight:600;"> Section Name: </label>
                                <label for="level_name" class="field prepend-icon">
                                  <?php echo Form::text('section_add['.$counterss.'][section_name]',$sections->section_name,array('class' => 'gui-input','placeholder' => '','id'=>'' )); ?>

                                  <label for="Account Mobile" class="field-icon">
                                    <i class="fa fa-pencil"></i>
                                  </label>
                                </label>
                              </div>
                            </div>
                          

                          <div class="section">
                            <div class="col-md-12" style="margin-left:20px !important;">
                              <button type="button" onclick="removerecord('<?php echo e($counterss); ?>');" name="add" id="" class="button btn-danger pull-right mr10 mb10"><i class="fa fa-trash-o" aria-hidden="true"></i> &nbsp; Remove Fields
                              </button>
                            </div>
                          </div>
                          <div class="clearfix"> </div>
                          </div> 
                        
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>

                        <?php if($get_record != ""): ?>
                        <div id="questionrowsres"></div>
                        <?php endif; ?>

                        <?php if($get_record == ""): ?>
                                
                            <div class="" style="margin-top: 10px;">
                              <div class="section">
                                <label for="level_name" class="field-label" style="font-weight:600;"> Section Name : </label>
                                <label for="level_name" class="field prepend-icon">
                                  <?php echo Form::text('section_add[0][section_name]',$get_record[0]->section_name,array('class' => 'gui-input','placeholder' => '','id'=>'' )); ?>

                                  <label for="Account Mobile" class="field-icon">
                                    <i class="fa fa-pencil"></i>
                                  </label>
                                </label>
                              </div>
                            </div>
                            <div class="clearfix"> </div>
                            <div id="questionrowsres"></div>
                                
                        <?php endif; ?>

                        <div id="questionrowsres"></div>

                        <div class="section">
                          <div class="col-md-12"> 
                            <button type="button" onclick="addrecord();" style="margin-right: -12px !important; " name="add" id="" class="button btn-primary pull-right"> <i class="fa fa-plus" aria-hidden="true"></i> &nbsp; Add Section </button>
                          </div>
                        </div>
                        
                        <input type="hidden" name="countidres1" id="countidres1" value="<?php echo e(count($section)); ?>"> <br>
                        <input type="hidden" name="countidres2" id="countidres2" value="0">
                      </div>

                        <!-- <div class="col-md-12">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;" >Short Name :  </label>  
                          <label for="level_name" class="field prepend-icon">
                            <?php echo Form::text('short_name',$get_record[0]->act_short_name,array('class' => 'gui-input','placeholder' => '' )); ?>

                              <label for="Account Mobile" class="field-icon">
                              <i class="fa fa-pencil"></i>
                            </label>                           
                          </label>
                        </div>
                        </div> -->
                        <!-- <div class="col-md-12">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Category :  </label>
                            <div class="option-group field">  
                              <label for="level_name" class="radio_div">
                                <?php echo Form::radio('c_name','2',$get_record[0]->act_category=='2' ? 'checked' :'',array('class' => 'high_court','placeholder' => '','id'=>'civil' )); ?> <label for="civil"><span>Civil</span></label>
                                <?php echo Form::radio('c_name','1',$get_record[0]->act_category=='1' ? 'checked' :'',array('class' => 'trial_court','placeholder' => '','id'=>'criminal' )); ?> <label for="criminal"><span>Criminal</span></label>                        
                              </label>
                            </div>
                          </div>
                        </div> -->
                       </div>

                      <div class="panel-footer text-right">
                          <?php echo Form::submit('Save', array('class' => 'button btn-primary', 'id' => 'maskedKey')); ?>

                          <?php echo Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')); ?>

                      </div>   
                        <?php echo Form::close(); ?>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

<script type="text/javascript">

  jQuery(document).ready(function() {
  
    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z,0-9," "]+$/i.test(value);
    }, "This field contains alphabets and numeric only");

    jQuery.validator.addMethod("checkemail", function(value, element) 
    {
    return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
    }, "Enter a VALID email address"); 

    $.validator.addMethod('minStrict', function (value, el, param) {
     return value > param; 
    });
    $.validator.addMethod('maxStrict', function (value, el, param) {
     return value < param; 
    });


    /* @custom  validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#form_add_question").validate({

      /* @validation  states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation  rules 
      ------------------------------------------ */

      rules: {
        // type_name: {
        //   required: true,
        //   lettersonly: true
        // },
        // c_name: {
        //   required: true,
        //   lettersonly: true
        // },
        // short_name: {
        //   required: true,
        //   lettersonly: true
        // },
      },

      /* @validation  error messages 
      ---------------------------------------------- */

      messages: {
        type_name: {
          required: 'Please Fill Required Act Name'
        },
        c_name: {
          required: 'Please Fill Required Category'
        },
        short_name: {
          required: 'Please Fill Required Short Name'
        },        
      },

      /* @validation  highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });


  function addrecord(){
    var count1 = $('#countidres1').val();
    var count2 = $('#countidres2').val();

    // alert(count1);
    // alert(count2);

    var counter1 = parseInt(count1) + 1;
    var counter2 = parseInt(count2) + 1;
    $('#countidres1').val(counter1);
    $('#countidres2').val(counter2);

    if(count2 >= 0){
      $('#questionrowsres').append('<div class="questiondivsres" id="questiondivsres'+counter1+'"> <div class=""><div class="section"><label for="level_name" class="field-label" style="font-weight:600;" > Section Name : </label><label for="level_name" class="field prepend-icon"><input type="text" name="section_add['+counter1+'][section_name]" value="" class="gui-input" placeholder="" ><label for="Account Mobile" class="field-icon"><i class="fa fa-pencil"></i></label></label></div></div> <div class="section"><div class=""><button type="button" onclick="removerecord('+counter1+');" name="add" id="" class="button btn-danger pull-right mb10"> <i class="fa fa-trash-o" aria-hidden="true"></i> &nbsp; Remove Fields </button></div></div> </div></div>');
    }
  }


  function removerecord(countnew){
    $( "#questiondivsres"+countnew+"" ).remove();
    $( "#hide_resp"+countnew+"" ).remove();

    var count2 = $('#countidres1').val();
    var counter2 = parseInt(count2) - 1;
    $('#countidres1').val(counter2);
  }


  </script>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('advocate_admin/layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>