<?php $__env->startSection('content'); ?>
<style type="text/css">
  .admin-form .select, .admin-form .gui-input, .admin-form .select > select, .admin-form .select-multiple select{
    height: 28px !important;
  }
  .admin-form a.button, .admin-form span.button, .admin-form label.button
  {
    line-height: 28px !important;
  }
  .admin-form .append-icon .field-icon, .admin-form .prepend-icon .field-icon{
    line-height: 28px !important;
  }
  .admin-form .gui-textarea {
    line-height: 7px !important;
  }
/*  .gui-textarea{
    height: 100px !important;
  }*/
/* .admin-form .gui-input {
  padding: 5px;
 }*/
 /*.admin-form .prepend-icon .field-icon {
  top: 6px;
 }*/
 .admin-form .button{
    height: 28px !important;
    line-height: 1px !important;
  }
 .btn {
    height: 29px !important;
    line-height: 1px !important;  }

 .down{
  margin-top: 70px;
 }
.account_setting {
  margin: 0px 20px !important;
  margin-top: 25px !important;
  /*padding: 5px 0px !important;*/
 }
</style>

  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left down">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="<?php echo e(url('/advocate-panel/view-court')); ?>">View Court</a>
            </li>
            <li class="crumb-icon">
              <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">Home</a>
            </li>
            <li class="crumb-trail">Add Court</li>
          </ol>
        </div>
      </header>

      <div class="row">
        <div class="col-md-12">
          <?php if(\Session::has('success')): ?>
          <div class="alert alert-success account_setting" >
            <?php echo \Session::get('success'); ?>

          </div>
          <?php endif; ?>
        </div>
      </div>
      
      <!-- Begin: Content -->

      <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center">
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-heading">
                <span class="panel-title">Add Court</span>
              </div>
              <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                <!-- IF any error found -->
                <div class="col-md-12">
                <?php if($errors->any()): ?>
                  <div id="log_error" class="alert alert-danger" style='font-family: josefin_sansregular !important;'>
                  <?php echo e($errors->first()); ?>  </i></div>
                <?php endif; ?>
                </div>
                <!-- IF any error found -->
                  <div id="tab1_1" class="tab-pane active">
                    <?php echo Form::open(['name'=>'form_add_question','url'=>'advocate-panel/insert-court/'.$get_record[0]->court_id,'id'=>'form_add_question' ,'autocomplete'=>'off']); ?>

                    <div class="row">
                      
                     <!--  <div class="col-md-12">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;" >Choose Type :  </label>  
                              <label for="level_name" class="radio_div">
                                
                                <?php echo Form::radio('choose_type','2',$get_record[0]->court_type=='2' ? 'checked' :'',array('class' => 'high_court','placeholder' => '','id'=>'high' )); ?> 
                                <label for="high"><span>High Court</span></label>
                                <?php echo Form::radio('choose_type','1',$get_record[0]->court_type=='1' ? 'checked' :'',array('class' => 'trial_court','placeholder' => '','id'=>'trial' )); ?> 
                                <label for="trial"><span>Trial Court</span></label>             

                            </div>
                          </div> -->

                      <div class="col-md-12">
                        <div class="section" style="margin-bottom: 40px;">
                          <label for="level_name" class="field-label" style="font-weight:600;" >Choose Type :  </label>  
                          <div class="col-md-2">
                            <label class="option" style="font-size:12px;">

                              <?php if($get_record[0]->court_type != ""): ?>
                                <?php echo Form::radio('choose_type','1',$get_record[0]->court_type == 1 ? 'checked' : '', array( 'class' => 'check' )); ?>

                              <?php else: ?> 
                                <?php echo Form::radio('choose_type','1',checked, array( 'class' => 'check' , 'class' => 'check' )); ?>

                              <?php endif; ?>
                              <span class="radio"></span> Trial Court
                            </label>
                          </div>

                          <div class="col-md-2">
                            <label class="option" style="font-size:12px;">
                              <?php echo Form::radio('choose_type','2',$get_record[0]->court_type == 2 ? 'checked' : '', array( 'class' => 'check'  )); ?>

                              <span class="radio"></span> High Court
                            </label>
                          </div>
                        </div>

                        <div class="section"></div>
                        <div class="clearfix"></div>
                      </div>

                          <div class="col-md-12">
                            <div class="section">
                              <label for="level_name" class="field-label" style="font-weight:600;" >Court Name :  </label>  
                              <label for="level_name" class="field prepend-icon">
                                <?php echo Form::text('court_name',$get_record[0]->court_name,array('class' => 'gui-input','placeholder' => '' )); ?>

                                  <label for="Account Mobile" class="field-icon">
                                  <i class="fa fa-pencil"></i>
                                </label>                           
                              </label>
                            </div>
                          </div> 
                          <!-- <div class="col-md-6">
                            <div class="section">
                              <label for="level_name" class="field-label" style="font-weight:600;" >Short Name :  </label>  
                              <label for="level_name" class="field prepend-icon">
                                <?php echo Form::text('short_name',$get_record[0]->court_short_name,array('class' => 'gui-input','placeholder' => '' )); ?>

                                  <label for="Account Mobile" class="field-icon">
                                  <i class="fa fa-pencil"></i>
                                </label>                           
                              </label>
                            </div>
                          </div> -->
                        </div>

                        <div class="panel-footer text-right">
                            <?php echo Form::submit('Save', array('class' => 'button btn-primary', 'id' => 'maskedKey')); ?>

                            <?php echo Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')); ?>

                        </div>   
                        <?php echo Form::close(); ?>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>


<script type="text/javascript">

$(document).on('click','.mysave',function(){

    var case_no = $("#case_type_ajax").val();
    
    if(case_no == 0){
      $('#descrption_error').html('<div class="state-error"></div><em for="services_excerpt" class="state-error">Please select case no.</em>');
      $(".select2-selection").css({"border": "1px solid #de888a"});
      $(".select2-selection").css({"background": "#fee9ea"});
      return false;
    }
  })

</script>
<script type="text/javascript">

  jQuery(document).ready(function() {
  
    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "This field contains alphabets only");

    jQuery.validator.addMethod("checkemail", function(value, element) 
    {
    return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
    }, "Enter a VALID email address"); 

    $.validator.addMethod('minStrict', function (value, el, param) {
     return value > param; 
    });
    $.validator.addMethod('maxStrict', function (value, el, param) {
     return value < param; 
    });


    /* @custom  validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#form_add_question").validate({

      /* @validation  states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation  rules 
      ------------------------------------------ */

      rules: {
        // choose_type: {
        //   required: true
        // },
        // court_name: {
        //   required: true,
        //   lettersonly: true
        // },
        // short_name: {
        //   required: true,
        //   lettersonly: true
        // },
      },

      /* @validation  error messages 
      ---------------------------------------------- */

      messages: {
        choose_type: {
          required: 'Please Fill Required Choose Type'
        },
        court_name: {
          required: 'Please Fill Required Court Name'
        },
        short_name: {
          required: 'Please Fill Required Short Name'
        },        
      },

      /* @validation  highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });


  </script>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('advocate_admin/layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>