<?php $__env->startSection('content'); ?>

  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="<?php echo e(url('/advocate-panel/view-case-sub-type')); ?>">View Case Sub Type</a>
            </li>
            <li class="crumb-icon">
              <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">Home</a>
            </li>
            <li class="crumb-trail">Add Case Sub Type</li>
          </ol>
        </div>
      </header>

      <!-- Begin: Content -->

      <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center">
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-heading">
                <span class="panel-title">Add Case Sub Type</span>
              </div>
              <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                <!-- IF any error found -->
                <div class="col-md-12">
                <?php if($errors->any()): ?>
                  <div id="log_error" class="alert alert-danger" style='font-family: josefin_sansregular !important;'>
                  <?php echo e($errors->first()); ?>  </i></div>
                <?php endif; ?>
                </div>
                <!-- IF any error found -->
                  <div id="tab1_1" class="tab-pane active">
                    <?php echo Form::open(['name'=>'form_add_question','url'=>'advocate-panel/insert-case-sub-type/'.$get_record[0]->subtype_id,'id'=>'form_add_question']); ?>

                    <div class="row">
                      <div class="col-md-6">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Choose Case Type : </label>
                              <label for="artist_state" class="field select">
                                <select class="form-control" id="" name="choose_case_type">
                                  <option value=''>Choose Case Type</option>
                                  <?php if($get_record != ""): ?> 
                                   <?php $__currentLoopData = $case_type_entry; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $case_type_entrys): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                     <option value="<?php echo e($case_type_entrys->case_id); ?>" <?php echo e($case_type_entrys->case_id == $get_record[0]->subtype_case_id ? 'selected="selected"' : ''); ?> ><?php echo e($case_type_entrys->case_name); ?> </option>
                                   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php else: ?>
                                   <?php $__currentLoopData = $case_type_entry; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $case_type_entrys): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                     <option value="<?php echo e($case_type_entrys->case_id); ?>"><?php echo e($case_type_entrys->case_name); ?> </option>
                                   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>                               
                                </select>
                                <i class="arrow double"></i>
                              </label>
                          </div>
                        </div>
                        <div class="col-md-6">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;" >Sub Type Name :  </label>  
                          <label for="level_name" class="field prepend-icon">
                            <?php echo Form::text('sub_type_name',$get_record[0]->subtype_name,array('class' => 'gui-input','placeholder' => '' )); ?>

                              <label for="Account Mobile" class="field-icon">
                              <i class="fa fa-pencil"></i>
                            </label>                           
                          </label>
                        </div>
                        </div>
                        <div class="col-md-6">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;" >Code :  </label>  
                          <label for="level_name" class="field prepend-icon">
                            <?php echo Form::text('code',$get_record[0]->subtype_code,array('class' => 'gui-input','placeholder' => '' )); ?>

                              <label for="Account Mobile" class="field-icon">
                              <i class="fa fa-pencil"></i>
                            </label>                           
                          </label>
                        </div>
                        </div>
                        <div class="col-md-6">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;" >Short Name :  </label>  
                          <label for="level_name" class="field prepend-icon">
                            <?php echo Form::text('short_name',$get_record[0]->subtype_short_name,array('class' => 'gui-input','placeholder' => '' )); ?>

                              <label for="Account Mobile" class="field-icon">
                              <i class="fa fa-pencil"></i>
                            </label>                           
                          </label>
                        </div>
                        </div>
                        <div class="col-md-12">
                        <div class="section" id="textasrea">
                          <label for="level_name" class="field-label" style="font-weight:600;" >Description :  </label>  
                          <label for="level_name" class="field prepend-icon">
                            <?php echo Form::textarea('description',$get_record[0]->subtype_description,array('class' => 'gui-textarea','placeholder' => '' )); ?>

                              <label for="Account Mobile" class="field-icon">
                              <i class="fa fa-pencil"></i>
                            </label>                          
                          </label>
                        </div>
                        </div>
                     </div>

                  <div class="panel-footer text-right">
                      <?php echo Form::submit('Save', array('class' => 'button btn-primary', 'id' => 'maskedKey')); ?>

                      <?php echo Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')); ?>

                  </div>   
                    <?php echo Form::close(); ?>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

<script type="text/javascript">

  jQuery(document).ready(function() {
  
    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "This field contains alphabets only");

    jQuery.validator.addMethod("checkemail", function(value, element) 
    {
    return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
    }, "Enter a VALID email address"); 

    $.validator.addMethod('minStrict', function (value, el, param) {
     return value > param; 
    });
    $.validator.addMethod('maxStrict', function (value, el, param) {
     return value < param; 
    });


    /* @custom  validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#form_add_question").validate({

      /* @validation  states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation  rules 
      ------------------------------------------ */

      rules: {
        choose_case_type: {
          required: true
        },
        sub_type_name: {
          required: true
        },
        code: {
          required: true
        },
        description: {
          required: true
        },
        short_name: {
          required: true
        },
      },

      /* @validation  error messages 
      ---------------------------------------------- */

      messages: {
        choose_case_type: {
          required: 'Please Fill Required Choose Case Type'
        },
        sub_type_name: {
          required: 'Please Fill Required Sub Type Name'
        },
        code: {
          required: 'Please Fill Required Code'
        },
        description: {
          required: 'Please Fill Required Description'
        },
        short_name: {
          required: 'Please Fill Required Short Name'
        },        
      },

      /* @validation  highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });


  </script>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('advocate_admin/layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>