
    <select id="framework_3" name="section[]" multiple class="form-control">
        <?php $__currentLoopData = $act_type_change; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $act_type_changes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
		 <option value="<?php echo e($act_type_changes->section_id); ?>" <?php echo e($act_type_changes->section_id == $get_record->reg_section_id ? 'selected="selected"' : ''); ?> ><?php echo e($act_type_changes->section_name); ?> </option>
		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>
    <i class="arrow double arrow_new"></i>


<script type="text/javascript">
	

	$('#framework_3').multiselect({
      nonSelectedText: 'Select Section',
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
      buttonWidth:'400px'
     });
	
</script>
	
