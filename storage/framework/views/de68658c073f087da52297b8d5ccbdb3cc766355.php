<?php $__env->startSection('content'); ?>
<style type="text/css">
  /*.admin-form .select, .admin-form .gui-input, .admin-form .select > select, .admin-form .select-multiple select{
    height: 28px !important;
  }*/
  .admin-form .append-icon .field-icon, .admin-form .prepend-icon .field-icon{
    line-height: 28px !important;
  }
  .admin-form .gui-textarea {
    line-height: 15px !important;
  }

  .form-control {
    height: 28px !important;
  }
  .admin-form .button{
    height: 28px !important;
    line-height: 1px !important;
  }
  .btn {
    height: 29px !important;
    line-height: 1px !important;  }

 .down{
  margin-top: 70px;
 }
 .select-text{
  padding: 0px 12px;
 }
.admin-form .select, .admin-form .gui-input, .admin-form  .admin-form .select > select, .admin-form .select-multiple select {
  height: 28px !important;
}
</style>
<style type="text/css">

.select_outer{
  height: 30px !important;
}
.new_select{
  height: 30px !important;
  padding: 0px 10px !important;
  font-size: 12px !important;
}

.arrow_new{
  top: 2px !important;
}

.new_text{
  height: 30px !important;
  font-size: 12px !important;
}

.search_new{
  line-height: 32px !important;
}

.new_button{
  height: 30px !important;
  font-size: 12px !important;
  padding: 5px 10px; 
  line-height: 30px !important;
}

.new_button_2{
  height: 30px !important;
  font-size: 12px !important;
  padding: 5px 10px; 
}

.caretss {
    margin-left: 30% !important;
}

.mutbtnnn button{
  height: 30px !important;
  padding: 3px 10px !important; 
}

.admin-form .checkbox, .admin-form .radio{
  border: 0px solid !important;
  background: none !important;
}

 .close{
  margin-top: -6px !important;
  font-size: 30px !important;
  width: 30px !important;
}

/*.modal{
  position: absolute !important;
  top: -130px;
  left: -25px;
}*/

</style>
  <!-- Start: Content-Wrapper -->
  <section id="content_wrapper">
    <header id="topbar" style="padding-top: 70px;">
      <div class="topbar-left">
        <ol class="breadcrumb">
          <li class="crumb-active">
            <a href="#">Peshi / Cause List Entry</a>
          </li>
          <li class="crumb-icon">
            <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">
              <span class="glyphicon glyphicon-home"></span>
            </a>
          </li>
          <li class="crumb-link">
            <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">Home</a>
          </li>
          <li class="crumb-trail">Peshi / Cause List Entry </li>
        </ol>
      </div>
    </header>

    <div class="row">
      <div class="col-md-12">
        <?php if(\Session::has('success')): ?>
        <div class="alert alert-danger account_setting mt10">
          <?php echo \Session::get('success'); ?>

        </div>
        <?php endif; ?>

        <?php if(\Session::has('danger')): ?>
        <div class="alert alert-success account_setting mt10">
          <?php echo \Session::get('danger'); ?>

        </div>
        <?php endif; ?>

      </div>
    </div>

    <div class="" style="margin-top:10px;">
      <div class="col-md-12">
        <div class="panel panel-primary panel-border top mb70">  
          <div class="panel-heading">
            <div class="panel-title hidden-xs">
              <span class="glyphicon glyphicon-tasks"></span> Peshi / Cause List Entry </div>
          </div>

          <div class="panel-menu admin-form theme-primary" style="padding: 5px 20px;">
            <div class="row">
              <?php echo Form::open(['url'=>'/advocate-panel/view-peshi' ,'autocomplete'=>'off']); ?>


                <div class="col-md-2">
                    <label for="level" class="field prepend-icon select_outer" style="margin-top:10px !important;">
                      <select class=" form-control new_select" name="court_type" id="level" style="color: black;">
                        <!-- <option value="">Court Type</option> -->
                            <option value="1" <?php echo e($court_type == 1 ? 'selected="selected"' : ''); ?> >Trial Court</option>
                            <option value="2" <?php echo e($court_type == 2 ? 'selected="selected"' : ''); ?> >High Court</option>
                            <option value="3" <?php echo e($court_type == 3 ? 'selected="selected"' : ''); ?> >Both</option> 
                      </select>
                    </label>
                </div>

                <div class="col-md-2">
                    <label for="level" class="field prepend-icon select_outer" style="margin-top:10px !important;">
                      <select class=" form-control new_select" name="case_status" id="level" style="color: black;">
                        <!-- <option value="">Select Status</option> -->
                            <option value="1" <?php echo e($case_status == 1 ? 'selected="selected"' : ''); ?> > Pending </option>
                            <option value="2" <?php echo e($case_status == 2 ? 'selected="selected"' : ''); ?> > Disposal</option>
                            <option value="3" <?php echo e($case_status == 3 ? 'selected="selected"' : ''); ?> >Both</option> 
                      </select>
                    </label>
                </div>

                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 10px;">
                    <?php echo Form::text('reg_file_no','',array('class' => 'form-control new_text','placeholder' => 'File No', 'id' => 'filenum', 'autocomplete' => 'off' )); ?>

                    <label for="pincode" class="field-icon search_new">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>

                <div class="col-md-2">
                  <div class="" style="margin-top:10px !important;">
                    <label for="artist_state" class="field select select_outer">
                      <select class="form-control new_select" name="type" style="color: black;">
                        <option value=''>Case Type</option> 
                        
                        <?php $__currentLoopData = $case_type_entry; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $case_type_entrys): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                        <option value="<?php echo e($case_type_entrys->case_id); ?>" <?php echo e($case_type_entrys->case_id == $reg_case_type_id ? 'selected="selected"' : ''); ?> ><?php echo e($case_type_entrys->case_name); ?> </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        
                      </select>
                      <i class="arrow double arrow_new"></i>
                    </label>
                  </div>
                </div>

                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 10px;">
                    <?php echo Form::text('petitioner_name','',array('class' => 'form-control new_text','placeholder' => 'Pet. Name', 'autocomplete' => 'off' )); ?>

                    <label for="pincode" class="field-icon search_new">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>

                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 10px;">
                    <?php echo Form::text('respondent','',array('class' => 'form-control new_text','placeholder' => 'Res. Name', 'autocomplete' => 'off' )); ?>

                    <label for="pincode" class="field-icon search_new">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>

                <div class="col-md-2" style="display: none" id="case_no_hide">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 10px;">
                    <?php echo Form::text('case_no','',array('class' => 'form-control new_text','placeholder' => 'Case No', 'autocomplete' => 'off' )); ?>

                    <label for="pincode" class="field-icon search_new">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>

                <!-- <div class="col-md-2" style="display: none" id="year_hide">
                  <div class="" style="margin-top:10px !important;">
                      <label for="artist_state" class="field select select_outer">
                        <select class="form-control new_select" id="" name="case_year">
                          <option value=''>Select Year</option>
                      
                          <?php
                              $currentYear = date('Y');
                          ?>

                          <?php $__currentLoopData = range(1950, $currentYear); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value=<?php echo e($value); ?> <?php echo e($value == $case_year ? 'selected="selected"' : ''); ?> ><?php echo e($value); ?></option>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </select>
                        <i class="arrow double arrow_new"></i>
                      </label>
                  </div>
                </div> -->

                <!-- <div class="clearfix"></div> -->
                <div class="col-md-2" style="display: none" id="ncv_no_hide">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 10px;">
                    <?php echo Form::text('ncv_no','',array('class' => 'form-control new_text','placeholder' => 'NCV No', 'autocomplete' => 'off' )); ?>

                    <label for="pincode" class="field-icon search_new">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>
              
                <div class="col-md-2" style="display: none" id="stage_hide">
                  <div class="" style="margin-top:10px !important;">
                      <label for="artist_state" class="field select select_outer">
                        <select class="form-control new_select" id="hide_choose_sub_type" name="reg_stage_id" style="color: black;">
                          <option value=''>Select Stage</option>
                          <?php $__currentLoopData = $stage; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $stages): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                            <option value="<?php echo e($stages->stage_id); ?>" <?php echo e($stages->stage_id == $reg_stage_id ? 'selected="selected"' : ''); ?> ><?php echo e($stages->stage_name); ?> </option>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                        <i class="arrow double arrow_new"></i>
                      </label>
                  </div>
                </div>

                <div class="col-md-2" style="display: none" id="from_date_hide">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 10px;">
                    <?php echo Form::text('from_date','',array('class' => 'form-control fromdate_search new_text','placeholder' => 'Reg. From Date', 'autocomplete' => 'off' )); ?>

                    <label for="pincode" class="field-icon search_new">
                      <i class="fa fa-calendar"></i>
                    </label>
                  </label>
                </div>

                <div class="col-md-2" style="display: none" id="to_date_hide">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 10px;">
                    <?php echo Form::text('to_date','',array('class' => 'form-control todate new_text','placeholder' => 'Reg. To Date', 'autocomplete' => 'off' )); ?>

                    <label for="pincode" class="field-icon search_new">
                      <i class="fa fa-calendar"></i>
                    </label>
                  </label>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-2" style="display: none" id="next_date_hide">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 10px;">
                    <?php echo Form::text('further_date','',array('class' => 'form-control fromdate_search new_text','placeholder' => 'Next Date', 'autocomplete' => 'off', 'id' => '' )); ?>

                    <label for="pincode" class="field-icon search_new">
                      <i class="fa fa-calendar"></i>
                    </label>
                  </label>
                </div>

                <div class="col-md-3">
                  <div class="" style="margin-top:10px !important;">
                    <label for="artist_state" class="field select select_outer">
                      <select id="multiselect2" multiple="multiple" name="selected_value[]" onchange="selected_fields(this.value)" style="color: black;">
                          <option value="1">Case No.</option>
                          <!-- <option value="2">Select Year</option> -->
                          <option value="3">NCV No.</option>
                          <option value="4">Select Stage</option>
                          <option value="5">Reg. From Date</option>
                          <option value="6">Reg. To Date</option>
                          <option value="7">Next Date</option>
                        </select>
                    </label>
                  </div>
                </div>
                
                <!-- <div class="clearfix"></div>
          
                 -->

                <div class="col-md-1 pull-right" style="margin-top:10px !important;">
                  <button type="submit" name="search" class="button btn-primary new_button"> Search </button>
                </div>
              <?php echo Form::close(); ?>             
                <div class="col-md-1" style="margin-top:10px !important;">
                   <a href="<?php echo e(url('/advocate-panel/view-peshi/')); ?>"><?php echo Form::submit('Default', array('class' => 'btn btn-primary new_button_2', 'id' => 'maskedKey')); ?></a>
                </div>  

                <div class="col-md-2 mt10">
                   <a href="<?php echo e(url('/advocate-panel/view-peshi/all')); ?>"><?php echo Form::button('Show All Records', array('class' => 'btn btn-primary', 'id' => 'maskedKey')); ?></a>
                </div>

                 

                <div class="col-md-2" style="margin-top:10px !important;">
                  <a href="#" style="text-transform: capitalize; text-decoration:none;" onclick="print_cause_list()" > <?php echo Form::button('Cause List Date', array('class' => 'btn btn-primary new_button_2', 'id' => 'maskedKey')); ?> </a>
                    <div id="print_cause_list" class="modal fade in" role="dialog">
                      <div class="modal-dialog" style="width:700px; margin-top:100px;">
                        <div class="modal-content">
                          <div class="modal-header" style="padding-bottom: 35px;">
                            <button type="button" class="close" data-dismiss="modal" onclick="close_button_print_cause_list()">&times;</button>
                            <h4 class="modal-title pull-left"> Cause List Date </h4>
                          </div>
                          <section id="content" class="table-layout animated fadeIn">
                            <div>
                              <div class="center-block">
                                <div class="panel panel-primary panel-border top mb35">
                                  <div class="panel-body bg-light dark">
                                    <div class="tab-content pn br-n admin-form">
                                      <div id="tab1_1" class="tab-pane active">
                                        
                                        <?php echo Form::open(['url'=>'/advocate-panel/print-cause-list','name'=>'form', 'id'=>'print_cause_list_form' ,'autocomplete'=>'off']); ?>


                                        <div class="row">                                                
                                          <div class="col-md-12">
                                            <label for="level" class="field prepend-icon" style="margin-top:10px !important;">
                                              <select class=" form-control" name="court_type" id="level" >
                                                <!-- <option value="">Court Type</option> -->
                                                <option value="1"> Trial Court</option>
                                                <option value="2" selected> High Court</option>
                                              </select>
                                            </label>
                                          </div>

                                          <div class="col-md-12">
                                            <label for="pincode" class="field prepend-icon" style="margin-top: 10px;">
                                              <?php echo Form::text('further_date','',array('class' => 'form-control fromdate','placeholder' => 'Cause List Date', 'autocomplete' => 'off', 'id' => 'cause_further_date' )); ?>

                                              <label for="pincode" class="field-icon">
                                                <i class="fa fa-calendar"></i>
                                              </label>
                                            </label>
                                          </div>

                                                                                     
                                        </div>

                                        <div class="text-right" style="margin-top: 10px;">
                                            <?php echo Form::submit('Save', array('class' => 'button btn-primary mysave', 'id' => 'maskedKey')); ?>

                                        </div>   
                                            <?php echo Form::close(); ?>

                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </section>
                          <div class="clearfix"></div>
                        </div> 
                      </div>
                    </div>
                </div>

            </div>
          </div>

          <div class="panel-body pn">
              <!-- <?php echo Form::open(['url'=>'/advocate-panel/view-peshi','name'=>'form']); ?> -->
              <div class="table-responsive" style="overflow: auto;">
                <table class="table admin-form table-bordered table-striped theme-warning tc-checkbox-1 fs13" id="datatable">
                  <thead>
                    <tr class="bg-light">
                      <!-- <th style="width:90px !important;" class="text-left">
                        <label class="option block mn">
                          <input type="checkbox" id="check_all"> 
                          <span class="checkbox mn"></span>
                          Select All
                        </label>
                      </th> -->
                      <th> S.No</th>
                      <th class="text-left">File No.</th>
                      <th class="text-left">Type of Case</th>
                      <th class="text-left">Case No.</th>
                      <th class="text-left">NCV No.</th>
                      <th class="text-left">Title of Case</th>
                      <th class="text-left">Stage</th>                      
                      <th class="text-left">Previous Date</th>
                      <th class="text-left">Peshi Entry</th>
                      <th class="text-left">Cause List</th>
                      <th class="text-left">Result/Next Date</th>
                    </tr>
                  </thead>
                  <tbody>

                  <?php if(count($get_record) != 0): ?>  
                  
                  <?php $sno = 0; ?>
                  <?php $__currentLoopData = $get_record; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_records): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>     

                  <?php $sno = $sno+1; ?>         
                    <tr>
                      <!-- <td class="text-left" style="padding-left: 18px;">
                        <label class="option block mn">
                          <input type="checkbox" name="check[]" class="check" value="<?php echo e($get_records->pessi_id); ?>">
                          <span class="checkbox mn"></span>
                        </label>
                      </td> -->
                      <td> <?php echo e($sno); ?> </td>

                      <td class="text-left" style="padding-left: 20px;"> <a href="<?php echo e(url('/advocate-panel/case-detail')); ?>/<?php echo e(sha1($get_records->reg_id)); ?>"> <?php if($get_records->reg_file_no == ""): ?> ----- <?php else: ?> <?php echo e($get_records->reg_file_no); ?> <?php endif; ?> </a> </td>
                      
                      <td class="text-left" style="padding-left: 20px;"> <?php if($get_records->case_name == ""): ?> ----- <?php else: ?> <?php echo e($get_records->case_name); ?> <?php endif; ?> </td>

                      <td class="text-left" style="padding-left: 20px;">  <?php if($get_records->reg_case_number == ""): ?> ----- <?php else: ?> <?php echo e($get_records->reg_case_number); ?> <?php endif; ?> </td>

                      <td class="text-left" style="padding-left: 20px;">  <?php if($get_records->reg_vcn_number == ""): ?> ----- <?php else: ?> <?php echo e($get_records->reg_vcn_number); ?> <?php endif; ?> </td>
                      
                      <td class="text-left" style="padding-left: 20px;">  <?php echo e($get_records->reg_petitioner); ?> v/s <?php echo e($get_records->reg_respondent); ?>  </td>


                      <td class="text-left" style="padding-left: 20px;"> <?php if($get_records->stage_name == ""): ?> ----- <?php else: ?> <?php echo e($get_records->stage_name); ?> <?php endif; ?> </td>


<!--                       <td class="text-left" style="padding-left: 20px;">
                        <?php if($get_records->reg_court == 2): ?>
                               High Court
                        <?php elseif($get_records->reg_court == 1): ?>
                               Trial Court
                        <?php endif; ?>
                      </td>
 -->
                      
                      
                      <td class="text-left" style="padding-left: 20px;">  
                        <?php if($get_records->pessi_prev_date == "1970-01-01" || $get_records->pessi_prev_date == ""): ?>
                          -----
                        <?php else: ?>
                          <?php echo e(date('d F Y',strtotime($get_records->pessi_prev_date))); ?>

                        <?php endif; ?>
                      </td>

                      <td style="padding-left: 20px;">
                        <a href="#" style="text-transform: capitalize; text-decoration:none;" onclick="edit_pessi('<?php echo e($get_records->pessi_id); ?>')" > Peshi Entry </a>
                      </td>

                      <td style="padding-left: 20px;">
                        <?php if($get_records->reg_court == 2): ?>
                               <a href="#" style="text-transform: capitalize; text-decoration:none;" onclick="cause_list(<?php echo e($get_records->pessi_id); ?>)" > Cause List </a>
                        <?php elseif($get_records->reg_court == 1): ?>
                               -------
                        <?php endif; ?>
                      </td>

                      <td style="padding-left: 20px;">
                        <?php if($get_records->pessi_choose_type == 0): ?>

                        <?php if($get_records->pessi_further_date == "" || $get_records->pessi_further_date == "1970-01-01"): ?>
                          Next Date:-  ------
                        <?php else: ?> 
                          Next Date:-  <?php echo e(date('d F Y',strtotime($get_records->pessi_further_date))); ?>

                        <?php endif; ?>
                          
                        <?php elseif($get_records->pessi_choose_type == 1): ?>
                          Decide :- <?php if($get_records->pessi_decide_id == 1): ?> Case in Favour <?php elseif($get_records->pessi_decide_id == 2): ?> Case in against <?php elseif($get_records->pessi_decide_id == 3): ?> Withdraw <?php elseif($get_records->pessi_decide_id == 4): ?> None <?php endif; ?>
                        <?php else: ?>
                          Due Course
                        <?php endif; ?>
                      </td>

                    </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                  <?php else: ?> 

                  

                  <?php endif; ?>

                  </tbody>
                </table>
              </div>
              <?php echo Form::close(); ?>

          </div>
          <div class="panel-body pn">
            <div class="table-responsive">
              <table class="table admin-form theme-warning tc-checkbox-1 fs13">                                
                <tbody>
                  <tr class="">
                     <!-- <th class="text-left">
                      <button type="button" class="btn btn-primary" onclick="go_delete()"><i class="glyphicon glyphicon-trash"></i> Delete Multiple </button>
                    </th> -->

                    <?php if(count($get_record) == 0): ?>
                      
                      
                    
                    <?php else: ?> 

                      <th class="text-right">
                        <?php echo e($get_record->links()); ?>

                      </th>

                    <?php endif; ?>
                    
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

<?php $__currentLoopData = $get_record; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_records): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

<div id="edit_pessi<?php echo e($get_records->pessi_id); ?>" class="modal" role="dialog" style="overflow: scroll;"> 
  <div class="modal-dialog" style="width:700px; margin-top:80px;">
    <div class="modal-content">
      <div class="modal-header" style="padding-bottom: 35px;">
        <button type="button" class="close" data-dismiss="modal" onclick="close_button_edit_pessi(<?php echo e($get_records->pessi_id); ?>)" >&times;</button>
        <h4 class="modal-title pull-left"> Peshi Entry </h4>
      </div>
      <section id="content" class="table-layout animated fadeIn">
        <div>
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                  <div id="tab1_1" class="tab-pane active">
                    
                    <?php echo Form::open(['name'=>'form_add_pessi','url'=>'advocate-panel/insert-peshi/'.$get_records->pessi_id,'id'=>'form_add_pessi'.$get_records->pessi_id ,'autocomplete'=>'off']); ?>


                    <div class="row">                                                
                        
                        <div class="col-md-12">
                          <div class="section" style="margin-bottom: 40px;">
                            <div class="col-md-3">
                              <label class="option" style="font-size:12px;">
                                <?php echo Form::radio('choose_type','0',$get_records['pessi_choose_type'] == 0 ? 'checked' : '', array('onclick' => "show_further('$get_records->pessi_id')", 'class' => 'check' )); ?>

                                <span class="radio" style="border: 2px solid #4a89dc !important;"></span> Next Date
                              </label>
                            </div>

                            <div class="col-md-3">
                              <label class="option" style="font-size:12px;">
                                <?php echo Form::radio('choose_type','1',$get_records['pessi_choose_type'] == 1 ? 'checked' : '', array('onclick' => "show_decide('$get_records->pessi_id')", 'class' => 'check' )); ?>

                                <span class="radio" style="border: 2px solid #4a89dc !important;"></span> Decide
                              </label>
                            </div>

                            <div class="col-md-3">
                              <label class="option" style="font-size:12px;">
                                <?php echo Form::radio('choose_type','2',$get_records['pessi_choose_type'] == 2 ? 'checked' : '', array('onclick' => "show_due_course('$get_records->pessi_id')", 'class' => 'check' )); ?>

                                <span class="radio" style="border: 2px solid #4a89dc !important;"></span> Due Course
                              </label>
                            </div>

                          </div>

                          <div class="section"></div>
                          <div class="clearfix"></div>
                        </div>

                        <div class="col-md-6" <?php if($get_records['pessi_choose_type'] == 1): ?> style="display:none;" <?php endif; ?>  id="show_further_date<?php echo e($get_records['pessi_id']); ?>" >
                          <div class="section section_from">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Next Date </label>  
                            <label for="level_name" class="field prepend-icon" id="discount_date_from_lb">
                            <?php echo Form::hidden('current_date',date('d-m-Y'),array('class' => 'gui-input current_date','placeholder' => '','id'=>'current_date'  )); ?>


                              <?php if($get_records->pessi_further_date == "" || $get_records->pessi_further_date == "1970-01-01"): ?>

                              <?php echo Form::text('further_date','',array('class' => 'gui-input disposal_to remove_further_date_autofocus','placeholder' => '','id'=>'further_date_autofocus_'.$get_records->pessi_id  )); ?>


                              <?php else: ?> 

                              <?php echo Form::text('further_date',date('d-m-Y',strtotime($get_records->pessi_further_date)),array('class' => 'gui-input disposal_to remove_further_date_autofocus','placeholder' => '','id'=>'further_date_autofocus_'.$get_records->pessi_id   )); ?>


                              <?php endif; ?>

                              

                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-calendar"></i>
                              </label>                           
                              <div></div>
                            </label>
                            <!-- <label for="level_name" class="field-label"> <b>Note :-</b> Date format will be dd/mm/yyyy </label> -->

                            <!-- <label for="level_name" class="field-label" style="color: rgb(222, 136, 138);" > <b>Note :-</b> Date format will be dd/mm/yyyy </label> -->
                          </div>
                        </div>
                        <div class="col-md-12">
                        <label for="level_name" class="field-label" style="color: rgb(222, 136, 138);" > <b>Note :-</b> Please select date greater than from the next date. If you want to select previous date then first delete that entry.   </label>
                      </div>

                        <div class="col-md-6" <?php if($get_records['pessi_choose_type'] == 1): ?> <?php else: ?> style="display: none;" <?php endif; ?> id="show_decide<?php echo e($get_records['pessi_id']); ?>">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;"> Decide : </label>
                              <!-- <label for="artist_state" class="field select"> -->
                                <label for="level" class="field prepend-icon">
                                <select class="form-control select-text" id="decide" name="decide" >
                                  <option value=''>Select Decide </option>
                                  <option value='1' <?php echo e($get_records->pessi_decide_id == 1 ? 'selected="selected"' : ''); ?> >Case In Favour </option>      
                                  <option value='2' <?php echo e($get_records->pessi_decide_id == 2 ? 'selected="selected"' : ''); ?> >Case in against </option>
                                  <option value='3' <?php echo e($get_records->pessi_decide_id == 3 ? 'selected="selected"' : ''); ?> > Withdraw </option>
                                  <option value='4' <?php echo e($get_records->pessi_decide_id == 4 ? 'selected="selected"' : ''); ?> > None </option>
                                </select>
                                <i class="arrow double"></i>
                              </label>
                          </div>
                        </div>

                        <div class="col-md-2" style="display: none;" id="show_due_course<?php echo e($get_records['pessi_id']); ?>">
                          <button type="button" class="btn btn-success br2 btn-xs field select form-control" style="margin-top: 23px; margin-bottom: 22px;" > Due Course </button>
                        </div>

                        <div class="col-md-12">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;"> Select Stage : </label>
                              <!-- <label for="artist_state" class="field select"> -->
                                <label for="level" class="field prepend-icon">
                                <select class="form-control select-text" name="reg_stage" id="reg_stage">
                                  <!-- <option value=''>Select Stage</option> -->
                                    <?php $__currentLoopData = $get_stage; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $stages): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                      <option value="<?php echo e($stages->stage_id); ?>" <?php echo e($stages->stage_id == $get_records->pessi_statge_id ? 'selected="selected"' : ''); ?> ><?php echo e($stages->stage_name); ?> </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                
                                </select>
                                <i class="arrow double"></i>
                              </label>
                          </div>
                        </div>

                        <div class="col-md-12">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;"> Order Sheet :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              <?php echo Form::textarea('order_sheet',$get_records->pessi_order_sheet,array('class' => 'gui-textarea accc1','placeholder' => '', 'id'=>'rupam_'.$get_records->pessi_id )); ?>

                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-comment"></i>
                              </label                           
                            </label>
                          </div>
                        </div>

                        

<div class="col-md-12">
  <div class="field" align="left">
    <label for="documents" class="field-label" style="font-weight:600;" > <span style="float: left;margin-bottom: 10px;"> Send SMS </span> </label>
  </div>
</div>

<div class="col-md-12">
  <div class="section" style="margin-bottom: 40px;">  
    <div class="col-md-3">
      <label class="option block mn" style="font-size:12px;"> 
        <?php echo Form::checkbox('sms_type[]','1','', array( 'class' => 'check' , 'class' => 'check' )); ?>

        <span class="checkbox mn" style="border: 2px solid #4a89dc !important;"></span> Client
      </label>
    </div>

    <div class="col-md-3">
      <label class="option block mn" style="font-size:12px;">
        <?php echo Form::checkbox('sms_type[]','2', '', array( 'class' => 'check'  )); ?>

        <span class="checkbox mn" style="border: 2px solid #4a89dc !important;"></span> Referred
      </label>
    </div>

    <div class="col-md-3">
      <label class="option block mn" style="font-size:12px;">
        <?php echo Form::checkbox('sms_type[]','3','', array( 'class' => 'check', 'id' => 'sms_type' ,'onclick'=>'reg_sms_type()'  )); ?>

        <span class="checkbox mn" style="border: 2px solid #4a89dc !important;"></span> Other
      </label>
    </div>
  </div>
</div>

<div class="col-md-4" style="display: none;" id="show_other_mobile">
  <div class="section">
    <label for="level_name" class="field-label" style="font-weight:600;"> Other Mobile No. :  </label>  
    <label for="level_name" class="field prepend-icon">
      <?php echo Form::text('reg_other_mobile','',array('class' => 'gui-input','placeholder' => '' )); ?>

        <label for="Account Mobile" class="field-icon">
        <i class="fa fa-pencil"></i>
      </label>                           
    </label>
  </div>
</div>

<div class="col-md-8">
  <div class="section">
    <label for="level_name" class="field-label" style="font-weight:600;"> Text :  </label>
    <label for="level_name" class="field prepend-icon">
      <?php echo Form::text('reg_sms_text', '' ,array('class' => 'gui-input','placeholder' => '' )); ?>

        <label for="Account Mobile" class="field-icon">
        <i class="fa fa-pencil"></i>
      </label>                           
    </label>
  </div>
</div>
                      
                    </div>

                  <div class="panel-footer text-right">
                      <?php echo Form::submit('Save', array('class' => 'button btn-primary mysave', 'id' => 'maskedKey')); ?>

                      <!-- <?php echo Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')); ?> -->
                  </div>   
                    <?php echo Form::close(); ?>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <div class="clearfix"></div>
    </div> 
  </div>
</div>





<div id="cause_list<?php echo e($get_records->pessi_id); ?>" class="modal fade in" role="dialog" style="overflow: scroll;" >
  <div class="modal-dialog" style="width:700px; margin-top:80px;">
    <div class="modal-content">
      <div class="modal-header" style="padding-bottom: 35px;">
        <button type="button" class="close" data-dismiss="modal" onclick="close_button_cause_list(<?php echo e($get_records->pessi_id); ?>)" >&times;</button>
        <h4 class="modal-title pull-left"> Cause List </h4>
      </div>
      
      <section id="content" class="table-layout animated fadeIn">
        <div class="">
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                  <div id="tab1_1" class="tab-pane active">
                    
                    <?php echo Form::open(['name'=>'form_add_question','url'=>'advocate-panel/insert-cause-list/'.$get_records->pessi_id,'id'=>'form_add_question'.$get_records->pessi_id,'files'=>'ture' ,'autocomplete'=>'off']); ?>


                    <div class="row">

                      <div class="col-md-6">
                        <div class="section section_from">
                          <label for="level_name" class="field-label" style="font-weight:600;"> Date   </label>  
                          <label for="level_name" class="field prepend-icon" id="discount_date_from_lb">

                          <?php echo Form::hidden('current_date',date('d-m-Y'),array('class' => 'gui-input current_date','placeholder' => '','id'=>'current_date'  )); ?>


                          <?php if($get_records->pessi_further_date == "" || $get_records->pessi_further_date == "1970-01-01"): ?>

                            <?php echo Form::text('further_date','',array('class' => 'gui-input disposal_to cause_further_date','placeholder' => '','id'=>'cause_further_date'.$get_records->pessi_id  )); ?>


                          <?php else: ?>

                            <?php echo Form::text('further_date',date('d-m-Y',strtotime($get_records->pessi_further_date)),array('class' => 'gui-input disposal_to cause_further_date','placeholder' => '','id'=>'cause_further_date'.$get_records->pessi_id  )); ?> 

                          <?php endif; ?>
                              <label for="Account Mobile" class="field-icon">
                              <i class="fa fa-calendar"></i>
                            </label>                           
                          </label>
                          <!-- <label for="level_name" class="field-label"> <b>Note :-</b> Date format will be dd/mm/yyyy </label> -->
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;"> Select Stage : </label>
                            <!-- <label for="artist_state" class="field select"> -->
                              <label for="level" class="field prepend-icon">
                              <select class="form-control select-text" name="reg_stage" id="reg_stage<?php echo e($get_records->pessi_id); ?>">
                                <!-- <option value=''>Select Stage</option> -->
                                  <?php $__currentLoopData = $get_stage; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $stages): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                    <option value="<?php echo e($stages->stage_id); ?>" <?php echo e($stages->stage_id == $get_records->pessi_statge_id ? 'selected="selected"' : ''); ?> ><?php echo e($stages->stage_name); ?> </option>
                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                
                              </select>
                              <i class="arrow double"></i>
                            </label>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;"> Hon'ble Justice : </label>
                            <!-- <label for="artist_state" class="field select"> -->
                              <label for="level" class="field prepend-icon">
                              <select class="form-control select-text" name="honarable_justice" id="honarable_justice<?php echo e($get_records->pessi_id); ?>">
                                <option value=''>Select Hon'ble Justice</option>
                                  <?php $__currentLoopData = $get_judge; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_judges): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                    <option value="<?php echo e($get_judges->judge_id); ?>" <?php echo e($get_judges->judge_id == $get_records->honaurable_justice ? 'selected="selected"' : ''); ?> ><?php echo e($get_judges->judge_name); ?> </option>
                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                
                              </select>
                              <i class="arrow double"></i>
                            </label>
                        </div>
                      </div>

                      <div class="col-md-6">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;"> D.B. : </label>
                              <!-- <label for="artist_state" class="field select"> -->
                                <label for="level" class="field prepend-icon">
                                <select class="form-control select-text" name="honarable_justice_db" id="honarable_justice_db<?php echo e($get_records->pessi_id); ?>">
                                  <option value=''>Select Hon'ble Justice</option>
                                    <?php $__currentLoopData = $get_judge; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_judges): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                      <option value="<?php echo e($get_judges->judge_id); ?>" <?php echo e($get_judges->judge_id == $get_records->honarable_justice_db ? 'selected="selected"' : ''); ?> ><?php echo e($get_judges->judge_name); ?> </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                
                                </select>
                                <i class="arrow double"></i>
                              </label>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;"> Court No. :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              <?php echo Form::text('court_no',$get_records->court_number,array('class' => 'gui-input','placeholder' => '','id'=>'court_no'.$get_records->pessi_id )); ?>

                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-pencil"></i>
                              </label>                           
                            </label>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;"> Serial No. :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              <?php echo Form::text('serial_no',$get_records->serial_number,array('class' => 'gui-input','placeholder' => '','id'=>'serial_no'.$get_records->pessi_id  )); ?>

                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-pencil"></i>
                              </label>                           
                            </label>
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;"> Page No. :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              <?php echo Form::text('page_no',$get_records->page_number,array('class' => 'gui-input','placeholder' => '','id'=>'page_no'.$get_records->pessi_id )); ?>

                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-pencil"></i>
                              </label>                           
                            </label>
                          </div>
                        </div> 


                     </div>
                    <div class="col-md-12">
                      
                      

                      <div class="panel-footer text-right">

                        <b  style="font-size: 20px;">
                          <?php if($get_records->pessi_choose_type == 0): ?>
                              
                          <?php elseif($get_records->pessi_choose_type == 1): ?>
                            Decide :- <?php if($get_records->pessi_decide_id == 1): ?> Case in Favour <?php elseif($get_records->pessi_decide_id == 2): ?> Case in against <?php elseif($get_records->pessi_decide_id == 3): ?> Withdraw <?php elseif($get_records->pessi_decide_id == 4): ?> None <?php endif; ?>
                          <?php else: ?>
                            Due Course
                          <?php endif; ?> 
                        </b> &nbsp &nbsp &nbsp
                      
                        <?php echo Form::button('Save', array('class' => 'button btn-primary mysave', 'id' => 'maskedKey', 'onclick' => "submit_cause_list($get_records->pessi_id)" )); ?>

                      </div>    
                    </div>

                       
                    <?php echo Form::close(); ?>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <div class="clearfix"></div>
    </div> 
  </div>
</div>



<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


<?php echo Form::hidden('min_temp_array',$min_temp_array,array('class' => '','placeholder' => '','id'=>'min_temp_array'  )); ?>

<?php echo Form::hidden('max_temp_array',$max_temp_array,array('class' => '','placeholder' => '','id'=>'max_temp_array'  )); ?>

<style type="text/css">
.view a{
  color:#fff !important;
}
</style>
<style type="text/css">
.dt-panelfooter{
  display: none !important;
}
.dataTables_filter{
  float: left;
}


</style>

<script type="text/javascript">

  function submit_cause_list(pessi_id){

    //form_add_question
    //if(confirm("Are you sure you want to update the date?")) {
        $("#form_add_question"+pessi_id).submit();
    //}
  }

  function edit_pessi(pessi_id){

    //remove_further_date_autofocus
    //alert(document.getElementById('rupam_'+pessi_id).value());
//     min_temp_array = $("#min_temp_array").val();
//     max_temp_array = $("#max_temp_array").val();

//     for(var i = min_temp_array; i < max_temp_array; i++){

// //      alert(i);
//       document.getElementById('further_date_autofocus_'+i).setAttribute('autofocus', '');
//     }

//     document.getElementById('further_date_autofocus_'+pessi_id).setAttribute('autofocus', 'autofocus');
    //document.getElementById('further_date_autofocus_'+pessi_id).setAttribute('autofocus', '');
    //alert(pessi_id);

    //document.getElementById('further_date_autofocus_'+pessi_id).focus(); 

    // var cls = document.getElementsByClassName("remove_further_date_autofocus");

    // for(var i = 0; i < cls.length; i++) {
    //    cls[i].removeAttribute('autofocus');
    // }

    // document.getElementById("further_date_autofocus_"+pessi_id).setAttribute('autofocus', 'autofocus');

  

  }


  $(document).on("keyup",".disposal_to",function(){
      $('.mysave').removeAttr('disabled');
      var  currentdate = this.value;
      var n=$(this);    

      var checkdate = /^([0]?[1-9]|[1|2][0-9]|[3][0|1])[-]([0]?[1-9]|[1][0-2])[-]([0-9]{4}|[0-9]{4})$/.test(currentdate);
      
      if(checkdate == true) {

        value=currentdate.split("-");
        var newDate_notice =value[1]+"/"+value[0]+"/"+value[2];
        var notice_status_date = new Date(newDate_notice).getTime();
        
        var notice_issue_date = $('#current_date').val();
        notice_issue_date = notice_issue_date.split("-");
        var newDate_notice_issue = notice_issue_date[1]+"/"+notice_issue_date[0]+"/"+notice_issue_date[2];
        var notice_issue_date = new Date(newDate_notice_issue).getTime();


        //if(notice_status_date > notice_issue_date){
          n.next().next().html("");
          n.next().next().css("color", "#DE888A");
          n.parent().addClass("state-success");
          $('.mysave').removeAttr('disabled');
        // } else {
        //   n.next().next().html("Next date must be greater than current date.");
        //   n.next().next().css("color", "#DE888A");
        //   n.parent().addClass("state-error");
        //   n.parent().removeClass("state-success");
        //   $(this).closest('.mysave').html('');
        //   $('.mysave').attr('disabled','disabled');
        //   return false;
        // }

        
      } else {
        n.next().next().html("Enter date in dd-mm-yyyy format.");
        n.next().next().css("color", "#DE888A");
        n.parent().addClass("state-error");
        n.parent().removeClass("state-success");
        $(this).closest('.mysave').html('');
        $('.mysave').attr('disabled','disabled');
        return false;
      }
  });


  $(document).on("change keyup keydown click paste",".cause_further_date",function(){
      // alert('asd');
      var dateid = $(this).attr('id');
      var suffix = dateid.match(/\d+/);
      // alert(suffix);
      $('.mysave').removeAttr('disabled');
      var  currentdate = this.value;
      var n=$(this); 
      
      $('#reg_stage'+suffix).val('');
      $('#honarable_justice'+suffix).val('');
      $('#honarable_justice_db'+suffix).val('');  
      $('#court_no'+suffix).val('');
      $('#serial_no'+suffix).val('');
      $('#page_no'+suffix).val('');

      // alert(currentdate);   

      BASE_URL = '<?php echo e(url('/')); ?>';
      $.ajax({
        url:BASE_URL+"/advocate-panel/get-cause-list/"+suffix+"/"+currentdate,
        success: function(result){
          if(result.status == true){
            $('#reg_stage'+suffix).val(result.pessi_statge_id);
            $('#honarable_justice'+suffix).val(result.honaurable_justice);
            $('#honarable_justice_db'+suffix).val(result.honarable_justice_db);  
            $('#court_no'+suffix).val(result.court_number);
            $('#serial_no'+suffix).val(result.serial_number);
            $('#page_no'+suffix).val(result.page_number);
          } else {
            $('#honarable_justice'+suffix).val('');
            $('#honarable_justice_db'+suffix).val('');  
            $('#court_no'+suffix).val('');
            $('#serial_no'+suffix).val('');
            $('#page_no'+suffix).val('');
          }
        }
      });

      var checkdate = /^([0]?[1-9]|[1|2][0-9]|[3][0|1])[-]([0]?[1-9]|[1][0-2])[-]([0-9]{4}|[0-9]{4})$/.test(currentdate);
      
      if(checkdate == true) {

        value=currentdate.split("-");
        var newDate_notice =value[1]+"/"+value[0]+"/"+value[2];
        var notice_status_date = new Date(newDate_notice).getTime();
        
        var notice_issue_date = $('#current_date').val();
        notice_issue_date = notice_issue_date.split("-");
        var newDate_notice_issue = notice_issue_date[1]+"/"+notice_issue_date[0]+"/"+notice_issue_date[2];
        var notice_issue_date = new Date(newDate_notice_issue).getTime();


      //  if(notice_status_date > notice_issue_date){
          n.next().next().html("");
          n.next().next().css("color", "#DE888A");
          n.parent().addClass("state-success");
          $('.mysave').removeAttr('disabled');
      //  } else {
        //   n.next().next().html("Next date must be greater than current date.");
        //   n.next().next().css("color", "#DE888A");
        //   n.parent().addClass("state-error");
        //   n.parent().removeClass("state-success");
        //   $(this).closest('.mysave').html('');
        //   $('.mysave').attr('disabled','disabled');
        //   return false;
        // }

        
      } else {
        n.next().next().html("Enter date in dd-mm-yyyy format.");
        n.next().next().css("color", "#DE888A");
        n.parent().addClass("state-error");
        n.parent().removeClass("state-success");
        $(this).closest('.mysave').html('');
        $('.mysave').attr('disabled','disabled');
        return false;
      }
  });
  

  jQuery(document).ready(function() {

    "use strict";

    $('#datatable').dataTable({
      "aoColumnDefs": [{
        'bSortable': false,
        'aTargets': [-1]
      }],
      "oLanguage": {
        "oPaginate": {
          "sPrevious": "",
          "sNext": ""
        }
      },
      "iDisplayLength": -1,
      
      "sDom": '<"dt-panelmenu clearfix"lfr>t<"dt-panelfooter clearfix"ip>',
      "oTableTools": {
        "sSwfPath": "vendor/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
      }
    });

    $('.dataTables_filter input').attr("placeholder", "Enter Terms...");
    //$('div.dataTables_filter input').focus();
    $('#datatable_length').hide();

  });



  // sms type

  function reg_sms_type(){

    if ($('#sms_type').is(":checked")){
      $('#show_other_mobile').show();
    } else {
      $('#show_other_mobile').hide();      
    }

  }


  function selected_fields(){


    var new_selected = $("#multiselect2").val();

    if(new_selected == null){
      $('#case_no_hide').css('display','none');
      $('#year_hide').css('display','none');      
      $('#ncv_no_hide').css('display','none');
      $('#stage_hide').css('display','none');
      $('#from_date_hide').css('display','none');
      $('#to_date_hide').css('display','none');
      $('#next_date_hide').css('display','none');
    }    

    var selected = $("#multiselect2").val().toString();

    if(selected != null){
      var values   = selected.split(",");      
    }


    if(selected.includes("1") == true){  
      $('#case_no_hide').css('display','block');  
    } else {  
      $('#case_no_hide').css('display','none');  
    }

    if(selected.includes("2") == true){
      $('#year_hide').css('display','block');
    } else {
      $('#year_hide').css('display','none');
    }

    if(selected.includes("3") == true){
      $('#ncv_no_hide').css('display','block');
    } else {
      $('#ncv_no_hide').css('display','none');
    }

    if(selected.includes("4") == true){
      $('#stage_hide').css('display','block');
    } else {
      $('#stage_hide').css('display','none');
    }

    if(selected.includes("5") == true){
      $('#from_date_hide').css('display','block');
    } else {
      $('#from_date_hide').css('display','none');
    }

    if(selected.includes("6") == true){
      $('#to_date_hide').css('display','block');
    } else {
      $('#to_date_hide').css('display','none');
    }

    if(selected.includes("7") == true){
      $('#next_date_hide').css('display','block');
    } else {
      $('#next_date_hide').css('display','none');
    }


    
    // alert(values[0]);
    // alert(values[1]);
    // alert(values[2]);
    // alert(values[3]);
    // alert(values[4]);
    // alert(values[5]);
    // alert(values[6]);

  }
  
  function show_further(pessi_id){
    document.getElementById("show_further_date"+pessi_id).style.display = "block";
    document.getElementById("show_decide"+pessi_id).style.display = "none";
    document.getElementById("show_due_course"+pessi_id).style.display = "none";
  }


  function show_decide(pessi_id){
    document.getElementById("show_further_date"+pessi_id).style.display = "none";
    document.getElementById("show_decide"+pessi_id).style.display = "block";
    document.getElementById("show_due_course"+pessi_id).style.display = "none";
  }


  function show_due_course(pessi_id){
    document.getElementById("show_further_date"+pessi_id).style.display = "none";
    document.getElementById("show_decide"+pessi_id).style.display = "none";
  //  document.getElementById("show_due_course").style.display = "block";
  }

</script>

<script type="text/javascript">

  jQuery(document).ready(function() {

    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "This field contains alphabets only");

    jQuery.validator.addMethod("checkemail", function(value, element) 
    {
    return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
    }, "Enter a VALID email address"); 

    $.validator.addMethod('minStrict', function (value, el, param) {
     return value > param; 
    });
    $.validator.addMethod('maxStrict', function (value, el, param) {
     return value < param; 
    });

    jQuery.validator.addMethod("checkdate", function(value, element) 
    {
    return this.optional(element) || /^([0]?[1-9]|[1|2][0-9]|[3][0|1])[-]([0]?[1-9]|[1][0-2])[-]([0-9]{4}|[0-9]{4})$/.test(value);
    }, "Enter date in dd-mm-yyyy format");

    /* @custom  validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#form_add_pessi").validate({

      /* @validation  states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation  rules 
      ------------------------------------------ */

      rules: {
        // case_no: {
        //   required: true
        // },
        // reg_stage: {
        //   required: true
        // },
        // order_sheet: {
        //   required: true
        // },
        // choose_type: {
        //   required: true
        // },
        // further_date: {
        //   required: true
        // },
        // decide: {
        //   required: true
        // }
      },
      /* @validation  error messages 
      ---------------------------------------------- */

      messages: {
        // reg_stage: {
        //   required: 'Please select stage'
        // },
        // order_sheet: {
        //   required: 'Please enter order sheet'
        // },
        // type: {
        //   required: 'Please Fill Required Type'
        // },
        // further_date: {
        //   required: 'Please select further date'
        // },
        // decide: {
        //   required: 'Please select decide'
        // },
      },

      /* @validation  highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });



    $("#print_cause_list_form").validate({

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      rules: {
        further_date: {
          required: true,
          checkdate: true
        },
      },
      /* @validation  error messages 
      ---------------------------------------------- */

      messages: {
        // reg_stage: {
        //   required: 'Please select stage'
        // },
      },

      /* @validation  highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });



  });


  // setInterval(function(){ 
  //   var dt_val = $("#cause_further_date").val();

  //   alert(dt_val)
  //   // if (dt_val!="") {
  //   //     $("#discount_date_from_lb").addClass("state-success");
  //   //     $("#discount_date_from_lb").removeClass("state-error");
  //   //     $(".section_from .state-error").html("");
  //   // }
  // }, 500);



  jQuery(document).ready(function() {

    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "This field contains alphabets only");

    jQuery.validator.addMethod("letternumeric", function(value, element) 
    {
    return this.optional(element) || /^[a-z,0-9," "]+$/i.test(value);
    }, "This field contains only alphabets and numeric only");

    jQuery.validator.addMethod("checkemail", function(value, element) 
    {
    return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
    }, "Enter a VALID email address"); 

    $.validator.addMethod('minStrict', function (value, el, param) {
     return value > param; 
    });
    $.validator.addMethod('maxStrict', function (value, el, param) {
     return value < param; 
    });


    /* @custom  validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#form_add_question").validate({

      /* @validation  states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation  rules 
      ------------------------------------------ */

      rules: {
        // case_no: {
        //   required: true
        // },
        // reg_stage: {
        //   required: true
        // },
        // honarable_justice: {
        //   required: true
        // },
        // court_no: {
        //   required: true,
        //   letternumeric: true
        // },
        // serial_no: {
        //   required: true,
        //   letternumeric: true
        // },
        // page_no: {
        //   required: true,
        //   letternumeric: true
        // }
      },
      /* @validation  error messages 
      ---------------------------------------------- */

      messages: {
        // reg_stage: {
        //   required: 'Please select stage'
        // },
        // honarable_justice: {
        //   required: 'Please select honarable justice'
        // },
        // court_no: {
        //   required: 'Please enter court no'
        // },
        // serial_no: {
        //   required: 'Please enter serial no'
        // },
        // page_no: {
        //   required: 'Please enter page no'
        // },
      },

      /* @validation  highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });

//alert(document.getElementById('further_date_autofocus_582').value);
//document.getElementById('filenum').focus();
  
  
  function print_cause_list(){
    document.getElementById('print_cause_list').style.display='block';
  }

  function close_button_print_cause_list(){
    document.getElementById('print_cause_list').style.display='none';
  }


  function cause_list(pessi_id){
    $('body').css('overflow','hidden');
    $("html, body").animate({ scrollTop: 0 }, "slow");
    document.getElementById('cause_list'+pessi_id).style.display='block';
  }

  function close_button_cause_list(pessi_id){
    $('body').css('overflow','auto', 'important');
    document.getElementById('cause_list'+pessi_id).style.display='none';
  }

  function edit_pessi(pessi_id){
    $('body').css('overflow','hidden');
    $("html, body").animate({ scrollTop: 0 }, "slow");    
    document.getElementById('edit_pessi'+pessi_id).style.display='block';
  }

  function close_button_edit_pessi(pessi_id){
    $('body').css('overflow','auto', 'important');
    document.getElementById('edit_pessi'+pessi_id).style.display='none';
  }






  </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('advocate_admin/layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>