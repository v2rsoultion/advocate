<?php $__env->startSection('content'); ?>

<style type="text/css">

.select_outer{
  height: 30px !important;
}  
 .close{
  margin-top: -6px !important;
  font-size: 30px !important;
  width: 30px !important;
}
  /*.admin-form .select, .admin-form .gui-input, .admin-form .select > select, .admin-form .select-multiple select{
    height: 28px !important;
  }*/
  .admin-form .append-icon .field-icon, .admin-form .prepend-icon .field-icon{
    line-height: 28px !important;
  }
  .admin-form .gui-textarea {
    line-height: 7px !important;
  }

  .form-control {
    height: 28px !important;
    /*padding: 0px 12px !important;*/
  }
  .admin-form .button{
    height: 28px !important;
    line-height: 1px !important;
  }
  .btn {
    height: 29px !important;
    line-height: 1px !important;  }

 .down{
  margin-top: 70px;
 }
.select-text {
  padding: 0px 12px !important;
}
 .close{
  margin-top: -6px !important;
  font-size: 30px !important;
  width: 30px !important;
}

</style>


  <!-- Start: Content-Wrapper -->
  <section id="content_wrapper">
    <header id="topbar" style="margin-top:60px;">
      <div class="topbar-left">
        <ol class="breadcrumb">
          <li class="crumb-active">
            <a href="<?php echo e(url('/advocate-panel/add-order-judgment')); ?>">Add Order Judgment</a>
          </li>
          <li class="crumb-icon">
            <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">
              <span class="glyphicon glyphicon-home"></span>
            </a>
          </li>
          <li class="crumb-link">
            <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">Home</a>
          </li>
          <li class="crumb-trail">View Order Judgment </li>
        </ol>
      </div>
    </header>

    <div class="" style="margin-top:10px;">
      <div class="col-md-12 mb30">
        <div class="panel panel-primary panel-border top mb35">  
          <div class="panel-heading">
            <div class="panel-title hidden-xs">
              <span class="glyphicon glyphicon-tasks"></span> View Order Judgment </div>
          </div>

          <div class="panel-menu admin-form theme-primary" style="padding: 5px 20px;">
            <div class="row">
              <?php echo Form::open(['url'=>'/advocate-panel/view-order-judgment','id'=>'form_add_question' ,'autocomplete'=>'off']); ?>


                <div class="col-md-2">
                  <div class="section" style="margin-top:10px !important;">
                      <label for="level" class="field prepend-icon">
                      <select class="form-control select-text" name="court_type" onchange="choose_court(this.value)" id="court_type" style="color: black;">
                        <option value="">Court Type</option>
                        <option value="1" <?php echo e($court_type == 1 ? 'selected="selected"' : ''); ?> >Trial Court</option>
                        <option value="2" <?php echo e($court_type == 2 ? 'selected="selected"' : ''); ?> >High Court</option>
                      </select>
                      <i class="arrow double"></i>
                    </label>
                  </div>
                </div>

                <div class="col-md-2">
                  <div class="section" style="margin-top:10px !important;">
                      <label for="level" class="field prepend-icon">
                      <select class="form-control select-text" name="court_name" onchange="choose_court_name(this.value)" id="court_name" style="color: black;">
                        <option value="">Court Name</option>
                          
                          <?php $__currentLoopData = $court_all; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $court_alls): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                            <option value="<?php echo e($court_alls->court_id); ?>" <?php echo e($court_alls->court_id == $court_name ? 'selected="selected"' : ''); ?> > <?php echo e($court_alls->court_name); ?> </option>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                      </select>
                      <i class="arrow double"></i>
                    </label>
                  </div>
                </div>

                <div class="col-md-2">
                  <div class="" style="margin-top:10px !important;">
                    <!-- <label for="artist_state" class="field select select_outer"> -->
                       <label for="level" class="field prepend-icon">
                      <select class="form-control new_select select-text" name="type" style="color: black;" onchange="choose_case(this.value)" id="choose_case_type">
                        <option value=''>Case Type</option> 
                        
                        <?php $__currentLoopData = $case_type_entry; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $case_type_entrys): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                        <option value="<?php echo e($case_type_entrys->case_id); ?>" <?php echo e($case_type_entrys->case_id == $reg_case_type_id ? 'selected="selected"' : ''); ?> ><?php echo e($case_type_entrys->case_name); ?> </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        
                      </select>
                      <i class="arrow double arrow_new"></i>
                    </label>
                  </div>
                </div>

                <!-- <div class="col-md-2" id="year_hide">
                  <div class="" style="margin-top:10px !important;">
                      <label for="artist_state" class="field select select_outer">
                        <select class="form-control new_select" id="" name="case_year">
                          <option value=''>Select Year</option>
                      
                          <?php
                              $currentYear = date('Y');
                          ?>

                          <?php $__currentLoopData = range(1950, $currentYear); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value=<?php echo e($value); ?> <?php echo e($value == $case_year ? 'selected="selected"' : ''); ?> ><?php echo e($value); ?></option>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </select>
                        <i class="arrow double arrow_new"></i>
                      </label>
                  </div>
                </div> -->

                <div class="col-md-2">
                  <div class="" style="margin-top:10px !important;">
                    <!-- <label for="artist_state" class="field select select_outer"> -->
                       <label for="level" class="field prepend-icon">
                      <select class="form-control new_select select-text" name="case_no" style="color: black;" onchange="choose_case_no(this.value)" id="order_judgment_case_no">
                        <option value=''> Case Number </option> 
                        
                        <?php $__currentLoopData = $case_registration; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $case_registrations): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                          <option value="<?php echo e($case_registrations->reg_id); ?>" <?php echo e($case_registrations->reg_id == $order_judgment_case_no ? 'selected="selected"' : ''); ?> ><?php echo e($case_registrations->reg_case_number); ?> </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        
                      </select>
                      <i class="arrow double arrow_new"></i>
                    </label>
                  </div>
                </div>

                <div class="col-md-2">
                  <div class="" style="margin-top:10px !important;">
                    <!-- <label for="artist_state" class="field select select_outer"> -->
                      <select class="form-control new_select select-text" name="file_no" style="color: black;" id="order_judgment_file_no">
                        <option value=''> File Number </option> 
                        
                        <?php $__currentLoopData = $case_registration; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $case_registrations): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                          <option value="<?php echo e($case_registrations->reg_id); ?>" <?php echo e($case_registrations->reg_id == $order_judgment_file_no ? 'selected="selected"' : ''); ?> ><?php echo e($case_registrations->reg_file_no); ?> </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        
                      </select>
                      <i class="arrow double arrow_new"></i>
                    </label>
                  </div>
                </div>

                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 10px;">
                    <?php echo Form::text('respondent','',array('class' => 'form-control','placeholder' => 'Resp. Name', 'autocomplete' => 'off' )); ?>

                    <label for="pincode" class="field-icon">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 0px;">
                    <?php echo Form::text('petitioner_name','',array('class' => 'form-control','placeholder' => 'Pet. Name', 'autocomplete' => 'off' )); ?>

                    <label for="pincode" class="field-icon">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>

                <div class="col-md-2" id="case_no_hide">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 0px;">
                    <?php echo Form::text('caption',$caption,array('class' => 'form-control new_text','placeholder' => 'Caption', 'autocomplete' => 'off' )); ?>

                    <label for="pincode" class="field-icon search_new">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>



                <div class="col-md-2" id="case_no_hide">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 0px;">
                    <?php echo Form::text('from_date',$from_date,array('class' => 'form-control new_text fromdate_search','placeholder' => 'From Date', 'autocomplete' => 'off' )); ?>

                    <label for="pincode" class="field-icon search_new">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>

                <div class="col-md-2" id="case_no_hide">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 0px;">
                    <?php echo Form::text('to_date',$to_date,array('class' => 'form-control new_text todate','placeholder' => 'To Date', 'autocomplete' => 'off' )); ?>

                    <label for="pincode" class="field-icon search_new">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>


                <div class="col-md-1" style="margin-top:0px !important;">
                   <a href="<?php echo e(url('/advocate-panel/view-order-judgment/')); ?>"><?php echo Form::button('Default', array('class' => 'btn btn-primary', 'id' => 'maskedKey')); ?></a>
                </div>

                <div  class="col-md-2">
                  <a href="<?php echo e(url('advocate-panel/download-order-judgement/xls')); ?>?reg_case_type_id=<?php echo e($reg_case_type_id); ?>&order_judgment_case_no=<?php echo e($order_judgment_case_no); ?>&order_judgment_file_no=<?php echo e($order_judgment_file_no); ?>&caption=<?php echo e($caption); ?>&from_date=<?php echo e($from_date); ?>&to_date=<?php echo e($to_date); ?>&court_type=<?php echo e($court_type); ?>&court_name=<?php echo e($court_name); ?>&pet_name=<?php echo e($pet_name); ?>&res_name=<?php echo e($res_name); ?>"><button type="button" class="btn btn-primary " > Download All </button></a>
                </div>

                <div class="col-md-2 mt10">
                   <a href="<?php echo e(url('/advocate-panel/view-order-judgment/all')); ?>"><?php echo Form::button('Show All Records', array('class' => 'btn btn-primary', 'id' => 'maskedKey')); ?></a>
                </div>

                <div class="col-md-1 pull-right" style="margin-top:10px !important; margin-right:15px !important;">
                  <button type="submit" name="search" class="button btn-primary"> Search </button>
                </div>
              <?php echo Form::close(); ?>             
                
                <div class="clearfix"></div>     
            </div>
          </div>

          <div class="panel-body pn">
              <?php echo Form::open(['url'=>'/advocate-panel/view-order-judgment','name'=>'form' ,'autocomplete'=>'off']); ?>

              <div class="table-responsive">
                <table class="table admin-form table-bordered table-striped theme-warning tc-checkbox-1 fs13" id="datatable">
                  <thead>
                    <tr class="bg-light">
                      <th style="width:90px !important;" class="text-left">
                        <label class="option block mn" style="width:90px !important;">
                          <input type="checkbox" id="check_all"> 
                          <span class="checkbox mn " style="border: 2px solid #4a89dc !important;"></span>
                          Select All
                        </label>
                      </th>
                      <th class="">Court Type</th>
                      <th class="">Court Name</th>
                      <th class="">Type of Case</th>
                      <th class="">Case No.</th>
                      <th class="">Title</th>
                      <th class="">View Document</th>
                      <th class=""> Action </th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php $__currentLoopData = $get_record; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $get_records): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 

                    <tr>
                      <td class="text-left" style="padding-left: 18px;">
                        <label class="option block mn">
                          <input type="checkbox" name="check[]" class="check" value="<?php echo e($get_records->reg_id); ?>">
                          <span class="checkbox mn" style="border: 2px solid #4a89dc !important;"></span>
                        </label>
                      </td>

                      <td class="text-left" style="padding-left: 20px;"> <?php if($get_records->upload_court_type == 1): ?> Trail Court  <?php elseif($get_records->upload_court_type == 2): ?> High Court <?php else: ?> ----- <?php endif; ?> </td>

                      <td class="text-left" style="padding-left: 20px;"> <?php if($get_records->court_name == ""): ?> ----- <?php else: ?> <?php echo e($get_records->court_name); ?> <?php endif; ?> </td>

                      <td class="text-left" style="padding-left: 20px;"> <?php if($get_records->case_name == ""): ?> ----- <?php else: ?> <?php echo e($get_records->case_name); ?> <?php endif; ?> </td>

                      <td class="text-left" style="padding-left: 20px;"> <?php if($get_records->reg_case_number == ""): ?> ----- <?php else: ?> <?php echo e($get_records->reg_case_number); ?> <?php endif; ?> </td>

                      <td class="text-left" style="padding-left:20px">                        
                        <?php echo e($get_records->reg_respondent); ?> v/s <?php echo e($get_records->reg_petitioner); ?>

                      </td>

                      <td class="" style="padding-left: 20px;">
                        <button type="button" class="btn btn-primary view" onclick="document_image3(<?php echo e($get_records->reg_id); ?>)" >
                          <a href="#" style="text-transform: capitalize; text-decoration:none;"> View </a>
                         </button>
                          <div id="document_image3<?php echo e($get_records->reg_id); ?>" class="modal fade in" role="dialog" style="overflow: scroll;">
                          <div class="modal-dialog" style="margin-top:80px;">
                            <div class="modal-content">
                              <div class="modal-header" style="padding-bottom: 35px;">
                                <button type="button" class="close" data-dismiss="modal" onclick="close_button_document(<?php echo e($get_records->reg_id); ?>)"  >&times;</button>
                                  <h4 class="modal-title pull-left">
                                    View Documents
                                  </h4>
                              </div>
                              <div class="">
                              
                                <?php
                                  $images_cat =  \App\Model\Upload_Document\Upload_Document::where(['upload_case_id' => $get_records->reg_id ])->get();
                                ?>

                                <?php if(count($images_cat) != 0): ?>
                                  <?php $__currentLoopData = $images_cat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $images_cats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 

                                    <?php
                                      $doc_image = explode("/",$images_cats['upload_images']);
                                      $last =  substr($doc_image[2],-4);
                                    ?>


                                      <div class="col-md-4 document_clear" style="margin-bottom: 22px; margin-top: 22px; position: relative;" id="cat_img<?php echo e($images_cats['upload_id']); ?>">
                                        
                                        <?php if($last == '.jpg' or $last == 'jpeg' or $last == '.png' or $last == '.gif'): ?>
                                         
<!--                                          <?php echo Html::image($images_cats['upload_images'], '', array('class' => 'media-object mw150 customer_profile', 'width'=>'100%', 'height'=>'250')); ?> -->

                                          <a target="_blank" href="<?php echo e(url($images_cats['upload_images'])); ?>" class="btn btn-xs btn-primary" style="margin-left: 10px; margin-top: 10px; padding-top: 12px;"> View </a>
                                        <?php elseif($last == '.pdf'): ?>
                                         <i class="fa fa-file-pdf-o"></i> 
                                          <a target="_blank" href="<?php echo e(url($images_cats['upload_images'])); ?>" class="btn btn-xs btn-primary" style="margin-left: 10px; padding-top: 12px;"> View </a>
                                        <?php elseif($last == 'docx' or $last == '.doc'): ?>
                                         <i class="fa fa-file-word-o"></i>
                                          <a target="_blank" href="<?php echo e(url($images_cats['upload_images'])); ?>" class="btn btn-xs btn-primary" style="margin-left: 10px; padding-top: 12px;"> View </a>
                                        <?php elseif($last == '.xls' or $last == 'xlsx'): ?>
                                         <i class="fa fa-file-excel-o"></i>
                                          <a target="_blank" href="<?php echo e(url($images_cats['upload_images'])); ?>" class="btn btn-xs btn-primary" style="margin-left: 10px; padding-top: 12px;"> Download </a>
                                        <?php else: ?>
                                          No document available
                                        <?php endif; ?>

                                        <?php if($images_cats['upload_caption'] != "" ): ?>
                                          <div style="margin-top: 10px;"> Caption :- <?php echo e($images_cats['upload_caption']); ?> </div>
                                        <?php endif; ?>

                                        <?php if($images_cats['upload_date'] != "" ): ?>
                                          <div style="margin-top: 10px;"> Date :- 
                                            <?php if($images_cats['upload_date'] == "1970-01-01"): ?>
                                              -----
                                            <?php else: ?>
                                              <?php echo e(date('d/m/Y',strtotime($images_cats['upload_date']))); ?>

                                            <?php endif; ?>
                                          </div>
                                        <?php endif; ?>
                                      </div>
                                    

                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php else: ?> 
                                  <div style="margin: 20px auto;" class="text-center" > No Documents Available !! </div>
                                <?php endif; ?>

                              </div>
                              <div class="clearfix"></div>
                            </div> 
                          </div>
                        </div>
                      </td>

                      <td class="" style="padding-left: 20px;">
                        <div class="btn-group ">
                          <a href="<?php echo e(url('/advocate-panel/add-order-judgment')); ?>/<?php echo e(sha1($get_records->upload_id)); ?>"> <button type="button" class="btn btn-primary br2 btn-xs fs12 dropdown-toggle" > Edit </button> </a>
                        </div>
                      </td>

                    </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </tbody>
                </table>
              </div>
              <?php echo Form::close(); ?>

          </div>
          <div class="panel-body pn">
            <div class="table-responsive">
              <table class="table admin-form theme-warning tc-checkbox-1 fs13">                                
                <tbody>
                  <tr class="">
                     <th class="text-left">
                      <button type="button" class="btn btn-primary" onclick="go_delete()"><i class="glyphicon glyphicon-trash"></i> Delete Multiple </button>
                    </th>
                    <th class="text-right">
                      <?php echo e($get_record->links()); ?>

                    </th>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


<style type="text/css">
.view a{
  color:#fff !important;
}
 
.document_clear:nth-child(4){
  clear: both;
}

</style>

<style type="text/css">
.dt-panelfooter{
  display: none !important;
}
</style>


<script type="text/javascript">
  


  function choose_court(court_type) { 
    BASE_URL = '<?php echo e(url('/')); ?>';
      $.ajax({
        url:BASE_URL+"/advocate-panel/compliance-court-type/"+court_type,
        success: function(result){
            $("#court_name").html(result);
        }
      });
  }



  function choose_court_name(court_name) { 

    var court_type = $("#court_type").val();

    BASE_URL = '<?php echo e(url('/')); ?>';
      $.ajax({
        url:BASE_URL+"/advocate-panel/order-judgement-case-type/"+court_name+"/"+court_type,
        success: function(result){
            $("#choose_case_type").html(result);
        }
      });
  }


  // choose_case

  function choose_case(case_type) { 

  //  if(case_type != ""){
    var court_type = $("#court_type").val();

  //  alert(court_type);

    BASE_URL = '<?php echo e(url('/')); ?>';
      $.ajax({
        url:BASE_URL+"/advocate-panel/ajax-case-type/"+case_type+"/"+court_type,
        success: function(result){
            $("#order_judgment_case_no").html(result);
        }
      });
  //  }
  }




  // choose_case_no

  function choose_case_no(case_no) { 

    BASE_URL = '<?php echo e(url('/')); ?>';

      $.ajax({
        url:BASE_URL+"/advocate-panel/ajax-case-no/"+case_no,
        success: function(result){
            $("#order_judgment_file_no").html(result);
        }
      });
  }




  
  jQuery(document).ready(function() {

    "use strict";

    $('#dataTable').dataTable({
      "aoColumnDefs": [{
        'bSortable': false,
        'aTargets': [-1]
      }],
      "oLanguage": {
        "oPaginate": {
          "sPrevious": "",
          "sNext": ""
        }
      },
      "iDisplayLength": -1,
      
      "sDom": '<"dt-panelmenu clearfix"lfr>t<"dt-panelfooter clearfix"ip>',
      "oTableTools": {
        "sSwfPath": "vendor/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
      }
    });

    $('.dataTables_filter input').attr("placeholder", "Enter Terms...");
    $('div.dataTables_filter input').focus();
    $('#datatable_length').hide();

  });
  

  function document_image3(reg_id){
    $('body').css('overflow','hidden');
    $("html, body").animate({ scrollTop: 0 }, "slow");
    document.getElementById('document_image3'+reg_id).style.display='block';
  }

  function close_button_document(reg_id){
    $('body').css('overflow','auto', 'important');
    document.getElementById('document_image3'+reg_id).style.display='none';
  }


</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('advocate_admin/layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>