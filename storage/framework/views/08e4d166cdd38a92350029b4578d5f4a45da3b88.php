<?php $__env->startSection('content'); ?>

<style type="text/css">
  
  blink, .blink {
      animation: blinker 1s linear infinite;
  }

 .close{
  margin-top: -6px !important;
  font-size: 30px !important;
  width: 30px !important;
}

 @keyframes  blinker {  
      50% { opacity: 0; }
 }

  .table > thead > tr > th {
    font-weight: 500;
  }
  b{
    font-weight: 1000;
  }  
  .admin-form .gui-input, .admin-form{
    padding: 10px;
  }
  .bg-light {
    background-color: #4B77BE !important;
    color: black !important;
    font-weight: bold;
  }
  .pagebreak { page-break-before: always; }
@media  print {
    h1 {page-break-before: always;}
}

</style>

<style type="text/css">
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
</style>

  <!-- Start: Content-Wrapper -->
  <section id="content_wrapper">
    <header id="topbar" style="margin-top: 60px;">
      <div class="topbar-left">
        <ol class="breadcrumb">
          <li class="crumb-active">
            <a href="<?php echo e(url('/advocate-panel/view-case-form')); ?>">View Case Registration</a>
          </li>
          <li class="crumb-icon">
            <a href="<?php echo e(url('/advocate-panel/dashboard')); ?>">
              <span class="glyphicon glyphicon-home"></span>
            </a>
          </li>
          <li class="crumb-link">
            <a href="<?php echo e(url('/advocate-panel/dashboard')); ?>">Home</a>
          </li>
          <li class="crumb-trail">Case Registration Details </li>
        </ol>

        <ol class="breadcrumb pull-right">
          <li class="crumb-active">
            <a href="#" onclick="share_email()" > Share via Email</a>
          </li>
        </ol>

      </div>
    </header>

    <div class="row mt20">
        <div class="col-md-12">
          <?php if(\Session::has('success')): ?>
          <div class="alert alert-success account_setting" >
            <?php echo \Session::get('success'); ?>

          </div>
          <?php endif; ?>

          <?php if(\Session::has('failure')): ?>
          <div class="alert alert-danger account_setting" >
            <?php echo \Session::get('failure'); ?>

          </div>
          <?php endif; ?>
        </div>
      </div>




    <div class="" style="margin-top:10px;">
      <div class="col-md-12">
        <div class="panel panel-primary panel-border top">  
          <div class="panel-heading">
            <div class="panel-title hidden-xs">
              <span class="glyphicon glyphicon-tasks"></span>Case Registration Details 
              <span class="pull-right"> <a href="#" onclick="printInfo(this)">
                <i class="fa fa-print"></i>
              </a>  </span>

<script type="text/javascript">

function printInfo(){


  var prtContent = document.getElementById("content");
var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
WinPrint.document.write(prtContent.innerHTML);
WinPrint.document.close();
WinPrint.focus();
WinPrint.print();
WinPrint.close();
}

</script>

              <span class="" style="margin-left: 330px;">  </span>
            </div>

          </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>

      <section id="content" class="animated fadeIn">
        <!-- <div class="page-heading" style="background-color: #FFF; margin:-45px -9px 5px;"> -->
          <div class="page-heading" style="background-color: #FFF; ">
            <div class="media clearfix">
              <div class="col-md-12">
                
                <div class="panel-heading" style="background: #4B77BE !important; color: black; font-weight: bold; font-size: 16px; margin-bottom: 30px; font-family: sans-serif;">
                  <div class="panel-title hidden-xs text-center" style="padding: 4px 0px; text-align: center;"> 
                    <?php if($get_case_details->pessi_choose_type == 0): ?> Pending <?php elseif($get_case_details->pessi_choose_type == 1): ?> <?php echo e(date('d F Y',strtotime($get_case_details->pessi_further_date))); ?> :- Disposal <?php else: ?> Due Course <?php endif; ?>
                  </div>
                </div>

                <div class="panel-heading" style="background: #4B77BE !important; color: black; font-weight: bold; font-size: 16px;">
                  <div class="panel-title hidden-xs text-center" style="padding: 4px 0px; text-align: center;"> Case Details </div>
                </div>

                <div class="panel-body pn">
                  <table class="table table-striped table-hover" id="" cellspacing="0" width="100%" style="color: #000; font-size: 12px;">
                    <thead>
                      <tr>
                        <th class="" style="border: 1px solid gray; text-align: left; padding: 7px 7px; font-weight: normal;"> <b> Court Name :- </b>  <?php if( $get_case_details->court_name != ""): ?> <?php echo e($get_case_details->court_name); ?> <?php else: ?> ----- <?php endif; ?> </th>
                      </tr>
                      <tr>
                        <th class="" style="border: 1px solid gray; padding: 7px 7px; background-color: antiquewhite; font-weight: normal;">
                         <!-- class= col-md-6 -->
                          <div class="" style="text-align: left; float:left; width: 45%; "> 
                            <b> Institution Date :- </b> <?php if( $get_case_details->reg_date != ""): ?> <?php echo e(date('d/m/Y',strtotime($get_case_details->reg_date))); ?> <?php else: ?> ----- <?php endif; ?> <br> 

                            <b> File No :- </b> <?php if( $get_case_details->reg_file_no != ""): ?> <?php echo e($get_case_details->reg_file_no); ?> <?php else: ?> ----- <?php endif; ?>
                          </div>
                          <div class="" style=" text-align: left; float: left; width: 45%;  border-left: 4px solid #eeeeee !important;"> 
                            <b style="margin-left: 10px;"> NCV No :- </b> <?php if( $get_case_details->reg_vcn_number != ""): ?> <?php echo e($get_case_details->reg_vcn_number); ?> <?php else: ?> ----- <?php endif; ?> <br> 
                            <b style="margin-left: 10px;"> Class Code :- </b> <?php if( $get_case_details->classcode_name != ""): ?> <?php echo e($get_case_details->classcode_name); ?> <?php else: ?> ----- <?php endif; ?> </div> 
                        </th>  
                      </tr>
                      <tr style="text-align: left;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px; font-weight: normal;"> 
                          <b> Case Number :- </b>
                            <?php if( $get_case_details->case_name != ""): ?> <?php echo e($get_case_details->case_name); ?> <?php else: ?> ---- <?php endif; ?> 
                            <?php if( $get_case_details->reg_case_number != ""): ?> <?php echo e($get_case_details->reg_case_number); ?> <?php else: ?> ---- <?php endif; ?>
                            <!-- <?php if( $get_case_details->reg_date != ""): ?> <?php echo e(date('Y',strtotime($get_case_details->reg_date))); ?> <?php else: ?> ----- <?php endif; ?> -->
                          </th>
                      </tr>
                      <tr style="text-align: left; background-color: antiquewhite; font-weight: normal;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px; font-weight: normal;"> <b> Case Title :- </b> <?php if( $get_case_details->reg_petitioner != ""): ?> <?php echo e($get_case_details->reg_petitioner); ?> <?php else: ?> ---- <?php endif; ?> v/s <?php if( $get_case_details->reg_respondent != ""): ?> <?php echo e($get_case_details->reg_respondent); ?> <?php else: ?> ---- <?php endif; ?> </th>
                      </tr>
                      <tr style="text-align: left; font-weight: normal;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px; font-weight: normal;"> <b> Power :- </b>
                          <?php if($get_case_details->reg_power == 1): ?> Petitioner <?php elseif($get_case_details->reg_power == 2 ): ?> <?php if($get_case_details->reg_power_respondent != ""): ?> Respondent - <?php echo e($get_case_details->reg_power_respondent); ?> <?php else: ?> Respondent <?php endif; ?> <?php elseif($get_case_details->reg_power == 3 ): ?> Caveat <?php elseif($get_case_details->reg_power == 4 ): ?> None <?php else: ?> ---- <?php endif; ?>
                        </th>
                      </tr>
                      <tr style="font-weight: normal;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px; background-color: antiquewhite; font-weight: normal;"> 
                          <!-- class= col-md-6 -->
                          <div class="" style="text-align: left; float:left; width: 45%;"> 
                            <b class=""> FIR No :- </b> <?php if( $get_case_details->reg_fir_id != ""): ?> <?php echo e($get_case_details->reg_fir_id); ?> <?php else: ?> ---- <?php endif; ?> <br> 
                            <b class=""> FIR Year :- </b> <?php if( $get_case_details->reg_fir_year != ""): ?> <?php echo e($get_case_details->reg_fir_year); ?> <?php else: ?> ---- <?php endif; ?> </div>
                          <div class="" style="float: left; text-align: left; width: 45%; border-left: 4px solid #eeeeee !important;"> <b style="margin-left: 15px;"> Police Station :- </b> <?php if( $get_case_details->reg_police_thana != ""): ?> <?php echo e($get_case_details->reg_police_thana); ?> <?php else: ?> ---- <?php endif; ?> </div> 
                        </th>
                      </tr>
                    </thead>
                  </table>
                </div>

                <br>

                <div class="panel-heading" style="background: #4B77BE !important; color: black; font-weight: bold; font-size: 16px;">
                  <div class="panel-title hidden-xs text-center" style="padding: 4px 0px; text-align: center;"> Case Status </div>
                </div>

                <div class="panel-body pn">
                  <table class="table table-striped table-hover" id="" cellspacing="0" width="100%" style="color: #000; font-size: 12px;">
                    <thead>
                      <tr style="text-align: left;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px; font-weight: normal;"> <b> Status :- </b> <?php if($get_case_details->pessi_choose_type == 0): ?> Pending <?php elseif($get_case_details->pessi_choose_type == 1): ?> Disposal ( <?php echo e(date('d F Y',strtotime($get_case_details->pessi_further_date))); ?> ) <?php else: ?> Due Course <?php if($get_case_details->pessi_further_date == "1970-01-01"): ?>
                            <?php else: ?> --- <?php echo e(date('d/m/Y',strtotime($get_case_details->pessi_further_date))); ?> 
                            <?php endif; ?>  <?php endif; ?> </th>
                      </tr>
                      
                      <?php if($get_case_details->pessi_choose_type != 2): ?>
                        <tr style="text-align: left; background-color: antiquewhite; font-weight: normal;">
                          <th class="" style="border: 1px solid gray; padding: 7px 7px; font-weight: normal;"> <b> Previous Date :- </b>
                            <?php if($get_case_details->pessi_prev_date == "1970-01-01" || $get_case_details->pessi_prev_date == ""): ?>
                              -----
                            <?php else: ?>
                              <?php echo e(date('d/m/Y',strtotime($get_case_details->pessi_prev_date))); ?>

                            <?php endif; ?>
                          </th>
                        </tr>
                        <tr style="text-align: left;">
                          <th class="" style="border: 1px solid gray; padding: 7px 7px; font-weight: normal;"> <b> Next Date :- </b>
                            <?php if($get_case_details->pessi_further_date == "1970-01-01"): ?>
                              -----
                            <?php else: ?>
                              <blink> <?php echo e(date('d/m/Y',strtotime($get_case_details->pessi_further_date))); ?> </blink>
                            <?php endif; ?>
                          </th>
                        </tr>
                      <?php endif; ?>


                      <tr style="text-align: left; background-color: antiquewhite;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px; font-weight: normal;"> <b> Stage :- </b> <?php if( $get_case_details->stage_name != ""): ?> <blink> <?php echo e($get_case_details->stage_name); ?> </blink> <?php else: ?> ---- <?php endif; ?> </th>
                      </tr>
                      <tr style="text-align: left;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px;"> <b> Extra Party :- </b> 
                          <button type="button" class="btn btn-primary view" style="padding: 4px 10px; background-color: #4a89dc; border:none;"  onclick="reg_petitioner(<?php echo e($get_case_details->reg_id); ?>)" > 
                            <a href="#" style="text-transform: capitalize; text-decoration:none; color: black;"> Pet. Details </a> 
                          </button> 

                          <button type="button" class="btn btn-primary view" style="padding: 4px 10px; background-color: #4a89dc; border:none;" onclick="reg_respondent(<?php echo e($get_case_details->reg_id); ?>)" >
                            <a href="#" style="text-transform: capitalize; text-decoration:none; color: black;"> Resp. Details </a> 
                          </button> 
                        </th>
                      </tr>
                    </thead>
                  </table>
                </div>
                            
               
                        


                <br>

                <div class="panel-heading" style="background: #4B77BE !important; color: black; font-weight: bold; font-size: 16px;">
                  <div class="panel-title hidden-xs text-center" style="padding: 4px 0px; text-align: center;"> Client Details </div>
                </div>

                <div class="panel-body pn">
                  <table class="table table-striped table-hover" id="" cellspacing="0" width="100%" style="color: #000;  font-size: 12px;">
                    <thead>
                      <tr style="text-align: left;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px; font-weight: normal;"> <b> Referred By :- </b> <?php if( $get_case_details->ref_advocate_name != ""): ?> <?php echo e($get_case_details->ref_advocate_name); ?> <?php else: ?> ---- <?php endif; ?> </th>
                      </tr>
                      <tr style="text-align: left;">
                        <th class="" style="border: 1px solid gray; padding: 7px 5px; text-align: left; background-color: antiquewhite; font-weight: normal;"> <div class="col-md-2"> <b> Client Name :- </b> </div> 
                          <?php if( $get_case_details->reg_client_group_id != ""): ?> 

                            <?php if($get_case_details->cl_group_type == "1"): ?> 
                              
                              <div class="col-md-11" >
                                <?php echo e($get_case_details->cl_group_name); ?> <?php if($get_case_details->cl_father_name != ""): ?> <?php echo e($get_case_details->cl_name_prefix); ?> <?php echo e($get_case_details->cl_father_name); ?> <?php endif; ?> <br> <?php echo e($get_case_details->cl_group_address); ?> <?php echo e($get_case_details->cl_group_place); ?> <br> <?php echo e($get_case_details->cl_group_mobile_no); ?> 
                              </div>

                            <?php else: ?> 
                              
                              <div class="col-md-11" >
                                <?php echo e($get_case_details->cl_group_name); ?> <?php if($get_case_details->cl_father_name != ""): ?> <?php echo e($get_case_details->cl_name_prefix); ?> <?php echo e($get_case_details->cl_father_name); ?> <?php endif; ?> <br> <?php echo e($get_case_details->sub_client_name); ?> <br> <?php echo e($get_case_details->sub_client_address); ?> <?php echo e($get_case_details->sub_client_place); ?> <br> <?php echo e($get_case_details->sub_client_mobile_no); ?> 
                              </div>

                            <?php endif; ?>

                          <?php else: ?> ---- <?php endif; ?> </th>
                      </tr>
                    </thead>
                  </table>
                </div>

                <br>

                <div class="panel-heading" style="background: #4B77BE !important; color: black; font-weight: bold; font-size: 16px;">
                  <div class="panel-title hidden-xs text-center" style="padding: 4px 0px; text-align: center;"> Other Details </div>
                </div>

                <div class="panel-body pn">
                  <table class="table table-striped table-hover" id="" cellspacing="0" width="100%" style="color: #000;  font-size: 12px;">
                    <thead>
                      <tr style="text-align: left;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px; font-weight: normal;"> <b> Assigned :- </b> <?php if( $get_case_details->assign_advocate_name != ""): ?> <?php echo e($get_case_details->assign_advocate_name); ?> <?php else: ?> ---- <?php endif; ?></th>
                      </tr>
                      <tr style="background-color: antiquewhite; text-align: left;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px; font-weight: normal;"> <b> Act :- </b> <?php if( $get_case_details->act_name != ""): ?> <?php echo e($get_case_details->act_name); ?> <?php else: ?> ---- <?php endif; ?></th>
                      </tr>

                      <?php
                        $section_name  = "";
                        $get_case_details->reg_section_id = explode(',', $get_case_details->reg_section_id);
                        $section =  \App\Model\Section\Section::whereIn('section_id', $get_case_details->reg_section_id )->where('section_name', '!=' ,'')->get();

                        foreach($section as $sections){
                          $section_name .= $sections->section_name.' , ';
                        }

                        $section_name = substr($section_name,0,-2);
                      ?>
                      

                      <tr style="text-align: left;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px; font-weight: normal;"> <b> Section :- </b> <?php if( $section_name != ""): ?> <?php echo e($section_name); ?> <?php else: ?> ---- <?php endif; ?> </th>
                      </tr>
                      

                      <tr style="background-color: antiquewhite; text-align: left;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px; font-weight: normal;"> <b> Opposite Counsel :- </b> <?php if( $get_case_details->reg_opp_council != ""): ?> <?php echo e($get_case_details->reg_opp_council); ?> <?php else: ?> ---- <?php endif; ?> </th>
                      </tr>
                      <tr style="text-align: left;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px; font-weight: normal;"> <b> Remark Field :- </b> <?php if( $get_case_details->reg_remark != ""): ?> <?php echo e($get_case_details->reg_remark); ?> <?php else: ?> ---- <?php endif; ?> </th>
                      </tr>

                      <tr style="text-align: left; background-color: antiquewhite;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px; font-weight: normal;"> <b> Other Field :- </b> <?php if( $get_case_details->reg_other_field != ""): ?> <?php echo e($get_case_details->reg_other_field); ?> <?php else: ?> ---- <?php endif; ?> </th>
                      </tr>
                    </thead>
                  </table>
                </div>

                <br>

                <div class="panel-heading" style="background: #4B77BE !important; color: black; font-weight: bold; font-size: 16px;">
                  <div class="panel-title hidden-xs text-center" style="padding: 4px 0px; text-align: center;"> History of Case </div>
                </div>

                <div class="panel-body pn">
                  <table class="table table-striped table-hover" id="" cellspacing="0" width="100%" style="color: #000;  font-size: 12px;">
                    <thead>
                      <tr>
                        <th class="" style="border: 1px solid gray; padding: 7px 7px;"> <b> Peshi Date </b></th>
                        <th class="" style="border: 1px solid gray; padding: 7px 7px;"> <b> Court No: Judge Name </b></th>
                        <th class="" style="border: 1px solid gray; padding: 7px 7px;"> <b> Purpose / Stage </b></th>
                        <th class="" style="border: 1px solid gray; padding: 7px 7px;"> <b> Order Sheet </b></th>
                        <th class="text-left" style="border: 1px solid gray; padding: 7px 7px;">
                          <!-- <label class="option block mn"> -->
                           <b> Delete</b>
                          <!-- </label> -->
                        </th>
                      </tr>
                    </thead>

                    <tbody>
                      <?php $__currentLoopData = $pessi_detail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pessi_details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        
                        <tr>
                          <!-- <td class=""> 
                            <a href="<?php echo e(url('advocate-panel/view-peshi')); ?>" style="text-decoration: none;">
                              <?php if($pessi_details->pessi_choose_type == 0): ?>
                                Date:-  <?php echo e(date('d F Y',strtotime($pessi_details->pessi_further_date))); ?>

                              <?php elseif($pessi_details->pessi_choose_type == 1): ?>
                                Decide :- <?php if($pessi_details->pessi_decide_id == 1): ?> Case In Favour <?php elseif($pessi_details->pessi_decide_id == 2): ?> Case in against <?php elseif($pessi_details->pessi_decide_id == 3): ?> Withdraw <?php elseif($pessi_details->pessi_decide_id == 4): ?> None <?php endif; ?>
                              <?php else: ?>
                                Due Course
                              <?php endif; ?>
                            </a> -->

                          <td style="border: 1px solid gray; padding: 7px 7px;">
                            <a href="#" style="text-transform: capitalize; text-decoration:none;" onclick="pessi_details(<?php echo e($pessi_details->pessi_id); ?>)" > 

                              <?php if($pessi_details->pessi_choose_type == 0): ?>

                                <?php if($pessi_details->pessi_further_date == "" || $pessi_details->pessi_further_date ==  "1970-01-01"): ?>
                                  Date:-  -----
                                <?php else: ?>

                                  Date:-  <?php echo e(date('d F Y',strtotime($pessi_details->pessi_further_date))); ?>

                                
                                <?php endif; ?>

                                
                              <?php elseif($pessi_details->pessi_choose_type == 1): ?>

                                Decide :- <?php if($pessi_details->pessi_decide_id == 1): ?> Case In Favour <?php elseif($pessi_details->pessi_decide_id == 2): ?> Case in against <?php elseif($pessi_details->pessi_decide_id == 3): ?> Withdraw <?php elseif($pessi_details->pessi_decide_id == 4): ?> None <?php endif; ?>
                              <?php else: ?>
                                Due Course
                              <?php endif; ?>

                            </a>

                            

                          </td>

                          <script type="text/javascript">
                            function update_pessi_details(pessi_id) {

                              if(confirm("Are you sure to want update records?")) {
                                $("[name=form_add_pessi"+pessi_id+"]").submit();    
                              }
                            }
                          </script>

                          <?php
                            $justise_name =  \App\Model\Judge\Judge::where(['judge_id' => $pessi_details->honarable_justice_db ])->first();
                          ?>

                          <td class="" style=" border: 1px solid gray; padding: 7px 7px;"> 
                            <?php if($pessi_details->reg_court == 2): ?>
                              <?php if($pessi_details->judge_name == "" && $pessi_details->court_number == ""): ?>
                                -----
                              <?php else: ?>
                                <?php if($pessi_details->court_number != ""): ?>  C-<?php echo e($pessi_details->court_number); ?>: &nbsp <?php endif; ?>

                                <?php if($pessi_details->judge_name != ""): ?>  <?php echo e($pessi_details->judge_name); ?>  <?php endif; ?>

                                <?php if($justise_name->judge_name != "" && $pessi_details->judge_name != ""): ?> &  <?php endif; ?>
                                <?php if($justise_name->judge_name != ""): ?>  <?php echo e($justise_name->judge_name); ?>  <?php endif; ?>

                                <?php if( ($pessi_details->judge_name != "" || $justise_name->judge_name != "") && $pessi_details->court_number != ""): ?>   <?php endif; ?>
                                
                              <?php endif; ?>
                            <?php elseif($pessi_details->reg_court == 1): ?>
                                   -------
                            <?php endif; ?>
                          </td>
                          <td class="" style=" border: 1px solid gray; padding: 7px 7px;"> <?php if($pessi_details->stage_name == ""): ?> ----- <?php else: ?> <?php echo e($pessi_details->stage_name); ?> <?php endif; ?> </td>
                          <td class="" style=" border: 1px solid gray; padding: 7px 7px;"> <?php if($pessi_details->pessi_order_sheet == ""): ?> ----- <?php else: ?> <?php echo e($pessi_details->pessi_order_sheet); ?> <?php endif; ?></td>


                          <td class="text-left" style="padding-left: 20px; border: 1px solid gray; padding: 7px 7px;">
                            <?php if(count($pessi_detail) == 1): ?>

                            <?php else: ?>
                            <label class="option block mn" onclick="delete_pessi('<?php echo e(sha1($pessi_details->pessi_id)); ?>')">
                              <!-- <a href="<?php echo e(url('advocate-panel/delete-pessi')); ?>/<?php echo e(sha1($pessi_details->pessi_id)); ?>"> -->
                                <span class="fa fa-trash" style="cursor: pointer;"></span>
                              <!-- </a> -->
                            </label>

                            <?php endif; ?>
                          </td>


                        </tr>

                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                  </table>
                </div>

                <br>

                <div class="panel-heading" style="background: #4B77BE !important; color: black; font-weight: bold; font-size: 16px;">
                  <div class="panel-title hidden-xs text-center" style="padding: 4px 0px; text-align: center;"> Compliance </div>
                </div>

                <div class="panel-body pn">
                  <table class="table table-striped table-hover" id="" cellspacing="0" width="100%" style="color: #000;  font-size: 12px;">
                    <thead>
                      <tr>
                        <th class="" style="border: 1px solid gray; padding: 7px 7px;"> <b> Created </b></th>
                        <th class="" style="border: 1px solid gray; padding: 7px 7px;"> <b> View Compliance </b></th>
                      </tr>
                    </thead>

                    <tbody>

                      <?php if(count($compliance_detail) != 0): ?>
                        <?php $__currentLoopData = $compliance_detail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $compliance_details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <tr>
                            <td class="" style="border: 1px solid gray; padding: 7px 7px;"> <?php if($compliance_details->compliance_created != "" || $compliance_details->compliance_created != "1970-01-01"): ?> <?php echo e(date('d M Y',strtotime($compliance_details->compliance_created))); ?> <?php else: ?> ----- <?php endif; ?> </td>
              
                            <td class="text-left" style="border: 1px solid gray; padding: 7px 7px;">
                              <button type="button" class="btn btn-primary view btn-xs" onclick="view_compliance(<?php echo e($compliance_details->compliance_id); ?>)" style="padding: 4px 10px; background-color: #4a89dc; border:none;">
                                <a href="#" style="text-transform: capitalize; text-decoration:none; color: white;"> View Compliance</a>
                              </button>
                            </td>
                          </tr>    
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      <?php else: ?> 
                        
                        <tr>
<!--                           <td colspan="5">
                            <div style="text-align: center; border: 1px solid gray; padding: 7px 7px;" class="text-center" > No Record Found !! </div>
                          </td> -->

                          <td colspan="5" class="text-center" style="border: 1px solid gray; text-align: center; padding: 7px 7px;"> No Record Found !! </td>
                        </tr>
                      <?php endif; ?>

                    </tbody>
                  </table>
                </div>

                <br>

                <div class="panel-heading" style="background: #4B77BE !important; color: black; font-weight: bold; font-size: 16px;">
                  <div class="panel-title hidden-xs text-center" style="padding: 4px 0px; text-align: center;"> Documents </div>
                </div>

                <div style="border: 1px solid gray; padding: 15px 7px;" class="panel-body pn" style="color: #000;  font-size: 12px;">

                  <?php
                    $images_cat =  \App\Model\Upload_Document\Upload_Document::where(['upload_case_id' => $get_case_details->reg_id ])->where('upload_images', '!=' ,'')->get();
                  ?>

                  <?php if(count($images_cat) != 0): ?>
                    <?php $__currentLoopData = $images_cat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $images_cats): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 


                    <?php
                      $doc_image = explode("/",$images_cats['upload_images']);
                      $last =  substr($doc_image[2],-4);
                    ?>
                    
                      <div class="col-md-4 document_clear" style="margin-bottom: 22px; margin-top: 22px; position: relative;" id="cat_img<?php echo e($images_cats['upload_id']); ?>">
                        
                        <?php if($last == '.jpg' or $last == 'jpeg' or $last == '.png' or $last == '.gif'): ?>
                         
                         <!-- <?php echo Html::image($images_cats['upload_images'], '', array('class' => 'media-object mw150 customer_profile', 'width'=>'100%', 'height'=>'250')); ?> -->

                          <a target="_blank" href="<?php echo e(url($images_cats['upload_images'])); ?>" class="btn btn-xs btn-primary" style="margin-left: 10px; margin-top: 10px;"> View </a>
                        <?php elseif($last == '.pdf'): ?>
                         <i class="fa fa-file-pdf-o"></i> 
                          <a target="_blank" href="<?php echo e(url($images_cats['upload_images'])); ?>" class="btn btn-xs btn-primary" style="margin-left: 10px;"> View </a>
                        <?php elseif($last == 'docx' or $last == '.doc'): ?>
                         <i class="fa fa-file-word-o"></i>
                          <a target="_blank" href="<?php echo e(url($images_cats['upload_images'])); ?>" class="btn btn-xs btn-primary" style="margin-left: 10px;"> View </a>
                        <?php elseif($last == '.xls' or $last == 'xlsx'): ?>
                         <i class="fa fa-file-excel-o"></i>
                          <a target="_blank" href="<?php echo e(url($images_cats['upload_images'])); ?>" class="btn btn-xs btn-primary" style="margin-left: 10px;"> Download </a>
                        <?php else: ?>
                          <a target="_blank" href="<?php echo e(url($images_cats['upload_images'])); ?>" class="btn btn-xs btn-primary" style="margin-left: 10px;"> View </a>
                        <?php endif; ?>

                        <?php if($images_cats['upload_caption'] != "" ): ?>
                          <div style="margin-top: 10px;"> Caption :- <?php echo e($images_cats['upload_caption']); ?> </div>
                        <?php endif; ?>

                        <?php if($images_cats['upload_date'] != "" ): ?>
                          <div style="margin-top: 10px;"> Date :- 
                            <?php if($images_cats['upload_date'] == "1970-01-01"): ?>
                              -----
                            <?php else: ?>
                              <?php echo e(date('d/m/Y',strtotime($images_cats['upload_date']))); ?>

                            <?php endif; ?> 
                          </div>
                        <?php endif; ?>


                      </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  <?php else: ?> 
                    <div style="text-align: center; border: 1px solid gray; padding: 15px 7px; color: #000;" class="text-center" > No Documents Available !! </div>
                  <?php endif; ?>
                  
                </div>
              </div>
            </div>
        </div>  
      </section>

  </section>



  <?php $__currentLoopData = $compliance_detail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $compliance_details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                         
    <div id="view_compliance<?php echo e($compliance_details->compliance_id); ?>" class="modal fade in" role="dialog" style="overflow: scroll;">
      <div class="modal-dialog" style="width:500px; margin-top:80px;">
        <div class="modal-content">
          <div class="modal-header" style="padding-bottom: 35px;">
            <button type="button" class="close" data-dismiss="modal" onclick="close_button_comp(<?php echo e($compliance_details->compliance_id); ?>)" >&times;</button>
              <h4 class="modal-title pull-left" style="color: #626262;">
                 View Compliance
              </h4>
          </div>

          <?php if($compliance_details->compliance_type_notice == 1): ?> 
            <div class="modal-header" style="padding-bottom: 35px;">
                <h4 class="modal-title pull-left" style="color: #626262;">  Notice Details  </h4>
            </div>

            <div class="modal-body">
              <table class="table table-bordered mbn">
                <thead>
                  <tr class="bg-light">
                    <th class="text-left" style="padding-left: 10px !important; font-weight: 600; color: #000;">Issue Date</th>
                    <th class="text-left" style="padding-left: 10px !important; font-weight: 600; color: #000;">Status</th>
                  </tr>
                </thead>
                <tbody>            
                  <tr class="">
                    <td class="text-left" style="font-weight: 400; color: #000;" > <?php if($compliance_details->notice_issue_date != "1970-01-01"): ?> <?php echo e(date('d M Y',strtotime($compliance_details->notice_issue_date))); ?> <?php else: ?> ----- <?php endif; ?> </td>
                    <td class="text-left" style="font-weight: 400; color: #000;"> <?php if($compliance_details->notice_status == 1): ?> Pending <?php elseif($compliance_details->notice_status == 2): ?>  Complete ( <?php if($compliance_details->notice_status_date != "" || $compliance_details->notice_status_date != "1970-01-01"): ?> <?php echo e(date('d M Y',strtotime($compliance_details->notice_status_date))); ?> <?php endif; ?> ) <?php else: ?> ----- <?php endif; ?> </td>
                  </tr>
                </tbody>
              </table>
            </div>
          <?php endif; ?>
          <div class="clearfix"></div>

          <?php if($compliance_details->compliance_type_date == 1): ?> 
            <div class="divider" style="margin: 0px;"></div>
            <div class="modal-header" style="padding-bottom: 35px;">
                <h4 class="modal-title pull-left" style="color: #626262;" >  Date  </h4>
            </div>

            <div class="modal-body">
              <table class="table table-bordered mbn">
                <thead>
                  <tr class="bg-light">
                    <th class="text-left" style="padding-left: 10px !important; font-weight: 600; color: #000;">Date Type</th>
                    <th class="text-left" style="padding-left: 10px !important; font-weight: 600; color: #000;">Date</th>
                    <th class="text-left" style="padding-left: 10px !important; font-weight: 600; color: #000;">Status</th>
                  </tr>
                </thead>
                <tbody>            
                  <tr class="">
                    <td class="text-left" style="font-weight: 400; color: #000;"> <?php if($compliance_details->compliance_date_type == 1): ?> Short Date <?php else: ?> Long Date <?php endif; ?></td>
                    <td class="text-left" style="font-weight: 400; color: #000;"> <?php if($compliance_details->compliance_date != "1970-01-01"): ?> <?php echo e(date('d M Y',strtotime($compliance_details->compliance_date))); ?> <?php else: ?> ----- <?php endif; ?></td>
                    <td class="text-left" style="font-weight: 400; color: #000;"><?php if($compliance_details->compliance_status == 1): ?> Pending <?php elseif($compliance_details->compliance_status == 2): ?> Complete ( <?php echo e(date('d M Y',strtotime($compliance_details->compliance_date_status))); ?> ) <?php else: ?> ----- <?php endif; ?> </td>
                  </tr>
                </tbody>
              </table>
            </div>
          <?php endif; ?>

          <?php if($compliance_details->compliance_type_defect_case == 1): ?> 
            <div class="divider" style="margin: 0px;"></div>
            <div class="modal-header" style="padding-bottom: 35px;">
                <h4 class="modal-title pull-left" style="color: #626262;">  Defect Case  </h4>
            </div>

            <div class="modal-body">
              <table class="table table-bordered mbn">
                <thead>
                  <tr class="bg-light">
                    <!-- <th class="text-left" style="padding-left: 10px !important;">Act Reason</th> -->
                    <th class="text-left" style="padding-left: 10px !important; font-weight: 600; color: #000;">Status</th>
                  </tr>
                </thead>
                <tbody>            
                  <tr class="">
                    <!-- <td class="text-left"> <?php echo e($compliance_details->compliance_act_reason); ?> </td> -->
                    <td class="text-left" style="font-weight: 400; color: #000;"><?php if($compliance_details->compliance_defect_status == 1): ?> Pending <?php elseif($compliance_details->compliance_defect_status == 2): ?> Complete ( <?php echo e(date('d M Y',strtotime($compliance_details->compliance_defect_date_status))); ?> ) <?php else: ?> ----- <?php endif; ?>  </td>
                  </tr>
                </tbody>
              </table>
            </div>
          <?php endif; ?>

          <?php if($compliance_details->compliance_type_reply == 1): ?> 
            <div class="divider" style="margin: 0px;"></div>
            <div class="modal-header" style="padding-bottom: 35px;">
                <h4 class="modal-title pull-left" style="color: #626262;">  Reply  </h4>
            </div>

            <div class="modal-body">
              <table class="table table-bordered mbn">
                <thead>
                  <tr class="bg-light">
                    <th class="text-left" style="padding-left: 10px !important;font-weight: 600; color: #000;">Reply</th>
                    <th class="text-left" style="padding-left: 10px !important;font-weight: 600; color: #000;">Status</th>
                  </tr>
                </thead>
                <tbody>            
                  <tr class="">
                    <td class="text-left" style="font-weight: 400; color: #000;" > <?php if($compliance_details->compliance_reply != ""): ?> <?php echo e($compliance_details->compliance_reply); ?> <?php else: ?> ----- <?php endif; ?> </td>
                    <td class="text-left" style="font-weight: 400; color: #000;" > <?php if($compliance_details->compliance_reply_status == 1): ?> Pending <?php elseif($compliance_details->compliance_reply_status == 2): ?> Complete ( <?php echo e(date('d M Y',strtotime($compliance_details->compliance_reply_date_status))); ?> ) <?php else: ?> ----- <?php endif; ?> </td>
                  </tr>
                </tbody>
              </table>
            </div>
          <?php endif; ?>

          <?php if($compliance_details->compliance_type_certified == 1): ?> 
            <div class="divider" style="margin: 0px;"></div>
            <div class="modal-header" style="padding-bottom: 35px;">
                <h4 class="modal-title pull-left" style="color: #626262;">  Apply Date Date  </h4>
            </div>

            <div class="modal-body">
              <table class="table table-bordered mbn">
                <thead>
                  <tr class="bg-light">
                    <th class="text-left" style="padding-left: 10px !important;font-weight: 600; color: #000; ">Apply Date Date</th>
                    <th class="text-left" style="padding-left: 10px !important;font-weight: 600; color: #000; ">Status</th>
                  </tr>
                </thead>
                <tbody>            
                  <tr class="">
                    <td class="text-left" style="font-weight: 400; color: #000;"><?php if($compliance_details->certified_copy_date != "1970-01-01"): ?> <?php echo e(date('d M Y',strtotime($compliance_details->certified_copy_date))); ?> <?php else: ?> ----- <?php endif; ?></td>
                    <td class="text-left" style="font-weight: 400; color: #000;"> <?php if($compliance_details->certified_copy_status == 1): ?> Pending <?php elseif($compliance_details->certified_copy_status == 2): ?> Complete ( <?php echo e(date('d M Y',strtotime($compliance_details->certified_copy_date_status))); ?> ) <?php else: ?> ----- <?php endif; ?> </td>
                  </tr>
                </tbody>
              </table>
            </div>
          <?php endif; ?>

          <?php if($compliance_details->compliance_type_other == 1): ?> 
            <div class="divider" style="margin: 0px;"></div>
            <div class="modal-header" style="padding-bottom: 35px;">
                <h4 class="modal-title pull-left" style="color: #626262;"> Other </h4>
            </div>

            <div class="modal-body">
              <table class="table table-bordered mbn">
                <thead>
                  <tr class="bg-light">
                    <th class="text-left" style="padding-left: 10px !important; font-weight: 600; color: #000;">Other</th>
                    <th class="text-left" style="padding-left: 10px !important; font-weight: 600; color: #000;">Status</th>
                  </tr>
                </thead>
                <tbody>            
                  <tr class="">
                    <td class="text-left" style="font-weight: 400; color: #000;"> <?php if($compliance_details->compliance_other != ""): ?> <?php echo e($compliance_details->compliance_other); ?> <?php else: ?> ----- <?php endif; ?> </td>
                    <td class="text-left" style="font-weight: 400; color: #000;"> <?php if($compliance_details->compliance_other_status == 1): ?> Pending <?php elseif($compliance_details->compliance_other_status == 2): ?> Complete ( <?php echo e(date('d M Y',strtotime($compliance_details->compliance_other_date_status))); ?> ) <?php else: ?> ----- <?php endif; ?> </td>
                  </tr>
                </tbody>
              </table>
            </div>
          <?php endif; ?>

        </div> 
      </div> 
    </div>  
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>





 <div id="reg_petitioner<?php echo e($get_case_details->reg_id); ?>" class="modal fade in " role="dialog" style="overflow: scroll;">
    <div class="modal-dialog" style="margin-top:80px;">
      <div class="modal-content">
        <div class="modal-header" style="padding-bottom: 35px;">
          <button type="button" class="close" data-dismiss="modal" onclick="close_button_petit(<?php echo e($get_case_details->reg_id); ?>)" >&times;</button>
            <h4 class="modal-title pull-left">
              Petitioner Extra Party
            </h4>
        </div>
        <div class="">
          <table class="table admin-form table-bordered table-striped theme-warning tc-checkbox-1 fs13" id="datatable">
            <thead>
              <tr class="bg-light">
                <th class="text-left" style="font-weight: 600;" >Name</th>
                <th class="text-left" style="font-weight: 600;" >Prefix</th>
                <th class="text-left" style="font-weight: 600;" >Guardian Name</th>
                <th class="text-left" style="font-weight: 600;" >Address</th>
              </tr>
          </thead>
          <tbody>
           <?php
            $reg_petitioner_extra = json_decode($get_case_details->reg_petitioner_extra,true);
            ?>
            <?php $__currentLoopData = $reg_petitioner_extra; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $respondent_extras): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

            <tr>
              <td class="text-left" style="padding-left: 20px;"> <?php if($respondent_extras[petitioner_name] != ""): ?> <?php echo e($respondent_extras[petitioner_name]); ?> <?php else: ?> ----- <?php endif; ?> </td>
              <td class="text-left" style="padding-left: 20px;"> <?php if($respondent_extras[petitioner_prefix] != ""): ?> <?php echo e($respondent_extras[petitioner_prefix]); ?> <?php else: ?> ----- <?php endif; ?> </td>
              <td class="text-left" style="padding-left: 20px;"> <?php if($respondent_extras[petitioner_father_name] != ""): ?> <?php echo e($respondent_extras[petitioner_father_name]); ?> <?php else: ?> ----- <?php endif; ?> </td>
              <td class="text-left" style="padding-left: 20px;"> <?php if($respondent_extras[petition_address] != ""): ?> <?php echo e($respondent_extras[petition_address]); ?> <?php else: ?> ----- <?php endif; ?> </td>
            </tr>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
        </div>
        <div class="clearfix"></div>
      </div> 
    </div>
 </div>
                         
 <div id="reg_respondent<?php echo e($get_case_details->reg_id); ?>" class="modal fade in" role="dialog" style="overflow: scroll;">
    <div class="modal-dialog" style="margin-top:80px;">
      <div class="modal-content">
        <div class="modal-header" style="padding-bottom: 35px;">
          <button type="button" class="close" data-dismiss="modal" onclick="close_button_resp(<?php echo e($get_case_details->reg_id); ?>)" >&times;</button>
            <h4 class="modal-title pull-left">
              Respondent Extra Party
            </h4>
        </div>
        <div class="">
          <table class="table admin-form table-bordered table-striped theme-warning tc-checkbox-1 fs13" id="datatable_1">
            <thead>
              <tr class="bg-light"> 
                <th class="text-left" style="font-weight: 600;" >Name</th>
                <th class="text-left" style="font-weight: 600;" >Prefix</th>
                <th class="text-left" style="font-weight: 600;" >Guardian Name</th>
                <th class="text-left" style="font-weight: 600;" >Address</th>
              </tr>
          </thead>
          <tbody>
           <?php
            $respondent_extra = json_decode($get_case_details->reg_respondent_extra,true);
            ?>
            <?php $__currentLoopData = $respondent_extra; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $respondent_extras): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            
            <tr class="json_view">
              <td class="text-left" style="padding-left: 20px;"> <?php if($respondent_extras[respondent_name] != ""): ?> <?php echo e($respondent_extras[respondent_name]); ?> <?php else: ?> ----- <?php endif; ?> </td>
              <td class="text-left" style="padding-left: 20px;"> <?php if($respondent_extras[respondent_prefix] != ""): ?> <?php echo e($respondent_extras[respondent_prefix]); ?> <?php else: ?> ----- <?php endif; ?> </td>
              <td class="text-left" style="padding-left: 20px;"> <?php if($respondent_extras[respondent_father_name] != ""): ?> <?php echo e($respondent_extras[respondent_father_name]); ?> <?php else: ?> ----- <?php endif; ?> </td>
              <td class="text-left" style="padding-left: 20px;"> <?php if($respondent_extras[respondent_address] != ""): ?> <?php echo e($respondent_extras[respondent_address]); ?> <?php else: ?> ----- <?php endif; ?> </td>
            </tr>

              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
        </div>
        <div class="clearfix"></div>
      </div> 
    </div>
 </div>


<?php $__currentLoopData = $pessi_detail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pessi_details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

 <div id="pessi_details<?php echo e($pessi_details->pessi_id); ?>" class="modal fade in" role="dialog" style="overflow: scroll;">
  <div class="modal-dialog" style="width:700px; margin-top:80px;">
    <div class="modal-content">
      <div class="modal-header" style="padding-bottom: 35px;">
        <button type="button" class="close" data-dismiss="modal" onclick="close_button_pessi(<?php echo e($pessi_details->pessi_id); ?>)" >&times;</button>
        <h4 class="modal-title pull-left"> Peshi Entry </h4>
      </div>
      <section id="content" class="table-layout animated fadeIn">
        <div>
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                  <div id="tab1_1" class="tab-pane active">
                    
                    <?php echo Form::open(['name'=>'form_add_pessi'.$pessi_details->pessi_id,'url'=>'advocate-panel/edit-peshi/'.$pessi_details->pessi_id,'id'=>'form_add_pessi' ,'autocomplete'=>'off' ]); ?>


                    <div class="row">                                                
                        
                        <!-- <div class="col-md-12">
                          <div class="section" style="margin-bottom: 40px;">
                            <div class="col-md-3">
                              <label class="option" style="font-size:12px;">
                                <?php echo Form::radio('choose_type','0',$pessi_details['pessi_choose_type'] == 0 ? 'checked' : '', array('onclick' => "show_further('$pessi_details->pessi_id')", 'class' => 'check' )); ?>

                                <span class="radio" style="border: 2px solid #4a89dc !important;"></span> Next Date
                              </label>
                            </div>

                            <div class="col-md-3">
                              <label class="option" style="font-size:12px;">
                                <?php echo Form::radio('choose_type','1',$pessi_details['pessi_choose_type'] == 1 ? 'checked' : '', array('onclick' => "show_decide('$pessi_details->pessi_id')", 'class' => 'check' )); ?>

                                <span class="radio" style="border: 2px solid #4a89dc !important;"></span> Decide
                              </label>
                            </div>

                            <div class="col-md-3">
                              <label class="option" style="font-size:12px;">
                                <?php echo Form::radio('choose_type','2',$pessi_details['pessi_choose_type'] == 2 ? 'checked' : '', array('onclick' => "show_due_course('$pessi_details->pessi_id')", 'class' => 'check' )); ?>

                                <span class="radio" style="border: 2px solid #4a89dc !important;"></span> Due Course
                              </label>
                            </div>

                          </div>

                          <div class="section"></div>
                          <div class="clearfix"></div>
                        </div> -->

                        <div class="col-md-6" <?php if($pessi_details['pessi_choose_type'] == 1 || $pessi_details['pessi_choose_type'] == 2): ?> style="display:none;" <?php endif; ?>  id="show_further_date<?php echo e($pessi_details['pessi_id']); ?>" >
                          <div class="section section_from">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Next Date  </label>  
                            <label for="level_name" class="field prepend-icon" id="discount_date_from_lb">

                              <?php echo Form::hidden('current_date',date('d-m-Y'),array('class' => 'gui-input current_date','placeholder' => '','id'=>'current_date'  )); ?>


                              <?php if($pessi_details->pessi_further_date == "" || $pessi_details->pessi_further_date ==  "1970-01-01"): ?>
                                
                                <?php if($pessi_details->pessi_status == 1): ?>
                                  <?php echo Form::text('further_date','',array('class' => 'gui-input disposal_to','placeholder' => '','id'=>'' )); ?>

                                <?php else: ?>
                                  <?php echo Form::text('further_date','',array('class' => 'gui-input','placeholder' => '','id'=>'' ,disabled )); ?>

                                <?php endif; ?>
                              <?php else: ?>

                                <?php if($pessi_details->pessi_status == 1): ?>
                                  <?php echo Form::text('further_date',date('d-m-Y',strtotime($pessi_details->pessi_further_date)),array('class' => 'gui-input disposal_to','placeholder' => '','id'=>''  )); ?>

                                <?php else: ?>
                                  <?php echo Form::text('further_date',date('d-m-Y',strtotime($pessi_details->pessi_further_date)),array('class' => 'gui-input disposal_to','placeholder' => '','id'=>'', disabled  )); ?>

                                <?php endif; ?>

                              <?php endif; ?>
                              <!-- // disposal_to -->
                              
                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-calendar"></i>
                              </label>      
                              <div></div>                     
                            </label>
                            <!-- <label for="level_name" class="field-label"> <b>Note :-</b> dd/mm/yyyy </label> -->
                          </div>
                        </div>

                        <div class="col-md-6" <?php if($pessi_details['pessi_choose_type'] == 1): ?> <?php else: ?> style="display: none;" <?php endif; ?> id="show_decide<?php echo e($pessi_details['pessi_id']); ?>">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;"> Decide : </label>
                              <label for="artist_state" class="field select">
                                <select class="form-control" id="decide" name="decide" >
                                  <option value=''>Select Decide </option>
                                  <option value='1' <?php echo e($pessi_details->pessi_decide_id == 1 ? 'selected="selected"' : ''); ?> >Case In Favour </option>      
                                  <option value='2' <?php echo e($pessi_details->pessi_decide_id == 2 ? 'selected="selected"' : ''); ?> >Case in against </option>
                                  <option value='3' <?php echo e($pessi_details->pessi_decide_id == 3 ? 'selected="selected"' : ''); ?> > Withdraw </option>
                                  <option value='4' <?php echo e($pessi_details->pessi_decide_id == 4 ? 'selected="selected"' : ''); ?> > None </option>
                                </select>
                                <i class="arrow double"></i>
                              </label>
                          </div>
                        </div>

                        <input type="hidden" name="decide" value="<?php echo e($pessi_details->pessi_decide_id); ?>">
                        <input type="hidden" name="choose_type" value="<?php echo e($pessi_details->pessi_choose_type); ?>">

                        <div class="col-md-2" style="display: none;" id="show_due_course<?php echo e($pessi_details['pessi_id']); ?>">
                          <button type="button" class="btn btn-success br2 btn-xs field select form-control" style="margin-top: 23px; margin-bottom: 22px;" > Due Course </button>
                        </div>


                        <div class="col-md-12">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;"> Select Stage : </label>
                              <label for="artist_state" class="field select">
                                <select class="form-control" name="reg_stage" id="reg_stage">
                                  <!-- <option value=''>Select Stage</option> -->
                                    <?php $__currentLoopData = $get_stage; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $stages): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                      <option value="<?php echo e($stages->stage_id); ?>" <?php echo e($stages->stage_id == $pessi_details->pessi_statge_id ? 'selected="selected"' : ''); ?> ><?php echo e($stages->stage_name); ?> </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                
                                </select>
                                <i class="arrow double"></i>
                              </label>
                          </div>
                        </div>

                        <div class="col-md-12">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;"> Order Sheet :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              <?php echo Form::textarea('order_sheet',$pessi_details->pessi_order_sheet,array('class' => 'gui-textarea accc1','placeholder' => '','id'=>'' )); ?>

                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-comment"></i>
                              </label                           
                            </label>
                          </div>
                        </div>

                        

<div class="col-md-12">
  <div class="field" align="left">
    <label for="documents" class="field-label" style="font-weight:600;" > <span style="float: left;margin-bottom: 10px;"> Send SMS </span> </label>
  </div>
</div>

<div class="col-md-12">
  <div class="section" style="margin-bottom: 40px;">  
    <div class="col-md-3">
      <label class="option block mn" style="font-size:12px;"> 
        <?php echo Form::checkbox('sms_type[]','1','', array( 'class' => 'check' , 'class' => 'check' )); ?>

        <span class="checkbox mn" style="border: 2px solid #4a89dc !important;"></span> Client
      </label>
    </div>

    <div class="col-md-3">
      <label class="option block mn" style="font-size:12px;">
        <?php echo Form::checkbox('sms_type[]','2', '', array( 'class' => 'check'  )); ?>

        <span class="checkbox mn" style="border: 2px solid #4a89dc !important;"></span> Referred
      </label>
    </div>

    <div class="col-md-3">
      <label class="option block mn" style="font-size:12px;">
        <?php echo Form::checkbox('sms_type[]','3','', array( 'class' => 'check', 'id' => 'sms_type' ,'onclick'=>'reg_sms_type()'  )); ?>

        <span class="checkbox mn" style="border: 2px solid #4a89dc !important;"></span> Other
      </label>
    </div>
  </div>
</div>

<div class="col-md-4" style="display: none;" id="show_other_mobile">
  <div class="section">
    <label for="level_name" class="field-label" style="font-weight:600;"> Other Mobile No. :  </label>  
    <label for="level_name" class="field prepend-icon">
      <?php echo Form::text('reg_other_mobile','',array('class' => 'gui-input','placeholder' => '' )); ?>

        <label for="Account Mobile" class="field-icon">
        <i class="fa fa-pencil"></i>
      </label>                           
    </label>
  </div>
</div>

<div class="col-md-8">
  <div class="section">
    <label for="level_name" class="field-label" style="font-weight:600;"> Text :  </label>
    <label for="level_name" class="field prepend-icon">
      <?php echo Form::text('reg_sms_text', '' ,array('class' => 'gui-input','placeholder' => '' )); ?>

        <label for="Account Mobile" class="field-icon">
        <i class="fa fa-pencil"></i>
      </label>                           
    </label>
  </div>
</div>
                      
                    </div>

                  <div class="panel-footer text-right">
                      <?php echo Form::button('Save', array('class' => 'button btn-primary mysave', 'onclick' => 'update_pessi_details("'.$pessi_details->pessi_id.'")', 'id' => 'maskedKey')); ?>

                      <!-- <?php echo Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')); ?> -->
                  </div>   
                    <?php echo Form::close(); ?>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <div class="clearfix"></div>
    </div> 
  </div>
</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

<script type="text/javascript">
  
  $(document).on("keyup",".disposal_to",function(){
      $('.mysave').removeAttr('disabled');
      var  currentdate = this.value;
      var n=$(this);    

      var checkdate = /^([0]?[1-9]|[1|2][0-9]|[3][0|1])[-]([0]?[1-9]|[1][0-2])[-]([0-9]{4}|[0-9]{4})$/.test(currentdate);
      
      if(checkdate == true) {

        value=currentdate.split("-");
        var newDate_notice =value[1]+"/"+value[0]+"/"+value[2];
        var notice_status_date = new Date(newDate_notice).getTime();

        
        var notice_issue_date = $('#current_date').val();
        notice_issue_date = notice_issue_date.split("-");
        var newDate_notice_issue = notice_issue_date[1]+"/"+notice_issue_date[0]+"/"+notice_issue_date[2];
        var notice_issue_date = new Date(newDate_notice_issue).getTime();


        //if(notice_status_date > notice_issue_date){
          n.next().next().html("");
          n.next().next().css("color", "#DE888A");
          n.parent().addClass("state-success");
          $('.mysave').removeAttr('disabled');
        // } else {
        //   n.next().next().html("Next date must be greater than current date.");
        //   n.next().next().css("color", "#DE888A");
        //   n.parent().addClass("state-error");
        //   n.parent().removeClass("state-success");
        //   $(this).closest('.mysave').html('');
        //   $('.mysave').attr('disabled','disabled');
        //   return false;
        // }

        
      } else {
        n.next().next().html("Enter date in dd-mm-yyyy format.");
        n.next().next().css("color", "#DE888A");
        n.parent().addClass("state-error");
        n.parent().removeClass("state-success");
        $(this).closest('.mysave').html('');
        $('.mysave').attr('disabled','disabled');
        return false;
      }
  });

</script>

<style type="text/css">

.mutbtnnn button{
  height: 36px !important;
}
.multiselect {
    text-align: left;
}
.btn.multiselect .caret {
    display: none;
}
.multiselect-search{
  height: 36px !important;
}

.multiselect-container li a label {
    border: 0px !important;
    background: none !important;
}
.multiselect-container.dropdown-menu{
  max-height: 200px;
  overflow-x: auto;
}
/*.dropdown-toggle{
  height: auto !important;
}*/
</style>

<script type="text/javascript">
  
  function share_email(){
    $('body').css('overflow','hidden');
    $("html, body").animate({ scrollTop: 0 }, "slow");
    document.getElementById('share_email').style.display='block';

  }

</script>

 <div id="share_email" class="modal fade in" role="dialog" style="overflow: scroll;">
  <div class="modal-dialog" style="width:700px; margin-top:80px;">
    <div class="modal-content">
      <div class="modal-header" style="padding-bottom: 35px;">
        <button type="button" class="close" data-dismiss="modal" onclick="close_button()" >&times;</button>
        <h4 class="modal-title pull-left"> Share via Email </h4>
      </div>
      <section id="content" class="table-layout animated fadeIn">
        <div>
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                  <div id="tab1_1" class="tab-pane active">      
                    <?php echo Form::open(['name'=>'form_share_email', 'url'=>'advocate-panel/share-email', 'id'=>'form_share_email' ,'files' => true ,'autocomplete'=>'off']); ?>

                      <div class="row">                                                
                        

                        <!-- <div class="col-md-6" id="">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Referred : </label>
                              <label for="artist_state" class="field">
                                <select id="referred" name="referred[]" multiple class="form-control">
                                    <?php $__currentLoopData = $referred; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $referreds): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                      <option value="<?php echo e($referreds->ref_id); ?>"> <?php echo e($referreds->ref_advocate_name); ?> </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                              </label>
                          </div>
                        </div>

                        <div class="col-md-6" id="">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Assigned : </label>
                              <label for="artist_state" class="field">
                                <select id="assigned" name="assigned[]" multiple class="form-control">
                                    <?php $__currentLoopData = $assigned; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $assigneds): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                      <option value="<?php echo e($assigneds->ref_id); ?>"> <?php echo e($assigneds->ref_advocate_name); ?> </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                              </label>
                          </div>
                        </div> -->

                        <!-- <div class="col-md-12">
                          <div class="section" style="margin-bottom: 40px;">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Send sms :  </label>  
                            
                            <div class="col-md-3">
                              <label class="option" style="font-size:12px;">
                                  <?php echo Form::radio('send_sms','1',true, array( 'class' => 'check' , 'onclick' => 'send_sms_all(1)' )); ?>

                                <span class="radio"></span> All
                              </label>
                            </div>

                            <div class="col-md-3">
                              <label class="option" style="font-size:12px;">
                                  <?php echo Form::radio('send_sms','2','', array( 'class' => 'check' , 'onclick' => 'send_sms_all(2)' )); ?>

                                <span class="radio"></span> Pending
                              </label>
                            </div>
                          </div>
                          <div class="section"></div>
                          <div class="clearfix"></div>
                        </div> -->

                        <?php if($get_case_details->assign_advocate_email != ""): ?>
                        <div class="col-md-12">
                          <div class="section" style="margin-bottom: 40px;">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Assign :  </label>  
                            
                            <div class="col-md-12">
                              <label class="option block mn" style="font-size:12px;">
                                <?php echo Form::checkbox('assign',$get_case_details->assign_id,'', array( 'class' => 'check', 'id' => 'sms_type'  )); ?>

                                <span class="checkbox mn" style="border: 2px solid #4a89dc !important;"></span> <?php echo e($get_case_details->assign_advocate_name); ?> ( <?php echo e($get_case_details->assign_advocate_email); ?>)
                              </label>
                            </div>
                          </div>
                          <div class="section"></div>
                          <div class="clearfix"></div>
                        </div>
                        <?php endif; ?>

                        <?php if($get_case_details->ref_advocate_email != ""): ?>
                        <div class="col-md-12">
                          <div class="section" style="margin-bottom: 40px;">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Referred :  </label>  
                            
                            <div class="col-md-12">
                              <label class="option block mn" style="font-size:12px;">
                                <?php echo Form::checkbox('referred',$get_case_details->ref_id,'', array( 'class' => 'check', 'id' => 'sms_type'  )); ?>

                                <span class="checkbox mn" style="border: 2px solid #4a89dc !important;"></span> <?php echo e($get_case_details->ref_advocate_name); ?> ( <?php echo e($get_case_details->ref_advocate_email); ?>)
                              </label>
                            </div>
                          </div>
                          <div class="section"></div>
                          <div class="clearfix"></div>
                        </div>
                        <?php endif; ?>


                        <?php if($get_case_details->cl_group_name != ""): ?>
                        <div class="col-md-12">
                          <div class="section" style="margin-bottom: 40px;">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Client :  </label>  
                            
                            <div class="col-md-12">
                              <label class="option block mn" style="font-size:12px;">
                                <?php echo Form::checkbox('client[]',$get_case_details->cl_id,'', array( 'class' => 'check', 'id' => 'sms_type'  )); ?>

                                <span class="checkbox mn" style="border: 2px solid #4a89dc !important;"></span> <?php echo e($get_case_details->cl_group_name); ?> ( <?php echo e($get_case_details->cl_group_email_id); ?>)
                              </label>
                            </div>
                          </div>
                          <div class="section"></div>
                          <div class="clearfix"></div>
                        </div>
                        <?php endif; ?>

                        <?php if($get_case_details->sub_client_name != ""): ?>
                        <div class="col-md-12">
                          <div class="section" style="margin-bottom: 40px;">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Sub Client :  </label>  
                            
                            <div class="col-md-12">
                              <label class="option block mn" style="font-size:12px;">
                                <?php echo Form::checkbox('sub_client[]',$get_case_details->sub_client_id,'', array( 'class' => 'check', 'id' => 'sms_type'  )); ?>

                                <span class="checkbox mn" style="border: 2px solid #4a89dc !important;"></span> <?php echo e($get_case_details->sub_client_name); ?> ( <?php echo e($get_case_details->sub_client_email_id); ?>)
                              </label>
                            </div>
                          </div>
                          <div class="section"></div>
                          <div class="clearfix"></div>
                        </div>
                        <?php endif; ?>


                        <input type="hidden" name="client_name" value="<?php echo e($get_case_details->cl_group_name); ?>">
                        <input type="hidden" name="client_email" value="<?php echo e($get_case_details->cl_group_email_id); ?>">

                        <!-- <div class="col-md-12" id="hide_framework">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Client : </label>
                              <label for="artist_state" class="field">
                                <select id="framework" name="client[]" multiple class="form-control" onchange="get_client(this.value)" >
                                    <?php $__currentLoopData = $client; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $clients): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                      <option value="<?php echo e($clients->cl_id); ?>"> <?php echo e($clients->cl_group_name); ?> <?php if($clients->cl_father_name != ""): ?> <?php echo e($clients->cl_name_prefix); ?> <?php echo e($clients->cl_father_name); ?> <?php endif; ?> <?php if($clients->cl_group_email_id != ""): ?> (<?php echo e($clients->cl_group_email_id); ?>) <?php endif; ?> </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                              </label>
                          </div>
                        </div> -->

                        <div class="col-md-12" id="framework_sms">
                        </div>

                        <div class="col-md-12" id="sub_client_view">
                          
                        </div>

                        <div class="col-md-12">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;"> Subject :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              <?php echo Form::text('subject','',array('class' => 'gui-input','placeholder' => '','id'=>'' )); ?>

                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-pencil"></i>
                              </label>                           
                            </label>
                          </div>
                        </div>

                        <input type="hidden" name="client_name" value="<?php echo e($get_case_details->cl_group_name); ?>">
                        <input type="hidden" name="client_email" value="<?php echo e($get_case_details->cl_group_email_id); ?>">

                        <div class="col-md-12">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;"> Message :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              <?php echo Form::textarea('message','',array('class' => 'gui-textarea accc1','placeholder' => '','id'=>'' )); ?>

                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-comment"></i>
                              </label>                           
                            </label>
                          </div>
                        </div>
                        <?php echo Form::hidden('reg_id',$get_case_details->reg_id,array('class' => 'gui-textarea accc1','placeholder' => '','id'=>'' )); ?>

                        <div class="col-md-12">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;"> Document Upload :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              <input type="file" id="" name="document_upload" style="padding: 10px;border: 1px solid #ccc;width: 100%;margin-bottom: 20px;" />                           
                            </label>
                          </div>
                        </div>

                      </div>

                      <div class="panel-footer text-right">
                        <?php echo Form::submit('Send', array('class' => 'button btn-primary mysave', 'onclick' => 'send_email()', 'id' => 'maskedKey')); ?>

                      </div>   
                    <?php echo Form::close(); ?>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <div class="clearfix"></div>
    </div> 
  </div>
</div>
<script type="text/javascript">

  // sms type

  function reg_sms_type(){

    if ($('#sms_type').is(":checked")){
      $('#show_other_mobile').show();
    } else {
      $('#show_other_mobile').hide();      
    }

  }


  function get_client(client_id){

    var new_selected = $("#framework").val();
    var new_new = $("#framework_3").val()

    if(new_selected == null){
      var new_selected = $("#framework_3").val();
    }

    BASE_URL = '<?php echo e(url('/')); ?>';
    $.ajax({
      url:BASE_URL+"/advocate-panel/get-sub-client-ajax-email/"+new_selected,
      success: function(result){
          $("#sub_client_view").html(result); 
      }
    });
   
  }


  // send sms

  function send_sms_all(sms){

  //  alert(sms);

    BASE_URL = '<?php echo e(url('/')); ?>';
    $.ajax({
      url:BASE_URL+"/advocate-panel/send-sms-client-email/"+sms,
      success: function(result){
          $('#hide_framework').hide();  
          $("#framework_sms").html(result); 
      }
    });
  }


  jQuery(document).ready(function() {

    $('#referred').multiselect({
      nonSelectedText: 'Select Referred',
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
      buttonWidth:'400px'
     });

    $('#assigned').multiselect({
      nonSelectedText: 'Select Assigned',
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
      buttonWidth:'400px'
     });

    $('#framework').multiselect({
      nonSelectedText: 'Select Client',
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
      buttonWidth:'400px'
     });

    $('#framework_2').multiselect({
      nonSelectedText: 'Select Sub Client',
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
      buttonWidth:'400px'
     });

    $('#framework_3').multiselect({
      nonSelectedText: 'Select Client',
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
      buttonWidth:'400px'
     });

  });

</script>


<style type="text/css">
  
  .document_clear:nth-child(4){
    clear: both;
  }
  
</style>

<script type="text/javascript">
  
  function show_further(pessi_id){
    document.getElementById("show_further_date"+pessi_id).style.display = "block";
    document.getElementById("show_decide"+pessi_id).style.display = "none";
    document.getElementById("show_due_course"+pessi_id).style.display = "none";
  }


  function show_decide(pessi_id){
    document.getElementById("show_further_date"+pessi_id).style.display = "none";
    document.getElementById("show_decide"+pessi_id).style.display = "block";
    document.getElementById("show_due_course"+pessi_id).style.display = "none";
  }


  function show_due_course(pessi_id){
    document.getElementById("show_further_date"+pessi_id).style.display = "none";
    document.getElementById("show_decide"+pessi_id).style.display = "none";
  //  document.getElementById("show_due_course").style.display = "block";
  }


  function view_compliance(compliance_id){
    $('body').css('overflow','hidden');
    $("html, body").animate({ scrollTop: 0 }, "slow");
    document.getElementById('view_compliance'+compliance_id).style.display='block';
  }

  function close_button_comp(compliance_id){
    $('body').css('overflow','auto', 'important');
    document.getElementById('view_compliance'+compliance_id).style.display='none';
  }


  function reg_petitioner(petitioner_id){
    $('body').css('overflow','hidden');
    $("html, body").animate({ scrollTop: 0 }, "slow");
    document.getElementById('reg_petitioner'+petitioner_id).style.display='block';
  }

  function close_button_petit(petitioner_id){
    $('body').css('overflow','auto', 'important');
    document.getElementById('reg_petitioner'+petitioner_id).style.display='none';
  }


  function reg_respondent(reg_id){
    $('body').css('overflow','hidden');
    $("html, body").animate({ scrollTop: 0 }, "slow");
    document.getElementById('reg_respondent'+reg_id).style.display='block';
  }

  function close_button_resp(reg_id){
    $('body').css('overflow','auto', 'important');
    document.getElementById('reg_respondent'+reg_id).style.display='none';
  }


  function pessi_details(pessi_id){
    $('body').css('overflow','hidden');
    $("html, body").animate({ scrollTop: 0 }, "slow");
    document.getElementById('pessi_details'+pessi_id).style.display='block';
  }

  function close_button_pessi(pessi_id){
    $('body').css('overflow','auto', 'important');
    document.getElementById('pessi_details'+pessi_id).style.display='none';
  }





  // function update_pessi_details() {
     
  //   if(confirm("Are you sure to want update records?")) {
  //     $("[name=form_add_pessi]").submit();    
  //   }
  // }

    function delete_pessi(pessi_id) {
     
      if(confirm("Are you sure to want delete records?")) {
        //$("[name=form_add_pessi]").submit();    

        $.ajax({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          url: "<?php echo e(url('advocate-panel/delete-pessi-new')); ?>",
          type: 'GET',
          data: {
              'pessi_id': pessi_id
          },
          success: function (data) {
            location.reload();
          }
      });

      }
    }

</script>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('advocate_admin/layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>