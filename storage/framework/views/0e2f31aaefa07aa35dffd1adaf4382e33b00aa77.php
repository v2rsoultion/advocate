<?php $__env->startSection('content'); ?>

  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="<?php echo e(url('/advocate-panel/view-certified-copy-date')); ?>">View Certified Copy Date</a>
            </li>
            <li class="crumb-icon">
              <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="<?php echo e(url('advocate-panel/dashboard')); ?>">Home</a>
            </li>
            <li class="crumb-trail">Add Certified Copy Date</li>
          </ol>
        </div>
      </header>

      <!-- Begin: Content -->

      <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center">
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-heading">
                <span class="panel-title">Add Certified Copy Date</span>
              </div>
              <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                <!-- IF any error found -->
                <div class="col-md-12">
                <?php if($errors->any()): ?>
                  <div id="log_error" class="alert alert-danger" style='font-family: josefin_sansregular !important;'>
                  <?php echo e($errors->first()); ?>  </i></div>
                <?php endif; ?>
                </div>
                <!-- IF any error found -->
                  <div id="tab1_1" class="tab-pane active">
                    <?php echo Form::open(['name'=>'form_add_question','url'=>'advocate-panel/insert-certified-copy-date/'.$get_record[0]->ccd_id,'id'=>'form_add_question']); ?>

                    <div class="row">
                        <div class="col-md-6">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Case No : </label>
                              <label for="artist_state" class="field select">
                                <select class="select2-single form-control" name="case_no" id="user_role" onchange="certified_caseno_ajax(this.value)">
                                  <option value=''>--Select-Option--</option>
                                  <?php if($get_record != ""): ?> 
                                   <?php $__currentLoopData = $defect_case_no; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $defect_case_nos): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                     <option value="<?php echo e($defect_case_nos->reg_id); ?>" <?php echo e($defect_case_nos->reg_id == $get_record[0]->defect_case_no ? 'selected="selected"' : ''); ?> ><?php echo e($defect_case_nos->reg_case_number); ?> </option>
                                   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php else: ?>
                                   <?php $__currentLoopData = $defect_case_no; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $defect_case_nos): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                     <option value="<?php echo e($defect_case_nos->reg_id); ?>"><?php echo e($defect_case_nos->reg_case_number); ?> </option>
                                   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   <?php endif; ?>
                                </select>
                              </label>
                          </div>
                        </div>
                      <div class="col-md-6">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;" >Title :  </label>  
                          <label for="level_name" class="field prepend-icon">
                            <?php echo Form::text('title','',array('class' => 'gui-input','placeholder' => '','id'=>'certified_caseno_change','disabled'=>'disabled' )); ?>

                              <label for="Account Mobile" class="field-icon">
                              <i class="fa fa-pencil"></i>
                            </label>                          
                          </label>
                        </div>
                        </div>
                      <div class="col-md-6">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;" >Date :  </label>  
                          <label for="level_name" class="field prepend-icon">
                            <?php echo Form::text('date_picker',$get_record[0]->ccd_date,array('class' => 'gui-input','placeholder' => '','id'=>'registerdate' )); ?>

                              <label for="Account Mobile" class="field-icon">
                              <i class="fa fa-calendar"></i>
                            </label>                          
                          </label>
                        </div>
                      </div>
                        <div class="col-md-6">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Date : </label>
                              <label for="artist_state" class="field select">
                                <select class="form-control" name="status_date_s" onchange="showDiv(this.value)"  >
                                  <option value='' >--Select-Option--</option>
                                  <option value='2' <?php if($get_record[0]->ccd_status_date == "2"){ echo "selected";}?>>Filled</option>
                                  <option value='1' <?php if($get_record[0]->ccd_status_date == "1"){ echo "selected";}?>>Pending</option>
                                </select>
                                <i class="arrow double"></i>
                              </label>
                          </div>
                        </div>
                      </div>

                     <div class="panel-footer text-right">
                      <?php echo Form::submit('Save', array('class' => 'button btn-primary', 'id' => 'maskedKey')); ?>

                      <?php echo Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')); ?>

                    </div>
                    <?php echo Form::close(); ?>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

<script type="text/javascript">

function certified_caseno_ajax(certified_caseno_id){

  if (certified_caseno_id != ''){
    BASE_URL = '<?php echo e(url('/')); ?>';
    $.ajax({
      url:BASE_URL+"/advocate-panel/change-certified-case-no-ajax/"+certified_caseno_id,
      success: function(result){
        $("#certified_caseno_change").val(result.petitioner+' V/S '+result.respondent);
      }
    });
  }

}

</script>

<script>
function showDiv(show_id) {

  if (show_id > 1) {
    document.getElementById('welcomeDiv').style.display = "block";
  }else if (show_id = 1){
      document.getElementById('welcomeDiv').style.display = "none";
  }

}
</script>      

<script>
function myFunction() {
    var x = document.getElementById("myDIV");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}
</script>

<style>
#myDIV {
    width: 100%;
    padding: 50px 0;
    text-align: left !important;
    margin-top: 20px;
    display:none;
}
</style>
<script>
function myFunctionsub() {
    var x = document.getElementById("subtype");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}
</script>
<script>
    function showhide()
     {
           var div = document.getElementById("myDIV");
    if (div.style.display !== "none") {
        div.style.display = "none";
    }
      div.style.display = "none";
    }
     
</script>

<style>
#subtype {
    width: 100%;
    padding: 50px 0;
    text-align: left !important;
    margin-top: 20px;
    display:none;
}
</style>
<script type="text/javascript">

  jQuery(document).ready(function() {
  
    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "This field contains alphabets only");

    jQuery.validator.addMethod("checkemail", function(value, element) 
    {
    return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
    }, "Enter a VALID email address"); 

    $.validator.addMethod('minStrict', function (value, el, param) {
     return value > param; 
    });
    $.validator.addMethod('maxStrict', function (value, el, param) {
     return value < param; 
    });


    /* @custom  validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#form_add_question").validate({

      /* @validation  states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation  rules 
      ------------------------------------------ */

      rules: {
        status_date_s: {
          required: true
        },
        title: {
          required: true
        },
        case_no: {
          required: true
        },
        status_date: {
          required: true
        },
        date_picker: {
          required: true
        },
      },

      /* @validation  error messages 
      ---------------------------------------------- */

      messages: {
        status_date_s: {
          required: 'Please Fill Required Case No'
        },
        status_date: {
          required: 'Please Fill Required Status Date'
        },
        case_no: {
          required: 'Please Fill Required Status Date'
        },
        title: {
          required: 'Please Fill Required Title'
        },
        date_picker: {
          required: 'Please Fill Required Date'
        },
      },

      /* @validation  highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });


  function addRecords(){
    var count1 = $('#countid1').val();
    var count2 = $('#countid2').val();


    var counter1 = parseInt(count1) + 1;
    var counter2 = parseInt(count2) + 1;
    $('#countid1').val(counter1);
    $('#countid2').val(counter2);

    if(count2 >= 0){
      $('#questionrow').append('<div class="questiondiv" id="questiondiv'+count1+'"><div class="divider"></div><div class="col-md-12"><div class="section"><label for="level_name" class="field-label" style="font-weight:600;" >Extra Party : </label><label for="level_name" class="field prepend-icon"><input type="text" name="extra_party" value="" class="gui-input" placeholder="" required><label for="Account Mobile" class="field-icon"><i class="fa fa-pencil"></i></label></label></div></div><div class="section row mb5 customClasss"><div class=""> <button type="button" onclick="removeRecords('+count1+');" name="add" id="add" class="btn btn-danger pull-right mr30"> <i class="fa fa-trash-o" aria-hidden="true"></i> &nbsp; Remove Fields </button></div></div></div></div>');
       // $('#countid').val(count);
    }
  }

  function removeRecords(countnew){
    $( "#questiondiv"+countnew+"" ).remove();
    var count2 = $('#countid2').val();
    var counter2 = parseInt(count2) - 1;
    $('#countid2').val(counter2);
  }


  </script>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('advocate_admin/layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>