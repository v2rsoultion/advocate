<!DOCTYPE html>
<html>
<head>
	<title> <?php echo e($title); ?> </title>
  <link rel="shortcut icon" type="image/png" href="<?php echo e(url('/')); ?>/public/images/logo2.png" sizes="100x100"/ >
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="public/css/responsive.css" rel="stylesheet">
	<link href="public/css/style.css" rel="stylesheet">	
  <link href="public/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
  
  <script src="public/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="public/js/wow.js"></script>
  <link href=" https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/fonts/fontawesome-webfont.woff2">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Nunito+Sans" rel="stylesheet">
  <script type="text/javascript" src="public/js/jquery.validate.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <?php echo Html::script('public/js/bootstrapvalidator.min.js'); ?>

  <?php echo Html::script('public/js/bootstrap-formhelpers.min.js'); ?>

  
</head>

<style type="text/css">
    body{
    background-size: cover;
    background:url(public/images/sign.jpg)center no-repeat fixed;
  }
  .form-group input{
    margin-top: 0px !important;
    margin-bottom: 0px !important;
  }

/*  .help-block{
    margin-top: -15px !important;
  }*/
  .modal-backdrop{
    position: inherit;
  }
</style>

<body>
<?php echo Form::open(['name' => 'registration' , 'id' => 'contact-form' ,'autocomplete'=>'off']); ?>

<div class="">
  <div class="container">
    <a href="<?php echo e(url('/')); ?>"><img src="public/images/F_logo_01.svg" alt="img logo"  height="60px" title="Ideal Lawyer" class="logo"></a>
      <div class="signup-page">
        <div class="row fromin">
          <h2>Sign In</h2>
          <div class="forms">
<!--             <form action="" name="registration" id="contact-form"> -->

              
              <div id="signup_messsage" style="padding: 10px 0px;"></div>
              

              <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="form-group">
                  <label for="email">Email</label>
                  <input type="text" class="form-control" name="email" id="email" autocomplete="off">
                </div>
              </div>


              <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="form-group">
                  <label for="Password">Password</label>
                  <input type="password" class="form-control" name="password" id="password" autocomplete="off">
                </div>
              </div>

              <div class="col-md-12 col-xs-12 col-sm-12 button">
                <button type="submit" class="btn btn-default" id="sign_up" title="Sign In">SIGN IN</button>
              </div>
            
          </div>

          <div class="col-md-12 col-xs-12 col-sm-12 log">
            <h4> <a href="<?php echo e(url('sign-up')); ?>" title="Sign up">Sign Up</a> | <a href="<?php echo e(url('forgot-password')); ?>" title="Forgot password">Forgot Password</a></h4>
            <h5>By signing up and using Ideal Lawyer, you are agreeing to its  <u><a href="<?php echo e(url('term-services')); ?>" title="End-user license agreement">End-user license agreement</a></u> & 
              <u><a href="<?php echo e(url('privacy-policy')); ?>" title="Privacy Policy">Privacy Policy</a></u> </h5>
          </div>
        </div>
      </div>
  </div>
  
    
  
</div>

<!-- <div class="modal" id="myModalsign" role="dialog">
      <div class="modal-dialog" style="margin-top: 15%;">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Enter a OTP</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">  
              <input type="text" class="form-control" placeholder="Enter a OTP.."  id="otp" name="otp">
            </div>
          </div> 
          <button type="button" class="btn  btn-info btn-lg">Submit</button> 
        </div>
      </div>
    </div> -->
</form> 
</body>
</html>

<script>

 $(document).ready(function(){

  $('#contact-form').bootstrapValidator({

      submitHandler: function (validotor, form, submitButton){

        $("#sign_up").html('Loading...<i class="fa fa-circle-o-notch fa-spin fa-1x fa-fw"></i>');
        $('#sign_up').prop('disabled', true);

      BASE_URL = '<?php echo e(url('/')); ?>';

      $.ajax({
        type: "POST",
        url: BASE_URL+"/submit-login",   
          data: $(form).serialize(),
          success: function(result) {
            if(result.status == 0){
              //  $('#contact-form').bootstrapValidator('resetForm', true);
                $("#sign_up").html('submit');
                $('#sign_up').prop('disabled', false);
                $("#signup_messsage").html('<div class="alert alert-danger" style="padding:10px; margin-bottom:15px;"> <a href="#" class="close close_message" data-dismiss="alert" aria-label="close">&times;</a> Incorrect Email id or password. </div>');
                $('#otp_show').show(); 
            } else if(result.status == 1){
              //  $('#contact-form').bootstrapValidator('resetForm', true);
                $("#sign_up").html('submit');
                $('#sign_up').prop('disabled', false);
                $("#signup_messsage").html('<div class="alert alert-danger" style="padding:10px; margin-bottom:15px;"> <a href="#" class="close close_message" data-dismiss="alert" aria-label="close">&times;</a>Your account has been deactivated by admin.</div>');
            } else if( result.status == 2) {
                $("#sign_up").html('submit');
                $('#sign_up').prop('disabled', false);
                $("#signup_messsage").html('<div class="alert alert-success" style="padding:10px; margin-bottom:15px;"> <a href="#" class="close close_message" data-dismiss="alert" aria-label="close">&times;</a> Login successfully, redirecting please wait.. !! </div>');
                window.location='advocate-panel/dashboard';
            } else {
                $("#sign_up").html('submit');
                $('#sign_up').prop('disabled', false);
                $("#signup_messsage").html('<div class="alert alert-danger" style="padding:10px; margin-bottom:15px;"> <a href="#" class="close close_message" data-dismiss="alert" aria-label="close">&times;</a>' +result.message+ ' </div>');
            }
          }
      });
      },
        feedbackIcons: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
              validators: {
                  notEmpty: {
                    message: 'Please enter your name'
                },
                regexp: {
                  regexp: /^[a-zA-Z_]+$/i,
                  message: 'This field contains alphabets only'
                }
              }
            },
            email: {
              validators: {
                  notEmpty: {
                    message: 'Please enter your email'
                },
                emailAddress: {
                  message: 'Please enter valid email'
                },
              }
            },    
            phone_number: {
              validators: {
                  notEmpty: {
                    message: 'Please enter your phone number'
                },
                stringLength: {
                    min: 10,
                    max: 13,
                    message: 'Phone number should be minimum 10 digits and maximum 13 digits in length'
                },
                // regexp: {
                //   regexp: /^[0-9]+$/i,
                //   message: 'This field contains numeric only'
                // }
              }
            },
            password: {
              validators: {
                notEmpty: {
                    message: 'Please enter your password'
                },
                stringLength: {
                    min: 6,
                    message: "The password should be at least 6 digits long."
                }
              }
            },
            confirm_password: {
                validators: {            
                    notEmpty: {
                        message: 'Confirm your password'
                    },
                    identical: {
                      field: 'password',
                      message: 'Password and Confirm password should be same.'
                    }
                }
            }
        }
    })
  });

 $(function(){
      $('.numeric').on('input', function (event) { 
          this.value = this.value.replace(/[^0-9]/g, '');
      });
    });
</script>
