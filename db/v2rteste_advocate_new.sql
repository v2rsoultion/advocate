-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 26, 2020 at 12:47 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `v2rteste_advocate_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `adv_account_settings`
--

CREATE TABLE `adv_account_settings` (
  `account_id` int(11) NOT NULL,
  `account_name` varchar(255) DEFAULT NULL,
  `account_email` varchar(255) DEFAULT NULL,
  `account_number` varchar(255) DEFAULT NULL,
  `account_logo` varchar(255) DEFAULT NULL,
  `account_free_trail` int(11) DEFAULT NULL,
  `account_address` longtext,
  `account_android_app` varchar(11) NOT NULL,
  `account_ios_app` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adv_account_settings`
--

INSERT INTO `adv_account_settings` (`account_id`, `account_name`, `account_email`, `account_number`, `account_logo`, `account_free_trail`, `account_address`, `account_android_app`, `account_ios_app`) VALUES
(1, 'Hello', 'email77@gmail.com', '9876543210', 'uploads/admin/1532512682.png', 10, 'INDIA', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `adv_act`
--

CREATE TABLE `adv_act` (
  `act_id` int(11) NOT NULL,
  `user_admin_id` int(11) DEFAULT NULL,
  `act_name` varchar(50) DEFAULT NULL,
  `act_short_name` varchar(50) DEFAULT NULL,
  `act_category` tinyint(2) DEFAULT NULL COMMENT '2=Civil,1=Criminal	',
  `act_status` tinyint(2) NOT NULL COMMENT '0=Inactive,1=Active	'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adv_act`
--

INSERT INTO `adv_act` (`act_id`, `user_admin_id`, `act_name`, `act_short_name`, `act_category`, `act_status`) VALUES
(1, 6, 'Act One', 'AONE', NULL, 1),
(2, 6, 'Act Two', 'Atwo', NULL, 0),
(3, 4, '5656', '6', NULL, 0),
(4, 4, 'St 132', 'ST', NULL, 0),
(5, 9, 'Test One', '1', NULL, 1),
(6, 9, 'Test Two', '12', NULL, 1),
(7, 9, 'Test Three', '123', NULL, 1),
(8, 4, 'Test one', 'TO', NULL, 1),
(9, 4, 'Test Two', 'TT', NULL, 1),
(11, 8, 'Test One', 'TO', NULL, 1),
(12, 8, 'Test Two', 'TT', NULL, 1),
(13, 4, '125 CRPC', NULL, NULL, 1),
(14, 4, '438', NULL, NULL, 1),
(15, 4, '432', NULL, NULL, 1),
(16, 14, '270 (I)', NULL, NULL, 1),
(17, 14, '270 (II)', NULL, NULL, 1),
(18, 14, '270 (III)', NULL, NULL, 1),
(19, 14, '270 (IV)', NULL, NULL, 1),
(21, 14, '270 (v)', NULL, NULL, 0),
(22, 4, 'Arms Act', NULL, NULL, 1),
(23, 16, '1/101', NULL, NULL, 1),
(24, 16, '2/101', NULL, NULL, 1),
(25, 15, 'Specific Relief Act', NULL, NULL, 1),
(26, 15, 'Negotiable Instrumental Act', NULL, NULL, 1),
(27, 15, 'Indian Contract Act', NULL, NULL, 1),
(28, 15, 'Raj. Revenue Act', NULL, NULL, 1),
(29, 15, 'Consumer Protection Act', NULL, NULL, 1),
(30, 15, 'Prevention of Corruption Act', NULL, NULL, 1),
(31, 15, 'Motor Vehicle Act', NULL, NULL, 1),
(32, 15, 'Hindu Marriage Act', NULL, NULL, 1),
(33, 4, 'test', NULL, NULL, 1),
(34, 18, 'ARBITRATION AND CONCILIATION ACT, 1996', NULL, NULL, 1),
(35, 19, '1/1', NULL, NULL, 1),
(36, 19, '1/2', NULL, NULL, 1),
(37, 22, 'Motor Vehicle Act', NULL, NULL, 1),
(38, 22, 'Specific Relief Act', NULL, NULL, 1),
(39, 22, 'Arms Act', NULL, NULL, 1),
(40, 4, 'a', NULL, NULL, 1),
(41, 4, 'Harassment', NULL, NULL, 1),
(42, 4, 'Hindu Marriage Act', NULL, NULL, 1),
(43, 29, 'ACT 45', NULL, NULL, 1),
(44, 30, 'ACt-45', NULL, NULL, 1),
(45, 30, 'ACT 46', NULL, NULL, 1),
(46, 31, 'Hindu Marriage Act', NULL, NULL, 1),
(47, 31, 'Negotiable Act', NULL, NULL, 1),
(48, 31, 'Motor Vehicle Act', NULL, NULL, 1),
(49, 39, 'The National Sports University Act', NULL, NULL, 1),
(50, 39, 'The National Sports University Act', NULL, NULL, 1),
(51, 39, 'The National Sports University Act', NULL, NULL, 1),
(52, 40, 'NEGOTIABLE INSTRUMENT ACT', NULL, NULL, 1),
(53, 40, 'NEGOTIABLE INSTRUMENT ACT', NULL, NULL, 1),
(54, 31, 'YOGESH PUROHIT', NULL, NULL, 1),
(55, 31, 'aaaaa', NULL, NULL, 1),
(56, 4, 'sd', NULL, NULL, 1),
(57, 4, 'test', NULL, NULL, 1),
(58, 4, 'CPC', NULL, NULL, 1),
(59, 4, 'NEGOTIABLE INSTURCT ACT', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `adv_admin`
--

CREATE TABLE `adv_admin` (
  `admin_id` int(11) NOT NULL,
  `admin_name` varchar(255) DEFAULT NULL,
  `admin_email` varchar(255) DEFAULT NULL,
  `admin_number` varchar(255) DEFAULT NULL,
  `admin_address` text,
  `admin_image` varchar(255) DEFAULT NULL,
  `admin_password` varchar(255) DEFAULT NULL,
  `admin_firmname` varchar(255) DEFAULT NULL,
  `admin_advocates` longtext,
  `admin_type` int(11) NOT NULL COMMENT '1 - Super Admin 2 - Normal Admin',
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `admin_degree` varchar(200) DEFAULT NULL,
  `admin_court` varchar(200) DEFAULT NULL,
  `reset_password_status` int(11) NOT NULL,
  `special_permission` longtext,
  `admin_status` int(11) NOT NULL DEFAULT '1' COMMENT '0 - Deactive 1 - Active '
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adv_admin`
--

INSERT INTO `adv_admin` (`admin_id`, `admin_name`, `admin_email`, `admin_number`, `admin_address`, `admin_image`, `admin_password`, `admin_firmname`, `admin_advocates`, `admin_type`, `start_date`, `end_date`, `admin_degree`, `admin_court`, `reset_password_status`, `special_permission`, `admin_status`) VALUES
(1, 'Advocate Admin', 'advocate09@gmail.com', '0123456789', '', 'uploads/admin/1532512714.jpg', 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, 1, '0000-00-00', '0000-00-00', NULL, NULL, 1, NULL, 1),
(4, 'IDEAL LAWYER', 'v2rteam@gmail.com', '9314785478', 'Lorem ipsum dummy', 'uploads/admin/1528719314.png', 'e10adc3949ba59abbe56e057f20f883e', 'Testr', 'TEst,asdjhaskjd,sakjdjasd,mnaskdgsa', 2, '2018-06-20', '2019-12-31', 'LLB', 'High court', 0, '{\"case_registration\":\"1\",\"pessi_cause_list\":\"1\",\"daily_cause_list\":\"1\",\"sms_to_clients\":\"1\",\"order_judgement_upload\":\"1\",\"complaince\":\"1\",\"calendar\":\"1\",\"undated_case\":\"1\",\"reporting\":\"1\",\"due_course\":\"1\",\"master_modules\":\"1\"}', 1),
(6, 'Jeet Test', 'jeet@gmail.com', '9782608731', 'Paota Jodhpur', '', 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, 2, '2018-06-30', '2018-07-02', 'M.B.A', 'Jodhpur HC', 0, NULL, 1),
(10, 'Sneha', 'sneha45798@gmail.com', '6765655656', '4th road', '', 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, 2, '2018-07-02', '2018-08-02', NULL, NULL, 0, NULL, 0),
(15, 'DHEERAJ KUMAR PUROHIT', 'admin@gmail.com', '7737388902', '18 MANGAL VIHAR NEAR RTO OFFICE JODHPUR', NULL, 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, 2, '2018-08-08', '2020-02-01', NULL, NULL, 0, '{\"case_registration\":\"1\",\"pessi_cause_list\":\"1\",\"daily_cause_list\":\"1\",\"sms_to_clients\":\"1\",\"order_judgement_upload\":\"1\",\"complaince\":\"1\",\"calendar\":\"1\",\"undated_case\":\"1\",\"reporting\":\"1\",\"due_course\":\"1\",\"master_modules\":\"1\"}', 1),
(18, 'DR SACHIN ACHARYA', 'sachinpra@gmail.com', '9950718980', 'Jaipur:204, Upasana Mayfair G-1, Jamnalal Bajaj Marg \'C\' Scheme, Jaipur-302007\r\n\r\n\r\n\r\n\r\nJodhpur: 18, Mangal Vihar, Near RTO Paota \'C\' Road, Jodhpur-342006\r\n\r\n\r\n\r\nMob. 9950718980       Email:pslegalfirm@gmail.com', 'uploads/admin/1536244374.jpg', '22cd92ef7c49d6857557e3b36323d250', 'P&S LEGAL', 'DR. PRAMILA ACHARYA', 2, '2018-09-06', '2019-06-30', 'L.L.B., L.L.M., Ph.D.', 'RAJASTHAN HIGH COURT JODHPUR', 0, '{\"case_registration\":\"1\",\"pessi_cause_list\":\"1\",\"daily_cause_list\":\"1\",\"sms_to_clients\":\"1\",\"order_judgement_upload\":\"1\",\"complaince\":\"1\",\"calendar\":\"1\",\"undated_case\":\"1\",\"reporting\":\"1\",\"due_course\":\"1\",\"master_modules\":\"1\"}', 0),
(22, 'Yogesh Purohit', 'yogeshmdpurohit@gmail.com', '7877325464', '18, Mangal Vihar, Near RTO Paota \'C\' Road\r\n\r\n\r\n\r\nJodhpur', 'uploads/admin/1538630385.JPG', 'e10adc3949ba59abbe56e057f20f883e', 'Yogesh Purohit & Associates', 'Ajeet Singh, Rahul Rajpurohit, Aditya Sharma, Himanshu Maheshwari, Chayan Bothra', 2, '2018-09-09', '2019-03-28', 'B.Com L.L.B M.CA', 'Rajasthan High Court', 0, '{\"case_registration\":\"1\",\"pessi_cause_list\":\"1\",\"daily_cause_list\":\"1\",\"sms_to_clients\":\"1\",\"order_judgement_upload\":\"1\",\"complaince\":\"1\",\"calendar\":\"1\",\"undated_case\":\"1\",\"reporting\":\"1\",\"due_course\":\"1\",\"master_modules\":\"1\"}', 1),
(23, 'Aditya', 'aditya@gmail.com', '7597037331', 'Jodhpur', 'uploads/admin/1541242217.png', 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, 2, '2018-11-03', '2018-12-20', NULL, NULL, 0, '{\"case_registration\":\"1\",\"pessi_cause_list\":\"1\",\"daily_cause_list\":\"1\",\"sms_to_clients\":\"1\",\"order_judgement_upload\":\"1\",\"complaince\":\"1\",\"calendar\":\"1\",\"undated_case\":\"1\",\"reporting\":\"1\",\"due_course\":\"1\",\"master_modules\":\"1\"}', 1),
(24, 'Aditya Soni', 'abc@gmail.com', '7597037331', NULL, NULL, 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, 2, '2018-11-03', '2018-11-13', NULL, NULL, 0, '{\"case_registration\":1,\"pessi_cause_list\":1,\"daily_cause_list\":1,\"sms_to_clients\":1,\"order_judgement_upload\":1,\"complaince\":1,\"calendar\":1,\"undated_case\":1,\"reporting\":1,\"due_course\":1,\"master_modules\":1}', 1),
(25, 'Adi', 'testingv2r@gmail.com', '7597037331', NULL, NULL, 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, 2, '2018-11-03', '2018-11-13', NULL, NULL, 0, '{\"case_registration\":1,\"pessi_cause_list\":1,\"daily_cause_list\":1,\"sms_to_clients\":1,\"order_judgement_upload\":1,\"complaince\":1,\"calendar\":1,\"undated_case\":1,\"reporting\":1,\"due_course\":1,\"master_modules\":1}', 1),
(26, 'test', 'v2rtea1m@gmail.com', '7597037331', NULL, NULL, 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, 2, '2018-11-05', '2018-11-15', NULL, NULL, 0, '{\"case_registration\":1,\"pessi_cause_list\":1,\"daily_cause_list\":1,\"sms_to_clients\":1,\"order_judgement_upload\":1,\"complaince\":1,\"calendar\":1,\"undated_case\":1,\"reporting\":1,\"due_course\":1,\"master_modules\":1}', 1),
(27, 'Sneha T', 'sweta.v2rsolution@gmail.com', '656565545', '1st tr', NULL, 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, 2, '2018-11-21', '2018-11-06', NULL, NULL, 0, '[]', 1),
(28, 'test sumit', 'test@gmail.com', '1234567890', NULL, NULL, 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, 2, '2018-11-29', '2018-12-08', NULL, NULL, 0, '{\"case_registration\":\"1\",\"pessi_cause_list\":\"1\",\"daily_cause_list\":\"1\",\"sms_to_clients\":\"1\",\"order_judgement_upload\":\"1\",\"complaince\":\"1\",\"calendar\":\"1\",\"undated_case\":\"1\",\"reporting\":\"1\",\"due_course\":\"1\",\"master_modules\":\"1\"}', 1),
(30, 'ws Test', 'jone.boaz@gmail.com', '9782608731', 'BJS Jodhpur', NULL, 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, 2, '2018-12-18', '2019-12-31', NULL, NULL, 0, '{\"case_registration\":\"1\",\"pessi_cause_list\":\"1\",\"daily_cause_list\":\"1\",\"sms_to_clients\":\"1\",\"order_judgement_upload\":\"1\",\"complaince\":\"1\",\"calendar\":\"1\",\"undated_case\":\"1\",\"reporting\":\"1\",\"due_course\":\"1\",\"master_modules\":\"1\"}', 1),
(31, 'PUROHIT AND ASSOCIATION', 'bhagya@gmail.com', '9950718980', 'A-22, Shastri Nagar\r\nJodhpur', NULL, 'e10adc3949ba59abbe56e057f20f883e', 'PUROHIT & ASSOCIATES', 'JL Purohit(Sr Advocate),Rajeev Purohit, NR Budaniya, Shashank Joshi', 2, '2018-12-19', '2020-05-01', NULL, NULL, 0, '{\"case_registration\":\"1\",\"pessi_cause_list\":\"1\",\"daily_cause_list\":\"1\",\"sms_to_clients\":\"1\",\"order_judgement_upload\":\"1\",\"complaince\":\"1\",\"calendar\":\"1\",\"undated_case\":\"1\",\"reporting\":\"1\",\"due_course\":\"1\",\"master_modules\":\"1\"}', 1),
(32, 'Pramila Acharya', 'cotactyogi41@gmail.com', '9950718980', '18, Mangal Vihar, Near RTO Paota C Road\r\nJodhpur', NULL, 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, 2, '2019-01-01', '2049-12-31', NULL, NULL, 0, '{\"case_registration\":\"1\",\"pessi_cause_list\":\"1\",\"daily_cause_list\":\"1\",\"sms_to_clients\":\"1\",\"order_judgement_upload\":\"1\",\"complaince\":\"1\",\"calendar\":\"1\",\"undated_case\":\"1\",\"reporting\":\"1\",\"due_course\":\"1\",\"master_modules\":\"1\"}', 1),
(33, 'Pramila Acharya', 'contactyogi41@gmail.com', '9950718980', '18, Mangal Vihar, Near RTO Paota C Road\r\nJodhpur', NULL, 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, 2, '2019-01-01', '2049-01-31', NULL, NULL, 0, '{\"case_registration\":\"1\",\"pessi_cause_list\":\"1\",\"daily_cause_list\":\"1\",\"sms_to_clients\":\"1\",\"order_judgement_upload\":\"1\",\"complaince\":\"1\",\"calendar\":\"1\",\"undated_case\":\"1\",\"reporting\":\"1\",\"due_course\":\"1\",\"master_modules\":\"1\"}', 1),
(34, NULL, NULL, '8919324366', NULL, NULL, NULL, NULL, NULL, 2, '2019-01-06', '2019-01-16', NULL, NULL, 0, '{\"case_registration\":1,\"pessi_cause_list\":1,\"daily_cause_list\":1,\"sms_to_clients\":1,\"order_judgement_upload\":1,\"complaince\":1,\"calendar\":1,\"undated_case\":1,\"reporting\":1,\"due_course\":1,\"master_modules\":1}', 1),
(35, NULL, NULL, '9502592745', NULL, NULL, NULL, NULL, NULL, 2, '2019-01-06', '2019-01-16', NULL, NULL, 0, '{\"case_registration\":1,\"pessi_cause_list\":1,\"daily_cause_list\":1,\"sms_to_clients\":1,\"order_judgement_upload\":1,\"complaince\":1,\"calendar\":1,\"undated_case\":1,\"reporting\":1,\"due_course\":1,\"master_modules\":1}', 1),
(36, NULL, NULL, '2345678987', NULL, NULL, NULL, NULL, NULL, 2, '2019-01-06', '2019-01-16', NULL, NULL, 0, '{\"case_registration\":1,\"pessi_cause_list\":1,\"daily_cause_list\":1,\"sms_to_clients\":1,\"order_judgement_upload\":1,\"complaince\":1,\"calendar\":1,\"undated_case\":1,\"reporting\":1,\"due_course\":1,\"master_modules\":1}', 1),
(37, NULL, NULL, '9999999999', NULL, NULL, NULL, NULL, NULL, 2, '2019-01-06', '2019-01-16', NULL, NULL, 0, '{\"case_registration\":1,\"pessi_cause_list\":1,\"daily_cause_list\":1,\"sms_to_clients\":1,\"order_judgement_upload\":1,\"complaince\":1,\"calendar\":1,\"undated_case\":1,\"reporting\":1,\"due_course\":1,\"master_modules\":1}', 1),
(38, NULL, NULL, '09876522222', NULL, NULL, NULL, NULL, NULL, 2, '2019-01-06', '2019-01-16', NULL, NULL, 0, '{\"case_registration\":1,\"pessi_cause_list\":1,\"daily_cause_list\":1,\"sms_to_clients\":1,\"order_judgement_upload\":1,\"complaince\":1,\"calendar\":1,\"undated_case\":1,\"reporting\":1,\"due_course\":1,\"master_modules\":1}', 1),
(39, 'Aarohi Shivay Purohit', 'aarohi@gmail.com', NULL, '111-K, Near Vishavkarma Boarding, Laxmipura,\r\nPhalodi Dist: Jodhpur (Raj)-342301', NULL, 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, 2, '2019-01-01', '2049-12-31', NULL, NULL, 0, '{\"case_registration\":\"1\",\"pessi_cause_list\":\"1\",\"daily_cause_list\":\"1\",\"sms_to_clients\":\"1\",\"order_judgement_upload\":\"1\",\"complaince\":\"1\",\"calendar\":\"1\",\"undated_case\":\"1\",\"reporting\":\"1\",\"due_course\":\"1\",\"master_modules\":\"1\"}', 1),
(40, 'DHEERAJ KUMAR PUROHIT', 'dheerajbpurohit@gmail.com', '7737388902', '20/69 CHOPASANI HOUSING BOARD JODHPUR', NULL, 'e10adc3949ba59abbe56e057f20f883e', 'dheeraj@associate', 'YOGESH KUMAR PUROHIT ADVOCATE', 2, '2019-02-15', '2019-02-25', 'LLB, LLM , PHD', 'RAJASTHAN HIGH COURT , JODHPUR', 0, '{\"case_registration\":1,\"pessi_cause_list\":1,\"daily_cause_list\":1,\"sms_to_clients\":1,\"order_judgement_upload\":1,\"complaince\":1,\"calendar\":1,\"undated_case\":1,\"reporting\":1,\"due_course\":1,\"master_modules\":1}', 1);

-- --------------------------------------------------------

--
-- Table structure for table `adv_assigned`
--

CREATE TABLE `adv_assigned` (
  `assign_id` int(11) NOT NULL,
  `assign_advocate_name` varchar(50) DEFAULT NULL,
  `assign_advocate_email` varchar(100) DEFAULT NULL,
  `assign_mobile_number` varchar(50) DEFAULT NULL,
  `user_admin_id` int(11) NOT NULL,
  `assign_status` tinyint(2) NOT NULL COMMENT '0=Inactive,1=Active	'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adv_assigned`
--

INSERT INTO `adv_assigned` (`assign_id`, `assign_advocate_name`, `assign_advocate_email`, `assign_mobile_number`, `user_admin_id`, `assign_status`) VALUES
(1, 'Vishal Singh', NULL, '9632587410', 6, 1),
(2, 'Mohan Singh', NULL, '7894651230', 6, 0),
(3, 'rajesh', NULL, '6565654653', 4, 0),
(4, 'Test one', NULL, '9874566555', 9, 1),
(5, 'Test One', NULL, '9874566555', 9, 1),
(6, 'Test Three', NULL, '9783541235', 9, 1),
(7, 'Prem', NULL, '7896541230', 4, 1),
(8, 'Akash', NULL, '9786541259', 4, 1),
(9, 'Test One', NULL, '9786541259', 8, 1),
(10, 'Test Two', NULL, '9786541259', 8, 1),
(11, 'Shreedhar Mehta', NULL, '0987651243', 4, 1),
(12, 'Rahul Rajpurohit', NULL, '123498899', 4, 1),
(13, 'Test', NULL, '0985632563', 14, 1),
(14, 'Test One', NULL, '0985632563', 14, 1),
(15, 'Akash', NULL, '7597037331', 16, 1),
(16, 'Mangu', NULL, '7597037331', 16, 1),
(17, 'Mukesh Rajpurohit', NULL, '9571568147', 15, 1),
(18, 'Jitendra Mohan Choudhary', NULL, '7508090227', 15, 1),
(19, 'Rakesh Chotia', NULL, '9460893121', 15, 1),
(20, 'Ajeet Singh', NULL, '9461539315', 15, 1),
(21, 'M.S.B', NULL, '9619172851', 18, 1),
(22, 'P', NULL, '8520369741', 19, 1),
(23, 'Q', NULL, '7896543210', 19, 1),
(24, 'RAKESH KUMAR', NULL, '9460893121', 22, 1),
(25, 'RAHUL', NULL, '9529747370', 22, 1),
(26, 'ADITYA SHARMA', NULL, '7014852759', 22, 1),
(27, 'Adv. Sumit', NULL, '9782608731', 29, 1),
(28, 'Adv. Sumit', 'jone.boaz@gmail.com', '9782608731', 30, 1),
(29, 'Adv. Sumit', 'jone.boaz@gmail.com', '9782608731', 30, 1),
(30, 'Chayan Bothra', 'chayan7bothra@gmail.com', '7791058796', 31, 1),
(31, 'Rakesh Chota', 'adv.rakeshchotia@gmail.com', '9460893121', 31, 1),
(32, 'Rahul Rajpurohit', 'rahul.raj827@gmail.com', '9529747370', 31, 1),
(33, 'Shridhar Mehta', 'mehta.shridhar@gmail.com', '9549931174', 33, 1),
(34, 'Manvendra Singh Bhati', NULL, NULL, 33, 1),
(35, 'Rakesh Chotia', NULL, NULL, 33, 1),
(36, 'Rahul Rajpurohit', NULL, NULL, 33, 1),
(37, 'Rakesh Chotia', 'adv.rakeshchotia@gmail.com', '9460893121', 39, 1),
(38, 'Rahul Rajpurohit', 'rahul.raj827@gmail.com', '9529747370', 39, 1),
(39, 'Shridhar Mehta', 'mehta.shridhar@gmail.com', '9549931174', 39, 1),
(40, 'Manvendra Bhati', 'Mansa.bhati@gmail.com', '9602822294', 39, 1),
(41, 'Ajeet Singh', 'ajitsinghsolankibadu@gmail.com', '9461539305', 39, 1),
(42, 'Shashank Joshi', 'shash.joshi@gmail.com', '9414559100', 39, 1),
(43, 'NR Budania', 'nrbudania@gmail.com', '9829073578', 39, 1);

-- --------------------------------------------------------

--
-- Table structure for table `adv_case_reg`
--

CREATE TABLE `adv_case_reg` (
  `reg_id` int(11) NOT NULL,
  `user_admin_id` int(11) DEFAULT NULL,
  `reg_file_no` varchar(50) DEFAULT NULL,
  `reg_case_number` varchar(255) DEFAULT NULL,
  `reg_court` tinyint(2) DEFAULT NULL COMMENT '2=High Court,1=Trial Court',
  `reg_court_id` int(11) DEFAULT NULL,
  `reg_date` date DEFAULT NULL,
  `reg_case_type_category` int(11) DEFAULT NULL,
  `reg_case_type_id` int(11) DEFAULT NULL,
  `reg_vcn_number` varchar(255) DEFAULT NULL,
  `reg_case_subtype_id` int(11) DEFAULT NULL,
  `reg_stage_id` int(11) DEFAULT NULL,
  `reg_power` tinyint(4) DEFAULT NULL COMMENT '1=power petitioner, 2=power respondent, 3=caveat, 4=non',
  `reg_power_respondent` varchar(50) DEFAULT NULL,
  `reg_petitioner` varchar(255) DEFAULT NULL,
  `reg_petitioner_extra` longtext,
  `reg_respondent` varchar(255) DEFAULT NULL,
  `reg_respondent_extra` longtext,
  `reg_assigend_id` int(11) DEFAULT NULL,
  `reg_act_id` int(11) DEFAULT NULL,
  `reg_section_id` varchar(255) DEFAULT NULL,
  `reg_reffeerd_by_id` int(11) DEFAULT NULL,
  `reg_client_group_id` int(11) DEFAULT NULL,
  `reg_client_subgroup_id` int(11) DEFAULT NULL,
  `reg_fir_id` varchar(50) DEFAULT NULL,
  `reg_fir_year` int(11) DEFAULT NULL,
  `reg_police_thana` varchar(255) DEFAULT NULL,
  `reg_remark` text,
  `reg_other_field` text,
  `reg_opp_council` varchar(50) DEFAULT NULL,
  `reg_nxt_further_date` date DEFAULT NULL,
  `reg_file_uploaded` varchar(255) DEFAULT NULL,
  `reg_extra_party` varchar(255) DEFAULT NULL,
  `reg_document` varchar(255) DEFAULT NULL,
  `reg_status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1 = Pending 2 - completed',
  `reg_disposal_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adv_case_reg`
--

INSERT INTO `adv_case_reg` (`reg_id`, `user_admin_id`, `reg_file_no`, `reg_case_number`, `reg_court`, `reg_court_id`, `reg_date`, `reg_case_type_category`, `reg_case_type_id`, `reg_vcn_number`, `reg_case_subtype_id`, `reg_stage_id`, `reg_power`, `reg_power_respondent`, `reg_petitioner`, `reg_petitioner_extra`, `reg_respondent`, `reg_respondent_extra`, `reg_assigend_id`, `reg_act_id`, `reg_section_id`, `reg_reffeerd_by_id`, `reg_client_group_id`, `reg_client_subgroup_id`, `reg_fir_id`, `reg_fir_year`, `reg_police_thana`, `reg_remark`, `reg_other_field`, `reg_opp_council`, `reg_nxt_further_date`, `reg_file_uploaded`, `reg_extra_party`, `reg_document`, `reg_status`, `reg_disposal_date`) VALUES
(1, 6, '26_06_2018_1', '123test', 2, NULL, '2018-06-26', NULL, 2, NULL, 3, 2, 1, NULL, 'Sandeep', '[{\"petitioner_name\":\"mohan\",\"petitioner_father_name\":\"sohan\",\"petition_address\":\"jodhpur\"}]', 'Rupam', '[{\"respondent_name\":\"roahn\",\"respondent_father_name\":\"suresh\",\"respondent_address\":\"jodhpur\"}]', 1, 2, '1', 1, 2, 4, NULL, NULL, NULL, NULL, NULL, 'test', '2018-06-26', NULL, 'null', NULL, 1, NULL),
(3, 4, '27_06_2018_3', '131', 2, NULL, '2018-06-06', 2, 5, NULL, 5, 3, 1, NULL, 'DHEERAJ KUMAR', '[{\"petitioner_name\":\"DHEERAJ KUMAR\",\"petitioner_father_name\":\"RANVEER KUMAR\",\"petition_address\":\"JODHPUR\"}]', 'YOGESH', '[{\"respondent_name\":\"YOGESH KUMAR\",\"respondent_father_name\":\"RANVEER\",\"respondent_address\":\"JODHPUR\"}]', 3, 4, '4', 3, 5, NULL, NULL, NULL, NULL, NULL, NULL, 'RAMESH G', '2018-06-30', NULL, 'null', NULL, 1, '2018-07-03'),
(4, 9, '27_06_2018_4', 'Test', 2, NULL, '2018-06-27', 2, 8, '111111111111111111111111111111111111222222222222222222222222222222222333333333333333333333333333333333333344444444444444444444444444444444458555555556666666666666', 7, 4, 3, NULL, 'Test', '{\"1\":{\"petitioner_name\":\"Test\",\"petitioner_father_name\":\"Test\",\"petition_address\":\"Test\"},\"2\":{\"petitioner_name\":\"Test 1\",\"petitioner_father_name\":\"Test 2\",\"petition_address\":\"Test\"}}', 'Test', 'null', 6, 5, '8', 7, 9, 7, NULL, NULL, NULL, NULL, NULL, 'Test', '2018-08-01', NULL, 'null', NULL, 1, NULL),
(5, 9, '28_06_2018_5', 'Test', 2, NULL, '2018-06-01', 1, 6, NULL, 7, 4, 4, NULL, 'Test', '[{\"petitioner_name\":null,\"petitioner_father_name\":null,\"petition_address\":null}]', 'Test', '[{\"respondent_name\":null,\"respondent_father_name\":null,\"respondent_address\":null}]', 6, 7, '6', 7, 6, NULL, 'Test', NULL, NULL, NULL, NULL, 'Test', '2018-06-30', NULL, 'null', NULL, 1, NULL),
(6, 6, '28_06_2018_6', '123', 2, NULL, '2018-06-28', 1, 1, NULL, 1, 2, 1, NULL, 'Sandeep', '[{\"petitioner_name\":\"mohan\",\"petitioner_father_name\":\"sohan\",\"petition_address\":\"test\"}]', 'Rupam', '[{\"respondent_name\":\"rohan\",\"respondent_father_name\":\"johan\",\"respondent_address\":\"test\"}]', 1, 1, '3', 1, 2, 3, '123', NULL, NULL, NULL, NULL, 'test', '2018-06-29', NULL, 'null', NULL, 1, NULL),
(7, 4, '30_06_2018_7', '1', 1, NULL, '2018-06-30', 1, 11, '9876541230', NULL, 7, 1, NULL, 'Aditya', 'null', 'Pradeep', 'null', 7, 9, '9', 8, 13, NULL, '1', NULL, NULL, NULL, NULL, '1', '2018-07-05', NULL, 'null', NULL, 1, NULL),
(9, 8, '30_06_2018_9', '12345', 1, NULL, '2018-06-30', 1, 13, '12345', NULL, 9, 1, NULL, 'Pradeep', '[{\"petitioner_name\":\"Test\",\"petitioner_father_name\":\"Test\",\"petition_address\":\"Test\"}]', 'Pradeep', '[{\"respondent_name\":\"Test\",\"respondent_father_name\":\"Test\",\"respondent_address\":\"Test\"}]', 10, 11, '11', 12, 16, NULL, '12345', NULL, NULL, NULL, NULL, '12345', '2018-06-30', NULL, 'null', NULL, 2, '2018-06-30'),
(10, 8, '30_06_2018_10', '1', 2, NULL, '2018-06-30', 2, 14, NULL, 11, 9, 4, NULL, 'Pradeep', '[{\"petitioner_name\":null,\"petitioner_father_name\":null,\"petition_address\":null}]', 'Pradeep', '[{\"respondent_name\":null,\"respondent_father_name\":null,\"respondent_address\":null}]', 9, 12, '12', 12, 16, NULL, NULL, NULL, NULL, NULL, NULL, 'Pradeep', '2018-07-01', NULL, 'null', NULL, 1, NULL),
(11, 4, '02_07_2018_11', '1245454545', 1, NULL, '2018-07-01', 2, 12, '343', NULL, 7, 2, NULL, 'James', '[{\"petitioner_name\":null,\"petitioner_father_name\":null,\"petition_address\":null}]', 'Thomas', '[{\"respondent_name\":null,\"respondent_father_name\":null,\"respondent_address\":null}]', 7, 8, '10', 8, 13, NULL, NULL, NULL, NULL, NULL, NULL, 'tena', '2018-07-05', NULL, 'null', NULL, 1, NULL),
(16, 4, '28_07_2018_16', '234/18', 2, 12, '2018-07-27', 2, 15, NULL, 12, 11, 1, NULL, 'JNVU', '{\"1\":{\"petitioner_name\":\"Modan Lal Sukhadiya University- Udaipur\",\"petitioner_father_name\":\"Mohan Lal Sukhadiya University- Udaipur\",\"petition_address\":\"R\\/o:- 111-K Near Gagan Path, Laxmi Nagar, Udaipur\"},\"2\":{\"petitioner_name\":\"Kota University\",\"petitioner_father_name\":\"Kota University\",\"petition_address\":\"Near Shyam Nagar, Pathik Road\"}}', 'MDSU', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(17, 4, '28_07_2018_17', '234/18', 2, 12, '2018-07-27', 2, 15, NULL, 12, 11, 1, NULL, 'JNVU', '{\"1\":{\"petitioner_name\":\"Modan Lal Sukhadiya University- Udaipur\",\"petitioner_father_name\":\"Mohan Lal Sukhadiya University- Udaipur\",\"petition_address\":\"R\\/o:- 111-K Near Gagan Path, Laxmi Nagar, Udaipur\"},\"2\":{\"petitioner_name\":\"Kota University\",\"petitioner_father_name\":\"Kota University\",\"petition_address\":\"Near Shyam Nagar, Pathik Road\"}}', 'MDSU', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(18, 4, '28_07_2018_18', '234/18', 2, 12, '2018-07-27', 2, 15, NULL, 12, 11, 1, NULL, 'JNVU', '{\"1\":{\"petitioner_name\":\"Modan Lal Sukhadiya University- Udaipur\",\"petitioner_father_name\":\"Mohan Lal Sukhadiya University- Udaipur\",\"petition_address\":\"R\\/o:- 111-K Near Gagan Path, Laxmi Nagar, Udaipur\"},\"2\":{\"petitioner_name\":\"Kota University\",\"petitioner_father_name\":\"Kota University\",\"petition_address\":\"Near Shyam Nagar, Pathik Road,\"}}', 'MDSU', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(19, 4, '28_07_2018_19', '123/2018', 2, 12, '2018-07-24', 2, 17, NULL, 19, 13, 1, NULL, 'Hari Shankar', '{\"1\":{\"petitioner_name\":\"Modan Lal Sukhadiya University- Udaipur\",\"petitioner_father_name\":\"Modan Lal Sukhadiya University- Udaipur\",\"petition_address\":\"111- K MLSU Capmpus, Udaipur\"},\"2\":{\"petitioner_name\":\"Kota University\",\"petitioner_father_name\":\"Kota University\",\"petition_address\":\"8-B,Kota Unversity, Kota\"},\"3\":{\"petitioner_name\":\"NIMS University Jaipur\",\"petitioner_father_name\":\"NIMS University Jaipur\",\"petition_address\":\"Near Ajmeri Puliya,NIMS University, Jaipur\"}}', 'Ram Shankar', 'null', 11, 22, '15', 14, 29, NULL, NULL, NULL, NULL, 'Defect', 'Fees Not Received', 'Rajesh Joshi', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(21, 14, '1/2018', NULL, 2, NULL, '2018-08-02', 1, 21, NULL, NULL, NULL, 1, NULL, 'Test', 'null', 'Test', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(22, 14, '2/2018', '1', 1, 17, '2018-08-02', 1, 21, '7450', NULL, 15, 1, NULL, 'Test', 'null', 'Test', 'null', 13, 19, '14', 16, 27, NULL, '54', 2015, 'Ramganj', 'Test2', 'Test3', 'Test1', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(23, 14, '3/2018', '1', 2, NULL, '2018-08-01', 2, 22, NULL, 22, 15, 2, NULL, 'Test', 'null', 'test', 'null', 14, 18, '16', 16, 27, NULL, 'asdas', 1967, 'sdasdasd', 'asdas', 'dasd', 'asdasd', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(24, 14, '4/2018', 'a', 2, 18, '2018-08-01', 1, 21, NULL, 15, 15, 3, NULL, 'Aditya', '{\"1\":{\"petitioner_name\":\"Test\",\"petitioner_father_name\":\"Test\",\"petition_address\":\"Test\"}}', 'Soni', '{\"1\":{\"respondent_name\":\"Test\",\"respondent_father_name\":\"Test\",\"respondent_address\":\"Test\"}}', 13, 19, '14', 16, 27, NULL, '1', 2018, 'Ajmer', 'Yes', 'Yeah!', 'No', '2018-08-31', NULL, 'null', NULL, 1, NULL),
(25, 14, '5/2018', 'b', 2, 18, '2018-08-02', 1, 21, NULL, 15, 15, 4, NULL, 'Aditya', '{\"1\":{\"petitioner_name\":\"Test\",\"petitioner_father_name\":\"Test\",\"petition_address\":\"Test\"}}', 'Sandeep', '{\"1\":{\"respondent_name\":\"Test\",\"respondent_father_name\":\"Test\",\"respondent_address\":\"Test\"}}', 13, 18, '16', 16, 27, NULL, '1', 2018, 'Ajmer', 'Yes', 'Yeah!', 'No', '2018-08-31', NULL, 'null', NULL, 1, NULL),
(26, 4, '15/2018', '111/2018', 1, NULL, '2015-08-09', 2, 15, '765757', 12, 11, 2, '2,5,11', 'Ram', '{\"1\":{\"petitioner_name\":\"Mr Ramesh Chandra\",\"petitioner_father_name\":\"Mr Suresh Chandra\",\"petition_address\":\"118, Gokul Puri, Near Govt School, Jodhpur\"}}', 'Rahim', 'null', 11, 22, '15', 9, 20, NULL, NULL, NULL, NULL, 'Name Not Show', 'Fee Not Received', 'Hanuman Singh', '2018-08-23', NULL, 'null', NULL, 1, NULL),
(27, 4, '16/2018', '1113/2017', 2, 12, '2018-08-09', 2, 16, NULL, 13, 12, 1, NULL, 'Om', 'null', 'Shyom', 'null', 7, 14, NULL, 10, 24, NULL, NULL, NULL, NULL, 'Memo Not Filed', 'Name Not Show', 'Ramavtar Choudhary', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(28, 4, '17/2018', '233/2018', 1, 15, '2018-08-09', 1, 19, '111', NULL, 14, 1, NULL, 'Shyam', 'null', 'Ram', 'null', 7, 13, '13', 3, 24, NULL, '2001', 1950, 'Jodhpur', 'Defect', 'Client', 'Yashraj Mehta', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(29, 4, '18/2018', '654', 1, 9, '2018-08-09', 2, 16, '456', NULL, 11, 1, NULL, 'rahul', 'null', 'jatin', 'null', 8, 14, NULL, 8, 13, NULL, '312', 1964, 'sardarpura', '100000', 'Fees Not Received', 'Rajesh Joshi', '2018-08-11', NULL, 'null', NULL, 1, NULL),
(31, 4, '19/2018', '08.01', 1, NULL, '2018-08-01', 2, 12, '1234567890', NULL, 7, 2, NULL, 'ABC', 'null', 'abc', 'null', 8, 22, '15', 9, 13, NULL, '1', 2018, 'Ajmer', 'Yes', 'Yeah!', 'No', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(32, 16, '1/2018', '1', 1, 24, '2018-08-10', 2, 26, '1', NULL, 18, 1, NULL, 'ABC', '{\"1\":{\"petitioner_name\":\"Test1\",\"petitioner_father_name\":\"Test1\",\"petition_address\":\"Test1\"}}', 'abc', '{\"1\":{\"respondent_name\":\"Test2\",\"respondent_father_name\":\"Test2\",\"respondent_address\":\"Test2\"}}', 16, 23, '17', 19, 33, NULL, 'Ajmer', 1968, 'Ajmer', 'Ajmer', 'Ajmer', 'Ajmer', '2018-08-11', NULL, 'null', NULL, 1, NULL),
(33, 16, '2/2018', '2', 2, 23, '2018-08-10', 2, 26, NULL, 23, 19, 2, 'A', 'XYZ', 'null', 'xyz', 'null', 15, 24, '18', 18, 34, 56, 'Ajmer', 1968, 'Ajmer', 'Ajmer', 'Ajmer', 'Ajmer', '2018-08-31', NULL, 'null', NULL, 1, NULL),
(34, 15, '1/2018', '565UY', 1, 22, '2018-08-01', 1, 24, '451', NULL, 17, 2, NULL, 'ANIL', 'null', 'MUKESH', 'null', NULL, NULL, NULL, NULL, 32, NULL, 'SDF', 1963, 'DSFA', 'DSF', 'SADF', 'DSFA', '2018-08-11', NULL, 'null', NULL, 1, NULL),
(35, 15, '2/2018', '5444', 2, 25, '2018-08-10', 2, 27, NULL, NULL, 17, 2, '25', 'RAVIKANT', '{\"1\":{\"petitioner_name\":\"ASDF\",\"petitioner_father_name\":\"DSAF\",\"petition_address\":\"SADFASDFDSAFDSAFSDAFDFSA\"},\"2\":{\"petitioner_name\":\"SDAFSDAAFSD\",\"petitioner_father_name\":\"DSFDSFAS\",\"petition_address\":\"DSADSAFADSFDSAAFDSA\"},\"3\":{\"petitioner_name\":\"SDAFDASFADS\",\"petitioner_father_name\":\"DSFDASFSD\",\"petition_address\":\"DFASDFDSFASDFFSF\"},\"4\":{\"petitioner_name\":\"DSFDSDFDAS\",\"petitioner_father_name\":\"DFDSAFDSAFADS\",\"petition_address\":\"FDDSFADSFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF\"},\"5\":{\"petitioner_name\":\"SDAFDSAFASDF\",\"petitioner_father_name\":\"DFDASFSSDFS\",\"petition_address\":\"FDFSAFDSFDASF\"}}', 'SURESH', '{\"1\":{\"respondent_name\":\"DSAFDASFASDDSAF\",\"respondent_father_name\":\"DSFDSAFDSFSADFSADSAD\",\"respondent_address\":\"SDADSFASFFSSASFDFDSSDADSF\"},\"2\":{\"respondent_name\":\"DSADSAFDSAFDSAFDSAFSDADDSAFSA\",\"respondent_father_name\":\"DSSASDFADSDSAADSASASDSDAASD\",\"respondent_address\":\"DSFDASASDAAFFSFSDDSFA\"},\"3\":{\"respondent_name\":\"DSAADSFSADFFDSSFADSFDA\",\"respondent_father_name\":\"DAFADSFDDSDSFA\",\"respondent_address\":\"DSDASSFDASFDFSADSSDFSDSDA\"},\"4\":{\"respondent_name\":\"DSAFADSFDSASDF\",\"respondent_father_name\":\"FDSFASDAFFASDS\",\"respondent_address\":\"DASFDSSADSFASDASF\"},\"5\":{\"respondent_name\":\"DFDAAADFADS\",\"respondent_father_name\":\"DDASFDFDASF\",\"respondent_address\":\"DSFDFSDSADSDASF\"}}', 17, NULL, NULL, 21, 32, NULL, '54', 1961, 'ASDFDASFASDF', 'DFDSAFDADSAFAASD', 'DSFDSAFDSFSD', 'ASDFDASFDASFADSDF', '2018-08-11', NULL, 'null', NULL, 1, NULL),
(36, 16, '3/2018', NULL, 2, NULL, '1970-01-01', NULL, NULL, NULL, NULL, NULL, 1, NULL, '!@#', '{\"1\":{\"petitioner_name\":\"@!#\",\"petitioner_father_name\":\"#!\",\"petition_address\":\"ad\"},\"2\":{\"petitioner_name\":\"!@#\",\"petitioner_father_name\":\"@#\",\"petition_address\":\"@!#das\"}}', '!@#', '{\"1\":{\"respondent_name\":\"@#!\",\"respondent_father_name\":\"@!#\",\"respondent_address\":\"asd\"},\"2\":{\"respondent_name\":\"@!#\",\"respondent_father_name\":\"@#\",\"respondent_address\":\"@#\"}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(37, 15, '3/2018', '2232', 2, 25, '1970-01-01', 2, 27, NULL, NULL, 17, 1, NULL, 'RANVEER', 'null', 'KAPIL', 'null', 17, NULL, NULL, 21, 32, NULL, '123', 1957, NULL, 'OFFF', 'RAM', 'RAJEEV', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(38, 4, '20/2018', NULL, 1, 8, '1970-01-01', NULL, NULL, NULL, NULL, NULL, 2, NULL, '1111', 'null', '1111', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(42, 15, '3/2018', '1111/2018', 2, 26, '2018-08-01', NULL, 27, NULL, 30, 20, 1, NULL, 'Suresh Chandra', 'null', 'State', 'null', 17, 25, '19', 21, 35, NULL, NULL, NULL, NULL, 'Defect', 'Fees Not Received', 'Rajesh Joshi', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(43, 15, '4/2018', '123/2018', 2, 26, '2018-08-05', 2, 31, NULL, 26, 21, 1, NULL, 'Om Prakash', 'null', 'State & Ors', 'null', 18, 26, '21', 22, 36, NULL, NULL, NULL, NULL, 'Time Barred', 'Permission 08.08.18', 'Ashok Mehta', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(44, 15, '5/2018', '535/2018', 2, 25, '2018-08-06', 2, 31, NULL, 32, 21, 2, NULL, 'Shyam CHouhan', 'null', 'Harshit', 'null', 20, 30, '26', 23, 39, NULL, NULL, NULL, NULL, 'Case Not Pass', 'Client Meeting', 'Harsh Mehta', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(45, 15, '6/2018', '157/2018', 2, 25, '2018-08-10', 2, 33, NULL, 33, 22, 3, NULL, 'Ram Karan', 'null', 'Manphool & Ors', 'null', 19, 31, NULL, 22, 40, NULL, NULL, NULL, NULL, 'Caveat', 'Inward Not Show', 'Gouri Shankar', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(46, 15, '7/2018', '165/2018', 2, 26, '2018-08-09', 2, 32, NULL, 28, 21, 1, NULL, 'Rahul Kumawat', 'null', 'Board of Rajasthan', 'null', 20, 29, '25', 23, 38, NULL, NULL, NULL, NULL, 'Check File', 'Annex 2 Not Filed', 'Yogesh Kumar', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(47, 15, '8/2018', '982/2017', 2, 26, '2018-08-07', 2, 34, NULL, NULL, 17, 1, NULL, 'Shankar Lal', 'null', 'Hari Gopal & Ors', 'null', 19, 28, '24', 21, 37, NULL, NULL, NULL, NULL, 'Memo Not Filed', 'Stay Copy', 'Balkrishan Thanvi', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(48, 15, '9/2018', '652/2015', 2, 25, '2018-08-02', 2, 37, NULL, NULL, NULL, 4, NULL, 'Shiv Prakash', 'null', 'Kunal Khemu', 'null', 18, 29, '25', 24, 39, NULL, NULL, NULL, NULL, 'Case Title', 'Signature Not Proper', 'Pratham Gehlot', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(49, 15, '10/2018', '9561/2018', 2, 26, '2018-08-02', 2, 32, NULL, 27, 22, 1, NULL, 'Tikam Chand', 'null', 'Rahim Watwani', 'null', 20, 30, '26', 22, 32, NULL, NULL, NULL, NULL, 'Fees Not Received', 'Defect', 'Salumber Dhoot', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(50, 15, '11/2018', '325/2015', 2, 26, '2017-08-03', 2, 34, NULL, NULL, 17, 1, NULL, 'Bunty@Banwari Lal', 'null', 'State & Anr', 'null', 17, 27, '23', 24, 40, NULL, NULL, NULL, NULL, 'Section-5 Limitation', 'Power Not Filed', 'Ajay Vyas', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(51, 15, '11/2018', '325/2013', 2, 25, '2018-08-06', 2, 27, NULL, 29, NULL, 1, NULL, 'Ramesh Kumar', 'null', 'Govind Lal', 'null', 18, 26, '21', 23, 35, NULL, NULL, NULL, NULL, 'NOC Not Received', 'Certified Copy', 'NR Budania', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(52, 15, '12/2018', '352/2018', 1, 28, '2018-08-11', NULL, 36, '123', NULL, 17, 1, NULL, 'Sukhi Devi & Anr', 'null', 'Ram Pyari & Ors', 'null', 17, 25, NULL, 23, 37, NULL, '137', 2017, 'Jodhpur', 'Title Copy', 'Bench Not Perfect', 'Ramesh Thanvi', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(53, 15, '13/2018', '375/2017', 1, 29, '2018-08-02', 1, 35, '505', NULL, 17, 1, NULL, 'Ganesh Lal', 'null', 'Ladu Ram Chaturvedi', 'null', 19, 28, '24', 21, 38, NULL, '87', 2013, 'Bhilwara', 'Lrs Application is not filed', 'Affidavit', 'SK Nahar', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(54, 15, '13/2018', '325/2015', 1, 28, '2018-08-03', 1, 28, '328', NULL, 17, 1, NULL, 'Teena Kumari', 'null', 'Jagganath', 'null', 20, 29, '25', 21, 35, NULL, '18', 2000, 'Phalodi', 'Case Not Pass', 'Type Copy Not Proper', 'Sanjeet Purohit', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(55, 15, '15/2018', '225/2015', 1, 30, '2018-08-02', 2, 36, '578', NULL, 20, 2, NULL, 'Laxmi Narayan Baheti', 'null', 'Sukhvindra Lahotiya', 'null', 18, 29, '25', 22, 40, NULL, '33', 2018, 'Doongarpur', 'Power Not Filed', 'Stay Application', 'Devendra Bohra', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(56, 15, '16/2018', '215/2015', 1, 28, '2018-08-02', 1, 29, '358', NULL, 21, 1, NULL, 'Narottam Vyas', 'null', 'Kapil Bothra', 'null', 18, 27, '23', 22, 35, NULL, '57', 2015, 'Bikaner', 'Case Not Proper', 'Defect & Limitatoin', 'Hasmukh Choudhary', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(57, 15, '17/2018', '248/2015', 1, 29, '2018-08-03', 1, 30, '587', NULL, 17, 2, NULL, 'Tikam Chand', 'null', 'Resham Singh & Ors', 'null', 19, 26, '21', 22, 35, NULL, '78', 2015, 'Osia', 'Order Sheet Proper', 'Case Not Pass', 'Shridhar Mehta', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(58, 15, '18/2018', '652/2016', 1, NULL, '2018-08-07', 2, 33, '248', NULL, 21, 4, NULL, 'Yashoda Bai', 'null', 'Krishna Mehta', 'null', 20, 26, '21', 22, 37, NULL, '57', 2015, 'Jaisalmer', 'Type Paper Mismatch', 'Petition Copy Not Proper', 'Ranjeet Joshi', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(59, 15, '19/2018', '248/2018', 1, 30, '2018-08-01', 2, 33, '243', NULL, 21, 1, NULL, 'Ravi Sekhar', 'null', 'Hari Govind', 'null', 19, 31, NULL, 22, 35, NULL, NULL, NULL, NULL, 'Annexure 1 p', NULL, 'CS Vyas', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(60, 15, '20/2018', '248/2018', 1, NULL, '2018-08-01', 2, 33, '243', NULL, 21, 1, NULL, 'Ravi Sekhar', 'null', 'Hari Govind', 'null', 19, 31, NULL, 22, 35, NULL, '958', 2015, 'Jaipur', 'Annexure No 1 Page No 25 Not Correct', '60 Days Time Barred', 'CS Vyas', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(61, 15, '21/2018', '384/2018', 1, 27, '2018-08-03', 1, 30, '784', NULL, 20, 1, NULL, 'Naveen Joshi', 'null', 'Dimpal Choudhary', 'null', 19, 26, NULL, 23, 32, NULL, '38', 2014, 'Banswara', 'Defect any other limitation', 'Signature Issue', 'Jaswant Singh', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(62, 15, '22/2018', '359/2018', 2, 26, '2018-07-01', 1, 29, NULL, 35, 20, 1, NULL, 'UOI(CBI)', 'null', 'Vineet Choudhary', 'null', 19, 30, '26', 22, 42, 64, NULL, NULL, NULL, 'Check Annexure', 'Bail Paper', 'Kumar Rajat', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(63, 15, '23/2018', '317/2018', 2, 26, '2018-07-02', 1, 28, NULL, NULL, 17, 1, NULL, 'UOI (CBI)', 'null', 'Rakesh Kumar', 'null', 17, 26, '19', 23, 42, 63, NULL, NULL, NULL, 'Copy Not Received', 'Lrs Application', 'Yashwant Kumar', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(64, 15, '24/2018', '257/2018', 1, 28, '2018-07-05', 2, 27, '351', NULL, 20, 1, NULL, 'CBI', 'null', 'Narendra Kumar', 'null', 18, 27, '22', 24, 42, 65, '32', 2017, 'Kuchaman City', 'Compliance', 'Fee Issue', 'Jayant Dashora', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(65, 15, '25/2018', '617/2018', 2, 26, '2018-07-05', 2, 31, NULL, 26, 20, 1, NULL, 'State Bank of India', 'null', 'Ankit Rajpurohit', 'null', 19, 30, '26', 23, 41, 59, NULL, NULL, NULL, 'File in Due Course', 'Early Hearing Application', 'SC Mehta', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(66, 15, '26/2018', '789/2018', 1, 29, '2018-07-05', 2, 27, '357', NULL, 21, 1, NULL, 'SBI', 'null', 'Pankaj Mathur', 'null', 18, 25, '19', 23, 41, 58, '987', 2015, 'Ajmer', 'Memo of Copy', 'Drafting', 'Rajesh Joshi', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(67, 16, '3/2018', NULL, 2, NULL, '2000-03-01', NULL, NULL, NULL, NULL, NULL, 1, NULL, 'Test', 'null', 'Test', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(68, 4, '2/1970', NULL, 2, NULL, '1970-01-01', 2, 17, NULL, NULL, NULL, 1, NULL, 'Tema', 'null', 'Thomas', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(69, 15, '2/1970', '123/2018', 2, 25, '1970-01-01', 2, 27, NULL, 29, 17, 1, NULL, 'Rakesh Kumar', 'null', 'Suresh Mohan', 'null', 20, 25, '20', 22, 40, NULL, '324', 2018, 'Rajsamand', 'Dear Sir,\r\nYour Case is filed on 31.08.18.\r\nContact undersigned immediately.', 'Jai Shree Krishana\r\n\r\nIn this case Vakalatnama is filed on 31.08.18\r\nThank you,', 'Rajesh Joshi', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(70, 15, '3/1970', '123/18', 1, 29, '1970-01-01', 2, 36, '1223', NULL, 20, 1, NULL, 'RAMESH ORS .', '{\"1\":{\"petitioner_name\":\"ANIRDH\",\"petitioner_father_name\":\"RAGHUVEER\",\"petition_address\":\"JODPUR\"}}', 'SURESH ORS', 'null', 18, 26, '21', 22, 32, NULL, '34', 1954, 'JODHPUR', 'URGENT MATTER', '234,000/-', 'RAM SINGH', '2018-08-21', NULL, 'null', NULL, 1, NULL),
(71, 15, '4/1970', '123/18', 2, 26, '1970-01-01', 2, 27, NULL, 29, 17, 1, NULL, 'DHEERAJ', '{\"1\":{\"petitioner_name\":\"PAWAN JI\",\"petitioner_father_name\":\"RITUPAL SINGH\",\"petition_address\":\"JODHPUR CHOPASNI HOUSINGH BOARD JODHPUR\"}}', 'YOGESH', 'null', 17, 29, '25', 22, 39, NULL, '34', 2018, 'RATANADA', 'URGENT', '21000', 'RAM SINGH', '2018-09-28', NULL, 'null', NULL, 1, NULL),
(72, 15, '5/1970', '123/18', 1, 29, '1970-01-01', 2, 33, '234', NULL, 17, 1, '444', 'ANURAG', '{\"1\":{\"petitioner_name\":\"ANIRUDH PANDYA\",\"petitioner_father_name\":\"GANPAT JI\",\"petition_address\":\"DISTRICT EXECISE JODHPUR\"},\"2\":{\"petitioner_name\":\"RANVEER SINGH\",\"petitioner_father_name\":\"KRIPA SINGH\",\"petition_address\":\"32 CHB JODHPUR\"}}', 'KASHYAP', '{\"1\":{\"respondent_name\":\"KASHYAP JI\",\"respondent_father_name\":\"ANURAG JI\",\"respondent_address\":\"JODHPUR\"}}', 19, 30, '26', 22, 32, NULL, '34`', 1968, 'RATANADA', 'JODHPUR URGENT CASE', '340000', 'RAN VEER SINGH', '2018-09-18', NULL, 'null', NULL, 1, NULL),
(73, 15, '6/1970', '33', 2, 26, '1970-01-01', 1, 28, NULL, NULL, 20, 2, '3,4,9,11', 'dhee', 'null', 'ab', 'null', NULL, NULL, NULL, 22, 41, 64, NULL, NULL, NULL, 'That the petitioners and other per4ators are plying their vechicles as per the time table issued by the Regional Transport Authority and also paying tax as per the prescribed schedule. It is important to mention here that ti is a Lok Parivaha Sewa Route and the Route has been opned after denotifying the scheme of nationalization.', NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(74, 4, '3/1970', NULL, 2, NULL, '2000-12-05', NULL, NULL, NULL, 13, NULL, 1, NULL, 'ram', 'null', 'Prakash', 'null', NULL, NULL, NULL, NULL, 44, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2000-05-02', NULL, 'null', NULL, 1, NULL),
(75, 16, '1/-0001', '1001', 1, 24, '0000-00-00', 2, 26, '1001', NULL, 18, 2, 'Respondent Number', 'Petitioner Name', '{\"1\":{\"petitioner_name\":\"Petitioner 1\",\"petitioner_father_name\":\"Petitioner 1 Father Name\",\"petition_address\":\"Petitioner 1 Address\"}}', 'Respondent Name', '{\"1\":{\"respondent_name\":\"Respondent 1\",\"respondent_father_name\":\"Respondent 1 Father Name\",\"respondent_address\":\"Respondent 1 Address\"}}', 16, 24, '18', 18, 34, 55, '0.001', 2018, 'Police Station', 'Remark', 'Other Field', '1,2,3,4,5,6,7,8,9,0', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(76, 16, '1/-0001', '1002', 2, 31, '0000-00-00', 1, 25, NULL, 24, 18, 4, NULL, 'Pradeep Rai', '{\"1\":{\"petitioner_name\":\"Aditya\",\"petitioner_father_name\":\"Ad\",\"petition_address\":\"Petitioner Address\"}}', 'Prem Prakash Chandel', '{\"1\":{\"respondent_name\":\"Pankaj\",\"respondent_father_name\":\"pk\",\"respondent_address\":\"Respondent Address\"}}', 15, 23, '17', 19, 33, NULL, '1002', 2017, 'Ramganj', 'Hit and Run', 'Drink and Drive', '11,12,13,14,15,16,17,18,19,20', '2018-02-01', NULL, 'null', NULL, 1, NULL),
(77, 16, '3/2018', '1003', 1, 32, '2018-12-06', 1, 25, NULL, NULL, 18, 1, NULL, 'Pradeep Rai', 'null', 'Prem Prakash Chandel', 'null', 16, 24, '18', 18, 33, NULL, 'a', 2018, 'a', 'a', 'a', 'a', '2018-11-10', NULL, 'null', NULL, 1, NULL),
(78, 16, '4/2018', NULL, 2, 31, '2018-12-06', 1, 25, NULL, NULL, NULL, 1, NULL, 'Pradeep Rai', 'null', 'Prem Prakash Chandel', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-29', NULL, 'null', NULL, 1, NULL),
(79, 4, '2/2015', NULL, 2, 12, '2015-08-09', 2, NULL, NULL, 12, 8, 2, '56', 'Ratan', 'null', 'Rahim', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(80, 16, '5/2018', NULL, 2, NULL, '2018-06-12', NULL, NULL, NULL, NULL, NULL, 1, NULL, 'Pradeep Rai', 'null', 'Prem Prakash Chandel', 'null', NULL, NULL, NULL, NULL, 33, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-09-29', NULL, 'null', NULL, 1, NULL),
(81, 16, '1/2015', NULL, 2, NULL, '2015-12-12', NULL, NULL, NULL, NULL, NULL, 1, NULL, 'sad', '{\"1\":{\"petitioner_name\":null,\"petitioner_father_name\":null,\"petition_address\":\"sdf sds\"}}', 'sadas', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(82, 16, '2/2015', NULL, 2, NULL, '2015-12-12', NULL, NULL, NULL, NULL, NULL, 1, NULL, 'sad', '{\"1\":{\"petitioner_name\":null,\"petitioner_father_name\":null,\"petition_address\":\"sdf sdssadsad\"}}', 'sadas', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(83, 16, '3/2015', NULL, 2, NULL, '2015-12-12', NULL, NULL, NULL, NULL, NULL, 1, NULL, 'sad', '{\"1\":{\"petitioner_name\":null,\"petitioner_father_name\":null,\"petition_address\":\"sdf sdssadsad\"}}', 'sadas', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(84, 16, '4/2015', NULL, 2, NULL, '2015-12-12', NULL, NULL, NULL, NULL, NULL, 1, NULL, 'sad', '{\"1\":{\"petitioner_name\":null,\"petitioner_father_name\":null,\"petition_address\":\"sdf sdssadsad\"}}', 'sadas', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(85, 16, '5/2015', NULL, 2, NULL, '2015-12-12', NULL, NULL, NULL, NULL, NULL, 1, NULL, 'sad', '{\"1\":{\"petitioner_name\":null,\"petitioner_father_name\":null,\"petition_address\":\"sdf sdssadsad\"}}', 'sadas', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(86, 16, '6/2015', NULL, 2, NULL, '2015-12-12', NULL, NULL, NULL, NULL, NULL, 1, NULL, 'sad', '{\"1\":{\"petitioner_name\":null,\"petitioner_father_name\":null,\"petition_address\":\"sdf sdssadsad\"}}', 'sadas', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(87, 16, '7/2015', NULL, 2, NULL, '2015-12-12', NULL, NULL, NULL, NULL, NULL, 1, NULL, 'sad', '{\"1\":{\"petitioner_name\":null,\"petitioner_father_name\":null,\"petition_address\":\"sdf sdssadsadsadas\"}}', 'sadas', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(88, 16, '8/2015', NULL, 2, NULL, '2015-12-12', NULL, NULL, NULL, NULL, NULL, 1, NULL, 'sad', '{\"1\":{\"petitioner_name\":null,\"petitioner_father_name\":null,\"petition_address\":\"sdf sdssadsadsadasd\"}}', 'sadas', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(89, 16, '9/2015', NULL, 2, NULL, '2015-12-12', NULL, NULL, NULL, NULL, NULL, 1, NULL, 'sad', '{\"1\":{\"petitioner_name\":null,\"petitioner_father_name\":null,\"petition_address\":\"sdf sdssadsadsadasd\"}}', 'sadas', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(90, 16, '10/2015', NULL, 2, NULL, '2015-12-12', NULL, NULL, NULL, NULL, NULL, 1, NULL, 'sad', '{\"1\":{\"petitioner_name\":null,\"petitioner_father_name\":null,\"petition_address\":\"sdf sdssadsadsadasdsad\"}}', 'sadas', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(91, 16, '11/2015', NULL, 2, NULL, '2015-12-12', NULL, NULL, NULL, NULL, NULL, 1, NULL, 'sad', '{\"1\":{\"petitioner_name\":null,\"petitioner_father_name\":null,\"petition_address\":\"sdf sdssadsadsadasdsad\"}}', 'sadas', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(92, 16, '12/2015', NULL, 2, NULL, '2015-12-12', NULL, NULL, NULL, NULL, NULL, 1, NULL, 'sad', '{\"1\":{\"petitioner_name\":null,\"petitioner_father_name\":null,\"petition_address\":\"sdf sdssadsadsadasdsadsad\"}}', 'sadas', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(93, 16, '1/2020', NULL, 2, NULL, '2020-12-12', NULL, NULL, NULL, NULL, NULL, 1, NULL, 'a', 'null', 'b', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-09-30', NULL, 'null', NULL, 1, NULL),
(94, 16, '2/1970', NULL, 2, 23, '1970-01-01', NULL, NULL, NULL, NULL, NULL, 1, NULL, 'c', 'null', 'd', 'null', NULL, NULL, NULL, NULL, 33, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06', NULL, 'null', NULL, 1, NULL),
(96, 4, '4/1970', NULL, 2, 12, '1970-01-01', 2, 17, '765757', NULL, 13, 1, NULL, 'sneha', 'null', 'rw', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(97, 15, '27/2018', '4444', 1, 29, '2018-01-08', 2, 31, '4444', NULL, 17, 1, NULL, 'ddd', 'null', 'dddddd', 'null', 19, 29, '25', 22, 37, NULL, '33', NULL, NULL, 'dddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd', 'dd', 'dddd', '2018-09-22', NULL, 'null', NULL, 1, NULL),
(98, 15, '7/1970', '4444', 1, 29, '1970-01-01', 2, 34, '4444', NULL, 20, 1, NULL, 'ffff', 'null', 'dddd', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(99, 15, '28/2018', NULL, 1, 30, '2018-01-01', 1, 29, NULL, NULL, NULL, 1, NULL, 'jhkjhkjh', 'null', 'kjhjihkuhu', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(100, 15, '8/1970', NULL, 1, NULL, '1970-01-01', 1, 28, '765757', NULL, NULL, 1, NULL, 'tes', 'null', 'ds', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(101, 4, '3/2015', NULL, 2, NULL, '2015-02-05', 2, 12, NULL, NULL, NULL, 1, NULL, 'sneha', 'null', 'gf', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(102, 18, '1/2018', '1/2018', 2, 33, '2018-01-02', 2, 39, NULL, NULL, 23, 1, NULL, 'SURESH CHANDRA', 'null', 'DURGA LAL', 'null', NULL, NULL, NULL, 26, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(103, 18, '2/2018', '27/18', 2, 33, '2018-01-02', 1, 44, NULL, NULL, 24, 1, NULL, 'DEVI SINGH', 'null', 'STATE', 'null', NULL, NULL, NULL, 27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(104, 18, '3/2018', '6/18', 2, 33, '2013-02-01', 2, 47, NULL, NULL, 23, 1, NULL, 'HINDUSTAN ZINZ LTD.', 'null', 'GLENCORE', '{\"1\":{\"respondent_name\":\"GENCORE INTERNATIONAL A.G. (GIAG),\",\"respondent_father_name\":null,\"respondent_address\":\"BAARERMATTSTRASSE 3, PO BOX 777, CH-6341 BAAR, SWITZERLAND\"},\"2\":{\"respondent_name\":\"LONDON COURT OF INTERNATIONAL ARB\",\"respondent_father_name\":null,\"respondent_address\":\"70 FLEET ST, LONDON EC4Y1EU, UK\"}}', 21, 34, '31', NULL, 43, 72, NULL, NULL, NULL, '1.     SEC. 34 INTERNATION COMMERCIAL ARBTRITATION . APPEAL AGAINST THE AWARD ISSUED BY LCIA. \r\n\r\n2.     SEC. 34 INTERNATION COMMERCIAL ARBTRITATION . APPEAL AGAINST THE AWARD ISSUED BY LCIA. \r\n\r\n3.   1.     SEC. 34 INTERNATION COMMERCIAL ARBTRITATION . APPEAL AGAINST THE AWARD ISSUED BY LCIA. \r\n\r\n\r\n4. 1.     SEC. 34 INTERNATION COMMERCIAL ARBTRITATION . APPEAL AGAINST THE AWARD ISSUED BY LCIA.', NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(105, 15, '9/1970', NULL, 2, NULL, '1970-01-01', NULL, NULL, NULL, NULL, NULL, 1, NULL, 'abc', 'null', 'xyz', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(106, 15, '29/2018', NULL, 2, NULL, '2018-01-02', NULL, NULL, NULL, NULL, NULL, 1, NULL, 'shyam', 'null', 'ram', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(107, 18, '4/2018', '500/17', 2, 33, '2018-01-02', 2, 39, NULL, NULL, 23, 2, NULL, 'CHANDI DEVI', 'null', 'NARESH KUMAR', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(108, 18, '5/2018', '/18', 2, 33, '2018-01-02', 2, 38, NULL, NULL, 23, 3, NULL, 'M/S LAXMI LAL', 'null', 'KAMAL NAYAN', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(109, 18, '6/2018', '1476/17', 2, 33, '2018-03-01', 2, 38, NULL, NULL, 23, 2, NULL, 'TULSI DEVI', '{\"1\":{\"petitioner_name\":null,\"petitioner_father_name\":null,\"petition_address\":null}}', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(110, 18, '7/2018', '15174/17', 2, 33, '2018-03-01', 1, 42, NULL, NULL, 23, 2, NULL, 'DEEP SINGH', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(111, 18, '8/2018', '16419/17', 2, 33, '2018-01-04', 2, 38, NULL, NULL, 23, 2, NULL, 'PRADEEP KUMAR', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(112, 18, '9/2018', '632/18', 2, 33, '2018-01-05', 2, 41, NULL, NULL, 23, 1, NULL, 'NARAYAN SINGH & ANR.', 'null', 'THEPA GAMETI & ORS.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(113, 18, '10/2018', '939/18', 2, 33, '2018-01-05', 2, 41, NULL, NULL, 23, 1, NULL, 'NARAYAN SINGH & ANR.', 'null', 'MANJU DEVI & ORS.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(114, 18, '11/2018', '72/18', 2, 33, '2018-01-08', 1, 43, NULL, NULL, 24, 1, NULL, 'RAVI @ RAVINDRA SINGH', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(115, 18, '12/2018', '9614/17', 2, 33, '2018-01-08', 2, 38, NULL, NULL, 23, 1, NULL, 'BHALA RAM', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(116, 18, '13/2018', '2/18', 2, 33, '2018-01-11', 2, 48, NULL, NULL, 23, 3, NULL, 'DOSHIAN', 'null', 'HINDUSTAN ZINC LTD.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(117, 18, '14/2018', '22/18', 2, 33, '2018-01-12', 2, 39, NULL, NULL, 23, 1, NULL, 'SHIV SHANKAR & ORS.', 'null', 'JAGDISH CHANDRA & ORS.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(118, 18, '15/2018', '23/18', 2, 33, '2018-01-12', 2, 39, NULL, NULL, 23, 1, NULL, 'SHIV SHANKAR & ORS.', 'null', 'RAJENDRA KUMAR & ORS.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(119, 18, '16/2018', '/18', 2, 33, '2018-01-09', 2, 52, NULL, NULL, 23, 1, NULL, 'OMPRAKASH', 'null', 'KAILASH CHANDRA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(120, 18, '17/2018', '63/18', 2, 33, '2018-01-11', 1, 57, NULL, NULL, 23, 1, NULL, 'PAPPU @ PAPPIDO KHADAV', 'null', 'UOI (CBI)', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(121, 18, '18/2018', NULL, 1, 36, '2018-01-12', 1, 42, NULL, NULL, 24, 1, NULL, 'SMT. JANI', 'null', 'OMPRAKASH', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(122, 18, '19/2018', '93/18', 2, 33, '2018-01-17', 1, 53, NULL, NULL, 23, 2, NULL, 'SUNIL KUMAR', 'null', 'NARESH', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(123, 18, '20/2018', '12702/17', 2, 33, '2018-01-18', 2, 38, NULL, NULL, 23, 1, NULL, 'SOHAN LAL', 'null', 'STATE ORS', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(124, 18, '21/2018', '326/18', 2, 33, '2018-01-18', 1, 42, NULL, NULL, 23, 1, NULL, 'SMT. KANKU DEVI', 'null', 'STATE & ANR.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(125, 18, '22/2018', '10/18', 2, 33, '2018-01-05', 2, 40, NULL, NULL, 23, 1, NULL, 'LRS OF GOPAL NATH', 'null', 'LRS OF NARBADA DEVI', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(126, 18, '23/2018', '881/18', 2, 33, '2018-01-19', 2, 38, NULL, NULL, 23, 2, NULL, 'UMA CHARAN', 'null', 'UOI (CBI)', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(127, 18, '24/2018', '1908/17', 2, 33, '2018-01-19', 1, 42, NULL, NULL, 23, 2, '2', 'PARSHANATH TRADERS', 'null', 'STATE & ANR.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(128, 18, '25/2018', '/18', 2, 33, '2018-01-20', 2, 48, NULL, NULL, 23, 3, NULL, 'M.C.S. HOLDING', 'null', 'HINDUSTAN ZINC LTD.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(129, 18, '26/2018', '/18', 2, 33, '2018-01-22', 2, 39, NULL, NULL, 23, 3, NULL, 'STATE OF RAJ.', 'null', 'SMT. ZAINAB BAI', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(130, 18, '27/2018', '511/18', 2, 33, '2018-01-23', 2, 38, NULL, NULL, 23, 2, NULL, 'BHUPAT SINGH', 'null', 'STATE R.H.C.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(131, 18, '28/2018', '16478', 2, 33, '2018-01-23', 2, 38, NULL, NULL, 23, 2, NULL, 'MUKESH KUMAR SARASWAT', 'null', 'R.H.C.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(132, 18, '29/2018', '16479/17', 2, 33, '2018-01-23', 2, 38, NULL, NULL, 23, 2, NULL, 'RAJENDRA SINGH', 'null', 'R.H.C.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(133, 18, '30/2018', '16480/17', 2, 33, '2018-01-23', 2, 38, NULL, NULL, 23, 2, NULL, 'VIRENDRA SINGH', 'null', 'R.H.C.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(134, 18, '31/2018', '16480', 2, 33, '2018-01-23', 2, 38, NULL, NULL, 23, 2, NULL, 'REWAT RAM', 'null', 'R.H.C.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(135, 18, '32/2018', '4333/17', 2, 33, '2018-01-22', 1, 42, NULL, NULL, 23, 2, NULL, 'VIMLA KANWAR', 'null', 'STATE & ORS.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(136, 18, '33/2018', '1133/18', 2, 33, '2018-01-23', 1, 44, NULL, NULL, 23, 1, NULL, 'MOHD. JUNAID', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(137, 18, '34/2018', '1526/18', 2, 33, '2018-01-23', 2, 38, NULL, NULL, 23, 1, NULL, 'M.M. TRAVELS AGENCY', 'null', 'STATE & ORS.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(138, 18, '35/2018', '/18', 2, 33, '2018-01-23', 1, 42, NULL, NULL, 23, 2, NULL, 'RAKESH KUMAR BANSAL', 'null', 'CBI', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(139, 18, '36/2018', '17014/17', 2, 33, '2018-01-23', 2, 38, NULL, NULL, 23, 2, NULL, 'NARESH', 'null', 'STATE(EXCISE)', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(140, 18, '37/2018', '1495/18', 2, 33, '2018-01-24', 2, 38, NULL, NULL, 23, 1, NULL, 'WOLKEM INDUSTRIES', 'null', 'U.O.I. & ORS.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(141, 18, '38/2018', '/18', 2, 33, '2018-01-29', 1, 43, NULL, NULL, 23, 1, NULL, 'TINU BHA @ KISHAN SINGH', 'null', 'STATE & ORS.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(142, 18, '39/2018', '623/18', 2, 33, '2018-01-29', 2, 38, NULL, NULL, 23, 2, NULL, 'BHANWARI', 'null', 'LT. JAIMAL & ORS.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(143, 18, '40/2018', '1781/18', 2, 33, '2018-01-30', 2, 38, NULL, NULL, 23, 1, NULL, 'MAHAVEER TRADING', 'null', 'U.O.I. & ORS.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(144, 18, '41/2018', '402/18', 2, 33, '2018-02-01', 1, 42, NULL, NULL, 23, 1, NULL, 'KOTO TRADE & SERVICE', 'null', 'STATE & ANR.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(145, 18, '42/2018', '/18', 2, 33, '2018-02-02', 2, 38, NULL, NULL, 23, 3, NULL, 'STATE OF RAJ.', 'null', 'GENERAL PUBLIC', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(146, 18, '43/2018', '1629/18', 2, 33, '2018-02-07', 2, 38, NULL, NULL, 23, 2, NULL, 'JAGAT SINGH INDUS LTD', 'null', 'STATE(EXCISE)', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(147, 18, '44/2018', '1633/18', 2, 33, '2018-02-07', 2, 38, NULL, NULL, 23, 2, NULL, 'UNITED SPIRIT LTD.', 'null', 'STATE(EXCISE)', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(148, 18, '45/2018', '3414/17', 2, 33, '2018-02-03', 2, 38, NULL, NULL, 23, 2, NULL, 'ASHOK KUMAR', 'null', 'DEEPESH CHANDRA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(149, 18, '46/2018', '3276/17', 2, 33, '2018-02-05', 2, 41, NULL, NULL, 23, 2, NULL, 'KISHAN SINGH', 'null', 'GAJANAND BARODIYA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(150, 18, '47/2018', '821/18', 2, 33, '2018-02-05', 2, 38, NULL, NULL, 23, 2, NULL, 'KISHORE KUMAR', 'null', 'STATE BANK OF INDIA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(151, 18, '48/2018', '/18', 2, 33, '2018-02-06', 2, 52, NULL, NULL, 23, 1, NULL, 'MAHAVEER PRASAD', 'null', 'SUSHILA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(152, 18, '49/2018', '1514/18', 2, 33, '2018-02-06', 1, 44, NULL, NULL, 24, 1, NULL, 'BHERU SINGH', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(153, 18, '50/2018', '838/18', 2, 33, '2018-02-07', 2, 38, NULL, NULL, 23, 1, NULL, 'NARAT MAL', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(154, 4, '2/2000', NULL, 2, NULL, '2000-02-08', NULL, NULL, NULL, NULL, NULL, 1, NULL, 'dfs', 'null', 'dsf', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ffffffffffffff\r\njkkkkkkkkkkkkkk\r\njkkkkkkkkkk\r\njh\r\nsadf\r\ndsf\r\ndfsdfs\r\nfdsdgs\r\nsdf\r\ndsg\r\ndf\r\nfsd', NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(156, 16, '2/2000', NULL, 2, NULL, '2000-12-01', NULL, NULL, NULL, NULL, NULL, 2, '1,2,3,4,5,6,4,6,', 'hghfdg', 'null', 'dfgdg', 'null', 15, 23, '17', 18, 34, 56, NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-05', NULL, 'null', NULL, 1, NULL),
(157, 18, '51/2018', '16851/17', 2, 33, '2018-02-07', 2, 38, NULL, NULL, 23, 2, NULL, 'SMT. OMI DEVI  ORS', 'null', 'BHANWAR SINGH', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(158, 18, '52/2018', '6523/17', 2, 33, '2018-02-08', 2, 38, NULL, NULL, 23, 2, NULL, 'CHHAJU RAM', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(159, 18, '53/2018', '15825/17', 2, 33, '2018-02-08', 2, 38, NULL, NULL, 23, 2, NULL, 'MAHAMAYA LIQUOR', 'null', 'STATE (EXCISE)', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(160, 18, '54/2018', NULL, 1, 37, '2018-02-04', 2, 58, NULL, NULL, 23, 1, NULL, 'HINDUN ZINC LTD.', 'null', 'BOARD OF REVENUE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(161, 18, '55/2018', '398/18', 2, 35, '2018-02-06', 1, 42, NULL, NULL, 23, 1, NULL, 'BIHARI LAL', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(162, 18, '56/2018', '424/16', 2, 33, '2018-02-12', 2, 39, NULL, NULL, 23, 2, '3', 'ABHISHEK', 'null', 'SATYANARAYAN & ORS.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(163, 18, '57/2018', '583/18', 2, 33, '2018-02-12', 1, 42, NULL, NULL, 23, 1, NULL, 'SMT. SANTOSH GEHLOT', 'null', 'STATE & ANR.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(164, 18, '58/2018', '/18', 2, 33, '2018-02-12', 2, 39, NULL, NULL, 23, 1, NULL, 'SMT. CHANDUBALA & ORS.', 'null', 'RAMESH KUMAR', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(165, 18, '59/2018', '555/18', 2, 33, '2018-02-15', 2, 46, NULL, NULL, 23, 1, NULL, 'STATE OF RAJ.', 'null', 'RAJABALA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(166, 18, '60/2018', '906/18', 2, 33, '2018-02-16', 1, 42, NULL, NULL, 23, 1, NULL, 'JAI RAJ & ORS.', 'null', 'STATE OF RAJ.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(167, 18, '61/2018', '1809/18', 2, 33, '2018-02-16', 2, 38, NULL, NULL, 23, 1, NULL, 'ASHU RAM', 'null', 'RAJ. TAX BOARD AJMER', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(168, 18, '62/2018', '536/18', 2, 33, '2018-02-17', 1, 42, NULL, NULL, 23, 2, NULL, 'ARJUN RAM', 'null', 'CBI', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(169, 18, '63/2018', '663/18', 2, 33, '2018-02-12', 1, 42, NULL, NULL, 23, 1, NULL, 'RANVEER SINGH', 'null', 'STATE OF RAJ.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(170, 18, '64/2018', '5/18', 2, 33, '2018-02-23', 2, 47, NULL, NULL, 23, 1, NULL, 'HINDUN ZINC LTD.', 'null', 'M/S GLOBAL COKE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(171, 18, '65/2018', '3145/18', 2, 33, '2018-02-26', 2, 38, NULL, NULL, 23, 1, NULL, 'SMT. CHANDRAKALA', 'null', 'RAJESH & ANR.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(172, 18, '66/2018', '144/17', 2, 33, '2018-02-26', 2, 59, NULL, NULL, 23, 2, NULL, 'RITA ARORA', 'null', 'STATE & ORS.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL);
INSERT INTO `adv_case_reg` (`reg_id`, `user_admin_id`, `reg_file_no`, `reg_case_number`, `reg_court`, `reg_court_id`, `reg_date`, `reg_case_type_category`, `reg_case_type_id`, `reg_vcn_number`, `reg_case_subtype_id`, `reg_stage_id`, `reg_power`, `reg_power_respondent`, `reg_petitioner`, `reg_petitioner_extra`, `reg_respondent`, `reg_respondent_extra`, `reg_assigend_id`, `reg_act_id`, `reg_section_id`, `reg_reffeerd_by_id`, `reg_client_group_id`, `reg_client_subgroup_id`, `reg_fir_id`, `reg_fir_year`, `reg_police_thana`, `reg_remark`, `reg_other_field`, `reg_opp_council`, `reg_nxt_further_date`, `reg_file_uploaded`, `reg_extra_party`, `reg_document`, `reg_status`, `reg_disposal_date`) VALUES
(173, 18, '67/2018', NULL, 1, 36, '2018-02-26', 1, 42, NULL, NULL, 24, 1, NULL, 'GOVIND KUMAR', 'null', 'INDRA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(174, 18, '68/2018', '3173/18', 2, 33, '2018-02-26', 1, 38, NULL, NULL, 23, 1, NULL, 'SHRI PATI MEHTA', 'null', 'RABIYA BANO & ANR.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(175, 18, '69/2018', '733/18', 2, 33, '2018-02-26', 1, 42, NULL, NULL, 23, 1, NULL, 'BHERU LAL  & ANR.', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(176, 18, '70/2018', '2556/18', 2, 33, '2018-02-26', 1, 44, NULL, NULL, 24, 1, NULL, 'DUDA RAM', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(177, 18, '71/2018', '302/18', 2, 33, '2018-02-27', 1, 43, NULL, NULL, 23, 1, NULL, 'PRAKASH KUMAR & ANR.', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(178, 18, '72/2018', '782/18', 2, 33, '2018-03-06', 1, 42, NULL, NULL, 23, 1, NULL, 'DR. NAVNEET GARG', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(179, 18, '73/2018', '144/17', 2, 33, '2018-03-05', 2, 52, NULL, NULL, 23, 2, NULL, 'RITA ARORA', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(180, 18, '74/2018', '1006/18', 2, 33, '2018-03-05', 2, 38, NULL, NULL, 23, 1, NULL, 'JAITAN', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(181, 18, '75/2018', '1041/18', 2, 33, '2018-03-05', 2, 38, NULL, NULL, 23, 2, NULL, 'HAJIYA RAM', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(182, 18, '76/2018', '3648/18', 2, 33, '2018-03-07', 1, 42, NULL, NULL, 23, 1, NULL, 'M.B. COLLEGE', 'null', 'U.O.I.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(183, 18, '77/2018', '/18', 2, 33, '2018-03-07', 1, 42, NULL, NULL, 23, 1, NULL, 'RAJENDRA SINGH', 'null', 'SONIKA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(184, 18, '78/2018', NULL, 1, 38, '2018-09-06', 2, 60, NULL, NULL, 23, 2, '2', 'PARVATI DEVI', 'null', 'B.I.G.S.( HINDUSTAN ZINC LTD.)', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(185, 18, '79/2018', '/18', 2, 33, '2018-03-07', 2, 41, NULL, NULL, 23, 1, NULL, 'AMBA LAL', 'null', 'RAMPAL', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(186, 18, '80/2018', '331/18', 2, 33, '2018-03-08', 1, 43, NULL, NULL, 23, 1, NULL, 'NARAYAN SINGH', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(187, 18, '81/2018', '875/18', 2, 33, '2018-03-08', 1, 42, NULL, NULL, 23, 1, NULL, 'JAI KUMAR JANAK RAJ', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(188, 18, '82/2018', '2468/18', 2, 33, '2018-03-08', 1, 44, NULL, NULL, 23, 1, NULL, 'AJAY KUMAR', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(189, 18, '83/2018', '3051/18', 2, 33, '2018-03-13', 2, 38, NULL, NULL, 23, 2, NULL, 'ARJUN SINGH', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(190, 18, '84/2018', '1324/18', 2, 33, '2018-03-13', 2, 38, NULL, NULL, 23, 2, '1', 'MANAGING COMMITTEE', 'null', 'GANGA DURGA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(191, 18, '85/2018', '4/18', 2, 33, '2018-03-13', 1, 55, NULL, NULL, 23, 2, NULL, 'RADHEY SHYAM', 'null', 'U.O.I. ORS(C.B.I.)', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(193, 18, '86/2018', '25/18', 2, 33, '2018-03-14', 2, 56, NULL, NULL, 23, 1, NULL, 'PINTU KUMAR', 'null', 'TANSINGH', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(194, 18, '87/2018', '/18', 2, 33, '2018-03-14', 2, 38, NULL, NULL, 23, 1, NULL, 'APEX INSTITUTE', 'null', 'U.O.I. & ORS.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(195, 18, '88/2018', '12586/17', 2, 33, '2018-03-14', 2, 38, NULL, NULL, 23, 2, NULL, 'RADICO KHAITAN', 'null', 'STATE(EXCISE)', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(196, 18, '89/2018', '16123/17', 2, 33, '2018-03-14', 2, 38, NULL, NULL, 23, 2, NULL, 'MAHAMAYA LIQUOR', 'null', 'STATE(EXCISE)', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(197, 18, '90/2018', '14413/17', 2, 33, '2018-03-14', 2, 38, NULL, NULL, 23, 2, NULL, 'MAHAMAYA LIQUOR', 'null', 'STATE(EXCISE)', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(198, 18, '91/2018', '15578/17', 2, 33, '2018-03-14', 2, 38, NULL, NULL, 23, 2, NULL, 'U.S.L.', 'null', 'STATE(EXCISE)', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(199, 18, '92/2018', '1340/18', 2, 35, '2018-03-15', 1, 42, NULL, NULL, 23, 1, NULL, 'MANISH', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(200, 18, '93/2018', '/18', 2, 33, '2018-03-15', 1, 42, NULL, NULL, 23, 1, NULL, 'AVTAR SINGH', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(201, 18, '94/2018', '156/18', 2, 33, '2018-03-21', 2, 39, NULL, NULL, 23, 3, NULL, 'BALU & ANR.', 'null', 'RATAN LAL', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(202, 18, '95/2018', '618/17', 2, 33, '2018-03-21', 2, 39, NULL, NULL, 24, 2, NULL, 'RAJESH KUMAR', 'null', 'D.E.O.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(203, 18, '96/2018', '441/17', 2, 33, '2018-03-21', 2, 38, NULL, NULL, 23, 2, NULL, 'RATAN LAL', 'null', 'FATEH LAL', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(204, 18, '97/2018', '264/17', 2, 33, '2018-03-21', 2, 40, NULL, NULL, 23, 2, NULL, 'GURU IQBAL SINGH', 'null', 'RAVINDRA SINGH', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(205, 18, '98/2018', '4439/18', 2, 33, '2018-03-21', 2, 38, NULL, NULL, 23, 1, NULL, 'SHRI GANDHI BAL NIKETAN', 'null', 'STATE & ORS.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(206, 18, '99/2018', '/18', 2, 33, '2018-03-21', 2, 38, NULL, NULL, 23, 3, NULL, 'ARJUN SINGH', 'null', 'STATE BANK OF INDIA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(207, 18, '100/2018', '3410/18', 2, 33, '2018-03-21', 2, 38, NULL, NULL, 23, 2, NULL, 'SANDEEP KUMAR', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(208, 16, '6/2018', '2', 1, 32, '2018-08-08', 1, 25, '1', NULL, 18, 3, NULL, '1', '{\"1\":{\"petitioner_name\":\"3\",\"petitioner_father_name\":\"3\",\"petition_address\":\"3\"}}', '2', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(209, 16, '7/2018', NULL, 1, 32, '2018-08-08', NULL, NULL, NULL, NULL, NULL, 1, NULL, '1', 'null', '2', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(210, 18, '101/2018', '/18', 2, 33, '2018-03-21', 2, 38, NULL, NULL, 23, 3, NULL, 'VIJENDRA SINGH', 'null', 'STATE BANK OF INDIA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(211, 18, '102/2018', '/18', 2, 33, '2018-03-21', 2, 38, NULL, NULL, NULL, 3, NULL, 'SHRA RAM', 'null', 'STATE BANK OF INDIA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(212, 18, '103/2018', '/18', 2, 33, '2018-03-21', 2, 38, NULL, NULL, 23, 3, NULL, 'SANWALA RAM', 'null', 'STATE BANK OF INDIA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(213, 18, '1/1970', NULL, NULL, NULL, '1970-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'null', NULL, 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(217, 18, '104/2018', '/18', 2, 33, '2018-03-21', 2, 38, NULL, NULL, 23, 3, NULL, 'RAMESH KUMAR', 'null', 'STATE BANK OF INDIA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(218, 18, '105/2018', '/18', 2, 33, '2018-03-21', 2, 38, NULL, NULL, 23, 3, NULL, 'RAMESH KUMAR', 'null', 'STATE BANK OF INDIA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(219, 18, '106/2018', '/18', 2, 33, '2018-03-20', 2, 38, NULL, NULL, 23, 3, NULL, 'PANKAJ DAVE', 'null', 'STATE BANK OF INDIA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(220, 18, '107/2018', '/18', 2, 33, '2018-03-20', 2, 38, NULL, NULL, 23, 3, NULL, 'NILESH SOLANKI', 'null', 'STATE BANK OF INDIA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(221, 18, '108/2018', '/18', 2, 33, '2018-03-20', 2, 38, NULL, NULL, 23, 3, NULL, 'MUSTAK', 'null', 'STATE BANK OF INDIA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(222, 18, '109/2018', '/18', 2, 33, '2018-03-20', 2, 38, NULL, NULL, 23, 3, NULL, 'GUMNA', 'null', 'STATE BANK OF INDIA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(223, 18, '110/2018', NULL, 2, 33, '2018-03-20', 2, 38, NULL, NULL, 23, 3, NULL, 'MEETHA LAL', 'null', 'STATE BANK OF INDIA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(224, 18, '111/2018', NULL, 2, 33, '2018-03-20', 2, 38, NULL, NULL, 23, 3, NULL, 'GULABA RAM', 'null', 'STATE BANK OF INDIA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(225, 18, '112/2018', NULL, 2, 33, '2018-03-20', 2, 38, NULL, NULL, 23, 3, NULL, 'GOVIND RAM', 'null', 'STATE BANK OF INDIA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(226, 18, '113/2018', NULL, 2, 33, '2018-03-20', 2, 38, NULL, NULL, 23, 3, NULL, 'CHANDRA VEER', 'null', 'STATE BANK OF INDIA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(227, 18, '114/2018', NULL, 2, 33, '2018-03-20', 2, 38, NULL, NULL, 23, 3, NULL, 'AZAD KHAN', 'null', 'STATE BANK OF INDIA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(228, 18, '115/2018', NULL, 2, 33, '2018-03-20', 2, 38, NULL, NULL, 23, 3, NULL, 'ASHOK KUMAR', 'null', 'STATE BANK OF INDIA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(229, 18, '116/2018', NULL, 2, 33, '2018-03-20', 2, 38, NULL, NULL, 23, 3, NULL, 'ASHOK KUMAR', 'null', 'STATE BANK OF INDIA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(230, 18, '117/2018', NULL, 2, 33, '2018-03-20', 2, 38, NULL, NULL, 23, 3, NULL, 'VIRENDRA PARIHAR', 'null', 'STATE BANK OF INDIA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(231, 18, '118/2018', '218/18', 2, 33, '2018-03-22', 2, 39, NULL, NULL, 23, 1, NULL, 'SMT. RESHMA DEVI', 'null', 'LRS OF KHOOB CHAND', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(232, 18, '119/2018', '/18', 2, 33, '2018-03-22', 1, 53, NULL, NULL, 23, 1, NULL, 'U.O.I. (C.B.I)', 'null', 'ANIL KUMAR', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(233, 18, '120/2018', '1519/18', 2, 33, '2018-03-27', 2, 38, NULL, NULL, 23, 2, '1', 'GANDNHI VIDHYA MANDIR', 'null', 'LAXMI DEVI', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(234, 18, '121/2018', '4645/18', 2, 33, '2018-03-27', 2, 38, NULL, NULL, 23, 1, NULL, 'SHRUTI CHARAN', 'null', 'MEDICAL COUNCIL', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(235, 18, '122/2018', '4592/18', 2, 33, '2018-04-02', 2, 38, NULL, NULL, 23, 1, NULL, 'SMT. HARSHA CHOUHAN', 'null', 'HPCL', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(236, 18, '123/2018', '/18', 2, 33, '2018-04-02', 2, 38, NULL, NULL, 23, 3, NULL, 'SWROOP DEWASI', 'null', 'STATE BANK OF INDIA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(237, 18, '124/2018', NULL, 2, 33, '2018-02-04', 2, 38, NULL, NULL, 23, 3, NULL, 'MAHENDRA SINGH', 'null', 'STATE BANK OF INDIA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(238, 18, '125/2018', NULL, 2, 33, '2018-04-02', 2, 38, NULL, NULL, 23, 3, NULL, 'DEEPA RAM', 'null', 'STATE BANK OF INDIA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(239, 18, '126/2018', NULL, 2, 33, '2018-03-27', 2, 38, NULL, NULL, 23, 3, NULL, 'ASIA ALI', 'null', 'STATE BANK OF INDIA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(240, 18, '127/2018', NULL, 2, 33, '2018-04-02', 2, 38, NULL, NULL, NULL, 3, NULL, 'MUBARAK KHAN', 'null', 'STATE BANK OF INDIA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(241, 18, '128/2018', NULL, 2, 33, '2018-04-02', 2, 38, NULL, NULL, 23, 3, NULL, 'VINOD KUMAR', 'null', 'STATE BANK OF INDIA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(242, 18, '129/2018', NULL, 2, 33, '2018-04-02', 2, 38, NULL, NULL, 23, 3, NULL, 'LAKH SINGH', 'null', 'STATE BANK OF INDIA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(243, 18, '130/2018', NULL, 2, 33, '2018-04-02', 2, 38, NULL, NULL, 23, 3, NULL, 'SUKH RAM', 'null', 'STATE BANK OF INDIA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(244, 18, '131/2018', NULL, 2, 33, '2018-04-02', 2, 38, NULL, NULL, 23, 3, NULL, 'GOVERDHAN RAM', 'null', 'STATE BANK OF INDIA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(245, 18, '132/2018', NULL, 2, 33, '2018-04-02', 2, 38, NULL, NULL, 23, 3, NULL, 'SALAUDIN BURESHI', 'null', 'STATE BANK OF INDIA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(246, 18, '133/2018', NULL, 2, 33, '2018-04-02', 2, 38, NULL, NULL, 23, 3, NULL, 'MOHD. IQBAL', 'null', 'STATE BANK OF INDIA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(247, 18, '134/2018', NULL, 2, 33, '2018-04-02', 1, 44, NULL, NULL, 24, 1, NULL, 'RAJU @ RAJENDRA', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(248, 18, '135/2018', '3372/18', 2, 33, '2018-04-02', 2, 38, NULL, NULL, 23, 2, NULL, 'BASANTI SONI', 'null', 'R.H.C.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(249, 18, '136/2018', '618/18', 2, 33, '2018-04-03', 2, 46, NULL, NULL, 23, 2, NULL, 'CHHAJJU RAM', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(250, 18, '137/2018', '343/18', 2, 33, '2018-03-22', 1, 45, NULL, NULL, 25, 2, NULL, 'MUKESH KUMAR MEENA', 'null', 'U.O.I. (C.B.I.)', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(251, 18, '138/2018', '1197/18', 2, 33, '2018-04-04', 1, 42, NULL, NULL, 23, 1, NULL, 'UDAIPUR COLD STORAGE', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(252, 18, '139/2018', '5100/18', 2, 33, '2018-04-04', 2, 38, NULL, NULL, 23, 1, NULL, 'NARPAT SINGH', 'null', 'P. OFFICER LAW', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(253, 18, '140/2018', '3375/18', 2, 33, '2018-04-05', 1, 44, NULL, NULL, 24, 1, NULL, 'AMAR CHAND', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(254, 18, '141/2018', '4434/18', 2, 33, '2018-04-05', 2, 38, NULL, NULL, 23, 1, NULL, 'DHARMVEER', 'null', 'AMICHAND', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(255, 18, '142/2018', '1093/18', 2, 33, '2018-04-05', 1, 42, NULL, NULL, 23, 1, NULL, 'RAVINDRA NATH', 'null', 'NEHA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(256, 18, '143/2018', '3407/18', 2, 33, '2018-04-07', 1, 44, NULL, NULL, 23, 1, NULL, 'AMAR CHAND', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(257, 18, '144/2018', '469/18', 2, 35, '2018-03-23', 1, 45, NULL, NULL, 23, 3, NULL, 'SATISH KATTA', 'null', 'SUNEET KARNAWAT', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(258, 18, '145/2018', '468/18', 2, 35, '2018-03-23', 1, 45, NULL, NULL, 23, 2, NULL, 'SATISH KATTA', 'null', 'SUNEET KARNAWAT', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(259, 18, '146/2018', '399/18', 2, 35, '2018-03-23', 1, 45, NULL, NULL, 24, 2, NULL, 'SATISH KATTA', 'null', 'SUNEET KARNAWAT', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(260, 18, '147/2018', '400/18', 2, 35, '2018-03-23', 1, 45, NULL, NULL, 24, 2, NULL, 'SATISH KATTA', 'null', 'SUNEET KARNAWAT', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(261, 18, '148/2018', '31/18', 2, 33, '2018-04-09', 2, 39, NULL, NULL, 23, 2, NULL, 'PRAMOD RANWAR', 'null', 'JITENDRA BOHRA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(262, 18, '149/2018', '4291/18', 2, 33, '2018-04-10', 2, 38, NULL, NULL, 23, 2, NULL, 'KAILASH KUMAR', 'null', 'R.H.C.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(263, 18, '150/2018', '2890/18', 2, 33, '2018-04-07', 2, 38, NULL, NULL, 23, 2, NULL, 'KULDEEP SINGH', 'null', 'R.H.C.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(264, 4, '12/2018', '4343', 1, 14, '2018-09-01', 2, 16, '877887', NULL, 11, 2, '2,5,11', 'Ria', 'null', 'Rahim', 'null', 8, 13, '13', 9, 22, 47, '342', 1965, 'Jodhpur', 'High Court\r\nHigh Court\r\nHigh Court\r\nHigh Court\r\nHigh Court\r\nHigh Court\r\nHigh Court\r\nHigh Court', 'High Court\r\nHigh Court\r\nHigh Court\r\nHigh Court\r\nHigh Court\r\nHigh Court\r\nHigh Court\r\nHigh Court', 'Hanuman Singh', '2018-01-09', NULL, 'null', NULL, 1, NULL),
(265, 4, '4/1970', NULL, 2, NULL, '1970-01-01', NULL, NULL, NULL, NULL, NULL, 1, NULL, 'ter', 'null', 'ter', 'null', NULL, NULL, NULL, NULL, 44, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(266, 4, '13/2018', NULL, 2, 12, '2018-09-08', 2, 16, NULL, NULL, NULL, 1, NULL, 'sneha', '{\"1\":{\"petitioner_name\":\"Mr Ramesh Chandra\",\"petitioner_father_name\":\"Mr Suresh Chandra\",\"petition_address\":\"s\"}}', 'Te', '{\"1\":{\"respondent_name\":\"ter\",\"respondent_father_name\":\"ghhj\",\"respondent_address\":\"hjksdgf\"}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(267, 19, '1/2019', '0', 2, 40, '2019-06-30', 2, 62, NULL, 39, 30, 4, NULL, '1', '{\"1\":{\"petitioner_name\":\"1\",\"petitioner_father_name\":null,\"petition_address\":\"Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s\"}}', '1', '{\"1\":{\"respondent_name\":\"1\",\"respondent_father_name\":null,\"respondent_address\":\"Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s\"}}', 22, 35, '33', 28, 45, NULL, 'a', 2018, 'a', 'Lorem Ipsum has been the industry\'s', 'Lorem Ipsum has been the industry\'', 'a', '2018-10-06', NULL, 'null', NULL, 1, NULL),
(268, 19, '2/2019', '1', 1, 39, '2019-01-25', 1, 61, '1', NULL, 30, 3, NULL, '1', 'null', '1', 'null', 23, 36, '34', 29, 45, NULL, 'a', 1968, '1', '1', '1', 'a', '2018-09-30', NULL, 'null', NULL, 1, NULL),
(269, 19, '1/2018', NULL, 2, 40, '2018-10-17', NULL, NULL, NULL, NULL, NULL, 1, NULL, '5', 'null', '6', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(271, 18, '151/2018', '4530/17', 2, 33, '2018-04-11', 1, 42, NULL, NULL, 23, 2, NULL, 'ABHIMANYU TANWAR', 'null', 'STATE (C.B.I.)', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(272, 18, '152/2018', '692/18', 2, 33, '2018-04-12', 2, 46, NULL, NULL, 23, 3, NULL, 'YASH AMU CEMENT', 'null', 'M. M. TRAVELS', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(273, 18, '153/2018', '19/18', 2, 33, '2018-04-12', 2, 63, NULL, NULL, 23, 1, NULL, 'SHWETA', 'null', 'SAURABHA MODI', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(274, 18, '154/2018', '700/18', 2, 33, '2018-04-13', 2, 41, NULL, NULL, 23, 2, NULL, 'REKHA SIROHI', 'null', 'ARUN KUMAR', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(275, 18, '155/2018', '532/17', 2, 33, '2018-04-16', 1, 42, NULL, NULL, 23, 1, NULL, 'JAI RAJ', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(276, 18, '156/2018', '5506/18', 2, 33, '2018-04-16', 2, 38, NULL, NULL, 23, 1, NULL, 'HINDUSTAN ZINC LTD.', 'null', 'State & Ors', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(277, 18, '157/2018', '4304/18', 2, 33, '2018-04-19', 2, 38, NULL, NULL, 23, 2, NULL, 'ASHISH KUMAR', 'null', 'STATE BANK OF INDIA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(278, 18, '158/2018', '6074/18', 2, 33, '2018-04-20', 2, 38, NULL, NULL, 23, 3, NULL, 'DOSHION', 'null', 'HINDUSTAN ZINC LTD.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(279, 18, '159/2018', '/18', 2, 33, '2018-04-20', 2, 41, NULL, NULL, 23, 3, NULL, 'DOSHION', 'null', 'HINDUSTAN ZINC LTD.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(280, 18, '160/2018', '/18', 2, 33, '2018-04-20', 2, 39, NULL, NULL, 23, 1, NULL, 'ARUN SUKHADIYA', 'null', 'M/S TECHNO', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(281, 18, '161/2018', '/18', 2, 33, '2018-04-23', 1, 42, NULL, NULL, 23, 1, NULL, 'RAM SINGH', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(282, 18, '162/2018', '7129/18', 2, 33, '2018-04-23', 2, 38, NULL, NULL, 23, 1, NULL, 'NISHUTOSH GUPTA', 'null', 'BALDEV', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(283, 18, '163/2018', NULL, 2, 33, '2018-04-23', 2, 38, NULL, NULL, 23, 3, NULL, 'HARI SHANKAR PALIWAL', 'null', 'STATE BANK OF INDIA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(284, 18, '164/2018', '5972/14', 2, 33, '2018-04-12', 2, 38, NULL, NULL, 23, 2, NULL, 'MALAN SINGH', 'null', 'STATE (EXCISE)', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(285, 22, '1/2017', '111/2018', 2, 41, '2017-09-01', 1, 64, NULL, 40, 31, 1, NULL, 'ASHOK KUMAR', '{\"1\":{\"petitioner_name\":\"RAMVATAR CHOUDHARY\",\"petitioner_father_name\":\"RAMDAYAL CHOUDHARY\",\"petition_address\":\"CIRCUIT HOUSE ROAD\\r\\nRAJSAMAND\"},\"2\":{\"petitioner_name\":\"SHIV DAYAL\",\"petitioner_father_name\":\"HARIKISHAN\",\"petition_address\":\"SURAJ POLE\\r\\nNEAR GANGASAHAR\\r\\nBIKANER\"}}', 'DIPESH KUMAR', 'null', 24, 37, '35', 30, 48, NULL, NULL, NULL, NULL, 'DEFECT', 'FEE NOT RECEIVED', 'RAJESH JOSHI', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(286, 22, '2/2017', '1112/2018', 2, 41, '2017-06-01', 1, 64, NULL, 41, 33, 2, '1,3', 'State', 'null', 'Devi Singh', '{\"1\":{\"respondent_name\":\"Shankar Lal\",\"respondent_father_name\":\"Ramesh Chandra\",\"respondent_address\":\"Gokul Dham, Power House Road,\\r\\nRajasamand\"},\"2\":{\"respondent_name\":\"Ghanshyam Das\",\"respondent_father_name\":\"Rukman Das\",\"respondent_address\":\"111-5, RK Vyas Nagar\\r\\nRajsamand\"}}', 25, 38, '36', 31, 49, NULL, NULL, NULL, NULL, 'ANNEXURE NOT PROPER', 'CLIENT COMMUNICATION', 'Yogesh Kumar', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(287, 22, '3/2017', '123/2018', 1, 43, '2017-12-01', 1, 64, '123', NULL, 31, 1, NULL, 'Sunil Mehta', 'null', 'State', 'null', 26, 39, '37', 32, 50, NULL, '235', 2017, 'Jodhpur', 'FIR QUASHING', 'CASE NOT PASS', 'Aarohi Purohit', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(288, 22, '1/2016', '352/2015', 2, 41, '2016-01-01', 1, 65, NULL, 42, 33, 3, NULL, 'DWARKA DAS', 'null', 'KUNAL PUROHIT', 'null', 24, 37, '35', 30, 51, NULL, NULL, NULL, NULL, 'CASE NOT PASS', 'FEE NOT RECEIVED', 'Harsh Mehta', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(289, 22, '1/2018', '982/2018', 2, 42, '2018-01-01', 1, 66, NULL, 44, 31, 1, NULL, 'ANJU JAIN', 'null', 'DAMODAR', 'null', 25, 38, '36', 31, 52, NULL, NULL, NULL, NULL, 'XXX', 'XYZ', 'Rajesh Joshi', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(290, 22, '2/2018', '972/2017', 1, 43, '2018-02-01', 1, 66, '456', NULL, 32, 1, NULL, 'KAPIL', 'null', 'MOHAN', 'null', 26, 39, '37', 32, 53, NULL, NULL, NULL, NULL, NULL, NULL, 'Harsh Mehta', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(291, 22, '3/2018', '325/2018', 2, 42, '2018-02-03', 1, 66, NULL, 45, 34, 1, NULL, 'CHAND MAL', 'null', 'HIMMAT SINGH', 'null', 24, 37, '35', 30, 48, NULL, NULL, NULL, NULL, NULL, NULL, 'Balkrishan Thanvi', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(292, 22, '4/2018', '384/2018', 1, 44, '2018-03-01', 2, 67, '573', NULL, 31, 2, '1,2', 'DEEP SINGH', 'null', 'STATE', 'null', 25, 38, '36', 31, 49, NULL, '928', 2016, 'PHALODI', NULL, NULL, 'Yogesh Kumar', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(293, 22, '5/2018', '318/2018', 2, 41, '2018-03-15', 2, 67, NULL, 46, 32, 1, NULL, 'RK SONI & ANR', 'null', 'SBI & ORS', 'null', 26, NULL, NULL, 32, 55, 78, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(294, 22, '6/2018', '715/2017', 2, 41, '2018-05-15', 2, 68, NULL, 48, 31, 2, NULL, 'TULSI DEVI', 'null', 'SBI & ORS', 'null', 24, 37, '35', 30, 55, 79, NULL, NULL, NULL, NULL, NULL, 'Harsh Mehta', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(295, 22, '1/0218', '4117/2018', 1, 45, '2018-05-15', 2, 69, '719', NULL, 34, 1, NULL, 'SBI & ORS', 'null', 'SUCHETA JAIN', 'null', 25, 38, '36', 31, 55, 80, NULL, 2015, 'SAWAI MADHOPUR', 'CHARGE SHEET NOT FILED', 'MEMO OF APPEAL', 'AAROHI PUROHIT', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(296, 22, '7/2018', '325/2017', 2, 42, '2018-06-30', 2, 69, NULL, 50, 32, 1, NULL, 'SBI', 'null', 'STATE', 'null', 26, 39, '37', 32, 55, 78, NULL, NULL, NULL, 'XYZ', 'ABCD', 'YOGESH KUMAR', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(297, 22, '9/2018', '321/207', 2, 42, '2018-06-30', 1, 65, NULL, 42, 32, 2, NULL, 'VIKRAM JEET', 'null', 'UOI(CBI)', 'null', 24, 37, '35', 30, 54, 74, NULL, NULL, NULL, 'ABC', 'XYZ', NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(298, 22, '10/2018', '248/2018', 1, 46, '2018-09-01', 1, 64, '3247', NULL, 33, 2, '3', 'SHAYM LAL CHOUDHARY', 'null', 'UOI (CBI)', 'null', 25, 39, '37', 31, 54, 75, NULL, NULL, NULL, NULL, NULL, 'Harsh Mehta', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(299, 22, '11/2018', '325/2018', 2, 41, '2018-08-15', 1, 64, NULL, 41, 31, 1, NULL, 'UOI (CBI)', 'null', 'RAJENDRA SINGH', 'null', 26, 38, '36', 32, 54, 76, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(300, 22, '12/2018', '982/2018', 2, 41, '2018-09-01', 1, 66, NULL, 44, 32, 1, NULL, 'SHYAM SUNDRA', 'null', 'UOI(CBI)', 'null', 26, 37, '35', 30, 54, 77, NULL, NULL, NULL, 'ZYX', 'CBA', 'SHYAM THANVI', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(301, 22, '13/2018', '3197/2018', 2, 41, '2018-09-05', 2, 67, NULL, 46, 32, 2, '1', 'VIRENDRA PARIHAR', 'null', 'STATE(CBI)', 'null', 24, 39, '37', 31, 54, 77, NULL, NULL, NULL, NULL, NULL, 'Gouri Shankar', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(302, 22, '14/2018', '617/2018', 2, 41, '2018-09-09', 1, 66, NULL, 45, 34, 1, NULL, 'ASHOK PRAJAPAT', 'null', 'SUSHIL CHOUDHARY', 'null', 25, 38, '36', 31, 48, NULL, NULL, NULL, NULL, NULL, NULL, 'OM PUROHIT', '2018-09-15', NULL, 'null', NULL, 1, NULL),
(303, 22, '15/2018', NULL, 2, 41, '2018-09-14', 1, 65, NULL, 42, 31, 1, NULL, 'Gynesh', 'null', 'Kuldeep Jadhav', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-09-18', NULL, 'null', NULL, 1, NULL),
(304, 22, '16/2018', NULL, 2, 41, '2018-09-16', 1, 64, NULL, 40, 31, 1, NULL, 'Ram', 'null', 'Rahim', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(305, 18, '164/2018', '2901/12', 2, 33, '2018-04-23', 2, 38, NULL, NULL, 23, 2, NULL, 'MANI RAM', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(306, 18, '165/2018', '4529/17', 2, 33, '2018-04-23', 1, 42, NULL, NULL, NULL, 2, NULL, 'SANJAY TANWAR', 'null', 'UOI', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(307, 18, '166/2018', NULL, 2, 33, '2018-04-26', 2, 47, NULL, NULL, NULL, 1, NULL, 'DOSHION', 'null', 'HINDUSTAN ZINC LTD.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(308, 18, '167/2018', NULL, 2, 33, '2018-04-26', 2, 47, NULL, NULL, NULL, 1, NULL, 'DOSHION', 'null', 'HINDUSTAN ZINC LTD.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(309, 18, '168/2018', '6022/18', 2, 33, '2018-04-26', 2, 38, NULL, NULL, NULL, 1, NULL, 'SAMPAT LAL', 'null', 'MANISH KUMAR & ORS', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(310, 22, '17/2018', '1111', 2, 41, '2018-09-16', 1, 64, NULL, 40, 31, 1, NULL, 'RAM', 'null', 'RAVAN', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-09-21', NULL, 'null', NULL, 1, NULL),
(311, 22, '18/2018', '135/2018', 2, 41, '2018-09-10', 1, 65, NULL, NULL, 31, 1, NULL, 'NARESH', 'null', 'LALIT', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-09-25', NULL, 'null', NULL, 1, NULL),
(312, 22, '19/2018', '123/2013', 2, 41, '2018-09-15', 1, 65, NULL, 42, 31, 1, NULL, 'HARGOVIND', 'null', 'KAPIL', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-09-30', NULL, 'null', NULL, 1, NULL),
(313, 22, '20/2018', '276/2017', 2, 41, '2018-09-15', 1, 66, NULL, NULL, 31, 1, NULL, 'Rama', 'null', 'Shyama', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'qjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjqjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjqjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjqjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjqjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjqjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjqjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjqjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjqjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjqjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjqjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjqjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjqjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjqjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj', NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(314, 22, '21/2018', '145/2018', 2, 41, '2018-09-11', 1, 65, NULL, 42, 31, 1, NULL, 'ramu', 'null', 'shyamu', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(315, 18, '1/2016', '136/16', 2, 33, '2016-01-04', 2, 38, NULL, NULL, 23, 1, NULL, 'Chief Manager Raj. West', 'null', 'M/s Akshaydan & Ors.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(316, 18, '2/2016', '131/16', 2, 33, '2016-01-04', 2, 38, NULL, NULL, 23, 1, NULL, 'Chandra Kanta', 'null', 'Kamal Singh & Ors.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(317, 22, '22/2018', '111/2019', 1, 44, '2018-09-18', 1, 64, NULL, NULL, 31, 1, NULL, 'ram', 'null', 'yogesh', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-11', NULL, 'null', NULL, 1, NULL),
(318, 22, '23/2018', '111', 2, NULL, '2018-11-15', 1, 64, NULL, NULL, 31, 1, NULL, 'aarohi', 'null', 'shyam', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(319, 22, '24/2018', '999', 2, 41, '2018-09-11', 1, 64, NULL, NULL, 31, 1, NULL, 'xxx', 'null', 'yyy', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(320, 18, '3/2016', '137/16', 2, 33, '2016-01-04', 2, 38, NULL, NULL, 23, 1, NULL, 'SHRAWAN SINGH', 'null', 'STATE & OTR', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(321, 18, '4/2016', '138/16', 2, 33, '2016-01-04', 2, 38, NULL, NULL, NULL, 1, NULL, 'SHRAWAN SINGH', 'null', 'STATE & OTR', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(324, 18, '5/2016', '129/16', 2, 33, '2016-01-04', 2, 38, NULL, NULL, NULL, 1, NULL, 'PRABHU SINGH', 'null', 'STATE & OTR', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(326, 18, '6/2016', '202/15', 2, 33, '2016-01-04', 1, 55, NULL, NULL, NULL, 2, NULL, 'EMANA', 'null', 'MOHMAD SALIM', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(327, 18, '7/2016', '130/16', 2, 33, '2016-01-04', 2, 38, NULL, NULL, NULL, 1, NULL, 'GULJARI LAL', 'null', 'STATE & OTR', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(328, 18, '8/2016', '131/16', 2, 33, '2016-01-04', 2, 38, NULL, NULL, NULL, 1, NULL, 'CHANDRA KANTA', 'null', 'KAMAL SINGH', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(329, 18, '9/2016', '198/16', 2, 33, '2016-01-05', 2, 38, NULL, NULL, NULL, 1, NULL, 'MAKHAN SINGH', 'null', 'STATE & OTR', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(330, 18, '10/2016', '14669/15', 2, 33, '2016-01-08', 2, 38, NULL, NULL, NULL, 2, NULL, 'MS GN ENTERPRISES', 'null', 'SBBJ', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(331, 18, '11/2016', '7/15', 2, 33, '2016-01-06', 2, 47, NULL, NULL, NULL, 2, NULL, 'KARANI SINGH', 'null', 'ANSHUL SIROYA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(332, 18, '12/2016', '195/16', 2, 33, '2016-01-06', 2, 38, NULL, NULL, NULL, 2, NULL, 'ASHOK VARDHAN', 'null', 'STATE & OTR', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(333, 18, '13/2016', '264/16', 2, 33, '2016-01-06', 2, 38, NULL, NULL, NULL, 1, NULL, 'MOTI RAM', 'null', 'GANPAT RAM', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(334, 18, '14/2016', NULL, 1, 36, '2016-01-06', 2, NULL, NULL, NULL, NULL, 1, NULL, 'PANKAJ DARE', 'null', 'ANKITA DARE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(335, 18, '15/2016', '311/16', 2, 33, '2016-01-07', 2, 38, NULL, NULL, NULL, 1, NULL, 'MM TRAVELS AGENCY', 'null', 'STATE & OTR', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(336, 18, '16/2016', '1053/15', 2, 33, '2016-01-11', 2, 38, NULL, NULL, NULL, 2, NULL, 'DAMODAR LAL', 'null', 'DINKAR', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(337, 18, '17/2016', '31/16', 2, 33, '2016-01-11', 2, 40, NULL, NULL, NULL, 1, NULL, 'BHAGWAN DAS', 'null', 'VIJAY KUMAR', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(338, 18, '18/2016', NULL, 1, 36, '2016-01-13', NULL, NULL, NULL, NULL, NULL, 1, NULL, 'ANKITA DARE', 'null', 'PANKAJ DARE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(339, 18, '19/2016', '137/16', 2, 33, '2016-01-14', 1, 42, NULL, NULL, NULL, 1, NULL, 'JALA RAM', 'null', 'STATE & OTR', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(340, 18, '20/2016', '2461/15', 2, 33, '2016-01-15', 2, 41, NULL, NULL, NULL, 2, NULL, 'KHUSBEER SINGH', 'null', 'FIRM PALI CYCLE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(341, 18, '21/2016', '2462/15', 2, 33, '2016-01-15', 2, 41, NULL, NULL, NULL, 2, NULL, 'KHUSBEER SINGH', 'null', 'FIRM PALI CYCLE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(342, 18, '22/2016', '1831/15', 2, 33, '2016-01-15', 2, 41, NULL, NULL, NULL, 2, NULL, 'NARAYAN', 'null', 'SARASWATI', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(343, 18, '23/2016', '138/16', 2, 33, '2016-01-15', 1, 42, NULL, NULL, NULL, 1, NULL, 'RAGHUVEER SINGH', 'null', 'STATE & OTR', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(344, 18, '24/2016', '3672/15', 2, 33, '2016-01-19', 1, 42, NULL, NULL, NULL, 2, NULL, 'RAJESH', 'null', 'MURALIDHAR', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(345, 18, '25/2016', '920/16', 2, 33, '2016-01-22', 2, 38, NULL, NULL, NULL, 1, NULL, 'BANNI DEVI', 'null', 'AVVNL', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(346, 18, '26/2016', '9/16', 2, 33, '2016-01-25', 2, 51, NULL, NULL, NULL, 1, NULL, 'BALKISHAN', 'null', 'SHANTI DEVI', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(347, 18, '2/1970', NULL, NULL, NULL, '1970-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'null', NULL, 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(348, 18, '27/2016', '210/16', 2, 33, '2016-01-25', 2, 38, NULL, NULL, NULL, 2, NULL, 'PAWAN KUMAR', 'null', 'STATE & OTR', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(349, 18, '28/2016', '1092/16', 2, 33, '2016-01-27', 2, 38, NULL, NULL, NULL, 1, NULL, 'RAJENDRA KUMAR', 'null', 'SUSHIL KUMAR', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(350, 18, '29/2016', '230/16', 2, 33, '2016-01-27', 2, 42, NULL, NULL, NULL, 1, NULL, 'TANMAY PATHAK', 'null', 'STATE & OTR', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(351, 22, '25/2018', '12345/2911', 2, 41, '2018-11-11', 1, 65, NULL, NULL, NULL, 1, NULL, 'HARI', 'null', 'RAM', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(352, 18, '30/2016', '291/15', 2, 33, '2016-01-29', 2, 40, NULL, NULL, NULL, 2, NULL, 'KAYUM', 'null', 'MOHMAD IKRAR', 'null', NULL, NULL, NULL, NULL, 47, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(353, 18, '31/2016', '8895/15', 2, 33, '2016-01-29', 2, 38, NULL, NULL, NULL, 2, NULL, 'SULA VINEYRD', 'null', 'STATE & OTR', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(354, 18, '32/2016', '410/15', 2, 33, '2016-02-01', 2, 38, NULL, NULL, NULL, 2, NULL, 'RAMA GANGADHAR', 'null', 'STATE & OTR', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(355, 18, '33/2016', '134/16', 2, 33, '2016-02-01', 2, 38, NULL, NULL, NULL, 1, NULL, 'AASHKARAN', 'null', 'KAILASH', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(356, 18, '34/2016', '1419/16', 2, 33, '2016-02-02', 2, 38, NULL, NULL, NULL, 1, NULL, 'SUNIL KUMAR', 'null', 'SANGEETA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(357, 18, '35/2016', '5573/16', 2, 33, '2016-04-07', 2, 38, NULL, NULL, NULL, 1, NULL, 'AMRIT LAL', 'null', 'B O R', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(358, 18, '36/2016', '370/16', 2, 33, '2016-02-03', 2, 38, NULL, NULL, NULL, 2, NULL, 'UNITED SPIRIT LTD.', 'null', 'STATE & OTR', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(359, 18, '37/2016', '371/16', 2, 33, '2016-02-03', 2, 38, NULL, NULL, NULL, 2, NULL, 'UNITED SPIRIT LTD.', 'null', 'STATE & OTR', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(360, 18, '38/2016', '369/16', 2, 33, '2016-02-03', 2, 38, NULL, NULL, NULL, 2, NULL, 'UNITED SPIRIT LTD.', 'null', 'STATE & OTR', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(361, 18, '39/2016', '433/16', 2, 33, '2016-02-03', 2, 38, NULL, NULL, NULL, 2, NULL, 'PERNOD RICARD', 'null', 'STATE & OTR', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(362, 18, '40/2016', '432/16', 2, 33, '2016-02-03', 2, 38, NULL, NULL, NULL, 2, NULL, 'PERNOD RICARD', 'null', 'STATE & OTR', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(363, 18, '41/2016', '436/16', 2, 33, '2016-02-03', 2, 38, NULL, NULL, NULL, 2, NULL, 'RADICO KHAITAN', 'null', 'STATE & OTR', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(364, 18, '42/2016', '435/16', 2, 33, '2016-02-03', 2, 38, NULL, NULL, NULL, 2, NULL, 'RADICO KHAITAN', 'null', 'STATE & OTR', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(365, 18, '43/2016', '8646/14', 2, 33, '2016-02-03', 2, 38, NULL, NULL, NULL, 2, NULL, 'SHREE RAM SUDESH', 'null', 'STATE & OTR', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(366, 18, '44/2016', '22/16', 2, 33, '2016-02-04', 2, 71, NULL, NULL, NULL, 1, NULL, 'GANESH PARIKH', 'null', 'THE SEC. CJ', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(367, 18, '45/2016', '1495/16', 2, 33, '2016-02-06', 2, 38, NULL, NULL, NULL, 1, NULL, 'SIDHARTH MEHTA', 'null', 'STATE & OTR', 'null', NULL, NULL, NULL, NULL, 43, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(368, 18, '46/2016', '1496/16', 2, 33, '2016-02-06', 2, 38, NULL, NULL, NULL, 1, NULL, 'SIDHARTH MEHTA', 'null', 'STATE & OTR', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(369, 18, '47/2016', '1912/2016', 2, 33, '2016-02-06', 2, 38, NULL, NULL, NULL, 3, NULL, 'SHREE LAL', 'null', 'DURGA LAL', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL);
INSERT INTO `adv_case_reg` (`reg_id`, `user_admin_id`, `reg_file_no`, `reg_case_number`, `reg_court`, `reg_court_id`, `reg_date`, `reg_case_type_category`, `reg_case_type_id`, `reg_vcn_number`, `reg_case_subtype_id`, `reg_stage_id`, `reg_power`, `reg_power_respondent`, `reg_petitioner`, `reg_petitioner_extra`, `reg_respondent`, `reg_respondent_extra`, `reg_assigend_id`, `reg_act_id`, `reg_section_id`, `reg_reffeerd_by_id`, `reg_client_group_id`, `reg_client_subgroup_id`, `reg_fir_id`, `reg_fir_year`, `reg_police_thana`, `reg_remark`, `reg_other_field`, `reg_opp_council`, `reg_nxt_further_date`, `reg_file_uploaded`, `reg_extra_party`, `reg_document`, `reg_status`, `reg_disposal_date`) VALUES
(370, 18, '48/2016', '12032/13', 2, 33, '2016-01-28', 2, 38, NULL, NULL, NULL, 2, NULL, 'SHEE JAIN SWETAMBAR', 'null', 'ASSISTANT COM. DEP.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(371, 18, '49/2016', '1615/16', 2, 33, '2016-02-08', 2, 38, NULL, NULL, NULL, 1, NULL, 'DEEPAK KUMAR', 'null', 'PHOOLWATI DEVI', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(372, 18, '50/2016', '446/16', 2, 33, '2016-02-08', 2, 38, NULL, NULL, NULL, 2, NULL, 'PRADEEP BHANSAL', 'null', 'STA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(374, 18, '51/2016', '549/16', 2, 33, '2016-02-10', 2, 41, NULL, NULL, NULL, 1, NULL, 'CHAIN SUKH', 'null', 'GAUTAM CHAND', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(375, 18, '52/2016', '1737/16', 2, 33, '2016-02-11', 2, 38, NULL, NULL, NULL, 1, NULL, 'SACHCHANANAD', 'null', 'SURESH CHANDRA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(376, 18, '53/2016', '1617/16', 2, 33, '2016-02-11', 2, 38, NULL, NULL, NULL, 2, NULL, 'UNITED SPIRITS LTD', 'null', 'STATE OF RAJASTHAN', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(377, 18, '54/2016', '1618/16', 2, 33, '2016-02-11', 2, 38, NULL, NULL, NULL, 2, NULL, 'RADICO KHAITAN', 'null', 'STATE OF RAJASTHAN', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(378, 18, '55/2016', '1619/16', 2, 33, '2016-02-11', 2, 38, NULL, NULL, NULL, 2, NULL, 'RADICO KHAITAN', 'null', 'STATE OF RAJASTHAN', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(379, 18, '56/2016', '1620/16', 2, 33, '2016-02-11', 2, 38, NULL, NULL, NULL, 2, NULL, 'UNITED SPIRITS LTD', 'null', 'STATE OF RAJASTHAN', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(380, 18, '57/2016', '1621/16', 2, 33, '2016-02-11', 2, 38, NULL, NULL, NULL, 2, NULL, 'RADICO KHAITAN', 'null', 'STATE OF RAJASTHAN', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(381, 18, '58/2016', '1623/16', 2, 33, '2016-02-11', 2, 38, NULL, NULL, NULL, 2, NULL, 'UNITED SPIRITS LTD', 'null', 'STATE OF RAJASTHAN', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(382, 18, '59/2016', '1622/16', 2, 33, '2016-02-11', 2, 38, NULL, NULL, NULL, 2, NULL, 'UNITED SPIRITS LTD', 'null', 'STATE OF RAJASTHAN', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(383, 18, '60/2016', '1624/16', 2, 33, '2016-02-11', 2, 38, NULL, NULL, NULL, 2, NULL, 'RADICO KHAITAN', 'null', 'STATE OF RAJASTHAN', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(384, 18, '61/2016', '1632/16', 2, 33, '2016-02-11', 2, 38, NULL, NULL, NULL, 2, NULL, 'UNITED SPIRITS LTD', 'null', 'STATE OF RAJASTHAN', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(385, 18, '62/2016', '1633/16', 2, 33, '2016-02-11', 2, 38, NULL, NULL, NULL, 2, NULL, 'UNITED SPIRITS LTD', 'null', 'STATE OF RAJASTHAN', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(386, 18, '63/2016', '1636/16', 2, 33, '2016-02-11', 2, 38, NULL, NULL, NULL, 2, NULL, 'UNITED SPIRITS LTD', 'null', 'STATE OF RAJASTHAN', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(387, 18, '64/2016', '1761/16', 2, 33, '2016-02-15', 2, 38, NULL, NULL, NULL, 2, NULL, 'SUNIL KUMAR', 'null', 'R H C JODHPUR', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(388, 18, '65/2016', '1044/16', 2, 33, '2016-02-15', 2, 38, NULL, NULL, NULL, 2, NULL, 'ROOP NARAYAN', 'null', 'STATE OF RAJASTHAN', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(389, 18, '66/2016', '21/16', 2, 33, '2016-02-16', 2, 39, NULL, NULL, NULL, 2, NULL, 'SOHAN LAL', 'null', 'KAILASH CHANDRA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(390, 18, '67/2016', '1719/16', 2, 33, '2016-02-18', 2, 38, NULL, NULL, NULL, 2, NULL, 'CHANDER SINGH', 'null', 'STATE & OTHERS', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(391, 18, '68/2016', '1670/16', 2, 33, '2016-02-18', 2, 38, NULL, NULL, NULL, 2, NULL, 'RANJEET', 'null', 'STATE & OTHERS', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(392, 18, '69/2016', '2116/16', 2, 33, '2016-02-19', 2, 38, NULL, NULL, NULL, 1, NULL, 'RATAN SINGH', 'null', 'B O R', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(393, 18, '70/2016', '1815/16', 2, 33, '2016-02-19', 1, 44, NULL, NULL, NULL, 1, NULL, 'DHANWANTI', 'null', 'STATE & OTHERS', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(394, 18, '71/2016', '2089/16', 2, 33, '2016-02-19', 2, 38, NULL, NULL, NULL, 1, NULL, 'NARESH KUMAR', 'null', 'STATE & OTHERS', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(395, 18, '72/2016', NULL, 1, NULL, '2016-02-20', 2, 71, NULL, NULL, NULL, 3, NULL, 'STATE OF RAJASTHAN', 'null', 'GENERAL PUBLIC', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(396, 18, '73/2016', NULL, 1, NULL, '2016-01-20', 2, 71, NULL, NULL, NULL, 1, NULL, 'SBBJ', 'null', 'PRAKASH  HARIDASANI', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(397, 18, '74/2016', '1634/16', 2, 33, '2016-02-22', 2, 38, NULL, NULL, NULL, 2, NULL, 'UNITED SPIRITS LTD', 'null', 'STATE OF RAJASTHAN', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(398, 18, '75/2016', '1635/16', 2, 33, '2016-02-22', 2, 38, NULL, NULL, NULL, 2, NULL, 'RADICO KHAITAN', 'null', 'STATE OF RAJASTHAN', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(399, 18, '76/2016', '196/16', 2, 33, '2016-02-22', 1, 44, NULL, NULL, NULL, 1, NULL, 'PRAHALAD RAM', 'null', 'STATE & OTHERS', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(400, 18, '77/2016', '2277/16', 2, 33, '2016-02-23', 2, 38, NULL, NULL, NULL, 1, NULL, 'AVANI SURGICAL', 'null', 'RRVPNL', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(401, 18, '78/2016', '55/18', 2, 33, '2016-02-23', 2, 39, NULL, NULL, NULL, 1, NULL, 'KANA RAM', 'null', 'SATYA PRAKASH', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(402, 18, '3/1970', NULL, NULL, NULL, '1970-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'null', NULL, 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(403, 18, '79/2016', '2243/16', 2, 33, '2016-02-25', 2, 38, NULL, NULL, NULL, 2, NULL, 'RADICO KHAITAN', 'null', 'STATE OF RAJASTHAN', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(404, 18, '80/2016', '2241/16', 2, 33, '2016-02-25', 2, 38, NULL, NULL, NULL, 2, NULL, 'UNITED SPIRITS LTD', 'null', 'STATE OF RAJASTHAN', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(405, 18, '81/2016', '2242/16', 2, 33, '2016-02-25', 2, 38, NULL, NULL, 23, 2, NULL, 'UNITED SPIRITS LTD', 'null', 'STATE OF RAJASTHAN', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(406, 18, '82/2016', '2244/16', 2, 33, '2016-02-25', 2, 38, NULL, NULL, NULL, 2, NULL, 'RADICO KHAITAN', 'null', 'STATE OF RAJASTHAN', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(407, 18, '83/2016', '2248/16', 2, 33, '2016-02-25', 2, 38, NULL, NULL, NULL, 2, NULL, 'UNITED SPIRITS LTD', 'null', 'STATE OF RAJASTHAN', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(408, 18, '84/2016', '2247/16', 2, 33, '2016-02-25', 2, 38, NULL, NULL, NULL, 2, NULL, 'UNITED SPIRITS LTD', 'null', 'STATE OF RAJASTHAN', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(410, 18, '85/2016', '1531/16', 2, 33, '2016-02-25', 2, 38, NULL, NULL, NULL, 2, NULL, 'JAI KISHAN', 'null', 'STATE & OTHERS', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(411, 18, '86/2016', '335/16', 2, 33, '2016-02-25', 2, 38, NULL, NULL, NULL, 2, NULL, 'KAMLA DAMOR', 'null', 'SBBJ', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(412, 18, '87/2016', '11577/15', 2, 33, '2016-02-25', 2, 38, NULL, NULL, NULL, 1, NULL, 'BANSHI LAL', 'null', 'STATE & OTHERS', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(413, 18, '88/2016', '11893/15', 2, 33, '2016-02-25', 2, 38, NULL, NULL, NULL, 1, NULL, 'MATHURA LAL', 'null', 'STATE & OTHERS', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(414, 18, '89/2016', '12015/15', 2, 33, '2016-02-25', 2, 38, NULL, NULL, NULL, 1, NULL, 'MOHAN LAL', 'null', 'STATE & OTHERS', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(415, 18, '90/2016', '11894/15', 2, 33, '2016-02-25', 2, 38, NULL, NULL, NULL, 1, NULL, 'NARAYAN', 'null', 'STATE & OTHERS', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(416, 18, '1/2015', '15/15', 2, 33, '2015-01-15', 1, 45, NULL, NULL, 23, 1, NULL, 'MAHIPAL', 'null', 'STATE & ORS.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(417, 18, '2/2015', '88/15', 2, 33, '2015-01-05', 2, 38, NULL, NULL, 23, 1, NULL, 'HARISH CHANDRA GEHLOT', 'null', 'STATE & ORS.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(418, 18, '3/2015', '173/15', 2, 33, '2015-01-05', 2, 38, NULL, NULL, 23, 3, NULL, 'GANESH & ORS', 'null', 'ADC TRUST', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(419, 18, '4/2015', '122/15', 2, 33, '2015-01-05', 2, 38, NULL, NULL, 23, 1, NULL, 'SMT. GHISHI', 'null', 'BOR & ORS.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(420, 18, '5/2015', '124/15', 2, 33, '2015-01-05', 2, 38, NULL, NULL, 23, 1, NULL, 'M/S MITHILESH FABRICS', 'null', 'WELMARK SYNTHETIC', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(421, 18, '6/2015', '6915/14', 2, 33, '2015-01-05', 2, 38, NULL, NULL, 23, 1, NULL, 'LRS OF SOMREES', 'null', 'STATE & ORS.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(422, 18, '7/2015', '1426/14', 2, 33, '2015-01-05', 2, 46, NULL, NULL, 23, 2, NULL, 'KAILASH BHATI', 'null', 'STATE & ORS.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(423, 18, '8/2015', '7/13', 2, 33, '2015-01-06', 2, 73, NULL, NULL, 23, 2, NULL, 'M/S SWASTIK COAL', 'null', 'CHUMAND SYNPRO CESS', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(424, 18, '9/2015', '/15', 1, 36, '2015-01-05', 1, 42, NULL, NULL, 23, 1, NULL, 'VINITA DAIYA', 'null', 'RAKSHIT DAIYA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(425, 18, '10/2015', '8018/11', 2, 33, '2015-01-06', 2, 38, NULL, NULL, 23, 2, NULL, 'SATPAL & ORS.', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(426, 18, '11/2015', '4931/12', 2, 33, '2015-01-06', 2, 38, NULL, NULL, 23, 2, NULL, 'ANDA RAM', 'null', 'STATE & ORS.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(427, 18, '12/2015', '7544/14', 2, 33, '2015-01-07', 2, 38, NULL, NULL, 23, 2, NULL, 'RAMDEV SUTHAR', 'null', 'STATE BANK OF INDIA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(428, 18, '13/2015', '218/15', 2, 33, '2015-01-07', 2, 38, NULL, NULL, 23, 1, NULL, 'SANTOSH KUMAR SUMAN', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(429, 18, '14/2015', '236/15', 2, 33, '2015-01-07', 2, 38, NULL, NULL, 23, 1, NULL, 'M/S SANKHLA CONSTRUCTION', 'null', 'RIICO & ANR.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(430, 18, '15/2015', '/15 SAW 882/12', 2, 33, '2015-01-07', 2, 74, NULL, NULL, 23, 1, NULL, 'BHIKAM CHAND', 'null', 'BOR & ORS.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(431, 18, '16/2015', '/15 SAW 929/12', 2, 33, '2015-01-07', 2, 74, NULL, NULL, 23, 1, NULL, 'BHIKAM CHAND', 'null', 'BOR & ORS.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(432, 18, '17/2015', '/15 SAW 885/12', 2, 33, '2015-01-07', 2, 74, NULL, NULL, 23, 1, NULL, 'BHIKAM CHAND', 'null', 'BOR & ORS.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(433, 18, '18/2015', '/15 SAW 912/12', 2, 33, '2015-01-07', 2, 74, NULL, NULL, 23, 1, NULL, 'BHIKAM CHAND', 'null', 'BOR & ORS.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(434, 18, '19/2015', '2019/1', 2, 33, '2015-01-08', 2, 38, NULL, NULL, 23, 2, '4', 'NEERAJ KACHHAWA', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(435, 18, '20/2015', '321/15', 2, 33, '2015-01-08', 2, 38, NULL, NULL, 23, 1, NULL, 'S.N. SHARMA', 'null', 'D.C. UDAIPUR', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(436, 18, '21/2015', '88/15', 2, 33, '2015-01-08', 2, 41, NULL, NULL, 23, 1, NULL, 'RAJEEV SINGH', 'null', 'AMITA & ORS.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(437, 18, '22/2015', '604/15', 2, 33, '2015-01-09', 2, 38, NULL, NULL, 23, 1, NULL, 'BHANWAR SINGH', 'null', 'RAJASTHAN STATE HUMAN RIGHTS', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(438, 18, '23/2015', NULL, 2, 33, '2015-01-12', 2, 38, NULL, NULL, 23, 3, NULL, 'BABULAL JAIN', 'null', 'PIYUSH DOSHI', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(439, 18, '24/2015', NULL, 2, 33, '2015-01-12', 2, 39, NULL, NULL, 23, 2, '1,2', 'KAILASH', 'null', 'DHINGA & ORS.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(440, 18, '25/2015', '734/14', 2, 33, '2015-01-13', 2, 50, NULL, NULL, 23, 2, NULL, 'PRAKASH HARIDASANI', 'null', 'JYOTI', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(441, 18, '26/2015', '676/15', 2, 33, '2015-01-14', 2, 38, NULL, NULL, 23, 1, NULL, 'DIGRAJ SINGH SHARMA', 'null', 'LEA. CJ & ORS.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(442, 18, '27/2015', '789/15', 2, 33, '2015-01-15', 2, 38, NULL, NULL, 23, 1, NULL, 'LAXMI KANWAR', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(443, 18, '28/2015', '8/15', 2, 33, '2015-01-16', 2, 52, NULL, NULL, 23, 1, NULL, 'MOTI', 'null', 'RAMNARAYAN & ORS.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(444, 18, '29/2015', '112/15', 2, 33, '2015-01-19', 2, 41, NULL, NULL, 23, 3, NULL, 'INDUSIND BANK', 'null', 'HUKAM SINGH', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(445, 18, '30/2015', '144/15', 2, 33, '2015-01-19', 2, 41, NULL, NULL, 23, 1, NULL, 'DARSHANA GUPTA', 'null', 'NONE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(446, 18, '31/2015', '902/15', 2, 33, '2015-01-19', 2, 38, NULL, NULL, 23, 1, NULL, 'SATYADEV PARASAR', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(447, 18, '32/2015', '7857/14', 2, 33, '2015-01-21', 2, 38, NULL, NULL, 23, 2, NULL, 'SUMER SINGH', 'null', 'STATE BANK OF INDIA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(448, 18, '33/2015', '1180/14', 2, 33, '2015-01-21', 1, 44, NULL, NULL, 24, 1, NULL, 'PRABHU LAL', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(449, 18, '34/2015', '1012/15', 2, 33, '2015-01-24', 2, 38, NULL, NULL, 23, 1, NULL, 'SURESH CHANDRA', 'null', 'M.C. KAPASAN', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(450, 18, '35/2015', '1051/15', 2, 33, '2015-01-21', 2, 38, NULL, NULL, 23, 1, NULL, 'CHANDRA MAL JAIN', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(451, 18, '36/2015', '1093/15', 2, 33, '2015-01-21', 2, 38, NULL, NULL, 23, 1, NULL, 'PEERU LAL', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(452, 18, '37/2015', '1081/15', 2, 33, '2015-01-21', 2, 38, NULL, NULL, 23, 1, NULL, 'PANKAJ GANG', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(453, 18, '38/2015', '1091/15', 2, 33, '2015-01-21', 2, 38, NULL, NULL, 23, 1, NULL, 'SMT. RUPI BAI', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(454, 18, '39/2015', '1092/15', 2, 33, '2015-01-21', 2, 38, NULL, NULL, 23, 1, NULL, 'BHERU LAL JAT', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(455, 18, '40/2015', '1040/15', 2, 33, '2015-01-21', 2, 38, NULL, NULL, NULL, 1, NULL, 'SMT. SUMITRA', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(456, 18, '41/2015', '4974/07', 2, 33, '2015-01-23', 2, 38, NULL, NULL, 23, 1, NULL, 'NARIN BHANSALI', 'null', 'BOR & ORS.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(457, 18, '42/2015', '1071/15', 2, 33, '2015-01-23', 2, 38, NULL, NULL, 23, 1, NULL, 'M/S CHAMUNDA TAP NIRODHAK', 'null', 'BOB & ORS.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(458, 18, '43/2015', '7793/14', 2, 33, '2015-01-27', 2, 38, NULL, NULL, 23, 2, NULL, 'NAEEM AKHTAR', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(459, 18, '44/2015', '1505/14', 2, 33, '2015-01-27', 2, 41, NULL, NULL, 23, 2, '2', 'RAJESH SHARMA & ANR', 'null', 'BABU RAM', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(460, 18, '45/2015', '/14', 1, 36, '2015-01-28', 1, 42, NULL, NULL, NULL, 1, NULL, 'MANJU JAIN', 'null', 'PREMCHAND & ORS.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(461, 18, '46/2015', '20/15', 2, 33, '2015-01-28', 2, 71, NULL, NULL, NULL, 1, NULL, 'NARKER BULSTATE', 'null', 'SIDHAR ENERGY', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(462, 18, '47/2015', '19/15', 2, 33, '2015-01-28', 2, 71, NULL, NULL, 23, 1, NULL, 'ANU MEHTA', 'null', 'SIDHARTH RAJA ENTERPRISES', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(463, 18, '48/2015', '1266/14', 2, 33, '2015-02-02', 1, 45, NULL, NULL, 23, 2, NULL, 'ARDESH KUMAR', 'null', 'SMT. ALKA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(464, 18, '49/2015', '111/15', 2, 33, '2015-02-03', 2, 46, NULL, NULL, 23, 1, NULL, 'JITENDRA KUMAR', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(465, 18, '50/2015', '6980/14', 2, 33, '2015-02-04', 2, 38, NULL, NULL, 23, 2, '1', 'PREM MARBLES PVT LTD.', 'null', 'STATE BANK OF INDIA', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(466, 18, '51/2015', '21/15', 2, 33, '2015-01-28', 2, 71, NULL, NULL, 23, 1, NULL, 'NARKAR BUILDHOME', 'null', 'SIDHARTH RAJA ENTERPRISES', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(467, 18, '52/2015', '357/07', 2, 33, '2015-02-04', 2, 40, NULL, NULL, 23, 1, NULL, 'DEVI LAL', 'null', 'SMT. KANTA & ORS.', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(468, 18, '53/2015', '1414/15', 2, 33, '2015-02-06', 2, 38, NULL, NULL, 23, 1, NULL, 'MRIGENDRA SINGH BAHETI', 'null', 'M.C. JODHPUR', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(469, 18, '54/2015', '137/15', 2, 33, '2015-02-06', 1, 43, NULL, NULL, 23, 1, NULL, 'AYUB KHAN', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(470, 18, '55/2015', '314/09', 2, 33, '2015-02-06', 2, 39, NULL, NULL, 23, 1, NULL, 'SMT. SHYAMA', 'null', 'PRITHVI SINGH', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(471, 18, '56/2015', '/15', 2, 33, '2015-02-10', 2, 38, NULL, NULL, 23, 3, NULL, 'KISHAN SINGH', 'null', 'BHANWAR SINGH', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(472, 18, '57/2015', '29/15', 2, 33, '2015-02-12', 2, 71, NULL, NULL, 23, 1, NULL, 'ASHWIN GARG', 'null', 'DR. RADHEYSHYAM', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(473, 18, '58/2015', '1408/15', 2, 33, '2015-02-12', 2, 38, NULL, NULL, 23, 2, NULL, 'ARJAN BHAI', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(474, 18, '59/2015', '297/15', 2, 33, '2015-02-12', 2, 50, NULL, NULL, 23, 1, NULL, 'SMT. SUSHILA DEVI', 'null', 'POOJA MOOLCHANDANI', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(475, 18, '60/2015', '149/15', 2, 33, '2015-02-12', 2, 38, NULL, NULL, 23, 2, NULL, 'KAMLA DEVI', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(476, 18, '61/2015', '/15', 2, 33, '2015-02-13', 2, 38, NULL, NULL, NULL, 3, NULL, 'BHAWANI SHANKAR GUPTA', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(477, 22, '26/2018', '123/2017', 2, 41, '2018-09-27', 1, 64, NULL, 40, 35, 1, NULL, 'Kotilya', 'null', 'Chirag', 'null', NULL, NULL, NULL, NULL, 59, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(478, 22, '27/2018', '98248/2018', 2, 41, '2018-09-28', 1, 65, NULL, 42, 35, 1, NULL, 'Guru Charan', 'null', 'Sulekha', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(479, 22, '28/2018', '248/2018', 1, 46, '2018-10-02', 1, 64, '245', NULL, 31, 1, NULL, 'Dhanraj', 'null', 'Mohan Ram', 'null', 24, 37, '35', 30, 59, NULL, '2001', 2018, 'Jodhpur', 'Your Case has been filed on 02.10.18', 'Fees Not Received', 'CS Vyas', '2018-10-05', NULL, 'null', NULL, 1, NULL),
(480, 23, '1/2018', NULL, 2, NULL, '2018-11-10', NULL, NULL, NULL, NULL, NULL, 1, NULL, 'Suraj', 'null', 'Sachin', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(481, 23, '2/2018', NULL, 1, NULL, '2018-11-03', NULL, NULL, NULL, NULL, NULL, 1, NULL, 'Pramod', 'null', 'Pratyush', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(482, 4, '14/2018', NULL, 2, NULL, '2018-11-30', NULL, NULL, NULL, NULL, NULL, 1, NULL, 'Ad', 'null', 'Soni', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-09', NULL, 'null', NULL, 1, NULL),
(483, 4, '15/2018', NULL, 2, NULL, '2018-11-30', NULL, NULL, NULL, NULL, NULL, 1, NULL, '1', 'null', '2', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-30', NULL, 'null', NULL, 1, NULL),
(484, 4, '16/2018', NULL, 2, NULL, '2018-11-22', NULL, NULL, NULL, NULL, NULL, 1, NULL, 'Sandeep', 'null', 'Pramod', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(485, 4, '17/2018', NULL, 2, NULL, '2018-11-30', NULL, NULL, NULL, NULL, NULL, 1, NULL, 'Test', 'null', 'Test', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-30', NULL, 'null', NULL, 1, NULL),
(486, 4, '18/2018', NULL, 1, 14, '2018-11-30', NULL, NULL, '6eq67', NULL, NULL, 1, NULL, 'R', 'null', 'U', 'null', 11, 9, '9', 3, 29, NULL, NULL, NULL, NULL, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', NULL, '2018-12-01', NULL, 'null', NULL, 1, NULL),
(487, 4, '19/2018', '245/2018', 1, 14, '2018-11-29', 1, 18, '123/2018', NULL, NULL, 1, NULL, 'Pramod Godishala', 'null', 'Sandeep Bhati', 'null', 7, 41, '40', 9, 65, NULL, '2018', 2018, 'Jodhpur police station', 'All time', 'All time', 'Pradeep Rai', '2018-11-30', NULL, 'null', NULL, 1, NULL),
(488, 4, '20/2018', '566565', 1, NULL, '2018-12-02', 2, 15, '13212', NULL, 12, 1, NULL, 'Sweta', '{\"1\":{\"petitioner_name\":\"Tr\",\"petitioner_father_name\":\"G\",\"petition_address\":\"12\\r\\n12\\r\\n13\\r\\nsad\\r\\nfsd\\r\\ndfs\\r\\nfds\\r\\nfds\\r\\nfds\"}}', 'Sweta', '{\"1\":{\"respondent_name\":\"qw\",\"respondent_father_name\":\"te\",\"respondent_address\":\"12\\r\\n12\\r\\n13\\r\\nsad\\r\\nfsd\\r\\ndfs\\r\\nfds\\r\\nfds\\r\\nfds\"}}', 7, 33, '28,29', 3, 23, 48, '23', 1964, '342', 'tra\r\nsad\r\nsda\r\nA\r\nSAD\r\nsda\r\nsa', 'A\r\nSAD\r\nsda\r\nsa', '43342', '2018-12-02', NULL, 'null', NULL, 1, NULL),
(489, 4, '21/2018', '467/2018', 2, 12, '2018-12-01', 2, 15, NULL, 12, 11, 1, NULL, 'Shailendra Bhandari', 'null', 'State', 'null', 11, 42, '41,42', 14, 66, NULL, NULL, NULL, NULL, 'Fee Not Received till today', 'Case Not Pass\r\nDefect\r\n-Type Copy not Filed\r\n-Annex not complete', 'Bihar Lal', '2018-12-06', NULL, 'null', NULL, 1, NULL),
(490, 29, '1/2018', 'M-123', 2, 50, '2018-12-15', 1, 75, NULL, 52, 36, 1, NULL, 'Sumit', 'null', 'Rupam', 'null', 27, 43, '43', 33, 67, 89, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(491, 29, '2/2018', '123', 2, 51, '2018-12-16', 1, 75, NULL, 52, 36, 1, NULL, 'sandeep', 'null', 'mohit', 'null', 27, 43, '43', 33, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(492, 4, '22/2018', '78445441', 2, 12, '2018-12-11', 2, 15, NULL, 12, 11, 2, '1012', 'Sweta', 'null', 'Ter', 'null', 7, 22, '15', 9, 18, 46, '12231', 1964, 'Thana', NULL, NULL, NULL, '2018-12-04', NULL, 'null', NULL, 1, NULL),
(493, 30, '1/2018', '123', 1, 53, '2018-12-21', 1, 80, '123', NULL, 38, 2, '123', 'sumit', 'null', 'rupam', 'null', 28, 44, '46', 36, 68, 90, NULL, 2018, 'Umaid', NULL, NULL, NULL, '2018-12-19', NULL, 'null', NULL, 1, NULL),
(494, 30, '2/2018', '456', 2, 52, '2018-12-21', 2, 81, NULL, 58, 39, 3, NULL, 'sumit', 'null', 'rupam', 'null', 29, 45, '48', 36, 69, NULL, '632', 2018, NULL, 'test', 'test', NULL, '2018-12-19', NULL, 'null', NULL, 1, NULL),
(495, 31, '1/2017', '1/2017', 2, 56, '2017-01-01', 2, 88, NULL, 61, 42, 1, NULL, 'Ghewar Chand', '{\"1\":{\"petitioner_name\":\"Sampat Lal\",\"petitioner_father_name\":\"Bhanwar Lal\",\"petition_address\":\"1-3 Transport Nagar, Basni Second Phase\\r\\nJodhpur\"},\"2\":{\"petitioner_name\":\"Bharat Shahu\",\"petitioner_father_name\":\"Ramawtar Shahu\",\"petition_address\":\"Village & Post Nawa\\r\\nTehsil: Bhada\\r\\nDist Hanumangarh\"}}', 'State & Ors', 'null', 30, 46, '50,51', 37, 70, NULL, NULL, NULL, NULL, 'This is to inform you that all the DB Special Appeals (Writs) in the Business Correspondents\' matter before Hon’ble Rajasthan High Court, Jodhpur have been allowed.', 'In light of the above, you are requested to process the payment of the due legal fee in the captioned matters.', 'Khet Singh Rajpurohit', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(496, 31, '1/2016', '325/2018', 1, 58, '2016-01-01', 1, 85, '111/2018', NULL, 42, 1, '1,3,5', 'Shyam Sunder & Ors', 'null', 'Laxmi Narayan', '{\"1\":{\"respondent_name\":\"Neha Gupta\",\"respondent_father_name\":\"GL Gupta\",\"respondent_address\":\"114, Agroha, PWD Colony\\r\\nJodhpur\"},\"2\":{\"respondent_name\":\"LIC\",\"respondent_father_name\":null,\"respondent_address\":\"LIC- Manager, Branch First, Infront of Railway Station,\\r\\nJodhpur\"},\"3\":{\"respondent_name\":\"LIC\",\"respondent_father_name\":null,\"respondent_address\":\"LIC- Branch Manager, Yokshem Jivan Bhima Marg,\\r\\nMumbai\"}}', 30, 46, '51', 37, 70, NULL, '134', 2014, 'Jodhpur', '1.	That in reply to Para No. 4 of the writ petition respondent no. 3 has alleged that although notification under Section 4 was issued but the possession of 11 Bighas of land of petitioner society has not been taken nor it has been used by the Govt. in any manner.', '2.	That in reply to the writ petition respondent no. 3 has mainly contended that 11 Bighas of land of Petitioner’s Society was not required therefore, it has not been used for the construction of treatment plant and respondent department has taken action for cancelling of acquisition', 'Khet Singh Rajpurohit', '2018-12-06', NULL, 'null', NULL, 1, NULL),
(497, 31, '1/2018', '681/2018', 2, 57, '2018-01-01', 2, 88, NULL, 61, 43, 2, '1,3,5', 'Ram Gopal', 'null', 'State', 'null', 30, 46, '52', 37, 70, NULL, NULL, NULL, NULL, 'Govt. cannot withdraw from the acquisition and proceedings commenced for withdrawing from acquisition of the lands in relation to Khasra No. 204/71', '3.	That respondent no. 3 has produce a letter dated 20.06.2014 written by the Commissioner Municipal Council, Banswara to the Executive Engineer, PHED requesting to not to surrender the disputed lands and the', 'Khet Singh Rajpurohit', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(498, 31, '2/2017', '879/2017', 2, 56, '2017-01-01', 2, 88, NULL, 61, 44, 1, NULL, 'HZL', 'null', 'State & Ors', 'null', 30, 46, '51', 37, 70, NULL, NULL, NULL, NULL, 'This is to inform you that all the DB Special Appeals (Writs) in the Business Correspondents\' matter before Hon’ble Rajasthan High Court, Jodhpur have been allowed.', 'In light of the above, you are requested to process the payment of the due legal fee in the captioned matters.', 'Khet Singh Rajpurohit', '2018-12-01', NULL, 'null', NULL, 1, NULL),
(499, 31, '1/2001', '12345/2018', 2, 56, '2001-01-01', 1, 86, NULL, NULL, 42, 1, NULL, 'JODHPUR', 'null', 'BOBAY', 'null', 30, 46, NULL, 39, 72, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-01', NULL, 'null', NULL, 1, NULL),
(500, 31, '1/1970', NULL, NULL, NULL, '1970-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'null', NULL, 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(501, 31, '2/2018', '111/2018', 2, 56, '2018-01-01', 1, 85, NULL, 62, 42, 1, NULL, 'RAM', 'null', 'MOHAN', 'null', NULL, NULL, NULL, NULL, 70, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-01', NULL, 'null', NULL, 1, NULL),
(502, 31, '3/2018', '111', 2, 56, '2018-12-01', 1, 86, NULL, NULL, 42, 1, NULL, 'a', 'null', 'b', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(503, 30, '3/2018', '11/2017', 2, 52, '2018-12-01', 1, 80, NULL, 56, 38, 1, NULL, 'sumit', 'null', 'rupam', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-25', NULL, 'null', NULL, 1, NULL),
(504, 30, '4/2018', '12-1222', 2, 52, '2018-12-05', 1, 80, NULL, NULL, 39, 1, NULL, 'sumit', 'null', 'rupam', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(505, 30, '5/2018', '12/4555', 2, 54, '2018-12-01', 1, 82, NULL, NULL, 39, 1, NULL, 'sumit', 'null', 'rupam', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(506, 31, '4/2018', '111/2018', 2, 56, '2018-07-01', 1, 85, NULL, NULL, 42, 1, NULL, 'XYZ', 'null', 'ABC', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(507, 30, '1/2019', '1/2019', 2, 52, '2019-01-01', 1, 80, NULL, 56, 38, 1, NULL, 'sumit', 'null', 'rupam', 'null', 28, NULL, NULL, 35, 68, 90, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(508, 30, '6/2018', '12/2220', 1, 53, '2018-12-31', NULL, NULL, '12', NULL, 38, 1, NULL, 'tes', 'null', 'test', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-31', NULL, 'null', NULL, 1, NULL),
(509, 31, '5/2018', '987/2018', 2, 56, '2018-12-01', 1, 85, NULL, 62, 42, 1, NULL, 'SHANTI LAL KARNAWAT', 'null', 'STATE & ORS', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(510, 33, '1/2019', '202/19', 2, 63, '2019-01-01', 2, 91, NULL, NULL, 45, 1, NULL, 'Hindustan Zinc Ltd', 'null', 'Industrial Tribunal (Kedar Das)', 'null', 34, NULL, NULL, NULL, 80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(511, 33, '2/2019', '206/19', 2, 63, '2019-01-01', 2, 91, NULL, NULL, 45, 1, NULL, 'Hindustan Zinc Ltd', 'null', 'Industrial Tribunal(Bagh Singh)', 'null', 34, NULL, NULL, NULL, 80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(512, 33, '3/2019', '2/19', 2, 63, '2019-01-01', 2, 95, NULL, NULL, 45, 1, NULL, 'SBI', 'null', 'Rajendra', 'null', 33, NULL, NULL, NULL, 81, 95, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(513, 33, '4/2019', '3/19', 2, 63, '2019-01-01', 2, 95, NULL, NULL, 45, 1, NULL, 'SBI', 'null', 'Aditya Kumawat', 'null', 33, NULL, NULL, NULL, 81, 95, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(514, 33, '5/2019', '4/19', 2, 63, '2019-01-01', 2, 95, NULL, NULL, 45, 1, NULL, 'SBI', 'null', 'Ramesh Kumar', 'null', 33, NULL, NULL, NULL, 81, 95, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(515, 33, '6/2019', '154/19', 2, 63, '2019-01-02', 2, 91, NULL, NULL, 45, 1, NULL, 'Mazhar Mohammad', 'null', 'State & Ors', 'null', 33, NULL, NULL, 40, 82, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(516, 33, '7/2019', NULL, 2, 63, '2019-01-02', 2, 96, NULL, NULL, 45, 1, NULL, 'Lrs of Roopa Ram', 'null', 'Lrs of Badar Mal', 'null', 33, NULL, NULL, NULL, 85, NULL, NULL, NULL, NULL, 'Case Not Pass', NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(517, 33, '8/2019', '36/18', 2, 63, '2019-01-02', 2, 93, NULL, NULL, 46, 2, NULL, 'M/s Raw Edge', 'null', 'Hindustan Zinc Ltd', 'null', 34, NULL, NULL, NULL, 80, NULL, NULL, NULL, NULL, 'Name Not Show', NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(518, 33, '9/2019', 'Caveat', 2, 63, '2019-01-02', 2, 91, NULL, NULL, NULL, 3, NULL, 'Labour Union', 'null', 'Hindustan Zinc Ltd (Sachiv Debari)', 'null', 34, NULL, NULL, NULL, 80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(519, 33, '10/2019', NULL, 2, 63, '2019-01-03', 2, 92, NULL, NULL, 45, 1, NULL, 'Ganga Ram Dangi & Ors', 'null', 'State & Ors', 'null', 33, NULL, NULL, NULL, 83, NULL, NULL, NULL, NULL, 'Defect', NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(520, 33, '11/2019', '18157/18', 2, 63, '2019-01-03', 2, 91, NULL, NULL, 46, 2, '3', 'Udaipur Entertainment Pvt Ltd', 'null', 'State (CBI)', 'null', 36, NULL, NULL, NULL, 84, 96, NULL, NULL, NULL, 'Name Not Show', NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(521, 33, '12/2019', '232/18', 2, 63, '2019-01-04', 2, 96, NULL, NULL, 46, 2, '1', 'Lrs Durga Dutt', 'null', 'Girdhari Lal', 'null', 35, NULL, NULL, 41, 86, NULL, NULL, NULL, NULL, 'Name Not Show', NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(522, 33, '13/2019', '18826/18', 2, 63, '2019-01-05', 2, 91, NULL, NULL, 46, 2, '3', 'Ranjan Tak', 'null', 'UOI(ONGC)', 'null', 33, NULL, NULL, NULL, 89, 97, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(523, 33, '14/2019', '1406/18', 2, 63, '2019-01-08', 2, 97, NULL, NULL, 46, 2, '1', 'Prateek', 'null', 'Dinesh Kumar & Ors', 'null', 35, NULL, NULL, 42, 87, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(524, 33, '15/2019', NULL, 2, 63, '2019-01-08', 2, 91, NULL, NULL, 45, 1, NULL, 'Khaitan Business Pvt Ltd', 'null', 'State & Ors', 'null', 33, NULL, NULL, NULL, 88, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(525, 33, '16/2019', NULL, 2, 63, '2019-01-10', 2, 97, NULL, NULL, 45, 1, NULL, 'Tulsi Amrit Niketan', 'null', 'UII', 'null', 35, NULL, NULL, 44, 90, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(526, 33, '17/2019', NULL, 2, 63, '2019-01-10', 2, 97, NULL, NULL, 45, 1, NULL, 'Tusli Amrit Niketan', 'null', 'UII', 'null', 35, NULL, NULL, 44, 90, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(527, 33, '18/2019', '4605/18', 2, 63, '2019-01-10', 2, 91, NULL, NULL, NULL, 1, NULL, 'Bhagwat Singh', 'null', 'SBI & Ors', 'null', 33, NULL, NULL, NULL, 81, 95, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(528, 33, '19/2019', NULL, 2, 63, '2019-01-11', 1, 98, NULL, NULL, NULL, 1, NULL, 'Mahaveer', 'null', 'State', 'null', 36, NULL, NULL, 43, 91, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(529, 33, '20/2019', NULL, 2, 63, '2019-01-11', 2, 92, NULL, NULL, NULL, 1, NULL, 'SBI', 'null', 'Rajendra Kumar', 'null', 33, NULL, NULL, NULL, 81, 95, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(530, 33, '21/2019', '1611/18', 2, 63, '2019-01-11', 2, 99, NULL, NULL, 46, 1, NULL, 'Gora Ram Ashiya', 'null', 'Rameshwar Vyas', 'null', 33, NULL, NULL, NULL, 92, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(531, 33, '22/2019', '12823/18', 2, 63, '2019-01-11', 2, 91, NULL, NULL, 46, 2, NULL, 'Happy Choubisa', 'null', 'State(Excise)', 'null', 33, NULL, NULL, NULL, 94, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(532, 33, '23/2019', '12805/18', 2, 63, '2019-01-11', 2, 91, NULL, NULL, 46, 2, NULL, 'Rakesh Kumar', 'null', 'State(Excise)', 'null', 33, NULL, NULL, NULL, 94, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(533, 33, '24/2019', '12811/18', 2, 63, '2019-01-11', 2, 91, NULL, NULL, 46, 2, NULL, 'Ravindra Kumar', 'null', 'State(Excise)', 'null', 33, NULL, NULL, NULL, 94, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(534, 33, '25/2019', '12820/18', 2, 63, '2019-01-11', 2, 91, NULL, NULL, 46, 2, NULL, 'Amba Lal', 'null', 'State(Excise)', 'null', 33, NULL, NULL, NULL, 94, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(535, 33, '26/2019', '12846/18', 2, 63, '2019-01-11', 2, 91, NULL, NULL, 46, 2, NULL, 'Usha Sunil', 'null', 'State(Excise)', 'null', 33, NULL, NULL, NULL, 94, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(536, 33, '27/2019', '12812/18', 2, 63, '2019-01-11', 2, 91, NULL, NULL, 46, 2, NULL, 'Mahesh Vaishnav', 'null', 'State(Excise)', 'null', 33, NULL, NULL, NULL, 94, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(537, 33, '28/2019', NULL, 2, 63, '2019-01-14', 1, 100, NULL, NULL, 45, 1, NULL, 'Smt Lalita', 'null', 'State & Ors', 'null', 36, NULL, NULL, 43, 93, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(538, 4, '1/2019', NULL, 1, NULL, '2019-02-15', 1, 19, NULL, NULL, NULL, 2, NULL, 'sad', '{\"1\":{\"petitioner_name\":\"asd\",\"petitioner_father_name\":\"sadas\",\"petition_address\":\"sadas\"},\"\'.$counters.\'\":{\"petitioner_prefix\":\"F\\/O\"},\"2\":{\"petitioner_name\":\"sdfds\",\"petitioner_prefix\":\"D\\/O\",\"petitioner_father_name\":\"sdfsd\",\"petition_address\":\"sdfs\"}}', 'sadas', '{\"1\":{\"respondent_name\":\"asdas\",\"respondent_father_name\":\"asd\",\"respondent_address\":\"asdas\"},\"\'.$counterss.\'\":{\"respondent_prefix\":null},\"2\":{\"respondent_name\":\"sdfsd\",\"respondent_father_name\":\"dsf\",\"respondent_address\":\"sdfsdf\"}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(539, 33, '29/2019', '1061/2019', 2, 63, '2019-01-15', 2, 91, NULL, NULL, 45, 1, NULL, 'Amba Lal', 'null', 'Roshan Lal & Ors', 'null', 35, NULL, NULL, 42, 109, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(540, 33, '30/2019', '1003/19', 2, 63, '2019-03-16', 2, 91, NULL, NULL, 45, 1, NULL, 'Kamal Sharma & Ors', 'null', 'Anupam & Ors', 'null', 35, NULL, NULL, NULL, 110, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(541, 33, '31/2019', NULL, 2, 63, '2019-01-16', 2, 91, NULL, NULL, 45, 1, NULL, 'Hindustan Zinc Ltd', 'null', 'Industrial Tribunal (Dhoolchand)', 'null', 34, NULL, NULL, NULL, 80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(542, 33, '32/2019', NULL, 2, 63, '2019-01-16', 2, 91, NULL, NULL, 45, 1, NULL, 'HIndustan Zinc Ltd', 'null', 'Industrial Tribunal (Dhanraj)', 'null', NULL, NULL, NULL, NULL, 80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(543, 33, '33/2019', NULL, 2, 63, '2019-01-16', 2, 91, NULL, NULL, 45, 1, NULL, 'Hindustan Zinc Ltd', 'null', 'Industrial Tribunal (Harikishan)', 'null', NULL, NULL, NULL, NULL, 80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(544, 33, '34/2019', NULL, 2, 63, '2019-01-16', 2, 91, NULL, NULL, 45, 1, NULL, 'Hindustan Zinc Ltd', 'null', 'Industrial Tribunal (Govind Singh)', 'null', NULL, NULL, NULL, NULL, 80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(545, 33, '35/2019', '26/17', 2, 63, '2019-01-18', 2, 93, NULL, NULL, NULL, 1, NULL, 'Maximus International', 'null', 'RK Industries', 'null', 33, NULL, NULL, NULL, 111, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(546, 33, '36/2019', '260/19', 2, 63, '2019-01-18', 2, 97, NULL, NULL, 45, 1, NULL, 'Kheta Bhai', 'null', 'Dhanraj & Ors', 'null', 35, NULL, NULL, NULL, 112, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(547, 33, '37/2019', NULL, 2, 63, '2019-01-18', 2, 94, NULL, NULL, NULL, 1, NULL, 'Hemraj & Ors', 'null', 'Amba Lal & Ors', 'null', 35, NULL, NULL, 42, 113, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(548, 33, '38/2019', '1253/19', 2, 63, '2019-01-18', 2, 91, NULL, NULL, 45, 1, NULL, 'Hemraj & Ors', 'null', 'Amba Lal & Ors', 'null', 35, NULL, NULL, NULL, 113, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(549, 33, '39/2019', '2415/19', 2, 63, '2019-01-24', 1, 98, NULL, NULL, NULL, 1, NULL, 'Shyam Lal', 'null', 'State', 'null', 36, NULL, NULL, NULL, 114, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(550, 33, '40/2019', '18566/18', 2, 63, '2019-01-24', 2, 91, NULL, NULL, 45, 2, NULL, 'Chajju Ram', 'null', 'State(Excise)', 'null', 33, NULL, NULL, NULL, 94, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL);
INSERT INTO `adv_case_reg` (`reg_id`, `user_admin_id`, `reg_file_no`, `reg_case_number`, `reg_court`, `reg_court_id`, `reg_date`, `reg_case_type_category`, `reg_case_type_id`, `reg_vcn_number`, `reg_case_subtype_id`, `reg_stage_id`, `reg_power`, `reg_power_respondent`, `reg_petitioner`, `reg_petitioner_extra`, `reg_respondent`, `reg_respondent_extra`, `reg_assigend_id`, `reg_act_id`, `reg_section_id`, `reg_reffeerd_by_id`, `reg_client_group_id`, `reg_client_subgroup_id`, `reg_fir_id`, `reg_fir_year`, `reg_police_thana`, `reg_remark`, `reg_other_field`, `reg_opp_council`, `reg_nxt_further_date`, `reg_file_uploaded`, `reg_extra_party`, `reg_document`, `reg_status`, `reg_disposal_date`) VALUES
(551, 33, '41/2019', '1308/18', 2, 63, '2019-01-24', 1, 115, NULL, NULL, 45, 1, NULL, 'Navendra Pal', 'null', 'State(CBI)', 'null', NULL, NULL, NULL, NULL, 84, 96, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(552, 33, '42/2019', '216/19', 2, 63, '2019-01-24', 1, 100, NULL, NULL, 45, 1, NULL, 'Devendra Daga', 'null', 'State & Anr', 'null', 36, NULL, NULL, NULL, 115, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(553, 33, '43/2019', '1013/19', 2, 63, '2019-01-24', 2, 91, NULL, NULL, 45, 2, NULL, 'Bal Kishan Chhangani', 'null', 'State Bank of India', 'null', 33, NULL, NULL, NULL, 81, 100, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(554, 33, '44/2019', NULL, 2, 63, '2019-01-30', 2, 116, NULL, NULL, 45, 3, NULL, 'Anandi Lal', 'null', 'Pawan Kumar', 'null', 35, NULL, NULL, 52, 116, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(555, 33, '45/2019', NULL, 2, 63, '2019-01-30', 2, 116, NULL, NULL, 45, 3, NULL, 'Anandi Lal', 'null', 'Pawan Kumar', 'null', 35, NULL, NULL, 52, 116, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(556, 33, '46/2019', '1303/19', 2, 63, '2019-02-04', 1, 98, NULL, NULL, 46, 1, NULL, 'Asif', 'null', 'State', 'null', 36, NULL, NULL, 53, 117, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(557, 33, '47/2019', '25/19', 2, 63, '2019-02-04', 2, 116, NULL, NULL, 45, 1, NULL, 'Kanhaiya Lal', 'null', 'Narbada', 'null', 35, NULL, NULL, 54, 118, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(558, 33, '48/2019', '33/19', 2, 63, '2019-02-04', 2, 117, NULL, NULL, 46, 1, NULL, 'Paras Kumar', 'null', 'Narendra Kumar', 'null', 35, NULL, NULL, 55, 119, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(559, 33, '49/2019', '1815/19', 2, 63, '2019-02-04', 2, 91, NULL, NULL, 45, 1, NULL, 'Narayan Lal@ Kalu Bhai', 'null', 'Lal Chand & Anr', 'null', 35, NULL, NULL, 56, 120, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(560, 33, '50/2019', '261/13', 2, 63, '2019-02-05', 2, 116, NULL, NULL, 47, 2, NULL, 'Kalu', 'null', 'Dinkar Mogra', 'null', 35, NULL, NULL, 57, 121, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(561, 33, '51/2019', NULL, 2, 63, '2019-02-05', 2, 91, NULL, NULL, NULL, 3, NULL, 'State(Excise)', 'null', 'General Public', 'null', 33, NULL, NULL, NULL, 94, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(562, 33, '52/2019', '216/19', 2, 63, '2019-02-05', 1, 100, NULL, NULL, 46, 1, NULL, 'Devendra Daga', 'null', 'State', 'null', 36, NULL, NULL, NULL, 115, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(563, 33, '53/2019', NULL, 2, 63, '2019-02-05', 2, 91, NULL, NULL, 45, 1, NULL, 'Hindustan Zinc Ltd', 'null', 'State & Ors (SK South SB)', 'null', 34, NULL, NULL, NULL, 80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(564, 33, '54/2019', NULL, 2, 63, '2019-02-05', 2, 92, NULL, NULL, 45, 1, NULL, 'State(Excise)', 'null', 'Manoj', 'null', 33, NULL, NULL, NULL, 94, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(565, 33, '55/2019', '1763/19', 2, 63, '2019-02-05', 1, 98, NULL, NULL, 46, 1, NULL, 'Mukesh', 'null', 'State', 'null', 36, NULL, NULL, 58, 122, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(566, 33, '56/2019', '698/19', 2, 63, '2019-02-05', 1, 100, NULL, NULL, 46, 1, NULL, 'Mahaveer', 'null', 'State & Anr', 'null', 36, NULL, NULL, NULL, 91, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(567, 33, '57/2019', '3229/18', 2, 63, '2019-02-06', 1, 100, NULL, NULL, NULL, 1, NULL, 'Radhakishan', 'null', 'State (CBI)', 'null', 36, NULL, NULL, NULL, 84, 96, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(568, 33, '58/2019', '2940/18', 2, 63, '2019-02-06', 1, 100, NULL, NULL, 46, 1, NULL, 'Radhakishan', 'null', 'State (CBI)', 'null', 36, NULL, NULL, NULL, 84, 101, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(569, 33, '59/2019', '2938/18', 2, 63, '2019-02-06', 1, 100, NULL, NULL, 46, 2, NULL, 'Radhakishan', 'null', 'State(CBI)', 'null', 36, NULL, NULL, NULL, 84, 101, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(570, 33, '60/2019', '592/18', 2, 63, '2019-02-06', 2, 116, NULL, NULL, 46, 2, NULL, 'Ridhi Sidhi Associates', 'null', 'Smt Ummed Kanwar', 'null', 34, NULL, NULL, NULL, 123, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(571, 33, '61/2019', '2428/18', 2, 63, '2019-02-07', 2, 91, NULL, NULL, 45, 2, NULL, 'Bar Asso. Rajgarh', 'null', 'State (RHC)', 'null', 33, NULL, NULL, NULL, 124, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(572, 33, '62/2019', '19227/18', 2, 63, '2019-02-07', 2, 91, NULL, NULL, 46, 2, NULL, 'Maneela', 'null', 'RHC', 'null', 33, NULL, NULL, NULL, 124, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(573, 31, '1/2019', '111/2019', 2, 56, '2019-04-01', 1, 87, NULL, NULL, 42, 1, NULL, 'Rajesh Kumar & Ors', 'null', 'Suresh Kumar & Ors', 'null', 31, 46, '50', 38, 70, NULL, NULL, NULL, NULL, NULL, NULL, 'Mr Naresh Sankhla', '2019-04-05', NULL, 'null', NULL, 1, NULL),
(574, 31, '2/2019', '982/2019', 2, 56, '2019-04-01', 1, 85, NULL, 62, 42, 1, NULL, 'Smt Parvati Devi & Ors', 'null', 'BIGS & Ors', 'null', 30, 47, '53,54', 37, 73, NULL, NULL, NULL, NULL, 'Fees', 'Defect', 'Jaydev Mali', '2019-04-01', NULL, 'null', NULL, 1, NULL),
(575, 31, '3/2019', NULL, 1, 58, '2019-04-04', 1, 86, NULL, NULL, 42, 1, NULL, 'RADHEY', 'null', 'SHYAM', 'null', 31, 46, NULL, 38, 70, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(576, 31, '4/2019', '876/2019', 2, 57, '2019-06-01', 1, 86, NULL, NULL, 42, 1, NULL, 'GULLY BOY', 'null', 'SILY BOY', 'null', 30, 48, '56,57', 38, 77, 91, NULL, NULL, NULL, 'Fees Not Received', 'Case Not Pass', 'Ram Mohan', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(577, 31, '5/2019', '1234/2018', 1, 59, '2019-07-11', 1, 85, '111/20011', NULL, 42, 1, NULL, 'Ram Pratap Goswami @ Ramu Goswami & ors', 'null', 'State of Rajasthan through Secretary  & Ors', 'null', 31, 48, '56,57,58', 38, 78, 93, '123/2018', 1970, 'Jodhpur', 'Fee', 'Defect', 'Yogesh Purohit', '2019-07-18', NULL, 'null', NULL, 1, NULL),
(578, 31, '6/2019', 'DDDDDD', 2, 56, '2019-07-01', 1, 85, NULL, NULL, 43, 1, NULL, 'YYY', 'null', 'ZZZ', 'null', 30, 48, '57', 37, 78, NULL, NULL, NULL, NULL, NULL, NULL, 'ABC', '2019-07-24', NULL, 'null', NULL, 1, NULL),
(579, 31, '7/2019', NULL, 2, 56, '2019-07-01', 1, 86, NULL, NULL, 42, 1, NULL, 'Ramdev', 'null', 'State & Ors', 'null', 31, 47, '53', 38, 70, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-07-02', NULL, 'null', NULL, 1, NULL),
(580, 4, '2/2019', '23/209', 2, 12, '2019-08-01', 2, 5, NULL, NULL, 11, 1, NULL, 'abc', 'null', 'def', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(581, 4, '3/2019', '123/2018 K', 2, 13, '2019-08-01', 2, 16, '123K(P)', 12, 12, 1, NULL, 'Kailash Shah', '{\"1\":{\"petitioner_name\":\"Radhey Mohan\",\"petitioner_father_name\":\"Gurdev Sharma\",\"petition_address\":\"111-K CHB Housing Board \\r\\nJodhpur\"},\"\'.$counters.\'\":{\"petitioner_prefix\":\"W\\/O\"},\"2\":{\"petitioner_name\":\"Pinky Suthar\",\"petitioner_father_name\":\"Ganeshi Mal Suthar\",\"petition_address\":\"A-8, Shastri Nagar\\r\\nJodhpur\"}}', 'State & Ors', 'null', 12, 42, '41,42', 15, 23, 48, NULL, NULL, NULL, 'Case Not Pass', 'Fee Pending', 'Rakesh Chotia', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(582, 4, '4/2019', '567/2019', 2, 12, '2019-08-01', 2, 5, NULL, NULL, 12, 1, NULL, 'DK Garg', 'null', 'State', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(583, 4, '5/2019', NULL, 2, 12, '2019-08-05', NULL, NULL, NULL, NULL, NULL, 1, NULL, 'Manmohan Singh', 'null', 'State', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(584, 4, '6/2019', '32478/0219', 2, NULL, '2019-08-10', 2, 16, NULL, NULL, NULL, 1, NULL, 'Hari Prasad', 'null', 'Ram Mohan', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(585, 4, '7/2019', NULL, 1, NULL, '2019-08-16', NULL, NULL, NULL, NULL, NULL, 1, NULL, 'Manoj', 'null', 'Mahesh', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(586, 4, '8/2019', '3545', 1, NULL, '2019-08-20', 2, 15, '222', NULL, NULL, 1, NULL, 'Shiv Shankar', 'null', 'Ram Mohan', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(587, 4, '9/2019', NULL, 2, 13, '2019-08-19', NULL, NULL, NULL, NULL, NULL, 1, NULL, 'Deo Krishan', 'null', 'CBI', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(588, 4, '10/2019', NULL, 2, 12, '2019-08-01', 2, 15, NULL, NULL, NULL, 2, '1,3,4', 'INDIA', 'null', 'PAK', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(589, 4, '11/2019', '555/2015', 2, 12, '2019-08-01', 2, 17, NULL, 19, 14, 1, NULL, 'Vinod Kumar', 'null', 'Seema', 'null', 11, 13, '13', 10, 19, NULL, NULL, NULL, NULL, 'Paper Not Filed', 'Fees Not Received', 'Manvendra Singh', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(590, 4, '12/2019', '98257/2019', 2, 13, '2019-08-11', 1, 19, NULL, NULL, 13, 1, NULL, 'Agro Tech Pvt Ltd', 'null', 'State', 'null', 11, 42, '41,42', 10, 125, 103, NULL, NULL, NULL, 'XXXXXXXXXXXXX', 'YYYYYYYYYYYYYYYYY', 'Manvendra Singh', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(591, 4, '13/2019', '92578/2017', 2, 13, '2019-08-16', 1, 19, NULL, NULL, 11, 1, NULL, 'Radheshyam', 'null', 'UOI', 'null', 11, 14, NULL, 15, 126, NULL, NULL, NULL, NULL, NULL, NULL, 'Mahesh Kumar', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(592, 4, '14/2019', NULL, 2, 12, '2019-08-01', 2, 12, NULL, NULL, 8, 1, NULL, 'NZ', 'null', 'Eng', 'null', 11, 58, '59', 3, 126, NULL, NULL, NULL, NULL, NULL, NULL, 'XXX', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(593, 4, '15/2019', '134/2018', 2, 12, '2019-08-01', 2, 16, NULL, 18, 13, 1, NULL, 'SHYAM SUNDER', 'null', 'STATE', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-15', NULL, 'null', NULL, 1, NULL),
(594, 4, '16/2019', '12345/2015', 2, 12, '2019-08-01', 1, 20, NULL, 21, NULL, 1, NULL, 'MUKESH', 'null', 'SANWIR', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-02', NULL, 'null', NULL, 1, NULL),
(595, 4, '17/2019', '92548/2009', 2, 12, '2019-09-01', 2, 16, NULL, 18, 11, 2, '1,3,8', 'Smt Vimla Devi', 'null', 'State', 'null', 11, 42, '41,42', NULL, 127, NULL, NULL, NULL, NULL, 'xxxxxxxxxxxxxx', 'yyyyyyyyyyyyyyyyyyyy', 'Mukesh Rajpurohit', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(596, 4, '18/2019', '7437/2019', 2, 12, '2019-09-01', 2, 16, NULL, 18, 14, 1, NULL, 'All India SBBJ Employeement', 'null', 'State', 'null', 11, 14, NULL, 9, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(597, 4, '19/2019', NULL, 2, 12, '2019-09-01', 2, 15, NULL, NULL, NULL, 1, NULL, 'a', 'null', 'b', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(598, 4, '20/2019', NULL, 2, 12, '2019-09-01', 2, 15, NULL, NULL, NULL, 1, NULL, 'RAM', 'null', 'RAVAN', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(600, 4, '22/2019', '123456789/18', 1, 14, '2019-09-01', 2, 5, '123456/19', NULL, 7, 2, NULL, 'DHEERAJ PUROHIT', '{\"1\":{\"petitioner_name\":\"ANIL\",\"petitioner_father_name\":\"VIJAY\",\"petition_address\":\"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\"},\"\'.$counters.\'\":{\"petitioner_prefix\":\"S\\/O\"},\"2\":{\"petitioner_name\":null,\"petitioner_father_name\":null,\"petition_address\":null}}', 'DEEPAK DEORA', '{\"1\":{\"respondent_name\":\"VIJAY\",\"respondent_father_name\":\"SUNIL\",\"respondent_address\":\"AAAAAAAAAAAAAAAAAAAAAAAAAAAAA\"},\"\'.$counterss.\'\":{\"respondent_prefix\":\"S\\/O\"},\"2\":{\"respondent_name\":null,\"respondent_father_name\":null,\"respondent_address\":null}}', 11, 59, '61,62', 59, 128, NULL, '1348', 2019, 'RATANADA', 'REMARKKKKKKKKKKKKKKKKKKKKKKKK', 'OTEHR FILEDDDDDDDDDDDDDDDDDDDDDDDDDDD', 'RAJEEV', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(601, 4, '22/2019', '123/19', 2, 12, '2019-09-02', 2, 17, NULL, 19, 11, 2, '1,3', 'LUCKY', 'null', 'VICKY', 'null', 11, 59, '61,62', 59, 128, NULL, '111', 2019, 'RATANADA', 'RAMERAKKKKKKKKKKKKKKKKKKKKKKKKKK', NULL, 'RAJU JI', '2019-09-26', NULL, 'null', NULL, 1, NULL),
(602, 4, '23/2019', NULL, 1, 72, '2019-09-01', 2, 16, '123/001', NULL, 11, 1, NULL, 'Gur Pratap', 'null', 'Shiv Ratan', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(603, 4, '24/2019', '124/2014', 1, 14, '2019-09-01', 2, 15, '25/2018', NULL, 12, 1, NULL, 'Prem Prakash', 'null', 'Ganesh Kumar', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(604, 4, '25/2019', '325/2017', 1, 15, '2019-09-01', 2, 15, '928/2017', NULL, 12, 2, '1,3', 'Nagfani', 'null', 'State', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(605, 4, '26/2019', '925/2014', 1, 16, '2019-09-01', 1, 18, '417/2017', NULL, 12, 1, NULL, 'Shreeji Animal', 'null', 'State', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(606, 4, '27/2019', '925', 1, 14, '2019-09-01', 2, 16, '225', NULL, 12, 1, NULL, 'Smt Maheshwari', 'null', 'State', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(607, 4, '28/2019', '123/2019', 2, 12, '2019-09-01', 2, 15, NULL, 12, 11, 1, NULL, 'Imrati Choudhary', 'null', 'State', 'null', 11, 59, '61,62', 15, 64, NULL, NULL, NULL, NULL, 'ZZZZZZZZZZZZ', 'AAAAAAAAAAAAAA', 'Chayan Boothra', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(608, 4, '29/2019', '4954/04', 2, 12, '2019-08-27', 2, 5, NULL, NULL, 12, 2, '1,3', 'Sayar Singh', 'null', 'SBBJ', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(609, 4, '30/2019', '1021/2019', 2, 12, '2019-08-27', 2, 15, NULL, 12, 11, 1, NULL, 'Hira Industries', 'null', 'State', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(610, 4, '31/2019', '4809/2019', 2, 12, '2019-08-27', 1, 19, NULL, 20, 13, 1, NULL, 'Rohtash Jain', 'null', 'State', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(611, 4, '32/2019', '1246/2016', 2, 12, '2019-08-27', 1, 20, NULL, NULL, 13, 1, NULL, 'Banshidhar Swami', 'null', 'UOI(CBI)', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(612, 4, '33/2019', '193/2007', 2, 12, '2019-08-27', 2, 15, NULL, NULL, 13, 1, NULL, 'MC Bhilwara', 'null', 'LC Bhilwara', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(613, 4, '34/2019', '5200/2018', 2, 12, '2019-08-27', 2, 5, NULL, NULL, 13, 2, '1,5,6', 'Devyani Phosphate', 'null', 'SBBJ', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(614, 4, '35/2019', '621/2019', 2, 12, '2019-08-27', 2, 15, NULL, NULL, 13, 1, NULL, 'Hussain Ali', 'null', 'Mehtab Singh', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(615, 4, '36/2019', '1347/2016', 2, 12, '2019-08-27', 2, 5, NULL, NULL, 13, 1, NULL, 'Balveer Singh', 'null', 'Karan Singh', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(616, 4, '37/2019', '325/2019', 2, 12, '2019-08-27', 1, 19, NULL, NULL, NULL, 1, NULL, 'Radico Khaitan', 'null', 'State', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(617, 4, '38/2019', '2220/02', 2, 12, '2019-08-27', 2, 5, NULL, NULL, 13, 1, NULL, 'Gopi Lal', 'null', 'State', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(618, 4, '39/2019', '635/201', 2, 12, '2019-08-27', 2, 5, NULL, NULL, 13, 1, NULL, 'Pernod Ricard', 'null', 'State', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(619, 4, '40/2019', '11827/19', 2, NULL, '2019-08-28', 2, 5, NULL, NULL, 13, 1, NULL, 'Bheru Lal', 'null', 'Kusum', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(620, 4, '41/2019', '12694/2015', 2, 12, '2019-08-27', 2, 5, NULL, NULL, 11, 1, NULL, 'M/s Bichwara Filling Station', 'null', 'BPCL', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(621, 4, '42/2019', '12694/2015', 2, 12, '2019-08-27', 2, 5, NULL, NULL, 11, 1, NULL, 'M/s Bichwara Filling Station', 'null', 'BPCL', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(622, 4, '1/2020', '2325/2018', 2, 12, '2020-01-01', 2, 5, NULL, NULL, 7, 1, NULL, 'Udaipur Entertainment', 'null', 'State', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(623, 4, '2/2020', '9251/2019', 2, 12, '2020-01-01', 2, 16, NULL, 13, 13, 2, NULL, 'Lrs of Durga Datt', 'null', 'State', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(624, 4, '3/2020', '9351/2018', 2, 12, '2020-01-01', 2, 16, NULL, NULL, 11, 1, NULL, 'Ranjan Tak', 'null', 'ONGC', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(625, 4, '4/2020', '9257/2018', 2, 12, '2020-01-02', 1, 18, NULL, NULL, 12, 1, NULL, 'Prateek', 'null', 'Dinesh Kumar', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(626, 4, '43/2019', '1114/2019', 2, 12, '2019-09-03', 1, 20, NULL, NULL, 11, 1, NULL, 'Khatain Business', 'null', 'State & Ors', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(627, 4, '5/2020', '1114/2019', 2, 12, '2020-01-02', 2, 15, NULL, 12, 8, 1, NULL, 'Khaitan Business', 'null', 'State', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(628, 4, '6/2020', '290/19', 2, 12, '2020-01-01', 2, 15, NULL, 12, 13, 1, NULL, 'Tulsi Amrit Niketan', 'null', 'UI Insurance', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(629, 4, '7/2020', '291/19', 2, 12, '2020-01-01', 2, 17, NULL, 19, 7, 1, NULL, 'Tulsi Amrit Niketan', 'null', 'United Insurance', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(630, 4, '8/2020', '4605/2018', 2, 12, '2020-01-01', 2, 5, NULL, NULL, 7, 1, NULL, 'Bhagwat Singh', 'null', 'SBI', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(631, 4, '9/2020', '259/19', 2, 12, '2020-01-01', 2, 5, NULL, NULL, 13, 1, NULL, 'Mahaveer', 'null', 'State & Ors', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(632, 4, '10/2020', '45/2018', 2, 12, '2020-01-01', 2, 15, NULL, NULL, NULL, 1, NULL, 'SBI', 'null', 'Rajendra Kumar', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(633, 4, '11/2020', '1611/18', 1, 14, '2020-01-01', 2, 5, '123/2019', NULL, 7, 1, NULL, 'Gora Dan Ashiya', 'null', 'Rameshwar Vyas', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(634, 4, '12/2020', '12823/18', 1, 14, '2020-01-01', 2, 5, '1232', NULL, 7, 2, NULL, 'Happy Choubisa', 'null', 'State', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(635, 4, '13/2020', '12805/18', 1, 15, '2020-01-01', 2, 15, '2211', NULL, 7, 1, NULL, 'Rakesh Kumar', 'null', 'State', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(636, 4, '14/2020', '12811/18', 1, 15, '2020-01-01', 2, 16, '325', NULL, 7, 1, NULL, 'Ravindra Kumar', 'null', 'State', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(637, 4, '15/2020', '12820/18', 1, 72, '2020-01-01', 2, 17, NULL, NULL, 11, 1, NULL, 'Amba Lal', 'null', 'State', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(638, 4, '16/2020', '12846/18', 1, 72, '2020-01-01', 2, 16, NULL, NULL, 7, 1, NULL, 'Usha Sunil', 'null', 'State', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(639, 4, '44/2019', NULL, 2, NULL, '2019-09-01', 2, 16, NULL, NULL, NULL, 1, NULL, 'zaaaa', 'null', 'pppp', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(640, 4, '45/2019', NULL, 2, 12, '2019-09-01', 2, 16, NULL, NULL, NULL, 1, NULL, 'Google', 'null', 'Server', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(641, 4, '46/2019', NULL, 2, NULL, '2019-09-05', NULL, NULL, NULL, NULL, NULL, 1, NULL, 'www', 'null', 'qqq', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(642, 4, '47/2019', '215/2019', 1, 15, '2019-09-01', 1, 119, '325/2019', NULL, 7, 1, NULL, 'Hari Prasad', 'null', 'Rakesh Kumar', 'null', 11, 58, '59', 59, 64, NULL, NULL, NULL, NULL, NULL, NULL, 'Manvendra Singh', '1970-01-01', NULL, 'null', NULL, 1, NULL),
(643, 4, '48/2019', 'fdsfsf', 1, 14, '2019-09-01', 2, 15, 'cdfsf', NULL, 7, 1, NULL, 'Rahim', 'null', 'UOI', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL),
(644, 4, '49/2019', NULL, 1, 14, '2019-09-01', 2, 17, NULL, NULL, NULL, 1, NULL, 'Munshi', 'null', 'Advocate', 'null', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, 'null', NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `adv_case_type`
--

CREATE TABLE `adv_case_type` (
  `case_id` int(11) NOT NULL,
  `user_admin_id` int(11) DEFAULT NULL,
  `case_name` varchar(50) DEFAULT NULL,
  `case_short_name` varchar(50) DEFAULT NULL,
  `case_category` tinyint(2) DEFAULT NULL COMMENT '2=Civil,1=Criminal	',
  `case_status` tinyint(2) NOT NULL COMMENT '0=Inactive,1=Active	'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adv_case_type`
--

INSERT INTO `adv_case_type` (`case_id`, `user_admin_id`, `case_name`, `case_short_name`, `case_category`, `case_status`) VALUES
(1, 6, 'Murder', 'MUD', 1, 1),
(2, 6, 'Real State', 'RST', 2, 1),
(3, 6, 'Talak', 'TK', 2, 0),
(4, 6, 'Chori', 'CR', 1, 0),
(5, 4, 'C.W', NULL, 2, 1),
(6, 9, 'Test Three', 'TE', 1, 1),
(7, 9, 'Test two', 'TE', 1, 1),
(8, 9, 'Test one', 'TE', 2, 1),
(9, 9, NULL, NULL, 1, 0),
(11, 4, 'Test One', 'TE', 1, 0),
(12, 4, 'Test Two', 'TE', 2, 1),
(13, 8, 'Test one', 'TT', 1, 1),
(14, 8, 'Test two', 'TT', 2, 1),
(15, 4, 'C.W', NULL, 2, 1),
(16, 4, 'C.FA', NULL, 2, 1),
(17, 4, 'C.SA', NULL, 2, 1),
(18, 4, 'C.RLMP', NULL, 1, 1),
(19, 4, 'BAIL', NULL, 1, 1),
(20, 4, 'C.RLR', NULL, 1, 1),
(21, 14, 'Test', NULL, 1, 1),
(22, 14, 'Test One', NULL, 2, 1),
(24, 15, 'C.RLA', NULL, 1, 1),
(25, 16, 'Criminal', NULL, 1, 1),
(26, 16, 'Civil', NULL, 2, 1),
(27, 15, 'C.W', NULL, 2, 1),
(28, 15, 'C.RLMP', NULL, 1, 1),
(29, 15, 'BAIL', NULL, 1, 1),
(30, 15, 'C.RLR', NULL, 1, 1),
(31, 15, 'C.FA', NULL, 2, 1),
(32, 15, 'C.SA', NULL, 2, 1),
(33, 15, 'C.MA', NULL, 2, 1),
(34, 15, 'S.AW', NULL, 2, 1),
(35, 15, 'C.RRLA', NULL, 1, 1),
(36, 15, 'C.RLW', NULL, 2, 1),
(37, 15, 'C.LA', NULL, 2, 1),
(38, 18, 'CW', NULL, 2, 1),
(39, 18, 'CFA', NULL, 2, 1),
(40, 18, 'CSA', NULL, 2, 1),
(41, 18, 'CMA', NULL, 2, 1),
(42, 18, 'CRLMP', NULL, 1, 1),
(43, 18, 'CRLA', NULL, 1, 1),
(44, 18, 'BAIL', NULL, 1, 1),
(45, 18, 'CRLR', NULL, 1, 1),
(46, 18, 'SAW', NULL, 2, 1),
(47, 18, 'ARB', NULL, 2, 1),
(48, 18, 'SAC', NULL, 2, 1),
(49, 18, 'CCP', NULL, 2, 1),
(50, 18, 'WCP', NULL, 2, 1),
(51, 18, 'CRES', NULL, 2, 1),
(52, 18, 'WRES', NULL, 2, 1),
(53, 18, 'CRLLA', NULL, 1, 1),
(54, 18, 'CMAP', NULL, 2, 1),
(55, 18, 'CRLW', NULL, 1, 1),
(56, 18, 'CTA', NULL, 2, 1),
(57, 18, 'CRLAS', NULL, 1, 1),
(58, 18, 'REVENUE SECOND APPEAL', NULL, 2, 1),
(59, 18, 'WRW', NULL, 2, 1),
(60, 18, 'COMPENSATION', NULL, 2, 1),
(61, 19, 'CR1', NULL, 1, 1),
(62, 19, 'CR2', NULL, 2, 1),
(63, 18, 'CREW', NULL, 2, 1),
(64, 22, 'CRLMP', NULL, 1, 1),
(65, 22, 'BAIL', NULL, 1, 1),
(66, 22, 'CRLR', NULL, 1, 1),
(67, 22, 'CFA', NULL, 2, 1),
(68, 22, 'CSA', NULL, 2, 1),
(69, 22, 'CMA', NULL, 2, 1),
(71, 18, 'CR(CIV REV.)', NULL, 2, 1),
(72, 18, 'OA', NULL, 2, 1),
(73, 18, 'COP', NULL, 2, 1),
(74, 18, 'IA', NULL, 2, 1),
(75, 29, 'Murder', NULL, 1, 1),
(76, 29, 'Daketi', NULL, 2, 1),
(77, 29, 'Murder One', NULL, 1, 1),
(78, 29, 'Daketi One', NULL, 2, 0),
(79, 29, 'Garelu', NULL, 2, 1),
(80, 30, 'Murder', NULL, 1, 1),
(81, 30, 'Daketi', NULL, 2, 1),
(82, 30, 'Murder One', NULL, 1, 1),
(83, 30, 'Daketi One', NULL, 2, 1),
(84, 30, 'Garelu', NULL, 2, 1),
(85, 31, 'C.RLMP', NULL, 1, 1),
(86, 31, 'C.RLR', NULL, 1, 1),
(87, 31, 'BAIL', NULL, 1, 1),
(88, 31, 'C.W', NULL, 2, 1),
(89, 31, 'S.AW', NULL, 1, 1),
(90, 31, 'C.MA', NULL, 2, 1),
(91, 33, 'C.W', NULL, 2, 1),
(92, 33, 'S.AW', NULL, 2, 1),
(93, 33, 'ARB', NULL, 2, 1),
(94, 33, 'C.W', NULL, 2, 1),
(95, 33, 'W.MAP', NULL, 2, 1),
(96, 33, 'C.SA', NULL, 2, 1),
(97, 33, 'C.MA', NULL, 2, 1),
(98, 33, 'BAIL', NULL, 1, 1),
(99, 33, 'W.CP', NULL, 2, 1),
(100, 33, 'C.RLMP', NULL, 1, 1),
(101, 39, 'C.RLMP', NULL, 1, 1),
(102, 39, 'BAIL', NULL, 1, 1),
(103, 39, 'C.RLR', NULL, 1, 1),
(104, 39, 'C.RLA', NULL, 1, 1),
(105, 39, 'C.RLLA', NULL, 1, 1),
(106, 39, 'C.MA', NULL, 2, 1),
(107, 39, 'C.W', NULL, 2, 1),
(108, 39, 'C.LA', NULL, 2, 1),
(109, 39, 'C.SA', NULL, 2, 1),
(110, 39, 'C.FA', NULL, 2, 1),
(111, 39, 'W.RES', NULL, 2, 1),
(112, 39, 'C.RES', NULL, 2, 1),
(113, 39, 'S.AW', NULL, 2, 1),
(114, 40, 'CW', NULL, 2, 1),
(115, 33, 'C.RLR', NULL, 1, 1),
(116, 33, 'C.FA', NULL, 2, 1),
(117, 33, 'C.R', NULL, 2, 1),
(118, 4, 'BIAL', NULL, 1, 1),
(119, 4, 'CR REGULAR', NULL, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `adv_certified_copy_date`
--

CREATE TABLE `adv_certified_copy_date` (
  `ccd_id` int(11) NOT NULL,
  `user_admin_id` int(11) DEFAULT NULL,
  `ccd_status_date` varchar(255) DEFAULT NULL,
  `ccd_date_pic` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ccd_title` varchar(255) DEFAULT NULL,
  `ccd_case_no` varchar(255) DEFAULT NULL,
  `ccd_date` date DEFAULT NULL,
  `ccd_status` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `adv_class_code`
--

CREATE TABLE `adv_class_code` (
  `classcode_id` int(11) NOT NULL,
  `subtype_case_id` int(11) DEFAULT NULL,
  `user_admin_id` int(11) DEFAULT NULL,
  `classcode_name` varchar(50) DEFAULT NULL,
  `classcode_short_name` varchar(50) DEFAULT NULL,
  `classcode_code` varchar(50) DEFAULT NULL,
  `classcode_description` varchar(255) DEFAULT NULL,
  `classcode_status` tinyint(2) NOT NULL COMMENT '0=Inactive,1=Active	'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adv_class_code`
--

INSERT INTO `adv_class_code` (`classcode_id`, `subtype_case_id`, `user_admin_id`, `classcode_name`, `classcode_short_name`, `classcode_code`, `classcode_description`, `classcode_status`) VALUES
(1, 1, 6, 'MDR', 'MR', 'MDR123', 'This type is murder', 1),
(2, 1, 6, 'mdtest', 'mtest', 'test1', 'asd', 1),
(3, 2, 6, 'RSCode', 'RSC', 'RSOn', 'asd', 1),
(4, 5, 4, 'gf', 'vcx', '21', 'ff', 0),
(5, 5, 4, 'Ab', 'vcx', '21', 'ff', 0),
(6, NULL, 9, NULL, NULL, NULL, NULL, 0),
(7, 6, 9, 'ad', 'Test', '10B@78%', 'Test', 1),
(8, 11, 4, 'one', 'One', 'One', 'One', 1),
(9, 12, 4, 'Two', 'Two', 'Two', 'Two', 1),
(10, 13, 8, 'one', 'One', 'One', 'Test', 1),
(11, 14, 8, 'Two', 'TT', 'Two', 'Test', 1),
(12, 15, 4, '4300- Writ Not Cover', NULL, NULL, NULL, 1),
(13, 16, 4, '5100- Bank Loan', NULL, NULL, NULL, 1),
(14, 19, 4, '438- Bail', NULL, NULL, NULL, 1),
(15, 21, 14, 'Test', NULL, NULL, NULL, 1),
(18, 16, 4, '1500- Civil First Appeal', NULL, NULL, NULL, 1),
(19, 17, 4, '1500- Civil Second Appeal', NULL, NULL, NULL, 1),
(20, 19, 4, '1100- Criminal Misc Bail', NULL, NULL, NULL, 1),
(21, 20, 4, '1100- Criminal Revision', NULL, NULL, NULL, 1),
(22, 22, 14, 'Test', NULL, NULL, NULL, 1),
(23, 26, 16, 'Class Code 1', NULL, NULL, NULL, 1),
(24, 25, 16, 'Class Code 2', NULL, NULL, NULL, 1),
(25, 27, 15, '4300- Writ Not Cover', NULL, NULL, NULL, 1),
(26, 31, 15, '5100- Bank Loan', NULL, NULL, NULL, 1),
(27, 32, 15, '1500- Civil Second Appeal', NULL, NULL, NULL, 1),
(28, 32, 15, '1300- Civil Second Appeal', NULL, NULL, NULL, 1),
(29, 27, 15, '510- Transfer', NULL, NULL, NULL, 1),
(30, 27, 15, '0512- AD HOC Appointment', NULL, NULL, NULL, 1),
(31, 27, 15, '2200- Forest Act Cases', NULL, NULL, NULL, 1),
(32, 31, 15, '5900- R.F.C.', NULL, NULL, NULL, 1),
(33, 33, 15, '501- Minor Penalty', NULL, NULL, NULL, 1),
(34, 33, 15, '502-Removal Dissmissal', NULL, NULL, NULL, 1),
(35, 29, 15, '438- Bail', NULL, NULL, NULL, 1),
(36, 29, 15, '439-Bail', NULL, NULL, NULL, 1),
(37, 29, 15, '438-Bail', NULL, NULL, NULL, 1),
(38, 61, 19, '1', NULL, NULL, NULL, 1),
(39, 62, 19, '2', NULL, NULL, NULL, 1),
(40, 64, 22, '482- CRIMINAL', NULL, NULL, NULL, 1),
(41, 64, 22, '438- CRIMINAL', NULL, NULL, NULL, 1),
(42, 65, 22, '439-BAIL', NULL, NULL, NULL, 1),
(43, 65, 22, '125-BAIL', NULL, NULL, NULL, 1),
(44, 66, 22, '101-CRIMINAL REVISION', NULL, NULL, NULL, 1),
(45, 66, 22, '103-CRIMINAL REVISION', NULL, NULL, NULL, 1),
(46, 67, 22, '535-CIVIL FIRST APPEAL', NULL, NULL, NULL, 1),
(47, 67, 22, '536-CIVIL SECOND APPEAL', NULL, NULL, NULL, 1),
(48, 68, 22, '1008-CIVIL SECOND APPEAL', NULL, NULL, NULL, 1),
(49, 68, 22, '1011-CIVIL SECOND APPEAL', NULL, NULL, NULL, 1),
(50, 69, 22, '101-CIVIL MISC APPEAL', NULL, NULL, NULL, 1),
(51, 69, 22, '1011-CIVIL MISC APPEAL', NULL, NULL, NULL, 1),
(52, 75, 29, 'MU001', NULL, NULL, NULL, 1),
(53, 75, 29, 'MU001', NULL, NULL, NULL, 1),
(54, 75, 29, 'MU001', NULL, NULL, NULL, 1),
(55, 75, 29, 'MU001', NULL, NULL, NULL, 1),
(56, 80, 30, 'MU001', NULL, NULL, NULL, 1),
(57, 80, 30, 'MU002', NULL, NULL, NULL, 1),
(58, 81, 30, 'DE001', NULL, NULL, NULL, 1),
(59, 82, 30, 'MU001', NULL, NULL, NULL, 1),
(60, 84, 30, 'GE001', NULL, NULL, NULL, 1),
(61, 88, 31, '4300- Writ Not Covered', NULL, NULL, NULL, 1),
(62, 85, 31, '482- Criminal Misc Petition', NULL, NULL, NULL, 1),
(63, 87, 31, '438- Bail', NULL, NULL, NULL, 1),
(64, 90, 31, 'Fatal Insure', NULL, NULL, NULL, 1),
(65, 107, 39, '0501-MATTERS RELATING TO MINOR PENALTIES', NULL, NULL, NULL, 1),
(66, 107, 39, '0502-  MATTERS RELATING TO SUSPENSION FROM SERVICE', NULL, NULL, NULL, 1),
(67, 107, 39, '0509- MATTERS RELATING TO RETRIAL BENEFITS', NULL, NULL, NULL, 1),
(68, 107, 39, '0510- MATTERS RELATING TO TRANSFER', NULL, NULL, NULL, 1),
(69, 101, 39, '73(S)- MATTERS RELATING TO LOAN AND RECOVERY', NULL, NULL, NULL, 1),
(70, 102, 39, '438- CRIMINAL BAIL', NULL, NULL, NULL, 1),
(71, 102, 39, '439- CRIMINAL BAIL', NULL, NULL, NULL, 1),
(72, 103, 39, 'CRIMINAL REVISION', NULL, NULL, NULL, 1),
(73, 104, 39, 'CRIMINAL APPEAL', NULL, NULL, NULL, 1),
(74, 105, 39, 'CRIMINAL LEAVE TO APPEAL', NULL, NULL, NULL, 1),
(75, 101, 39, 'MATTERS RELATING TO APPEAL AGAINST AQUATAL', NULL, NULL, NULL, 1),
(76, 106, 39, '0601- SIMPAL INJURY MATTERS', NULL, NULL, NULL, 1),
(77, 106, 39, '0602- GRIEVOUS INJURY MATTERS', NULL, NULL, NULL, 1),
(78, 106, 39, '0603- FATAL INJURY, PERMANENT DISABILITY MATTERS', NULL, NULL, NULL, 1),
(79, 109, 39, '2300- FOREST ACT MATTERS', NULL, NULL, NULL, 1),
(80, 109, 39, '3000- MORTGAGE & REDEMPTION MATTERS', NULL, NULL, NULL, 1),
(81, 110, 39, '5300- R.S.E.B. WRITS', NULL, NULL, NULL, 1),
(82, 110, 39, '5400- RAJASTHAN MUNICIPALITIES ACT', NULL, NULL, NULL, 1),
(83, 111, 39, '0512- AD HOC APPOINTMENTS', NULL, NULL, NULL, 1),
(84, 111, 39, '0514- CASUAL, DAILY WAGES APPOINTMENTS ETC', NULL, NULL, NULL, 1),
(85, 112, 39, '0516 OTHER SERVICE MATTERS', NULL, NULL, NULL, 1),
(86, 112, 39, '1501- REGULARIZATION OF AD HOC CASUAL & DAILY WAGE', NULL, NULL, NULL, 1),
(87, 112, 39, '1502- INDUSTRIAL DISPUTES ACT 1947', NULL, NULL, NULL, 1),
(88, 113, 39, '1506- FACTORIES ACT, 1948', NULL, NULL, NULL, 1),
(89, 113, 39, '1508 PAYMENT OF GRATUITY ACT 1962', NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `adv_client`
--

CREATE TABLE `adv_client` (
  `cl_id` int(11) NOT NULL,
  `user_admin_id` int(11) DEFAULT NULL,
  `cl_group_name` varchar(50) DEFAULT NULL,
  `cl_name_prefix` varchar(50) DEFAULT NULL,
  `cl_father_name` varchar(50) DEFAULT NULL,
  `cl_group_email_id` varchar(255) DEFAULT NULL,
  `cl_group_mobile_no` varchar(50) DEFAULT NULL,
  `cl_group_place` varchar(50) DEFAULT NULL,
  `cl_group_address` text,
  `cl_group_type` int(11) NOT NULL DEFAULT '1' COMMENT '1 - Individual 2 - Organisation',
  `cl_status` tinyint(2) NOT NULL COMMENT '0=Inactive,1=Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adv_client`
--

INSERT INTO `adv_client` (`cl_id`, `user_admin_id`, `cl_group_name`, `cl_name_prefix`, `cl_father_name`, `cl_group_email_id`, `cl_group_mobile_no`, `cl_group_place`, `cl_group_address`, `cl_group_type`, `cl_status`) VALUES
(1, 6, 'Test individual', NULL, NULL, 'test@gmail.com', '1234567890', NULL, 'test 123', 1, 1),
(2, 6, 'Test Organisation', NULL, NULL, 'Testo@gmail.com', '1234567890', NULL, 'test', 2, 1),
(3, 6, 'mohan', NULL, NULL, 'mo@mo.com', '3698521470', NULL, 'asas\r\ntest\r\ntest', 1, 1),
(4, 4, 'SBI', NULL, NULL, 'sbi@gmail.com', '7878787878', NULL, '5th Road', 2, 1),
(6, 9, 'Aditya', NULL, NULL, 'testingv2r@gmail.com', '1234567890', NULL, 'Test', 1, 1),
(8, 9, 'Ad test', NULL, NULL, 'test@gmail.com', '1234567890', NULL, 'Test', 2, 1),
(9, 9, 'Ad test one', NULL, NULL, 'test1@gmail.com', '9876543210', NULL, 'Test', 2, 1),
(12, 9, 'Ad test one', NULL, NULL, 'test1@gmail.com', '9876543210', NULL, 'Test', 2, 1),
(13, 4, 'Test one', NULL, NULL, 'test1@gmail.com', '1236547890', NULL, 'Test', 1, 1),
(14, 4, 'Test Two', NULL, NULL, 'test2@gmail.com', '1236547890', NULL, 'Test', 1, 0),
(15, 4, 'Pradeep', NULL, NULL, 'Pradeep@gmail.com', '9874562222', NULL, 'Pradeep', 2, 1),
(16, 8, 'Pradeep rai', NULL, NULL, 'Pradeep@gmail.com', '1236547890', NULL, 'Test', 1, 1),
(17, 8, 'Prem', NULL, NULL, 'prem@gmail.com', '1236547890', NULL, 'Test', 2, 1),
(18, 4, 'sbi', NULL, NULL, 'ifb@gr.fdry', '7887777768', NULL, 'njjhjhj', 2, 1),
(19, 4, 'Ram Mohan Choudhary', NULL, NULL, 'rammohan@gmail.com', '1234567890', 'Jodhpur', '20/21, Kudi Housing Board\r\nJodhpur', 1, 1),
(20, 4, 'Ram Prakash Choudhary', NULL, NULL, 'ramprakash@gmail.com', '0987654321', 'Jaipur', '111 Near Pratap School \r\nJaipur', 1, 1),
(21, 4, 'UIT', 'D/O', 'ty', NULL, '77887766767', 'Bikaner', 'Near Public Park,\r\nBikaner\r\nNear Public Park,\r\nBikaner\r\nNear Public Park,\r\nBikaner\r\nNear Public Park,\r\nBikaner', 2, 1),
(22, 4, 'State bank of India', NULL, NULL, 'sbi@co,in', NULL, 'Jodhpur', NULL, 2, 1),
(23, 4, 'CBI', NULL, NULL, NULL, NULL, 'Jodhpur', NULL, 2, 1),
(24, 4, 'Ramavtar Choudhary', NULL, NULL, 'ramavtar@gmail.com', '12345667654', 'Ajmer', 'Ajmeri Gate, Near Hotel Badri Palace\r\nAjmer', 1, 1),
(27, 14, 'Aditya Soni', NULL, NULL, 'adityasoni@v2rsolution.com', '7597037331', 'Ajmer', 'Test', 1, 1),
(28, 14, 'Aditya Soni', NULL, NULL, 'adityasoni@v2rsolution.com', '7597037331', 'Ajmer', 'Test', 1, 1),
(29, 4, 'Mukesh Kumar S/o Mahesh Kumar Sharma', NULL, NULL, 'mukesh@gmail.com', '9876543287', 'Sawai Madhopur', '89-B IInd Floor, Ram Apartment, Sawai Madhopur', 1, 1),
(32, 15, 'DHEERAJ KUMAR PUROHIT', NULL, NULL, 'dheerajbpurohit@gmail.com', '7737388902', 'JODHPUR', '20/969 CHB JODHPUR', 1, 1),
(33, 16, 'Individual', NULL, NULL, 'v2rteam@gmail.com', '7597037331', 'Ajmer', 'Ajmer', 1, 1),
(34, 16, 'Organisation', NULL, NULL, 'a@gmail.com', '7597037331', 'Ajmer', 'Ajmer', 2, 1),
(35, 15, 'Prahlad Soni S/o Duli Chand Soni', NULL, NULL, 'prahladsoni@gmail.com', '9460804356', 'Udaipur', '7-A, Bapu Nagar, Near DENA Bank\r\nUdaipur (Raj)', 1, 1),
(36, 15, 'Sanveer Singh S/o Nathu Singh Sekhawat', NULL, NULL, 'sanveer.sekhwat@gmail.com', '8769387512', 'Nagaur', '67, Rajputo Ka Bass, Nimbola Kala\r\nVia Ren, Tehsil:- Degana\r\nDistrict: Nagau (Raj)\r\nPin Code: 341514', 1, 1),
(37, 15, 'Aashkaran S/o Poonam Chand', NULL, NULL, 'ashkran@gmail.com', '9828999989', 'Bikaner', 'Mahabali Puram Colony\r\nNokha Road, Ganga Shahar\r\nBikaner (Raj)', 1, 1),
(38, 15, 'Abhisek Dadhich S/o Anil Dadhich', NULL, NULL, 'abhisek@gmail.com', '9413347879', 'Bhilwara', 'Azad Nagar, PS Pratapnagar\r\nBHILWARA', 1, 1),
(39, 15, 'Ajay Kumar S/o Raj Kumar', NULL, NULL, 'ajay.12@gmail.com', '9314348822', 'Sri Ganganagar', 'Ward No 4, Tehsil:- Srikaranpur\r\nSri Ganganagar', 1, 1),
(40, 15, 'Ajeet Jain S/o IP Jain', NULL, NULL, 'ajeetjain12@gmail.com', '7597300330', 'Abu Raod', 'Hotel Sigma, Vishnu Dharmsala Road\r\nAbu Raod\r\nDist:- Sirohi', 1, 1),
(41, 15, 'State Bank of India', NULL, NULL, 'sbi@co.in', '9929109149', 'Jaipur', 'State Bank of India\r\nLocal Head Office,Tilak Marg\r\nJaipur (Raj)', 2, 1),
(42, 15, 'Central Bureau of Investigation', NULL, NULL, 'cbi@gmail.com', NULL, 'Jodhpur', 'Central Bureau of Investigation\r\nLal Sagar\r\nJodhpur', 2, 1),
(43, 18, 'HINDUSTAN ZINC LTD.', NULL, NULL, 'vineet.bose@vedanta.co.in', NULL, 'UDAIPUR', 'YASHADBHAWAN, SWAROOP SAGAR,', 2, 1),
(44, 4, 'Sneha', NULL, 'TR', 'sneha45798@gmail.com', NULL, 'Jodhpur', '23', 1, 1),
(45, 19, 'A', NULL, 'A', 'admin@gmail.com', '0', NULL, NULL, 1, 1),
(46, 19, 'a', NULL, 'A', 'admin@gmail.com', '0', NULL, NULL, 2, 0),
(47, 18, 'DHEERAJ', NULL, NULL, NULL, '7877325464', NULL, NULL, 1, 1),
(48, 22, 'Suresh Chandra', NULL, 'Nand Lal', 'suresh@gmail.com', '9982618900', 'Chittorgarh', 'Begun, Tehsil: Begun', 1, 1),
(49, 22, 'Devi Singh', NULL, 'Hajar Singh', 'devi@gmail.com', '9414534202', 'Jalore', 'Badi Pole Ke Bahar, Tilakdwar Road', 1, 1),
(50, 22, 'Kamal Narayan', NULL, 'Kan Kamal Agrawal', 'kamal@gmail.com', '9413356250', 'Bhilwara', 'Bhadada Mohalla, Ramji Mod Ki Gali', 1, 1),
(51, 22, 'Pramod Kumar', NULL, 'Omprakash', 'pramod@gmail.com', '9928373856', 'Jaipur', 'Plot No 1/3, Krishna Vihar,\r\n\r\nNear Railway Station', 1, 1),
(52, 22, 'Shiv Shankar', NULL, 'Jeevan Ram Vyas', 'shiv@gmail.com', '9414306952', 'Chittorgarh', '118, Meera Nagar,', 1, 1),
(53, 22, 'Gajanand Badodiya', NULL, 'Suwa Lal Badodiya', 'gajaand@gmail.com', '9829095891', 'Bhilwara', 'Ganga Chowk, Sanganer\r\n\r\n\r\nTehsil & Dist Bhilwara', 1, 1),
(54, 22, 'CBI - Central Bureau of Investigation', NULL, NULL, 'cbi@co.in', '9950718980', 'Jodhpur', 'Near Gulab Sagar', 2, 1),
(55, 22, 'State Bank of India', NULL, NULL, 'sbi.com', '7737388902', 'Jodhpur', 'A-21, Zonal Office\r\n\r\n\r\nShastri Nagar', 2, 1),
(56, 22, 'fggf', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1),
(57, 22, 'ram', NULL, 'shyam', NULL, NULL, NULL, NULL, 1, 1),
(58, 22, 'test', NULL, NULL, NULL, NULL, NULL, NULL, 2, 1),
(59, 22, 'YKP', NULL, 'MSP', 'contactyogi41@gmail.com', '7877325464', 'Jodhpur', NULL, 1, 1),
(60, 22, 'Yogesh Kumar', NULL, 'Madhusudhan', 'yogeshmdpurohit@gmail.com', '7877325464', 'Jodhpur', '111-K, Chopasani Housing Board\r\n\r\n\r\nJodhpur', 1, 1),
(61, 23, 'Aastha Singh Jadon', 'W/O', 'Navneet Singh jadon', 'aastha@gmail.com', '9847563210', 'Lucknow', 'Lucknow', 1, 1),
(62, 23, 'Tamanna Bhatia', 'S/O', 'Guru Nanak', 'tb@gmail.com', '9784653210', 'Ajmer', 'Ajmer', 2, 1),
(63, 23, 'PS', 'W/O', 'KB', 'kb@gmail.com', '9245988122', 'Jodhpur', 'WsCube Tech 1st Floor Laxmi Tower\r\nBhaskar Circle Ratanada', 1, 1),
(64, 4, 'Yogesh Kumar', 'S/O', 'Madhusudhan', 'yogeshmdpurohit@gmail.com', '7877325464', 'Jodhpur', '111, K Near Bohra Colony\r\nRatanada, Jodhpur', 1, 1),
(65, 4, 'A', 'S/O', 'B', 'aditya@gmail.com', '7597037331', 'Jodhpur', 'Jodhpur', 1, 1),
(66, 4, 'Aashkaran', 'S/O', 'Hari Shankar', 'ashkaran@gmail.com', '1234567890', 'BIKANER', 'Mahabali Puram Colony\r\nNokha Road, Ganga Shahar\r\nBIKANER', 2, 1),
(67, 29, 'Vijay Ltd', 'S/O', 'Mohan', 'jone.boaz@gmail.com', '9782608731', 'jodhpur', 'jodhpur', 2, 1),
(68, 30, 'STech Pvt Ltd', 'S/O', 'Mohan', 'jone.boaz@gmail.com', '9782833224', 'jodhpur', 'Paota Jodhpur', 2, 1),
(69, 30, 'STech Pvt Ltd', 'S/O', 'Sjone', 'sumit.v2rsolution@gmail.com', '9782608731', 'jodhpur', 'Paota Jodhpur', 1, 1),
(70, 31, 'Suresh Chandra', 'S/O', 'Nand Lal', 'sureshchandra@gmail.com', '7877325464', 'Chittorgarh', 'Begun, Tehsil: Begun', 1, 1),
(71, 31, 'Devi Singh', 'S/O', 'Hajar Singh', 'devi@gmail.com', '7877325464', 'Jalore', 'Badi Pole Ke Bahar, Tilak Dwar Road', 1, 1),
(72, 31, 'Anju Manwani', 'W/O', 'Sudhir Manwani', 'anju@gmail.com', '7877325464', 'Bhilwara', '184, Sindh Nagar', 1, 1),
(73, 31, 'Ravindra Singh', 'S/O', 'Kuldeep Singh', 'ravi@gmail.com', '7877325464', 'Sriganganagar', 'Ward No 20, PS Anupgarh\r\nDist Sriganganagar', 1, 1),
(74, 31, 'Suneet Karnawat', 'S/O', 'Mahendra Karnawat', 'suneet@gmail.com', '7877325464', 'Jaipur', 'House No 4679, Vijay Bhawan, Opposite PUrani Kotawali\r\nJohari Bazar', 1, 1),
(75, 31, 'Jitendra Bohra', 'C/O', 'Jethmal Bohra', 'Jitendra@gmail.com', '7877325464', 'Jodhpur', 'Abha Set 1st D Road, Plot No 889,\r\nSardarpura', 1, 1),
(76, 31, 'Rekha Sirohi', 'D/O', 'Prem Singh', 'rekha@gmail.com', '7877325464', 'Bikaner', 'Jeengar Gurisaiya Ka Mohalla\r\nModi Furniture, Near Kumkum Beauty Parlor,\r\nFad Bazar', 1, 1),
(77, 31, 'Central Bureau of Investigation', 'S/O', NULL, NULL, '7877325464', 'Jodhpur', 'Near Gokulji Ji Pyau\r\nLal Sagar', 2, 1),
(78, 31, 'State Bank of India', 'Thr/O', 'Sanjeev Mathur', 'sbi@gmail.com', '7877325464', 'Jodhpur', 'Zonal Office, Shastri Nagar', 2, 1),
(79, 30, 'sadsad', NULL, '', 'sumit.v2rsolution@gmail.com', '9632587410', 'jodhpur', 'Paota Jodhpur', 1, 1),
(80, 33, 'Hindustan Zinc Ltd', 'Thr/O', 'Mr Vineet Bose', NULL, '9116001517', 'Udaipur', 'Yashad Bhawan, Swaroop Sagar,', 1, 1),
(81, 33, 'State Bank of India', NULL, '', NULL, NULL, NULL, NULL, 2, 1),
(82, 33, 'Mazhar Mohd Sheikh', 'S/O', 'Sattar Mohammad', NULL, '8233038017', 'Banswara', 'Shri Ram Colony, Dahad Road', 1, 1),
(83, 33, 'Ganga Ram Dangi', 'S/O', 'Uda Dangi', NULL, NULL, 'Udaipur', 'Mokal Bavari, Sukher', 1, 1),
(84, 33, 'Central Bureau of Investigation', NULL, '', NULL, NULL, 'Jodhpur', 'Near Gokul Ji Ki Pyao, Magra Punjla, Lal Sagar,', 2, 1),
(85, 33, 'Paras Mal Daiya', NULL, '', NULL, '9460406872', 'Nagaur', 'Darjiyo Ka Mohalla,\r\nMerta City,', 1, 1),
(86, 33, 'Girdhari Lal', 'S/O', 'Shubh Karan', NULL, NULL, 'Churu', 'Ratangarh,', 1, 1),
(87, 33, 'Ganapat Lal', 'S/O', 'Mohan Lal', NULL, NULL, 'Pali', 'Mundra, Tehsil:- Bali,', 1, 1),
(88, 33, 'Khaitan Business', 'C/O', 'Baijnath Chouhan', NULL, '9414171157', 'Rajsamand', '67, Opposite Old Bus Stand, Nathdwara', 1, 1),
(89, 33, 'Oil & Natural Gas Corporation Ltd', NULL, '', NULL, NULL, 'Jodhpur', 'KDM Complex, Mandore Road,', 2, 1),
(90, 33, 'Tulsi Amrit Niketan Samiti', 'Thr/O', 'Shanti Chandra S/o Takhat Mal- Manager', NULL, NULL, 'Udaipur', 'Sadar Bazar, Kanod, Tehsil:- Bhinder,', 1, 1),
(91, 33, 'Mahaveer Prasad', 'S/O', 'Shyam Lal', NULL, '9414682934', 'Chittorgarh', 'Dungala.', 1, 1),
(92, 33, 'Dist. & Session Judge-Pali', 'Thr/O', 'Rameshwar Vyas', NULL, NULL, 'Pali', 'District & Session Court,', 1, 1),
(93, 33, 'Lalita', 'W/O', 'Prakash Singh', NULL, '8905782479', 'Chittorgarh', 'Kalyanpura PS Badi Sadari,', 1, 1),
(94, 33, 'District Excise Office', 'Thr/O', 'DEO(Prosecution)', NULL, '9828891410', 'Jodhpur', 'District Excise Office, Paota Circle,', 1, 1),
(95, 39, 'Mahipal Singh', 'S/O', 'Puna Ram Jat', 'yogeshmdpurohit@gmail.com', '7877325464', 'Nagaur', 'Ramnagar, Kaswa Ki Dhani, \r\nTehsil:- Merta,', 1, 1),
(96, 39, 'Harish Chandra', 'S/O', 'Khima Ram Gehlot', 'yogeshmdpurohit@gmail.com', '9950718980', 'Sirohi', 'Street No 6, Raichand Colony, \r\nTehsil Road, Chawani, Sheoganj,', 1, 1),
(97, 39, 'Animal Aid Charitable Trust', 'Thr/O', 'Ramgopal Kumawat- Secretary', 'contactyogi41@gmail.com', '7877325464', 'Udaipur', 'Village:- Badi, TB Hospital,', 1, 1),
(98, 39, 'Ghisi', 'Thr/O', 'Badar Khan', 'contactyogi41@gmail.com', '9950718980', 'Bhilwara', 'Nandshah (K),\r\nTehsil:- Sahada,', 1, 1),
(99, 39, 'Mithliesh Sharma', 'S/O', 'Kailash Sharma', 'conactyogi@gmail.com', '7877325464', 'Alwar', '85, Sanjay Colony,', 1, 1),
(100, 39, 'Dist Excise Office', 'Thr/O', 'Mr Noor Mohd- DEO(Prosecution)', 'contactyogi41@gmail.com', '9950718980', 'Jodhpur', 'Paota Circle, Behind Rajasthan High Court Campus,', 1, 1),
(101, 39, 'Amit Agrawal', 'S/O', 'Laxmi Narayan Agrawal', 'yogeshmdpurohit@gmail.com', '7877325464', 'Bhilwara', 'Chamunda Tap Nirodhak Pvt Ltd,\r\nAjmer Road,', 1, 1),
(102, 39, 'Vinita Daiya', 'D/O', 'Ashok Solanki', 'yogeshmdpurohit@gmail.com', '7877325464', 'Jodhpur', '9/32, Chopasani Housing Board,', 1, 1),
(103, 39, 'Aditya Agrawal', 'S/O', 'KC Agrawal', 'yogeshmdpurohit@gmail.com', '787325464', 'Jaipur', 'Vice President- Rajwest Power Ltd,\r\nRajmahal Palace, Sardar Patel Marg,', 1, 1),
(104, 39, 'Jagdish', 'S/O', 'Sona Ram', 'contactyogi41@gmail.com', '9950718980', 'Jodhpur', 'Bhadado Ka Bas, Pal,', 1, 1),
(105, 39, 'S.N. Sharma', 'S/O', 'Kanhaiya Lal Sharma', 'contactyogi41@gmail.com', '7877325464', 'Udaipur', '55, Laxmi Nagar,Tehsil:- Girwa,', 1, 1),
(106, 39, 'Rajeev Singh Kachhawa', 'S/O', 'Vinay Kumar Kachhawa', 'yogeshmdpurohit@gmail.com', '9950718980', 'Udaipur', '20, Panchwati Road No-2,', 1, 1),
(107, 40, 'RAMESH PUROHIT', 'S/O', 'SURESH PUROHIT', 'rameshpurohit@gmail.com', '7737388902', 'JODHPUR', 'JODHPUR AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA', 1, 1),
(108, 40, 'STATE BANK OF INDIA', NULL, '', 'sbi@gmail.com', '7737388902', 'JODHPUR', 'JODHPUR', 2, 1),
(109, 33, 'Amba', 'S/O', 'Magni Ram', NULL, NULL, 'Udaipur', '9, Nalwaya Chowk, Dhanmandi,', 1, 1),
(110, 33, 'Kamal Sharma', 'W/O', 'Ghanshyam  Das Sharma', NULL, NULL, 'Sriganganagar', 'Shankar Oil Mills, Ward No 9, Opposite Public Park\r\nPadampur,', 1, 1),
(111, 33, 'Gautam Chand Jain', 'Thr/O', 'RK Industries', NULL, NULL, 'Jodhpur', 'RK Industries, G-184, Manodre Industrial Area,\r\nNagaur Road,', 1, 1),
(112, 33, 'Kheta Bhai', 'S/O', 'Late Jasdan @ Jaskaran', NULL, '8005703205', 'Pali', 'Village: Kushalpura, Tehsil: Raipur,', 1, 1),
(113, 33, 'Hemraj', 'S/O', 'Champa Lal', NULL, NULL, 'Udaipur', 'Savina Main Road, Tehsil: Girwa,', 1, 1),
(114, 33, 'Shyam Lal', 'S/O', 'Mohan Lal', NULL, NULL, 'Chittorgarh', 'Dungala, PS Dungala,', 1, 1),
(115, 33, 'Devendra Daga', 'S/O', 'Padam Chand Daga', NULL, '9799441122', 'Pali', 'A-33, Veer Durgadas Nagar,', 1, 1),
(116, 33, 'Pawan Kumar', 'S/O', 'Mohan Lal', NULL, NULL, 'Churu', 'Ward No 10, Swarnkar Colony, Ratangarh,', 1, 1),
(117, 33, 'Shamim Bi', 'W/O', 'Sohrab Khan', NULL, NULL, 'Banswara', 'Gora Khimly,', 1, 1),
(118, 33, 'Narbada Shankar', 'S/O', 'Heera Lal', NULL, '9001210055', 'Rajsamand', 'Pipli Acharyan, Tehsil & Dist: Rajsamand,', 1, 1),
(119, 33, 'Paras Kumar', 'S/O', 'Shiv Kumar', NULL, NULL, 'Udaipur', 'Near Police Station, Bapu Bazar,\r\nRishabhdev,', 1, 1),
(120, 33, 'Narayan Lal @ Kalu Bhai', 'S/O', 'Bhanwar Lal', NULL, '9413261048', 'Pali', '1st Floor, 10 Khopra Katla,', 1, 1),
(121, 33, 'Pankaj Jain', 'S/O', 'Shankar Lal', NULL, '9829789308', 'Udaipur', '87, Sarv Ritu Vilash,', 1, 1),
(122, 33, 'Mukesh', 'S/O', 'Bhanwar', NULL, NULL, 'Rajsamand', 'Panchratan Complex, Kankaroli,', 1, 1),
(123, 33, 'OP Sharma', 'Thr/O', 'RIDCOR', NULL, '9413388917', 'Jaipur', 'IL & FS Transportation Networks Limited\r\n1st Floor, Jeevan Needhi Buliding,\r\nAmbedkar Circle, Bhawani Singh Marg,', 1, 1),
(124, 33, 'Rajasthan High Court Jodhpur', NULL, '', NULL, NULL, NULL, NULL, 1, 1),
(125, 4, 'Hindustan Zinc Ltd', 'Thr/O', 'Mr Vineet Bose', NULL, '7877325464', 'Udaipur', 'Hindustan Zinc Ltd\r\nSwaroop Sagar,', 2, 1),
(126, 4, 'Rajveer Parwa', 'S/O', 'Manmohan Singh', 'abc@gmail.com', '7877325464', 'Phalodi', '572/P, Shastri Nagar, Near Vyas Circle', 1, 1),
(127, 4, 'Hari Prakash', 'S/O', 'Ram Prakash', 'hari@gmail.com', '9950718980', 'Jodhpur', '111-5(A) Near Railway Station Jodhpur', 1, 1),
(128, 4, 'DHEERAJ', 'S/O', 'MADHUSUDAN', 'DHEERAJBPUROHIT', '7737388902', 'JODHPUR', 'CHB JODHPURRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR', 1, 1),
(129, 4, 'IDEAL LAWYER', NULL, '', NULL, NULL, NULL, NULL, 2, 1),
(130, 4, 'Shivay Purohit', 'S/O', 'Yogesh Purohit', NULL, '7877325464', 'Jodhpur', '111-K, Near Vishkarma Boarding,\r\nPhaodi,', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `adv_compliance`
--

CREATE TABLE `adv_compliance` (
  `compliance_id` int(11) NOT NULL,
  `user_admin_id` int(11) NOT NULL,
  `compliance_court_type` int(11) DEFAULT NULL,
  `compliance_court_name` int(11) DEFAULT NULL,
  `compliance_category` int(11) NOT NULL COMMENT 'There is case category  1 - Criminal 2 - Civil',
  `compliance_case_type` int(11) DEFAULT NULL,
  `compliance_case_id` int(11) NOT NULL,
  `compliance_type_notice` int(11) DEFAULT NULL,
  `compliance_type_date` int(11) DEFAULT NULL,
  `compliance_type_defect_case` int(11) DEFAULT NULL,
  `compliance_type_reply` int(11) DEFAULT NULL,
  `compliance_type_certified` int(11) DEFAULT NULL,
  `compliance_type_other` int(11) DEFAULT NULL,
  `notice_issue_date` date DEFAULT NULL,
  `notice_status` int(11) DEFAULT NULL,
  `notice_status_date` date DEFAULT NULL,
  `compliance_date_type` int(11) DEFAULT NULL,
  `compliance_date` date DEFAULT NULL,
  `compliance_status` int(11) DEFAULT NULL,
  `compliance_date_status` date DEFAULT NULL,
  `compliance_act_reason` longtext,
  `compliance_defect_status` int(11) DEFAULT NULL,
  `compliance_defect_date_status` date DEFAULT NULL,
  `compliance_reply` longtext,
  `compliance_reply_status` int(11) DEFAULT NULL,
  `compliance_reply_date_status` date DEFAULT NULL,
  `certified_copy_date` date DEFAULT NULL,
  `certified_copy_status` int(11) DEFAULT NULL,
  `certified_copy_date_status` date DEFAULT NULL,
  `compliance_other` longtext,
  `compliance_other_status` int(11) DEFAULT NULL,
  `compliance_other_date_status` date NOT NULL,
  `compliance_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adv_compliance`
--

INSERT INTO `adv_compliance` (`compliance_id`, `user_admin_id`, `compliance_court_type`, `compliance_court_name`, `compliance_category`, `compliance_case_type`, `compliance_case_id`, `compliance_type_notice`, `compliance_type_date`, `compliance_type_defect_case`, `compliance_type_reply`, `compliance_type_certified`, `compliance_type_other`, `notice_issue_date`, `notice_status`, `notice_status_date`, `compliance_date_type`, `compliance_date`, `compliance_status`, `compliance_date_status`, `compliance_act_reason`, `compliance_defect_status`, `compliance_defect_date_status`, `compliance_reply`, `compliance_reply_status`, `compliance_reply_date_status`, `certified_copy_date`, `certified_copy_status`, `certified_copy_date_status`, `compliance_other`, `compliance_other_status`, `compliance_other_date_status`, `compliance_created`) VALUES
(1, 4, NULL, NULL, 1, 1, 29, 1, 1, 1, 1, 1, 1, '2018-06-16', 2, '2018-07-20', 1, '2018-06-16', 2, '0000-00-00', NULL, 2, '0000-00-00', 'sdgasdfsdf', 2, '0000-00-00', '2018-06-15', 2, '0000-00-00', 'sdgadsgfsag', 2, '0000-00-00', '2018-07-17 08:15:47'),
(2, 4, NULL, NULL, 1, 1, 29, 1, 1, 1, 1, 1, 1, '2018-06-23', 2, '2018-07-24', 1, '1970-01-01', 2, '2018-07-24', NULL, 2, '2018-07-24', NULL, 2, '2018-07-24', '1970-01-01', 2, '2018-07-24', NULL, 2, '2018-07-24', '2018-07-17 08:15:47'),
(4, 4, NULL, NULL, 2, 6, 28, 1, 1, 1, NULL, NULL, NULL, '0001-11-13', 1, '1970-01-01', 1, '2018-06-08', 1, '1970-01-01', NULL, 1, '1970-01-01', NULL, NULL, '1970-01-01', '0001-11-30', NULL, '1970-01-01', NULL, NULL, '1970-01-01', '2018-07-17 08:15:47'),
(5, 4, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, NULL, 1, '1970-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, NULL, NULL, NULL, '0000-00-00', '2018-07-17 08:15:47'),
(6, 4, NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, NULL, 1, '1970-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, NULL, NULL, NULL, '0000-00-00', '2018-07-17 08:15:47'),
(7, 4, NULL, NULL, 1, 1, 32, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, NULL, 1, '1970-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, NULL, NULL, NULL, '0000-00-00', '2018-07-17 08:15:47'),
(8, 4, NULL, NULL, 1, 1, 29, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, NULL, 1, '1970-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, NULL, NULL, NULL, '0000-00-00', '2018-07-17 08:15:57'),
(9, 4, NULL, NULL, 1, 1, 29, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, NULL, 1, '1970-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, NULL, NULL, NULL, '0000-00-00', '2018-07-24 08:17:59'),
(10, 4, NULL, NULL, 1, 1, 29, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, NULL, 1, '1970-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, NULL, NULL, NULL, '0000-00-00', '2018-07-24 08:18:06'),
(11, 4, NULL, NULL, 2, 17, 19, NULL, NULL, NULL, 1, NULL, NULL, '1970-01-01', NULL, NULL, 1, '1970-01-01', NULL, NULL, NULL, NULL, NULL, 'Reply to be filed', 2, '2018-08-01', '1970-01-01', NULL, NULL, NULL, NULL, '0000-00-00', '2018-07-30 14:41:46'),
(12, 4, NULL, NULL, 1, 18, 14, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, '2018-08-01', 1, '1970-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, NULL, NULL, NULL, '0000-00-00', '2018-08-01 05:11:02'),
(13, 4, NULL, NULL, 2, 15, 16, 1, NULL, NULL, NULL, 1, NULL, '2018-09-04', 2, '2018-09-02', 1, '1970-01-01', NULL, '2018-09-03', NULL, NULL, '2018-09-03', NULL, NULL, '2018-09-03', '2018-08-01', 2, '2018-08-01', NULL, NULL, '0001-11-30', '2018-08-01 05:24:24'),
(14, 14, NULL, NULL, 1, 21, 22, 1, NULL, NULL, NULL, NULL, NULL, '2018-08-31', 2, '2018-08-02', 2, '1970-01-01', NULL, '2018-08-02', NULL, NULL, '2018-08-02', NULL, NULL, '2018-08-02', '1970-01-01', NULL, NULL, NULL, NULL, '0000-00-00', '2018-08-02 06:57:20'),
(15, 14, NULL, NULL, 1, 21, 24, 1, 1, 1, 1, 1, 1, '2018-08-10', NULL, NULL, 1, '2018-08-02', 1, NULL, NULL, 1, NULL, '10 August 2018', 1, NULL, '2018-08-09', 1, NULL, '10 August 2018', 2, '0000-00-00', '2018-08-02 09:21:20'),
(17, 15, NULL, NULL, 2, 27, 35, NULL, 1, 1, 1, NULL, NULL, '1970-01-01', NULL, '2018-08-10', 1, '2018-08-11', 2, '2018-08-10', NULL, 1, NULL, NULL, 1, NULL, '1970-01-01', NULL, NULL, NULL, NULL, '0000-00-00', '2018-08-10 07:41:50'),
(18, 16, NULL, NULL, 2, 26, 32, NULL, NULL, NULL, NULL, 1, NULL, '1970-01-01', NULL, NULL, 1, '1970-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-31', 1, NULL, NULL, NULL, '0000-00-00', '2018-08-13 04:51:35'),
(19, 15, NULL, NULL, 2, 27, 42, 1, 1, 1, 1, 1, 1, '2018-08-01', 2, '2018-08-16', 2, '2018-08-17', 2, '2018-08-13', NULL, 2, '2018-08-12', 'Reply to be filed', 2, '2018-08-15', '2018-08-16', 2, '2018-08-13', 'Title Copy', 2, '2018-08-15', '2018-08-16 07:11:55'),
(20, 16, 2, NULL, 1, 25, 76, 1, 1, 1, 1, 1, 1, '2018-09-30', 1, '1970-01-01', 2, '2019-09-30', 1, '1970-01-01', NULL, 1, '1970-01-01', 'Test', 1, '1970-01-01', '2019-08-31', 1, '1970-01-01', 'Test', 1, '1970-01-01', '2018-09-06 06:25:39'),
(21, 19, 1, NULL, 1, 61, 268, 1, 1, 1, 1, 1, 1, '2018-09-08', 2, '2018-09-30', 1, '2018-09-28', 2, '2018-09-30', NULL, 2, '2018-09-08', 'Test', 2, '2018-09-08', '2018-10-19', 1, '1970-01-01', 'Test', 1, '1970-01-01', '2018-09-08 11:38:43'),
(22, 22, 2, NULL, 2, 67, 293, 1, 1, 1, 1, 1, 1, '2018-09-01', 1, '1970-01-01', 1, '2018-09-05', 1, '1970-01-01', NULL, 1, '1970-01-01', 'Reply to be filed', 1, '1970-01-01', '2018-09-05', 1, '1970-01-01', 'REJOINDER', 1, '1970-01-01', '2018-09-10 12:28:30'),
(23, 22, NULL, NULL, 1, 65, 297, 1, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, '1970-01-01', 1, '1970-01-01', NULL, '1970-01-01', NULL, NULL, '1970-01-01', NULL, NULL, '1970-01-01', '1970-01-01', NULL, '1970-01-01', NULL, NULL, '1970-01-01', '2018-09-19 11:08:31'),
(24, 22, NULL, NULL, 1, 64, 298, NULL, NULL, 1, 1, NULL, NULL, '1970-01-01', NULL, '1970-01-01', 1, '1970-01-01', NULL, '1970-01-01', NULL, NULL, '1970-01-01', NULL, NULL, '1970-01-01', '1970-01-01', NULL, '1970-01-01', NULL, NULL, '1970-01-01', '2018-09-19 11:09:15'),
(25, 22, NULL, NULL, 1, 65, 311, NULL, 1, 1, NULL, NULL, NULL, '1970-01-01', NULL, '1970-01-01', 1, '2018-09-18', 2, '2018-09-19', NULL, NULL, '1970-01-01', NULL, NULL, '1970-01-01', '1970-01-01', NULL, '1970-01-01', NULL, NULL, '1970-01-01', '2018-09-19 11:10:33'),
(26, 22, 1, NULL, 2, NULL, 295, 1, 1, 1, 1, 1, 1, '2018-09-26', 2, '2018-10-01', 2, '2018-09-26', 1, '1970-01-01', NULL, 1, '1970-01-01', NULL, 1, '1970-01-01', '2018-09-26', NULL, '1970-01-01', NULL, 1, '1970-01-01', '2018-09-19 11:11:25'),
(27, 4, 2, 12, 1, NULL, 16, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, '1970-01-01', 1, '1970-01-01', NULL, '1970-01-01', NULL, NULL, '1970-01-01', NULL, NULL, '1970-01-01', '1970-01-01', NULL, '1970-01-01', NULL, NULL, '1970-01-01', '2018-11-09 09:54:34'),
(28, 29, 2, 50, 1, 75, 490, 1, 1, 1, 1, 1, 1, '2018-12-15', 1, '1970-01-01', 1, '2018-12-15', 1, '1970-01-01', NULL, 1, '1970-01-01', 'test', 1, '1970-01-01', '2018-12-15', 1, '1970-01-01', 'test', 1, '1970-01-01', '2018-12-14 13:54:45'),
(29, 4, 2, 12, 1, NULL, 492, 1, NULL, 1, NULL, NULL, NULL, '2018-12-19', 1, '1970-01-01', 1, '1970-01-01', NULL, '1970-01-01', NULL, 2, '2018-12-18', NULL, NULL, '1970-01-01', '1970-01-01', NULL, '1970-01-01', NULL, NULL, '1970-01-01', '2018-12-18 12:03:15'),
(30, 30, 1, NULL, 1, NULL, 493, NULL, NULL, NULL, NULL, NULL, NULL, '1970-01-01', NULL, '1970-01-01', 1, '1970-01-01', NULL, '1970-01-01', NULL, NULL, '1970-01-01', NULL, NULL, '1970-01-01', '1970-01-01', NULL, '1970-01-01', NULL, NULL, '1970-01-01', '2018-12-18 13:50:13'),
(31, 31, 1, 58, 0, 85, 496, 1, 1, 1, NULL, NULL, NULL, '2018-12-01', 2, '2019-04-11', 1, '2018-12-01', 2, '2019-04-11', NULL, 2, '2019-04-11', NULL, NULL, '1970-01-01', '1970-01-01', NULL, '1970-01-01', NULL, NULL, '1970-01-01', '2018-12-30 06:18:24'),
(32, 31, 2, 56, 0, 85, 574, 1, NULL, NULL, NULL, NULL, NULL, '2019-04-01', 2, '2019-04-02', 1, '1970-01-01', NULL, '1970-01-01', NULL, NULL, '1970-01-01', NULL, NULL, '1970-01-01', '1970-01-01', NULL, '1970-01-01', NULL, NULL, '1970-01-01', '2019-04-11 11:44:40'),
(33, 31, 2, 56, 0, 85, 501, 1, NULL, 1, 1, 1, 1, '2019-04-01', 2, '2019-04-11', 1, '1970-01-01', NULL, '1970-01-01', NULL, 2, '2019-04-09', 'Reply to be filed', 2, '2019-04-08', '2019-04-01', 2, '2019-04-15', 'Fee is not Received', 2, '2019-04-11', '2019-04-11 11:50:30'),
(34, 4, 2, 12, 0, 15, 16, 1, NULL, 1, NULL, 1, 1, '2019-08-01', 1, '1970-01-01', 1, '1970-01-01', NULL, '1970-01-01', NULL, 1, '1970-01-01', NULL, NULL, '1970-01-01', '2019-08-02', 1, '1970-01-01', 'Rejoinder Not filed', 1, '1970-01-01', '2019-08-26 06:11:33'),
(35, 4, 2, 12, 0, 16, 27, 1, NULL, NULL, NULL, 1, NULL, '2019-08-05', 1, '1970-01-01', 1, '1970-01-01', NULL, '1970-01-01', NULL, NULL, '1970-01-01', NULL, NULL, '1970-01-01', '2019-08-15', 1, '1970-01-01', NULL, NULL, '1970-01-01', '2019-08-26 06:13:33'),
(36, 4, 2, 12, 0, 5, 582, 1, 1, NULL, NULL, 1, NULL, '2019-08-25', 1, '1970-01-01', 1, '2019-08-25', 1, '1970-01-01', NULL, NULL, '1970-01-01', NULL, NULL, '1970-01-01', '2019-08-15', 1, '1970-01-01', NULL, NULL, '1970-01-01', '2019-09-01 09:16:05'),
(37, 4, 2, 12, 0, 16, 595, 1, 1, NULL, NULL, 1, NULL, '2019-08-12', 2, '2019-08-15', 1, '2019-08-20', 1, '1970-01-01', NULL, NULL, '1970-01-01', NULL, NULL, '1970-01-01', '2019-08-20', 1, '1970-01-01', NULL, NULL, '1970-01-01', '2019-09-01 09:16:51');

-- --------------------------------------------------------

--
-- Table structure for table `adv_comp_date`
--

CREATE TABLE `adv_comp_date` (
  `cdate_id` int(11) NOT NULL,
  `user_admin_id` int(11) DEFAULT NULL,
  `cstatus_date` varchar(255) DEFAULT NULL,
  `cdate_pic` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cdate_case_no` varchar(255) DEFAULT NULL,
  `cdate_title` varchar(255) DEFAULT NULL,
  `cdate_type` tinyint(2) DEFAULT NULL COMMENT '2=short date,1=long date',
  `cdate_date` date DEFAULT NULL,
  `cdate_status` tinyint(4) NOT NULL COMMENT '0=Inactive,1=Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `adv_comp_defect_case`
--

CREATE TABLE `adv_comp_defect_case` (
  `defect_id` int(11) NOT NULL,
  `defect_status_date` varchar(255) DEFAULT NULL,
  `defect_date_pic` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_admin_id` int(11) DEFAULT NULL,
  `defect_case_no` varchar(255) DEFAULT NULL,
  `defect_title` varchar(255) DEFAULT NULL,
  `defect_reason` varchar(255) DEFAULT NULL,
  `defect_status` tinyint(2) NOT NULL COMMENT '0=Inactive,1=Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `adv_comp_notice`
--

CREATE TABLE `adv_comp_notice` (
  `notice_id` int(11) NOT NULL,
  `user_admin_id` int(11) DEFAULT NULL,
  `notice_status_date` varchar(255) DEFAULT NULL COMMENT '2=complete,1=pending',
  `notice_date_pic` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `notice_case_no` varchar(255) DEFAULT NULL,
  `notice_title` varchar(255) DEFAULT NULL,
  `notice_issue_date` date DEFAULT NULL,
  `notice_status` tinyint(2) NOT NULL COMMENT '0=Inactive,1=Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `adv_comp_reply`
--

CREATE TABLE `adv_comp_reply` (
  `reply_id` int(11) NOT NULL,
  `user_admin_id` int(11) DEFAULT NULL,
  `reply_status_date` varchar(255) DEFAULT NULL,
  `reply_date_pic` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reply_case_no` varchar(255) DEFAULT NULL,
  `reply_title` varchar(255) DEFAULT NULL,
  `reply_desc` varchar(255) DEFAULT NULL,
  `reply_status` tinyint(2) NOT NULL COMMENT '0=Inactive,1=Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `adv_courselist_in_high_court`
--

CREATE TABLE `adv_courselist_in_high_court` (
  `list_id` int(11) NOT NULL,
  `course_case_reg` int(11) NOT NULL,
  `list_user_id` int(11) NOT NULL,
  `list_previous_date` date NOT NULL,
  `list_reg_stage` int(11) NOT NULL,
  `list_justice_id` int(11) NOT NULL,
  `list_court_no` varchar(255) NOT NULL,
  `list_serial_no` varchar(255) NOT NULL,
  `list_page_no` varchar(255) NOT NULL,
  `list_status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '0=Inactive,1=Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `adv_court`
--

CREATE TABLE `adv_court` (
  `court_id` int(11) NOT NULL,
  `user_admin_id` int(11) DEFAULT NULL,
  `court_type` tinyint(2) DEFAULT NULL COMMENT '2=High Court,1=Trial Court	',
  `court_name` varchar(50) DEFAULT NULL,
  `court_short_name` varchar(50) DEFAULT NULL,
  `court_status` tinyint(2) NOT NULL COMMENT '0=Inactive,1=Active	'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adv_court`
--

INSERT INTO `adv_court` (`court_id`, `user_admin_id`, `court_type`, `court_name`, `court_short_name`, `court_status`) VALUES
(1, 6, 1, 'Jodhpur trial one', 'JTONE', 1),
(2, 6, 2, 'Jodhpur HC', 'JODHC', 1),
(3, 6, 2, 'Jaipur HC', 'JAIHO', 0),
(4, 4, 1, 'Jodhpur', 'JHC', 0),
(5, 9, 1, 'Ad test', 'AD', 1),
(6, 9, 1, 'Ad test', 'Ad', 1),
(7, 9, 2, 'Ad test', 'AD', 1),
(8, 4, 1, 'Test one', 'TE', 1),
(9, 4, 2, 'Test two', 'TE', 1),
(10, 8, 1, 'Test one', 'TO', 1),
(11, 8, 2, 'Test two', 'TT', 1),
(12, 4, 2, 'RHC- Jodhpur', NULL, 1),
(13, 4, 2, 'RHC-Jaipur', NULL, 1),
(14, 4, 1, 'Family Court', NULL, 1),
(15, 4, 1, 'Labour Court', NULL, 1),
(16, 4, 1, 'AMJMT', NULL, 1),
(17, 14, 1, 'Test', NULL, 1),
(18, 14, 2, 'Test', NULL, 1),
(22, 15, 1, 'District & Session Judge-Jodhpur', NULL, 1),
(23, 16, 2, 'Trial Court', NULL, 1),
(24, 16, 1, 'High Court', NULL, 1),
(25, 15, 2, 'Rajasthan High Court- Jaipur', NULL, 1),
(26, 15, 2, 'Rajasthan High Court- Jodhpur', NULL, 1),
(27, 15, 1, 'Family Court-1', NULL, 1),
(28, 15, 1, 'ACJ Court- Jodhpur', NULL, 1),
(29, 15, 1, 'AMJM Court Jodhpur', NULL, 1),
(30, 15, 1, 'District Court Jodhpur-Metro', NULL, 1),
(31, 16, 2, 'Rajasthan High Court', NULL, 1),
(32, 16, 1, 'Haryana High Court', NULL, 1),
(33, 18, 2, 'RAJASTHAN HIGH COURT JODHPUR', NULL, 1),
(35, 18, 2, 'RAJASTHAN HIGH COURT BENCH JAIPUR', NULL, 1),
(36, 18, 1, 'FAMILY COURT', NULL, 1),
(37, 18, 1, 'BOARD OF REVENUE AJMER', NULL, 1),
(38, 18, 1, 'LABOUR COURT, PALI', NULL, 1),
(39, 19, 1, 'ABC', NULL, 1),
(40, 19, 2, 'BCD', NULL, 1),
(41, 22, 2, 'RHC- Jodhpur', NULL, 1),
(42, 22, 2, 'RHC- Jaipur', NULL, 1),
(43, 22, 1, 'Family Court- Jodhpur', NULL, 1),
(44, 22, 1, 'AMJM-1 Jodhpur', NULL, 1),
(45, 22, 1, 'Dist & Session Court - Jodhpur', NULL, 1),
(46, 22, 1, 'NI Court - Jodhpur', NULL, 1),
(47, 18, 1, 'UDAIPUR COURT', NULL, 1),
(48, 29, 1, 'Jodhpur', NULL, 1),
(49, 29, 1, 'jaipur', NULL, 1),
(50, 29, 2, 'Jodhpur High Court', NULL, 1),
(51, 29, 2, 'Jaipur High Court', NULL, 1),
(52, 30, 2, 'Jodhpur High Court', NULL, 1),
(53, 30, 1, 'Jodhpur', NULL, 1),
(54, 30, 2, 'Jaipur High Court', NULL, 1),
(55, 30, 1, 'jaipur', NULL, 1),
(56, 31, 2, 'RHC- Jodhpur', NULL, 1),
(57, 31, 2, 'RHC- Jaipur', NULL, 1),
(58, 31, 1, 'Family court', NULL, 1),
(59, 31, 1, 'District Court Jodhpur', NULL, 1),
(60, 31, 1, 'JM-7 Jodhpur', NULL, 1),
(61, 31, 1, 'Lok Adalat', NULL, 1),
(62, 31, 2, 'RHC- AJMER 1', NULL, 1),
(63, 33, 2, 'RAJASTHAN HIGH COURT- JODHPUR', NULL, 1),
(64, 39, 2, 'RAJASTHAN HIGH COURT, JODHPUR', NULL, 1),
(65, 39, 2, 'RAJASTHAN HIGH COURT, JAIPUR', NULL, 1),
(66, 39, 1, 'Dist & Session Court Jodhpur (Metro)', NULL, 1),
(67, 39, 1, 'Dist & Session Court Jodhpur(Gramin)', NULL, 1),
(68, 39, 1, 'NI Court Jodhpur', NULL, 1),
(69, 39, 1, 'Family Court Jodhpur', NULL, 1),
(70, 40, 1, 'DJ COURT', NULL, 1),
(71, 40, 2, 'RAJASTHAN HIGH COURT', NULL, 1),
(72, 4, 1, 'NI COURT', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `adv_document_uploads`
--

CREATE TABLE `adv_document_uploads` (
  `upload_id` int(11) NOT NULL,
  `user_admin_id` int(11) DEFAULT NULL,
  `upload_case_type` int(11) DEFAULT NULL,
  `upload_court_type` int(11) DEFAULT NULL,
  `upload_file_no` int(11) DEFAULT NULL,
  `upload_case_year` int(11) DEFAULT NULL,
  `upload_case_id` int(11) DEFAULT NULL,
  `upload_caption` varchar(255) DEFAULT NULL,
  `upload_date` date DEFAULT NULL,
  `upload_images` varchar(255) DEFAULT NULL,
  `upload_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adv_document_uploads`
--

INSERT INTO `adv_document_uploads` (`upload_id`, `user_admin_id`, `upload_case_type`, `upload_court_type`, `upload_file_no`, `upload_case_year`, `upload_case_id`, `upload_caption`, `upload_date`, `upload_images`, `upload_status`) VALUES
(142, 4, 4, NULL, NULL, 2018, 34, 'asd', '1970-01-01', 'uploads/case_registration/1533125977142.xlsx', 0),
(143, 4, 12, NULL, NULL, 2018, 11, NULL, '1970-01-01', 'uploads/case_registration/1533125977142.xlsx', 0),
(147, 4, 12, NULL, NULL, 2018, 11, NULL, '1970-01-01', 'uploads/case_registration/1533126250147.jpg', 0),
(151, 4, 12, NULL, NULL, 2018, 11, NULL, '1970-01-01', 'uploads/case_registration/1533129142151.xlsx', 0),
(152, 4, 4, NULL, NULL, 2018, 36, NULL, '1970-01-01', NULL, 0),
(155, 14, NULL, NULL, NULL, NULL, 22, 'asdasdasd', '1999-01-05', 'uploads/case_registration/1533194913155.jpeg', 0),
(156, 14, 21, NULL, NULL, 2018, 22, 'One', '1996-01-05', 'uploads/case_registration/1533195142156.doc', 0),
(157, 14, 22, NULL, NULL, 2018, 23, 'One', '1996-01-05', 'uploads/case_registration/1533195219157.docx', 0),
(158, 14, 22, NULL, NULL, 2018, 23, 'Two', '1996-01-05', 'uploads/case_registration/1533195353158.csv', 0),
(159, 14, 22, NULL, NULL, 2018, 23, 'Three', '1996-01-05', 'uploads/case_registration/1533195353159.exe', 0),
(160, 14, 21, NULL, NULL, 2018, 24, 'Test', '1996-01-05', 'uploads/case_registration/1533202001160.docx', 0),
(161, 4, 15, NULL, NULL, 2017, 14, NULL, '1970-01-01', 'uploads/case_registration/1533361273161.xlsx', 0),
(162, 16, 25, NULL, NULL, 1968, 32, 'Test', '2010-12-05', NULL, 0),
(163, 16, 25, NULL, NULL, 1968, 32, 'Test', '2018-08-09', 'uploads/case_registration/1534747750163.pdf', 0),
(164, 15, 27, NULL, NULL, NULL, 35, 'REWT', '2018-09-08', 'uploads/case_registration/1533885942164.docx', 0),
(165, 15, 27, NULL, NULL, NULL, 35, NULL, '1970-01-01', 'uploads/case_registration/1533886824165.docx', 0),
(166, 15, 24, NULL, NULL, 1954, 34, NULL, '1970-01-01', 'uploads/case_registration/1533965343166.jpg', 0),
(167, 16, 25, NULL, NULL, 1968, 32, 'Test', '2018-08-24', 'uploads/case_registration/1534747750167.pdf', 0),
(168, 15, 27, NULL, NULL, 2018, 42, 'Stay Order', '2018-08-16', 'uploads/case_registration/1534399762168.pdf', 0),
(169, 15, 27, NULL, NULL, 2018, 42, 'Cause List', '2018-08-16', 'uploads/case_registration/1534399762169.docx', 0),
(170, 15, 27, NULL, NULL, NULL, 42, 'Decided Order', '2018-07-12', 'uploads/case_registration/1534400687170.pdf', 0),
(171, 15, NULL, NULL, NULL, NULL, 42, 'Normal Order Sheet', '2018-05-02', 'uploads/case_registration/1534400919171.pdf', 0),
(172, 15, 27, NULL, NULL, NULL, 42, 'Lord Ganesha Image', '2017-11-11', 'uploads/case_registration/1534402840172.jpg', 0),
(173, 15, 27, NULL, NULL, 2018, 42, 'Excel Formet', '2017-07-13', 'uploads/case_registration/1534403035173.xlsx', 0),
(174, 16, NULL, NULL, NULL, NULL, 36, NULL, '1970-01-01', NULL, 0),
(175, 4, NULL, NULL, NULL, NULL, 3, NULL, '1970-01-01', NULL, 0),
(176, 16, 25, 2, NULL, NULL, 76, 'Order Judgment', '2018-05-01', NULL, 0),
(177, 16, 25, 2, NULL, NULL, 76, 'Order Judgment', '2019-10-04', 'uploads/case_registration/1536215503177.jpg', 0),
(178, 16, 25, 2, NULL, NULL, 76, 'Order Judgment', '2019-08-28', 'uploads/case_registration/1536215544178.pdf', 0),
(179, 16, 25, 2, NULL, NULL, 76, 'Order Judgment', '2019-01-24', 'uploads/case_registration/1536215576179.doc', 0),
(180, 16, 25, 2, NULL, NULL, 76, 'Order Judgment', '2019-05-25', 'uploads/case_registration/1536215682180.txt', 0),
(181, 18, NULL, 2, NULL, NULL, 104, 'PET. COPY', '2018-09-06', 'uploads/case_registration/1536248965181.jpg', 0),
(182, 16, NULL, NULL, NULL, NULL, 32, NULL, '1970-01-01', NULL, 0),
(183, 16, NULL, NULL, NULL, NULL, 67, NULL, '1970-01-01', 'uploads/case_registration/1536325091183.docx', 0),
(184, 16, 25, 1, NULL, NULL, 208, 'Order Judgment', '1970-01-01', NULL, 0),
(185, 4, 17, 2, NULL, NULL, 19, '1', '2018-09-22', 'uploads/case_registration/1536401620185.png', 0),
(186, 19, 61, 1, NULL, NULL, 268, NULL, '1970-01-01', NULL, 0),
(187, 19, 61, NULL, NULL, NULL, 268, 'Test', '2018-12-21', 'uploads/case_registration/1536406638187.doc', 0),
(188, 19, 61, NULL, NULL, NULL, 268, 'Test', '2018-09-21', 'uploads/case_registration/1536406638188.png', 0),
(189, 22, 64, 2, NULL, NULL, 299, 'Stay', '2018-09-07', 'uploads/case_registration/1536580043189.pdf', 0),
(190, 22, 64, 1, NULL, NULL, 287, 'Stay', '2018-09-01', 'uploads/case_registration/1536580147190.pdf', 0),
(191, 22, 69, 1, NULL, NULL, 295, 'Stay', '2018-09-05', 'uploads/case_registration/1536580196191.pdf', 0),
(192, 22, 66, 1, NULL, NULL, 290, 'Decided', '2018-09-03', 'uploads/case_registration/1536580383192.pdf', 0),
(193, 22, 67, 2, NULL, NULL, 293, 'Decided on So and So', '2018-09-08', 'uploads/case_registration/1536580427193.pdf', 0),
(194, 22, 67, 2, NULL, NULL, 293, 'Ganesh Ji', '2018-09-10', 'uploads/case_registration/1536581046194.jpg', 0),
(195, 22, 67, 2, NULL, NULL, 293, 'Cause List', '2018-09-10', 'uploads/case_registration/1536581137195.docx', 0),
(196, 22, 67, 2, NULL, NULL, 293, 'Photo', '2018-09-05', 'uploads/case_registration/1536581642196.jpg', 0),
(197, 22, 67, 2, NULL, NULL, 293, 'Excel Formet', '2018-09-06', 'uploads/case_registration/1536582146197.xlsx', 0),
(198, 22, NULL, NULL, NULL, NULL, 304, NULL, '1970-01-01', 'uploads/case_registration/1537078066198.pdf', 0),
(199, 22, NULL, NULL, NULL, NULL, 285, 'test1', '1970-01-01', NULL, 0),
(200, 22, NULL, 1, NULL, NULL, 287, NULL, '1970-01-01', NULL, 0),
(201, 22, 65, 2, NULL, NULL, 351, NULL, '1970-01-01', NULL, 0),
(202, 22, NULL, NULL, NULL, NULL, 290, NULL, '1970-01-01', 'uploads/case_registration/1537355149202.jpg', 0),
(203, 22, 64, 2, NULL, NULL, 286, 'Yogesh', '2018-09-28', 'uploads/case_registration/1538115550203.docx', 0),
(204, 22, 64, 2, NULL, NULL, 286, 'Dheeraj', '2018-09-28', 'uploads/case_registration/1538115756204.jpg', 0),
(205, 22, 64, 2, NULL, NULL, 286, 'Kapil', '2018-09-28', 'uploads/case_registration/1538115822205.docx', 0),
(206, 4, 15, 1, NULL, NULL, 26, 'FIrst Doc', '2018-10-12', 'uploads/case_registration/1538415282206.jpg', 0),
(207, 4, 15, NULL, NULL, NULL, 26, '2nd', '2018-10-15', NULL, 0),
(208, 4, 15, NULL, NULL, NULL, 26, '2nd', '2018-10-15', 'uploads/case_registration/1538980077208.png', 0),
(210, 31, 88, 2, NULL, NULL, 495, 'Stay Order', '2018-12-20', 'uploads/case_registration/1545303185210.docx', 0),
(211, 31, 85, 1, NULL, NULL, 496, 'Stay Order', '2018-12-21', 'uploads/case_registration/1545303225211.docx', 0),
(212, 31, 88, 2, NULL, NULL, 497, 'Stay Order', '2018-12-18', 'uploads/case_registration/1545303290212.docx', 0),
(213, 31, 85, 1, NULL, NULL, 496, 'Decided', '2018-12-01', 'uploads/case_registration/1545303328213.docx', 0),
(214, 31, 88, 2, NULL, NULL, 498, 'Decided', '2018-12-05', 'uploads/case_registration/1545303374214.docx', 0),
(215, 30, 81, 2, NULL, NULL, 494, 'test', '2018-12-12', NULL, 0),
(216, 31, 88, 2, NULL, NULL, 498, 'Stay Order', '2019-04-01', 'uploads/case_registration/1554982276216.docx', 0),
(217, 31, 85, 1, NULL, NULL, 496, 'Decided', '2019-04-01', 'uploads/case_registration/1554982385217.docx', 0),
(218, 4, 16, 1, NULL, NULL, 29, 'Dheeraj', '2019-08-25', 'uploads/case_registration/1567328725218.docx', 0),
(219, 4, 16, 1, NULL, NULL, 29, 'Shivay', '2019-08-20', 'uploads/case_registration/1567328725219.docx', 0),
(220, 4, 16, 1, NULL, NULL, 29, 'Yogesh', '2019-08-25', 'uploads/case_registration/1567328725220.docx', 0),
(221, 4, 18, 1, NULL, NULL, 487, 'yogesh', '2019-08-07', 'uploads/case_registration/1567329207221.doc', 0),
(222, 4, 5, 2, NULL, NULL, 3, 'JHU', '2019-09-02', 'uploads/case_registration/1567405838222.doc', 0),
(223, 4, 16, 1, NULL, NULL, 29, 'xxxx', '2019-09-01', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `adv_fir`
--

CREATE TABLE `adv_fir` (
  `fir_id` int(11) NOT NULL,
  `user_admin_id` int(11) DEFAULT NULL,
  `fir_number` varchar(50) DEFAULT NULL,
  `fir_year` varchar(50) DEFAULT NULL,
  `fir_pt_name` varchar(50) DEFAULT NULL,
  `fir_status` tinyint(2) NOT NULL COMMENT '0=Inactive,1=Active	'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `adv_holiday`
--

CREATE TABLE `adv_holiday` (
  `holiday_id` int(11) NOT NULL,
  `holiday_title` varchar(50) DEFAULT NULL,
  `holiday_description` longtext,
  `holiday_date` date DEFAULT NULL,
  `holiday_status` int(11) NOT NULL DEFAULT '1',
  `holiday_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adv_holiday`
--

INSERT INTO `adv_holiday` (`holiday_id`, `holiday_title`, `holiday_description`, `holiday_date`, `holiday_status`, `holiday_created`) VALUES
(3, 'fgjd', 'fgdjghfj', '2018-07-27', 1, '2018-07-16 13:02:27'),
(4, 'sdfasdfsd', 'sdfgadsfgfdhg', '2018-07-18', 1, '2018-07-16 13:02:41'),
(5, 'gghj', NULL, '2018-07-24', 1, '2018-07-24 12:28:07'),
(6, NULL, NULL, '2018-07-31', 1, '2018-07-25 09:45:42'),
(7, 'Test', 'Test', '2018-09-30', 0, '2018-09-08 11:25:19');

-- --------------------------------------------------------

--
-- Table structure for table `adv_judge`
--

CREATE TABLE `adv_judge` (
  `judge_id` int(11) NOT NULL,
  `user_admin_id` int(11) DEFAULT NULL,
  `judge_name` varchar(50) DEFAULT NULL,
  `judge_status` tinyint(2) NOT NULL COMMENT '0=Inactive,1=Active	'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adv_judge`
--

INSERT INTO `adv_judge` (`judge_id`, `user_admin_id`, `judge_name`, `judge_status`) VALUES
(1, 6, 'Mohan Lal', 0),
(2, 6, 'Vishal Sharma', 1),
(3, 6, 'Vivek mehta', 1),
(4, 4, 'Mahesh', 0),
(5, 9, 'Test', 0),
(6, 9, 'Test One', 1),
(7, 9, 'Test Two', 1),
(8, 9, 'Test Three', 1),
(9, 4, 'Test One', 1),
(10, 4, 'Test Two', 1),
(11, 8, 'Test One', 1),
(12, 8, 'Test Two', 1),
(13, 4, 'Gopal Krishan Vyas', 1),
(14, 4, 'VIjay Kumar Vyas', 1),
(15, 4, 'PK Lohra', 1),
(16, 14, 'Test One', 1),
(18, 16, 'Nilesh', 1),
(19, 16, 'Pankaj', 1),
(20, 16, 'Aditya', 1),
(21, 16, 'Pradeep', 1),
(22, 15, 'Gopal Krishan Vyas', 1),
(23, 15, 'VIjay Kumar Vyas', 1),
(24, 15, 'PK Lohra', 1),
(25, 15, 'Sandeep Mehta', 1),
(26, 15, 'Sangeet Lodha', 1),
(27, 19, '@', 1),
(28, 19, '#', 1),
(29, 22, 'HON\'BLE GK VYAS', 1),
(30, 22, 'HON\'BLE VIJAY KUMAR VYAS', 1),
(31, 22, 'HON\'BLE SANDEEP MEHTA', 1),
(32, 22, 'HON\'BLE PK LOHRA', 1),
(33, 29, 'Mohan Lal', 1),
(34, 29, 'Lalit Jala', 1),
(35, 30, 'Mohan Lal', 1),
(36, 30, 'Mohan Lal', 1),
(37, 30, 'Lalit Jala', 1),
(38, 31, 'Hon\'ble GK Vyas', 1),
(39, 31, 'PK Lohra', 1),
(40, 31, 'Sandeep Mehta', 1);

-- --------------------------------------------------------

--
-- Table structure for table `adv_master_pages`
--

CREATE TABLE `adv_master_pages` (
  `pages_id` int(11) NOT NULL,
  `pages_title` varchar(255) NOT NULL,
  `pages_description` longtext,
  `pages_english_pdf` varchar(255) NOT NULL,
  `pages_hindi_pdf` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adv_master_pages`
--

INSERT INTO `adv_master_pages` (`pages_id`, `pages_title`, `pages_description`, `pages_english_pdf`, `pages_hindi_pdf`) VALUES
(1, 'About Software', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<p><br></p><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br></p>', 'uploads/pages/english_pdf.pdf', 'uploads/pages/hindi_pdf.pdf'),
(2, 'Guideline Software', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'uploads/pages/english_pdf1532149629.pdf', 'uploads/pages/hindi_pdf1532149629.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `adv_notes`
--

CREATE TABLE `adv_notes` (
  `notes_id` int(11) NOT NULL,
  `notes_user_id` int(11) NOT NULL,
  `notes_description` longtext NOT NULL,
  `notes_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adv_notes`
--

INSERT INTO `adv_notes` (`notes_id`, `notes_user_id`, `notes_description`, `notes_date`) VALUES
(9, 6, 'hello  hello', '2018-06-29'),
(10, 4, 'lorem ipsun', '2018-06-30'),
(11, 8, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2018-07-05'),
(12, 4, 'meeting', '2018-07-05'),
(13, 15, 'APPOITEMENT', '2018-08-11'),
(14, 15, 'IIIIIIIIIIIIIIIIIIIIIIIIIIIIII', '2018-08-23'),
(15, 15, 'POOO', '2018-08-30'),
(16, 15, 'OOO', '2018-08-30'),
(17, 16, 'asd', '2018-09-30'),
(18, 16, 'asdasda', '2018-09-30'),
(19, 16, 'asdasdasd', '2018-09-30'),
(20, 4, 'sdf', '2018-09-30'),
(21, 4, 'sdfasd', '2018-09-12'),
(22, 4, 'sdfasdf', '2018-09-20'),
(23, 4, 'sdfasdf', '2018-09-30'),
(24, 4, 'sdfasdf', '2018-09-30'),
(25, 4, 'sdfasdfg', '2018-09-30'),
(26, 19, 'Test', '2018-09-30'),
(27, 19, 'Test', '2018-09-30'),
(28, 19, 'Test', '2018-09-30'),
(29, 19, 'Test', '2018-09-30'),
(30, 19, 'Test', '2018-09-30'),
(31, 19, 'Test', '2018-09-30'),
(32, 30, 'test', '2018-12-20'),
(33, 4, 'Testing 10 march notes', '2019-03-10'),
(34, 4, 'UUUUUUUU', '2019-09-02'),
(35, 4, 'KKKKKKKK', '2019-09-02');

-- --------------------------------------------------------

--
-- Table structure for table `adv_pessi`
--

CREATE TABLE `adv_pessi` (
  `pessi_id` int(11) NOT NULL,
  `pessi_case_reg` int(11) DEFAULT NULL,
  `pessi_user_id` int(11) NOT NULL,
  `pessi_prev_date` varchar(50) DEFAULT NULL,
  `pessi_statge_id` int(11) DEFAULT NULL,
  `pessi_further_date` date DEFAULT NULL,
  `pessi_decide_id` int(11) DEFAULT NULL,
  `pessi_order_sheet` longtext,
  `pessi_choose_type` int(11) DEFAULT NULL COMMENT '0 - further date 1 - decide 3 - due course',
  `pessi_due_course` varchar(255) DEFAULT NULL,
  `honaurable_justice` int(11) DEFAULT NULL,
  `honarable_justice_db` int(11) DEFAULT NULL,
  `court_number` varchar(255) DEFAULT NULL,
  `serial_number` varchar(100) DEFAULT NULL,
  `page_number` varchar(100) DEFAULT NULL,
  `cause_status` int(11) DEFAULT '0',
  `pessi_status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '0=Inactive,1=Active',
  `pessi_update` int(11) NOT NULL DEFAULT '0',
  `pessi_sms_status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adv_pessi`
--

INSERT INTO `adv_pessi` (`pessi_id`, `pessi_case_reg`, `pessi_user_id`, `pessi_prev_date`, `pessi_statge_id`, `pessi_further_date`, `pessi_decide_id`, `pessi_order_sheet`, `pessi_choose_type`, `pessi_due_course`, `honaurable_justice`, `honarable_justice_db`, `court_number`, `serial_number`, `page_number`, `cause_status`, `pessi_status`, `pessi_update`, `pessi_sms_status`, `created_at`, `updated_at`) VALUES
(1, 1, 6, NULL, 2, '2018-06-26', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2018-06-26 09:04:10', '2018-06-26 09:04:10'),
(2, 1, 6, '2018-06-26', 2, '2018-06-26', NULL, 'test', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2018-06-26 09:04:10', '2018-06-26 09:04:10'),
(3, 1, 6, '2018-06-26', 2, '2018-06-26', 4, 'test', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2018-06-26 09:04:10', '2018-06-26 09:04:10'),
(4, 1, 6, '2018-06-26', 2, '2018-06-25', NULL, 'test', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-06-26 09:09:33', '2018-06-26 09:04:10'),
(7, 3, 4, NULL, 3, '2018-06-30', NULL, NULL, 0, NULL, 4, NULL, '1', '31', '145', NULL, 0, 0, 0, '2018-07-10 11:01:45', '2018-07-10 11:01:45'),
(8, 4, 9, NULL, 5, '2018-08-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2018-06-27 11:42:25', '2018-06-27 11:42:25'),
(9, 4, 9, '2018-08-01', 5, '2018-08-01', 1, 'Test', 1, NULL, 7, NULL, '1', '123', '1', NULL, 1, 0, 0, '2018-06-27 11:43:03', '2018-06-27 11:43:03'),
(10, 5, 9, NULL, 4, '2018-06-30', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-06-28 04:45:39', '2018-06-28 04:45:39'),
(11, 6, 6, NULL, 2, '2018-06-29', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-06-28 12:12:09', '2018-06-28 12:12:09'),
(12, 3, 4, '2018-06-30', 3, '2018-06-30', 2, 'lorem', 1, NULL, 9, NULL, '9', NULL, '145', NULL, 0, 0, 0, '2018-07-10 11:01:45', '2018-07-10 11:01:45'),
(13, 7, 4, NULL, 7, '2018-07-05', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2018-07-10 11:39:39', '2018-07-10 11:39:39'),
(14, 8, 4, NULL, 8, '2018-06-30', NULL, NULL, 0, NULL, 13, NULL, '1', '111', '11', 1, 1, 0, 0, '2018-08-09 06:11:46', '2018-08-09 06:11:46'),
(15, 9, 8, NULL, 9, '2018-06-30', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2018-06-30 10:25:06', '2018-06-30 10:25:06'),
(16, 9, 8, '2018-06-30', 10, '2018-06-30', 3, 'Rest', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2018-06-30 10:25:06', '2018-06-30 10:25:06'),
(17, 9, 8, '2018-06-30', 10, '2018-06-30', 3, 'Rest', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-06-30 10:25:06', '2018-06-30 10:25:06'),
(18, 10, 8, NULL, 9, '2018-07-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-06-30 10:26:56', '2018-06-30 10:26:56'),
(19, 11, 4, NULL, 7, '2018-07-05', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2018-07-02 10:32:45', '2018-07-02 10:32:45'),
(20, 11, 4, '2018-07-05', 7, '2018-07-05', 2, 'test', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-07-02 10:32:45', '2018-07-02 10:32:45'),
(21, 12, 4, NULL, 8, '2018-07-17', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-07-02 13:47:15', '2018-07-02 13:47:15'),
(22, 3, 4, '2018-06-30', NULL, '2018-06-30', 2, 'lorem', 1, NULL, 9, NULL, '9', NULL, '145', NULL, 1, 0, 0, '2018-07-10 11:01:45', '2018-07-10 11:01:45'),
(23, 7, 4, '2018-07-05', 7, '2018-07-05', NULL, 'NJJKJK', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2018-07-10 11:39:39', '2018-07-10 11:39:39'),
(24, 7, 4, '2018-07-05', 7, '2018-07-05', NULL, 'NJJKJK', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-07-10 11:39:39', '2018-07-10 11:39:39'),
(25, 13, 4, NULL, 11, '2018-07-26', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-07-25 05:37:18', '2018-07-25 05:37:18'),
(26, 14, 4, NULL, 13, '2018-07-26', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-07-25 05:41:05', '2018-07-25 05:41:05'),
(27, 15, 4, NULL, 11, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2018-07-28 09:41:09', '2018-07-28 09:41:09'),
(28, 16, 4, NULL, 11, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2018-08-09 06:09:54', '2018-08-09 06:09:54'),
(29, 17, 4, NULL, 11, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2018-08-09 06:09:46', '2018-08-09 06:09:46'),
(30, 18, 4, NULL, 11, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2018-08-02 04:34:47', '2018-08-02 04:34:47'),
(31, 19, 4, NULL, 13, '2018-07-31', NULL, NULL, 0, NULL, 13, 14, '1', '10', '12', 1, 0, 0, 0, '2018-08-14 11:06:24', '2018-08-14 11:06:24'),
(32, 18, 4, '1970-01-01', 11, '2018-07-31', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2018-08-02 04:34:47', '2018-08-02 04:34:47'),
(33, 18, 4, '1970-01-01', 11, '2018-07-31', NULL, NULL, 0, NULL, 15, 13, '3', '17', '09', 1, 0, 0, 0, '2018-08-02 04:34:47', '2018-08-02 04:34:47'),
(34, 17, 4, '1970-01-01', 13, '2018-07-31', NULL, NULL, 0, NULL, 14, 13, '7', '09', '13', 1, 0, 0, 0, '2018-08-09 06:09:46', '2018-08-09 06:09:46'),
(35, 16, 4, '1970-01-01', 11, '2018-07-31', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2018-08-09 06:09:54', '2018-08-09 06:09:54'),
(36, 15, 4, '2018-07-31', 11, '2018-07-31', NULL, 'dsa', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-08-30 11:03:54', '2018-08-30 11:03:54'),
(38, 19, 4, '2018-07-31', 13, '2018-07-31', NULL, 'HON\'BLE GK VYAS J\r\nPUTUP ON AFTER TWO WEEK', 0, NULL, 13, NULL, '1', '10', '12', NULL, 0, 0, 0, '2018-08-14 11:06:24', '2018-08-14 11:06:24'),
(39, 18, 4, '2018-07-31', 11, '2018-07-31', NULL, 'HONBLE VIJAY KUMAR VYAS J\r\nISSUE NOTICE STAY', 0, NULL, 15, NULL, '3', '17', '09', NULL, 0, 0, 0, '2018-08-02 04:34:47', '2018-08-02 04:34:47'),
(40, 19, 4, '2018-07-31', 13, '2018-08-01', NULL, 'HON\'BLE GK VYAS J\r\nPUTUP ON AFTER TWO WEEK', 0, NULL, 13, NULL, '1', '10', '12', NULL, 0, 0, 0, '2018-08-14 11:06:24', '2018-08-14 11:06:24'),
(41, 19, 4, '2018-07-31', 13, '2018-08-01', NULL, 'HON\'BLE GK VYAS J\r\nPUTUP ON AFTER TWO WEEK', 0, NULL, 13, 14, '1', '10', '12', 1, 0, 0, 0, '2018-08-14 11:06:24', '2018-08-14 11:06:24'),
(42, 20, 4, NULL, 14, '2018-08-02', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-07-30 13:31:21', '2018-07-30 13:31:21'),
(43, 19, 4, '2018-08-01', 13, '2018-08-01', NULL, 'HON\'BLE GK VYAS J\r\nPUTUP ON AFTER TWO WEEK', 0, NULL, 13, NULL, '1', '10', '12', NULL, 0, 0, 0, '2018-08-14 11:06:24', '2018-08-14 11:06:24'),
(44, 18, 4, '2018-07-31', 11, '2018-07-31', NULL, 'HONBLE VIJAY KUMAR VYAS J\r\nISSUE NOTICE STAY', 2, NULL, 15, NULL, '3', '17', '09', NULL, 1, 0, 0, '2018-08-02 04:34:47', '2018-08-02 04:34:47'),
(45, 21, 14, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2018-08-02 06:54:52', '2018-08-02 06:54:52'),
(46, 21, 14, '1970-01-01', 15, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2018-08-02 06:54:52', '2018-08-02 06:54:52'),
(47, 21, 14, '1970-01-01', 15, '1970-01-01', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2018-08-02 06:54:52', '2018-08-02 06:54:52'),
(48, 22, 14, NULL, 15, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-08-02 05:09:57', '2018-08-02 05:09:57'),
(49, 23, 14, NULL, 15, '1970-01-01', NULL, NULL, 0, NULL, 16, 16, '1', '1', '1', 1, 0, 0, 0, '2018-08-02 07:39:55', '2018-08-02 07:39:55'),
(50, 21, 14, '1970-01-01', 15, '1970-01-01', NULL, 'Test', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-08-02 06:54:52', '2018-08-02 06:54:52'),
(51, 23, 14, '1970-01-01', 15, '1970-01-01', NULL, 'Order', 0, NULL, 16, NULL, '1', '1', '1', NULL, 1, 0, 0, '2018-08-02 07:39:55', '2018-08-02 07:39:55'),
(52, 24, 14, NULL, 15, '2018-08-31', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-08-02 09:14:45', '2018-08-02 09:14:45'),
(53, 25, 14, NULL, 15, '2018-08-31', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-08-02 09:40:01', '2018-08-02 09:40:01'),
(54, 19, 4, '2018-08-01', 13, '2018-08-03', NULL, 'HON\'BLE GK VYAS J\r\nPUTUP ON AFTER TWO WEEK', 0, NULL, 13, NULL, '1', '10', '12', NULL, 0, 0, 0, '2018-08-11 12:30:47', '2018-08-14 11:06:24'),
(55, 19, 4, '2018-08-03', 13, '2018-08-03', NULL, 'HON\'BLE GK VYAS J\r\nHON\'BLE VINEET MATHUR J\r\nPUTUP ON AFTER TWO WEEK', 0, NULL, 13, 14, '1', '10', '12', 1, 0, 0, 0, '2018-08-11 12:30:47', '2018-08-14 11:06:24'),
(56, 19, 4, '2018-08-03', 13, '2018-08-03', NULL, 'HON\'BLE GK VYAS J\r\nHON\'BLE VINEET MATHUR J\r\nPUTUP ON AFTER TWO WEEK', 0, NULL, 13, 13, '1', '10', '12', 1, 0, 0, 0, '2018-08-11 12:30:47', '2018-08-14 11:06:24'),
(57, 19, 4, '2018-08-03', 13, '1970-01-01', NULL, 'HON\'BLE GK VYAS J\r\nHON\'BLE VINEET MATHUR J\r\nPUTUP ON AFTER TWO WEEK', 0, NULL, 13, NULL, '1', '10', '12', NULL, 0, 0, 0, '2018-08-11 12:30:47', '2018-08-14 11:06:24'),
(58, 26, 4, '1970-01-01', 12, '2019-05-11', NULL, 'dismissed', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-03 05:53:38', '2018-09-03 05:53:38'),
(91, 38, 4, '2005-05-05', 7, '2005-05-05', 1, 'test', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-03 05:48:37', '2018-09-03 05:48:37'),
(135, 68, 4, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2019-09-01 09:29:28', '2019-09-01 09:29:28'),
(137, 70, 15, '2018-08-21', 20, '2018-08-24', NULL, 'DSFDSSDFDS', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-01 04:38:08', '2018-09-01 04:38:08'),
(138, 71, 15, '2018-09-05', 20, '2018-10-10', NULL, 'AAAAAAAAAAAAA', 0, NULL, 22, 24, '4', '444', '4', 1, 1, 0, 0, '2018-09-04 06:44:48', '2018-09-04 06:44:48'),
(139, 72, 15, '2018-09-18', 17, '2018-09-20', NULL, 'AAAAAAAAAAAA', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-01 04:37:37', '2018-09-01 04:37:37'),
(140, 73, 15, '2018-06-27', 22, '2018-05-25', NULL, 'Early Listing Allowed\r\nPutup on after two week..', 0, NULL, 26, 25, '1', '1', '1', 1, 0, 0, 0, '2018-09-24 09:50:06', '2018-09-24 09:50:06'),
(141, 74, 4, '2018-09-13', 11, '2018-09-13', NULL, 'testdfdfdsgfgfgfgdsfgdf', 2, NULL, 14, 13, '1', '3', '53', 1, 1, 0, 0, '2018-09-08 09:34:34', '2018-09-08 09:34:34'),
(142, 75, 16, NULL, 19, '1970-01-01', NULL, 'Hello', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-06 07:07:47', '2018-09-06 07:07:47'),
(143, 76, 16, '2018-02-01', 19, '0000-00-00', NULL, 'Test', 1, NULL, 20, 19, '1', '1.2', '1.3', 1, 1, 0, 0, '2018-09-06 07:04:24', '2018-09-06 07:04:24'),
(144, 77, 16, NULL, 18, '2018-11-10', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-06 07:13:36', '2018-09-06 07:13:36'),
(146, 79, 4, '1970-01-01', 12, '1970-01-01', 1, 'tesr', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-06 12:27:09', '2018-09-06 12:27:09'),
(147, 80, 16, NULL, 19, '2018-09-29', NULL, 'asaasd', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:05:54', '2018-09-08 05:05:54'),
(148, 81, 16, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-06 12:42:29', '2018-09-06 12:42:29'),
(149, 82, 16, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-06 12:42:30', '2018-09-06 12:42:30'),
(150, 83, 16, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-06 12:42:31', '2018-09-06 12:42:31'),
(151, 84, 16, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-06 12:42:31', '2018-09-06 12:42:31'),
(152, 85, 16, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-06 12:42:31', '2018-09-06 12:42:31'),
(153, 86, 16, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-06 12:42:31', '2018-09-06 12:42:31'),
(154, 87, 16, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-06 12:42:32', '2018-09-06 12:42:32'),
(155, 88, 16, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-06 12:42:32', '2018-09-06 12:42:32'),
(156, 89, 16, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-06 12:42:32', '2018-09-06 12:42:32'),
(157, 90, 16, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-06 12:42:32', '2018-09-06 12:42:32'),
(158, 91, 16, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-06 12:42:32', '2018-09-06 12:42:32'),
(159, 92, 16, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-06 12:42:33', '2018-09-06 12:42:33'),
(160, 93, 16, NULL, NULL, '2018-09-30', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-06 12:43:06', '2018-09-06 12:43:06'),
(162, 95, 16, '2018-09-29', 18, '2018-09-29', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-06 12:48:04', '2018-09-06 12:48:04'),
(163, 96, 4, '1970-01-01', 13, '1970-01-01', 2, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 09:40:56', '2018-09-08 09:40:56'),
(164, 97, 15, NULL, 17, '2018-09-22', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-06 13:18:44', '2018-09-06 13:18:44'),
(165, 98, 15, NULL, 20, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-06 13:20:08', '2018-09-06 13:20:08'),
(166, 99, 15, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-06 13:30:17', '2018-09-06 13:30:17'),
(167, 100, 15, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-06 13:32:50', '2018-09-06 13:32:50'),
(168, 101, 4, NULL, 7, '1970-01-01', NULL, 'fdhffffffff', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 09:38:43', '2018-09-08 09:38:43'),
(169, 102, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-06 14:55:07', '2018-09-06 14:55:07'),
(170, 103, 18, NULL, 24, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-06 15:02:11', '2018-09-06 15:02:11'),
(172, 105, 15, '1970-01-01', 17, '2018-10-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-24 10:37:00', '2018-09-24 10:37:00'),
(173, 106, 15, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2018-09-24 08:03:39', '2018-09-24 08:03:39'),
(174, 107, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 07:31:58', '2018-09-07 07:31:58'),
(175, 108, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 07:33:38', '2018-09-07 07:33:38'),
(176, 109, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 07:34:41', '2018-09-07 07:34:41'),
(177, 110, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 07:35:27', '2018-09-07 07:35:27'),
(178, 111, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 07:40:05', '2018-09-07 07:40:05'),
(179, 112, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 07:40:53', '2018-09-07 07:40:53'),
(180, 113, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 07:41:39', '2018-09-07 07:41:39'),
(181, 114, 18, NULL, 24, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 07:42:25', '2018-09-07 07:42:25'),
(182, 115, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 07:43:11', '2018-09-07 07:43:11'),
(183, 116, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 07:44:00', '2018-09-07 07:44:00'),
(184, 117, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 07:44:55', '2018-09-07 07:44:55'),
(185, 118, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 07:45:27', '2018-09-07 07:45:27'),
(186, 119, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 07:46:13', '2018-09-07 07:46:13'),
(187, 120, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 08:05:40', '2018-09-07 08:05:40'),
(188, 121, 18, NULL, 24, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 08:10:54', '2018-09-07 08:10:54'),
(189, 122, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 08:15:04', '2018-09-07 08:15:04'),
(190, 123, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 08:16:32', '2018-09-07 08:16:32'),
(191, 124, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 08:28:09', '2018-09-07 08:28:09'),
(192, 125, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 08:28:38', '2018-09-07 08:28:38'),
(193, 126, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 08:29:33', '2018-09-07 08:29:33'),
(194, 127, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 08:30:58', '2018-09-07 08:30:58'),
(195, 128, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 08:33:14', '2018-09-07 08:33:14'),
(196, 129, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 08:34:19', '2018-09-07 08:34:19'),
(197, 130, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 08:35:19', '2018-09-07 08:35:19'),
(198, 131, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 08:37:02', '2018-09-07 08:37:02'),
(199, 132, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 08:37:37', '2018-09-07 08:37:37'),
(200, 133, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 08:38:27', '2018-09-07 08:38:27'),
(201, 134, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 08:39:19', '2018-09-07 08:39:19'),
(202, 135, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 08:40:22', '2018-09-07 08:40:22'),
(203, 136, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 08:40:54', '2018-09-07 08:40:54'),
(204, 137, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 08:41:31', '2018-09-07 08:41:31'),
(205, 138, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 08:42:17', '2018-09-07 08:42:17'),
(206, 139, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 08:43:04', '2018-09-07 08:43:04'),
(207, 140, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 08:43:54', '2018-09-07 08:43:54'),
(208, 141, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 08:46:12', '2018-09-07 08:46:12'),
(209, 142, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 08:46:54', '2018-09-07 08:46:54'),
(210, 143, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 08:47:45', '2018-09-07 08:47:45'),
(211, 144, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 08:48:34', '2018-09-07 08:48:34'),
(212, 145, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 08:49:07', '2018-09-07 08:49:07'),
(213, 146, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 08:49:41', '2018-09-07 08:49:41'),
(214, 147, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 08:50:11', '2018-09-07 08:50:11'),
(215, 148, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 08:50:58', '2018-09-07 08:50:58'),
(216, 149, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 08:51:41', '2018-09-07 08:51:41'),
(217, 150, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 08:52:31', '2018-09-07 08:52:31'),
(218, 151, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 08:53:10', '2018-09-07 08:53:10'),
(219, 152, 18, NULL, 24, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 08:53:53', '2018-09-07 08:53:53'),
(220, 153, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 08:55:06', '2018-09-07 08:55:06'),
(222, 155, 16, NULL, NULL, '2018-09-29', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 10:42:45', '2018-09-07 10:42:45'),
(224, 157, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 13:16:08', '2018-09-07 13:16:08'),
(225, 158, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 13:16:53', '2018-09-07 13:16:53'),
(226, 159, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 13:17:48', '2018-09-07 13:17:48'),
(227, 160, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 13:24:10', '2018-09-07 13:24:10'),
(228, 161, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 13:24:56', '2018-09-07 13:24:56'),
(229, 162, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 13:25:27', '2018-09-07 13:25:27'),
(230, 163, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 13:26:05', '2018-09-07 13:26:05'),
(231, 164, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 13:27:10', '2018-09-07 13:27:10'),
(232, 165, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 13:28:14', '2018-09-07 13:28:14'),
(233, 166, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 13:29:19', '2018-09-07 13:29:19'),
(234, 167, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 13:31:10', '2018-09-07 13:31:10'),
(235, 168, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 13:31:59', '2018-09-07 13:31:59'),
(236, 169, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 13:33:14', '2018-09-07 13:33:14'),
(237, 170, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 13:34:24', '2018-09-07 13:34:24'),
(238, 171, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 13:35:28', '2018-09-07 13:35:28'),
(239, 172, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 13:38:39', '2018-09-07 13:38:39'),
(240, 173, 18, NULL, 24, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 13:40:13', '2018-09-07 13:40:13'),
(241, 174, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 13:41:38', '2018-09-07 13:41:38'),
(242, 175, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 13:44:14', '2018-09-07 13:44:14'),
(243, 176, 18, NULL, 24, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 13:44:49', '2018-09-07 13:44:49'),
(244, 177, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 13:46:01', '2018-09-07 13:46:01'),
(245, 178, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 13:46:53', '2018-09-07 13:46:53'),
(246, 179, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 13:48:41', '2018-09-07 13:48:41'),
(247, 180, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 13:49:26', '2018-09-07 13:49:26'),
(248, 181, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 13:49:58', '2018-09-07 13:49:58'),
(249, 182, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 13:54:34', '2018-09-07 13:54:34'),
(250, 183, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 13:55:05', '2018-09-07 13:55:05'),
(251, 184, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 13:56:36', '2018-09-07 13:56:36'),
(252, 185, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 13:57:24', '2018-09-07 13:57:24'),
(253, 186, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 14:01:11', '2018-09-07 14:01:11'),
(254, 187, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 14:01:43', '2018-09-07 14:01:43'),
(255, 188, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 14:02:06', '2018-09-07 14:02:06'),
(256, 189, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 14:04:41', '2018-09-07 14:04:41'),
(257, 190, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 14:05:42', '2018-09-07 14:05:42'),
(258, 191, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 14:07:00', '2018-09-07 14:07:00'),
(259, 192, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 14:07:48', '2018-09-07 14:07:48'),
(260, 193, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 14:45:49', '2018-09-07 14:45:49'),
(261, 194, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 14:47:03', '2018-09-07 14:47:03'),
(262, 195, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 14:47:47', '2018-09-07 14:47:47'),
(263, 196, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 14:48:38', '2018-09-07 14:48:38'),
(264, 197, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 14:49:18', '2018-09-07 14:49:18'),
(265, 198, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 14:49:52', '2018-09-07 14:49:52'),
(266, 199, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 14:50:17', '2018-09-07 14:50:17'),
(267, 200, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 14:50:43', '2018-09-07 14:50:43'),
(268, 201, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 14:51:11', '2018-09-07 14:51:11'),
(269, 202, 18, NULL, 24, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 14:51:47', '2018-09-07 14:51:47'),
(270, 203, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 14:52:12', '2018-09-07 14:52:12'),
(271, 204, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 14:52:55', '2018-09-07 14:52:55'),
(272, 205, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 14:53:35', '2018-09-07 14:53:35'),
(273, 206, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 14:54:18', '2018-09-07 14:54:18'),
(274, 207, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-07 14:54:56', '2018-09-07 14:54:56'),
(275, 208, 16, NULL, 18, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 04:28:13', '2018-09-08 04:28:13'),
(276, 209, 16, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 04:31:36', '2018-09-08 04:31:36'),
(277, 210, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:10:14', '2018-09-08 05:10:14'),
(278, 211, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:10:45', '2018-09-08 05:10:45'),
(279, 213, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:11:56', '2018-09-08 05:11:56'),
(280, 212, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:13:22', '2018-09-08 05:13:22'),
(281, 216, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:14:58', '2018-09-08 05:14:58'),
(282, 214, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:15:03', '2018-09-08 05:15:03'),
(283, 215, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:16:06', '2018-09-08 05:16:06'),
(284, 217, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:20:20', '2018-09-08 05:20:20'),
(285, 218, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:22:38', '2018-09-08 05:22:38'),
(286, 219, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:24:25', '2018-09-08 05:24:25'),
(287, 220, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:24:56', '2018-09-08 05:24:56'),
(288, 221, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:25:21', '2018-09-08 05:25:21'),
(289, 222, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:25:46', '2018-09-08 05:25:46'),
(290, 223, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:26:47', '2018-09-08 05:26:47'),
(291, 224, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:27:08', '2018-09-08 05:27:08'),
(292, 225, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:27:27', '2018-09-08 05:27:27'),
(293, 226, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:27:53', '2018-09-08 05:27:53'),
(294, 227, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:28:27', '2018-09-08 05:28:27'),
(295, 228, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:28:57', '2018-09-08 05:28:57'),
(296, 229, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:29:14', '2018-09-08 05:29:14'),
(297, 230, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:29:32', '2018-09-08 05:29:32'),
(298, 231, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:30:09', '2018-09-08 05:30:09'),
(299, 232, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:30:57', '2018-09-08 05:30:57'),
(300, 233, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:31:34', '2018-09-08 05:31:34'),
(301, 234, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:32:18', '2018-09-08 05:32:18'),
(302, 235, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:32:51', '2018-09-08 05:32:51'),
(303, 236, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:33:31', '2018-09-08 05:33:31'),
(304, 237, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:33:55', '2018-09-08 05:33:55'),
(305, 238, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:34:21', '2018-09-08 05:34:21'),
(306, 239, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:34:57', '2018-09-08 05:34:57'),
(307, 240, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:35:18', '2018-09-08 05:35:18'),
(308, 241, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:35:50', '2018-09-08 05:35:50'),
(309, 242, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:36:07', '2018-09-08 05:36:07'),
(310, 243, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:36:29', '2018-09-08 05:36:29'),
(311, 244, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:38:22', '2018-09-08 05:38:22'),
(312, 245, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:38:50', '2018-09-08 05:38:50'),
(313, 246, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:39:23', '2018-09-08 05:39:23'),
(314, 247, 18, NULL, 24, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:39:58', '2018-09-08 05:39:58'),
(315, 248, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:41:01', '2018-09-08 05:41:01'),
(316, 249, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:42:04', '2018-09-08 05:42:04'),
(317, 250, 18, NULL, 25, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:42:58', '2018-09-08 05:42:58'),
(318, 251, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:43:47', '2018-09-08 05:43:47'),
(319, 252, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:45:13', '2018-09-08 05:45:13'),
(320, 253, 18, NULL, 24, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:45:46', '2018-09-08 05:45:46'),
(321, 254, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:46:12', '2018-09-08 05:46:12'),
(322, 255, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:46:40', '2018-09-08 05:46:40'),
(323, 256, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:47:56', '2018-09-08 05:47:56'),
(324, 257, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:49:29', '2018-09-08 05:49:29'),
(325, 258, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:50:46', '2018-09-08 05:50:46'),
(326, 259, 18, NULL, 24, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:51:57', '2018-09-08 05:51:57'),
(327, 260, 18, NULL, 24, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:53:31', '2018-09-08 05:53:31'),
(328, 261, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:57:20', '2018-09-08 05:57:20'),
(329, 262, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:58:00', '2018-09-08 05:58:00'),
(330, 263, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 05:58:47', '2018-09-08 05:58:47'),
(333, 265, 4, '2018-06-01', 7, '2018-06-01', 1, '1', 1, NULL, 13, 13, '1', '1', '1', 1, 1, 0, 0, '2018-10-09 06:52:42', '2018-10-09 06:52:42'),
(334, 266, 4, '2018-11-04', 11, '2018-11-04', 1, 'trtrwe55bb666', 1, NULL, 13, 15, '5', '56', '2', 1, 1, 0, 0, '2018-09-29 07:36:36', '2018-09-29 07:36:36'),
(335, 267, 19, '1970-01-01', 30, '1970-01-01', 1, 'Test', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 11:40:04', '2018-09-08 11:40:04'),
(336, 268, 19, '2018-09-30', 29, '2019-02-01', NULL, 'Test', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-08 11:33:00', '2018-09-08 11:33:00'),
(337, 269, 19, NULL, 30, '2018-10-06', NULL, NULL, 0, NULL, 27, 28, '1', '1.2', '1.3', 1, 1, 0, 0, '2018-09-08 11:33:43', '2018-09-08 11:33:43'),
(338, 270, 18, '1970-01-01', 23, '2018-09-09', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-09 08:23:00', '2018-09-09 08:23:00'),
(339, 271, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-09 09:21:06', '2018-09-09 09:21:06'),
(340, 272, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-09 09:22:04', '2018-09-09 09:22:04'),
(341, 273, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-09 09:24:32', '2018-09-09 09:24:32'),
(342, 274, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-09 09:25:11', '2018-09-09 09:25:11'),
(343, 275, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-09 09:25:52', '2018-09-09 09:25:52'),
(344, 276, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-09 09:26:26', '2018-09-09 09:26:26'),
(345, 277, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-09 09:27:04', '2018-09-09 09:27:04'),
(346, 278, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-09 09:28:02', '2018-09-09 09:28:02'),
(347, 279, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-09 09:28:45', '2018-09-09 09:28:45'),
(348, 280, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-09 09:29:28', '2018-09-09 09:29:28'),
(349, 281, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-09 09:41:17', '2018-09-09 09:41:17'),
(350, 282, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-09 09:42:05', '2018-09-09 09:42:05'),
(351, 283, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-09 09:42:43', '2018-09-09 09:42:43'),
(352, 284, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-09 09:43:54', '2018-09-09 09:43:54'),
(353, 104, 18, '2018-09-08', 23, '2018-09-07', NULL, 'HONBLE ARUN BHANSALI\r\n\r\n\r\n07/09/2018\r\n\r\n\r\nLearned counsel for the petitioner prays for time.\r\n\r\n\r\n\r\nList on 14.09.2018', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-09 17:34:42', '2018-09-09 17:34:42'),
(354, 285, 22, NULL, 34, '1970-01-01', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-18 14:24:08', '2018-09-18 14:24:08'),
(355, 286, 22, NULL, 33, '1970-01-01', NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 11:41:54', '2018-09-19 11:41:54'),
(356, 287, 22, NULL, 31, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-10 07:28:09', '2018-09-10 07:28:09'),
(357, 288, 22, NULL, 33, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-10 07:36:32', '2018-09-10 07:36:32'),
(358, 289, 22, NULL, 32, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-10 07:47:41', '2018-09-10 07:47:41'),
(360, 291, 22, NULL, 34, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-10 07:51:36', '2018-09-10 07:51:36'),
(361, 292, 22, NULL, 31, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2018-09-16 12:31:47', '2018-09-16 12:31:47'),
(362, 293, 22, NULL, 32, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-10 07:54:33', '2018-09-10 07:54:33'),
(363, 294, 22, '2018-09-01', 31, '2018-09-10', NULL, 'CNL', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-16 12:39:53', '2018-09-16 12:39:53'),
(364, 295, 22, NULL, 34, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2018-09-13 05:37:35', '2018-09-13 05:37:35'),
(365, 296, 22, NULL, 31, '2018-09-10', NULL, NULL, 0, NULL, 32, 30, '7', '35', '88', 1, 1, 0, 0, '2018-09-10 09:45:21', '2018-09-10 09:45:21'),
(366, 297, 22, NULL, 34, '2018-09-10', NULL, NULL, 0, NULL, 31, 29, '2', '88', '26', 1, 1, 0, 0, '2018-09-10 09:44:23', '2018-09-10 09:44:23'),
(367, 298, 22, NULL, 33, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2018-09-19 11:39:47', '2018-09-19 11:39:47'),
(368, 299, 22, NULL, 32, '2018-09-10', NULL, NULL, 0, NULL, 30, 31, '3', '1', '7', 1, 0, 0, 0, '2018-09-16 13:48:59', '2018-09-16 13:48:59'),
(369, 300, 22, NULL, 31, '2018-09-10', NULL, 'PUTUP ON AFTER TWO WEEK', 0, NULL, 29, 31, '1', '3', '5', 1, 1, 0, 0, '2018-09-14 05:53:54', '2018-09-14 05:53:54'),
(370, 301, 22, '2018-09-10', 32, '2018-09-20', 1, 'HEARD,THE PETITION IS DECIDED IN FAVOUR', 0, NULL, 29, 30, '1', '1', '1', 1, 1, 0, 0, '2018-09-14 05:57:28', '2018-09-14 05:57:28'),
(371, 302, 22, '2018-09-10', 34, '2018-09-15', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-10 09:07:23', '2018-09-10 09:07:23'),
(372, 298, 22, '2018-09-17', 33, '2018-09-17', NULL, 'FOR HELD UP CASENEXT DATE 17.09.18', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2018-09-19 11:39:47', '2018-09-19 11:39:47'),
(373, 295, 22, '1970-01-01', 34, '2018-09-17', NULL, 'FOR HEARINGNEXT DATE 17.09.18', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-13 05:50:17', '2018-09-13 05:50:17'),
(374, 292, 22, '1970-01-01', 31, '2018-09-17', NULL, 'FOR REPLY NEXT DATE 17.09.18', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2018-09-16 12:31:47', '2018-09-16 12:31:47'),
(375, 290, 22, '1970-01-01', 32, '2018-09-17', NULL, 'FOR ORDER NEXT DATE 17.09.18', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2018-10-07 10:36:40', '2018-10-07 10:36:40'),
(376, 303, 22, '2018-09-18', 31, '2018-09-19', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2018-09-16 11:59:43', '2018-09-16 11:59:43'),
(377, 304, 22, NULL, 31, '2018-10-10', NULL, NULL, 0, NULL, 31, 30, '9', '9', '85', 1, 1, 0, 0, '2018-09-16 15:48:07', '2018-09-16 15:48:07'),
(378, 305, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-16 08:08:05', '2018-09-16 08:08:05'),
(379, 306, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-16 08:10:55', '2018-09-16 08:10:55'),
(380, 307, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-16 08:14:50', '2018-09-16 08:14:50'),
(381, 308, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-16 08:14:50', '2018-09-16 08:14:50'),
(382, 309, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-16 08:17:16', '2018-09-16 08:17:16'),
(383, 303, 22, '2018-09-25', 31, '2018-09-28', NULL, NULL, 0, NULL, 29, 29, '12', '13', '135', 1, 1, 0, 0, '2018-09-16 15:49:08', '2018-09-16 15:49:08'),
(384, 310, 22, '2018-09-20', 31, '2018-09-15', 1, NULL, 0, NULL, 29, 29, '7', '34', '876', 1, 1, 0, 0, '2018-09-16 12:50:46', '2018-09-16 12:50:46'),
(385, 292, 22, '2018-09-17', 31, '2018-09-20', NULL, 'FOR REPLY NEXT DATE 17.09.18', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-16 12:31:47', '2018-09-16 12:31:47'),
(386, 311, 22, '1970-01-01', 31, '2018-10-10', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-16 15:43:14', '2018-09-16 15:43:14'),
(387, 312, 22, '1970-01-01', 31, '2018-09-30', NULL, NULL, 0, NULL, 30, 30, '7', '43', '9743', 1, 1, 0, 0, '2018-09-16 13:20:00', '2018-09-16 13:20:00'),
(388, 313, 22, '2018-09-25', 31, '2018-09-30', NULL, NULL, 0, NULL, 30, 30, '7', '90', '980', 1, 1, 0, 0, '2018-09-18 05:12:41', '2018-09-18 05:12:41'),
(389, 299, 22, '2018-09-11', 32, '2018-09-13', NULL, NULL, 0, NULL, 30, NULL, '3', '1', '7', NULL, 1, 0, 0, '2018-09-16 13:48:59', '2018-09-16 13:48:59'),
(390, 314, 22, '2018-10-01', 31, '2018-11-01', 1, NULL, 1, NULL, 31, 30, '3', '132', '1334', 1, 1, 0, 0, '2018-09-17 05:25:34', '2018-09-17 05:25:34'),
(391, 315, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-17 07:33:18', '2018-09-17 07:33:18'),
(392, 316, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-17 07:33:56', '2018-09-17 07:33:56'),
(393, 317, 22, NULL, 31, '2018-11-11', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-18 12:34:33', '2018-09-18 12:34:33'),
(394, 318, 22, '2017-10-29', 31, '2018-11-12', NULL, NULL, 0, NULL, 30, 29, '9', '9', '9', 1, 1, 0, 0, '2018-09-18 13:01:21', '2018-09-18 13:01:21'),
(395, 319, 22, '2018-11-13', 31, '2018-11-11', NULL, NULL, 0, NULL, 29, 30, '9', '9', '77', 1, 1, 0, 0, '2018-09-19 10:27:02', '2018-09-19 10:27:02'),
(396, 320, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-18 17:45:10', '2018-09-18 17:45:10'),
(397, 321, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-18 17:49:13', '2018-09-18 17:49:13'),
(398, 322, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-18 17:50:07', '2018-09-18 17:50:07'),
(399, 323, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-18 17:50:45', '2018-09-18 17:50:45'),
(400, 324, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-18 17:53:53', '2018-09-18 17:53:53'),
(401, 325, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-18 17:56:57', '2018-09-18 17:56:57'),
(402, 326, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-18 18:01:07', '2018-09-18 18:01:07'),
(403, 327, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-18 18:06:26', '2018-09-18 18:06:26'),
(404, 328, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-18 18:07:32', '2018-09-18 18:07:32'),
(405, 329, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-18 18:08:37', '2018-09-18 18:08:37'),
(406, 330, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 04:28:30', '2018-09-19 04:28:30'),
(407, 331, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 04:31:00', '2018-09-19 04:31:00'),
(408, 332, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 04:32:17', '2018-09-19 04:32:17'),
(409, 333, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 04:34:00', '2018-09-19 04:34:00'),
(410, 334, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 04:35:46', '2018-09-19 04:35:46');
INSERT INTO `adv_pessi` (`pessi_id`, `pessi_case_reg`, `pessi_user_id`, `pessi_prev_date`, `pessi_statge_id`, `pessi_further_date`, `pessi_decide_id`, `pessi_order_sheet`, `pessi_choose_type`, `pessi_due_course`, `honaurable_justice`, `honarable_justice_db`, `court_number`, `serial_number`, `page_number`, `cause_status`, `pessi_status`, `pessi_update`, `pessi_sms_status`, `created_at`, `updated_at`) VALUES
(411, 335, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 04:37:10', '2018-09-19 04:37:10'),
(412, 336, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 04:38:32', '2018-09-19 04:38:32'),
(413, 337, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 04:39:43', '2018-09-19 04:39:43'),
(414, 338, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 04:41:03', '2018-09-19 04:41:03'),
(415, 339, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 04:42:20', '2018-09-19 04:42:20'),
(416, 340, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 04:43:54', '2018-09-19 04:43:54'),
(417, 341, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 04:47:07', '2018-09-19 04:47:07'),
(418, 342, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 04:49:35', '2018-09-19 04:49:35'),
(419, 343, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 04:50:58', '2018-09-19 04:50:58'),
(420, 344, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 04:52:39', '2018-09-19 04:52:39'),
(421, 345, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 04:59:25', '2018-09-19 04:59:25'),
(422, 346, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 05:01:16', '2018-09-19 05:01:16'),
(423, 347, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 05:06:45', '2018-09-19 05:06:45'),
(424, 348, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 05:10:14', '2018-09-19 05:10:14'),
(425, 349, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 05:12:20', '2018-09-19 05:12:20'),
(426, 350, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 05:13:52', '2018-09-19 05:13:52'),
(427, 351, 22, '1970-01-01', 31, '2018-12-21', NULL, 'rhrhrhrhrhrhhrhhh', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 13:29:17', '2018-09-19 13:29:17'),
(428, 298, 22, '2018-09-17', 33, '2018-11-11', NULL, 'FOR HELD UP CASENEXT DATE 17.09.18', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 11:39:47', '2018-09-19 11:39:47'),
(429, 352, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 17:21:36', '2018-09-19 17:21:36'),
(430, 353, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 17:22:58', '2018-09-19 17:22:58'),
(431, 354, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 17:24:38', '2018-09-19 17:24:38'),
(432, 355, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 17:26:11', '2018-09-19 17:26:11'),
(433, 356, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 17:27:24', '2018-09-19 17:27:24'),
(434, 357, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 17:28:41', '2018-09-19 17:28:41'),
(435, 358, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 17:34:42', '2018-09-19 17:34:42'),
(436, 359, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 17:35:43', '2018-09-19 17:35:43'),
(437, 360, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 17:36:37', '2018-09-19 17:36:37'),
(438, 361, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 17:37:47', '2018-09-19 17:37:47'),
(439, 362, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 17:38:19', '2018-09-19 17:38:19'),
(440, 363, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 17:39:35', '2018-09-19 17:39:35'),
(441, 364, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 17:40:20', '2018-09-19 17:40:20'),
(442, 365, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 17:41:19', '2018-09-19 17:41:19'),
(443, 366, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 17:43:13', '2018-09-19 17:43:13'),
(444, 367, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 17:44:13', '2018-09-19 17:44:13'),
(445, 368, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 17:44:58', '2018-09-19 17:44:58'),
(446, 369, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 17:46:23', '2018-09-19 17:46:23'),
(447, 370, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 17:48:19', '2018-09-19 17:48:19'),
(448, 371, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 17:49:36', '2018-09-19 17:49:36'),
(449, 372, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 17:50:47', '2018-09-19 17:50:47'),
(450, 373, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 17:50:52', '2018-09-19 17:50:52'),
(451, 374, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-19 17:53:10', '2018-09-19 17:53:10'),
(452, 375, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 09:24:30', '2018-09-20 09:24:30'),
(453, 376, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 09:26:23', '2018-09-20 09:26:23'),
(454, 377, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 09:27:22', '2018-09-20 09:27:22'),
(455, 378, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 09:28:00', '2018-09-20 09:28:00'),
(456, 379, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 09:28:48', '2018-09-20 09:28:48'),
(457, 380, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 09:29:34', '2018-09-20 09:29:34'),
(458, 381, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 09:30:10', '2018-09-20 09:30:10'),
(459, 382, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 09:31:03', '2018-09-20 09:31:03'),
(460, 383, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 09:31:59', '2018-09-20 09:31:59'),
(461, 384, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 09:33:03', '2018-09-20 09:33:03'),
(462, 385, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 09:33:49', '2018-09-20 09:33:49'),
(463, 386, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 09:34:33', '2018-09-20 09:34:33'),
(464, 387, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 09:34:34', '2018-09-20 09:34:34'),
(465, 388, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 09:38:45', '2018-09-20 09:38:45'),
(466, 389, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 09:39:42', '2018-09-20 09:39:42'),
(467, 390, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 09:40:42', '2018-09-20 09:40:42'),
(468, 391, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 09:41:20', '2018-09-20 09:41:20'),
(469, 392, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 09:42:28', '2018-09-20 09:42:28'),
(470, 393, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 09:43:37', '2018-09-20 09:43:37'),
(471, 394, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 09:44:53', '2018-09-20 09:44:53'),
(472, 395, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 09:44:54', '2018-09-20 09:44:54'),
(473, 396, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 09:49:31', '2018-09-20 09:49:31'),
(474, 397, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 09:50:24', '2018-09-20 09:50:24'),
(475, 398, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 09:51:04', '2018-09-20 09:51:04'),
(476, 399, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 09:52:01', '2018-09-20 09:52:01'),
(477, 400, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 09:58:41', '2018-09-20 09:58:41'),
(478, 401, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 10:00:20', '2018-09-20 10:00:20'),
(479, 402, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 10:04:45', '2018-09-20 10:04:45'),
(480, 403, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 10:06:48', '2018-09-20 10:06:48'),
(481, 404, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 10:07:42', '2018-09-20 10:07:42'),
(482, 405, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 10:09:38', '2018-09-20 10:09:38'),
(483, 406, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 10:12:57', '2018-09-20 10:12:57'),
(484, 407, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 10:13:48', '2018-09-20 10:13:48'),
(485, 408, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 10:14:37', '2018-09-20 10:14:37'),
(486, 409, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 10:16:17', '2018-09-20 10:16:17'),
(487, 410, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 10:21:15', '2018-09-20 10:21:15'),
(488, 411, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 10:23:39', '2018-09-20 10:23:39'),
(489, 412, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 10:25:49', '2018-09-20 10:25:49'),
(490, 413, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 10:26:31', '2018-09-20 10:26:31'),
(491, 414, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 10:27:09', '2018-09-20 10:27:09'),
(492, 415, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-20 10:27:42', '2018-09-20 10:27:42'),
(493, 416, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:04:04', '2018-09-23 07:04:04'),
(494, 417, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:04:46', '2018-09-23 07:04:46'),
(495, 418, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:05:24', '2018-09-23 07:05:24'),
(496, 419, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:06:04', '2018-09-23 07:06:04'),
(497, 420, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:06:52', '2018-09-23 07:06:52'),
(498, 421, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:07:38', '2018-09-23 07:07:38'),
(499, 422, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:08:13', '2018-09-23 07:08:13'),
(500, 423, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:09:55', '2018-09-23 07:09:55'),
(501, 424, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:10:52', '2018-09-23 07:10:52'),
(502, 425, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:11:44', '2018-09-23 07:11:44'),
(503, 426, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:12:31', '2018-09-23 07:12:31'),
(504, 427, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:13:18', '2018-09-23 07:13:18'),
(505, 428, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:14:04', '2018-09-23 07:14:04'),
(506, 429, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:14:42', '2018-09-23 07:14:42'),
(507, 430, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:16:43', '2018-09-23 07:16:43'),
(508, 431, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:17:28', '2018-09-23 07:17:28'),
(509, 432, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:18:34', '2018-09-23 07:18:34'),
(510, 433, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:19:32', '2018-09-23 07:19:32'),
(511, 434, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:20:06', '2018-09-23 07:20:06'),
(512, 435, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:20:42', '2018-09-23 07:20:42'),
(513, 436, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:22:57', '2018-09-23 07:22:57'),
(514, 437, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:23:36', '2018-09-23 07:23:36'),
(515, 438, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:24:06', '2018-09-23 07:24:06'),
(516, 439, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:24:41', '2018-09-23 07:24:41'),
(517, 440, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:25:26', '2018-09-23 07:25:26'),
(518, 441, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:26:19', '2018-09-23 07:26:19'),
(519, 442, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:26:44', '2018-09-23 07:26:44'),
(520, 443, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:27:10', '2018-09-23 07:27:10'),
(521, 444, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:27:42', '2018-09-23 07:27:42'),
(522, 445, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:28:19', '2018-09-23 07:28:19'),
(523, 446, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:28:55', '2018-09-23 07:28:55'),
(524, 447, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:30:43', '2018-09-23 07:30:43'),
(525, 448, 18, NULL, 24, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:31:13', '2018-09-23 07:31:13'),
(526, 449, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:31:41', '2018-09-23 07:31:41'),
(527, 450, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:32:23', '2018-09-23 07:32:23'),
(528, 451, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:32:51', '2018-09-23 07:32:51'),
(529, 452, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:34:12', '2018-09-23 07:34:12'),
(530, 453, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:35:05', '2018-09-23 07:35:05'),
(531, 454, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:35:37', '2018-09-23 07:35:37'),
(532, 455, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:36:07', '2018-09-23 07:36:07'),
(533, 456, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:36:43', '2018-09-23 07:36:43'),
(534, 457, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:37:28', '2018-09-23 07:37:28'),
(535, 458, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:45:31', '2018-09-23 07:45:31'),
(536, 459, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:46:30', '2018-09-23 07:46:30'),
(537, 460, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:47:10', '2018-09-23 07:47:10'),
(538, 461, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:48:40', '2018-09-23 07:48:40'),
(539, 462, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:49:41', '2018-09-23 07:49:41'),
(540, 463, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:50:11', '2018-09-23 07:50:11'),
(541, 464, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:50:38', '2018-09-23 07:50:38'),
(542, 465, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:51:08', '2018-09-23 07:51:08'),
(543, 466, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:53:31', '2018-09-23 07:53:31'),
(544, 467, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:54:15', '2018-09-23 07:54:15'),
(545, 468, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:54:56', '2018-09-23 07:54:56'),
(546, 469, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:56:16', '2018-09-23 07:56:16'),
(547, 470, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:56:46', '2018-09-23 07:56:46'),
(548, 471, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:57:16', '2018-09-23 07:57:16'),
(549, 472, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:58:10', '2018-09-23 07:58:10'),
(550, 473, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:58:36', '2018-09-23 07:58:36'),
(551, 474, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 07:59:15', '2018-09-23 07:59:15'),
(552, 475, 18, NULL, 23, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 08:00:17', '2018-09-23 08:00:17'),
(553, 476, 18, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-23 08:00:52', '2018-09-23 08:00:52'),
(554, 106, 15, '1970-01-01', 20, '2018-09-25', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-25 11:19:59', '2018-09-25 11:19:59'),
(555, 73, 15, '2018-05-25', 22, '2018-09-25', NULL, 'Early Listing Allowed\r\nPutup on after two week..', 0, NULL, 24, 23, '1', '1', '1', 1, 1, 0, 0, '2018-09-24 09:53:14', '2018-09-24 09:53:14'),
(556, 477, 22, NULL, 31, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-27 05:24:50', '2018-09-27 05:24:50'),
(557, 478, 22, NULL, 35, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-09-28 05:03:01', '2018-09-28 05:03:01'),
(558, 479, 22, '2018-10-07', 31, '2018-10-07', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-10-02 05:05:07', '2018-10-02 05:05:07'),
(559, 290, 22, '2018-09-17', 32, '2018-09-27', NULL, 'FOR ORDER NEXT DATE 17.09.18', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-10-07 10:36:40', '2018-10-07 10:36:40'),
(560, 68, 4, '1970-01-01', 7, '2018-10-25', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2019-09-01 09:29:28', '2019-09-01 09:29:28'),
(561, 480, 23, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-11-03 10:55:07', '2018-11-03 10:55:07'),
(562, 481, 23, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-11-03 10:55:38', '2018-11-03 10:55:38'),
(563, 68, 4, '2018-10-25', 12, '2018-11-09', NULL, NULL, 0, NULL, 13, 14, '1', '111', '21', 1, 0, 0, 0, '2019-09-01 09:29:28', '2019-09-01 09:29:28'),
(564, 482, 4, NULL, NULL, '2018-11-09', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2019-08-24 04:33:59', '2019-08-24 04:33:59'),
(565, 483, 4, NULL, 11, '2018-11-30', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2018-11-30 12:19:47', '2018-11-30 12:19:47'),
(566, 484, 4, '1970-01-01', 12, '2018-11-30', NULL, 'Test', 0, NULL, 15, 13, '2', '22', '222', 1, 0, 0, 0, '2019-09-01 09:28:54', '2019-09-01 09:28:54'),
(567, 485, 4, '2018-11-30', 3, '2018-12-04', NULL, 'Hiee', 0, NULL, 13, 14, '1', '10', '111', 1, 0, 0, 0, '2018-12-18 11:47:51', '2018-12-18 11:47:51'),
(568, 486, 4, NULL, 7, '2018-12-01', NULL, '2', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2018-12-18 13:02:26', '2018-12-18 13:02:26'),
(570, 484, 4, '2018-11-30', 12, '2018-12-05', NULL, 'PUTUP ON AFTER ONE WEEKPREMTORY ORDER..TWO WEEK TIME REMOVED DEFECT OTHERWISE WRIT PETITION IS DISMISSED WITHOUT REFERENCE TO THE  COURT THANK YOU', 2, NULL, 15, NULL, '2', '22', '224', 1, 0, 1, 0, '2019-09-01 09:28:54', '2019-09-01 09:28:54'),
(571, 482, 4, '2018-11-09', 7, '2018-12-11', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2019-08-24 04:33:59', '2019-08-24 04:33:59'),
(572, 488, 4, '2018-12-02', 12, '2018-12-03', NULL, 'Okay', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2018-12-18 13:01:14', '2018-12-18 13:01:14'),
(573, 485, 4, '2018-12-04', 7, '2018-12-05', NULL, 'Hiee gt fdgfd', 0, NULL, 13, NULL, '1', '10', '111', NULL, 1, 0, 0, '2018-12-18 11:47:51', '2018-12-18 11:47:51'),
(574, 489, 4, '2018-12-06', 11, '2018-12-12', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2018-12-17 05:49:49', '2018-12-17 05:49:49'),
(575, 488, 4, '2018-12-08', 8, '2018-12-08', 1, 'done', 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2018-12-18 13:01:14', '2018-12-18 13:01:14'),
(576, 489, 4, '2018-12-12', 11, '2018-12-12', NULL, 'STAY TO CONTINUE. PUTUP ON AFTER TWO WEEK\r\nTWO WEEK TIME FOR FILE REPLY\r\nLRS RECORD DASTI, NOTICE AWAIT FILE IN DUE COURSE\r\nSO PETITIONER IS NOT FILED MEMO APPLICATION', 0, NULL, 13, 14, '1', '111', '21', 1, 0, 0, 0, '2018-12-17 05:57:49', '2018-12-17 05:57:49'),
(577, 490, 29, NULL, 36, '1970-01-01', NULL, 'test', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2018-12-14 13:40:38', '2018-12-14 13:40:38'),
(578, 490, 29, '1970-01-01', 36, '2018-12-14', NULL, 'test', 2, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 1, '2018-12-14 13:40:38', '2018-12-14 13:40:38'),
(579, 490, 29, '2018-12-14', 36, '2018-12-15', NULL, NULL, 0, NULL, 33, 33, NULL, NULL, NULL, 1, 1, 0, 0, '2018-12-14 13:43:23', '2018-12-14 13:43:23'),
(580, 491, 29, NULL, 36, '1970-01-01', NULL, 'test', 0, NULL, 33, 33, NULL, NULL, NULL, 1, 0, 0, 0, '2018-12-14 13:47:54', '2018-12-14 13:47:54'),
(581, 491, 29, '1970-01-01', 36, '2018-12-15', NULL, NULL, 0, NULL, 33, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2018-12-14 13:47:54', '2018-12-14 13:47:54'),
(582, 491, 29, '1970-01-01', 36, '2018-12-15', NULL, NULL, 0, NULL, 33, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2018-12-14 13:47:54', '2018-12-14 13:47:54'),
(583, 491, 29, '1970-01-01', 36, '2018-12-15', NULL, NULL, 0, NULL, 33, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2018-12-14 13:47:54', '2018-12-14 13:47:54'),
(584, 491, 29, '1970-01-01', 36, '2018-12-15', NULL, NULL, 0, NULL, 33, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2018-12-14 13:47:54', '2018-12-14 13:47:54'),
(585, 491, 29, '1970-01-01', 36, '2018-12-15', NULL, NULL, 0, NULL, 33, NULL, NULL, NULL, NULL, 1, 1, 0, 0, '2018-12-14 13:48:03', '2018-12-14 13:48:03'),
(586, 68, 4, '2018-11-09', 12, '2018-12-16', NULL, 'PUTUP ON AFTER ONE WEEK', 2, NULL, 13, NULL, '1', '121', '31', 1, 0, 1, 0, '2019-09-01 09:29:28', '2019-09-01 09:29:28'),
(587, 489, 4, '2018-12-15', 11, '2018-12-12', NULL, NULL, 1, NULL, 13, NULL, '1', '111', '21', 0, 1, 1, 0, '2018-12-18 08:28:59', '2018-12-18 08:28:59'),
(589, 492, 4, '2018-12-19', 11, '2018-12-11', NULL, 'Case disposal pendingssdgfgf fdgsfdgh dfgsdg', 0, NULL, 13, 14, '1234', '78', '12', 1, 0, 1, 0, '2019-08-27 16:04:54', '2019-08-27 16:04:54'),
(590, 487, 4, '2018-12-31', 7, '2018-12-31', NULL, '78', 2, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2018-12-18 12:40:44', '2018-12-18 12:40:44'),
(591, 486, 4, '2018-12-01', 8, '2018-12-12', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2018-12-18 13:02:26', '2018-12-18 13:02:26'),
(592, 488, 4, '2018-12-08', 8, '2018-12-10', 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2018-12-18 13:01:14', '2018-12-18 13:01:14'),
(593, 488, 4, '2018-12-10', 12, '2018-12-18', 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2018-12-18 13:01:14', '2018-12-18 13:01:14'),
(594, 488, 4, '2018-12-18', 12, '2018-12-03', 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 1, 1, '2018-12-18 13:09:25', '2018-12-18 13:09:25'),
(595, 486, 4, '2018-12-12', 8, '2018-12-18', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2018-12-18 13:02:26', '2018-12-18 13:02:26'),
(596, 493, 30, NULL, 38, '2018-12-19', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2018-12-18 13:04:26', '2018-12-18 13:04:26'),
(597, 494, 30, NULL, 39, '2018-12-19', NULL, 'test', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2018-12-31 11:03:14', '2018-12-31 11:03:14'),
(598, 494, 30, '1970-01-01', 39, '2018-12-18', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2018-12-31 11:03:14', '2018-12-31 11:03:14'),
(599, 495, 31, NULL, 42, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-07-20 07:42:53', '2019-07-20 07:42:53'),
(600, 496, 31, '2018-12-01', 42, '2018-12-03', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-04-11 08:21:19', '2019-04-11 08:21:19'),
(601, 497, 31, NULL, 43, '2018-12-05', NULL, 'ANOTHER BENCHAPPOSITE COUNSEL IS NOT APPEAR BEHALF OF RESPONDENT NO 2,3. TIME ALLOWED PUTUP ON AFTER TWO WEEK', 1, NULL, 39, 38, '10', '11', '2', 1, 1, 1, 0, '2019-01-08 05:43:23', '2019-01-08 05:43:23'),
(602, 496, 31, '2018-12-03', 43, '2018-12-05', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-04-11 08:21:19', '2019-04-11 08:21:19'),
(604, 498, 31, '2018-12-06', 44, '2018-12-06', NULL, 'JAI SHREE KRISHNA', NULL, NULL, 38, NULL, '1', '11', '111', 1, 1, 0, 0, '2019-04-11 07:56:48', '2019-04-11 07:56:48'),
(605, 495, 31, '1970-01-01', 42, '2018-12-05', 1, NULL, 0, NULL, 38, 39, '7', '1', '777', 1, 0, 1, 0, '2019-07-20 07:42:53', '2019-07-20 07:42:53'),
(606, 499, 31, '2018-12-03', 42, '2018-12-03', NULL, 'hhghg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-04-07 04:53:19', '2019-04-07 04:53:19'),
(608, 500, 31, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2018-12-21 06:07:14', '2018-12-21 06:07:14'),
(612, 502, 31, '2018-12-25', 44, '2018-12-26', 1, 'xxxxxxxxxxxxxxx', 0, NULL, 40, 39, '1', '11', '111', 1, 0, 1, 0, '2019-04-06 04:51:16', '2019-04-06 04:51:16'),
(613, 503, 30, '2018-12-25', 41, '2018-12-24', NULL, NULL, 0, NULL, 36, 37, '100', '2', '2', 1, 1, 0, 0, '2018-12-31 11:05:34', '2018-12-31 11:05:34'),
(614, 494, 30, '2018-12-18', 39, '2018-12-20', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2018-12-31 11:03:14', '2018-12-31 11:03:14'),
(615, 494, 30, '2018-12-18', 39, '2018-12-26', NULL, 'dfjhdf sdfhsdf', 0, NULL, NULL, NULL, '15', '1', '12', 1, 0, 1, 0, '2018-12-31 11:03:14', '2018-12-31 11:03:14'),
(616, 504, 30, NULL, 39, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2018-12-31 11:02:46', '2018-12-31 11:02:46'),
(617, 504, 30, '1970-01-01', 39, '2018-12-15', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2018-12-31 11:02:46', '2018-12-31 11:02:46'),
(618, 505, 30, NULL, 39, '2018-12-16', NULL, NULL, 0, NULL, 35, 35, '12', '12', '12', 1, 0, 0, 0, '2018-12-31 11:02:38', '2018-12-31 11:02:38'),
(619, 505, 30, '2018-12-18', 39, '2018-12-18', NULL, 'test test cffs', NULL, NULL, 35, 36, '12', '12', '12', 1, 0, 0, 0, '2018-12-31 11:02:38', '2018-12-31 11:02:38'),
(621, 506, 31, '1970-01-01', 42, '2018-12-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-01-03 05:16:40', '2019-01-03 05:16:40'),
(622, 507, 30, NULL, 38, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2018-12-31 11:02:31', '2018-12-31 11:02:31'),
(623, 507, 30, '1970-01-01', 39, '2018-12-10', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 0, '2018-12-31 11:02:31', '2018-12-31 11:02:31'),
(624, 505, 30, '2018-12-18', 39, '2018-12-31', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2018-12-31 11:02:38', '2018-12-31 11:02:38'),
(625, 507, 30, '1970-01-01', 39, '2018-12-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2018-12-31 11:02:31', '2018-12-31 11:02:31'),
(626, 507, 30, '2018-12-01', 39, '2018-12-21', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2018-12-31 11:02:31', '2018-12-31 11:02:31'),
(627, 507, 30, '2018-12-21', 39, '2019-01-24', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2018-12-31 11:02:31', '2018-12-31 11:02:31'),
(628, 507, 30, '2018-12-21', 40, '2019-01-23', NULL, NULL, 0, NULL, 35, 37, '1', '1', '1', 1, 0, 0, 0, '2018-12-31 11:02:31', '2018-12-31 11:02:31'),
(629, 507, 30, '2019-01-23', 40, '2019-01-24', NULL, NULL, 0, NULL, 35, 37, '1', '1', '1', 1, 0, 0, 0, '2018-12-31 11:02:31', '2018-12-31 11:02:31'),
(630, 507, 30, '2019-01-24', 40, '2019-01-25', NULL, NULL, 0, NULL, 35, 37, '5', '1', '6', 1, 1, 0, 0, '2018-12-31 11:04:12', '2018-12-31 11:04:12'),
(631, 505, 30, '2018-12-18', 39, '2018-12-25', 3, 'test', 0, NULL, 37, 37, '7', '5', '2', 1, 1, 1, 0, '2018-12-31 13:01:35', '2018-12-31 13:01:35'),
(632, 504, 30, '2018-12-15', 39, '2018-12-25', NULL, NULL, 0, NULL, 36, 36, '2', '4', '4', 1, 1, 0, 0, '2018-12-31 11:05:17', '2018-12-31 11:05:17'),
(633, 494, 30, '2018-12-18', 39, '2018-12-25', NULL, NULL, 0, NULL, 35, 36, '15', '1', '12', 1, 1, 0, 0, '2018-12-31 11:05:44', '2018-12-31 11:05:44'),
(634, 508, 30, NULL, 38, '2018-12-31', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2018-12-31 13:07:18', '2018-12-31 13:07:18'),
(636, 509, 31, '2018-12-15', 42, '2018-12-15', NULL, 'aaaaaaaaaaaaaaaaaaaaaa', NULL, NULL, 38, 39, '1', '11', '111', 1, 1, 0, 0, '2019-04-06 04:41:22', '2019-04-06 04:41:22'),
(637, 506, 31, '2018-12-15', 44, '2018-12-15', NULL, 'ssssssssssssssssssssssssssssssssssssssss', NULL, NULL, 40, 38, '2', '22', '222', 1, 1, 0, 0, '2019-04-06 04:45:40', '2019-04-06 04:45:40'),
(639, 510, 33, NULL, 45, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-01-04 12:08:46', '2019-01-04 12:08:46'),
(640, 511, 33, NULL, 45, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-01-04 12:18:18', '2019-01-04 12:18:18'),
(641, 512, 33, NULL, 45, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-01-04 12:21:39', '2019-01-04 12:21:39'),
(642, 513, 33, NULL, 45, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-01-04 12:22:15', '2019-01-04 12:22:15'),
(643, 514, 33, NULL, 45, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-01-04 12:23:00', '2019-01-04 12:23:00'),
(644, 515, 33, NULL, 45, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-01-04 12:43:27', '2019-01-04 12:43:27'),
(645, 516, 33, NULL, 45, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-01-04 12:58:27', '2019-01-04 12:58:27'),
(646, 517, 33, NULL, 46, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-01-04 12:59:33', '2019-01-04 12:59:33'),
(647, 518, 33, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-01-04 13:00:58', '2019-01-04 13:00:58'),
(648, 519, 33, NULL, 45, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-01-04 13:02:36', '2019-01-04 13:02:36'),
(649, 520, 33, NULL, 46, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-01-04 13:06:36', '2019-01-04 13:06:36'),
(650, 521, 33, NULL, 46, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-01-05 10:30:25', '2019-01-05 10:30:25'),
(651, 522, 33, NULL, 46, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-01-08 11:25:44', '2019-01-08 11:25:44'),
(652, 523, 33, NULL, 46, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-01-08 11:27:22', '2019-01-08 11:27:22'),
(653, 524, 33, NULL, 45, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-01-08 11:30:03', '2019-01-08 11:30:03'),
(654, 525, 33, NULL, 45, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-01-15 09:36:48', '2019-01-15 09:36:48'),
(655, 526, 33, NULL, 45, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-01-15 09:38:41', '2019-01-15 09:38:41'),
(656, 527, 33, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-01-15 09:40:36', '2019-01-15 09:40:36'),
(657, 528, 33, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-01-15 10:09:56', '2019-01-15 10:09:56'),
(658, 529, 33, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-01-15 10:10:50', '2019-01-15 10:10:50'),
(659, 530, 33, NULL, 46, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-01-15 10:12:32', '2019-01-15 10:12:32'),
(660, 531, 33, NULL, 46, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-01-15 10:13:44', '2019-01-15 10:13:44'),
(661, 532, 33, NULL, 46, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-01-15 10:19:50', '2019-01-15 10:19:50'),
(662, 533, 33, NULL, 46, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-01-15 10:20:23', '2019-01-15 10:20:23'),
(663, 534, 33, NULL, 46, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-01-15 10:21:33', '2019-01-15 10:21:33'),
(664, 535, 33, NULL, 46, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-01-15 10:22:10', '2019-01-15 10:22:10'),
(665, 536, 33, NULL, 46, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-01-15 10:23:16', '2019-01-15 10:23:16'),
(666, 537, 33, NULL, 45, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-01-15 10:24:57', '2019-01-15 10:24:57'),
(667, 538, 4, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-02-14 18:18:15', '2019-02-14 18:18:15'),
(668, 539, 33, NULL, 45, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-03-14 10:46:18', '2019-03-14 10:46:18'),
(669, 540, 33, NULL, 45, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-03-14 10:47:59', '2019-03-14 10:47:59'),
(670, 541, 33, NULL, 45, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-03-14 10:49:55', '2019-03-14 10:49:55'),
(671, 542, 33, NULL, 45, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-03-14 10:50:48', '2019-03-14 10:50:48'),
(672, 543, 33, NULL, 45, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-03-14 10:51:44', '2019-03-14 10:51:44'),
(673, 544, 33, NULL, 45, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-03-14 10:52:29', '2019-03-14 10:52:29'),
(674, 545, 33, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-03-14 11:05:18', '2019-03-14 11:05:18'),
(675, 546, 33, NULL, 45, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-03-14 11:06:11', '2019-03-14 11:06:11'),
(676, 547, 33, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-03-14 11:06:55', '2019-03-14 11:06:55'),
(677, 548, 33, NULL, 45, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-03-14 11:07:40', '2019-03-14 11:07:40'),
(678, 549, 33, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-03-14 11:08:43', '2019-03-14 11:08:43'),
(679, 550, 33, NULL, 45, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-03-14 11:09:40', '2019-03-14 11:09:40'),
(680, 551, 33, NULL, 45, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-03-14 11:11:02', '2019-03-14 11:11:02'),
(681, 552, 33, NULL, 45, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-03-14 11:11:49', '2019-03-14 11:11:49'),
(682, 553, 33, NULL, 45, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-03-14 11:12:32', '2019-03-14 11:12:32'),
(683, 554, 33, NULL, 45, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-03-14 11:23:32', '2019-03-14 11:23:32'),
(684, 555, 33, NULL, 45, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-03-14 11:24:16', '2019-03-14 11:24:16'),
(685, 556, 33, NULL, 46, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-03-14 11:25:39', '2019-03-14 11:25:39'),
(686, 557, 33, NULL, 45, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-03-14 11:26:31', '2019-03-14 11:26:31'),
(687, 558, 33, NULL, 46, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-03-14 11:28:17', '2019-03-14 11:28:17'),
(688, 559, 33, NULL, 45, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-03-14 11:33:22', '2019-03-14 11:33:22'),
(689, 560, 33, NULL, 47, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-03-14 11:34:33', '2019-03-14 11:34:33'),
(690, 561, 33, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-03-14 11:35:40', '2019-03-14 11:35:40'),
(691, 562, 33, NULL, 46, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-03-14 11:36:31', '2019-03-14 11:36:31'),
(692, 563, 33, NULL, 45, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-03-14 11:38:41', '2019-03-14 11:38:41'),
(693, 564, 33, NULL, 45, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-03-14 11:39:22', '2019-03-14 11:39:22'),
(694, 565, 33, NULL, 46, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-03-14 11:43:13', '2019-03-14 11:43:13'),
(695, 566, 33, NULL, 46, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-03-14 11:43:56', '2019-03-14 11:43:56'),
(696, 567, 33, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-03-14 11:44:46', '2019-03-14 11:44:46'),
(697, 568, 33, NULL, 46, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-03-14 11:47:09', '2019-03-14 11:47:09'),
(698, 569, 33, NULL, 46, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-03-14 11:47:55', '2019-03-14 11:47:55'),
(699, 570, 33, NULL, 46, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-03-14 11:50:59', '2019-03-14 11:50:59'),
(700, 571, 33, NULL, 45, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-03-18 08:47:18', '2019-03-18 08:47:18'),
(701, 572, 33, NULL, 46, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-03-18 08:53:55', '2019-03-18 08:53:55'),
(702, 502, 31, '2018-12-27', 44, '2018-12-27', 1, '27777777777777', NULL, NULL, 40, NULL, '1', '11', '111', 0, 0, 0, 0, '2019-04-06 04:53:05', '2019-04-06 04:53:05'),
(703, 502, 31, '2018-12-28', 44, '2018-12-28', 1, 'aaaaaaaaaaaaaaaaaaaaaa', NULL, NULL, 40, NULL, '1', '11', '111', 0, 1, 0, 0, '2019-04-06 04:52:08', '2019-04-06 04:52:08'),
(706, 501, 31, '2018-12-20', 42, '2018-12-20', NULL, 'aaaaaaaaaaaaaaa', NULL, NULL, 39, NULL, '3', '13', '11', 1, 1, 0, 0, '2019-04-07 04:51:35', '2019-04-07 04:51:35'),
(707, 573, 31, '2019-04-05', 42, '2019-04-05', NULL, 'PUTUP ON 06.04.19', NULL, NULL, 38, 39, '1', '11', '111', 1, 0, 0, 0, '2019-07-20 07:37:37', '2019-07-20 07:37:37'),
(708, 496, 31, '2019-04-05', 43, '2019-04-05', NULL, 'PUTUP ON 11.04.19 AND NOTICE SERVED', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-04-11 08:22:39', '2019-04-11 08:22:39'),
(710, 496, 31, '2019-04-05', 43, '2019-04-11', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-04-11 08:21:19', '2019-04-11 08:21:19'),
(711, 495, 31, '2018-12-05', 43, '2019-04-11', 1, 'REJOINDER TO BE FILED , TIME GRANTED', 0, NULL, 39, 39, '3', '3', '35', 1, 0, 1, 0, '2019-07-20 07:42:53', '2019-07-20 07:42:53'),
(714, 495, 31, '2019-04-11', 44, '2019-04-12', 1, NULL, 0, NULL, 39, 38, '7', '77', '777', 1, 0, 0, 0, '2019-07-20 07:42:53', '2019-07-20 07:42:53'),
(715, 573, 31, '2019-04-05', 42, '2019-04-06', NULL, NULL, 0, NULL, 38, NULL, '2', '22', '222', 1, 0, 1, 0, '2019-07-20 07:37:37', '2019-07-20 07:37:37'),
(716, 574, 31, '2019-04-01', 43, '2019-04-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-07-19 05:20:11', '2019-07-19 05:20:11'),
(717, 574, 31, '2019-04-01', 42, '2019-04-11', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-07-19 05:19:37', '2019-07-19 05:19:37'),
(718, 575, 31, '1970-01-01', 42, '1970-01-01', NULL, 'HONBLE JUSTICE VIRENDRA MATHUR J\r\nPUTUP ON AFTER TWO WEEK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-07-19 04:57:07', '2019-07-19 04:57:07'),
(719, 576, 31, NULL, 42, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-06-29 11:42:14', '2019-06-29 11:42:14'),
(720, 576, 31, '1970-01-01', 42, '2019-06-22', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-06-29 11:42:14', '2019-06-29 11:42:14'),
(721, 576, 31, '1970-01-01', 42, '2019-06-22', NULL, NULL, 0, NULL, 39, 38, '1', '11', '111', 1, 0, 0, 0, '2019-06-29 11:42:14', '2019-06-29 11:42:14'),
(722, 576, 31, '2019-06-22', 42, '2019-06-26', NULL, NULL, 0, NULL, 39, NULL, '1', '11', '111', 0, 0, 0, 0, '2019-06-29 11:42:14', '2019-06-29 11:42:14'),
(723, 576, 31, '2019-06-26', 42, '2019-06-27', NULL, 'HIIIIIII    HELLO', 0, NULL, 39, NULL, '1', '11', '111', 0, 0, 1, 0, '2019-06-29 11:42:14', '2019-06-29 11:42:14'),
(724, 576, 31, '2019-06-27', 42, '2019-06-29', NULL, NULL, 0, NULL, 39, NULL, '1', '11', '111', 0, 0, 0, 0, '2019-06-29 11:42:14', '2019-06-29 11:42:14'),
(725, 576, 31, '2019-06-30', 42, '2019-06-30', NULL, 'llll', NULL, NULL, 39, NULL, '1', '11', '111', 1, 1, 0, 0, '2019-06-29 11:49:41', '2019-06-29 11:49:41'),
(726, 573, 31, '2019-04-06', 42, '2019-06-29', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-07-20 07:37:37', '2019-07-20 07:37:37'),
(727, 573, 31, '2019-04-06', 42, '2019-06-29', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-07-20 07:37:37', '2019-07-20 07:37:37'),
(728, 495, 31, '2019-04-12', 44, '2019-06-29', 1, 'DECIDED ON 29.06.2019', 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-07-20 07:42:53', '2019-07-20 07:42:53'),
(729, 573, 31, '2019-06-29', 42, '2019-06-30', NULL, 'dECIDED ON 01.07.2019', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-07-20 07:37:37', '2019-07-20 07:37:37'),
(730, 577, 31, '2019-07-18', 43, '2019-07-18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-07-11 05:35:34', '2019-07-11 05:35:34'),
(731, 578, 31, '1970-01-01', 43, '1970-01-01', NULL, 'HONBLE GK VYAS J\r\nPUTUP ON AFTER TWO WEEK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-07-19 05:03:18', '2019-07-19 05:03:18'),
(732, 574, 31, '2019-04-11', 42, '2019-04-12', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-07-19 05:19:37', '2019-07-19 05:19:37'),
(733, 574, 31, '2019-04-15', 44, '2019-04-15', NULL, 'HIIII', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-07-19 05:20:39', '2019-07-19 05:20:39'),
(734, 579, 31, '2019-07-02', 42, '2019-07-02', NULL, 'PUTUP ON AFTER TWO WEEK', NULL, NULL, 38, 38, '1', '11', '111', 1, 1, 0, 0, '2019-07-20 07:29:57', '2019-07-20 07:29:57'),
(735, 573, 31, '2019-06-30', 42, '2019-07-01', 2, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-07-20 07:37:37', '2019-07-20 07:37:37'),
(737, 580, 4, NULL, 11, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-01 06:39:45', '2019-09-01 06:39:45'),
(738, 581, 4, NULL, 12, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-08-27 05:57:37', '2019-08-27 05:57:37'),
(739, 581, 4, '1970-01-01', 12, '2019-08-15', NULL, 'PUTUP ON 16.08.2019', 0, NULL, NULL, NULL, '1', '11', '111', 1, 0, 0, 0, '2019-08-27 05:57:37', '2019-08-27 05:57:37'),
(740, 580, 4, '1970-01-01', 11, '2019-08-15', NULL, NULL, 0, NULL, 15, 13, '2', '22', '222', 1, 0, 1, 0, '2019-09-01 06:39:45', '2019-09-01 06:39:45'),
(741, 482, 4, '2018-12-11', 7, '2018-12-15', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-08-24 04:33:59', '2019-08-24 04:33:59'),
(742, 482, 4, '2019-08-15', 11, '2019-08-15', NULL, 'YOGESH PUROHIT', 0, NULL, 15, 13, '2', '567', '333', 1, 1, 1, 0, '2019-08-31 08:14:41', '2019-08-27 16:02:49'),
(743, 581, 4, '2019-08-15', 12, '2019-08-28', NULL, NULL, 0, NULL, 13, 14, '1', '11', '111', 1, 1, 1, 0, '2019-08-28 05:12:24', '2019-08-28 05:12:24'),
(744, 580, 4, '2019-08-15', 11, '2019-08-28', NULL, 'aaaaaaaa', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 0, '2019-09-01 07:26:55', '2019-09-01 07:26:55'),
(745, 580, 4, '2019-08-15', 11, '2019-08-28', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-01 06:39:45', '2019-09-01 06:39:45'),
(746, 580, 4, '2019-08-15', 11, '2019-08-28', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-01 06:39:45', '2019-09-01 06:39:45'),
(747, 580, 4, '2019-08-15', 11, '2019-08-28', NULL, NULL, 0, NULL, 13, 9, '1rr', '11', '111', 1, 0, 1, 0, '2019-09-01 06:39:45', '2019-09-01 06:39:45');
INSERT INTO `adv_pessi` (`pessi_id`, `pessi_case_reg`, `pessi_user_id`, `pessi_prev_date`, `pessi_statge_id`, `pessi_further_date`, `pessi_decide_id`, `pessi_order_sheet`, `pessi_choose_type`, `pessi_due_course`, `honaurable_justice`, `honarable_justice_db`, `court_number`, `serial_number`, `page_number`, `cause_status`, `pessi_status`, `pessi_update`, `pessi_sms_status`, `created_at`, `updated_at`) VALUES
(748, 492, 4, '2019-12-15', 11, '2019-12-15', NULL, 'YOGESH PUROHIT', 0, NULL, 10, 10, '1', '11', '111', 1, 1, 1, 0, '2019-08-31 08:14:49', '2019-08-27 16:06:29'),
(749, 582, 4, NULL, 12, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-08-30 08:15:40', '2019-08-30 08:15:40'),
(750, 583, 4, NULL, NULL, '1970-01-01', NULL, 'YOGESH KUMAR PUROHIT', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-08-30 09:00:27', '2019-08-30 09:00:27'),
(751, 584, 4, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-08-28 06:51:20', '2019-08-28 06:51:20'),
(752, 585, 4, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-08-28 06:35:27', '2019-08-28 06:35:27'),
(753, 586, 4, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-08-28 06:36:01', '2019-08-28 06:36:01'),
(754, 587, 4, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-08-28 06:46:52', '2019-08-28 06:46:52'),
(755, 587, 4, '1970-01-01', 7, '2019-08-25', 1, 'DECIDED ON 25.08.19', 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 1, 0, '2019-08-28 06:46:52', '2019-08-28 06:46:52'),
(756, 584, 4, '1970-01-01', 7, '2019-08-24', 2, 'DISPOSED ON 24.08.19', 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 1, 0, '2019-08-28 06:51:20', '2019-08-28 06:51:20'),
(757, 588, 4, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-08-30 07:49:26', '2019-08-30 07:49:26'),
(758, 589, 4, NULL, 14, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-08-30 06:04:14', '2019-08-30 06:04:14'),
(759, 590, 4, '1970-01-01', 13, '1970-01-01', NULL, 'hiii', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-08-30 08:51:46', '2019-08-30 08:51:46'),
(760, 591, 4, NULL, 11, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-08-29 12:34:58', '2019-08-29 12:34:58'),
(761, 591, 4, '1970-01-01', 11, '2019-08-25', 1, 'dfdf', 1, NULL, 9, 10, 'C2', '2', '2', 1, 1, 1, 0, '2019-08-29 12:34:58', '2019-08-29 12:34:58'),
(762, 590, 4, '1970-01-01', 13, '2019-08-26', NULL, NULL, 0, NULL, 10, 13, '5', '55', '555', 1, 0, 0, 0, '2019-08-30 05:54:33', '2019-08-30 05:54:33'),
(764, 590, 4, '2019-08-27', 13, '2019-08-28', NULL, 'YOGESH KUMAR PURHIT AND DHEERAJ PUROHIT PHALODI (RAJ)', 0, NULL, 10, NULL, '5', '55', '555', 0, 1, 1, 0, '2019-09-01 06:35:25', '2019-09-01 06:35:25'),
(765, 589, 4, '1970-01-01', 14, '2019-08-21', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-08-30 06:04:14', '2019-08-30 06:04:14'),
(766, 589, 4, '2019-08-21', 12, '2019-08-22', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-08-30 06:04:14', '2019-08-30 06:04:14'),
(767, 589, 4, '2019-08-23', 13, '2019-08-23', NULL, 'JYOTSANA PUROHIT', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 1, 0, '2019-08-30 12:53:57', '2019-08-30 07:28:09'),
(768, 588, 4, '1970-01-01', 7, '2019-08-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-08-30 07:49:26', '2019-08-30 07:49:26'),
(769, 588, 4, '2019-08-01', 7, '2019-08-02', 1, 'DHEERAJ PUROHIT', 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 1, 0, '2019-08-30 08:14:34', '2019-08-30 08:14:34'),
(770, 582, 4, '1970-01-01', 12, '2019-08-28', 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 1, 0, '2019-08-30 08:16:16', '2019-08-30 08:16:16'),
(771, 592, 4, '1970-01-01', 8, '1970-01-01', NULL, 'HELLO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-01 06:44:08', '2019-09-01 06:44:08'),
(772, 592, 4, '1970-01-01', 8, '2019-08-05', NULL, '05.08.19 YOGESH KUMAR PUROHIT', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-01 06:44:08', '2019-09-01 06:44:08'),
(773, 592, 4, '2019-08-05', 8, '2019-08-06', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-01 06:44:08', '2019-09-01 06:44:08'),
(774, 592, 4, '2019-08-10', 8, '2019-08-10', NULL, 'HIII HELLLO', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-01 06:44:08', '2019-09-01 06:44:08'),
(775, 583, 4, '1970-01-01', 7, '2019-08-10', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-08-30 09:00:27', '2019-08-30 09:00:27'),
(776, 580, 4, '2019-08-28', 11, '2019-08-29', NULL, NULL, 0, NULL, 13, NULL, '1rr', '11', '111', 0, 1, 0, 0, '2019-09-01 06:39:45', '2019-09-01 06:39:45'),
(777, 592, 4, '2019-08-10', 8, '2019-08-11', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-09-01 06:44:08', '2019-09-01 06:44:08'),
(778, 593, 4, NULL, 13, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-01 06:49:01', '2019-09-01 06:49:01'),
(779, 593, 4, '1970-01-01', 13, '2019-08-15', NULL, NULL, 0, NULL, 13, 14, '10', '100', '1000', 1, 0, 0, 0, '2019-09-01 06:49:01', '2019-09-01 06:49:01'),
(780, 593, 4, '2019-08-16', 13, '2019-08-16', 1, 'JAI SHREE KRISHNA AND JAI SHREE RAAM', 1, NULL, 13, NULL, '10', '100', '1000', 0, 1, 1, 0, '2019-09-01 07:38:21', '2019-09-01 07:38:21'),
(781, 594, 4, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-02 07:59:02', '2019-09-02 07:59:02'),
(782, 594, 4, '1970-01-01', 11, '2019-08-05', NULL, NULL, 0, NULL, 13, 14, '15', '15', '155', 1, 0, 0, 0, '2019-09-02 07:59:02', '2019-09-02 07:59:02'),
(783, 594, 4, '2019-08-05', 11, '2019-08-06', 1, 'YOGESH KUMAR PUROHIT S/O MADHU SUDHAN PUROHIT', 1, NULL, 15, 14, '11', '11', '111', 1, 0, 1, 0, '2019-09-02 07:59:02', '2019-09-02 07:59:02'),
(784, 594, 4, '2019-08-06', 11, '2019-08-07', 1, NULL, 0, NULL, 15, NULL, '11', '11', '111', 0, 0, 0, 0, '2019-09-02 07:59:02', '2019-09-02 07:59:02'),
(785, 595, 4, NULL, 11, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-09-01 07:42:18', '2019-09-01 07:42:18'),
(786, 596, 4, NULL, 14, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-01 08:42:26', '2019-09-01 08:42:26'),
(787, 596, 4, '2019-09-01', 14, '2019-09-01', NULL, 'yjtyjykyk', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-01 08:43:29', '2019-09-01 08:43:29'),
(788, 596, 4, '2019-09-01', 14, '2019-09-02', NULL, 'yyyyyyyyy', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 0, '2019-09-01 08:42:26', '2019-09-01 08:42:26'),
(789, 596, 4, '2019-09-02', 14, '2019-09-03', NULL, 'kkkkkkkkkk', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 0, '2019-09-01 08:42:26', '2019-09-01 08:42:26'),
(791, 596, 4, '2019-09-05', 14, '2019-09-05', NULL, 'shivay purohit', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 0, '2019-09-01 08:42:26', '2019-09-01 08:42:26'),
(793, 596, 4, '2019-09-10', 11, '2019-09-10', NULL, 'hari om', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 0, '2019-09-10 07:29:46', '2019-09-10 07:29:46'),
(794, 596, 4, '2019-09-10', 7, '2019-09-15', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-09-01 08:42:26', '2019-09-01 08:42:26'),
(795, 597, 4, NULL, 7, '2019-09-02', NULL, 'HII EVERY ONWWWE', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 0, '2019-09-02 03:09:02', '2019-09-02 03:09:02'),
(796, 484, 4, '2018-12-05', 3, '2019-01-01', NULL, NULL, 0, NULL, 15, NULL, '2', '22', '224', 0, 1, 0, 0, '2019-09-01 09:28:54', '2019-09-01 09:28:54'),
(797, 68, 4, '2018-12-16', 3, '2018-12-31', NULL, NULL, 0, NULL, 13, NULL, '1', '121', '31', 0, 1, 0, 0, '2019-09-01 09:29:28', '2019-09-01 09:29:28'),
(798, 598, 4, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-01 10:05:15', '2019-09-01 10:05:15'),
(799, 598, 4, '1970-01-01', 11, '2019-09-05', NULL, NULL, 0, NULL, 13, 14, '7', '77', '777', 1, 0, 0, 0, '2019-09-01 10:05:15', '2019-09-01 10:05:15'),
(800, 598, 4, '2019-09-06', 11, '2019-09-06', NULL, 'NO PANCHRATAN 007', 0, NULL, 15, 13, '8', '88', '888', 1, 0, 1, 0, '2019-09-01 10:06:17', '2019-09-01 10:06:17'),
(801, 597, 4, '2019-09-02', 7, '2019-09-03', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-09-01 10:01:41', '2019-09-01 10:01:41'),
(802, 598, 4, '2019-09-06', 11, '2019-09-07', NULL, NULL, 0, NULL, 15, NULL, '8', '88', '888', 0, 1, 0, 0, '2019-09-01 10:05:15', '2019-09-01 10:05:15'),
(803, 599, 4, NULL, 12, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-02 03:14:43', '2019-09-02 03:14:43'),
(804, 599, 4, '1970-01-01', 12, '2019-09-03', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-09-02 03:14:43', '2019-09-02 03:14:43'),
(805, 600, 4, NULL, 7, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-02 06:25:12', '2019-09-02 06:25:12'),
(806, 600, 4, '2019-09-30', 7, '2019-09-30', NULL, '30-09-2019 LLLLLLLLLLLLLLLLL', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-02 06:25:12', '2019-09-02 06:25:12'),
(807, 600, 4, '2019-09-30', 11, '2019-10-01', NULL, 'THIS IS SSSS01102019', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 0, '2019-09-02 06:25:12', '2019-09-02 06:25:12'),
(808, 600, 4, '2019-10-01', 11, '2019-10-02', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-09-02 06:25:12', '2019-09-02 06:25:12'),
(810, 601, 4, '2019-09-02', 11, '2019-09-03', NULL, 'HHJHHHHHHHHHHHHH', 0, NULL, 13, 13, '1', '11', '111', 1, 0, 1, 0, '2019-09-02 06:56:55', '2019-09-02 06:56:55'),
(811, 601, 4, '2019-09-03', 11, '2019-09-13', NULL, NULL, 0, NULL, 13, NULL, '1', '11', '111', 0, 0, 0, 0, '2019-09-02 06:56:55', '2019-09-02 06:56:55'),
(812, 601, 4, '2019-09-13', 11, '2019-09-23', NULL, 'HE IS 23-0-0200199999999', 0, NULL, 13, NULL, '1', '11', '111', 0, 0, 1, 0, '2019-09-02 06:56:55', '2019-09-02 06:56:55'),
(813, 601, 4, NULL, 12, '2019-09-26', NULL, NULL, 0, NULL, 13, 13, '1', '11', '111', 1, 1, 0, 0, '2019-09-02 07:02:25', '2019-09-02 07:02:25'),
(814, 594, 4, '2019-08-07', 11, '2019-09-02', 1, NULL, 0, NULL, 15, NULL, '11', '11', NULL, 1, 1, 0, 0, '2019-09-02 09:20:49', '2019-09-02 09:20:49'),
(815, 602, 4, NULL, 11, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-02 08:04:27', '2019-09-02 08:04:27'),
(816, 602, 4, '1970-01-01', 11, '2019-09-02', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-09-02 08:04:27', '2019-09-02 08:04:27'),
(817, 603, 4, NULL, 12, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-02 08:06:52', '2019-09-02 08:06:52'),
(818, 604, 4, NULL, 12, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-02 08:06:35', '2019-09-02 08:06:35'),
(819, 604, 4, '1970-01-01', 12, '2019-09-02', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-09-02 08:06:35', '2019-09-02 08:06:35'),
(820, 603, 4, '1970-01-01', 12, '2019-09-02', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-09-02 08:06:52', '2019-09-02 08:06:52'),
(821, 605, 4, NULL, 12, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-02 08:08:39', '2019-09-02 08:08:39'),
(822, 605, 4, '1970-01-01', 12, '2019-09-02', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-09-02 08:08:39', '2019-09-02 08:08:39'),
(823, 606, 4, NULL, 12, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-02 08:23:10', '2019-09-02 08:23:10'),
(824, 606, 4, '1970-01-01', 12, '2019-09-02', NULL, '22222222', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 0, '2019-09-02 08:23:10', '2019-09-02 08:23:10'),
(825, 606, 4, '2019-09-02', 12, '2019-09-04', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-09-02 08:23:10', '2019-09-02 08:23:10'),
(826, 607, 4, NULL, 11, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-09-02 08:28:29', '2019-09-02 08:28:29'),
(827, 608, 4, NULL, 12, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-02 08:58:44', '2019-09-02 08:58:44'),
(828, 609, 4, NULL, 11, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-02 08:58:21', '2019-09-02 08:58:21'),
(829, 610, 4, NULL, 13, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-02 08:57:54', '2019-09-02 08:57:54'),
(830, 611, 4, NULL, 13, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-02 08:57:28', '2019-09-02 08:57:28'),
(831, 612, 4, NULL, 13, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-02 08:57:06', '2019-09-02 08:57:06'),
(832, 613, 4, NULL, 13, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-02 08:56:54', '2019-09-02 08:56:54'),
(833, 614, 4, NULL, 13, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-02 17:12:24', '2019-09-02 17:12:24'),
(834, 615, 4, NULL, 13, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-02 08:56:25', '2019-09-02 08:56:25'),
(835, 616, 4, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-02 08:56:00', '2019-09-02 08:56:00'),
(836, 617, 4, NULL, 13, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-02 08:55:45', '2019-09-02 08:55:45'),
(837, 618, 4, NULL, 13, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-02 08:55:31', '2019-09-02 08:55:31'),
(838, 619, 4, NULL, 13, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-02 08:55:12', '2019-09-02 08:55:12'),
(839, 619, 4, '1970-01-01', 13, '2019-09-02', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-09-02 08:55:12', '2019-09-02 08:55:12'),
(840, 618, 4, '1970-01-01', 13, '2019-09-02', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-09-02 08:55:31', '2019-09-02 08:55:31'),
(841, 617, 4, '1970-01-01', 13, '2019-09-02', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-09-02 08:55:45', '2019-09-02 08:55:45'),
(842, 616, 4, '1970-01-01', 7, '2019-09-02', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-09-02 08:56:00', '2019-09-02 08:56:00'),
(843, 615, 4, '1970-01-01', 13, '2019-09-02', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-09-02 08:56:25', '2019-09-02 08:56:25'),
(844, 614, 4, '1970-01-01', 13, '2019-09-02', NULL, NULL, 0, NULL, 9, NULL, '10', '10', '10', 1, 0, 0, 0, '2019-09-03 06:30:53', '2019-09-03 06:30:53'),
(845, 613, 4, '1970-01-01', 13, '2019-09-02', NULL, NULL, 0, NULL, 10, NULL, '8', '75', NULL, 1, 1, 0, 0, '2019-09-02 09:13:45', '2019-09-02 09:13:45'),
(846, 612, 4, '1970-01-01', 12, '2019-09-02', NULL, NULL, 0, NULL, 9, NULL, '6', '231', NULL, 1, 1, 0, 0, '2019-09-02 09:13:10', '2019-09-02 09:13:10'),
(847, 611, 4, '1970-01-01', 13, '2019-09-02', NULL, NULL, 0, NULL, 15, NULL, '5', '101', NULL, 1, 1, 0, 0, '2019-09-02 09:12:34', '2019-09-02 09:12:34'),
(848, 610, 4, '1970-01-01', 13, '2019-09-02', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-09-02 08:57:54', '2019-09-02 08:57:54'),
(849, 609, 4, '1970-01-01', 11, '2019-09-02', NULL, NULL, 0, NULL, 13, 14, '2', '39', NULL, 1, 1, 0, 0, '2019-09-02 09:11:52', '2019-09-02 09:11:52'),
(850, 608, 4, '1970-01-01', 12, '2019-09-02', NULL, NULL, 0, NULL, 13, 14, '2', '5', NULL, 1, 1, 0, 0, '2019-09-02 09:10:55', '2019-09-02 09:10:55'),
(851, 620, 4, NULL, 11, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-02 09:14:17', '2019-09-02 09:14:17'),
(852, 621, 4, NULL, 11, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-02 09:02:16', '2019-09-02 09:02:16'),
(853, 621, 4, '1970-01-01', 11, '2019-09-02', NULL, NULL, 0, NULL, 10, NULL, '8', '123', NULL, 1, 1, 0, 0, '2019-09-02 09:16:49', '2019-09-02 09:16:49'),
(854, 620, 4, '1970-01-01', 11, '2019-09-02', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-09-02 09:14:17', '2019-09-02 09:14:17'),
(855, 622, 4, NULL, 7, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-02 11:11:47', '2019-09-02 11:11:47'),
(856, 623, 4, NULL, 13, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-02 11:11:24', '2019-09-02 11:11:24'),
(857, 624, 4, NULL, 11, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-02 11:10:56', '2019-09-02 11:10:56'),
(858, 625, 4, NULL, 12, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-02 11:09:05', '2019-09-02 11:09:05'),
(859, 626, 4, NULL, 11, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-02 13:43:44', '2019-09-02 13:43:44'),
(860, 627, 4, NULL, 8, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-02 11:10:16', '2019-09-02 11:10:16'),
(861, 628, 4, NULL, 13, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-02 11:08:24', '2019-09-02 11:08:24'),
(862, 629, 4, NULL, 7, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-02 11:08:07', '2019-09-02 11:08:07'),
(863, 630, 4, NULL, 7, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-02 11:07:50', '2019-09-02 11:07:50'),
(864, 631, 4, NULL, 13, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-02 11:07:33', '2019-09-02 11:07:33'),
(865, 632, 4, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-02 11:07:12', '2019-09-02 11:07:12'),
(866, 633, 4, NULL, 7, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-09-02 11:00:39', '2019-09-02 11:00:39'),
(867, 634, 4, NULL, 7, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-09-02 11:01:28', '2019-09-02 11:01:28'),
(868, 635, 4, NULL, 7, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-09-02 11:01:57', '2019-09-02 11:01:57'),
(869, 636, 4, NULL, 7, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-09-02 11:02:38', '2019-09-02 11:02:38'),
(870, 637, 4, NULL, 11, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-09-02 11:03:16', '2019-09-02 11:03:16'),
(871, 638, 4, NULL, 7, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-09-02 11:03:53', '2019-09-02 11:03:53'),
(872, 632, 4, '1970-01-01', 7, '2020-02-01', NULL, NULL, 0, NULL, 9, NULL, '4', '34', '82', 1, 1, 0, 0, '2019-09-02 12:54:49', '2019-09-02 12:54:49'),
(873, 631, 4, '1970-01-01', 13, '2020-02-01', NULL, NULL, 0, NULL, 13, 14, '2', '5', NULL, 1, 1, 0, 0, '2019-09-02 12:09:44', '2019-09-02 12:09:44'),
(874, 630, 4, '1970-01-01', 7, '2020-02-01', NULL, NULL, 0, NULL, 13, 14, '2(A)', '5', NULL, 1, 1, 0, 0, '2019-09-02 12:28:37', '2019-09-02 12:28:37'),
(875, 629, 4, '1970-01-01', 7, '2020-02-01', NULL, NULL, 0, NULL, 14, NULL, '5', '15', '174', 1, 1, 0, 0, '2019-09-02 12:30:46', '2019-09-02 12:30:46'),
(876, 628, 4, '1970-01-01', 13, '2020-02-01', NULL, NULL, 0, NULL, 13, 14, '2', '2', NULL, 1, 1, 0, 0, '2019-09-02 12:31:42', '2019-09-02 12:31:42'),
(878, 625, 4, '1970-01-01', 12, '2020-02-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-09-02 11:09:05', '2019-09-02 11:09:05'),
(879, 627, 4, '1970-01-01', 8, '2020-02-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-09-02 11:10:16', '2019-09-02 11:10:16'),
(880, 624, 4, '1970-01-01', 11, '2020-02-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-09-02 11:10:56', '2019-09-02 11:10:56'),
(881, 623, 4, '1970-01-01', 13, '2020-02-01', NULL, NULL, 0, NULL, 9, NULL, '3', '12', NULL, 1, 1, 0, 0, '2019-09-02 12:37:38', '2019-09-02 12:37:38'),
(882, 622, 4, '1970-01-01', 11, '2020-02-01', NULL, NULL, 0, NULL, 13, 14, '2', '1', NULL, 1, 1, 0, 0, '2019-09-02 12:10:38', '2019-09-02 12:10:38'),
(883, 626, 4, '1970-01-01', 11, '2019-09-05', 1, 'Decided on 05.09.2019', 1, NULL, NULL, NULL, '1', '11', '111', 1, 1, 1, 0, '2019-09-02 13:48:53', '2019-09-02 13:48:53'),
(884, 614, 4, '2019-09-02', 13, '2019-09-03', NULL, NULL, 0, NULL, 9, NULL, '1', '10', '10', 1, 1, 0, 0, '2019-09-05 16:14:16', '2019-09-05 16:14:16'),
(885, 639, 4, NULL, NULL, '1970-01-01', NULL, 'hello', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-02 17:21:27', '2019-09-02 17:21:27'),
(886, 639, 4, '1970-01-01', 7, '2019-09-02', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-09-02 17:21:27', '2019-09-02 17:21:27'),
(887, 640, 4, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-04 15:24:48', '2019-09-04 15:24:48'),
(888, 640, 4, '1970-01-01', 7, '2019-09-02', NULL, NULL, 0, NULL, 9, 10, '1', '11', '111', 1, 0, 0, 0, '2019-09-04 15:24:48', '2019-09-04 15:24:48'),
(889, 640, 4, '2019-09-02', 7, '2019-09-03', NULL, NULL, 0, NULL, 9, NULL, '1', '11', '111', 0, 0, 0, 0, '2019-09-04 15:24:48', '2019-09-04 15:24:48'),
(890, 640, 4, '2019-09-02', 11, '2019-09-03', NULL, NULL, 0, NULL, 13, 13, '2', '22', '222', 1, 1, 0, 0, '2019-09-04 15:25:18', '2019-09-04 15:25:18'),
(891, 641, 4, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-09 17:16:49', '2019-09-09 17:16:49'),
(892, 641, 4, '2019-09-06', 7, '2019-09-06', NULL, 'hiiii   hello', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-09 17:17:35', '2019-09-09 17:17:35'),
(893, 641, 4, '2019-09-06', 7, '2019-09-07', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-09-09 17:16:49', '2019-09-09 17:16:49'),
(894, 642, 4, NULL, 7, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-10 07:16:05', '2019-09-10 07:16:05'),
(895, 642, 4, '2019-09-10', 7, '2019-09-10', NULL, 'Jai Shree Krishna', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 1, 0, '2019-09-10 07:32:37', '2019-09-10 07:32:37'),
(896, 643, 4, NULL, 7, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-10 07:43:21', '2019-09-10 07:43:21'),
(897, 643, 4, '1970-01-01', 7, '2019-09-10', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-09-10 07:43:21', '2019-09-10 07:43:21'),
(898, 644, 4, NULL, NULL, '1970-01-01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-19 07:37:15', '2019-09-19 07:37:15'),
(899, 644, 4, '1970-01-01', 7, '2019-09-02', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-19 07:37:15', '2019-09-19 07:37:15'),
(900, 644, 4, '2019-09-02', 7, '2019-09-03', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-09-19 07:37:15', '2019-09-19 07:37:15'),
(901, 644, 4, '2019-09-03', 7, '2019-09-04', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 0, 0, '2019-09-19 07:46:47', '2019-09-19 07:46:47');

-- --------------------------------------------------------

--
-- Table structure for table `adv_referred`
--

CREATE TABLE `adv_referred` (
  `ref_id` int(11) NOT NULL,
  `user_admin_id` int(11) DEFAULT NULL,
  `ref_advocate_name` varchar(50) DEFAULT NULL,
  `ref_advocate_email` varchar(100) DEFAULT NULL,
  `ref_address` varchar(255) DEFAULT NULL,
  `ref_mobile_number` varchar(50) DEFAULT NULL,
  `ref_place` varchar(255) DEFAULT NULL,
  `ref_status` tinyint(2) NOT NULL COMMENT '0=Inactive,1=Active	'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adv_referred`
--

INSERT INTO `adv_referred` (`ref_id`, `user_admin_id`, `ref_advocate_name`, `ref_advocate_email`, `ref_address`, `ref_mobile_number`, `ref_place`, `ref_status`) VALUES
(1, 6, 'Vishal Thakur', NULL, 'B.J.S Jodhpur', '7896541230', 'B.J.S', 1),
(2, 6, 'Jai Shingh', NULL, 'paota jodhpur', '6985471230', 'Paota', 0),
(3, 4, 'Ramesh', NULL, '4th road', '6878787878', 'UP', 1),
(5, 9, 'Test One', NULL, 'Test', '9874563145', 'Test', 1),
(6, 9, 'Test One', NULL, 'Test', '9874563145', 'Test', 1),
(7, 9, 'Test Twoo', NULL, 'Test 123', '1236547890', 'Test 123', 1),
(8, 4, 'Pradeep Rai', NULL, 'Test', '9649508382', 'Test', 1),
(9, 4, 'Aditya Soni', NULL, 'Test', '7597037331', 'Ajmer', 1),
(10, 4, 'Pankaj', NULL, 'Test', '9874563145', 'v2r', 1),
(12, 8, 'Test One', NULL, 'Test', '9876543210', 'Test', 1),
(13, 8, 'Test Two', NULL, 'Test', '9874563145', 'Test', 1),
(14, 4, 'Rakesh Chotia', NULL, 'Chotia Street\r\nBikaner', '9461539305', 'Bikaner', 1),
(15, 4, 'Ajeet Singh', NULL, 'RTO Office\r\nNagaur', '1232312333', 'Nagaur', 1),
(16, 14, 'Ad Soni', NULL, 'Test', '7597037331', 'Ajmer', 1),
(18, 16, 'Pramod', NULL, 'Ajmer', '7597037331', 'Ajmer', 1),
(19, 16, 'Sachin', NULL, 'Ajmer', '7597037331', 'Ajmer', 1),
(20, 16, 'Suraj', NULL, 'Ajmer', '7597037331', 'Ajmer', 1),
(21, 15, 'K.C. Kasth', NULL, '1-K-21, RC Vyas Colony\r\nBhilwara (Raj)', '9414111263', 'BHILWARA', 1),
(22, 15, 'Shridhar Mehta', NULL, '28, Pokran House, JDA Circle \r\nJodhpur', '9549931174', 'Jodhpur', 1),
(23, 15, 'Rahul Rajpurohit', NULL, '\"Laxmi Niwas\" Near Rawat Hotel, Udai Mandir\r\nJodhpur', '9529747370', 'Jodhpur', 1),
(24, 15, 'Chayan Bothra', NULL, '3, Padmawati Nagar, Inside Jalam Vilash\r\nPaota B Road\r\nBikaner', '7791058796', 'Bikaner', 1),
(25, 18, 'AMBA LAL KUMAWAT', NULL, 'A-409 AAJAD NAGAR', '9414975821', 'BHILWARA', 1),
(26, 18, 'LALU RAM JI KUMAWAT', NULL, NULL, '9414730315', 'CHHITORGARH', 1),
(27, 18, 'VIKRAM SINGH', NULL, NULL, NULL, 'JALORE', 1),
(28, 19, 'T', NULL, NULL, '0', 'JOD', 1),
(29, 19, 's', NULL, NULL, '0', 'AII', 1),
(30, 22, 'K.C. KASTH', NULL, '1-K-21 R.C. VYAS COLONY         \r\n\r\n\r\nIN FRONT OF R.K. CONSUMER HALL', '9414111263', 'BHILWARA', 1),
(31, 22, 'Karan Singh Charan', NULL, 'H.No- 107, Karni Kot\r\n\r\n\r\nNear Mahaveer Udhyan Gandhipura\r\n\r\n\r\nBalotara', '7742887742', 'BALOTARA', 1),
(32, 22, 'O.P SHARMA', NULL, 'LEGAL CELL                \r\n\r\n\r\nMUNICIPAL COUNCIL', '9829095891', 'BHILWARA', 1),
(33, 29, 'Sumit', NULL, 'jodhpur', '9782833224', 'jodhpur', 1),
(34, 4, 'Sweta', 'sweta.v2rsolution@gmail.com', '1st\r\n1225241', '7976625157', 'Jodhpur', 1),
(35, 30, 'Sumit', 'sumit.v2rsolution@gmail.com', 'Paota Jodhpur', '9782833224', 'jodhpur', 1),
(36, 30, 'Jeet', 'jone.boaz@gmail.com', 'Paota Jodhpur', '9782608731', 'jodhpur', 1),
(37, 31, 'KC Kasth Advocate', 'kasth@gmail.com', '111-K, RC Vyas Colony', '7877325464', 'Bhilwara', 1),
(38, 31, 'Karan Singh Charan Advocate', 'karan@gmail.com', '111-K, Near ADJ Court', '7877325464', 'Balotara', 1),
(39, 31, 'Jayant Ojha Advocate', 'jayant@gmail.com', 'Near Love Garden, \r\nShahpura', '7877325464', 'Bhilwara', 1),
(40, 33, 'Ajit Singh Chouhan', NULL, NULL, NULL, 'Banswara', 1),
(41, 33, 'Murlidhar Soni Advocate', NULL, 'Ratangarh,', '7014838401', 'Churu', 1),
(42, 33, 'JK Dave', NULL, '22, Pathik Magari', '9414234140', 'Udaipur', 1),
(43, 33, 'Gautam JI', NULL, 'Badi Sadari,', NULL, 'Chittorgarh', 1),
(44, 33, 'Chetan Prakash Bhanawat Advocate', NULL, 'Kanod,', '9414684229', 'Udaipur', 1),
(45, 39, 'OP Sharma', 'yogeshmdpurohit@gmail.com', 'Municipal Council', '7877325464', 'Bhilwara', 1),
(46, 39, 'KC Kasth Advocate', 'yogeshmdpurohit@gmail.com', '111-K RC Vyas Colony,', '7877325464', 'Bhilwara', 1),
(47, 39, 'Jayant Ojha Advocate', 'contactyogi41@gmail.com', 'Shahpura,', '9950718980', 'Bhilwara', 1),
(48, 39, 'Manak Bhati Advocate', 'contactyogi41@gmail.com', 'Sardar Sahar,', '9950718980', 'Churu', 1),
(49, 39, 'JK Dave Advocate', 'yogeshmdpurohit@gmail.com', 'Sector-11, Hiran Magari,', '7877325464', 'Udaipur', 1),
(50, 39, 'Amit Chabra Advocate', 'contactyogi41@gmail.com', 'Advocate Colony,', '7877325464', 'Sriganganagar', 1),
(51, 40, 'ADV GANESH CHOUDHARY', 'rakeshchotia@gmail.com', NULL, '7737388902', 'JODHPUR', 1),
(52, 33, 'Ghanshaym Soni  Advocate', NULL, 'Radha Rani Kunj, Opposite Middle School,\r\nRatangarh, Churu', '9414084183', 'Churu', 1),
(53, 33, 'Sahid Khan Advcoate', NULL, 'Near Masjid, Indra Colony, Banswara', '9887421275', 'Banswara', 1),
(54, 33, 'Gopal Acharya Advocate', NULL, 'Teliwada,', '9352858032', 'Bikaner', 1),
(55, 33, 'Mahesh Jain Advcoate', NULL, 'Chamber No:-15, Court Parisar,', '8003579154', 'Doongarpur', 1),
(56, 33, 'Abhinav Chandaliya Advoate', NULL, '18, Khodo Ka Bas,', '7597794418', 'Pali', 1),
(57, 33, 'Hanuman Prasad Sharma Advocate', NULL, '18-E Block, Near Navdeep Academy School,\r\nUdaipur', '9414162345', 'Udaipur', 1),
(58, 33, 'Prajit Tiwari Advocate', NULL, NULL, '9414681878', 'Rajsamand', 1),
(59, 4, 'MANOJ JI', 'DDDDDDDD', NULL, '77373889222', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `adv_section`
--

CREATE TABLE `adv_section` (
  `section_id` int(11) NOT NULL,
  `section_act_id` int(11) NOT NULL,
  `user_admin_id` int(11) DEFAULT NULL,
  `section_name` varchar(50) DEFAULT NULL,
  `section_short_name` varchar(50) DEFAULT NULL,
  `section_code` varchar(50) DEFAULT NULL,
  `section_description` varchar(255) DEFAULT NULL,
  `section_status` tinyint(2) NOT NULL COMMENT '0=Inactive,1=Active	'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adv_section`
--

INSERT INTO `adv_section` (`section_id`, `section_act_id`, `user_admin_id`, `section_name`, `section_short_name`, `section_code`, `section_description`, `section_status`) VALUES
(1, 2, 6, 'Section one', 'SECONE', 'SEOne', 'sada adsa', 1),
(2, 2, 6, 'Section Two', 'SETwo', 'SETwo', 'dsa d', 0),
(3, 1, 6, 'asd', 'asd', 'asd', 'asd', 1),
(4, 4, 4, '456', '544', '67', 'trw', 0),
(5, 7, 9, '1One', 'TT', '#@!#@!', 'Test Three', 1),
(6, 7, 9, '1One', 'TT', '#@!#@!', 'Test Three', 1),
(7, 6, 9, '2Two', 'TE', '#@%^', 'Test Two', 1),
(8, 5, 9, '1 one', 'TE', '$&#5', 'Test', 1),
(9, 9, 4, 'Two', 'Two', 'Two', 'Two', 1),
(10, 8, 4, 'One', 'One', 'One', 'One', 1),
(11, 11, 8, 'one', 'One', 'One', 'Test', 1),
(12, 12, 8, 'Two', 'TT', 'Two', 'Test', 1),
(13, 13, 4, '13HMA', NULL, NULL, NULL, 1),
(14, 19, 14, 'Test', NULL, NULL, NULL, 1),
(15, 22, 4, '123', NULL, NULL, NULL, 1),
(16, 18, 14, 'Test', NULL, NULL, NULL, 1),
(17, 23, 16, 'Section 1', NULL, NULL, NULL, 1),
(18, 24, 16, 'Section 2', NULL, NULL, NULL, 1),
(19, 25, 15, '10', NULL, NULL, NULL, 1),
(20, 25, 15, '15', NULL, NULL, NULL, 1),
(21, 26, 15, '18', NULL, NULL, NULL, 1),
(22, 27, 15, '12(K)', NULL, NULL, NULL, 1),
(23, 27, 15, '13(K),16', NULL, NULL, NULL, 1),
(24, 28, 15, '138(B),ARB', NULL, NULL, NULL, 1),
(25, 29, 15, '23(C)135H', NULL, NULL, NULL, 1),
(26, 30, 15, '11A &138/A-5', NULL, NULL, NULL, 1),
(27, 32, 15, '15', NULL, NULL, NULL, 1),
(28, 33, 4, '1', NULL, NULL, NULL, 1),
(29, 33, 4, '2', NULL, NULL, NULL, 1),
(30, 33, 4, '4', NULL, NULL, NULL, 1),
(31, 34, 18, '34', NULL, NULL, NULL, 1),
(32, 34, 18, '48', NULL, NULL, NULL, 1),
(33, 35, 19, '120', NULL, NULL, NULL, 1),
(34, 36, 19, '120,121,122', NULL, NULL, NULL, 1),
(35, 37, 22, '13', NULL, NULL, NULL, 1),
(36, 38, 22, '11', NULL, NULL, NULL, 1),
(37, 39, 22, '138', NULL, NULL, NULL, 1),
(38, 40, 4, '1', NULL, NULL, NULL, 1),
(39, 40, 4, '2', NULL, NULL, NULL, 1),
(40, 41, 4, 'SMTP', NULL, NULL, NULL, 1),
(41, 42, 4, '11', NULL, NULL, NULL, 1),
(42, 42, 4, '21', NULL, NULL, NULL, 1),
(43, 43, 29, 'ACT 45-01', NULL, NULL, NULL, 1),
(44, 43, 29, 'ACT 45-02', NULL, NULL, NULL, 1),
(45, 44, 30, 'ACT 45-01', NULL, NULL, NULL, 1),
(46, 44, 30, 'ACT 45-02', NULL, NULL, NULL, 1),
(47, 44, 30, 'ACT 45-03', NULL, NULL, NULL, 1),
(48, 45, 30, 'ACT 46-01', NULL, NULL, NULL, 1),
(49, 45, 30, 'ACT 46-02', NULL, NULL, NULL, 1),
(50, 46, 31, '11', NULL, NULL, NULL, 1),
(51, 46, 31, '21', NULL, NULL, NULL, 1),
(52, 46, 31, '31', NULL, NULL, NULL, 1),
(53, 47, 31, '11', NULL, NULL, NULL, 1),
(54, 47, 31, '51', NULL, NULL, NULL, 1),
(55, 47, 31, '101', NULL, NULL, NULL, 1),
(56, 48, 31, '1', NULL, NULL, NULL, 1),
(57, 48, 31, '11(S), 5(A)', NULL, NULL, NULL, 1),
(58, 48, 31, '21-A(B),C', NULL, NULL, NULL, 1),
(59, 58, 4, 'Under Section_ A(B)', NULL, NULL, NULL, 1),
(60, 58, 4, 'Under Section AC(T)', NULL, NULL, NULL, 1),
(61, 59, 4, '138', NULL, NULL, NULL, 1),
(62, 59, 4, '144', NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `adv_sms_content`
--

CREATE TABLE `adv_sms_content` (
  `sms_content_id` int(11) NOT NULL,
  `user_admin_id` int(11) DEFAULT NULL,
  `sms_content_type` int(11) DEFAULT NULL,
  `sms_content_title` varchar(255) DEFAULT NULL,
  `sms_content_before` text,
  `sms_content_after` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adv_sms_content`
--

INSERT INTO `adv_sms_content` (`sms_content_id`, `user_admin_id`, `sms_content_type`, `sms_content_title`, `sms_content_before`, `sms_content_after`) VALUES
(1, NULL, NULL, 'Sign Up', 'testing', 'testing'),
(2, NULL, NULL, 'Case Registration', '1', '2'),
(3, NULL, NULL, 'Peshi / Cause List Entry', 'YOUR PESHI', 'NEXT DATE IS 555555'),
(4, NULL, NULL, 'Daily Diary', 'testing', 'testing'),
(5, NULL, NULL, 'Sms to Client', 'DEEWALI', 'KI HARDIK SUBHKAMNAE...'),
(6, NULL, NULL, 'Undated Case', NULL, NULL),
(7, NULL, NULL, 'Due Course', NULL, NULL),
(8, 4, 1, 'Sign Up', NULL, NULL),
(9, 4, 2, 'Case Registration', NULL, NULL),
(10, 4, 3, 'Peshi / Cause List Entry', NULL, NULL),
(11, 4, 4, 'Daily Diary', NULL, NULL),
(12, 4, 5, 'Sms to Client', NULL, NULL),
(13, 29, 1, 'Sign Up', NULL, NULL),
(14, 29, 2, 'Case Registration', 'Welcome For Case', 'Thank'),
(15, 29, 3, 'Peshi / Cause List Entry', 'Welcome For Peshi', 'Thank'),
(16, 29, 4, 'Daily Diary', 'Welcome For Daily Diary', 'Thank'),
(17, 29, 5, 'Sms to Client', 'Welcome For Client', 'Thank'),
(18, 30, 1, 'Sign Up', NULL, NULL),
(19, 30, 2, 'Case Registration', 'Welcome', 'Thanx'),
(20, 30, 3, 'Peshi / Cause List Entry', 'Welcome', 'Thanx'),
(21, 30, 4, 'Daily Diary', 'Welcome', 'Thanx'),
(22, 30, 5, 'Sms to Client', 'Welcome', 'Thanx');

-- --------------------------------------------------------

--
-- Table structure for table `adv_stage`
--

CREATE TABLE `adv_stage` (
  `stage_id` int(11) NOT NULL,
  `user_admin_id` int(11) DEFAULT NULL,
  `stage_name` varchar(50) DEFAULT NULL,
  `stage_short_name` varchar(50) DEFAULT NULL,
  `stage_status` tinyint(2) NOT NULL COMMENT '0=Inactive,1=Active	'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adv_stage`
--

INSERT INTO `adv_stage` (`stage_id`, `user_admin_id`, `stage_name`, `stage_short_name`, `stage_status`) VALUES
(1, 6, 'Stage One', 'SONE', 0),
(2, 6, 'Stage Two', 'Stwo', 1),
(3, 4, 'In progress', 'IP', 0),
(4, 9, 'Test Two', 'TE', 1),
(5, 9, 'Test One', 'TE', 1),
(6, 9, 'Test', 'TE,Test', 0),
(7, 4, 'Test one', 'TO', 1),
(8, 4, 'Test Two', 'TT', 1),
(9, 8, 'Test One', 'TO', 1),
(10, 8, 'Test Two', 'TT', 1),
(11, 4, 'Admission', NULL, 1),
(12, 4, 'Hearing', NULL, 1),
(13, 4, 'Order', NULL, 1),
(14, 4, 'Chalan Paper Filed', NULL, 1),
(15, 14, 'Test', NULL, 1),
(16, 14, 'TestOne', NULL, 0),
(17, 15, 'Final Disposal', NULL, 1),
(18, 16, 'Stage 1', NULL, 1),
(19, 16, 'Stage 2', NULL, 1),
(20, 15, 'Admission', NULL, 1),
(21, 15, 'Order', NULL, 1),
(22, 15, 'Hearing', NULL, 1),
(23, 18, 'A', NULL, 1),
(24, 18, 'O', NULL, 1),
(25, 18, 'H', NULL, 1),
(26, 18, 'FD', NULL, 1),
(27, 18, 'LOK', NULL, 1),
(28, 18, 'ARB', NULL, 1),
(29, 19, '1', NULL, 1),
(30, 19, '2', NULL, 1),
(31, 22, 'A', NULL, 1),
(32, 22, 'FD', NULL, 1),
(33, 22, 'O', NULL, 1),
(34, 22, 'H', NULL, 1),
(35, 22, 'Reply Not Filed', NULL, 1),
(36, 29, 'Stage one', NULL, 1),
(37, 29, 'Stage one', NULL, 0),
(38, 30, 'Stage one', NULL, 1),
(39, 30, 'Stage Two', NULL, 1),
(40, 30, 'Stage Three', NULL, 1),
(41, 30, 'Stage Four', NULL, 1),
(42, 31, 'ADMISSION', NULL, 1),
(43, 31, 'ORDER', NULL, 1),
(44, 31, 'HEARING', NULL, 1),
(45, 33, 'A', NULL, 1),
(46, 33, 'O', NULL, 1),
(47, 33, 'H', NULL, 1),
(48, 33, 'Sup.', NULL, 1),
(49, 39, 'A', NULL, 1),
(50, 39, 'H', NULL, 1),
(51, 39, 'O', NULL, 1),
(52, 39, 'FD', NULL, 1),
(53, 39, 'LA', NULL, 1),
(54, 39, 'Sup', NULL, 1),
(55, 40, 'A', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `adv_sub_client`
--

CREATE TABLE `adv_sub_client` (
  `sub_client_id` int(11) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `user_admin_id` int(11) DEFAULT NULL,
  `sub_client_name` varchar(255) DEFAULT NULL,
  `sub_name_prefix` varchar(50) DEFAULT NULL,
  `sub_guardian_name` varchar(100) DEFAULT NULL,
  `sub_client_email_id` varchar(255) DEFAULT NULL,
  `sub_client_mobile_no` varchar(50) DEFAULT NULL,
  `sub_client_place` varchar(50) DEFAULT NULL,
  `sub_client_address` text,
  `sub_client_status` tinyint(4) NOT NULL COMMENT '0=Inactive,1=Active	'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adv_sub_client`
--

INSERT INTO `adv_sub_client` (`sub_client_id`, `client_id`, `user_admin_id`, `sub_client_name`, `sub_name_prefix`, `sub_guardian_name`, `sub_client_email_id`, `sub_client_mobile_no`, `sub_client_place`, `sub_client_address`, `sub_client_status`) VALUES
(3, 2, 6, 'tat', NULL, NULL, 'tat@gmail.com', '1236547890', NULL, 'asas\r\ntest\r\ntest', 1),
(7, 21, 9, 'AD Test Two', NULL, NULL, 'test2@gmail.com', '8765326565', NULL, 'Test', 1),
(41, 10, 9, 'test1', NULL, NULL, 'test@gmail.com', '64', NULL, 'Test', 1),
(42, 20, 4, 'Pradeep', NULL, NULL, 'Pradeep@gmail.com', '8765326565', NULL, 'Pradeep', 1),
(43, 17, 8, 'Test', NULL, NULL, 'test@gmail.com', '1234567890', NULL, 'Test', 1),
(44, 17, 8, 'test1', NULL, NULL, 'test1@gmail.com', '1234567980', NULL, 'Test', 1),
(46, 18, 4, 'fddg', NULL, NULL, 'dgdgs', '8797878777', NULL, 'bhghgh', 1),
(47, 21, 4, 'Branch Manager', NULL, NULL, NULL, NULL, 'Jodhpur', 'Jalori Gate Branch,\r\nJodhpur', 1),
(48, 23, 4, 'CBI', NULL, NULL, NULL, NULL, 'Jodhpur', NULL, 1),
(55, 34, 16, 'Test', NULL, NULL, 'a@gmail.com', '7597037331', 'Ajmer', 'Ajmer', 1),
(56, 34, 16, 'Test1', NULL, NULL, 'a@gmail.com', '7597037331', 'Ajmer', 'Ajmer', 1),
(63, 42, 15, 'Special Crime Branch-III', NULL, NULL, 'sc1@gmail.com', NULL, 'New Delhi', 'Special Crime Branch-II,\r\nCGO Complex, Lodhi Road\r\nNew Delhi', 1),
(64, 42, 15, 'Special Crime Branch-II New Delhi', NULL, NULL, 'sc2@gmail.com', NULL, 'New Delhi', 'Special Crime Branch-II,\r\nCGO Complex, Lodhi Road\r\nNew Delhi', 1),
(65, 42, 15, 'Bank Security & Fraud', NULL, NULL, 'security@gmail.com', NULL, 'Jaipur', '10- Mahaveer Complex\r\nNear Railway Station\r\nJaipur (Raj)', 1),
(69, 41, 15, 'Jalori Gate- SBI Branch', NULL, NULL, 'jalori@gmail.com', NULL, 'Jodhpur', 'Mr Rajnish Prabhakar\r\nBranch Manager- SBI\r\nJalori Gate, Jodhpur', 1),
(70, 41, 15, 'Shastri Nagar- Jodhpur', NULL, NULL, 'shastrinagar@gmail.com', NULL, 'Jodhpur', 'Assistant General Manager- SBI\r\nNear Kalpatru Cinema, Shastri Nagar\r\nJodhpur (Raj)', 1),
(71, 41, 15, 'Zonal Office- Jodhpur', NULL, NULL, 'Zonal@gmail.com', NULL, 'Jodhpur', 'A-21, Shastri Nagar,\r\nDalley Khan Chakki Road,\r\nJodhpur', 1),
(72, 43, 18, 'TH. SHREE VINEET BOSE S/O SHRI S.C. BOSE A.G.M.', NULL, NULL, 'BAARERMATTSTRASSE 3, PO BOX 777, CH-6341 BAAR, SWITZERLAND', NULL, 'UDAIPUR', NULL, 1),
(73, 46, 19, 'B', NULL, NULL, NULL, NULL, NULL, NULL, 1),
(74, 54, 22, 'Special Crime Branch SIT/IT', NULL, NULL, 'scb@gmail.com', '995071890', 'Mumbai', 'Supdt of Polce\r\n\r\n\r\nSpecial Crime Branch', 1),
(75, 54, 22, 'Special Crime Branch-II', NULL, NULL, 'sc2@gmail.com', '7877325464', 'New Delhi', 'Supdt of Police\r\n\r\n\r\nSpecial Crime Branch Branch-II\r\n\r\nCGO Complex. Lodhi Raod', 1),
(76, 54, 22, 'Bank Security & Fraud Cell', NULL, NULL, 'bank@gmail.com', '9950718980', 'Jaipur', 'Supdt of Police-CBI\r\n\r\n\r\nJaipur', 1),
(77, 54, 22, 'Special Crime Branch-I', NULL, NULL, 'sc1@gmail.com', '9950718980', 'New Delhi', 'CGO Complex, Lodhi Road\r\n\r\n\r\nNew Delhi', 1),
(78, 55, 22, 'Jawahar Nagar Branch', NULL, NULL, 'jawahar@gmail.com', '8505000963', 'Jaipur', 'General Manager- SBI\r\n\r\nJawahar Nagar', 1),
(79, 55, 22, 'Shastri Nagar Branch', NULL, NULL, 'shastri@gmail.com', '7877325464', 'Jodhpur', 'General Manager- SBI\r\n\r\n\r\nNear Kalpatru Cinema\r\n\r\n\r\nBehind Stadium', 1),
(80, 55, 22, 'Zonal Office', NULL, NULL, 'zonaloffice@gmail.com', '77689005026', 'Bikaner', 'Chief Manager- SBI\r\n\r\nZonal Office, Near Public Park', 1),
(81, 58, 22, '3', NULL, NULL, NULL, NULL, NULL, NULL, 1),
(82, 58, 22, '2', NULL, NULL, NULL, NULL, NULL, NULL, 1),
(83, 21, 4, '3', 'S/O', NULL, NULL, '77887766767', NULL, NULL, 1),
(84, 21, 4, '2', 'S/O', NULL, NULL, NULL, NULL, NULL, 1),
(85, 21, 4, '1', 'S/O', NULL, NULL, '77887766767', NULL, NULL, 1),
(86, 21, 4, '4', 'S/O', NULL, NULL, NULL, NULL, NULL, 1),
(87, 62, 23, 'Aditya', 'S/O', 'Aditya', 'adi@gmail.com', '9465465206', 'Person', 'Piplaj\r\nasd\r\nasdsad\r\nasd\r\n\r\nsda\r\nda\r\ndas\r\nd\r\nsa\r\nsda\r\nsd\r\nsda\r\nda\r\nd\r\nsad\r\nsa\r\ndsa\r\nd\r\nsad\r\nsa\r\nd\r\nsad\r\nas\r\ndasdasdsadsad\r\n\r\nad\r\nas\r\ndsa\r\nd\r\na\r\nda\r\nd\r\nsa\r\ng\r\n\r\nhdf\r\njgk\r\ngl\r\nuio', 1),
(88, 62, 23, NULL, 'S/O', NULL, NULL, NULL, NULL, 'sdad\r\nsdadassdasda\r\nsda\r\nsda\r\nsa\r\nda\r\nsda\r\nsdasda\r\nsda\r\nsda\r\n\r\nda\r\nd', 1),
(89, 67, 29, 'Vijay phase one Ltd', 'S/O', 'mohan', 'jone.boaz@gmail.com', '9782608731', 'jodhpur', 'jodhpur', 1),
(90, 68, 30, 's test', 'S/O', 's mohan', 'jone.boaz@gmail.com', '9782608731', 'jodhpur', 'jodhpur', 1),
(91, 77, 31, 'Special Crime Branch-I', 'S/O', NULL, NULL, NULL, 'New Delhi', '5th Floor, B Wing, CGO Complex, Lodhi Road', 1),
(92, 77, 31, 'Special Crime Branch-II', 'S/O', NULL, NULL, NULL, 'New Delhi', '5th Floor, A Wing, CGO Complex, Lodhi Road\r\nNew Delhi', 1),
(93, 78, 31, 'Jalori Gate Branch', 'S/O', NULL, NULL, NULL, 'Jodhpur', 'State Bank of India- Jalori Gate Branch\r\nNear Jalori Gate Circle', 1),
(94, 78, 31, 'Raikabagh Branch', 'S/O', NULL, NULL, NULL, 'Jodhpur', 'State Bank of India- Raikabagh Branch\r\nNear Paota Circle, Paota', 1),
(95, 81, 33, 'Deputy General Manager', NULL, '', NULL, NULL, 'Jaipur', 'Administrative & Business Unit\r\nC Scheme, Tilak Marg,\r\nJaipur', 1),
(96, 84, 33, 'Bank Securities & Fraud Cell', 'Thr/O', 'GM Rathi (IO)', NULL, '9650395040', 'New Delhi', '5th Floor, CBI Headquarter,\r\nCGO Complex, Lodhi Raod', 1),
(97, 89, 33, 'Neeraj Kumar', 'C/O', 'ONGC', NULL, '9410396255', 'Dehradun', 'Corporate Administration, ONGC, Tel Bhawan,-248003', 1),
(98, 108, 40, 'SBI', 'Thr/O', 'ASST. MANGER JODHPUR', 'sbisbi@gmail.com', '7737388902', 'JODHPUR', 'JODHPUR ......................................................', 1),
(99, 66, 4, 'Test', 'S/O', 'Test', 'test@gmail.com', '9765462602', 'Jodhpur', 'Testing', 1),
(100, 81, 33, 'Prem Pal', 'S/O', 'Gopi Chand', NULL, NULL, 'Jodhpur', 'Assitant General Manager, Jalori Gate Branch,', 1),
(101, 84, 33, 'CBI- Jodhpur', NULL, '', NULL, NULL, 'Jodhpur', 'Near Gokulji Ki Pyau, Lal Sagar,', 1),
(102, 125, 4, 'HZL- Dariba', 'Thr/O', 'Shubham Yadav', NULL, '9950718980', 'Dariba', '111-5, Near Railway Station,', 1),
(103, 125, 4, 'HZL- Chittorgarh', 'Thr/O', 'Ravi Saxena', NULL, '7877325464', 'Chittorgarh', '204-K, Air Force, Near Sabji Mandi,', 1),
(104, 129, 4, 'YKP', 'Thr/O', 'IDEAL LAWYER', 'YKP', '7877325464', NULL, 'AAAAAAAAA', 1);

-- --------------------------------------------------------

--
-- Table structure for table `otp`
--

CREATE TABLE `otp` (
  `phone_number` varchar(15) DEFAULT NULL,
  `otp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `otp`
--

INSERT INTO `otp` (`phone_number`, `otp`) VALUES
('2147483647', 692075),
('2147483647', 908436),
('2147483647', 289310),
('9782371474', 528630),
('1245784512', 963528),
('12457896325', 89347),
('1245787777', 901764),
(NULL, 83526),
('1236547890', 638570),
('7339869408', 148057),
('7976625157', 849671),
('7597037331', 69135),
('21544646433', 867204),
('7877325464', 693547),
('9950718980', 928730),
('9876543210', 912685),
('9876543110', 137486),
('72992399329', 465182),
('7984653000', 365928),
('7728928902', 43129),
('9782608731', 912785),
('1234', 437209),
('9782218265', 257963),
('9999999999', 741560),
('9502592745', 812056),
('4994949449', 308672),
('8919324366', 97468),
('2345678987', 837012),
('2345678765', 853206),
('09876522222', 219475),
('7737388902', 897534);

-- --------------------------------------------------------

--
-- Table structure for table `reg_comp_other`
--

CREATE TABLE `reg_comp_other` (
  `other_id` int(11) NOT NULL,
  `user_admin_id` int(11) DEFAULT NULL,
  `other_status_date` varchar(255) DEFAULT NULL,
  `other_date_pic` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `other_case_no` varchar(255) DEFAULT NULL,
  `other_title` varchar(255) DEFAULT NULL,
  `other_compliance` varchar(255) DEFAULT NULL,
  `other_status` tinyint(2) NOT NULL COMMENT '0=Inactive,1=Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adv_account_settings`
--
ALTER TABLE `adv_account_settings`
  ADD PRIMARY KEY (`account_id`);

--
-- Indexes for table `adv_act`
--
ALTER TABLE `adv_act`
  ADD PRIMARY KEY (`act_id`);

--
-- Indexes for table `adv_admin`
--
ALTER TABLE `adv_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `adv_assigned`
--
ALTER TABLE `adv_assigned`
  ADD PRIMARY KEY (`assign_id`);

--
-- Indexes for table `adv_case_reg`
--
ALTER TABLE `adv_case_reg`
  ADD PRIMARY KEY (`reg_id`),
  ADD KEY `reg_fir_id` (`reg_fir_id`),
  ADD KEY `reg_stage_id` (`reg_stage_id`),
  ADD KEY `reg_assigend_id` (`reg_assigend_id`),
  ADD KEY `reg_act_id` (`reg_act_id`);

--
-- Indexes for table `adv_case_type`
--
ALTER TABLE `adv_case_type`
  ADD PRIMARY KEY (`case_id`);

--
-- Indexes for table `adv_certified_copy_date`
--
ALTER TABLE `adv_certified_copy_date`
  ADD PRIMARY KEY (`ccd_id`);

--
-- Indexes for table `adv_class_code`
--
ALTER TABLE `adv_class_code`
  ADD PRIMARY KEY (`classcode_id`),
  ADD KEY `subtype_case_id` (`subtype_case_id`),
  ADD KEY `subtype_case_id_2` (`subtype_case_id`);

--
-- Indexes for table `adv_client`
--
ALTER TABLE `adv_client`
  ADD PRIMARY KEY (`cl_id`);

--
-- Indexes for table `adv_compliance`
--
ALTER TABLE `adv_compliance`
  ADD PRIMARY KEY (`compliance_id`);

--
-- Indexes for table `adv_comp_date`
--
ALTER TABLE `adv_comp_date`
  ADD PRIMARY KEY (`cdate_id`);

--
-- Indexes for table `adv_comp_defect_case`
--
ALTER TABLE `adv_comp_defect_case`
  ADD PRIMARY KEY (`defect_id`);

--
-- Indexes for table `adv_comp_notice`
--
ALTER TABLE `adv_comp_notice`
  ADD PRIMARY KEY (`notice_id`);

--
-- Indexes for table `adv_comp_reply`
--
ALTER TABLE `adv_comp_reply`
  ADD PRIMARY KEY (`reply_id`);

--
-- Indexes for table `adv_courselist_in_high_court`
--
ALTER TABLE `adv_courselist_in_high_court`
  ADD PRIMARY KEY (`list_id`);

--
-- Indexes for table `adv_court`
--
ALTER TABLE `adv_court`
  ADD PRIMARY KEY (`court_id`);

--
-- Indexes for table `adv_document_uploads`
--
ALTER TABLE `adv_document_uploads`
  ADD PRIMARY KEY (`upload_id`);

--
-- Indexes for table `adv_fir`
--
ALTER TABLE `adv_fir`
  ADD PRIMARY KEY (`fir_id`);

--
-- Indexes for table `adv_holiday`
--
ALTER TABLE `adv_holiday`
  ADD PRIMARY KEY (`holiday_id`);

--
-- Indexes for table `adv_judge`
--
ALTER TABLE `adv_judge`
  ADD PRIMARY KEY (`judge_id`);

--
-- Indexes for table `adv_master_pages`
--
ALTER TABLE `adv_master_pages`
  ADD PRIMARY KEY (`pages_id`);

--
-- Indexes for table `adv_notes`
--
ALTER TABLE `adv_notes`
  ADD PRIMARY KEY (`notes_id`);

--
-- Indexes for table `adv_pessi`
--
ALTER TABLE `adv_pessi`
  ADD PRIMARY KEY (`pessi_id`);

--
-- Indexes for table `adv_referred`
--
ALTER TABLE `adv_referred`
  ADD PRIMARY KEY (`ref_id`);

--
-- Indexes for table `adv_section`
--
ALTER TABLE `adv_section`
  ADD PRIMARY KEY (`section_id`),
  ADD KEY `subtype_case_id` (`section_act_id`),
  ADD KEY `subtype_case_id_2` (`section_act_id`);

--
-- Indexes for table `adv_sms_content`
--
ALTER TABLE `adv_sms_content`
  ADD PRIMARY KEY (`sms_content_id`);

--
-- Indexes for table `adv_stage`
--
ALTER TABLE `adv_stage`
  ADD PRIMARY KEY (`stage_id`);

--
-- Indexes for table `adv_sub_client`
--
ALTER TABLE `adv_sub_client`
  ADD PRIMARY KEY (`sub_client_id`),
  ADD KEY `client_id` (`client_id`);

--
-- Indexes for table `reg_comp_other`
--
ALTER TABLE `reg_comp_other`
  ADD PRIMARY KEY (`other_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adv_account_settings`
--
ALTER TABLE `adv_account_settings`
  MODIFY `account_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `adv_act`
--
ALTER TABLE `adv_act`
  MODIFY `act_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `adv_admin`
--
ALTER TABLE `adv_admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `adv_assigned`
--
ALTER TABLE `adv_assigned`
  MODIFY `assign_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `adv_case_reg`
--
ALTER TABLE `adv_case_reg`
  MODIFY `reg_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=645;

--
-- AUTO_INCREMENT for table `adv_case_type`
--
ALTER TABLE `adv_case_type`
  MODIFY `case_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=120;

--
-- AUTO_INCREMENT for table `adv_certified_copy_date`
--
ALTER TABLE `adv_certified_copy_date`
  MODIFY `ccd_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `adv_class_code`
--
ALTER TABLE `adv_class_code`
  MODIFY `classcode_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT for table `adv_client`
--
ALTER TABLE `adv_client`
  MODIFY `cl_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT for table `adv_compliance`
--
ALTER TABLE `adv_compliance`
  MODIFY `compliance_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `adv_comp_date`
--
ALTER TABLE `adv_comp_date`
  MODIFY `cdate_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `adv_comp_defect_case`
--
ALTER TABLE `adv_comp_defect_case`
  MODIFY `defect_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `adv_comp_notice`
--
ALTER TABLE `adv_comp_notice`
  MODIFY `notice_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `adv_comp_reply`
--
ALTER TABLE `adv_comp_reply`
  MODIFY `reply_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `adv_courselist_in_high_court`
--
ALTER TABLE `adv_courselist_in_high_court`
  MODIFY `list_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `adv_court`
--
ALTER TABLE `adv_court`
  MODIFY `court_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `adv_document_uploads`
--
ALTER TABLE `adv_document_uploads`
  MODIFY `upload_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=224;

--
-- AUTO_INCREMENT for table `adv_fir`
--
ALTER TABLE `adv_fir`
  MODIFY `fir_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `adv_holiday`
--
ALTER TABLE `adv_holiday`
  MODIFY `holiday_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `adv_judge`
--
ALTER TABLE `adv_judge`
  MODIFY `judge_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `adv_master_pages`
--
ALTER TABLE `adv_master_pages`
  MODIFY `pages_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `adv_notes`
--
ALTER TABLE `adv_notes`
  MODIFY `notes_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `adv_pessi`
--
ALTER TABLE `adv_pessi`
  MODIFY `pessi_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=902;

--
-- AUTO_INCREMENT for table `adv_referred`
--
ALTER TABLE `adv_referred`
  MODIFY `ref_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `adv_section`
--
ALTER TABLE `adv_section`
  MODIFY `section_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `adv_sms_content`
--
ALTER TABLE `adv_sms_content`
  MODIFY `sms_content_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `adv_stage`
--
ALTER TABLE `adv_stage`
  MODIFY `stage_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `adv_sub_client`
--
ALTER TABLE `adv_sub_client`
  MODIFY `sub_client_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT for table `reg_comp_other`
--
ALTER TABLE `reg_comp_other`
  MODIFY `other_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
