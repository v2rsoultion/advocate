<?php
error_reporting(0);
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::any('show', function () {
//     return view('advocate_admin/share_mail');
// });

Route::any('/','Website\HomeController@home');

Route::any('sign-up','Website\HomeController@sign_up');

Route::any('submit-signup','Website\HomeController@submit_signup');

Route::any('sign-in','Website\HomeController@login');

Route::any('resend-otp/{mobile_number}','Website\HomeController@resend_otp');

Route::any('submit-login','Website\HomeController@submit_login');

Route::any('forgot-password','Website\HomeController@forgot_password');

Route::any('submit-forgot-password','Website\HomeController@submit_forgot_password');

Route::any('reset-password/{id}','Website\HomeController@reset_password');

Route::any('submit-reset-password','Website\HomeController@submit_reset_password');

Route::any('term-services','Website\HomeController@term_services');

Route::any('privacy-policy','Website\HomeController@privacy_policy');

/***************------------------- Admin Urls --------------------------****************/

Route::any('/advocate-panel/','Admin\MainController@login')->middleware('prevent-back-history');

Route::any('advocate-panel/admin-login','Admin\MainController@admin_login')->middleware('prevent-back-history');

Route::any('advocate-panel/forgot-password', 'Admin\MainController@forgot_password')->middleware('prevent-back-history');

Route::any('advocate-panel/reset-password/{id?}', 'Admin\MainController@reset_password')->middleware('prevent-back-history');

Route::any('advocate-panel/password-reset', 'Admin\MainController@password_reset')->middleware('prevent-back-history');

Route::any('/advocate-panel/dashboard','Admin\AdminController@dashboard')->middleware('prevent-back-history');

Route::any('/advocate-panel/logout','Admin\AdminController@logout')->middleware('prevent-back-history');

Route::any('/advocate-panel/change-password','Admin\AdminController@change_password')->middleware('prevent-back-history');

Route::any('/advocate-panel/account-settings','Admin\AdminController@account_settings')->middleware('prevent-back-history');

Route::any('/advocate-panel/account-profile','Admin\AdminController@admin_profile')->middleware('prevent-back-history');


/***************------------------- advocate Urls --------------------------****************/

Route::any('/advocate-panel/add-advocate/{advocate_id?}','Advocate\AdvocateController@add_advocate')->middleware('prevent-back-history');

Route::any('/advocate-panel/view-advocate/{type?}','Advocate\AdvocateController@view_advocate')->middleware('prevent-back-history');

Route::any('/advocate-panel/insert-advocate/{advocate_id?}','Advocate\AdvocateController@insert_advocate')->middleware('prevent-back-history');

Route::any('/advocate-panel/change-advocate-status/{id}/{value}','Advocate\AdvocateController@advocate_status')->middleware('prevent-back-history');

Route::any('/advocate-panel/advocate-login/{advocate_id}','Advocate\AdvocateController@advocate_login')->middleware('prevent-back-history');


/***************------------------- Holiday Urls --------------------------****************/

Route::any('/advocate-panel/add-holiday/{holiday_id?}','Holiday\HolidayController@add_holiday')->middleware('prevent-back-history');

Route::any('/advocate-panel/view-holiday/{type?}','Holiday\HolidayController@view_holiday')->middleware('prevent-back-history');

Route::any('/advocate-panel/insert-holiday/{holiday_id?}','Holiday\HolidayController@insert_holiday')->middleware('prevent-back-history');

Route::any('/advocate-panel/change-holiday-status/{id}/{value}','Holiday\HolidayController@holiday_status')->middleware('prevent-back-history');


/***************------------------- Clients Urls --------------------------****************/

Route::any('/advocate-panel/add-client/{client_id?}','Clients\ClientController@add_client')->middleware('prevent-back-history');

Route::any('/advocate-panel/view-client/{type?}','Clients\ClientController@view_client')->middleware('prevent-back-history');

Route::any('/advocate-panel/insert-client/{client_id?}','Clients\ClientController@insert_client')->middleware('prevent-back-history');

Route::any('/advocate-panel/change-client-status/{id}/{value}','Clients\ClientController@client_status')->middleware('prevent-back-history');

Route::any('/advocate-panel/delete-sub-group/{sub_client_id}','Clients\ClientController@delete_sub_group')->middleware('prevent-back-history');


/***************------------------- Courts Urls --------------------------****************/

Route::any('/advocate-panel/add-court/{court_entry_id?}','Court_Entry\CourtentryController@add_court')->middleware('prevent-back-history');
Route::any('/advocate-panel/view-court/{type?}','Court_Entry\CourtentryController@view_court')->middleware('prevent-back-history');
Route::any('/advocate-panel/insert-court/{court_entry_id?}','Court_Entry\CourtentryController@insert_court')->middleware('prevent-back-history');
Route::any('/advocate-panel/change-court-status/{id}/{value}','Court_Entry\CourtentryController@court_status')->middleware('prevent-back-history');


/***************------------------- Case type entry Urls --------------------------****************/

Route::any('/advocate-panel/add-case-type/{court_type_id?}','Case_Type_Entry\CasetypeentryController@add_case')->middleware('prevent-back-history');

Route::any('/advocate-panel/view-case-type/{type?}','Case_Type_Entry\CasetypeentryController@view_case')->middleware('prevent-back-history');

Route::any('/advocate-panel/check-case-type-name/{type?}','Case_Type_Entry\CasetypeentryController@check_case_type_name')->middleware('prevent-back-history');

Route::any('/advocate-panel/insert-case-type/{court_type_id?}','Case_Type_Entry\CasetypeentryController@insert_case_type')->middleware('prevent-back-history');

Route::any('/advocate-panel/change-case-type-status/{id}/{value}','Case_Type_Entry\CasetypeentryController@case_type_status')->middleware('prevent-back-history');


/***************------------------- Class Code Urls --------------------------****************/

Route::any('/advocate-panel/add-class-code/{class_code_id?}','Class_Code\ClasscodeController@add_class_code')->middleware('prevent-back-history');
Route::any('/advocate-panel/view-class-code/{type?}','Class_Code\ClasscodeController@view_class_code')->middleware('prevent-back-history');
Route::any('/advocate-panel/insert-class-code/{class_code_id?}','Class_Code\ClasscodeController@insert_class_code')->middleware('prevent-back-history');
Route::any('/advocate-panel/change-class-code-status/{id}/{value}','Class_Code\ClasscodeController@class_code_status')->middleware('prevent-back-history');


/***************------------------- Case sub type Urls --------------------------****************/

Route::any('/advocate-panel/add-case-sub-type/{court_sub_type_id?}','Case_Sub_Type\CasesubtypeController@add_case_sub_type')->middleware('prevent-back-history');
Route::any('/advocate-panel/view-case-sub-type','Case_Sub_Type\CasesubtypeController@view_case_sub_type')->middleware('prevent-back-history');
Route::any('/advocate-panel/insert-case-sub-type/{court_sub_type_id?}','Case_Sub_Type\CasesubtypeController@insert_case_sub_type')->middleware('prevent-back-history');
Route::any('/advocate-panel/change-case-sub-type-status/{id}/{value}','Case_Sub_Type\CasesubtypeController@case_sub_type_status')->middleware('prevent-back-history');


/***************------------------- Stage Urls --------------------------****************/

Route::any('/advocate-panel/add-stage/{stage_id?}','Stage\StageController@add_stage')->middleware('prevent-back-history');
Route::any('/advocate-panel/view-stage/{type?}','Stage\StageController@view_stage')->middleware('prevent-back-history');
Route::any('/advocate-panel/insert-stage/{stage_id?}','Stage\StageController@insert_stage')->middleware('prevent-back-history');
Route::any('/advocate-panel/change-stage-status/{id}/{value}','Stage\StageController@stage_status')->middleware('prevent-back-history');


/***************------------------- Acts Urls --------------------------****************/

Route::any('/advocate-panel/add-act/{act_id?}','Act\ActController@add_act')->middleware('prevent-back-history');
Route::any('/advocate-panel/view-act/{type?}','Act\ActController@view_act')->middleware('prevent-back-history');
Route::any('/advocate-panel/insert-act/{act_id?}','Act\ActController@insert_act')->middleware('prevent-back-history');
Route::any('/advocate-panel/change-act-status/{id}/{value}','Act\ActController@act_status')->middleware('prevent-back-history');


/***************------------------- Section Urls --------------------------****************/

Route::any('/advocate-panel/add-section/{section_id?}','Section\SectionController@add_section')->middleware('prevent-back-history');
Route::any('/advocate-panel/view-section','Section\SectionController@view_section')->middleware('prevent-back-history');
Route::any('/advocate-panel/insert-section/{section_id?}','Section\SectionController@insert_section')->middleware('prevent-back-history');
Route::any('/advocate-panel/change-section-status/{id}/{value}','Section\SectionController@section_status')->middleware('prevent-back-history');


/***************------------------- Judge Urls --------------------------****************/

Route::any('/advocate-panel/add-judge/{judge_id?}','Judge\JudgeController@add_judge')->middleware('prevent-back-history');
Route::any('/advocate-panel/view-judge/{type?}','Judge\JudgeController@view_judge')->middleware('prevent-back-history');
Route::any('/advocate-panel/insert-judge/{judge_id?}','Judge\JudgeController@insert_judge')->middleware('prevent-back-history');
Route::any('/advocate-panel/change-judge-status/{id}/{value}','Judge\JudgeController@judge_status')->middleware('prevent-back-history');


/***************------------------- Reffered Urls --------------------------****************/

Route::any('/advocate-panel/add-referred/{reffered_id?}','Reffered\RefferedController@add_reffered')->middleware('prevent-back-history');
Route::any('/advocate-panel/view-referred/{type?}','Reffered\RefferedController@view_reffered')->middleware('prevent-back-history');
Route::any('/advocate-panel/insert-referred/{reffered_id?}','Reffered\RefferedController@insert_reffered')->middleware('prevent-back-history');
Route::any('/advocate-panel/change-referred-status/{id}/{value}','Reffered\RefferedController@reffered_status')->middleware('prevent-back-history');


/***************------------------- Assigned Urls --------------------------****************/

Route::any('/advocate-panel/add-assign/{assign_id?}','Assigned\AssignedController@add_assign')->middleware('prevent-back-history');
Route::any('/advocate-panel/view-assign/{type?}','Assigned\AssignedController@view_assign')->middleware('prevent-back-history');
Route::any('/advocate-panel/insert-assign/{assign_id?}','Assigned\AssignedController@insert_assigned')->middleware('prevent-back-history');
Route::any('/advocate-panel/change-assign-status/{id}/{value}','Assigned\AssignedController@assigned_status')->middleware('prevent-back-history');


/***************------------------- Fir Urls --------------------------****************/

Route::any('/advocate-panel/add-fir/{fir_id?}','Fir\FirController@add_fir')->middleware('prevent-back-history');
Route::any('/advocate-panel/view-fir','Fir\FirController@view_fir')->middleware('prevent-back-history');
Route::any('/advocate-panel/insert-fir/{fir_id?}','Fir\FirController@insert_fir')->middleware('prevent-back-history');
Route::any('/advocate-panel/change-fir-status/{id}/{value}','Fir\FirController@fir_status')->middleware('prevent-back-history');


/***************------------------- Order Judgement Upload --------------------------****************/

Route::any('/advocate-panel/add-order-judgment/{order_judgment_id?}','Order_Judgement\Order_JudgementController@add_order_judgment')->middleware('prevent-back-history');

Route::any('/advocate-panel/view-order-judgment/{search_type?}','Order_Judgement\Order_JudgementController@view_order_judgment')->middleware('prevent-back-history');

Route::any('/advocate-panel/insert-order-judgment/{order_judgment_id?}','Order_Judgement\Order_JudgementController@insert_order_judgment')->middleware('prevent-back-history');

Route::any('/advocate-panel/get-all-caption','Order_Judgement\Order_JudgementController@get_all_caption')->middleware('prevent-back-history');

Route::any('/advocate-panel/download-order-judgement/{type?}','Order_Judgement\Order_JudgementController@download_order_judgement')->middleware('prevent-back-history');


Route::any('/advocate-panel/ajax-case-type/{case_type?}/{court_type?}','Order_Judgement\Order_JudgementController@ajax_case_type')->middleware('prevent-back-history');


Route::any('/advocate-panel/ajax-case-year/{case_year?}/{case_type?}','Order_Judgement\Order_JudgementController@ajax_case_year')->middleware('prevent-back-history');


Route::any('/advocate-panel/ajax-case-no/{case_no?}','Order_Judgement\Order_JudgementController@ajax_case_no')->middleware('prevent-back-history');

Route::any('/advocate-panel/order-judgement-case-type/{court_name?}/{court_type?}','Order_Judgement\Order_JudgementController@order_judgement_case_type')->middleware('prevent-back-history');


/***************------------------- Case Registration Form Urls --------------------------****************/

Route::any('/advocate-panel/add-case-form/{case_registration_form_id?}','Case_Regisration_Form\CaseregisrationformController@add_case_form')->middleware('prevent-back-history');

Route::any('/advocate-panel/view-case-form/{search_type?}','Case_Regisration_Form\CaseregisrationformController@view_case_form')->middleware('prevent-back-history');

Route::any('/advocate-panel/insert-case-form/{case_registration_form_id?}','Case_Regisration_Form\CaseregisrationformController@insert_case_registration')->middleware('prevent-back-history');

Route::any('/advocate-panel/change-case-form-status/{id}/{value}','Case_Regisration_Form\CaseregisrationformController@case_registration_status')->middleware('prevent-back-history');

Route::any('/advocate-panel/delete-document/{upload_id?}','Case_Regisration_Form\CaseregisrationformController@delete_document')->middleware('prevent-back-history');

Route::any('/advocate-panel/change-client-sub-type-ajax/{id?}','Client_Sub_Type_ajax\ClientsubtypeajaxController@client_sub_type_ajax')->middleware('prevent-back-history');

Route::any('/advocate-panel/get-case-category/{id?}','Client_Sub_Type_ajax\ClientsubtypeajaxController@get_case_category')->middleware('prevent-back-history');

Route::any('/advocate-panel/change-act-sub-type-section-ajax/{id?}','Client_Sub_Type_ajax\ClientsubtypeajaxController@act_sub_type_section')->middleware('prevent-back-history');

Route::any('/advocate-panel/change-sub-group-ajax/{id?}','Client_Sub_Type_ajax\ClientsubtypeajaxController@sub_group_change_ajax')->middleware('prevent-back-history');

Route::any('/advocate-panel/check-case-number/{case_number}/{case_type}/{category_type}','Client_Sub_Type_ajax\ClientsubtypeajaxController@check_case_number')->middleware('prevent-back-history');

Route::any('/advocate-panel/get-all-court/{court_type}','Client_Sub_Type_ajax\ClientsubtypeajaxController@get_all_court')->middleware('prevent-back-history');


Route::any('/get-all-clients','Client_Sub_Type_ajax\ClientsubtypeajaxController@get_all_clients')->middleware('prevent-back-history');


/***************------------------- Sms to Client --------------------------****************/

Route::any('/advocate-panel/sms-client','Sms_Client\Sms_ClientController@sms_client')->middleware('prevent-back-history');

Route::any('/advocate-panel/get-sub-client-ajax/{client_id}','Sms_Client\Sms_ClientController@get_sub_client_ajax')->middleware('prevent-back-history');

Route::any('/advocate-panel/get-sub-client-ajax-email/{client_id}','Sms_Client\Sms_ClientController@get_sub_client_ajax_email')->middleware('prevent-back-history');

Route::any('/advocate-panel/sms-to-client','Sms_Client\Sms_ClientController@sms_to_client')->middleware('prevent-back-history');

Route::any('/advocate-panel/send-sms-client/{sms?}','Sms_Client\Sms_ClientController@send_sms_client')->middleware('prevent-back-history');

Route::any('/advocate-panel/send-sms-client-email/{sms?}','Sms_Client\Sms_ClientController@send_sms_client_email')->middleware('prevent-back-history');

Route::any('/advocate-panel/sms-text','Sms_Client\Sms_ClientController@sms_text');

Route::any('/advocate-panel/insert-sms-text','Sms_Client\Sms_ClientController@insert_sms_text');


/***************------------------- Peshi Entry Urls --------------------------****************/

Route::any('/advocate-panel/add-peshi/{pessi_id?}','Pessi_Entry\Pessi_EntryController@add_pessi')->middleware('prevent-back-history');

Route::any('/advocate-panel/view-peshi/{pessi_count?}/{pessi_date?}/{court_type?}/{search_type?}','Pessi_Entry\Pessi_EntryController@view_pessi')->middleware('prevent-back-history');

Route::any('/advocate-panel/insert-peshi/{pessi_id?}','Pessi_Entry\Pessi_EntryController@insert_pessi')->middleware('prevent-back-history');

Route::any('/advocate-panel/edit-peshi/{pessi_id?}','Pessi_Entry\Pessi_EntryController@edit_pessi')->middleware('prevent-back-history');

Route::any('/advocate-panel/delete-pessi/{pessi_id}','Pessi_Entry\Pessi_EntryController@delete_pessi')->middleware('prevent-back-history');

Route::any('/advocate-panel/delete-pessi-new','Pessi_Entry\Pessi_EntryController@delete_pessi_new')->middleware('prevent-back-history');

Route::any('/advocate-panel/insert-cause-list/{pessi_id?}','Pessi_Entry\Pessi_EntryController@insert_cause_list')->middleware('prevent-back-history');

Route::any('/advocate-panel/change-peshi-status/{id}/{value}','Pessi_Entry\Pessi_EntryController@pessi_status')->middleware('prevent-back-history');

Route::any('/advocate-panel/get-cause-list/{pessi_id?}/{currentdate?}','Pessi_Entry\Pessi_EntryController@get_cause_list')->middleware('prevent-back-history');


Route::any('advocate-panel/get-case-registeration/{reg_id}','Pessi_Entry\Pessi_EntryController@get_case_registration')->middleware('prevent-back-history');

Route::any('/advocate-panel/print-cause-list','Pessi_Entry\Pessi_EntryController@print_cause_list')->middleware('prevent-back-history');


Route::any('/advocate-panel/daily-diary/{search_type?}','Pessi_Entry\Pessi_EntryController@daily_cause_list')->middleware('prevent-back-history');

Route::any('/advocate-panel/send-sms-daily','Pessi_Entry\Pessi_EntryController@send_sms_daily')->middleware('prevent-back-history');

Route::any('/advocate-panel/insert-daily-diary/{pessi_id?}','Pessi_Entry\Pessi_EntryController@insert_daily_cause_list')->middleware('prevent-back-history');

Route::any('/advocate-panel/print-daily-diary/{further_date?}/{court_type?}','Pessi_Entry\Pessi_EntryController@print_daily_cause_list')->middleware('prevent-back-history');

/***************------------------- Course List Urls --------------------------****************/

Route::any('/advocate-panel/add-course-list/{list_id?}','Course_List\Course_ListController@add_course_list')->middleware('prevent-back-history');

Route::any('/advocate-panel/view-course-list','Course_List\Course_ListController@view_course_list')->middleware('prevent-back-history');

Route::any('/advocate-panel/insert-course-list/{list_id?}','Course_List\Course_ListController@insert_course_list')->middleware('prevent-back-history');

Route::any('/advocate-panel/change-course-list-status/{id}/{value}','Course_List\Course_ListController@course_list_status')->middleware('prevent-back-history');

Route::any('advocate-panel/get-case-registeration-course/{reg_id?}','Course_List\Course_ListController@get_case_registration_course')->middleware('prevent-back-history');

/************************ Due Course -------------------------*/


Route::any('/advocate-panel/due-course/{search_type?}','Pessi_Entry\Pessi_EntryController@due_cause')->middleware('prevent-back-history');

Route::any('/advocate-panel/undated-case/{search_type?}','Pessi_Entry\Pessi_EntryController@undated_case')->middleware('prevent-back-history');

Route::any('/advocate-panel/download-undated-case-all/{file_type}','Pessi_Entry\Pessi_EntryController@download_undated_case')->middleware('prevent-back-history');

/************************ case registeration details-------------------------*/

Route::any('/advocate-panel/case-detail/{registration_id}','Case_Regisration_Form\CaseregisrationformController@case_registration_details')->middleware('prevent-back-history');


Route::any('/advocate-panel/share-email','Case_Regisration_Form\CaseregisrationformController@share_email');


/************************ case registeration details-------------------------*/

Route::get('/advocate-panel/filter-reporting','Reporting\ReportingController@filter_reporting')->middleware('prevent-back-history');

Route::post('/advocate-panel/filter-reporting','Reporting\ReportingController@view_filter_reporting');

Route::any('/advocate-panel/court-type/{court_type}','Reporting\ReportingController@court_type')->middleware('prevent-back-history');

Route::any('/advocate-panel/change-group/{group_type}','Reporting\ReportingController@change_group')->middleware('prevent-back-history');

Route::post('/advocate-panel/download-filter-reporting','Reporting\ReportingController@download_filter_reporting');


/***************------------------- compliance Urls --------------------------****************/

Route::any('/advocate-panel/add-compliance/{compliance_id?}','Compliance\ComplianceController@add_compliance')->middleware('prevent-back-history');

Route::any('/advocate-panel/get-case-registeration-category/{compliance_category}','Compliance\ComplianceController@get_case_registeration_category')->middleware('prevent-back-history');

Route::any('/advocate-panel/get-case-registeration-file/{compliance_category}','Compliance\ComplianceController@get_case_registeration_file')->middleware('prevent-back-history');

Route::any('/advocate-panel/get-type-case/{compliance_category}','Compliance\ComplianceController@get_type_case')->middleware('prevent-back-history');

Route::any('/advocate-panel/download-compliance/{file_type}','Compliance\ComplianceController@download_compliance')->middleware('prevent-back-history');

Route::any('/advocate-panel/view-compliance/{show_type?}','Compliance\ComplianceController@view_compliance')->middleware('prevent-back-history');

Route::any('/advocate-panel/insert-compliance/{compliance_id?}','Compliance\ComplianceController@insert_compliance')->middleware('prevent-back-history');

Route::any('/advocate-panel/change-compliance-status/{id}/{value}','Compliance\ComplianceController@compliance_status')->middleware('prevent-back-history');


Route::any('/advocate-panel/compliance-court-type/{court_type?}','Compliance\ComplianceController@compliance_court_type')->middleware('prevent-back-history');

Route::any('/advocate-panel/compliance-case-type/{compliance_category?}/{court_name?}/{court_type?}','Compliance\ComplianceController@compliance_case_type')->middleware('prevent-back-history');


/************************ Calendar -------------------------*/

Route::any('/advocate-panel/view-calendar','Calendar\CalendarController@view_calendar')->middleware('prevent-back-history');

Route::any('/advocate-panel/insert-note','Calendar\CalendarController@insert_note')->middleware('prevent-back-history');


/************************ Master Pages -------------------------*/

Route::any('/advocate-panel/about-software','Pages\PagesController@about_software')->middleware('prevent-back-history');

Route::any('/advocate-panel/guideline-software','Pages\PagesController@guideline_software')->middleware('prevent-back-history');

Route::any('/advocate-panel/update-about-software','Pages\PagesController@update_about_software')->middleware('prevent-back-history');

Route::any('/advocate-panel/update-guideline-software','Pages\PagesController@update_guideline_software')->middleware('prevent-back-history');


/***************------------------- Notice Issue Urls --------------------------****************/

Route::any('/advocate-panel/add-notice-issue/{notice_issue_id?}','Notice_Issue\NoticeissueController@add_notice_issue')->middleware('prevent-back-history');
Route::any('/advocate-panel/view-notice-issue','Notice_Issue\NoticeissueController@view_notice_issue')->middleware('prevent-back-history');
Route::any('/advocate-panel/insert-notice-issue/{notice_issue_id?}','Notice_Issue\NoticeissueController@insert_notice_issue')->middleware('prevent-back-history');
Route::any('/advocate-panel/change-notice-issue-status/{id}/{value}','Notice_Issue\NoticeissueController@notice_issue_status')->middleware('prevent-back-history');
Route::any('/advocate-panel/change-notice-case-no-ajax/{notice_caseno_id?}','Notice_Issue\NoticeissueController@notice_issue_case_no_ajax')->middleware('prevent-back-history');

/***************------------------- Date Urls --------------------------****************/

Route::any('/advocate-panel/add-date/{cdate_id?}','Date\DateController@add_date')->middleware('prevent-back-history');
Route::any('/advocate-panel/view-date','Date\DateController@view_date')->middleware('prevent-back-history');
Route::any('/advocate-panel/insert-date/{cdate_id?}','Date\DateController@insert_date')->middleware('prevent-back-history');
Route::any('/advocate-panel/change-date-status/{id}/{value}','Date\DateController@date_status')->middleware('prevent-back-history');
Route::any('/advocate-panel/change-date-case-no-ajax/{id_date_change?}','Date\DateController@date_case_no_ajax')->middleware('prevent-back-history');


/***************------------------- Defect Case Urls --------------------------****************/

Route::any('/advocate-panel/add-defect-case/{defect_id?}','Defect_Case\DefectcaseController@add_defect_case')->middleware('prevent-back-history');
Route::any('/advocate-panel/view-defect-case','Defect_Case\DefectcaseController@view_defect_case')->middleware('prevent-back-history');
Route::any('/advocate-panel/insert-defect-case/{defect_id?}','Defect_Case\DefectcaseController@insert_defect_case')->middleware('prevent-back-history');
Route::any('/advocate-panel/change-defect-case-status/{id}/{value}','Defect_Case\DefectcaseController@defect_case_status')->middleware('prevent-back-history');
Route::any('/advocate-panel/change-defect-case-no-ajax/{defect_case_no_id?}','Defect_Case\DefectcaseController@defeact_case_no_ajax')->middleware('prevent-back-history');


/***************------------------- Reply Urls --------------------------****************/

Route::any('/advocate-panel/add-reply/{reply_id?}','Reply\ReplyController@add_reply')->middleware('prevent-back-history');
Route::any('/advocate-panel/view-reply','Reply\ReplyController@view_reply')->middleware('prevent-back-history');
Route::any('/advocate-panel/insert-reply/{reply_id?}','Reply\ReplyController@insert_reply')->middleware('prevent-back-history');
Route::any('/advocate-panel/change-reply-status/{id}/{value}','Reply\ReplyController@reply_status')->middleware('prevent-back-history');
Route::any('/advocate-panel/change-reply-no-ajax/{reply_case_no_id?}','Reply\ReplyController@reply_case_no_ajax')->middleware('prevent-back-history');


/***************------------------- Certified Copy Date Urls --------------------------****************/

Route::any('/advocate-panel/add-certified-copy-date/{ccd_id?}','Certified_Copy_Date\CertifiedcopydateController@add_certified_copy_date')->middleware('prevent-back-history');
Route::any('/advocate-panel/view-certified-copy-date','Certified_Copy_Date\CertifiedcopydateController@view_certified_copy_date')->middleware('prevent-back-history');
Route::any('/advocate-panel/insert-certified-copy-date/{ccd_id?}','Certified_Copy_Date\CertifiedcopydateController@insert_certified_copy_date')->middleware('prevent-back-history');
Route::any('/advocate-panel/change-certified-copy-date-status/{id}/{value}','Certified_Copy_Date\CertifiedcopydateController@certified_copy_date_status')->middleware('prevent-back-history');
Route::any('/advocate-panel/change-certified-case-no-ajax/{certified_caseno_id}','Certified_Copy_Date\CertifiedcopydateController@certified_case_no_ajax')->middleware('prevent-back-history');


/***************------------------- Other Urls --------------------------****************/

Route::any('/advocate-panel/add-other/{other_id?}','Other\OtherController@add_other')->middleware('prevent-back-history');
Route::any('/advocate-panel/view-other','Other\OtherController@view_other')->middleware('prevent-back-history');
Route::any('/advocate-panel/insert-other/{other_id?}','Other\OtherController@insert_other')->middleware('prevent-back-history');
Route::any('/advocate-panel/change-other-status/{id}/{value}','Other\OtherController@other_status')->middleware('prevent-back-history');
Route::any('/advocate-panel/change-other-case-no-ajax/{other_caseno_id?}','Other\OtherController@other_case_no_ajax')->middleware('prevent-back-history');
