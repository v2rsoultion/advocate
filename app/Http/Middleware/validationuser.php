<?php

namespace App\Http\Middleware;

use Closure;

class validationuser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(!$request->session()->has('wow_user_id')) {
            return redirect('');
        }

        return $next($request);
    }
}
