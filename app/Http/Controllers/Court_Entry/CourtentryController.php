<?php

namespace App\Http\Controllers\Court_Entry;

use Illuminate\Http\Request;
use App\Model\Court\Court; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Admin\Account; // model name
use App\Http\Controllers\Controller;
use DB;
class CourtentryController extends Controller
{

    public function __construct()
      {
          $this->middleware('Validation');
      }
      
    //add court entry

    public function add_court($court_entry_id = false){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if(isset(json_decode($admin_details[0]->special_permission)->master_modules)){

      } else {
        return Redirect()->back();
      }

      $admin_id = session('admin_id');

      if($court_entry_id!=''){
          $get_record = DB::select('select * from adv_court where sha1(court_id) = "'.$court_entry_id.'" and user_admin_id = "'.$admin_id.'" ');

          if(count($get_record) == 0){
            return Redirect()->back();
          }

          $title = 'Add Court';
          $data  = compact('title','admin_details','get_record');
          return view('advocate_admin/add_court',$data);  
      }else{
          $get_record = "";
      }      
      $title = 'Add Court';
      $data  = compact('title','admin_details','get_record');
      return view('advocate_admin/add_court',$data);	
    }


    // court view

    public function view_court(Request $request,$type=false){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if(isset(json_decode($admin_details[0]->special_permission)->master_modules)){

      } else {
        return Redirect()->back();
      }
      
      if($request['check'] != ""){
        Court::whereIn('court_id', $request['check'])->delete();
        return Redirect()->back();
      }

      $query = Court::where('user_admin_id','=',session('admin_id'));

      if($request['choose_type'] != ""){
        $query = $query->where('court_type', 'LIKE', '%'.$request['choose_type'].'%');
      }
      if($request['court_name'] != ""){
        $query = $query->where('court_name', 'LIKE', '%'.$request['court_name'].'%');
      }
      if($request['short_name'] != ""){
        $query = $query->where('court_short_name', 'LIKE', '%'.$request['short_name'].'%');
      }
      $app_data =  array('court_name' => $request['court_name'],'choose_type' => $request['choose_type'],'short_name' => $request['short_name']);

      if($type == "all"){
        $perpage    = $query->count();
        $get_record = $query->orderBy('court_id', 'desc')->paginate($perpage)->appends($app_data); 
      } else {
        $get_record = $query->orderBy('court_id', 'desc')->paginate(25)->appends($app_data);
      }

      $choose_type = $request['choose_type'];
      $title = 'View Court';
      $data  = compact('title','admin_details','get_record','choose_type');
      return view('advocate_admin/view_court',$data);
    }

    //insert function

    public function insert_court(Request $request, $court_entry_id = false){

      if($court_entry_id==''){

      if($request['court_name'] == ""){
        return redirect('advocate-panel/add-court');
      }

      $Add = new Court;  
      $Add->court_type             = $request['choose_type'];
      $Add->court_name             = $request['court_name'];
      $Add->court_short_name       = $request['short_name'];
      $Add->user_admin_id          = session('admin_id');
      $Add->court_status = 1;
      $Add->save();
      //return redirect('advocate-panel/view-court');
       return redirect()->back()->with('success', 'Your Data has been added successfully');
    
    } else {

      $values  = array(
        'court_type'         =>  $request['choose_type'],
        'court_name'         =>  $request['court_name'],
        'court_short_name'   =>  $request['short_name']
        );
      Court::where('court_id','=',$court_entry_id)->update($values);
      //return redirect('advocate-panel/view-court');
       return redirect()->back()->with('success', 'Your Data has been updated successfully');
    }

  }

    //court status

    public function court_status($id,$value){

      $admin_id = session('admin_id');
      $get_record = DB::select('select * from adv_court where sha1(court_id) = "'.$id.'" and user_admin_id = "'.$admin_id.'" ');

      if(count($get_record) == 0){
        return Redirect()->back();
      }

      Court::where('court_id', $get_record[0]->court_id)->update(['court_status' => $value]);
      return Redirect()->back();
    }
}
