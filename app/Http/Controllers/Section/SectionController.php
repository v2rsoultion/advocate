<?php

namespace App\Http\Controllers\Section;

use Illuminate\Http\Request;
use App\Model\Section\Section; // model name
use App\Model\Act\Act; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Admin\Account; // model name
use App\Http\Controllers\Controller;
use DB;
class SectionController extends Controller
{
    public function __construct()
      {
          $this->middleware('Validation');
      }
      
    //add section

    public function add_section($section_id = false){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if(isset(json_decode($admin_details[0]->special_permission)->master_modules)){

      } else {
        return Redirect()->back();
      }


      $admin_id = session('admin_id');

      if($section_id!=''){

          $get_record = DB::select('select * from adv_section where sha1(section_id) = "'.$section_id.'" and user_admin_id = "'.$admin_id.'" ');

          if(count($get_record) == 0){
            return Redirect()->back();
          }

          $act   =  Act::where(['user_admin_id' => session('admin_id') , 'act_status' => 1 ])->where('act_name','!=', "")->get();       
          $title = 'Add Section';
          $data  = compact('title','admin_details','get_record','act');
          return view('advocate_admin/add_section',$data);
      
      }else{
          $get_record = "";
      }	

          $act   =  Act::where(['user_admin_id' => session('admin_id') , 'act_status' => 1 ])->where('act_name','!=', "")->get();       
          $title = 'Add Section';
          $data  = compact('title','admin_details','get_record','act');
          return view('advocate_admin/add_section',$data);
    }

    //view section

    public function view_section(Request $request){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if(isset(json_decode($admin_details[0]->special_permission)->master_modules)){

      } else {
        return Redirect()->back();
      }
      
      $query = Section::leftJoin('adv_act', function($join) {$join->on('act_id', '=', 'section_act_id');})->where('adv_section.user_admin_id','=',session('admin_id'));

      if($request['check'] != ""){
        Section::whereIn('section_id', $request['check'])->delete();
        return Redirect()->back();
      }

      if($request['section_act_id'] != ""){
        $query = $query->where('section_act_id', 'LIKE', '%'.$request['section_act_id'].'%');
      }
      if($request['section_name'] != ""){
        $query = $query->where('section_name', 'LIKE', '%'.$request['section_name'].'%');
      }
      if($request['section_code'] != ""){
        $query = $query->where('section_code', 'LIKE', '%'.$request['section_code'].'%');
      }
      if($request['section_short_name'] != ""){
        $query = $query->where('section_short_name', 'LIKE', '%'.$request['section_short_name'].'%');
      }

      $app_data =  array('section_short_name' => $request['section_short_name'],'section_name' => $request['section_name'],'section_act_id' => $request['section_act_id'],'section_code' => $request['section_code']);
      $get_record = $query->orderBy('section_id', 'desc')->paginate(25)->appends($app_data);  

      $act   =  Act::where(['user_admin_id' => session('admin_id') ])->where('act_name','!=', "")->get(); 
      $section_act_id = $request['section_act_id'];

      $title = 'View Section';
      $data  = compact('title','admin_details','get_record','act','section_act_id');
      return view('advocate_admin/view_section',$data);	
    }

    //insert section function

    public function insert_section(Request $request, $section_id = false){

      if($section_id==''){

        if($request['choose_case_type'] == "" || $request['sub_type_name'] == ""){
          return redirect('advocate-panel/add-section');
        }

        $Add = new Section;  
        $Add->section_act_id          = $request['choose_case_type'];
        $Add->section_name            = $request['sub_type_name'];
        $Add->section_short_name      = $request['short_name'];
        $Add->section_code            = $request['code'];
        $Add->user_admin_id           = session('admin_id');
        $Add->section_description     = $request['description'];
        $Add->section_status          = 1;
        $Add->save();
        return redirect('advocate-panel/view-section');
    }
    else
    {
      $values  = array(
        'section_act_id'          =>  $request['choose_case_type'],
        'section_name'            =>  $request['sub_type_name'],
        'section_short_name'      =>  $request['short_name'],
        'section_code'            =>  $request['code'],
        'section_description'     =>  $request['description']
        );
      Section::where('section_id','=',$section_id)->update($values);
      return redirect('advocate-panel/view-section');
    }

  }

    // section status

    public function section_status($id,$value){

      $admin_id = session('admin_id');
      $get_record = DB::select('select * from adv_section where sha1(section_id) = "'.$id.'" and user_admin_id = "'.$admin_id.'" ');

      if(count($get_record) == 0){
        return Redirect()->back();
      }

      Section::where('section_id', $get_record[0]->section_id)->update(['section_status' => $value]);
      return Redirect()->back();
    }
}
