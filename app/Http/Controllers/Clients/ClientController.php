<?php

namespace App\Http\Controllers\Clients;

use Illuminate\Http\Request;
use App\Model\Sub_Client\Sub_Client; // model name
use App\Model\Client\Client; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Admin\Account; // model name
use App\Http\Controllers\Controller;
use DB;
class ClientController extends Controller
{
    public function __construct()
      {
          $this->middleware('Validation');
      }
      
    //add client

    public function add_client($client_id = false){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if(isset(json_decode($admin_details[0]->special_permission)->master_modules)){

      } else {
        return Redirect()->back();
      }

      $admin_id = session('admin_id');

      if($client_id != ''){
        $get_record = DB::select('select * from adv_client where sha1(cl_id) = "'.$client_id.'" and user_admin_id = "'.$admin_id.'" ');

        if(count($get_record) == 0){
          return Redirect()->back();
        }

        $edit_id = $client_id;
        $title = 'Add Client';
        $data  = compact('title','admin_details','get_record','edit_id');
        return view('advocate_admin/add_client',$data);
      }else{
        $get_record = "";
        $edit_id    = "";
      }      
          $title = 'Add Client';
          $data  = compact('title','admin_details','get_record','edit_id');
          return view('advocate_admin/add_client',$data);
        }

    //view client

    public function view_client(Request $request,$type = false){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if(isset(json_decode($admin_details[0]->special_permission)->master_modules)){

      } else {
        return Redirect()->back();
      }

      if($request['check'] != ""){
        Client::whereIn('cl_id', $request['check'])->delete();
        return Redirect()->back();
      }

      $query = Client::where('user_admin_id','=',session('admin_id'));

      if($request['cl_group_name'] != ""){
        $query = $query->where('cl_group_name', 'LIKE', '%'.$request['cl_group_name'].'%');
      }
      if($request['cl_father_name'] != ""){
        $query = $query->where('cl_father_name', 'LIKE', '%'.$request['cl_father_name'].'%');
      }
      if($request['cl_group_email_id'] != ""){
        $query = $query->where('cl_group_email_id', 'LIKE', '%'.$request['cl_group_email_id'].'%');
      }
      if($request['cl_group_mobile_no'] != ""){
        $query = $query->where('cl_group_mobile_no', 'LIKE', '%'.$request['cl_group_mobile_no'].'%');
      }
      if($request['cl_group_place'] != ""){
        $query = $query->where('cl_group_place', 'LIKE', '%'.$request['cl_group_place'].'%');
      }
      if($request['cl_group_type'] != ""){
        $query = $query->where('cl_group_type', '=', $request['cl_group_type']);
      }

      $app_data =  array('cl_group_name' => $request['cl_group_name'],'cl_father_name' => $request['cl_father_name'],'cl_group_email_id' => $request['cl_group_email_id'],'cl_group_mobile_no' => $request['cl_group_mobile_no'],'cl_group_type' => $request['cl_group_type'],'cl_group_place' => $request['cl_group_place'] );

      
      if($type == "all"){
        $perpage    = $query->count();
        $get_record = $query->orderBy('cl_id', 'desc')->paginate($perpage)->appends($app_data);
      } else {
        $get_record = $query->orderBy('cl_id', 'desc')->paginate(25)->appends($app_data);
      }      
      
      $cl_group_type = $request['cl_group_type'];
      $title = 'View Client';
      $data  = compact('title','admin_details','get_record','cl_group_type');
      return view('advocate_admin/view_client',$data);	
    }

    //insert client function

    public function insert_client(Request $request, $client_id = false){


      // print_r($request->input());
      // die;

      if($request['name'] != '' && $request['father_name'] != ''){

        if($client_id==''){
          $query = Client::where('user_admin_id','=',session('admin_id'))->where('cl_group_name', $request['name'])->where('cl_father_name', $request['father_name'])->where('cl_group_type', $request['client_type'])->count();
        } else {
          $query = Client::where('user_admin_id','=',session('admin_id'))->where('cl_group_name', $request['name'])->where('cl_father_name', $request['father_name'])->where('cl_group_type', $request['client_type'])->where('cl_id','!=', $client_id )->count();
        }

        if($query >= 1){
          return redirect()->back()->with('failure', 'Record already exits from that name and father name. ')->withInput();
        }
      
      }

      if($client_id==''){

        if($request['name'] == '' && $request['email'] == '' && $request['mobile'] == '' && $request['place'] == '' ){
          return redirect('advocate-panel/add-client');
        }


        if($request['cl_name_prefix'] == ""){
          $request['father_name'] = "";
        }

          $Add = new Client;  
          $Add->cl_group_name               = $request['name'];
          $Add->cl_name_prefix              = $request['cl_name_prefix'];
          $Add->cl_father_name              = $request['father_name'];
          $Add->cl_group_email_id           = $request['email'];
          $Add->cl_group_mobile_no          = $request['mobile'];
          $Add->cl_group_place              = $request['place'];
          $Add->user_admin_id               = session('admin_id');
          $Add->cl_group_address            = $request['address'];
          $Add->cl_group_type               = $request['client_type'];
          $Add->cl_status                   = 1;
          $Add->save();
          $temp = $Add->id;

          if($request['client_type'] == 2){
            foreach ($request['sub_fild'] as $sub_fild){

              if($sub_fild['sub_name'] != ""){

              if($sub_fild['sub_name_prefix'] == ""){
                $sub_fild['sub_guardian_name'] = "";
              }

                $Add = new Sub_Client;
                $Add->client_id                 = $temp;  
                $Add->sub_client_name           = $sub_fild['sub_name'];
                $Add->sub_name_prefix           = $sub_fild['sub_name_prefix'];
                $Add->sub_guardian_name         = $sub_fild['sub_guardian_name'];
                $Add->sub_client_email_id       = $sub_fild['sub_email'];
                $Add->sub_client_place          = $sub_fild['sub_place'];
                $Add->user_admin_id             = session('admin_id');
                $Add->sub_client_mobile_no      = $sub_fild['sub_mobile'];
                $Add->sub_client_address        = $sub_fild['sub_address'];
                $Add->sub_client_status         = 1;
                $Add->save();
              }
            }
          }
        

      //return redirect('advocate-panel/view-client');
          return redirect()->back()->with('success', 'Your Data has been added successfully');
    }
    else
    {

      if($request['cl_name_prefix'] == ""){
          $request['father_name'] = "";
        }

      $values  = array(
        'cl_group_name'                =>  $request['name'],
        'cl_name_prefix'               =>  $request['cl_name_prefix'],
        'cl_father_name'               =>  $request['father_name'],
        'cl_group_email_id'            =>  $request['email'],
        'cl_group_place'               =>  $request['place'],
        'cl_group_mobile_no'           =>  $request['mobile'],
        'cl_group_address'             =>  $request['address'],
        'cl_group_type'                =>  $request['client_type']
      );
      Client::where('cl_id','=',$client_id)->update($values);


      // print_r($request->input());
      // die;

    //  Sub_Client::where('client_id','=',$client_id)->delete();

      if($request['client_type'] == 2){
        foreach ($request['sub_fild'] as $sub_fild) {

          if($sub_fild['sub_id'] == ""){

            if($sub_fild['sub_name'] != ""){

              if($sub_fild['sub_name_prefix'] == ""){
                $sub_fild['sub_guardian_name'] = "";
              }


              $Add = new Sub_Client;
              $Add->client_id                 = $client_id;  
              $Add->sub_client_name           = $sub_fild['sub_name'];
              $Add->sub_name_prefix           = $sub_fild['sub_name_prefix'];
              $Add->sub_guardian_name         = $sub_fild['sub_guardian_name'];
              $Add->sub_client_email_id       = $sub_fild['sub_email'];
              $Add->sub_client_place          = $sub_fild['sub_place'];
              $Add->user_admin_id             = session('admin_id');
              $Add->sub_client_mobile_no      = $sub_fild['sub_mobile'];
              $Add->sub_client_address        = $sub_fild['sub_address'];
              $Add->sub_client_status         = 1;
              $Add->save();

            }
          } else {

            if($sub_fild['sub_name_prefix'] == ""){
                $sub_fild['sub_guardian_name'] = "";
              }
              
            $values  = array(
              'client_id'                =>  $client_id,
              'sub_client_name'          =>  $sub_fild['sub_name'],
              'sub_name_prefix'          =>  $sub_fild['sub_name_prefix'],
              'sub_guardian_name'        =>  $sub_fild['sub_guardian_name'],
              'sub_client_email_id'      =>  $sub_fild['sub_email'],
              'sub_client_place'         =>  $sub_fild['sub_place'],
              'user_admin_id'            =>  session('admin_id'),
              'sub_client_mobile_no'     =>  $sub_fild['sub_mobile'],
              'sub_client_address'       =>  $sub_fild['sub_address'],
              'sub_client_status'        =>  1,
            );
            Sub_Client::where('sub_client_id','=',$sub_fild['sub_id'])->update($values);

          }
        }
      }

        //return redirect('advocate-panel/view-client');
      return redirect()->back()->with('success', 'Your Data has been updated successfully');
    }
  }

    //client status

    public function client_status($id,$value){

      $admin_id = session('admin_id');
      
      $get_record = DB::select('select * from adv_client where sha1(cl_id) = "'.$id.'" and user_admin_id = "'.$admin_id.'" ');

      if(count($get_record) == 0){
        return Redirect()->back();
      }

      Client::where('cl_id', $get_record[0]->cl_id)->update(['cl_status' => $value]);
      return Redirect()->back();
    }


    // delete sub group

    public function delete_sub_group($sub_client_id){

      Sub_Client::where('sub_client_id','=',$sub_client_id)->delete();

    }

}
