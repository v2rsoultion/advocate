<?php

namespace App\Http\Controllers\Act_Section_Ajax;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Case_Type_Entry\Case_Type_Entry; // model name
use App\Model\Case_Sub_Type\Case_Sub_Type; // model name
use App\Model\Case_Registration\Case_Registration; // model name
use App\Model\Section\Section; // model name
use App\Model\Act\Act; // model name
use App\Model\Admin\Admin; // model name
use DB;

class ClientsubtypeajaxController extends Controller{

    public function __construct()
    {
        $this->middleware('Validation');
    }

    public function client_sub_type_ajax($id = false){

      $client_sub_type = Case_Sub_Type::where('subtype_case_id','=',$id)->get();
      $data  = compact('client_sub_type');
      return view('advocate_admin/client_sub_type_ajax',$data);	
    }


}
