<?php

namespace App\Http\Controllers\Pages;

use Illuminate\Http\Request;
use App\Model\Pages\Pages; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Admin\Account; // model name
use App\Http\Controllers\Controller;
use DB;

class PagesController extends Controller
{

    public function __construct()
      {
          $this->middleware('Validation');
      }
      
    //view about software

    public function about_software($fir_id = false){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();
      $admin_id = session('admin_id');
      
      $get_record = Pages::where(['pages_id' => 1])->first();

      $title = 'About Software';
      $data  = compact('title','admin_details','get_record');
      return view('advocate_admin/about_software',$data);	
    }


    //view guideline software

    public function guideline_software($fir_id = false){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();
      $admin_id = session('admin_id');
      
      $get_record = Pages::where(['pages_id' => 2])->first();

      $title = 'Guideline Software';
      $data  = compact('title','admin_details','get_record');
      return view('advocate_admin/guideline_software',$data); 
    }

    
    //update about software

    public function update_about_software(Request $request){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();
      $admin_id = session('admin_id');
      $get_record = Pages::where(['pages_id' => 1])->first();

      if($request->pages_id != ""){

        $english_pdf = $request->file('english_pdf');
        if($english_pdf != ""){
          $destinationPath = 'uploads/pages/';
          $imageName = 'uploads/pages/'.'english_pdf'.time().'.'.$english_pdf->getClientOriginalExtension();
          $english_pdf->move($destinationPath,$imageName);
          $image=array(
              'pages_english_pdf'         => $imageName
          );
          Pages::where('pages_id','=', 1)->update($image);
        }

        $hindi_pdf = $request->file('hindi_pdf');
        if($hindi_pdf != ""){
          $destinationPath = 'uploads/pages/';
          $imageName = 'uploads/pages/'.'hindi_pdf'.time().'.'.$hindi_pdf->getClientOriginalExtension();
          $hindi_pdf->move($destinationPath,$imageName);
          $image=array(
              'pages_hindi_pdf'         => $imageName
          );
          Pages::where('pages_id','=', 1)->update($image);
        }


        return redirect()->back()->with('success', 'Your content has been updated successfully.'); 

      }

      $title = 'About Software';
      $data  = compact('title','admin_details','get_record');
      return view('advocate_admin/update_about_software',$data); 
    }


    //update guideline software

    public function update_guideline_software(Request $request){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();
      $admin_id = session('admin_id');
      $get_record = Pages::where(['pages_id' => 2])->first();

      if($request->pages_id != ""){

        $english_pdf = $request->file('english_pdf');
        if($english_pdf != ""){
          $destinationPath = 'uploads/pages/';
          $imageName = 'uploads/pages/'.'english_pdf'.time().'.'.$english_pdf->getClientOriginalExtension();
          $english_pdf->move($destinationPath,$imageName);
          $image=array(
              'pages_english_pdf'         => $imageName
          );
          Pages::where('pages_id','=', 2)->update($image);
        }

        $hindi_pdf = $request->file('hindi_pdf');
        if($hindi_pdf != ""){
          $destinationPath = 'uploads/pages/';
          $imageName = 'uploads/pages/'.'hindi_pdf'.time().'.'.$hindi_pdf->getClientOriginalExtension();
          $hindi_pdf->move($destinationPath,$imageName);
          $image=array(
              'pages_hindi_pdf'         => $imageName
          );
          Pages::where('pages_id','=', 2)->update($image);
        }


        return redirect()->back()->with('success', 'Your content has been updated successfully.'); 

      }

      $title = 'Guideline Software';
      $data  = compact('title','admin_details','get_record');
      return view('advocate_admin/update_guideline_software',$data); 
    }




}
