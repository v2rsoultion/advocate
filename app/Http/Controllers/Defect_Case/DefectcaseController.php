<?php

namespace App\Http\Controllers\Defect_Case;

use Illuminate\Http\Request;
use App\Model\Defect_Case\Defect_Case; // model name
use App\Model\Case_Registration\Case_Registration; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Admin\Account; // model name
use App\Http\Controllers\Controller;
use DB;
class DefectcaseController extends Controller
{

    public function __construct()
      {
          $this->middleware('Validation');
      }
      
    //add defect case

    public function add_defect_case($defect_id = false){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();
      $admin_id = session('admin_id');

      if($defect_id!=''){

         $get_record = DB::select('select * from adv_comp_defect_case where sha1(defect_id) = "'.$defect_id.'" and user_admin_id = "'.$admin_id.'" ');

          if(count($get_record) == 0){
            return Redirect()->back();
          }

          $def_case_no = Case_Registration::where('reg_status','=',1)->get();      
          $title       = 'Add Defect Case';
          $data        = compact('title','admin_details','get_record','def_case_no');
          return view('advocate_admin/add_defect_case',$data);
        }else{
          $get_record = "";
        }
          $def_case_no = Case_Registration::where('reg_status','=',1)->get();      
          $title       = 'Add Defect Case';
          $data        = compact('title','admin_details','get_record','def_case_no');
          return view('advocate_admin/add_defect_case',$data);
        }


    // defect case no ajax function
    public function defeact_case_no_ajax($defect_case_no_id){
      $arrayName = Case_Registration::where('reg_id','=',$defect_case_no_id)->first();
      return response()->json(['petitioner'=>$arrayName['reg_petitioner'],'respondent'=>$arrayName['reg_respondent']]);
    }


    //view defect case

    public function view_defect_case(Request $request){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();
      $query = Defect_Case::leftJoin('adv_case_reg', function($join) {$join->on('reg_id', '=', 'defect_case_no');})->where('adv_comp_defect_case.user_admin_id','=',session('admin_id'));

      if($request['check'] != ""){
        Defect_Case::whereIn('defect_id', $request['check'])->delete();
        return Redirect()->back();
      }

      if($request['defect_case_no'] != ""){
        $query = $query->where('defect_case_no', 'LIKE', '%'.$request['defect_case_no'].'%');
      }
      if($request['defect_title'] != ""){
        $query = $query->where('defect_title', 'LIKE', '%'.$request['defect_title'].'%');
      }
      if($request['defect_reason'] != ""){
        $query = $query->where('defect_reason', 'LIKE', '%'.$request['defect_reason'].'%');
      }

      $app_data =  array('defect_case_no' => $request['defect_case_no'],'defect_title' => $request['defect_title'],'defect_reason' => $request['defect_reason']);
      $get_record = $query->orderBy('defect_id', 'desc')->paginate(10)->appends($app_data);      
      $title = 'View Defect Case';
      $data  = compact('title','admin_details','get_record');
      return view('advocate_admin/view_defect_case',$data);	
    }

    //insert defect case function

    public function insert_defect_case(Request $request, $defect_id = false){

      if($defect_id==''){
      $Add = new Defect_Case;  
      $Add->defect_case_no         = $request['case_no'];
      $Add->user_admin_id          = session('admin_id');
      $Add->defect_reason          = $request['act_reason'];
      $Add->defect_status_date     = $request['status_date_s'];
      $Add->defect_status          = 1;
      $Add->save();
      return redirect('advocate-panel/view-defect-case');
    }
    else
    {
      $values  = array(
        'defect_case_no'         =>  $request['case_no'],
        'defect_reason'          =>  $request['act_reason'],
        'defect_status_date'     =>  $request['status_date_s']
        );

      Defect_Case::where('defect_id','=',$defect_id)->update($values);
      return redirect('advocate-panel/view-defect-case');
    }

  }

    //defect case status

    public function defect_case_status($id,$value){
      Defect_Case::where('defect_id', $id)->update(['defect_status' => $value]);
      return Redirect()->back();
    }
}
