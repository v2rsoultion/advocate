<?php

namespace App\Http\Controllers\Sms_Client;

use Illuminate\Http\Request;
use App\Model\Case_Registration\Case_Registration; // model name
use App\Model\Case_Type_Entry\Case_Type_Entry; // model name
use App\Model\Case_Sub_Type\Case_Sub_Type; // model name
use App\Model\Assigned\Assigned; // model name
use App\Model\Reffered\Reffered; // model name
use App\Model\Section\Section; // model name
use App\Model\Client\Client; // model name
use App\Model\Admin\Account; // model name
use App\Model\Stage\Stage; // model name
use App\Model\Sub_Client\Sub_Client; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Act\Act; // model name
use App\Model\Fir\Fir; // model name
use App\Model\Judge\Judge; // model name
use App\Model\Pessi\Pessi; // model name
use App\Model\SmsText\SmsText; // model name
use App\Http\Controllers\Controller;
use DB;
use Excel;

class Sms_ClientController extends Controller
{ 

  public function __construct()
    {
        $this->middleware('Validation');
        $this->send_message = new \App\Http\Controllers\Admin\AdminController;
    }

    //add sms client form

    public function sms_client($reg_id = false){

      $admin_details  = Admin::where(['admin_id' => session('admin_id')])->get();

      if(isset(json_decode($admin_details[0]->special_permission)->sms_to_clients)){

      } else {
        return Redirect()->back();
      }

      $client         = Client::where([ 'cl_status' => 1 , 'user_admin_id' => session('admin_id') ])->where('cl_group_name' ,'!=', '')->get();

      $sub_client     = Sub_Client::where([ 'sub_client_status' => 1 , 'user_admin_id' => session('admin_id') ])->where('sub_client_name' ,'!=', '')->get();

      $referred = Reffered::where([ 'ref_status' => 1 , 'user_admin_id' => session('admin_id') ])->where('ref_advocate_name' ,'!=', '')->get();
      
      // print_r($client);
      // die;

      $title      = 'Sms to Client';
      $data       = compact('title','admin_details','client','sub_client','referred');
      return view('advocate_admin/sms_client',$data);	
    
    }


    public function send_sms_client($sms = false){

      if($sms == "2"){
        $get_case = Case_Registration::leftJoin('adv_pessi', function($join) {$join->on('pessi_case_reg', '=', 'reg_id');})->where('pessi_choose_type', '=', 0)->where('reg_client_group_id', '!=', "")->where('adv_case_reg.user_admin_id','=',session('admin_id'))->where('pessi_status',1)->groupBy('reg_client_group_id')->get(['reg_client_group_id'])->toArray();   
        $client         = Client::where([ 'cl_status' => 1 , 'user_admin_id' => session('admin_id') ])->where('cl_group_name' ,'!=', '')->whereIn('cl_id',$get_case)->get();

      }  else {

        $client         = Client::where([ 'cl_status' => 1 , 'user_admin_id' => session('admin_id') ])->where('cl_group_name' ,'!=', '')->get();

      }

      $data       = compact('client');
      return view('advocate_admin/sms_client_all_ajax',$data); 

    }


    public function send_sms_client_email($sms = false){

      if($sms == "2"){
        $get_case = Case_Registration::leftJoin('adv_pessi', function($join) {$join->on('pessi_case_reg', '=', 'reg_id');})->where('pessi_choose_type', '=', 0)->where('reg_client_group_id', '!=', "")->where('adv_case_reg.user_admin_id','=',session('admin_id'))->where('pessi_status',1)->groupBy('reg_client_group_id')->get(['reg_client_group_id'])->toArray();   
        $client         = Client::where([ 'cl_status' => 1 , 'user_admin_id' => session('admin_id') ])->where('cl_group_name' ,'!=', '')->whereIn('cl_id',$get_case)->get();

      }  else {

        $client         = Client::where([ 'cl_status' => 1 , 'user_admin_id' => session('admin_id') ])->where('cl_group_name' ,'!=', '')->get();

      }

      $data       = compact('client');
      return view('advocate_admin/sms_client_all_ajax_email',$data); 

    }


    public function get_sub_client_ajax($client_id = false){

      $client_id_all = explode(',', $client_id);

      $sub_client     = Sub_Client::where([ 'sub_client_status' => 1 , 'user_admin_id' => session('admin_id') ])->where('sub_client_name' ,'!=', '')->whereIn('client_id' , $client_id_all)->get();

      $data  = compact('sub_client');
      return view('advocate_admin/ajax_sub_client',$data); 

    }


    public function get_sub_client_ajax_email($client_id = false){

      $client_id_all = explode(',', $client_id);

      $sub_client     = Sub_Client::where([ 'sub_client_status' => 1 , 'user_admin_id' => session('admin_id') ])->where('sub_client_name' ,'!=', '')->whereIn('client_id' , $client_id_all)->get();

      $data  = compact('sub_client');
      return view('advocate_admin/ajax_sub_client_email',$data); 

    }



    public function sms_to_client(Request $request){


      // print_r($request->input());
      // die;

      $case_registration =  SmsText::where(['sms_content_type' => 5, 'user_admin_id' => session('admin_id') ])->first();
        
      // For HC - Case Type  Case No. Title Text
      // For TC - Case Type  Case No. NCV No. Title Text

      if($case_registration->sms_content_before != ""){
        $message = $case_registration->sms_content_before;
      }

      $message = $message.' '.$request['reg_sms_text'];

      if($case_registration->sms_content_after != ""){
        $message = $message.' '.$case_registration->sms_content_after;
      }

      foreach($request['referred'] as $referred ) {

        $referred   = Reffered::where([ 'ref_id' => $referred ])->first();
        $mobile_no  = $referred->ref_mobile_number;
        $this->send_message->send_message($mobile_no,$message);

      }


      foreach($request['client'] as $client ) {

        $client     = Client::where([ 'cl_id' => $client ])->first();
        $mobile_no  = $client->cl_group_mobile_no;
        $this->send_message->send_message($mobile_no,$message);

      }

      foreach($request['sub_client'] as $sub_client ) {

        $sub_client  = Sub_Client::where([ 'sub_client_id' => $sub_client ])->first();
        $mobile_no   = $sub_client->sub_client_mobile_no;
        $this->send_message->send_message($mobile_no,$message);

      }


      return redirect()->back()->with('success', 'Your message has been successfully send.'); 

    }


    public function sms_text(Request $request){

      $admin_details  = Admin::where(['admin_id' => session('admin_id')])->get();

      $title      = 'SMS Text';
      $data       = compact('title','admin_details');
      return view('advocate_admin/sms_text',$data); 

    }


    public function insert_sms_text(Request $request){

      $admin_details  = Admin::where(['admin_id' => session('admin_id')])->get();

      $sms_client = SmsText::where(['user_admin_id' => session('admin_id') ])->count();

      //print_r($sms_client);
      //print_r($request->input());
    //  die;


      $values  = array(
        'sms_content_title'     =>  "Sign Up",
        'sms_content_before'    =>  $request['sign_up_before_text'],
        'sms_content_after'     =>  $request['sign_up_after_text'],
        'sms_content_type'      =>  1,
        'user_admin_id'         =>  session('admin_id'),
      );

      if($sms_client == 0){
        SmsText::where('sms_content_type','=',1)->insert($values);
      } else {
        SmsText::where(['sms_content_type' => 1 ,'user_admin_id' => session('admin_id') ])->update($values);
      }
      

      $values  = array(
        'sms_content_title'     =>  "Case Registration",
        'sms_content_before'    =>  $request['case_registration_before_text'],
        'sms_content_after'     =>  $request['case_registration_after_text'],
        'sms_content_type'      =>  2,
        'user_admin_id'         =>  session('admin_id'),
      );

      if($sms_client == 0){
        SmsText::where('sms_content_type','=',2)->insert($values);
      } else {
        SmsText::where(['sms_content_type' => 2 ,'user_admin_id' => session('admin_id') ])->update($values);
      }


      $values  = array(
        'sms_content_title'     =>  "Peshi / Cause List Entry",
        'sms_content_before'    =>  $request['pessi_before_text'],
        'sms_content_after'     =>  $request['pessi_after_text'],
        'sms_content_type'      =>  3,
        'user_admin_id'         =>  session('admin_id'),
      );

      if($sms_client == 0){
        SmsText::where('sms_content_type','=',3)->insert($values);
      } else {
        SmsText::where(['sms_content_type' => 3 ,'user_admin_id' => session('admin_id') ])->update($values);
      }


      $values  = array(
        'sms_content_title'     =>  "Daily Diary",
        'sms_content_before'    =>  $request['daily_diary_before_text'],
        'sms_content_after'     =>  $request['daily_diary_after_text'],
        'sms_content_type'      =>  4,
        'user_admin_id'         =>  session('admin_id'),
      );

      if($sms_client == 0){
        SmsText::where('sms_content_type','=',4)->insert($values);
      } else {
        SmsText::where(['sms_content_type' => 4 ,'user_admin_id' => session('admin_id') ])->update($values);
      }


      $values  = array(
        'sms_content_title'     =>  "Sms to Client",
        'sms_content_before'    =>  $request['sms_client_before_text'],
        'sms_content_after'     =>  $request['sms_client_after_text'],
        'sms_content_type'      =>  5,
        'user_admin_id'         =>  session('admin_id'),
      );

      if($sms_client == 0){
        SmsText::where('sms_content_type','=',5)->insert($values);
      } else {
        SmsText::where(['sms_content_type' => 5 ,'user_admin_id' => session('admin_id') ])->update($values);
      }
      

      return redirect()->back()->with('success', 'Your content has been successfully updated.'); 

    }



}
