<?php

namespace App\Http\Controllers\Fir;

use Illuminate\Http\Request;
use App\Model\Fir\Fir; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Admin\Account; // model name
use App\Http\Controllers\Controller;
use DB;
class FirController extends Controller
{

    public function __construct()
      {
          $this->middleware('Validation');
      }
      
    //add Fir

    public function add_fir($fir_id = false){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();
      $admin_id = session('admin_id');
      
      if($fir_id!=''){

          $get_record = DB::select('select * from adv_fir where sha1(fir_id) = "'.$fir_id.'" and user_admin_id = "'.$admin_id.'" ');

          if(count($get_record) == 0){
            return Redirect()->back();
          }

          $title = 'Add Fir';
          $data  = compact('title','admin_details','get_record');
          return view('advocate_admin/add_fir',$data);
      }else{
          $get_record = "";
      }      
      $title = 'Add Fir';
      $data  = compact('title','admin_details','get_record');
      return view('advocate_admin/add_fir',$data);	
    }

    //view Fir

    public function view_fir(Request $request){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();
      
      if($request['check'] != ""){
        Fir::whereIn('fir_id', $request['check'])->delete();
        return Redirect()->back();
      }

      $query = Fir::where('user_admin_id','=',session('admin_id'));

      if($request['fir_number'] != ""){
        $query = $query->where('fir_number', 'LIKE', '%'.$request['fir_number'].'%');
      }
      if($request['fir_type'] != ""){
        $query = $query->where('fir_year', 'LIKE', '%'.$request['fir_type'].'%');
      }
      if($request['fir_pt_name'] != ""){
        $query = $query->where('fir_pt_name', 'LIKE', '%'.$request['fir_pt_name'].'%');
      }

      $app_data =  array('fir_number' => $request['fir_number'],'fir_type' => $request['fir_type'],'fir_pt_name' => $request['fir_pt_name']);
      $get_record = $query->orderBy('fir_id', 'desc')->paginate(10)->appends($app_data);      
      $title = 'View Fir';
      $data  = compact('title','admin_details','get_record');
      return view('advocate_admin/view_fir',$data);	
    }


    //insert fir function

    public function insert_fir(Request $request, $fir_id = false){

      if($fir_id==''){
      $Add = new Fir;  
      $Add->fir_number          = $request['fir_no'];
      $Add->fir_year            = $request['fir_type'];
      $Add->user_admin_id       = session('admin_id');
      $Add->fir_pt_name         = $request['police_thane_name'];
      $Add->fir_status          = 1;
      $Add->save();
      return redirect('advocate-panel/view-fir');
    }
    else
    {
      $values  = array(
        'fir_number'          =>  $request['fir_no'],
        'fir_year'            =>  $request['fir_type'],
        'fir_pt_name'         =>  $request['police_thane_name']
        );
      Fir::where('fir_id','=',$fir_id)->update($values);
      return redirect('advocate-panel/view-fir');
    }

  }

    //fir status

    public function fir_status($id,$value){
      Fir::where('fir_id', $id)->update(['fir_status' => $value]);
      return Redirect()->back();
    }

}
