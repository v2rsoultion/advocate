<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AccountController extends Controller
{
    public function login(){
    	if(session('admin_id') != ""){
    		return redirect('advocate-panel/dashboard');
    	}
    	$account     = Account::get();
 		$error_msg   = "";
  		$data        = compact('error_msg','account');
  		return view('advocate_admin/login',$data);
    }

    public function admin_login(Request $request){
    	if(session('admin_id') != ""){
    		return redirect('advocate-panel/dashboard');
    	}
	    $admin_login = Admin::where(['admin_email' => $request['admin_email'], 'admin_password' => md5($request['admin_password']) ])->get();
	    if (count($admin_login) == 0){
	      	$account        =   Account::get();
	      	$error_msg      =   "User email or password is/are incorrect!!";
			$data           =   compact('error_msg','account');
			return view('advocate_admin/login',$data);
		}else{
			foreach($admin_login as $admin_data);
			session(['admin_id' => $admin_data->admin_id]);
			$admin_id = session('admin_id');
			return redirect('advocate-panel/dashboard');
		}
	}
}
