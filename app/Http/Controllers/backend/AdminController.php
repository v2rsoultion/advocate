<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function __construct(){
      	$this->middleware('Validation');
  	}

    public function dashboard(){
    	if(session('admin_id') == ""){
    		return redirect('advocate-panel/dashboard');
    	}      

	    $account         =     Account::all();
	    $admin_details   =     Admin::where(['admin_id' => session('admin_id')])->get();
	 	  $title           =     "Dashboard";
	    $data            =     compact('title','permission','admin_details','account');
	    return view('advocate_admin/dashboard',$data);

	  }

    public function logout(){
      session()->forget('admin_id');
      return redirect('advocate-panel');
    }


    public function change_password(Request $request){

      $account     = Account::get();
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if($request['new_password'] != ""){
   
        $admin_id = session('admin_id');
        $admin_login = Admin::where(['admin_password' => md5($request['current_password']), 'admin_id' => $admin_id ])->count();

        if($admin_login == ""){
          $message = "Current password does not match";
          $confirm_message = "";
        }else{
          $values=array(
              'admin_password'          => md5($request['new_password']),
          );
          Admin::where('admin_id', $admin_id)->update($values);
          $message = "";
          $confirm_message = "Password Changed successfully";
        }

        $title      = "Change Password";
        $data       = compact('title','message','confirm_message','account','admin_details');
        return view('advocate_admin/change_password',$data);
        
      }else{
        $message = "";
        $confirm_message = "";
        $title      = "Change Password";
        $data       = compact('title','message','confirm_message','account','admin_details');
        return view('advocate_admin/change_password',$data);
      }
    }

    public function account_settings(Request $request){

      if($request['account_name'] != ""){
        $values=array(
          'account_name'        => $request['account_name'],
          'account_number'      => $request['account_number'],
          'account_email'       => $request['account_email'],
          'account_address'     => $request['account_address'],
        );
        Account::where('account_id', 1)->update($values);
        $file = $request->file('account_logo');
        if($file != ""){
          $destinationPath = 'uploads/admin/';
          $imageName = 'uploads/admin/'.time().'.'.$file->getClientOriginalExtension();
          $file->move($destinationPath,$imageName);
          $images=array(
              'account_logo'       => $imageName
          );
          Account::where('account_id', 1)->update($images);
        }  
        return redirect('advocate-panel/account-settings ');      
      } 

      $account     = Account::get();
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();
      $id = session('admin_id');
      $title      = "Account Settings";
      $data       = compact('title','account','admin_details');
      return view('advocate_admin/account_settings',$data);
    }

    public function admin_profile(Request $request){

      if($request['admin_name'] != ""){
        $values=array(
            'admin_name'       =>   $request['admin_name'],
            'admin_number'     =>   $request['admin_number']
        );
        Admin::where('admin_id', session('admin_id'))->update($values);

        $file = $request->file('admin_image');
        if($file != ""){
          $destinationPath = 'uploads/admin/';
          $imageName = 'uploads/admin/'.time().'.'.$file->getClientOriginalExtension();
          $file->move($destinationPath,$imageName);
          $image=array(
              'admin_image'         => $imageName
          );
          Admin::where('admin_id', session('admin_id'))->update($image);
        }     
        return redirect('/advocate-panel/account-profile');      
      }
      
      $account     = Account::get();
      $id = session('admin_id');
      $get_record = Admin::where('admin_id',$id)->get();
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();
      $admin_detail = Admin::where(['admin_id' => session('admin_id')])->first();
      $title      = "Profile";
      $data       = compact('title','admin_detail','account','admin_details');
      return view('advocate_admin/profile',$data);
    }
}
