<?php

namespace App\Http\Controllers\Assigned;

use Illuminate\Http\Request;
use App\Model\Assigned\Assigned; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Admin\Account; // model name
use App\Http\Controllers\Controller;
use DB;
class AssignedController extends Controller
{

    public function __construct()
      {
          $this->middleware('Validation');
      }
    //add assigned

    public function add_assign($assign_id = false){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if(isset(json_decode($admin_details[0]->special_permission)->master_modules)){

      } else {
        return Redirect()->back();
      }

      $admin_id = session('admin_id');

      if($assign_id!=''){

          $get_record = DB::select('select * from adv_assigned where sha1(assign_id) = "'.$assign_id.'" and user_admin_id = "'.$admin_id.'" ');

          if(count($get_record) == 0){
            return Redirect()->back();
          }

          $title = 'Add Assigned';
          $data  = compact('title','admin_details','get_record');
          return view('advocate_admin/add_assign',$data);       
      }else{
          $get_record = "";
      }      
      $title = 'Add Assigned';
      $data  = compact('title','admin_details','get_record');
      return view('advocate_admin/add_assign',$data);	
    }

    //view assigned

    public function view_assign(Request $request,$type = false){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if(isset(json_decode($admin_details[0]->special_permission)->master_modules)){

      } else {
        return Redirect()->back();
      }
      
      if($request['check'] != ""){
        Assigned::whereIn('assign_id', $request['check'])->delete();
        return Redirect()->back();
      }

      $query = Assigned::where('user_admin_id','=',session('admin_id'));

      if($request['assign_advocate_name'] != ""){
        $query = $query->where('assign_advocate_name', 'LIKE', '%'.$request['assign_advocate_name'].'%');
      }
      if($request['assign_mobile_number'] != ""){
        $query = $query->where('assign_mobile_number', 'LIKE', '%'.$request['assign_mobile_number'].'%');
      }

      $app_data =  array('assign_advocate_name' => $request['assign_advocate_name'],'assign_mobile_number' => $request['assign_mobile_number']);
      

      if($type == "all"){
        $perpage    = $query->count();
        $get_record = $query->orderBy('assign_id', 'desc')->paginate($perpage)->appends($app_data);  
      } else {
        $get_record = $query->orderBy('assign_id', 'desc')->paginate(25)->appends($app_data);  
      }


          
      $title = 'View Assigned';
      $data  = compact('title','admin_details','get_record');
      return view('advocate_admin/view_assign',$data);	
    }


    //insert assigned function

    public function insert_assigned(Request $request, $assign_id = false){

      if($assign_id==''){

        if($request['advocate_assigned_name'] ==''){
          return redirect('advocate-panel/add-assign');
        }

        $Add = new Assigned;  
        $Add->assign_advocate_name         = $request['advocate_assigned_name'];
        $Add->assign_advocate_email        = $request['email'];
        $Add->assign_mobile_number         = $request['mobile_number'];
        $Add->user_admin_id                = session('admin_id');
        $Add->assign_status                = 1;
        $Add->save();
        //return redirect('advocate-panel/view-assign');
        return redirect()->back()->with('success', 'Your Data has been added successfully');
      
      } else {
        $values  = array(
          'assign_advocate_name'         =>  $request['advocate_assigned_name'],
          'assign_advocate_email'        =>  $request['email'],
          'assign_mobile_number'         =>  $request['mobile_number']
          );
        Assigned::where('assign_id','=',$assign_id)->update($values);
        //return redirect('advocate-panel/view-assign');
        return redirect()->back()->with('success', 'Your Data has been updated successfully');
      }

    }

    //court sub type status

    public function assigned_status($id,$value){

      $admin_id = session('admin_id');
      $get_record = DB::select('select * from adv_assigned where sha1(assign_id) = "'.$id.'" and user_admin_id = "'.$admin_id.'" ');

      if(count($get_record) == 0){
        return Redirect()->back();
      }

      Assigned::where('assign_id', $get_record[0]->assign_id)->update(['assign_status' => $value]);
      return Redirect()->back();
    }
}
