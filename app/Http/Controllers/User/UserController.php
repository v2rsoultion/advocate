<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Model\Admin\Admin; // model name
use App\Model\Admin\Account; // model name
use App\Http\Controllers\Controller;

class UserController extends Controller
{

  public function __construct(){
      $this->middleware('Validation');
  }
    //add user

    public function add_user($user_id = false){
      
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();


      if($admin_details[0]->admin_type == 2){
        return Redirect()->back();
      }
      
      if($user_id!=''){
          $get_record = Admin::where('admin_id','=',$user_id)->first();
      }else{
          $get_record = "";
      }

      $title = 'Add User';
      $data  = compact('title','admin_details','get_record');
      return view('advocate_admin/add_user',$data);	
    }

    //view user

    public function view_user(Request $request){
      
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if($admin_details[0]->admin_type == 2){
        return Redirect()->back();
      }

      if($request['check'] != ""){
        Admin::whereIn('admin_id', $request['check'])->delete();
        return Redirect()->back();
      }

      $query = Admin::query();

      if($request['admin_name'] != ""){
        $query = $query->where('admin_name', 'LIKE', '%'.$request['admin_name'].'%');
      }
      if($request['admin_email'] != ""){
        $query = $query->where('admin_email', 'LIKE', '%'.$request['admin_email'].'%');
      }
      if($request['admin_number'] != ""){
        $query = $query->where('admin_number', 'LIKE', '%'.$request['admin_number'].'%');
      }

      $app_data =  array('admin_name' => $request['admin_name'],'admin_email' => $request['admin_email'],'admin_number' => $request['admin_number']);
      $get_record = $query->where(['admin_type' => 2])->orderBy('admin_id', 'desc')->paginate(25)->appends($app_data);      
      $title = 'View User';
      $data  = compact('title','admin_details','get_record');
      return view('advocate_admin/view_user',$data);	
    }

    //insert user function

    public function insert_user(Request $request, $user_id = false){
      
      if($user_id==''){

        $get_record = Admin::where('admin_email','=', $request['email'] )->count();

        if($get_record != 0){
          return redirect()->back()->with('success', 'An account for the specified email address already exists.');
        }

        $Add = new Admin;  
        $Add->admin_name            = $request['name'];
        $Add->admin_email           = $request['email'];
        $Add->admin_number          = $request['number'];
        $Add->admin_password        = md5($request['password']);
        $Add->admin_type            = 2;
        $Add->save();
        $admin_id = $Add->id;

        $file = $request->file('admin_image');
        if($file != ""){
          $destinationPath = 'uploads/admin/';
          $imageName = 'uploads/admin/'.time().'.'.$file->getClientOriginalExtension();
          $file->move($destinationPath,$imageName);
          $image=array(
              'admin_image'         => $imageName
          );
          Admin::where('admin_id', $admin_id)->update($image);
        }


        return redirect('advocate-panel/view-user');

      }  else  {

        $values  = array(
          'admin_name'             =>  $request['name'],
          'admin_number'           =>  $request['number'],
        );

        Admin::where('admin_id','=',$user_id)->update($values);

        $file = $request->file('admin_image');
        if($file != ""){
          $destinationPath = 'uploads/admin/';
          $imageName = 'uploads/admin/'.time().'.'.$file->getClientOriginalExtension();
          $file->move($destinationPath,$imageName);
          $image=array(
              'admin_image'         => $imageName
          );
          Admin::where('admin_id', $user_id)->update($image);
        }

      return redirect('advocate-panel/view-user');

    }
  }

    //user status

    public function user_status($id,$value){
      Admin::where('admin_id', $id)->update(['admin_status' => $value]);
      return Redirect()->back();
    }


    //user login

    public function user_login($user_id = false){
      
      $get_record = Admin::where('admin_id','=',$user_id)->first();

      // print_r($get_record->admin_id);
      // die;
      
      session(['admin_id' => $get_record->admin_id]);
      $admin_id = session('admin_id');
      return redirect('advocate-panel/dashboard');
 
    }




}
