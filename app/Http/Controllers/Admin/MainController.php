<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Admin; // model name
use App\Model\Admin\Account; // model name
use Mail;
use DB;


class MainController extends Controller
{

	public function login(){
    	if(session('admin_id') != ""){
    		return redirect('advocate-panel/dashboard');
    	}
    	$account     = Account::get();
 		$error_msg   = "";
  		$data        = compact('error_msg','account');
  		return view('advocate_admin/login',$data);
    }


    public function admin_login(Request $request){
    	if(session('admin_id') != ""){
    		return redirect('advocate-panel/dashboard');
    	}
	    $admin_login = Admin::where(['admin_email' => $request['admin_email'], 'admin_password' => md5($request['admin_password']) ])->get();
	    if (count($admin_login) == 0){
	      	$account        =   Account::get();
	      	$error_msg      =   "Incorrect Email id or password.";
			$data           =   compact('error_msg','account');
			return view('advocate_admin/login',$data);
		}else{
			foreach($admin_login as $admin_data);

            if($admin_data->admin_status == 0){
                $account        =   Account::get();
                $error_msg      =   "Your account has been deactivated by admin.";
                $data           =   compact('error_msg','account');
                return view('advocate_admin/login',$data);
            }

            if($admin_data->admin_type == 2){

                $today_date = date("Y-m-d");

                if($admin_data->start_date <= $today_date && $admin_data->end_date >= $today_date){

                    session(['admin_id' => $admin_data->admin_id]);
                    $admin_id = session('admin_id');
                    return redirect('advocate-panel/dashboard');

                }


                if($today_date < $admin_data->start_date ){
                   
                    $account        =   Account::get();
                    $error_msg      =   "You will be able to login on " .date('d F Y',strtotime($admin_data->start_date)).".";
                    $data           =   compact('error_msg','account');
                    return view('advocate_admin/login',$data);
                }

                if($today_date > $admin_data->end_date){

                    $account        =   Account::get();
                    $error_msg      =   "You account has been expired on " .date('d F Y',strtotime($admin_data->end_date)).".";
                    $data           =   compact('error_msg','account');
                    return view('advocate_admin/login',$data);


                }

            //    your account will be activated from 
            }

			session(['admin_id' => $admin_data->admin_id]);
			$admin_id = session('admin_id');
			return redirect('advocate-panel/dashboard');
		}
	}



	public function forgot_password(Request $request){
    	if(session('admin_id') != ""){
    		return redirect('advocate-panel/dashboard');
    	}
    	$error_msg = "";
    	if($request['admin_email'] != ""){
    		$admin_login = Admin::where(['admin_email' => $request['admin_email'] ])->get()->toArray();
    		
    		foreach ($admin_login as $details);
    		$_REQUEST['admin_name'] = $details['admin_name'];
	    	if (count($admin_login) == 0) {
				$error_msg  = " Entered Email Id Is Invalid!";
	    	} else {


                if($details['admin_status'] == 0){
                    $account        =   Account::get();
                    $status_msg      =   "Your account has been deactivated by admin.";
                    $data           =   compact('status_msg','account');
                    return view('advocate_admin/forgot_password',$data);
                }

                if($details['admin_type'] == 2){

                    $today_date = date("Y-m-d");

                    if($details['start_date'] <= $today_date && $details['end_date'] >= $today_date){

                        $data=['admin_id'=> sha1($details['admin_id']) ];
                        Mail::send(['html' => 'advocate_admin/forgot_mail'], $data , function ($message) {
                        $message->to($_REQUEST['admin_email'], $_REQUEST['admin_name'])->subject('Password Change Request');
                        });

                        $values=array( 'reset_password_status' => 0 );
                        Admin::where('admin_email', $request['admin_email'])->update($values);

                        $admin_name = $details['admin_name'];
                        $data1 = compact('admin_name','account');
                        return view('advocate_admin/confirmation',$data1);

                    } else {
                        $account        =   Account::get();
                        $status_msg      =   "Your account has been deactivated by admin.";
                        $data           =   compact('status_msg','account');
                        return view('advocate_admin/forgot_password',$data);
                    }
                    
                }


	    		$data=['admin_id'=> sha1($details['admin_id']) ];
	    		Mail::send(['html' => 'advocate_admin/forgot_mail'], $data , function ($message) {
            	$message->to($_REQUEST['admin_email'], $_REQUEST['admin_name'])->subject('Password Change Request');
            	});

                $values=array( 'reset_password_status' => 0 );
                Admin::where('admin_email', $request['admin_email'])->update($values);

                $admin_name = $details['admin_name'];
                $data1 = compact('admin_name','account');
                return view('advocate_admin/confirmation',$data1);


	    	}
    	}
	 	$account     = Account::get();
	 	$title      = "Forgot Password";
	    $data       = compact('title','account','error_msg','success_msg');
	    return view('advocate_admin/forgot_password',$data);
    }


    public function reset_password(Request $request, $id) {

      $get_record   = DB::select('select * from adv_admin where sha1(admin_id) = "'.$id.'" ');
      
      $account     = Account::get();
      if(empty(count($get_record))){
        return redirect()->back()->withSuccess('IT WORKS!');
      } else {
		if($request['admin_password'] != ""){

			if($request['admin_password'] == $request['admin_cpassword']){
			
				$values=array(
				  'admin_password'          => md5($request['admin_password']),
				);
				Admin::where('admin_id', $get_record[0]->admin_id)->update($values);
				$success_msg = "Password Reset Successfully";
			} else {
				$error_msg  = "Password and Confirm Password doesn't match !!";
			}
		}
        $title       = "Reset Password";
        $admin_id  = $get_record[0]->admin_id;
        $data         = compact('title','admin_id','account','error_msg','success_msg','get_record' );
        return view('advocate_admin/reset_password',$data);
      }
    }


    public function password_reset(Request $request){

        $values=array( 
            'admin_password'            => md5($request['confirm_password']),
            'reset_password_status'     => 1, 
        );

        Admin::where('admin_id', $request['admin_id'])->update($values);

        $admin_login = Admin::where(['admin_id' => $request['admin_id'] ])->first();
        session(['admin_id' => $admin_login->admin_id]);
        return redirect('advocate-panel/dashboard');
    }


	



}
