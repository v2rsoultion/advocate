<?php

namespace App\Http\Controllers\Case_Regisration_Form;

use Illuminate\Http\Request;
use App\Model\Case_Registration\Case_Registration; // model name
use App\Model\Case_Type_Entry\Case_Type_Entry; // model name
use App\Model\Class_Code\Class_Code; // model name
use App\Model\Assigned\Assigned; // model name
use App\Model\Reffered\Reffered; // model name
use App\Model\Section\Section; // model name
use App\Model\Client\Client; // model name
use App\Model\Admin\Account; // model name
use App\Model\Stage\Stage; // model name
use App\Model\Court\Court; // model name
use App\Model\Compliance\Compliance; // model name
use App\Model\Sub_Client\Sub_Client; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Act\Act; // model name
use App\Model\Upload_Document\Upload_Document; // model name
use App\Model\Judge\Judge; // model name
use App\Model\Fir\Fir; // model name
use App\Model\Pessi\Pessi; // model name
use App\Model\SmsText\SmsText; // model name
use App\Http\Controllers\Controller;
use DB;
use Mail;

class CaseregisrationformController extends Controller
{

    public function __construct()
      {
          $this->middleware('Validation');
          $this->send_message = new \App\Http\Controllers\Admin\AdminController;
      }
      
    //add case registration form

    public function add_case_form($reg_id = false){
    //  $this->pessi->hello($pa1,);
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if(isset(json_decode($admin_details[0]->special_permission)->case_registration)){

      } else {
        return Redirect()->back();
      }

      
      $admin_id = session('admin_id');

      if($reg_id !=''){

        $get_record = DB::select('select * from adv_case_reg where sha1(reg_id) = "'.$reg_id.'" and user_admin_id = "'.$admin_id.'" ');

        if(count($get_record) == 0){
          return Redirect()->back();
        }

        $edit_id = $reg_id;
          
      } else {

        $get_record   = "";
        $edit_id      = "";
      }

      // print_r($get_record[0]->reg_court);
      // die;
          
      $case_type_entry   = Case_Type_Entry::where([ 'case_status' => 1 , 'user_admin_id' => $admin_id ])->where('case_name' ,'!=', '')->get();
      $case_sub_type     = Class_Code::where([ 'classcode_status' => 1 , 'user_admin_id' => $admin_id ])->where('classcode_name' ,'!=', '')->get();
      $fir               = Fir::where([ 'fir_status' => 1 , 'user_admin_id' => $admin_id ])->get();
      $stage             = Stage::where([ 'stage_status' => 1 , 'user_admin_id' => $admin_id ])->where('stage_name' ,'!=', '')->get();
      $assigned          = Assigned::where([ 'assign_status' => 1 , 'user_admin_id' => $admin_id ])->where('assign_advocate_name' ,'!=', '')->get();
      $act               = Act::where([ 'act_status' => 1 , 'user_admin_id' => $admin_id ])->where('act_name' ,'!=', '')->get();
      $section           = Section::where([ 'section_status' => 1 , 'user_admin_id' => $admin_id ])->where('section_name' ,'!=', '')->get();
      $reffered          = Reffered::where([ 'ref_status' => 1 , 'user_admin_id' => $admin_id ])->where('ref_advocate_name' ,'!=', '')->get();
      $client            = Client::where([ 'cl_status' => 1 , 'user_admin_id' => $admin_id ])->where('cl_group_name' ,'!=', '')->get();
      $sub_client        = Sub_Client::where([ 'sub_client_status' => 1 , 'user_admin_id' => $admin_id ])->where('sub_client_name' ,'!=', '')->get();
      $high_court        = Court::where([ 'court_status' => 1 , 'user_admin_id' => $admin_id , 'court_type' => 2 ])->where('court_name' ,'!=', '')->get();
      $trail_court       = Court::where([ 'court_status' => 1 , 'user_admin_id' => $admin_id , 'court_type' => 1 ])->where('court_name' ,'!=', '')->get();       
      
      $title             = 'Add Case';
      $data              = compact('title','edit_id','sub_client','admin_details','act','get_record','fir','case_type_entry','case_sub_type','stage','assigned','reffered','section','client','high_court','trail_court');
      return view('advocate_admin/add_case_registration',$data);	
    }


    //view case registration form

    public function view_case_form(Request $request , $search_type=false){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if(isset(json_decode($admin_details[0]->special_permission)->case_registration)){

      } else {
        return Redirect()->back();
      }
      
      if($request['check'] != ""){
        Case_Registration::whereIn('reg_id', $request['check'])->delete();
        return Redirect()->back();
      }

      $request['reg_section_id'] = $request['section'];
      // print_r($request['reg_section_id']);
      // die;

      // 

      $query = Case_Registration::leftJoin('adv_pessi', function($join) {$join->on('pessi_case_reg', '=', 'reg_id');})->leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->leftJoin('adv_class_code', function($join) {$join->on('classcode_id', '=', 'reg_case_subtype_id');})->leftJoin('adv_fir', function($join) {$join->on('fir_id', '=', 'reg_fir_id');})->leftJoin('adv_stage', function($join) {$join->on('stage_id', '=', 'reg_stage_id');})->leftJoin('adv_assigned', function($join) {$join->on('assign_id', '=', 'reg_assigend_id');})->leftJoin('adv_act', function($join) {$join->on('act_id', '=', 'reg_act_id');})->leftJoin('adv_referred', function($join) {$join->on('ref_id', '=', 'reg_reffeerd_by_id');})->leftJoin('adv_client', function($join) {$join->on('cl_id', '=', 'reg_client_group_id');})->leftJoin('adv_sub_client', function($join) {$join->on('sub_client_id', '=', 'reg_client_subgroup_id');})->where('adv_case_reg.user_admin_id','=',session('admin_id'));      

      if($search_type == "all"){
        $request['court_type'] = 3;
        $request['case_status'] = 3;
      
      } else {
        if($court_type != ""){
          $request['court_type'] = $court_type;
        } else if($request['court_type'] != ""){
          $request['court_type'] = $request['court_type'];
        } else {
          $request['court_type'] = 3;
        }

        if($request['case_status'] == ""){
          $request['case_status'] = 1;
        }
      }

      if($request['reg_file_no'] != ""){
        $query = $query->where('reg_file_no', 'LIKE', '%'.$request['reg_file_no'].'%');
      }

      if($request['case_no'] != ""){
        $query = $query->where('reg_case_number', 'LIKE', '%'.$request['case_no'].'%');
      }
      if($request['ncv_no'] != ""){
        $query = $query->where('reg_vcn_number', 'LIKE', '%'.$request['ncv_no'].'%');
      }
      if($request['petitioner_name'] != ""){
        $query = $query->where('reg_petitioner', 'LIKE', '%'.$request['petitioner_name'].'%');
      }
      if($request['type'] != ""){
        $query = $query->where('reg_case_type_id', '=', $request['type']);
      }

      if($request['reg_case_subtype_id'] != ""){
        $query = $query->where('reg_case_subtype_id', '=', $request['reg_case_subtype_id']);
      }
      if($request['reg_stage_id'] != ""){
        $query = $query->where('reg_stage_id', 'LIKE', '%'.$request['reg_stage_id'].'%');
      }

      if($request['reg_client_group_id'] != ""){
        $query = $query->where('reg_client_group_id', '=', $request['reg_client_group_id']);
      }

      if($request['reg_client_subgroup_id'] != ""){
        $query = $query->where('reg_client_subgroup_id', '=', $request['reg_client_subgroup_id']);
      }


      if($request['reg_reffeerd_by_id'] != ""){
        $query = $query->where('reg_reffeerd_by_id', '=', $request['reg_reffeerd_by_id']);
      }

      if($request['reg_act_id'] != ""){
        $query = $query->where('reg_act_id', '=', $request['reg_act_id']);
      }

      if($request['reg_section_id'] != ""){

        $section = $request['reg_section_id'];
        $review_count = 0;

          $review_count++; 
              $query = $query->where(function ($query) use($section) { 
                $query->whereRaw("find_in_set(".$section[0].",reg_section_id)");
                if(isset($section[0]))
                {
                  unset($section[0]);
                }
                if(!empty($section)) {
                  foreach ($section as $reg_section_id) {
                   $query->orwhereRaw("find_in_set(".$reg_section_id.",reg_section_id)") ;
                  }
                }
              
              })
              ;
        
      }

      if($request['reg_opp_council'] != ""){
        $query = $query->where('reg_opp_council', 'LIKE', '%'.$request['reg_opp_council'].'%');
      }

      if($request['reg_fir_id'] != ""){
        $query = $query->where('reg_fir_id', 'LIKE', '%'.$request['reg_fir_id'].'%');
      }

      if($request['reg_police_thana'] != ""){
        $query = $query->where('reg_police_thana', 'LIKE', '%'.$request['reg_police_thana'].'%');
      }

      if($request['reg_remark'] != ""){
        $query = $query->where('reg_remark', 'LIKE', '%'.$request['reg_remark'].'%');
      }

      if($request['reg_other_field'] != ""){
        $query = $query->where('reg_other_field', 'LIKE', '%'.$request['reg_other_field'].'%');
      }

      if($request['reg_fir_year'] != ""){
        $query = $query->where('reg_fir_year', '=', $request['reg_fir_year']);
      }

      if($request['stage'] != ""){
        $query = $query->where('stage_name', '=', $request['stage']);
      }
      if($request['reg_power'] != ""){
        $query = $query->where('reg_power', '=', $request['reg_power']);
      }
      if($request['reg_assigend_id'] != ""){
        $query = $query->where('reg_assigend_id', '=', $request['reg_assigend_id']);
      }
      // if($request['client_name'] != ""){
      //   $query = $query->where('cl_group_name', 'LIKE', '%'.$request['client_name'].'%');
      // }
      if($request['respondent'] != ""){
        $query = $query->where('reg_respondent', 'LIKE', '%'.$request['respondent'].'%');
      }

      // if($request['case_year'] != ""){
      //   $query = $query->where('reg_date', 'LIKE', '%'.$request['case_year'].'%');
      // }

      if($request['from_date'] != ""){
        $query = $query->where('reg_date', '>=', date('Y-m-d',strtotime($request['from_date'])) );
      }

      if($request['to_date'] != ""){
        $query = $query->where('reg_date', '<=', date('Y-m-d',strtotime($request['to_date'])) );
      }

      if($request['further_date'] != ""){
        $query = $query->where('pessi_further_date', '=', date('Y-m-d',strtotime($request['further_date'])) );
      }

      if($request['court_type'] != ""){
        if($request['court_type'] != 3){
          $query = $query = $query->where('reg_court', '=', $request['court_type']);
        }
      }

      if($request['reg_court_id'] != ""){
        $query = $query = $query->where('reg_court_id', '=', $request['reg_court_id']);
      }

      if($request['case_status'] == "1"){
      //  $query = $query = $query->where('pessi_choose_type', '=', 0);
          $query = $query->where(function ($query) { $query->where('pessi_choose_type', '=', 0); $query->orwhere('pessi_choose_type', '=', 2); }) ;
      }

      if($request['case_status'] == "2"){
        $query = $query = $query->where('pessi_choose_type', '=', 1);
      }

      if($request['case_status'] == "3"){
        $request['case_status'] == 3;
      }
      

      $app_data =  array('respondent' => $request['respondent'],'client_name' => $request['client_name'],'assign_wise' => $request['assign_wise'],'stage' => $request['stage'],'reffered_by' => $request['reffered_by'],'reg_stage_id' => $request['reg_stage_id'],'type' => $request['type'],'petitioner_name' => $request['petitioner_name'],'ncv_no' => $request['ncv_no'],'case_no' => $request['case_no'],'case_name' => $request['case_name'],'subtype_name' => $request['subtype_name'],'reg_file_no' => $request['reg_file_no'] ,'case_year' => $request['case_year'],'further_date' => $request['further_date'] ,'court_type' => $request['court_type'],'case_status' => $request['case_status'],'reg_court_id' => $request['reg_court_id'],'reg_case_subtype_id' => $request['reg_case_subtype_id'],'reg_power' => $request['reg_power'],'reg_client_group_id' => $request['reg_client_group_id'],'reg_client_subgroup_id' => $request['reg_client_subgroup_id'],'reg_reffeerd_by_id' => $request['reg_reffeerd_by_id'],'reg_assigend_id' => $request['reg_assigend_id'],'reg_act_id' => $request['reg_act_id'],'reg_section_id' => $request['reg_section_id'],'reg_opp_council' => $request['reg_opp_council'],'reg_fir_id' => $request['reg_fir_id'],'reg_fir_year' => $request['reg_fir_year'],'reg_police_thana' => $request['reg_police_thana'],'reg_remark' => $request['reg_remark'],'reg_other_field' => $request['reg_other_field'],'from_date' => $request['from_date'],'to_date' => $request['to_date']);

      if($search_type == "all"){
        $perpage    = $query->count();
        $get_record = $query->where('pessi_status',1)->orderBy('reg_id', 'desc')->paginate($perpage)->appends($app_data);
      } else {
        $get_record = $query->where('pessi_status',1)->orderBy('reg_id', 'desc')->paginate(5)->appends($app_data);
      }

      // print_r($get_record);
      // die;

      $case_type_entry    = Case_Type_Entry::where(['user_admin_id' => session('admin_id') ])->where('case_name','!=',"")->get();
      $stage              = Stage::where(['user_admin_id' => session('admin_id') ])->where('stage_name','!=',"")->get();

      $high_court        = Court::where([ 'court_status' => 1 , 'user_admin_id' => session('admin_id') ])->where('court_name' ,'!=', '')->get();

      $case_sub_type     = Class_Code::where([ 'classcode_status' => 1 , 'user_admin_id' => session('admin_id') ])->where('classcode_name' ,'!=', '')->get();

      $client            = Client::where([ 'cl_status' => 1 , 'user_admin_id' => session('admin_id') ])->where('cl_group_name' ,'!=', '')->get();
      
      $sub_client        = Sub_Client::where([ 'sub_client_status' => 1 , 'user_admin_id' => session('admin_id') ])->where('sub_client_name' ,'!=', '')->get();

      $reffered          = Reffered::where([ 'ref_status' => 1 , 'user_admin_id' => session('admin_id') ])->where('ref_advocate_name' ,'!=', '')->get();

      $assigned          = Assigned::where([ 'assign_status' => 1 , 'user_admin_id' => session('admin_id') ])->where('assign_advocate_name' ,'!=', '')->get();

      $act               = Act::where([ 'act_status' => 1 , 'user_admin_id' => session('admin_id') ])->where('act_name' ,'!=', '')->get();
      
      $section           = Section::where([ 'section_status' => 1 , 'user_admin_id' => session('admin_id') ])->where('section_name' ,'!=', '')->get();  


      $reg_case_type_id     = $request['type'];
      $reg_stage_id         = $request['reg_stage_id'];
      $case_year            = $request['case_year'];
      $court_type           = $request['court_type'];
      $case_status          = $request['case_status'];
      $reg_court_id         = $request['reg_court_id'];
      $reg_case_subtype_id  = $request['reg_case_subtype_id'];
      $selected_value       = $request['selected_value'];
      $reg_power            = $request['reg_power'];
    //  $reg_client_group_id  = $request['reg_client_group_id'];
      $reg_reffeerd_by_id   = $request['reg_reffeerd_by_id'];
      $reg_assigend_id      = $request['reg_assigend_id'];
      $reg_act_id           = $request['reg_act_id'];
      $reg_section_id       = implode(',',$request['reg_section_id']);
      $reg_opp_council      = $request['reg_opp_council'];
      $reg_fir_id           = $request['reg_fir_id'];
      $reg_fir_year         = $request['reg_fir_year'];
      $reg_police_thana     = $request['reg_police_thana'];
      $reg_remark           = $request['reg_remark'];
      $reg_other_field      = $request['reg_other_field'];   


      // print_r($reg_section_id);
      // die;   

      $title = 'View Case';
      $data  = compact('title','admin_details','get_record','case_type_entry','stage','reg_case_type_id','reg_stage_id','case_year','court_type','case_status','high_court','case_sub_type','client','sub_client','reffered','assigned','act','section','reg_court_id','reg_case_subtype_id','selected_value','reg_power','reg_reffeerd_by_id','reg_assigend_id','reg_act_id','reg_section_id','reg_opp_council','reg_fir_id','reg_fir_year','reg_police_thana','reg_remark','reg_other_field');
      return view('advocate_admin/view_case_registration',$data);	
    }

    //insert case registration function

    public function insert_case_registration(Request $request, $reg_id = false){

      $current_year    = date('Y',strtotime($request['date_picker']));
      $total_year_case = Case_Registration::whereYear('reg_date','=',$current_year)->where('user_admin_id','=',session('admin_id'))->count();

      // print_r($total_year_case);
      // die;

      $plus_one = $total_year_case + 1;

      $myDate = date('Y-m-d',strtotime($request['date_picker']));
      $futher = date('Y-m-d',strtotime($request['futher']));

      if($reg_id==''){

        $petitioner = json_encode($request['petitioner_extra']);
        $respondent = json_encode($request['respondent_extra']);
        $tempJson   = json_encode($request['reg_extra_party']);

        $request['section'] = implode(',', $request['section']);


        // print_r($request['section']);
        // die;
      
        $Add = new Case_Registration;  
        $Add->reg_date                    = $myDate;
        $Add->reg_court                   = $request['choose_type'];
        $Add->reg_case_type_id            = $request['type'];
        $Add->reg_case_subtype_id         = $request['sub_type'];
        $Add->reg_case_type_category      = $request['reg_case_type_category'];
        $Add->reg_fir_id                  = $request['fir'];
        $Add->user_admin_id               = session('admin_id');
        $Add->reg_petitioner              = $request['petitioner'];
        $Add->reg_respondent              = $request['respondent'];
        $Add->reg_petitioner_extra        = $petitioner;
        $Add->reg_respondent_extra        = $respondent;
        $Add->reg_file_no                 = $plus_one.'/'.$current_year;
        $Add->reg_extra_party             = $tempJson;
        $Add->reg_stage_id                = $request['reg_stage'];
        $Add->reg_power                   = $request['power'];
        $Add->reg_assigend_id             = $request['assigned'];
        $Add->reg_act_id                  = $request['act'];
        $Add->reg_section_id              = $request['section'];
        $Add->reg_reffeerd_by_id          = $request['reffered'];
        $Add->reg_opp_council             = $request['opposite_council'];
        $Add->reg_client_group_id         = $request['client_group'];
        $Add->reg_client_subgroup_id      = $request['client_sub_group'];
        $Add->reg_case_number             = $request['case_no'];
        $Add->reg_vcn_number              = $request['ncv'];
        $Add->reg_nxt_further_date        = $futher;
        $Add->reg_court_id                = $request['court_id'];
        $Add->reg_power_respondent        = $request['power_respondent'];
        $Add->reg_remark                  = $request['reg_remark'];
        $Add->reg_other_field             = $request['reg_other_field'];
        $Add->reg_fir_year                = $request['reg_fir_year'];
        $Add->reg_police_thana            = $request['reg_police_thana'];
        $Add->reg_status                  = 1;
        $Add->save();
        $id = $Add->id;


        $case_type_entry  = Case_Type_Entry::where([ 'case_id' => $request['type'] ])->first();

        $case_registration =  SmsText::where(['sms_content_type' => 2, 'user_admin_id' => session('admin_id') ])->first();
        // For HC - Case Type  Case No. Title Text
        // For TC - Case Type  Case No. NCV No. Title Text

        if($case_registration->sms_content_before != ""){
          $message = $case_registration->sms_content_before;
        }

        if($case_type_entry->case_name != ""){
          $message = "Case Type : $case_type_entry->case_name";
        }

        if($request['case_no'] != ""){
          $message = "$message Case No: ".$request['case_no'];
        }

        if($request['choose_type'] == "1"){
          $message = "$message NCV No: ".$request['ncv'];
        }       

        $message = "$message Title: ".$request['petitioner'].' V/s '.$request['respondent'];
        
        if($request['reg_sms_text'] != ""){
          $message = "$message Text: ".$request['reg_sms_text'];
        }

        if($case_registration->sms_content_after != ""){
          $message = $message.' '.$case_registration->sms_content_after;
        }


      //  print_r($request->input());


        foreach($request['sms_type'] as $sms_type ) {
          
          if($sms_type == 1){
            if($request['client_group'] != ""){

              $client     = Client::where([ 'cl_id' => $request['client_group'] ])->first();
              $mobile_no  = $client->cl_group_mobile_no;
              $this->send_message->send_message($mobile_no,$message);

            }

            if($request['client_sub_group'] != ""){
              
              $sub_client  = Sub_Client::where([ 'sub_client_id' => $request['client_sub_group'] ])->first();
              $mobile_no   = $sub_client->sub_client_mobile_no;
              $this->send_message->send_message($mobile_no,$message);

            }
          }

          if($sms_type == 2){
            if($request['reffered'] != ""){
              
              $reffered  = Reffered::where([ 'ref_id' => $request['reffered'] ])->first();
              $mobile_no = $reffered->ref_mobile_number;
              $this->send_message->send_message($mobile_no,$message);

            }
          }

          if($sms_type == 3){
            if($request['reg_other_mobile'] != ""){
              
              $mobile_no = $request['reg_other_mobile'];
              $this->send_message->send_message($mobile_no,$message);

            } 
          }

        }

        // add pessi

        $Pessi = new Pessi;  
        $Pessi->pessi_case_reg              = $id;
        $Pessi->pessi_statge_id             = $request['reg_stage'];
        $Pessi->pessi_user_id               = session('admin_id');
        $Pessi->pessi_further_date          = $futher;
        $Pessi->pessi_choose_type           = 0;
        $Pessi->save();

        // $reg_file_no = date('d_m_Y')._.$id;

        // Case_Registration::where('reg_id','=',$id)->update(['reg_file_no'=>$reg_file_no]); 
        
        $files = $request->file('img_reg');

        if($files != ""){

          foreach ($files as $file){

            if($file->getClientOriginalExtension() == "jpeg" || $file->getClientOriginalExtension() == "jpg" || $file->getClientOriginalExtension() == "png"){

              $Image = new Upload_Document;
              $Image->upload_case_id  =  $id;
              $Image->user_admin_id   =  session('admin_id');
              $Image->save();
              $image_id = $Image->id; 

              $destinationPath = 'uploads/case_registration';
              $imageName = 'uploads/case_registration/'.time().$image_id.'.'.$file->getClientOriginalExtension();
              $file->move($destinationPath,$imageName);
              $image=array(
                'upload_images'         => $imageName
              );
              Upload_Document::where(['upload_id'=> $image_id])->update($image);

            }
          }

        }

      // die;
      //  return redirect('advocate-panel/view-case-form');

        return redirect()->back()->with('success', 'Your case is successfully added. Your file number is- '.$plus_one.'/'.$current_year);
    
      } else {

        $petitioner = json_encode($request['petitioner_extra']);
        $respondent = json_encode($request['respondent_extra']);
        $tempJson   = json_encode($request['reg_extra_party']);

        $request['section'] = implode(',', $request['section']);


        $values  = array(
        'reg_date'                         =>  $myDate,
        'reg_court'                        =>  $request['choose_type'],
        'reg_case_type_id'                 =>  $request['type'],
        'reg_case_subtype_id'              =>  $request['sub_type'],
        'reg_fir_id'                       =>  $request['fir'],
        'reg_petitioner'                   =>  $request['petitioner'],
        'reg_respondent'                   =>  $request['respondent'],
        'reg_extra_party'                  =>  $tempJson,
        'reg_petitioner_extra'             =>  $petitioner,
        'reg_respondent_extra'             =>  $respondent,
        'reg_stage_id'                     =>  $request['reg_stage'],
        'reg_power'                        =>  $request['power'],
        'reg_assigend_id'                  =>  $request['assigned'],
        'reg_act_id'                       =>  $request['act'],
        'reg_section_id'                   =>  $request['section'],
        'reg_reffeerd_by_id'               =>  $request['reffered'],
        'reg_opp_council'                  =>  $request['opposite_council'],
        'reg_client_group_id'              =>  $request['client_group'],
        'reg_client_subgroup_id'           =>  $request['client_sub_group'],
        'reg_case_type_category'           =>  $request['reg_case_type_category'],
        'reg_case_number'                  =>  $request['case_no'],
        'reg_vcn_number'                   =>  $request['ncv'],
        'reg_nxt_further_date'             =>  $futher,
        'reg_document'                     =>  $request['address'],
        'reg_court_id'                     =>  $request['court_id'],
        'reg_power_respondent'             =>  $request['power_respondent'],
        'reg_remark'                       =>  $request['reg_remark'],
        'reg_other_field'                  =>  $request['reg_other_field'],
        'reg_fir_year'                     =>  $request['reg_fir_year'],
        'reg_police_thana'                 =>  $request['reg_police_thana'],
        );

      Case_Registration::where('reg_id','=',$reg_id)->update($values);
      $files = $request->file('img_reg');
      
      if($files != ""){
          foreach ($files as $file){
            if($file->getClientOriginalExtension() == "jpeg" || $file->getClientOriginalExtension() == "jpg" || $file->getClientOriginalExtension() == "png"){

              $Image = new Upload_Document;
              $Image->upload_case_id  =  $reg_id;
              $Image->user_admin_id   =  session('admin_id');
              $Image->save();
              $image_id = $Image->id; 
              $destinationPath = 'uploads/case_registration';
              $imageName = 'uploads/case_registration/'.time().$image_id.'.'.$file->getClientOriginalExtension();
              $file->move($destinationPath,$imageName);
              $image=array(
                'upload_images'         => $imageName
              );
              Upload_Document::where(['upload_id'=> $image_id])->update($image);
            }
          }

        }

        $total_year_registration = Case_Registration::where('reg_id','=',$reg_id)->first();

        //return redirect('advocate-panel/view-case-form');

        return redirect()->back()->with('success', 'Your case details have been updated successfully. Your file number is- '.$total_year_registration->reg_file_no);
      }

    }

    // delete document

    public function delete_document($upload_id){
      Upload_Document::where(['upload_id'=> $upload_id])->delete();
    }

    //case registration status

    public function case_registration_status($id,$value){

      $admin_id = session('admin_id');
      $get_record = DB::select('select * from adv_case_reg where sha1(reg_id) = "'.$id.'" and user_admin_id = "'.$admin_id.'" ');

      if(count($get_record) == 0){
        return Redirect()->back();
      }

      Case_Registration::where('reg_id', $get_record[0]->reg_id)->update(['reg_status' => $value , 'reg_disposal_date' => date('Y-m-d') ]);
      return Redirect()->back();
    }


    //view case registration form

    public function case_registration_details(Request $request, $registration_id){

      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if(isset(json_decode($admin_details[0]->special_permission)->case_registration)){

      } else {
        return Redirect()->back();
      }

      $get_case_registration = DB::select('select * from adv_case_reg where sha1(reg_id) = "'.$registration_id.'" and user_admin_id = "'.$admin_details[0]->admin_id.'" ');


      $get_case_details = Case_Registration::leftJoin('adv_pessi', function($join) {$join->on('pessi_case_reg', '=', 'reg_id');})->leftJoin('adv_stage', function($join) {$join->on('stage_id', '=', 'pessi_statge_id');})->leftJoin('adv_court', function($join) {$join->on('court_id', '=', 'reg_court_id');})->leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->leftJoin('adv_class_code', function($join) {$join->on('classcode_id', '=', 'reg_case_subtype_id');})->leftJoin('adv_fir', function($join) {$join->on('fir_id', '=', 'reg_fir_id');})->leftJoin('adv_assigned', function($join) {$join->on('assign_id', '=', 'reg_assigend_id');})->leftJoin('adv_act', function($join) {$join->on('act_id', '=', 'reg_act_id');})->leftJoin('adv_section', function($join) {$join->on('section_id', '=', 'reg_section_id');})->leftJoin('adv_referred', function($join) {$join->on('ref_id', '=', 'reg_reffeerd_by_id');})->leftJoin('adv_client', function($join) {$join->on('cl_id', '=', 'reg_client_group_id');})->leftJoin('adv_sub_client', function($join) {$join->on('sub_client_id', '=', 'reg_client_subgroup_id');})->where(['adv_case_reg.reg_id' => $get_case_registration[0]->reg_id , 'pessi_status' => '1' ])->first();


      $pessi_detail = Pessi::leftJoin('adv_case_reg', function($join) {$join->on('pessi_case_reg', '=', 'reg_id');})->leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->leftJoin('adv_stage', function($join) {$join->on('stage_id', '=', 'pessi_statge_id');})->leftJoin('adv_judge', function($join) {$join->on('judge_id', '=', 'honaurable_justice');})->where('pessi_user_id', $admin_details[0]->admin_id)->where('pessi_case_reg', $get_case_details->reg_id)->orderBy('pessi_id', 'desc')->get(); 


      $compliance_detail = Compliance::leftJoin('adv_case_reg', function($join) {$join->on('compliance_case_id', '=', 'reg_id');})->leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'compliance_case_type');})->where('adv_compliance.user_admin_id','=',session('admin_id'))->where('compliance_case_id','=' ,$get_case_details->reg_id)->orderBy('compliance_id', 'desc')->get();

      // ->->where('adv_case_reg.user_admin_id','=',session('admin_id'))->first();
      
      // echo "<pre>";
      // print_r($pessi_detail);
      // die;
      $client         = Client::where([ 'cl_status' => 1 , 'user_admin_id' => session('admin_id') ])->where('cl_group_name' ,'!=', '')->get();

      $sub_client     = Sub_Client::where([ 'sub_client_status' => 1 , 'user_admin_id' => session('admin_id') ])->where('sub_client_name' ,'!=', '')->get();

      // $referred = Reffered::where([ 'ref_status' => 1 , 'user_admin_id' => session('admin_id') ])->where('ref_advocate_name' ,'!=', '')->get();

      // $assigned = Assigned::where([ 'ref_status' => 1 , 'user_admin_id' => session('admin_id') ])->where('ref_advocate_name' ,'!=', '')->get();

    //  $case_type_entry      = Case_Type_Entry::get();  

    $get_stage           = Stage::where(['user_admin_id' => session('admin_id') , 'stage_status' => 1 ])->where('stage_name','!=',"")->get();   
      
      $title = 'Case Details';
      $data  = compact('title','admin_details','get_case_details','case_type_entry','pessi_detail','compliance_detail','get_stage','client','sub_client');
      return view('advocate_admin/case_registration_detail',$data); 
    }



    public function share_email(Request $request){

      if($request->subject != ""){
        $_REQUEST['subject'] = $request->subject;
      } else {
        $_REQUEST['subject'] = "Case Registration";
      }

      if($request->client_name != ""){
        $_REQUEST['client_name'] = $request->client_name;
      } else {
        $_REQUEST['client_name'] = "Case Registration";
      }
      
      $_REQUEST['client_email'] = $request->client_email;
      $_REQUEST['message'] = $request->message;

      

      $data=['message_share'=> $request->message , 'reg_id'=> $request->reg_id];

      $file = $request->file('document_upload');
      // print_r($file->getPathName());
      // die;

      if($file != ""){
        $destinationPath = 'uploads/share_mail/';
        $imageName = 'uploads/share_mail/'.time().'.'.$file->getClientOriginalExtension();
        $file->move($destinationPath,$imageName);
        $_REQUEST['file'] = $imageName;
      }


      foreach($request['client'] as $client ) {

        $client  = Client::where([ 'cl_id' => $client ])->first();
        
        if($client->cl_group_email_id != ""){
          
          if($client->cl_group_name != ""){ 
            $_REQUEST['client_name'] = $client->cl_group_name;
          } else {
            $_REQUEST['client_name'] = "Case Registration";
          }

          $_REQUEST['client_email'] = $client->cl_group_email_id;

          if($_REQUEST['file'] != ""){
            Mail::send(['html' => 'advocate_admin/share_mail'], $data , function ($message) {
              $message->to($_REQUEST['client_email'], $_REQUEST['client_name'])->subject($_REQUEST['subject'])->attach($_REQUEST['file']);
            });

          } else {
          
            Mail::send(['html' => 'advocate_admin/share_mail'], $data , function ($message) {
              $message->to($_REQUEST['client_email'], $_REQUEST['client_name'])->subject($_REQUEST['subject']);
            });
          }
        }
      }

      foreach($request['sub_client'] as $sub_client ) {
        $sub_client  = Sub_Client::where([ 'sub_client_id' => $sub_client ])->first();
        if($sub_client->sub_client_email_id != ""){
          if($sub_client->sub_client_name != ""){ 
            $_REQUEST['client_name'] = $sub_client->sub_client_name;
          } else {
            $_REQUEST['client_name'] = "Case Registration";
          }

          $_REQUEST['client_email'] = $sub_client->sub_client_email_id;

          if($_REQUEST['file'] != ""){
            Mail::send(['html' => 'advocate_admin/share_mail'], $data , function ($message) {
              $message->to($_REQUEST['client_email'], $_REQUEST['client_name'])->subject($_REQUEST['subject'])->attach($_REQUEST['file']);
            });
          } else {
            Mail::send(['html' => 'advocate_admin/share_mail'], $data , function ($message) {
              $message->to($_REQUEST['client_email'], $_REQUEST['client_name'])->subject($_REQUEST['subject']);
            });
          }
        }
      }


      if($request->assign != ""){

        $assigned = Assigned::where('assign_id' ,'=', $request->assign)->first();
      
        if($assigned->assign_advocate_email != ""){
            
          if($assigned->assign_advocate_name != ""){ 
            $_REQUEST['assign_advocate_name'] = $assigned->assign_advocate_name;
          } else {
            $_REQUEST['assign_advocate_name'] = "Case Registration";
          }

          $_REQUEST['assign_advocate_email'] = $assigned->assign_advocate_email;

          if($_REQUEST['file'] != ""){
            Mail::send(['html' => 'advocate_admin/share_mail'], $data , function ($message) {
              $message->to($_REQUEST['assign_advocate_email'], $_REQUEST['assign_advocate_name'])->subject($_REQUEST['subject'])->attach($_REQUEST['file']);
            });

          } else {
          
            Mail::send(['html' => 'advocate_admin/share_mail'], $data , function ($message) {
              $message->to($_REQUEST['assign_advocate_email'], $_REQUEST['assign_advocate_name'])->subject($_REQUEST['subject']);
            });
          }
        }
      }

      if($request->referred != ""){

        $reffered = Reffered::where('ref_id' ,'=', $request->referred)->first();
        

        if($reffered->ref_advocate_email != ""){
          
          if($reffered->ref_advocate_name != ""){ 
            $_REQUEST['ref_advocate_name'] = $reffered->ref_advocate_name;
          } else {
            $_REQUEST['ref_advocate_name'] = "Case Registration";
          }

          $_REQUEST['ref_advocate_email'] = $reffered->ref_advocate_email;

          if($_REQUEST['file'] != ""){
            Mail::send(['html' => 'advocate_admin/share_mail'], $data , function ($message) {
              $message->to($_REQUEST['ref_advocate_email'], $_REQUEST['ref_advocate_name'])->subject($_REQUEST['subject'])->attach($_REQUEST['file']);
            });

          } else {
          
            Mail::send(['html' => 'advocate_admin/share_mail'], $data , function ($message) {
              $message->to($_REQUEST['ref_advocate_email'], $_REQUEST['ref_advocate_name'])->subject($_REQUEST['subject']);
            });
          }
        }  
      }    

      return redirect()->back()->with('success', 'Your Email has been sent successfully');

    }

    

}
