<?php

namespace App\Http\Controllers\Reply;

use Illuminate\Http\Request;
use App\Model\Reply\Reply; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Case_Registration\Case_Registration; // model name
use App\Model\Admin\Account; // model name
use App\Http\Controllers\Controller;
use DB;
class ReplyController extends Controller
{

    public function __construct()
      {
          $this->middleware('Validation');
      }
      
    //add reply

    public function add_reply($reply_id = false){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();
      $admin_id = session('admin_id');

      if($reply_id!=''){

          $get_record = DB::select('select * from adv_comp_reply where sha1(reply_id) = "'.$reply_id.'" and user_admin_id = "'.$admin_id.'" ');

          if(count($get_record) == 0){
            return Redirect()->back();
          }

          $reply_case_no = Case_Registration::where('reg_status','=',1)->get();      
          $title         = 'Add Reply';
          $data          = compact('title','admin_details','get_record','reply_case_no');
          return view('advocate_admin/add_reply',$data);  
      }else{
          $get_record = "";
      }
      $reply_case_no = Case_Registration::where('reg_status','=',1)->get();      
      $title         = 'Add Reply';
      $data          = compact('title','admin_details','get_record','reply_case_no');
      return view('advocate_admin/add_reply',$data);	
    }


    // reply case no ajax function
    public function reply_case_no_ajax($reply_case_no_id){
      $arrayName = Case_Registration::where('reg_id','=',$reply_case_no_id)->first();
      return response()->json(['petitioner'=>$arrayName['reg_petitioner'],'respondent'=>$arrayName['reg_respondent']]);
    }


    //view reply

    public function view_reply(Request $request){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if($request['check'] != ""){
        Reply::whereIn('reply_id', $request['check'])->delete();
        return Redirect()->back();
      }

      $query = Reply::leftJoin('adv_case_reg', function($join) {$join->on('reg_id', '=', 'reply_case_no');})->where('adv_comp_reply.user_admin_id','=',session('admin_id'));
      if($request['reply_case_no'] != ""){
        $query = $query->where('reply_case_no', 'LIKE', '%'.$request['reply_case_no'].'%');
      }
      if($request['reply_title'] != ""){
        $query = $query->where('reply_title', 'LIKE', '%'.$request['reply_title'].'%');
      }
      if($request['reply_for'] != ""){
        $query = $query->where('reply_desc', 'LIKE', '%'.$request['reply_for'].'%');
      }

      $app_data =  array('reply_case_no' => $request['reply_case_no'],'reply_title' => $request['reply_title'],'reply_for' => $request['reply_for']);
      $get_record = $query->orderBy('reply_id', 'desc')->paginate(10)->appends($app_data);      
      $title = 'View Reply';
      $data  = compact('title','admin_details','get_record');
      return view('advocate_admin/view_reply',$data);	
    }


    //insert reply function

    public function insert_reply(Request $request, $reply_id = false){

      if($reply_id==''){
      $Add = new Reply;  
      $Add->reply_case_no         = $request['case_no'];
      $Add->reply_title           = $request['title'];
      $Add->user_admin_id         = session('admin_id'); 
      $Add->reply_desc            = $request['reply'];
      $Add->reply_status_date     = $request['status_date_s'];
      // if($request['status_date_s'] == 2){
      // $Add->reply_date_pic        = date('Y-m-d',strtotime($request['status_date']));
      // }
      $Add->reply_status          = 1;
      $Add->save();
      return redirect('advocate-panel/view-reply');
    }
    else
    {
      // if($request['status_date_s'] == 2){
      //   $date_status = date('Y-m-d',strtotime($request['status_date']));
      // }else{
      //   $date_status = '0000-00-00';
      // }
      $values  = array(
        'reply_case_no'         =>  $request['case_no'],
        'reply_title'           =>  $request['title'],
        'reply_desc'            =>  $request['reply'],
        'reply_status_date'     =>  $request['status_date_s']
        );
      Reply::where('reply_id','=',$reply_id)->update($values);
      return redirect('advocate-panel/view-reply');
    }

  }

    //reply status

    public function reply_status($id,$value){
      Reply::where('reply_id', $id)->update(['reply_status' => $value]);
      return Redirect()->back();
    }

}
