<?php

namespace App\Http\Controllers\Notice_Issue;

use Illuminate\Http\Request;
use App\Model\Notice_Issue\Notice_Issue; // model name
use App\Model\Case_Registration\Case_Registration; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Admin\Account; // model name
use App\Http\Controllers\Controller;
use DB;
class NoticeissueController extends Controller
{

    public function __construct()
      {
          $this->middleware('Validation');
      }
      
    //add notice issue

    public function add_notice_issue($notice_issue_id = false){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();
      $admin_id = session('admin_id');

      if($notice_issue_id!=''){
          $get_record = DB::select('select * from adv_comp_notice where sha1(notice_id) = "'.$notice_issue_id.'" and user_admin_id = "'.$admin_id.'" ');

          if(count($get_record) == 0){
            return Redirect()->back();
          }

          $notice_case_no = Case_Registration::where('reg_status','=',1)->get();      
          $title          = 'Add Notice Issue';
          $data           = compact('title','admin_details','get_record','notice_case_no','caseno');
          return view('advocate_admin/add_notice_issue',$data);

        }else{
          $get_record = "";
        }
          $notice_case_no = Case_Registration::where('reg_status','=',1)->get();      
          $title          = 'Add Notice Issue';
          $data           = compact('title','admin_details','get_record','notice_case_no','caseno');
          return view('advocate_admin/add_notice_issue',$data);
        }
    
    // notice case no ajax function
    public function notice_issue_case_no_ajax($notice_caseno_id){
      $arrayName = Case_Registration::where('reg_id','=',$notice_caseno_id)->first();
      return response()->json(['petitioner'=>$arrayName['reg_petitioner'],'respondent'=>$arrayName['reg_respondent']]);
    }


    //view notice issue

    public function view_notice_issue(Request $request){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if($request['check'] != ""){
        Notice_Issue::whereIn('notice_id', $request['check'])->delete();
        return Redirect()->back();
      }
      
      $query = Notice_Issue::leftJoin('adv_case_reg', function($join) {$join->on('reg_id', '=', 'notice_case_no');})->where('adv_comp_notice.user_admin_id','=',session('admin_id'));

      if($request['notice_case_no'] != ""){
        $query = $query->where('notice_case_no', 'LIKE', '%'.$request['notice_case_no'].'%');
      }
      if($request['notice_title'] != ""){
        $query = $query->where('notice_title', 'LIKE', '%'.$request['notice_title'].'%');
      }
      if($request['notice_issue_date'] != ""){
        $query = $query->where('notice_issue_date', 'LIKE', '%'.$request['notice_issue_date'].'%');
      }

      $app_data =  array('notice_case_no' => $request['notice_case_no'],'notice_title' => $request['notice_title'],'notice_issue_date' => $request['notice_issue_date']);
      $get_record = $query->orderBy('notice_id', 'desc')->paginate(10)->appends($app_data);      
      $title = 'View Notice Issue';
      $data  = compact('title','admin_details','get_record');
      return view('advocate_admin/view_notice_issue',$data);	
    }


    //insert notice Issue function

    public function insert_notice_issue(Request $request, $notice_issue_id = false){

      $myDate = date('Y-m-d',strtotime($request['status_date_s']));
      if($notice_issue_id==''){
      $Add = new Notice_Issue;  
      $Add->notice_case_no         = $request['case_no'];
      $Add->notice_status_date     = $request['status_date_s'];
      $Add->notice_issue_date      = $myDate;
      $Add->user_admin_id          = session('admin_id');
      $Add->notice_status          = 1;
      $Add->save();
      return redirect('advocate-panel/view-notice-issue');
    }
    else
    {
      $values  = array(
        'notice_case_no'         =>  $request['case_no'],
        'notice_status_date'     =>  $request['status_date_s'],
        'notice_issue_date'      =>  $myDate
        );

      Notice_Issue::where('notice_id','=',$notice_issue_id)->update($values);
      return redirect('advocate-panel/view-notice-issue');
    }

  }

    //court notice issue status

    public function notice_issue_status($id,$value){
      Notice_Issue::where('notice_id', $id)->update(['notice_status' => $value]);
      return Redirect()->back();
    }

}
