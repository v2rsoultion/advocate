<?php

namespace App\Http\Controllers\Act;

use Illuminate\Http\Request;
use App\Model\Act\Act; // model name
use App\Model\Section\Section; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Admin\Account; // model name
use App\Http\Controllers\Controller;
use DB;
class ActController extends Controller
{

    public function __construct()
      {
          $this->middleware('Validation');
      }
      
    //add act

    public function add_act($act_id = false){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if(isset(json_decode($admin_details[0]->special_permission)->master_modules)){

      } else {
        return Redirect()->back();
      }

      $admin_id = session('admin_id');

      if($act_id!=''){

          $get_record = DB::select('select * from adv_act where sha1(act_id) = "'.$act_id.'" and user_admin_id = "'.$admin_id.'" ');

          if(count($get_record) == 0){
            return Redirect()->back();
          }

          $title = 'Add Acts';
          $data  = compact('title','admin_details','get_record');
          return view('advocate_admin/add_act',$data);
      }else{
          $get_record = "";
      }      
      $title = 'Add Acts';
      $data  = compact('title','admin_details','get_record');
      return view('advocate_admin/add_act',$data);	
    }

    //view act

    public function view_act(Request $request,$type=false){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if(isset(json_decode($admin_details[0]->special_permission)->master_modules)){

      } else {
        return Redirect()->back();
      }
      
      $query = Act::where('user_admin_id','=',session('admin_id'));

      if($request['check'] != ""){
        Act::whereIn('act_id', $request['check'])->delete();

        Section::whereIn('section_act_id', $request['check'])->delete();
        return Redirect()->back();
      }

      if($request['act_name'] != ""){
        $query = $query->where('act_name', 'LIKE', '%'.$request['act_name'].'%');
      }
      if($request['act_short_name'] != ""){
        $query = $query->where('act_short_name', 'LIKE', '%'.$request['act_short_name'].'%');
      }
      if($request['act_category'] != ""){
        $query = $query->where('act_category', 'LIKE', '%'.$request['act_category'].'%');
      }

      $app_data =  array('act_name' => $request['act_name'],'act_short_name' => $request['act_short_name'],'act_category' => $request['act_category']);

      if($type == "all"){
        $perpage    = $query->count();
        $get_record = $query->orderBy('act_id', 'desc')->paginate($perpage)->appends($app_data);   
      } else {
        $get_record = $query->orderBy('act_id', 'desc')->paginate(25)->appends($app_data);   
      }

         
      $title = 'View Acts';
      $data  = compact('title','admin_details','get_record');
      return view('advocate_admin/view_act',$data);	
    }

    //insert act function

    public function insert_act(Request $request, $act_id = false){

      if($act_id==''){

        if($request['type_name'] == ""){
          return redirect('advocate-panel/add-act');
        }

        $ActData = new Act;  
        $ActData->act_name            = $request['type_name'];
        $ActData->user_admin_id       = session('admin_id');
        $ActData->act_status          = 1;
        $ActData->save();
        $act_id = $ActData->act_id;
        
        foreach ($request['section_add'] as $key => $value) {
          
          if($value['section_name'] != ""){
            $Add = new Section;  
            $Add->section_act_id          = $act_id;
            $Add->section_name            = $value['section_name'];
            $Add->user_admin_id       = session('admin_id');
            $Add->section_status          = 1;
            $Add->save();
          }
        }

        //return redirect('advocate-panel/view-act');
        return redirect()->back()->with('success', 'Your Data has been added successfully');
    }
    else
    {

      $values  = array(
        'act_name'             =>  $request['type_name'],
        'act_short_name'       =>  $request['short_name'],
        'act_category'         =>  $request['c_name']
        );
      Act::where('act_id','=',$act_id)->update($values);
 
      //  Section::where('section_act_id', $act_id)->delete();

      foreach ($request['section_add'] as $key => $value) {

        // $Add = new Section;  
        // $Add->section_act_id          = $act_id;
        // $Add->section_name            = $value['section_name'];
        // $Add->section_status          = 1;
        // $Add->save();

        if( $value['section_id'] == ''){

          $Add = new Section;  
          $Add->section_act_id          = $act_id;
          $Add->section_name            = $value['section_name'];
          $Add->section_status          = 1;
          $Add->save();
          
          $section_ids[] .= $Add->section_id.',';

        } else {
          $values  = array(
            'section_act_id'          =>  $act_id,
            'section_name'            =>  $value['section_name']
            );
          Section::where('section_id','=',$value['section_id'])->update($values);
          
          $section_ids2[] .= $value['section_id'].',';
        }

      }

      // print_r($section_ids2);
      // die;
      Section::where('section_act_id',$act_id)->whereNotIn('section_id', $section_ids)->whereNotIn('section_id', $section_ids2)->delete();

      //return redirect('advocate-panel/view-act');
      return redirect()->back()->with('success', 'Your Data has been updated successfully');
    }

  }

    //act status
    public function act_status($id,$value){

      $admin_id = session('admin_id');
      $get_record = DB::select('select * from adv_act where sha1(act_id) = "'.$id.'" and user_admin_id = "'.$admin_id.'" ');

      if(count($get_record) == 0){
        return Redirect()->back();
      }

      Act::where('act_id', $get_record[0]->act_id)->update(['act_status' => $value]);
      return Redirect()->back();
    }
}
