<?php
namespace App\Http\Controllers\Pessi_Entry;

use Illuminate\Http\Request;
use App\Model\Case_Registration\Case_Registration; // model name
use App\Model\Case_Type_Entry\Case_Type_Entry; // model name
use App\Model\Case_Sub_Type\Case_Sub_Type; // model name
use App\Model\Assigned\Assigned; // model name
use App\Model\Reffered\Reffered; // model name
use App\Model\Section\Section; // model name
use App\Model\Client\Client; // model name
use App\Model\Admin\Account; // model name
use App\Model\Stage\Stage; // model name
use App\Model\Sub_Client\Sub_Client; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Act\Act; // model name
use App\Model\Fir\Fir; // model name
use App\Model\Judge\Judge; // model name
use App\Model\SmsText\SmsText; // model name
use App\Model\Pessi\Pessi; // model name
use App\Http\Controllers\Controller;
use DB;
use Excel;

class Pessi_EntryController extends Controller
{ 

  public function __construct()
    {
        $this->middleware('Validation');
    //    $this->pessi = new Pessi_EntryController;
        $this->send_message = new \App\Http\Controllers\Admin\AdminController;
    }

    //add peshi entry form

    public function add_pessi($reg_id = false){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();
      
      // $get_file            = Case_Registration::where('user_admin_id','=',$admin_details[0]->admin_id )->get();
      // $get_ncv_detail      = Case_Registration::where('user_admin_id','=',$admin_details[0]->admin_id )->get();
      $get_case_regestered = Case_Registration::where('user_admin_id','=',$admin_details[0]->admin_id )->get();
      $stage               = Stage::where(['user_admin_id' => $admin_details[0]->admin_id , 'stage_status' => 1 ])->get();
        

      $title                = 'Add Peshi / Cause List Entry';
      $data                 = compact('title','edit_id','sub_client','admin_details','get_case_regestered','get_record','fir','case_type_entry','case_sub_type','stage','assigned','reffered','section','client');
      return view('advocate_admin/add_pessi',$data);  
    }


    // get case registration

    public function get_case_registration($reg_id){

      $query = Case_Registration::leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->where('reg_id', $reg_id)->first();


      // print_r($query);
      // die;

      $pessi = Pessi::where('pessi_case_reg', $reg_id)->where('pessi_choose_type' ,0)->orderBy('pessi_id', DESC)->first();

      if($pessi == ""){
        $pessi->pessi_statge_id = $query->reg_stage_id;
      }

      if($pessi->pessi_further_date == ""){
        $pre_date = "";
      } else {
        $pre_date = date('d F Y',strtotime($pessi->pessi_further_date));
      }



      return response()->json(['status'=>true,'reg_respondent'=>$query->reg_respondent, 'reg_petitioner'=>$query->reg_petitioner, 'case_name'=>$query->case_name, 'previous_date'=> $pre_date , 'pessi_statge_id'=> $pessi->pessi_statge_id ,  'reg_file_no'=> $query->reg_id , 'reg_vcn_number'=> $query->reg_id , 'reg_case_no'=> $query->reg_id , 'reg_court_id'=> $query->reg_court_id , 'reg_court'=> $query->reg_court , 'reg_case_type_id'=> $query->reg_case_type_id ]);

    }

    //view peshi form

    public function view_pessi(Request $request , $pessi_count=false, $pessi_date=false, $court_type=false){
      
      if($pessi_count == "all"){
        $search_type = "all";
      } else {
        $pessi_count = $pessi_count;
      }

      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if(isset(json_decode($admin_details[0]->special_permission)->pessi_cause_list)){

      } else {
        return Redirect()->back();
      }
      
      if($request['check'] != ""){
        Pessi::whereIn('pessi_id', $request['check'])->delete();
        return Redirect()->back();
      }      



      $query = Pessi::leftJoin('adv_case_reg', function($join) {$join->on('pessi_case_reg', '=', 'reg_id');})->leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->leftJoin('adv_stage', function($join) {$join->on('stage_id', '=', 'pessi_statge_id');});

      if($search_type == "all"){
        $request['court_type'] = 3;
        $request['case_status'] = 3;
      
      } else {
        if($court_type != ""){
          $request['court_type'] = $court_type;
        } else if($request['court_type'] != ""){
          $request['court_type'] = $request['court_type'];
        } else {
          $request['court_type'] = 2;
        }
        
        if($request['case_status'] == ""){
          $request['case_status'] = 1;
        }
      }

      if($pessi_date != ""){
        $request['further_date'] = $pessi_date;
      }

      if($request['reg_file_no'] != ""){
        $query = $query->where('reg_file_no', 'LIKE', '%'.$request['reg_file_no'].'%');
      }
      if($request['case_no'] != ""){
        $query = $query->where('reg_case_number', 'LIKE', '%'.$request['case_no'].'%');
      }
      if($request['ncv_no'] != ""){
        $query = $query->where('reg_vcn_number', 'LIKE', '%'.$request['ncv_no'].'%');
      }
      if($request['petitioner_name'] != ""){
        $query = $query->where('reg_petitioner', 'LIKE', '%'.$request['petitioner_name'].'%');
      }
      if($request['type'] != ""){
        $query = $query->where('case_id', 'LIKE', '%'.$request['type'].'%');
      }
      if($request['reg_stage_id'] != ""){
        $query = $query->where('pessi_statge_id', 'LIKE', '%'.$request['reg_stage_id'].'%');
      }
      if($request['reffered_by'] != ""){
        $query = $query->where('ref_advocate_name', 'LIKE', '%'.$request['reffered_by'].'%');
      }
      if($request['stage'] != ""){
        $query = $query->where('stage_name', 'LIKE', '%'.$request['stage'].'%');
      }
      if($request['assign_wise'] != ""){
        $query = $query->where('assign_advocate_name', 'LIKE', '%'.$request['assign_wise'].'%');
      }
      if($request['client_name'] != ""){
        $query = $query->where('cl_group_name', 'LIKE', '%'.$request['client_name'].'%');
      }
      if($request['respondent'] != ""){
        $query = $query->where('reg_respondent', 'LIKE', '%'.$request['respondent'].'%');
      }

      if($request['case_year'] != ""){
        $query = $query->where('reg_date', 'LIKE', '%'.$request['case_year'].'%');
      }

      if($request['from_date'] != ""){
        $query = $query->where('reg_date', '>=', date('Y-m-d',strtotime($request['from_date'])) );
      }

      if($request['to_date'] != ""){
        $query = $query->where('reg_date', '<=', date('Y-m-d',strtotime($request['to_date'])) );
      }

      if($request['further_date'] != ""){
        $query = $query->where('pessi_further_date', '=', date('Y-m-d',strtotime($request['further_date'])) );
      }

      if($request['court_type'] != ""){
        if($request['court_type'] != 3){
          $query = $query = $query->where('reg_court', '=', $request['court_type']);
        }
      }

      if($request['case_status'] == "1"){
        $query = $query = $query->where('pessi_choose_type', '=', 0);

        $query = $query->where('pessi_status',1);
      }

      if($request['case_status'] == "2"){
        $query = $query = $query->where('pessi_choose_type', '=', 1);
      }

      if($request['case_status'] == "3"){
        $request['case_status'] == 3;
      }

      $app_data =  array('respondent' => $request['respondent'],'client_name' => $request['client_name'],'assign_wise' => $request['assign_wise'],'stage' => $request['stage'],'reffered_by' => $request['reffered_by'],'reg_stage_id' => $request['reg_stage_id'],'type' => $request['type'],'petitioner_name' => $request['petitioner_name'],'ncv_no' => $request['ncv_no'],'case_no' => $request['case_no'],'case_name' => $request['case_name'],'subtype_name' => $request['subtype_name'],'reg_file_no' => $request['reg_file_no'] ,'case_year' => $request['case_year'],'further_date' => $request['further_date'],'court_type' => $request['court_type'],'case_status' => $request['case_status'],'from_date' => $request['from_date'],'to_date' => $request['to_date'] );

      // print_r($request->input());
      // die;
      

      if($search_type == "all"){
        $perpage    = $query->count();
        $get_record = $query->where('pessi_user_id', $admin_details[0]->admin_id)
        ->where('pessi_status',1)
        ->where('pessi_choose_type','!=',2)->orderBy('reg_id', 'desc')->paginate($perpage)->appends($app_data);
        
        $min_max_get_record = $query->where('pessi_user_id', $admin_details[0]->admin_id)->where('pessi_status',1)->where('pessi_choose_type','!=',2)->orderBy('reg_id', 'desc')->paginate($perpage)->toArray(); 

      } else {
        $get_record = $query->where('pessi_user_id', $admin_details[0]->admin_id)
        // ->where('pessi_status',1)
        ->where('pessi_choose_type','!=',2)->orderBy('reg_id', 'desc')->paginate(25)->appends($app_data); 
        
        $min_max_get_record = $query->where('pessi_user_id', $admin_details[0]->admin_id)->where('pessi_status',1)->where('pessi_choose_type','!=',2)->orderBy('reg_id', 'desc')->paginate(25)->toArray();
      }
      

      $min_temp_array =  min(array_column($min_max_get_record, 'pessi_id'));
      $max_temp_array =  max(array_column($min_max_get_record, 'pessi_id'));


      $get_case_regestered = Case_Registration::where('user_admin_id','=',$admin_details[0]->admin_id )->get();
      $stage               = Stage::where(['user_admin_id' => session('admin_id') ])->where('stage_name','!=',"")->get();
      $get_stage           = Stage::where(['user_admin_id' => session('admin_id') , 'stage_status' => 1 ])->where('stage_name','!=',"")->get();
      $case_type_entry     = Case_Type_Entry::where(['user_admin_id' => session('admin_id') ])->where('case_name','!=',"")->get();    
      $get_judge           = Judge::where(['user_admin_id' => $admin_details[0]->admin_id , 'judge_status' => 1 ])->where('judge_name','!=',"")->get();
      
      // print_r($get_record);

      $reg_case_type_id   = $request['type'];
      $reg_stage_id       = $request['reg_stage_id'];
      $case_year          = $request['case_year'];
      $court_type         = $request['court_type'];
      $case_status        = $request['case_status'];
      // die;

      $title = 'Peshi / Cause List Entry';
      $data  = compact('title','admin_details','get_record','get_case_regestered','stage','case_type_entry','case_no','reg_case_type_id','reg_stage_id','file_no','case_year','get_judge','get_stage','court_type','case_status','min_temp_array','max_temp_array');
      return view('advocate_admin/view_pessi',$data); 
    }



    // get cause list

    public function get_cause_list(Request $request, $pessi_id = false, $currentdate = false){  
      $get_pessi_get = Pessi::where('pessi_id','=', $pessi_id)->first();

      $get_pessi = Pessi::where('pessi_case_reg','=', $get_pessi_get->pessi_case_reg)->where('pessi_further_date','=', date('Y-m-d',strtotime($currentdate)) )->first();

      if($get_pessi == ""){
        return response()->json(['status'=>false ]);
      } else {
        return response()->json(['status'=>true,'pessi_statge_id'=>$get_pessi->pessi_statge_id, 'honaurable_justice'=>$get_pessi->honaurable_justice, 'honarable_justice_db'=>$get_pessi->honarable_justice_db, 'court_number'=> $get_pessi->court_number , 'serial_number'=> $get_pessi->serial_number ,  'page_number'=> $get_pessi->page_number ]);
      }

      // print_r($get_pessi);
      // die;
      //if($get_pessi->)

    }

    //insert case registration function
    public function insert_pessi(Request $request, $pessi_id = false){  

    $get_pessi     = Pessi::where('pessi_id','=', $pessi_id)->first();
    $get_regRecord = Case_Registration::where('reg_id','=', $get_pessi['pessi_case_reg'])->first();
    $Reg_date      = date('Y-m-d',strtotime($get_regRecord['reg_date']));
    

    if($request['further_date'] != ""){
      $next_date  = date('Y-m-d',strtotime($request['further_date']));
      if ($Reg_date <= $next_date) {
        $further_date  = date('Y-m-d',strtotime($request['further_date']));
      }else{
        return redirect()->back()->with('success', 'Next date must be greater than Case Registration date.');
      }
    } else {
      $further_date  = date('Y-m-d',strtotime($get_pessi['pessi_further_date']));
    }

    if(date('Y-m-d',strtotime($get_pessi['pessi_further_date'])) <= date('Y-m-d',strtotime($request['further_date'])) ){
      $new_previous_date  = date('Y-m-d',strtotime($get_pessi['pessi_further_date']));
    } else {
      $new_previous_date  = date('Y-m-d',strtotime($get_pessi['pessi_prev_date']));
    }

    if(date('Y-m-d',strtotime($request['further_date'])) <= date('Y-m-d',strtotime($get_pessi['pessi_prev_date'])) ){
      return redirect()->back()->with('success', 'Next date must be greater than previous date.');
    }

    if(date('Y-m-d',strtotime($request['further_date'])) <= date('Y-m-d',strtotime($get_pessi['pessi_further_date'])) ){
      return redirect()->back()->with('success', 'Please delete the current pessi entry to insert pessi of previous date.');
    }

    // print_r($new_further_date);
    // die;


    $query_pessi = Pessi::leftJoin('adv_case_reg', function($join) {$join->on('pessi_case_reg', '=', 'reg_id');})->leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->where('pessi_status',1)->where('pessi_id','=', $pessi_id)->first(); 


    $case_registration =  SmsText::where(['sms_content_type' => 3, 'user_admin_id' => session('admin_id') ])->first();
        
        // For HC - Case Type  Case No. Title Text
        // For TC - Case Type  Case No. NCV No. Title Text

        if($case_registration->sms_content_before != ""){
          $message = $case_registration->sms_content_before;
        }

        if($query_pessi->case_name != ""){
          $message = "Case Type : $query_pessi->case_name";
        }

        if($query_pessi->reg_case_number != ""){
          $message = "$message Case No: ".$query_pessi->reg_case_number;
        }

        if($query_pessi->reg_court == "1"){
          $message = "$message Case No: ".$query_pessi->reg_vcn_number;
        }

        $message = "$message Title: ".$query_pessi->reg_petitioner.' V/s '.$query_pessi->reg_respondent;
        
        if($request['reg_sms_text'] != ""){
          $message = "$message Text: ".$request['reg_sms_text'];
        }      

        if($case_registration->sms_content_after != ""){
          $message = $message.' '.$case_registration->sms_content_after;
        }

        foreach($request['sms_type'] as $sms_type ) {
          
            if($sms_type == 1){
              if($query_pessi->reg_client_group_id != ""){

          $client     = Client::where([ 'cl_id' => $query_pessi->reg_client_group_id ])->first();
          $mobile_no  = $client->cl_group_mobile_no;
          $this->send_message->send_message($mobile_no,$message);

              }

              if($query_pessi->reg_client_subgroup_id != ""){
                
                $sub_client  = Sub_Client::where([ 'sub_client_id' => $query_pessi->reg_client_subgroup_id ])->first();
                $mobile_no   = $sub_client->sub_client_mobile_no;
                $this->send_message->send_message($mobile_no,$message);

              }
            }

            if($sms_type == 2){
              if($query_pessi->reg_reffeerd_by_id != ""){
                
                $reffered  = Reffered::where([ 'ref_id' => $query_pessi->reg_reffeerd_by_id ])->first();
                $mobile_no = $reffered->ref_mobile_number;
                $this->send_message->send_message($mobile_no,$message);

              }
            }

      if($sms_type == 3){
        if($request['reg_other_mobile'] != ""){
          
          $mobile_no = $request['reg_other_mobile'];
          $this->send_message->send_message($mobile_no,$message);

        } 
      }

        }  

        if($request['choose_type'] != ""){
          if( date('Y-m-d',strtotime($get_pessi['pessi_further_date'])) == $further_date ){

            $update_pessi = array(
              'pessi_case_reg'      => $get_pessi['pessi_case_reg'],
              'pessi_prev_date'     => $get_pessi['pessi_prev_date'],
              'pessi_statge_id'     => $request['reg_stage'],
              'pessi_user_id'       => $get_pessi['pessi_user_id'],
              'pessi_decide_id'     => $request['decide'],
              'pessi_further_date'  => $further_date,
              'pessi_choose_type'   => $request['choose_type'],
              'pessi_order_sheet'   => $request['order_sheet'],
              'serial_number'       => $get_pessi['serial_number'],
              'honaurable_justice'  => $get_pessi['honaurable_justice'],
              'court_number'        => $get_pessi['court_number'],
              'page_number'         => $get_pessi['page_number'],
            );
              Pessi::where('pessi_id','=', $pessi_id)->update($update_pessi);
          } else {
          
            $update_pessi = array(
              'pessi_order_sheet'   => $request['order_sheet'],
            );
            Pessi::where('pessi_id','=', $pessi_id)->update($update_pessi);
        
            $Add = new Pessi;  
            $Add->pessi_case_reg              = $get_pessi['pessi_case_reg'];
            $Add->pessi_prev_date             = $new_previous_date;
            $Add->pessi_statge_id             = $request['reg_stage'];
            $Add->pessi_user_id               = $get_pessi['pessi_user_id'];
            $Add->pessi_decide_id             = $request['decide'];
            $Add->pessi_further_date          = $further_date;
            $Add->pessi_choose_type           = $request['choose_type'];
            //$Add->pessi_order_sheet           = $request['order_sheet'];
            $Add->serial_number               = $get_pessi['serial_number'];
            $Add->honaurable_justice          = $get_pessi['honaurable_justice'];
            $Add->court_number                = $get_pessi['court_number'];
            $Add->page_number                 = $get_pessi['page_number'];
            $Add->save();
            $pessi_id = $Add->id;
          
          }
          Pessi::where('pessi_id','!=', $pessi_id)->where('pessi_case_reg', $get_pessi['pessi_case_reg'])->update(['pessi_status' => 0]);
        }
        return Redirect()->back();
    }


    //edit peshi function

    public function edit_pessi(Request $request, $pessi_id = false){  

      // print_r($pessi_id);
      // die;



      $get_pessi = Pessi::where('pessi_id','=', $pessi_id)->first();

      if($request['further_date'] != ""){
        $further_date  = date('Y-m-d',strtotime($request['further_date']));
      } else {
        $further_date  = date('Y-m-d',strtotime($get_pessi['pessi_further_date']));

        $request['further_date'] = $further_date;
      }


// echo "<pre>";
//         print_r($request->input());
//         die;


      // if(date('Y-m-d',strtotime($request['further_date'])) <= date('Y-m-d',strtotime($get_pessi['pessi_prev_date'])) ){
      //   return redirect()->back()->with('failure', 'Next date must be greater than previous date.');
      // }

      // if(date('Y-m-d',strtotime($request['further_date'])) < date('Y-m-d',strtotime($get_pessi['pessi_further_date'])) ){
      //   return redirect()->back()->with('failure', 'Please delete the current pessi entry to insert pessi of previous date.');
      // }

      $query_pessi = Pessi::leftJoin('adv_case_reg', function($join) {$join->on('pessi_case_reg', '=', 'reg_id');})->leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->where('pessi_status',1)->where('pessi_id','=', $pessi_id)->first(); 


      $case_registration =  SmsText::where(['sms_content_type' => 3, 'user_admin_id' => session('admin_id') ])->first();
        
      // For HC - Case Type  Case No. Title Text
      // For TC - Case Type  Case No. NCV No. Title Text

      if($case_registration->sms_content_before != ""){
        $message = $case_registration->sms_content_before;
      }

      if($query_pessi->case_name != ""){
        $message = "Case Type : $query_pessi->case_name";
      }

      if($query_pessi->reg_case_number != ""){
        $message = "$message Case No: ".$query_pessi->reg_case_number;
      }

      if($query_pessi->reg_court == "1"){
        $message = "$message Case No: ".$query_pessi->reg_vcn_number;
      }

      $message = "$message Title: ".$query_pessi->reg_petitioner.' V/s '.$query_pessi->reg_respondent;
      
      if($request['reg_sms_text'] != ""){
        $message = "$message Text: ".$request['reg_sms_text'];
      }      

      if($case_registration->sms_content_after != ""){
        $message = $message.' '.$case_registration->sms_content_after;
      }
        foreach($request['sms_type'] as $sms_type ) {
          
          if($sms_type == 1){
            if($query_pessi->reg_client_group_id != ""){

              $client     = Client::where([ 'cl_id' => $query_pessi->reg_client_group_id ])->first();
              $mobile_no  = $client->cl_group_mobile_no;
              $this->send_message->send_message($mobile_no,$message);

            }

            if($query_pessi->reg_client_subgroup_id != ""){
              
              $sub_client  = Sub_Client::where([ 'sub_client_id' => $query_pessi->reg_client_subgroup_id ])->first();
              $mobile_no   = $sub_client->sub_client_mobile_no;
              $this->send_message->send_message($mobile_no,$message);

            }
          }

          if($sms_type == 2){
            if($query_pessi->reg_reffeerd_by_id != ""){
              
              $reffered  = Reffered::where([ 'ref_id' => $query_pessi->reg_reffeerd_by_id ])->first();
              $mobile_no = $reffered->ref_mobile_number;
              $this->send_message->send_message($mobile_no,$message);

            }
          }

          if($sms_type == 3){
            if($request['reg_other_mobile'] != ""){
              
              $mobile_no = $request['reg_other_mobile'];
              $this->send_message->send_message($mobile_no,$message);

            } 
          }

        }

        // echo "<pre>";
        // print_r($request->input());
        // die;

        if(date('Y-m-d',strtotime($request['further_date'])) == date('Y-m-d',strtotime($get_pessi['pessi_further_date'])) ){

          $update_pessi = array(
            'pessi_case_reg'      => $get_pessi['pessi_case_reg'],
            'pessi_prev_date'     => $get_pessi['pessi_further_date'],
            'pessi_statge_id'     => $request['reg_stage'],
            'pessi_user_id'       => $get_pessi['pessi_user_id'],
            
              'pessi_decide_id'     => $request['decide'],  
            
            
            'pessi_further_date'  => $further_date,
            'pessi_choose_type'   => $request['choose_type'],
            'pessi_order_sheet'   => $request['order_sheet'],
            'serial_number'       => $get_pessi['serial_number'],
            'honaurable_justice'  => $get_pessi['honaurable_justice'],
            'court_number'        => $get_pessi['court_number'],
            'page_number'         => $get_pessi['page_number'],
          //  'pessi_status'        => 1,
          );

          Pessi::where('pessi_id','=', $pessi_id)->update($update_pessi);
        
        } else {


          if(date('Y-m-d',strtotime($request['further_date'])) <= date('Y-m-d',strtotime($get_pessi['pessi_prev_date'])) ){
            return redirect()->back()->with('failure', 'Next date must be greater than previous date.');
          }

          if(date('Y-m-d',strtotime($request['further_date'])) < date('Y-m-d',strtotime($get_pessi['pessi_further_date'])) ){
            return redirect()->back()->with('failure', 'Please delete the current pessi entry to insert pessi of previous date.');
          }


          $update_pessi = array(
            'pessi_order_sheet'   => $request['order_sheet'],
          );
          Pessi::where('pessi_id','=', $pessi_id)->update($update_pessi);

          $Add = new Pessi;  
          $Add->pessi_case_reg              = $get_pessi['pessi_case_reg'];
          $Add->pessi_prev_date             = $get_pessi['pessi_further_date'];
          $Add->pessi_statge_id             = $request['reg_stage'];
          $Add->pessi_user_id               = $get_pessi['pessi_user_id'];
          $Add->pessi_decide_id             = $request['decide'];
          $Add->pessi_further_date          = date('Y-m-d',strtotime($request['further_date']));
          $Add->pessi_choose_type           = $request['choose_type'];
        //  $Add->pessi_order_sheet           = $request['order_sheet'];
          $Add->serial_number               = $get_pessi['serial_number'];
          $Add->honaurable_justice          = $get_pessi['honaurable_justice'];
          $Add->court_number                = $get_pessi['court_number'];
          $Add->page_number                 = $get_pessi['page_number'];
          $Add->save();
          $pessi_id = $Add->id;

          Pessi::where('pessi_id','!=', $pessi_id)->where('pessi_case_reg', $get_pessi['pessi_case_reg'])->update(['pessi_status' => 0]);
        }


      //  Pessi::where('pessi_id','>', $pessi_id)->delete();


        return Redirect()->back();

    }



    //delete pessi function

    public function delete_pessi(Request $request, $pessi_id = false){  

      $get_record = DB::select('select * from adv_pessi where sha1(pessi_id) = "'.$pessi_id.'" ');

      if($get_record[0]->pessi_status == 1){

        $get_pessi = Pessi::where('pessi_id','<', $get_record[0]->pessi_id)->where('pessi_case_reg', $get_record[0]->pessi_case_reg)->orderBy('pessi_id', DESC)->first();

        $update_pessi = array( 'pessi_status' => 1 );
        Pessi::where('pessi_id','=', $get_pessi->pessi_id)->update($update_pessi);

        Pessi::where('pessi_id', $get_record[0]->pessi_id)->delete();

      } else {

        Pessi::where('pessi_id', $get_record[0]->pessi_id)->delete();

      }

        return Redirect()->back();

    }


    //delete pessi function

    public function delete_pessi_new(Request $request){  

      $pessi_id = $request->pessi_id;

      $get_record = DB::select('select * from adv_pessi where sha1(pessi_id) = "'.$pessi_id.'" ');

      if($get_record[0]->pessi_status == 1){

        $get_pessi = Pessi::where('pessi_id','<', $get_record[0]->pessi_id)->where('pessi_case_reg', $get_record[0]->pessi_case_reg)->orderBy('pessi_id', DESC)->first();

        $update_pessi = array( 'pessi_status' => 1 );
        Pessi::where('pessi_id','=', $get_pessi->pessi_id)->update($update_pessi);

        Pessi::where('pessi_id', $get_record[0]->pessi_id)->delete();

      } else {

        $old_pessi = Pessi::where('pessi_id','<', $get_record[0]->pessi_id)->where('pessi_case_reg', $get_record[0]->pessi_case_reg)->orderBy('pessi_id', DESC)->first();

        $new_pessi = Pessi::where('pessi_id','>', $get_record[0]->pessi_id)->where('pessi_case_reg', $get_record[0]->pessi_case_reg)->orderBy('pessi_id', DESC)->first();
        
        $update_pessi = array( 'pessi_prev_date' => $old_pessi->pessi_further_date );
        Pessi::where('pessi_id','=', $new_pessi->pessi_id)->update($update_pessi);

        Pessi::where('pessi_id', $get_record[0]->pessi_id)->delete();

      }

      //  return true;

    }




    public function insert_cause_list(Request $request, $pessi_id = false){     


      $get_pessi = Pessi::where('pessi_id','=', $pessi_id)->first();

      // print_r($get_pessi);
      // print_r($request->input());
      // die;


      if($request['further_date'] != ""){
        $further_date  = date('Y-m-d',strtotime($request['further_date']));
      } else {
        $further_date  = date('Y-m-d',strtotime($get_pessi['pessi_further_date']));
      }

      if(date('Y-m-d',strtotime($request['further_date'])) <= date('Y-m-d',strtotime($get_pessi['pessi_prev_date'])) ){

        $get_pessi_new = Pessi::where('pessi_case_reg','=', $get_pessi['pessi_case_reg'])->where('pessi_user_id','=', $get_pessi['pessi_user_id'])->where('pessi_further_date','=', date('Y-m-d',strtotime($request['further_date'])))->first();


        if($get_pessi_new != ""){

          $cause_list = array(
            'pessi_statge_id'      => $request->reg_stage,
            'honaurable_justice'   => $request->honarable_justice, 
            'honarable_justice_db' => $request->honarable_justice_db,
            'court_number'         => $request->court_no, 
            'serial_number'        => $request->serial_no, 
            'page_number'          => $request->page_no,
            'cause_status'         => 1, 
          );

          Pessi::where('pessi_id','=', $get_pessi_new['pessi_id'])->update($cause_list);
          return redirect()->back()->with('danger', 'Your Previous date pessi has been updated.');  

        } else {
          return redirect()->back()->with('success', 'There is no previous pessi available for that date.');  
        }

        
      }


      if(date('Y-m-d',strtotime($request['further_date'])) > date('Y-m-d',strtotime($get_pessi['pessi_further_date'])) ){
        return redirect()->back()->with('success', 'you cannot select a date greater than the Next date. If you want to do so, please create a new entry.');
      }



      // echo "<pre>";
      // print_r($get_pessi);

      // print_r($request['further_date']);
      // echo "</pre>";die;

      // if($request->honarable_justice == "" && $request->court_no == "" && $request->serial_no == "" && $request->page_no == ""){
      //   return Redirect()->back();
      // }

        $cause_list = array(
          'pessi_statge_id'      => $request->reg_stage,
          'honaurable_justice'   => $request->honarable_justice, 
          'honarable_justice_db' => $request->honarable_justice_db,
          'court_number'         => $request->court_no, 
          'serial_number'        => $request->serial_no, 
          'page_number'          => $request->page_no,
          'pessi_further_date'   => date('Y-m-d',strtotime($request['further_date'])),
          'cause_status'         => 1, 
        );

        Pessi::where('pessi_id','=', $pessi_id)->update($cause_list);
        return redirect()->back()->with('danger', 'Your Cause List has been updated successfully.');  

    }


    //case registration status

    public function case_registration_status($id,$value){
      Case_Registration::where('reg_id', $id)->update(['reg_status' => $value]);
      
    }


    //view due course

    public function due_cause(Request $request , $search_type = false){
      
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();
      
      if(isset(json_decode($admin_details[0]->special_permission)->due_course)){

      } else {
        return Redirect()->back();
      }

      if($request['check'] != ""){
        Pessi::whereIn('pessi_id', $request['check'])->delete();
        return Redirect()->back();
      }      

      $query = Pessi::leftJoin('adv_case_reg', function($join) {$join->on('pessi_case_reg', '=', 'reg_id');})->leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->leftJoin('adv_stage', function($join) {$join->on('stage_id', '=', 'pessi_statge_id');});

      if($search_type == "all"){
        $request['court_type'] = 3;
      
      } else {
       
        if($court_type != ""){
          $request['court_type'] = $court_type;
        } else if($request['court_type'] != ""){
          $request['court_type'] = $request['court_type'];
        } else {
          $request['court_type'] = 2;
        }
      }


      if($request['reg_file_no'] != ""){
        $query = $query->where('reg_file_no', 'LIKE', '%'.$request['reg_file_no'].'%');
      }
      if($request['case_no'] != ""){
        $query = $query->where('reg_case_number', 'LIKE', '%'.$request['case_no'].'%');
      }
      if($request['ncv_no'] != ""){
        $query = $query->where('reg_vcn_number', 'LIKE', '%'.$request['ncv_no'].'%');
      }
      if($request['petitioner_name'] != ""){
        $query = $query->where('reg_petitioner', 'LIKE', '%'.$request['petitioner_name'].'%');
      }
      if($request['type'] != ""){
        $query = $query->where('case_id', 'LIKE', '%'.$request['type'].'%');
      }
      if($request['reg_stage_id'] != ""){
        $query = $query->where('pessi_statge_id', 'LIKE', '%'.$request['reg_stage_id'].'%');
      }
      if($request['reffered_by'] != ""){
        $query = $query->where('ref_advocate_name', 'LIKE', '%'.$request['reffered_by'].'%');
      }
      if($request['stage'] != ""){
        $query = $query->where('stage_name', 'LIKE', '%'.$request['stage'].'%');
      }
      if($request['assign_wise'] != ""){
        $query = $query->where('assign_advocate_name', 'LIKE', '%'.$request['assign_wise'].'%');
      }
      if($request['client_name'] != ""){
        $query = $query->where('cl_group_name', 'LIKE', '%'.$request['client_name'].'%');
      }
      if($request['respondent'] != ""){
        $query = $query->where('reg_respondent', 'LIKE', '%'.$request['respondent'].'%');
      }

      if($request['case_year'] != ""){
        $query = $query->where('reg_date', 'LIKE', '%'.$request['case_year'].'%');
      }

      if($request['from_date'] != ""){
        $query = $query->where('reg_date', '>=', date('Y-m-d',strtotime($request['from_date'])) );
      }

      if($request['to_date'] != ""){
        $query = $query->where('reg_date', '<=', date('Y-m-d',strtotime($request['to_date'])) );
      }

      if($request['further_date'] != ""){
        $query = $query->where('reg_nxt_further_date', '=', date('Y-m-d',strtotime($request['further_date'])) );
      }

      if($request['court_type'] != ""){
        if($request['court_type'] != 3){
          $query = $query = $query->where('reg_court', '=', $request['court_type']);
        }
      }


      $app_data =  array('respondent' => $request['respondent'],'client_name' => $request['client_name'],'assign_wise' => $request['assign_wise'],'stage' => $request['stage'],'reffered_by' => $request['reffered_by'],'reg_stage_id' => $request['reg_stage_id'],'type' => $request['type'],'petitioner_name' => $request['petitioner_name'],'ncv_no' => $request['ncv_no'],'case_no' => $request['case_no'],'case_name' => $request['case_name'],'subtype_name' => $request['subtype_name'],'reg_file_no' => $request['reg_file_no'] ,'case_year' => $request['case_year'],'further_date' => $request['further_date'],'court_type' => $request['court_type'],'from_date' => $request['from_date'],'to_date' => $request['to_date'] );

      if($search_type == "all"){
        $perpage    = $query->count();
        $get_record = $query->where('pessi_user_id', $admin_details[0]->admin_id)
        // ->where('pessi_status',1)
        ->where('pessi_choose_type','=',2)->orderBy('pessi_id', 'desc')->paginate($perpage)->appends($app_data); 
      } else {
        $get_record = $query->where('pessi_user_id', $admin_details[0]->admin_id)
        // ->where('pessi_status',1)
        ->where('pessi_choose_type','=',2)->orderBy('pessi_id', 'desc')->paginate(25)->appends($app_data); 
      }


      $get_case_regestered = Case_Registration::where('user_admin_id','=',$admin_details[0]->admin_id )->get();
      $stage               = Stage::where(['user_admin_id' => session('admin_id') ])->where('stage_name','!=',"")->get();
      $case_type_entry     = Case_Type_Entry::where(['user_admin_id' => session('admin_id') ])->where('case_name','!=',"")->get();    
      $get_judge           = Judge::where(['user_admin_id' => $admin_details[0]->admin_id , 'judge_status' => 1 ])->where('judge_name','!=',"")->get();
      
      // print_r($get_record);
      // die;
      
      $reg_case_type_id   = $request['type'];
      $reg_stage_id       = $request['reg_stage_id'];
      $case_year          = $request['case_year'];
      $court_type         = $request['court_type'];
      // die;

      $title = 'Due Course';
      $data  = compact('title','admin_details','get_record','get_case_regestered','stage','case_type_entry','case_no','reg_case_type_id','reg_stage_id','file_no','case_year','get_judge','court_type');
      return view('advocate_admin/due_course',$data); 
    }


    //view due course


    public function undated_case(Request $request, $search_type=false){
      
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();
      
      if(isset(json_decode($admin_details[0]->special_permission)->undated_case)){

      } else {
        return Redirect()->back();
      }
      // if($request['check'] != ""){
      //   Pessi::whereIn('pessi_id', $request['check'])->delete();
      //   return Redirect()->back();
      // }      

      $query = Pessi::leftJoin('adv_case_reg', function($join) {$join->on('pessi_case_reg', '=', 'reg_id');})->leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->leftJoin('adv_stage', function($join) {$join->on('stage_id', '=', 'pessi_statge_id');});

      if($search_type == "all"){
        $request['court_type'] = 3;
      
      } else {
        if($request['court_type'] != ""){
          $request['court_type'] = $request['court_type'];
        } else {
          $request['court_type'] = 2;
        }
      }

      // print_r($request->input());
      // die;

      if($request['reg_file_no'] != ""){
        $query = $query->where('reg_file_no', 'LIKE', '%'.$request['reg_file_no'].'%');
      }
      if($request['case_no'] != ""){
        $query = $query->where('reg_case_number', 'LIKE', '%'.$request['case_no'].'%');
      }
      if($request['ncv_no'] != ""){
        $query = $query->where('reg_vcn_number', 'LIKE', '%'.$request['ncv_no'].'%');
      }
      if($request['petitioner_name'] != ""){
        $query = $query->where('reg_petitioner', 'LIKE', '%'.$request['petitioner_name'].'%');
      }
      if($request['type'] != ""){
        $query = $query->where('case_id', 'LIKE', '%'.$request['type'].'%');
      }
      if($request['reg_stage_id'] != ""){
        $query = $query->where('pessi_statge_id', 'LIKE', '%'.$request['reg_stage_id'].'%');
      }
      if($request['reffered_by'] != ""){
        $query = $query->where('ref_advocate_name', 'LIKE', '%'.$request['reffered_by'].'%');
      }
      if($request['stage'] != ""){
        $query = $query->where('stage_name', 'LIKE', '%'.$request['stage'].'%');
      }
      if($request['assign_wise'] != ""){
        $query = $query->where('assign_advocate_name', 'LIKE', '%'.$request['assign_wise'].'%');
      }
      if($request['client_name'] != ""){
        $query = $query->where('cl_group_name', 'LIKE', '%'.$request['client_name'].'%');
      }
      if($request['respondent'] != ""){
        $query = $query->where('reg_respondent', 'LIKE', '%'.$request['respondent'].'%');
      }

      if($request['case_year'] != ""){
        $query = $query->where('reg_date', 'LIKE', '%'.$request['case_year'].'%');
      }

      if($request['from_date'] != ""){
        $query = $query->where('reg_date', '>=', date('Y-m-d',strtotime($request['from_date'])) );
      }

      if($request['to_date'] != ""){
        $query = $query->where('reg_date', '<=', date('Y-m-d',strtotime($request['to_date'])) );
      }

      if($request['further_date'] != ""){
        $query = $query->where('reg_nxt_further_date', '=', date('Y-m-d',strtotime($request['further_date'])) );
      }

      if($request['from'] != ""){
        $query = $query->where('reg_date', '<=', date('Y-m-d',strtotime($request['from'])) );
      }

      if($request['to'] != ""){
        if($request['from'] != ""){
          $query = $query->orwhere('reg_date', '>', date('Y-m-d',strtotime($request['to'])) );
        } else {
          $query = $query->where('reg_date', '>', date('Y-m-d',strtotime($request['to'])) );
        }
      }


      if($request['court_type'] != ""){
        if($request['court_type'] != 3){
          $query = $query = $query->where('reg_court', '=', $request['court_type']);
        }
      }


      $app_data =  array('respondent' => $request['respondent'],'client_name' => $request['client_name'],'assign_wise' => $request['assign_wise'],'stage' => $request['stage'],'reffered_by' => $request['reffered_by'],'reg_stage_id' => $request['reg_stage_id'],'type' => $request['type'],'petitioner_name' => $request['petitioner_name'],'ncv_no' => $request['ncv_no'],'case_no' => $request['case_no'],'case_name' => $request['case_name'],'subtype_name' => $request['subtype_name'],'reg_file_no' => $request['reg_file_no'] ,'case_year' => $request['case_year'],'further_date' => $request['further_date'],'court_type' => $request['court_type'],'from_date' => $request['from_date'],'to_date' => $request['to_date'],'from' => $request['from'] );

      if($search_type == "all"){
        $perpage    = $query->count();
        $get_record = $query->where('pessi_user_id', $admin_details[0]->admin_id)->where('pessi_status',1)->where('pessi_choose_type','!=',2)->where('pessi_further_date','<=',date('Y-m-d'))->orderBy('pessi_id', 'desc')->groupBy('reg_id')->paginate($perpage)->appends($app_data);
      } else {
        $get_record = $query->where('pessi_user_id', $admin_details[0]->admin_id)->where('pessi_status',1)->where('pessi_choose_type','!=',2)->where('pessi_further_date','<=',date('Y-m-d'))->orderBy('pessi_id', 'desc')->groupBy('reg_id')->paginate(25)->appends($app_data);
      }

       

      //       $get_record = $query->where('pessi_user_id', $admin_details[0]->admin_id)->where(['pessi_status' => 1])->where('pessi_further_date','>=',date('Y-m-d'))->orderByDesc('created_at', 'pessi_case_reg')->paginate(10)->appends($app_data); 


      $get_case_regestered = Case_Registration::where('user_admin_id','=',$admin_details[0]->admin_id )->get();
      $stage               = Stage::where(['user_admin_id' => session('admin_id') ])->where('stage_name','!=',"")->get();
      $case_type_entry     = Case_Type_Entry::where(['user_admin_id' => session('admin_id') ])->where('case_name','!=',"")->get();    
      $get_judge           = Judge::where(['user_admin_id' => $admin_details[0]->admin_id , 'judge_status' => 1 ])->where('judge_name','!=',"")->get();
      
      // print_r($get_record);

      $reg_case_type_id   = $request['type'];
      $reg_stage_id       = $request['reg_stage_id'];
      $case_year          = $request['case_year'];
      $case_no            = $request['case_no'];
      $respondent         = $request['respondent'];
      $petitioner_name    = $request['petitioner_name'];
      $ncv_no             = $request['ncv_no'];
      $reg_file_no        = $request['reg_file_no'];
      $from_date          = $request['from_date'];
      $to_date            = $request['to_date'];
      $further_date       = $request['further_date'];
      $court_type         = $request['court_type'];
      $from               = $request['from'];
      $to                 = $request['to'];
      // die;

      $title = 'Undated Case';
      $data  = compact('title','admin_details','get_record','get_case_regestered','stage','case_type_entry','case_no','reg_case_type_id','reg_stage_id','file_no','case_year','get_judge','case_no','petitioner_name','respondent','ncv_no','reg_file_no','from_date','to_date','further_date','court_type','from','to');
      
      return view('advocate_admin/undated_case',$data); 
    }

    // download undated case

    public function download_undated_case(Request $request){

      // print_r($request->input());
      // die;

      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      $query = Pessi::leftJoin('adv_case_reg', function($join) {$join->on('pessi_case_reg', '=', 'reg_id');})->leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->leftJoin('adv_stage', function($join) {$join->on('stage_id', '=', 'pessi_statge_id');});

       if($court_type != ""){
        $request['court_type'] = $court_type;
      } else if($request['court_type'] != ""){
        $request['court_type'] = $request['court_type'];
      } else {
        $request['court_type'] = 2;
      }

      if($request['file_no'] != ""){
        $query = $query->where('reg_file_no', 'LIKE', '%'.$request['file_no'].'%');
      }
      if($request['case_no'] != ""){
        $query = $query->where('reg_case_number', 'LIKE', '%'.$request['case_no'].'%');
      }
      if($request['ncv_no'] != ""){
        $query = $query->where('reg_vcn_number', 'LIKE', '%'.$request['ncv_no'].'%');
      }
      if($request['petitioner_name'] != ""){
        $query = $query->where('reg_petitioner', 'LIKE', '%'.$request['petitioner_name'].'%');
      }
      if($request['reg_case_type_id'] != ""){
        $query = $query->where('reg_case_type_id', 'LIKE', '%'.$request['reg_case_type_id'].'%');
      }
      if($request['stage_id'] != ""){
        $query = $query->where('reg_stage_id', 'LIKE', '%'.$request['stage_id'].'%');
      }
      if($request['client_name'] != ""){
        $query = $query->where('cl_group_name', 'LIKE', '%'.$request['client_name'].'%');
      }
      if($request['respondent'] != ""){
        $query = $query->where('reg_respondent', 'LIKE', '%'.$request['respondent'].'%');
      }
      if($request['case_year'] != ""){
        $query = $query->where('reg_date', 'LIKE', '%'.$request['case_year'].'%');
      }

      if($request['from_date'] != ""){
        $query = $query->where('reg_date', '>=', date('Y-m-d',strtotime($request['from_date'])) );
      }

      if($request['to_date'] != ""){
        $query = $query->where('reg_date', '<=', date('Y-m-d',strtotime($request['to_date'])) );
      }

      if($request['further_date'] != ""){
        $query = $query->where('reg_nxt_further_date', '=', date('Y-m-d',strtotime($request['further_date'])) );
      }

      if($request['court_type'] != ""){
        if($request['court_type'] != 3){
          $query = $query = $query->where('reg_court', '=', $request['court_type']);
        }
      }

      if($request['from'] != ""){
        $query = $query->where('reg_date', '<=', date('Y-m-d',strtotime($request['from'])) );
      }

      if($request['to'] != ""){
        if($request['from'] != ""){
          $query = $query->orwhere('reg_date', '>', date('Y-m-d',strtotime($request['to'])) );
        } else {
          $query = $query->where('reg_date', '>', date('Y-m-d',strtotime($request['to'])) );
        }
      }

      if($request['from'] == "" && $request['to'] == "" && $request['from_date'] == "" && $request['to_date'] == "" ){
        $date = "All Records";
      } else if($request['from'] != "" && $request['to'] != "" ){
        $date = "From:- ".$request['from']. " To:- ".$request['to'];
      } else if($request['from_date'] != "" && $request['to_date'] != "" ){
        $date = "From:- ".$request['from_date']. " To:- ".$request['to_date'];
      }


      $data = $query->where('pessi_user_id', $admin_details[0]->admin_id)->where('pessi_status',1)->where('pessi_further_date','<=',date('Y-m-d'))->orderBy('pessi_id', 'desc')->groupBy('reg_id')->get()->toArray();

      if($request['court_type'] == 1){
        $court_name = "Trail Court";
      } else {
        $court_name = "High Court";
      }

      Excel::create('Undated Case', function($excel) use ($data,$admin_details,$date,$court_name) {
        $excel->sheet('Sheet', function ($sheet) use ($data,$admin_details,$date,$court_name) {

          $sheet->setOrientation('landscape');
          $sheet->mergeCells('A1:G1');
          $sheet->mergeCells('A2:G2');
          $sheet->mergeCells('A3:G3');
          $sheet->mergeCells('A4:G4');
          $sheet->mergeCells('A5:G5');
          $sheet->mergeCells('A6:C6');
          $sheet->mergeCells('D6:E6');
          //$sheet->mergeCells('6:G6');
          $sheet->setHeight(array(
            1     =>  20,
            2     =>  20,
          ));
          $sheet->setSize('A8', 10, 18);
          $sheet->setSize('B8', 15, 18);
          $sheet->setSize('C8', 15, 18);
          $sheet->setSize('D8', 20, 18);
          $sheet->setSize('E8', 20, 18);
          $sheet->setSize('F8', 35, 18);
          $sheet->setSize('G8', 15, 18);
          $sheet->setSize('H8', 20, 18);
          $sheet->setSize('I8', 20, 18);
          $sheet->row(1, array( 'OFFICE OF' ));
          $sheet->row(1, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          $sheet->row(2, array( $admin_details[0]->admin_firmname ));
          $sheet->row(2, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          $sheet->row(3, array( $admin_details[0]->admin_advocates ));
          $sheet->row(3, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          $sheet->row(4, array( '(Advocates)' ));
          $sheet->row(4, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          $day  = date('l');
        //  $date = date('d/m/Y');
          $sheet->row(6, array( 'Date : '.$date, '', '','                 Undated Case','','' , $court_name));
          $sheet->row(6, function($row) { $row->setAlignment('center');} );
          $sheet->mergeCells('A7:G7');
          
          $sheet->row(8, array( 'S.No.', 'Case No.', 'File No.', 'Court Type' , 'Type of Case', 'Title of Case','Stage','Previous Date','Further Date'));
          $sheet->row(8, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
          $sheet->row(9, array('', '', '', ''));
          $sheet->mergeCells('A9:G9');
          // $sheet->setAutoSize(true);
           
          $i=10;

          if(empty($data) ){

              $sheet->mergeCells('A10:G10');
              $sheet->mergeCells('A11:G11');
              $sheet->mergeCells('A12:G12');
              $sheet->row(11, array('No Record Found'));
              $sheet->row(11, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
            } else {

           foreach ($data as $value) {

            if($value['reg_court'] == 1){
              $court_type = "Trail Court";
            } else {
              $court_type = "High Court";
            }

            $title_case = $value['reg_petitioner'].' v/s '.$value['reg_respondent'];

            if($value['pessi_prev_date'] == "1970-01-01" || $value['pessi_prev_date'] == ""){
              $previous_date = "-----";
            } else {
              $previous_date = date('d F Y',strtotime($value['pessi_prev_date']));
            }

            if($value['pessi_further_date'] == "1970-01-01" || $value['pessi_further_date'] == ""){
              $further_date = "-----";
            } else {
              $further_date = date('d F Y',strtotime($value['pessi_further_date']));
            }

            if($value['reg_case_number'] == ""){
              $value['reg_case_number'] = "-";
            }

            if($value['reg_file_no'] == ""){
              $value['reg_file_no'] = "-";
            }

            if($value['case_name'] == ""){
              $value['case_name'] = "-";
            }

            if($value['stage_name'] == ""){
              $value['stage_name'] = "-";
            }

            $sheet->row($i, array($i-9,$value['reg_case_number'], $value['reg_file_no'], $court_type, $value['case_name'], $title_case, $value['stage_name'], $previous_date, $further_date  ));
              $sheet->row($i, function($row) {$row->setAlignment('left');});

            $i++;
           }  }

           $add = $i+3;

            $sheet->row($i+3, array('Address :- '.$admin_details[0]->admin_address ));
            $sheet->row($i+3, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
            $sheet->mergeCells('A'.$add.':'.'G'.$add);

            $mob = $i+4;
            $sheet->row($i+4, array('Mobile No. :- '.$admin_details[0]->admin_number ));
            $sheet->row($i+4, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
            $sheet->mergeCells('A'.$mob.':'.'G'.$mob);

            $email = $i+5;
            $sheet->row($i+5, array('Email Id :- '.$admin_details[0]->admin_email ));
            $sheet->row($i+5, function($row) {$row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
            $sheet->mergeCells('A'.$email.':'.'G'.$email);


         });

      })->download('xlsx');
 
    }



    // print cause list

    public function print_cause_list(Request $request){

      // print_r($request->input());
      // die;

      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      $query = Pessi::leftJoin('adv_case_reg', function($join) {$join->on('pessi_case_reg', '=', 'reg_id');})->leftJoin('adv_judge', function($join) {$join->on('honaurable_justice', '=', 'judge_id');})->leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->leftJoin('adv_court', function($join) {$join->on('court_id', '=', 'reg_court_id');})->leftJoin('adv_stage', function($join) {$join->on('stage_id', '=', 'pessi_statge_id');});

      if($request['further_date'] != ""){
        $query = $query->where('pessi_further_date', '=', date('Y-m-d',strtotime($request['further_date'])) );
      }

      if($request['court_type'] != ""){
        $query = $query = $query->where('reg_court', '=', $request['court_type']);
      }


      if($request['court_type'] == 1){
        $data = $query->where('pessi_user_id', $admin_details[0]->admin_id)->where('pessi_status',1)->where('pessi_choose_type','=',0)->orderBy('court_number', 'ASC')->get()->toArray();
      } else {
        $data = $query->where('pessi_user_id', $admin_details[0]->admin_id)->where('pessi_status',1)->where('pessi_choose_type','=',0)->orderByRaw('LENGTH(court_number)', 'ASC')->orderBy('court_number', 'ASC')->get();
     }


     usort($data, function($a, $b) {
          return $a['court_number'] <=> $b['court_number'];
      });

     // echo "<pre>";
     // print_r($data);
     // die;

     // if($request['further_date'] == ""){
     //  $request['further_date'] = date('d/m/Y');
     // }

     
     if($request['further_date'] == ""){ 
      $date = date('d/m/Y'); 

   //   print_r($date);
    } else { 
      $date = date('d/m/Y',strtotime($request['further_date'])); 
    }

    $day  = date('l',strtotime($date));
      // print_r($request->input());
      // die;


    

     if($request['court_type'] == 1){

        Excel::create('Cause List', function($excel) use ($data,$admin_details,$day,$date) {
        $excel->sheet('Sheet', function ($sheet) use ($data,$admin_details,$day,$date) {

          $sheet->setOrientation('landscape');
          $sheet->mergeCells('A1:G1');
          $sheet->mergeCells('A2:G2');
          $sheet->mergeCells('A3:G3');
          $sheet->mergeCells('A4:G4');
          $sheet->mergeCells('A5:G5');
          $sheet->mergeCells('A6:B6');
          $sheet->mergeCells('C6:E6');
          $sheet->mergeCells('F6:G6');
          $sheet->setHeight(array(
            1     =>  20,
            2     =>  20,
          ));
          $sheet->setSize('A8', 15, 18);
          $sheet->setSize('B8', 18, 18);
          $sheet->setSize('C8', 23, 18);
          $sheet->setSize('D8', 23, 18);
          $sheet->setSize('E8', 23, 18);
          $sheet->setSize('F8', 40, 18);
          $sheet->setSize('G8', 15, 18);
          $sheet->row(1, array( 'OFFICE OF' ));
          $sheet->row(1, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          $sheet->row(2, array( $admin_details[0]->admin_firmname ));
          $sheet->row(2, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          $sheet->row(3, array( $admin_details[0]->admin_advocates ));
          $sheet->row(3, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          $sheet->row(4, array( '(Advocates)' ));
          $sheet->row(4, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          $sheet->row(6, array( 'Date : '.$date, '','                                        Cause List', '' , '', 'Trail Court'));
          $sheet->row(6, function($row) { $row->setAlignment('center');} );
          $sheet->mergeCells('A7:G7');
          $sheet->row(8, array( 'Prev. Date', 'Court','Purpose', 'Case No','NCV No', 'Title of Case' , 'Next Date'));
          $sheet->row(8, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
          $sheet->row(9, array('', '', '', '', ''));
          $sheet->mergeCells('A9:G9');
          // $sheet->setAutoSize(true);
           
          $i=10;

          if(empty($data) ){
            $sheet->mergeCells('A10:G10');
            $sheet->mergeCells('A11:G11');
            $sheet->mergeCells('A12:G12');
            $sheet->row(11, array('No Record Found'));
            $sheet->row(11, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          } else {

            foreach ($data as $keys => $values) {

              $title = $values['reg_petitioner'].'v/s'.$values['reg_respondent'];

              $pre_date  = date('d/m/Y',strtotime($values['pessi_prev_date']));

              $next_date = date('d/m/Y',strtotime($values['pessi_further_date']));


              if($values['court_name'] == ""){
                $values['court_name'] = "-";
              }

              if($values['stage_name'] == ""){
                $values['stage_name'] = "-";
              }

              if($values['reg_case_number'] == ""){
                $values['reg_case_number'] = "-";
              }

              if($values['reg_vcn_number'] == ""){
                $values['reg_vcn_number'] = "-";
              }
              
              $sheet->row($i, array($pre_date, $values['court_name'],$values['stage_name'], $values['reg_case_number'],$values['reg_vcn_number'], $title, '_______________'));
              $sheet->row($i, function($row) {$row->setAlignment('left');});

              $i++;

            }
          }

            $add = $i+3;

            $sheet->row($i+3, array('Address :- '.$admin_details[0]->admin_address ));
            $sheet->row($i+3, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
            $sheet->mergeCells('A'.$add.':'.'G'.$add);

            $mob = $i+4;
            $sheet->row($i+4, array('Mobile No. :- '.$admin_details[0]->admin_number ));
            $sheet->row($i+4, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
            $sheet->mergeCells('A'.$mob.':'.'G'.$mob);

            $email = $i+5;
            $sheet->row($i+5, array('Email Id :- '.$admin_details[0]->admin_email ));
            $sheet->row($i+5, function($row) {$row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
            $sheet->mergeCells('A'.$email.':'.'G'.$email);

     //    die;

         });

      })->download('xlsx');

     } else {
        $arr_court_data =[];
        foreach ($data as $key => $court_data)
        {
            
            // echo "<pre>";
            // print_r($court_data);
            // die;
            
            //1 (2:00)PM
            // print_r($court_data);


          if($court_data->court_number == ""){
              if (!in_array($court_data->court_number, $keys))
              {
                $court_data_detail = [];
              }
            } else {
            
            if (is_numeric($court_data->court_number)){
                if (!in_array($court_data->court_number, $keys))
                {   
                  $court_data_detail = [];
                }
            } else {
                if (!in_array($court_data->court_number, $keys,true))
                {   
                  $court_data_detail = [];
                }
            }
        }
            $get_judge    = Judge::where(['judge_id' => $court_data->honarable_justice_db , 'judge_status' => 1 ])->first();

            $court_data_detail[]   = array(
              'pessi_id'          => $court_data->pessi_id,
              'serial_number'     => $court_data->serial_number,
              'page_number'       => $court_data->page_number,
              'case_name'         => $court_data->case_name, 
              'reg_case_number'   => $court_data->reg_case_number,
              'reg_petitioner'    => $court_data->reg_petitioner,
              'reg_respondent'    => $court_data->reg_respondent,
              'reg_power'         => $court_data->reg_power,
              'honaurable_justice' => $court_data->honaurable_justice,
              'honarable_justice_db' => $court_data->honarable_justice_db,
              'judge_name'        => $court_data->judge_name,
              'honable_name'      => $get_judge->judge_name,
              'court_number'      => $court_data->court_number,
              'stage_name'        => $court_data->stage_name,
            );
            //$court_data;

        //    $arr_court_data[$court_data->court_number.'-'.$court_data->honaurable_justice.'-'.$court_data->honarable_justice_db] = $court_data_detail;
            $arr_court_data[$court_data->court_number] = $court_data_detail;
            $keys = array_keys($arr_court_data);
        }
        
        // die;
    //     echo "<pre>";
    // //$keys = array_keys($arr_court_data);
    //     print_r($arr_court_data );
    //     die;

      Excel::create('Cause List', function($excel) use ($arr_court_data,$admin_details,$day,$date) {
        $excel->sheet('Sheet', function ($sheet) use ($arr_court_data,$admin_details,$day,$date) {

          $sheet->setOrientation('landscape');
          $sheet->mergeCells('A1:G1');
          $sheet->mergeCells('A2:G2');
          $sheet->mergeCells('A3:G3');
          $sheet->mergeCells('A4:G4');
          $sheet->mergeCells('A5:G5');
          $sheet->mergeCells('A6:B6');
          $sheet->mergeCells('C6:E6');
          $sheet->mergeCells('F6:G6');

          $sheet->setHeight(array(
            1     =>  20,
            2     =>  20,
          ));

          $sheet->setSize('A8', 15, 18);
          $sheet->setSize('B8', 15, 18);
          $sheet->setSize('C8', 15, 18);
          $sheet->setSize('D8', 20, 18);
          $sheet->setSize('E8', 20, 18);
          $sheet->setSize('F8', 40, 18);
          $sheet->setSize('G8', 15, 18);

          $sheet->row(1, array( 'OFFICE OF' ));
          $sheet->row(1, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          $sheet->row(2, array( $admin_details[0]->admin_firmname ));
          $sheet->row(2, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          $sheet->row(3, array( $admin_details[0]->admin_advocates ));
          $sheet->row(3, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          $sheet->row(4, array( '(Advocates)' ));
          $sheet->row(4, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});

          $sheet->row(6, array( $date, '', '                                       Cause List','','','High Court'));
          $sheet->row(6, function($row) { $row->setAlignment('center');} );
          $sheet->mergeCells('A7:G7');
          $sheet->row(8, array( 'Page No.' , 'Serial Number' ,'Purpose', 'Type', 'Case No.', 'Title' , 'Remark'));
          $sheet->row(8, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
          $sheet->row(9, array('', '', '', '', ''));
          $sheet->mergeCells('A9:G9');
          // $sheet->setAutoSize(true);
           
          $i=10;

         
          if(empty($arr_court_data) ){
            $sheet->mergeCells('A10:G10');
            $sheet->mergeCells('A11:G11');
            $sheet->mergeCells('A12:G12');
            $sheet->row(11, array('No Record Found'));
            $sheet->row(11, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          } else {

            foreach ($arr_court_data as $key => $value) {
                
//                $explode_keys    = explode('-', $key);
            //   echo "<pre>";
            //   print_r($key);
            //   die;
        //             C-12 / Hon'ble Abc & XYZ
              
              if($value[0]['court_number']  != "" ){
                $court = "C-".$value[0]['court_number'];
              }

              if($value[0]['judge_name'] != "" && $value[0]['court_number'] != ""){
               $court =  $court."";
              }

              if($value[0]['judge_name']  != "" ){
                $court = $court."( Hon'ble ".$value[0]['judge_name'];
              }

              if($value[0]['honable_name']  != "" ){
                $court = $court." & ".$value[0]['honable_name']." )";
              }

              if($value[0]['court_number']  != "" && $value[0]['honable_name']  == "" ){ 
                $court = $court." ) ";
              }

              


              // if($value[0]['judge_name'] != "" && $value[0]['court_number'] != ""){
              //  $court =  $value[0]['judge_name'].'( Court No. - '.$value[0]['court_number'].')';
              // }else if($value[0]['judge_name'] == ""){
              //  $court =  '( Court No. - '.$value[0]['court_number'].')';
              // }else if($value[0]['court_number'] == ""){
              //  $court =  $value[0]['judge_name'];
              // }
              //print_r($court);

              $sheet->row($i, array( $court ));
              $sheet->mergeCells('A'.$i.':'.'G'.$i);
              $sheet->row($i, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
              $sheet->getStyle('A'.$i)->getAlignment()->setWrapText(true);
              $sheet->setHeight(array(
                $i     =>  30,
              ));
              
              foreach ($value as $keys => $values) {

                $title = $values['reg_petitioner'].' v/s '.$values['reg_respondent'];

                if($values['reg_power'] == 1){
                  $power = (P);
                } else if($values['reg_power'] == 2){
                  $power = (R);
                } else if($values['reg_power'] == 3){
                  $power = (C);
                } else {
                  $power = (N);
                }


                if($values['page_number'] == ""){
                  $values['page_number'] = "-";
                }

                if($values['serial_number'] == ""){
                  $values['serial_number'] = "-";
                }

                if($values['stage_name'] == ""){
                  $values['stage_name'] = "-";
                }

                if($values['reg_case_number'] == ""){
                  $values['reg_case_number'] = "-";
                }

                if($values['case_name'] == ""){
                  $values['case_name'] = "-";
                }

                
                $sheet->row($i+1, array($values['page_number'],$values['serial_number'],$values['stage_name'],$values['case_name'], $values['reg_case_number'], $title, $power));
                $sheet->row($i+1, function($row) {$row->setAlignment('left');});
                $i++;
              
              }
              
              $i++;
            }

          } 
        


         $add = $i+3;

            $sheet->row($i+3, array('Address :- '.$admin_details[0]->admin_address ));
            $sheet->row($i+3, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
            $sheet->mergeCells('A'.$add.':'.'G'.$add);

            $mob = $i+4;
            $sheet->row($i+4, array('Mobile No. :- '.$admin_details[0]->admin_number ));
            $sheet->row($i+4, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
            $sheet->mergeCells('A'.$mob.':'.'G'.$mob);

            $email = $i+5;
            $sheet->row($i+5, array('Email Id :- '.$admin_details[0]->admin_email ));
            $sheet->row($i+5, function($row) {$row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
            $sheet->mergeCells('A'.$email.':'.'G'.$email);
     
         });

      })->download('xlsx');

    }
 
    }



    //view peshi form

    public function daily_cause_list(Request $request , $search_type=false){
      
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if(isset(json_decode($admin_details[0]->special_permission)->daily_cause_list)){

      } else {
        return Redirect()->back();
      }

      if($request['check'] != ""){
        Pessi::whereIn('pessi_id', $request['check'])->delete();
        return Redirect()->back();
      }      

      $query = Pessi::leftJoin('adv_case_reg', function($join) {$join->on('pessi_case_reg', '=', 'reg_id');})->leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->leftJoin('adv_stage', function($join) {$join->on('stage_id', '=', 'pessi_statge_id');})->leftJoin('adv_court', function($join) {$join->on('court_id', '=', 'reg_court_id');})->leftJoin('adv_assigned', function($join) {$join->on('assign_id', '=', 'reg_assigend_id');})->leftJoin('adv_referred', function($join) {$join->on('ref_id', '=', 'reg_reffeerd_by_id');})->leftJoin('adv_client', function($join) {$join->on('cl_id', '=', 'reg_client_group_id');})->leftJoin('adv_sub_client', function($join) {$join->on('sub_client_id', '=', 'reg_client_subgroup_id');});

      $garbage = Pessi::leftJoin('adv_case_reg', function($join) {$join->on('pessi_case_reg', '=', 'reg_id');})->leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->leftJoin('adv_stage', function($join) {$join->on('stage_id', '=', 'pessi_statge_id');})->leftJoin('adv_court', function($join) {$join->on('court_id', '=', 'reg_court_id');})->leftJoin('adv_assigned', function($join) {$join->on('assign_id', '=', 'reg_assigend_id');})->leftJoin('adv_referred', function($join) {$join->on('ref_id', '=', 'reg_reffeerd_by_id');})->leftJoin('adv_client', function($join) {$join->on('cl_id', '=', 'reg_client_group_id');})->leftJoin('adv_sub_client', function($join) {$join->on('sub_client_id', '=', 'reg_client_subgroup_id');});


      if($search_type == ""){
      
        if($court_type != ""){
          $request['court_type'] = $court_type;
        } else if($request['court_type'] != ""){
          $request['court_type'] = $request['court_type'];
        } else {
          $request['court_type'] = 2;
        }
        
        if($pessi_date != ""){
          $request['further_date_search'] = $pessi_date;
        }

        if($request['further_date_search'] != ""){
          $query = $query->where('pessi_further_date', '=', date('Y-m-d',strtotime($request['further_date_search'])) );
          $garbage = $garbage->where('pessi_further_date', '=', date('Y-m-d',strtotime($request['further_date_search'])) );
        } else {
          $query = $query->where('pessi_further_date', '=', date('Y-m-d') );
          $garbage = $garbage->where('pessi_further_date', '=', date('Y-m-d') );
        }

        if($request['court_type'] != ""){
          if($request['court_type'] != 3){
            $query  = $query->where('reg_court', '=', $request['court_type']);
            $garbage  = $garbage->where('reg_court', '=', $request['court_type']);
          }
        }
      }



      // print_r($request->input());
      // die;

      $app_data =  array('respondent' => $request['respondent'],'client_name' => $request['client_name'],'assign_wise' => $request['assign_wise'],'stage' => $request['stage'],'reffered_by' => $request['reffered_by'],'reg_stage_id' => $request['reg_stage_id'],'type' => $request['type'],'petitioner_name' => $request['petitioner_name'],'ncv_no' => $request['ncv_no'],'case_no' => $request['case_no'],'case_name' => $request['case_name'],'subtype_name' => $request['subtype_name'],'reg_file_no' => $request['reg_file_no'] ,'case_year' => $request['case_year'],'further_date_search' => $request['further_date_search'],'court_type' => $request['court_type'] );
 
      if($search_type == "all"){
        $perpage    = $query->count();
        $get_record = $query->where('pessi_user_id', $admin_details[0]->admin_id)
        //->where('pessi_status',1)
        // ->where('pessi_choose_type','!=',2)
        // ->where('pessi_choose_type','!=',1)
        ->where('reg_id','!=',"")->where('cause_status','=',1)->orderBy('reg_id', 'desc')->orderBy('pessi_id', 'desc')->paginate($perpage)->appends($app_data);

        $garbage_data = $garbage->where('pessi_user_id', $admin_details[0]->admin_id)
        //->where('pessi_status',1)
        // ->where('pessi_choose_type','!=',2)
        // ->where('pessi_choose_type','!=',1)
        ->where('reg_id','!=',"")->where('cause_status','!=',1)->orderBy('reg_id', 'desc')->orderBy('pessi_id', 'desc')->paginate($perpage)->appends($app_data);

      } else {
        $get_record = $query->where('pessi_user_id', $admin_details[0]->admin_id)
        //->where('pessi_status',1)
        // ->where('pessi_choose_type','!=',2)
        // ->where('pessi_choose_type','!=',1)
        ->where('reg_id','!=',"")->where('cause_status','=',1)->orderBy('reg_id', 'desc')->paginate(10)->appends($app_data);

        $garbage_data = $garbage->where('pessi_user_id', $admin_details[0]->admin_id)
        //->where('pessi_status',1)
        // ->where('pessi_choose_type','!=',2)
        // ->where('pessi_choose_type','!=',1)
        ->where('reg_id','!=',"")->where('cause_status','!=',1)->orderBy('reg_id', 'desc')->paginate(10)->appends($app_data);

      }

      // echo "<pre>";
      // print_r($garbage_data);
      // die;

      $get_case_regestered = Case_Registration::where('user_admin_id','=',$admin_details[0]->admin_id )->get();
      $stage               = Stage::where(['user_admin_id' => session('admin_id') ])->where('stage_name','!=',"")->get();
      $get_stage           = Stage::where(['user_admin_id' => session('admin_id') , 'stage_status' => 1 ])->where('stage_name','!=',"")->get();
      $case_type_entry     = Case_Type_Entry::where(['user_admin_id' => session('admin_id') ])->where('case_name','!=',"")->get();    
      $get_judge           = Judge::where(['user_admin_id' => $admin_details[0]->admin_id , 'judge_status' => 1 ])->where('judge_name','!=',"")->get();


      $client         = Client::where([ 'cl_status' => 1 , 'user_admin_id' => session('admin_id') ])->where('cl_group_name' ,'!=', '')->get();

      $sub_client     = Sub_Client::where([ 'sub_client_status' => 1 , 'user_admin_id' => session('admin_id') ])->where('sub_client_name' ,'!=', '')->get();

      $referred = Reffered::where([ 'ref_status' => 1 , 'user_admin_id' => session('admin_id') ])->where('ref_advocate_name' ,'!=', '')->get();

      $assigned = Assigned::where([ 'assign_status' => 1 , 'user_admin_id' => session('admin_id') ])->where('assign_advocate_name' ,'!=', '')->get();
      
      $reg_case_type_id   = $request['type'];
      $reg_stage_id       = $request['reg_stage_id'];
      
      if($request['further_date_search'] == ""){
        $further_date          = date('Y-m-d');
      } else {
        $further_date          = date('Y-m-d',strtotime($request['further_date_search']));
      }

      $court_type         = $request['court_type'];
      $case_status        = $request['case_status'];


      // print_r($get_record);
      // die;
      

      $title = 'Daily Diary';
      $data  = compact('title','admin_details','get_record','get_case_regestered','stage','case_type_entry','case_no','reg_case_type_id','reg_stage_id','file_no','further_date','get_judge','get_stage','court_type','case_status','garbage_data','client','sub_client','referred','assigned');
      return view('advocate_admin/daily_cause_list',$data); 
    }




    public function send_sms_daily(Request $request){  

      $get_pessi = Pessi::where('pessi_id','=', $request['pessi_id'])->first();

      $query_pessi = Pessi::leftJoin('adv_case_reg', function($join) {$join->on('pessi_case_reg', '=', 'reg_id');})->leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->where('pessi_status',1)->where('pessi_id','=', $request['pessi_id'])->first(); 

      $case_registration =  SmsText::where(['sms_content_type' => 4, 'user_admin_id' => session('admin_id') ])->first();
        
      // For HC - Case Type  Case No. Title Text
      // For TC - Case Type  Case No. NCV No. Title Text

      if($case_registration->sms_content_before != ""){
        $message = $case_registration->sms_content_before;
      }

      if($query_pessi->case_name != ""){
        $message = "Case Type : $query_pessi->case_name";
      }

      if($query_pessi->reg_case_number != ""){
        $message = "$message Case No: ".$query_pessi->reg_case_number;
      }

      $message = "$message Title: ".$query_pessi->reg_petitioner.' V/s '.$query_pessi->reg_respondent;


      // print_r($request->input());
      // die;
        
      if($query_pessi->pessi_choose_type != "undefined"){
        $next_decide  ="$message Next Date/Decide: ".$query_pessi->pessi_further_date;
      } else { 
        if($query_pessi->pessi_choose_type == 1){    
          if($query_pessi->pessi_decide_id != ""){
            $decide  = $query_pessi->pessi_decide_id;
          } else {
            $decide  = $get_pessi['pessi_decide_id'];
          }

          if($decide == 1){
            $next_decide  ="$message Next Date/Decide: Case In Favour";
          }
          if($decide == 2){
            $next_decide  ="$message Next Date/Decide: Case in against";
          }
          if($decide == 3){
            $next_decide  ="$message Next Date/Decide: Withdraw";
          }
          if($decide == 4){
            $next_decide  ="$message Next Date/Decide: None";
          } 
        } else {
          $next_decide  ="$message Next Date/Decide: Due Course";
        }

      }

      if($request['sms_text'] != ""){
        $message = "$message Text: ".$request['sms_text'];
      }      

      if($case_registration->sms_content_after != ""){
        $message = $message.' '.$case_registration->sms_content_after;
      }


      // print_r($request->input());
      // die;


    //  foreach($request['assigned'] as $assigned ) {


        $assigned   = Assigned::where([ 'assign_id' => $request['assigned'] ])->first();
        $mobile_no  = $assigned->assign_mobile_number;
        $this->send_message->send_message($mobile_no,$message);
    //  }

    //  foreach($request['referred'] as $referred ) {
        $referred   = Reffered::where([ 'ref_id' => $request['referred'] ])->first();
        $mobile_no  = $referred->ref_mobile_number;
        $this->send_message->send_message($mobile_no,$message);
    //  }

    //  foreach($request['client'] as $client ) {
        $client     = Client::where([ 'cl_id' => $request['client'] ])->first();
        $mobile_no  = $client->cl_group_mobile_no;
        $this->send_message->send_message($mobile_no,$message);
    //  }

    //  foreach($request['sub_clients'] as $sub_client ) {
        $sub_client  = Sub_Client::where([ 'sub_client_id' => $request['sub_clients'] ])->first();
        $mobile_no   = $sub_client->sub_client_mobile_no;
        $this->send_message->send_message($mobile_no,$message);
    //  }


      $pessi_update = array('pessi_sms_status' => 1);
      Pessi::where('pessi_id','=', $request['pessi_id'])->update($pessi_update);


    }

    //insert Daily Diary function

    public function insert_daily_cause_list(Request $request, $pessi_id = false){    

      $get_pessi = Pessi::where('pessi_id','=', $request['pessi_id'])->first();

      if($request['further_date'] != ""){
        $further_date  = date('Y-m-d',strtotime($request['further_date']));
      } else {
        $further_date  = date('Y-m-d',strtotime($get_pessi['pessi_further_date']));
      }

      if(date('Y-m-d',strtotime($get_pessi['pessi_further_date'])) <= date('Y-m-d',strtotime($request['further_date'])) ){
        $new_previous_date  = date('Y-m-d',strtotime($get_pessi['pessi_further_date']));
      } else {
        $new_previous_date  = date('Y-m-d',strtotime($get_pessi['pessi_prev_date']));
      }

      $query_pessi = Pessi::leftJoin('adv_case_reg', function($join) {$join->on('pessi_case_reg', '=', 'reg_id');})->leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->where('pessi_status',1)->where('pessi_id','=', $request['pessi_id'])->first(); 

      if($request['choose_type'] != "undefined"){
        $choose_type  = $request['choose_type'];
      } else {
        $choose_type  = $get_pessi['pessi_choose_type'];
      }

      if($request['decide'] != ""){
        $decide  = $request['decide'];
      } else {
        $decide  = $get_pessi['pessi_decide_id'];
      }


      if( date('Y-m-d',strtotime($get_pessi['pessi_further_date'])) == $further_date ){


      // print_r($further_date);
      // echo "<pre>"; 
      // print_r($request->input());
      // die;

        $pessi_update = array(
          'pessi_statge_id'     => $request['stage'],
          'pessi_decide_id'     => $decide,
          'pessi_further_date'  => $further_date,
          'pessi_order_sheet'   => $request['order_sheet'],
          'pessi_choose_type'   => $choose_type,
          'pessi_update'        => 1,
        );

        Pessi::where('pessi_id','=', $request['pessi_id'])->update($pessi_update);

      } else {

        $update_pessi = array(
          'pessi_order_sheet'   => $request['order_sheet'],
        );

        Pessi::where('pessi_id','=', $request['pessi_id'])->update($update_pessi);


        $Add = new Pessi;  
        $Add->pessi_case_reg              = $get_pessi['pessi_case_reg'];
        $Add->pessi_prev_date             = $new_previous_date;
        $Add->pessi_statge_id             = $request['stage'];
        $Add->pessi_user_id               = $get_pessi['pessi_user_id'];
        $Add->pessi_decide_id             = $decide;
        $Add->pessi_further_date          = $further_date;
        $Add->pessi_choose_type           = $choose_type;
//        $Add->serial_number               = $get_pessi['serial_number'];
        // $Add->honaurable_justice          = $get_pessi['honaurable_justice'];
        // $Add->court_number                = $get_pessi['court_number'];
        // $Add->page_number                 = $get_pessi['page_number'];
        $Add->save();
        $pessi_id = $Add->id;

        Pessi::where('pessi_id','!=', $pessi_id)->where('pessi_case_reg', $get_pessi['pessi_case_reg'])->update(['pessi_status' => 0]);

        }

      //  return Redirect()->back();

    }
    

    public function insert_daily_cause_list_old(Request $request, $pessi_id = false){     

      print_r($request->input());
      DIE;

      $get_pessi = Pessi::where('pessi_id','=', $pessi_id)->first();

      if($request['further_date'] != ""){
        $further_date  = date('Y-m-d',strtotime($request['further_date']));
      } else {
        $further_date  = date('Y-m-d',strtotime($get_pessi['pessi_further_date']));
      }


      $query_pessi = Pessi::leftJoin('adv_case_reg', function($join) {$join->on('pessi_case_reg', '=', 'reg_id');})->leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->where('pessi_status',1)->where('pessi_id','=', $pessi_id)->first(); 

        $Add = new Pessi;  
        $Add->pessi_case_reg              = $get_pessi['pessi_case_reg'];
        $Add->pessi_prev_date             = $get_pessi['pessi_further_date'];
        $Add->pessi_statge_id             = $request['reg_stage'];
        $Add->pessi_user_id               = $get_pessi['pessi_user_id'];
        $Add->pessi_decide_id             = $request['decide'];
        $Add->pessi_further_date          = $further_date;
        $Add->pessi_choose_type           = 0;
        $Add->pessi_order_sheet           = $request['order_sheet'];
        $Add->serial_number               = $get_pessi['serial_number'];
        $Add->honaurable_justice          = $get_pessi['honaurable_justice'];
        $Add->court_number                = $get_pessi['court_number'];
        $Add->page_number                 = $get_pessi['page_number'];
        $Add->save();
        $id = $Add->id;

        Pessi::where('pessi_id','!=', $id)->where('pessi_case_reg', $get_pessi['pessi_case_reg'])->update(['pessi_status' => 0]);
        return Redirect()->back();

    }




    // print Daily Diary

    public function print_daily_cause_list(Request $request , $further_date = false ,$court_type = false ){

      $request['further_date'] =  $further_date;
      $request['court_type'] =  $court_type;


      // print_r($request->input());
      // die;  


      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      $query = Pessi::leftJoin('adv_case_reg', function($join) {$join->on('pessi_case_reg', '=', 'reg_id');})->leftJoin('adv_judge', function($join) {$join->on('honaurable_justice', '=', 'judge_id');})->leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->leftJoin('adv_court', function($join) {$join->on('court_id', '=', 'reg_court_id');})->leftJoin('adv_stage', function($join) {$join->on('stage_id', '=', 'pessi_statge_id');});

      $garbage = Pessi::leftJoin('adv_case_reg', function($join) {$join->on('pessi_case_reg', '=', 'reg_id');})->leftJoin('adv_judge', function($join) {$join->on('honaurable_justice', '=', 'judge_id');})->leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->leftJoin('adv_court', function($join) {$join->on('court_id', '=', 'reg_court_id');})->leftJoin('adv_stage', function($join) {$join->on('stage_id', '=', 'pessi_statge_id');});

      if($request['further_date'] != ""){
        $query = $query->where('pessi_further_date', '=', date('Y-m-d',strtotime($request['further_date'])) );
        $garbage = $garbage->where('pessi_further_date', '=', date('Y-m-d',strtotime($request['further_date'])) );
      } else {
        $query = $query->where('pessi_further_date', '=', date('Y-m-d') );
        $garbage = $garbage->where('pessi_further_date', '=', date('Y-m-d') );
      }

      if($request['court_type'] != ""){
        $query = $query->where('reg_court', '=', $request['court_type']);
        $garbage = $garbage->where('reg_court', '=', $request['court_type']);
      }

      $data = $query->where('pessi_user_id', $admin_details[0]->admin_id)
      // ->where('pessi_status',1)
      // ->where('pessi_choose_type','!=',2)
      // ->where('pessi_choose_type','!=',1)
      ->where('reg_id','!=',"")->where('cause_status','=',1)->orderBy('reg_id', 'desc')->get()->toArray();

      $garbage_data = $garbage->where('pessi_user_id', $admin_details[0]->admin_id)
      // ->where('pessi_status',1)->where('pessi_choose_type','!=',2)->where('pessi_choose_type','!=',1)
      ->where('reg_id','!=',"")->where('cause_status','=',0)->orderBy('reg_id', 'desc')->get()->toArray();


      $data = array_merge($data,$garbage_data);


      usort($data, function($a, $b) {
          return $a['court_number'] <=> $b['court_number'];
      });

      // function compareByName($a, $b) {
      //   return strcmp($a["court_number"], $b["court_number"]);
      // }
      // usort($data, 'compareByName');

      // echo "<pre>";
      // print_r($data);
      // die;
     
     if($request['further_date'] == ""){ 
      $date = date('d/m/Y'); 

   //   print_r($date);
    } else { 
      $date = date('d/m/Y',strtotime($request['further_date'])); 
    }

    $day  = date('l',strtotime($date));

    // print_r($day);
    // die;


      // echo "<pre>";
      // print_r($request->input());
      // die;

     if($request['court_type'] == 1){

        
        $arr_court_data =[];
        foreach ($data as $key => $court_data)
        {

      //     print_r($court_data);
      // die;
          
            // if (!in_array($court_data['court_number'], $keys))
            // {
            //     $court_data_detail = [];
            // }

            if (is_numeric($court_data['court_number'])){
                if (!in_array($court_data['court_number'], $keys))
                {   
                  $court_data_detail = [];
                }
            } else {
                if (!in_array($court_data['court_number'], $keys,true))
                {   
                  $court_data_detail = [];
                }
            }

            $honarable_justice_db = Judge::where(['judge_id' => $court_data['honarable_justice_db']])->first();

            $court_data_detail[]   = array(
              'pessi_id'             => $court_data['pessi_id'],
              'page_number'          => $court_data['page_number'],
              'case_name'            => $court_data['case_name'], 
              'reg_case_number'      => $court_data['reg_case_number'],
              'reg_vcn_number'       => $court_data['reg_vcn_number'],
              'reg_petitioner'       => $court_data['reg_petitioner'],
              'reg_respondent'       => $court_data['reg_respondent'],
              'reg_power'            => $court_data['reg_power'],
              'judge_name'           => $court_data['judge_name'],
              'court_number'         => $court_data['court_number'],
              'serial_number'        => $court_data['serial_number'],
              'stage_name'           => $court_data['stage_name'],
              'pessi_order_sheet'    => $court_data['pessi_order_sheet'],
              'pessi_further_date'   => $court_data['pessi_further_date'],
              'pessi_choose_type'    => $court_data['pessi_choose_type'],
              'pessi_sms_status'     => $court_data['pessi_sms_status'],
              'honarable_justice_db' => $honarable_justice_db['judge_name'],
            );
            //$court_data;

            $arr_court_data[$court_data['court_number'] ] = $court_data_detail;
            $keys = array_keys($arr_court_data);
        }

//$keys = array_keys($arr_court_data);
      // print_r($arr_court_data );
      // die;

      Excel::create('Daily Diary', function($excel) use ($arr_court_data,$admin_details,$date,$day) {
        $excel->sheet('Sheet', function ($sheet) use ($arr_court_data,$admin_details,$date,$day) {

        

          $sheet->setOrientation('landscape');
          $sheet->mergeCells('A1:K1');
          $sheet->mergeCells('A2:K2');
          $sheet->mergeCells('A3:K3');
          $sheet->mergeCells('A4:K4');
          $sheet->mergeCells('A5:K5');
          $sheet->mergeCells('A6:B6');
          $sheet->mergeCells('C6:G6');
          $sheet->mergeCells('H6:K6');

          $sheet->setHeight(array(
            1     =>  20,
            2     =>  20,
          ));

          $sheet->setSize('A8', 10, 18);
          $sheet->setSize('B8', 15, 18);
          $sheet->setSize('C8', 15, 18);
          $sheet->setSize('D8', 15, 18);
          $sheet->setSize('E8', 25, 18);
          $sheet->setSize('F8', 20, 18);
          $sheet->setSize('G8', 40, 18);
          $sheet->setSize('H8', 15, 18);
          $sheet->setSize('I8', 15, 18);
          $sheet->setSize('J8', 10, 18);
          $sheet->setSize('K8', 20, 18);

          $sheet->row(1, array( 'OFFICE OF' ));
          $sheet->row(1, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          $sheet->row(2, array( $admin_details[0]->admin_firmname ));
          $sheet->row(2, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          $sheet->row(3, array( $admin_details[0]->admin_advocates ));
          $sheet->row(3, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          $sheet->row(4, array( '(Advocates)' ));
          $sheet->row(4, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          
          // $day = date("l");
          // $date = date("d/m/Y");

          $sheet->row(6, array( 'Daily Diary', '',         '                                                              '.$date,'','','','' , 'Trail Court'));
          $sheet->row(6, function($row) { $row->setAlignment('center');} );
          $sheet->mergeCells('A7:K7');
          $sheet->row(8, array( 'S.l. No.','Case Type', 'Case Number', 'NCV No.' ,'Title','Purpose','Order Sheet', 'Next Date', 'Result','SMS', 'Remark'));
          $sheet->row(8, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
          $sheet->row(9, array('', '', '', '', ''));
          $sheet->mergeCells('A9:K9');
          // $sheet->setAutoSize(true);
           
          $i=10;

          $sl_number = 0;

          if(empty($arr_court_data) ){

            $sheet->mergeCells('A10:K10');
            $sheet->mergeCells('A11:K11');
            $sheet->mergeCells('A12:K12');
            $sheet->row(11, array('No Record Found'));
            $sheet->row(11, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          } else {

          foreach ($arr_court_data as $value) {
            
            $sl_number++;

            if($value[0]['judge_name'] != "" && $value[0]['court_number'] != ""){

              if($value[0]['honarable_justice_db'] != ""){
                $court =  "HON'BLE ".$value[0]['judge_name'].' V/S '.$value[0]['honarable_justice_db'].' ( Court No. - '.$value[0]['court_number'].')';
              } else {
                $court =  "HON'BLE ".$value[0]['judge_name'].' ( Court No. - '.$value[0]['court_number'].')';
              }
             
            } else if($value[0]['judge_name'] == ""){

             $court =  '( Court No. - '.$value[0]['court_number'].')';
            
            } else if($value[0]['court_number'] == ""){

              if($value[0]['honarable_justice_db'] != ""){
                $court =  "HON'BLE ".$value[0]['judge_name'].' V/S '.$value[0]['honarable_justice_db'];
              } else {
                $court =  "HON'BLE ".$value[0]['judge_name'];
              }
 
            }
              
            // print_r($court);
            // die;

              $sheet->row($i, array( $court ));
              $sheet->mergeCells('A'.$i.':'.'K'.$i);
              $sheet->row($i, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
              $sheet->getStyle('A'.$i)->getAlignment()->setWrapText(true);
              $sheet->setHeight(array(
                $i     =>  30,
              ));

            
            foreach ($value as $keys => $values) {

              $title = $values['reg_petitioner'].' V/S '.$values['reg_respondent'];

              if($values['pessi_choose_type'] == 0) {
                $values['pessi_choose_type'] = "_______________";
              } else if($values['pessi_choose_type'] == 1) {
                $values['pessi_choose_type'] = "Decide";
              } else if($values['pessi_choose_type'] == 2) {
                $values['pessi_choose_type'] = "Due Course";
              }
              

              if($values['pessi_order_sheet'] == ""){
                $values['pessi_order_sheet'] = "_______________";
              } else {
                $values['pessi_order_sheet'] = $values['pessi_order_sheet'];
              }

              if($values['pessi_sms_status'] == 1){
                $values['pessi_sms_status'] = "Yes";
              } else {
                $values['pessi_sms_status'] = "No";
              }


              if($values['reg_vcn_number'] == ""){
                $values['reg_vcn_number'] = "-";
              }

              if($values['case_name'] == ""){
                $values['case_name'] = "-";
              }

              if($values['reg_case_number'] == ""){
                $values['reg_case_number'] = "-";
              }

              if($values['stage_name'] == ""){
                $values['stage_name'] = "-";
              }

              if($values['pessi_order_sheet'] == ""){
                $values['pessi_order_sheet'] = "-";
              }

              if($values['pessi_choose_type'] == ""){
                $values['pessi_choose_type'] = "-";
              }

              $values['pessi_further_date'] = date('d/m/Y',strtotime($values['pessi_further_date'])); 
                          
              $sheet->row($i+1, array($sl_number , $values['case_name'] , $values['reg_case_number'], $values['reg_vcn_number'] , $title ,$values['stage_name'],$values['pessi_order_sheet'], $values['pessi_further_date'], $values['pessi_choose_type'], $values['pessi_sms_status'] , ''));
              $sheet->row($i+1, function($row) {$row->setAlignment('left');});

              $i++;

            }

          $i++;

         } }

         $add = $i+3;

            $sheet->row($i+3, array('Address :- '.$admin_details[0]->admin_address ));
            $sheet->row($i+3, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
            $sheet->mergeCells('A'.$add.':'.'K'.$add);

            $mob = $i+4;
            $sheet->row($i+4, array('Mobile No. :- '.$admin_details[0]->admin_number ));
            $sheet->row($i+4, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
            $sheet->mergeCells('A'.$mob.':'.'K'.$mob);

            $email = $i+5;
            $sheet->row($i+5, array('Email Id :- '.$admin_details[0]->admin_email ));
            $sheet->row($i+5, function($row) {$row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
            $sheet->mergeCells('A'.$email.':'.'K'.$email);
     
         });

      })->download('xlsx');



     } else {

        $arr_court_data =[];
        foreach ($data as $key => $court_data)
        {

      //     echo "<pre>";
      //     print_r($court_data);
      // die;

           // if (is_numeric($court_data['court_number'])){
           //      if (!in_array($court_data['court_number'], $keys))
           //      {   
           //        $court_data_detail = [];
           //      }
           //  } else {
           //      if (!in_array($court_data['court_number'], $keys,true))
           //      {   
           //        $court_data_detail = [];
           //      }
           //  }
            
              // echo "<pre>";
              // print_r($keys);


            if($court_data['court_number'] == ""){
              if (!in_array($court_data['court_number'], $keys))
              {
                $court_data_detail = [];
              }
            } else {

                if (is_numeric($court_data['court_number'])){
                    if (!in_array($court_data['court_number'], $keys))
                    {   
                      $court_data_detail = [];
                    }
                } else {
                    if (!in_array($court_data['court_number'], $keys,true))
                    {   
                      $court_data_detail = [];
                    }
                }
            }  
          
            

            $honarable_justice_db = Judge::where(['judge_id' => $court_data['honarable_justice_db']])->first();

            $court_data_detail[]   = array(
              'pessi_id'             => $court_data['pessi_id'],
              'page_number'          => $court_data['page_number'],
              'case_name'            => $court_data['case_name'], 
              'reg_case_number'      => $court_data['reg_case_number'],
              'reg_petitioner'       => $court_data['reg_petitioner'],
              'reg_respondent'       => $court_data['reg_respondent'],
              'reg_power'            => $court_data['reg_power'],
              'judge_name'           => $court_data['judge_name'],
              'court_number'         => $court_data['court_number'],
              'serial_number'        => $court_data['serial_number'],
              'stage_name'           => $court_data['stage_name'],
              'pessi_order_sheet'    => $court_data['pessi_order_sheet'],
              'pessi_further_date'   => $court_data['pessi_further_date'],
              'pessi_choose_type'    => $court_data['pessi_choose_type'],
              'pessi_sms_status'     => $court_data['pessi_sms_status'],
              'honarable_justice_db' => $honarable_justice_db['judge_name'],
            );
            //$court_data;

            $arr_court_data[$court_data['court_number'] ] = $court_data_detail;
            $keys = array_keys($arr_court_data);
        }

//$keys = array_keys($arr_court_data);
      // echo "<pre>";
      // print_r($arr_court_data );
      // die;

      Excel::create('Daily Diary', function($excel) use ($arr_court_data,$admin_details,$date,$day) {
        $excel->sheet('Sheet', function ($sheet) use ($arr_court_data,$admin_details,$date,$day) {

          $sheet->setOrientation('landscape');
          $sheet->mergeCells('A1:J1');
          $sheet->mergeCells('A2:J2');
          $sheet->mergeCells('A3:J3');
          $sheet->mergeCells('A4:J4');
          $sheet->mergeCells('A5:J5');
          $sheet->mergeCells('A6:B6');
          $sheet->mergeCells('C6:G6');
          $sheet->mergeCells('H6:J6');

          $sheet->setHeight(array(
            1     =>  20,
            2     =>  20,
          ));

          $sheet->setSize('A8', 10, 18);
          $sheet->setSize('B8', 15, 18);
          $sheet->setSize('C8', 15, 18);
          $sheet->setSize('D8', 25, 18);
          $sheet->setSize('E8', 20, 18);
          $sheet->setSize('F8', 40, 18);
          $sheet->setSize('G8', 15, 18);
          $sheet->setSize('H8', 15, 18);
          $sheet->setSize('I8', 10, 18);
          $sheet->setSize('J8', 20, 18);

          $sheet->row(1, array( 'OFFICE OF' ));
          $sheet->row(1, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          $sheet->row(2, array( $admin_details[0]->admin_firmname ));
          $sheet->row(2, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          $sheet->row(3, array( $admin_details[0]->admin_advocates ));
          $sheet->row(3, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          $sheet->row(4, array( '(Advocates)' ));
          $sheet->row(4, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          
          // $day = date("l");
          // $date = date("d/m/Y");

          $sheet->row(6, array( 'Daily Diary','',         '                                  '.$date,'','','','' , 'High Court'));
          $sheet->row(6, function($row) { $row->setAlignment('center');} );
          $sheet->mergeCells('A7:J7');
          $sheet->row(8, array( 'S.l. No.','Case Type', 'Case Number' ,'Title','Purpose','Order Sheet', 'Next Date', 'Result','SMS', 'Remark'));
          $sheet->row(8, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
          $sheet->row(9, array('', '', '', '', ''));
          $sheet->mergeCells('A9:J9');
          // $sheet->setAutoSize(true);
           
          $i=10;

          $sl_number = 0;
          if(empty($arr_court_data) ){

            $sheet->mergeCells('A10:J10');
            $sheet->mergeCells('A11:J11');
            $sheet->mergeCells('A12:J12');
            $sheet->row(11, array('No Record Found'));
            $sheet->row(11, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          } else {

          foreach ($arr_court_data as $value) {
            
            //$sl_number++;

            if($value[0]['judge_name'] != "" && $value[0]['court_number'] != ""){

              if($value[0]['honarable_justice_db'] != ""){
                $court =  "HON'BLE ".$value[0]['judge_name'].' V/S '.$value[0]['honarable_justice_db'].' ( Court No. - '.$value[0]['court_number'].')';
              } else {
                $court =  "HON'BLE ".$value[0]['judge_name'].' ( Court No. - '.$value[0]['court_number'].')';
              }
             
            } else if($value[0]['judge_name'] == ""){

             $court =  '( Court No. - '.$value[0]['court_number'].')';
            
            } else if($value[0]['court_number'] == ""){

              if($value[0]['honarable_justice_db'] != ""){
                $court =  "HON'BLE ".$value[0]['judge_name'].' V/S '.$value[0]['honarable_justice_db'];
              } else {
                $court =  "HON'BLE ".$value[0]['judge_name'];
              }
 
            }
              
            // print_r($court);
            // die;

              $sheet->row($i, array( $court ));
              $sheet->mergeCells('A'.$i.':'.'J'.$i);
              $sheet->row($i, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
              $sheet->getStyle('A'.$i)->getAlignment()->setWrapText(true);
              $sheet->setHeight(array(
                $i     =>  30,
              ));

            
            foreach ($value as $keys => $values) {

              $sl_number++;
              $title = $values['reg_petitioner'].' V/S '.$values['reg_respondent'];

              if($values['pessi_choose_type'] == 0) {
                $values['pessi_choose_type'] = "_______________";
              } else if($values['pessi_choose_type'] == 1) {
                $values['pessi_choose_type'] = "Decide";
              } else if($values['pessi_choose_type'] == 2) {
                $values['pessi_choose_type'] = "Due Course";
              }
              

              if($values['pessi_order_sheet'] == ""){
                $values['pessi_order_sheet'] = "_______________";
              } else {
                $values['pessi_order_sheet'] = $values['pessi_order_sheet'];
              }

              if($values['pessi_sms_status'] == 1){
                $values['pessi_sms_status'] = "Yes";
              } else {
                $values['pessi_sms_status'] = "No";
              }

              $values['pessi_further_date'] = date('d/m/Y',strtotime($values['pessi_further_date'])); 
              

              if($values['pessi_sms_status'] == ""){
                $values['pessi_sms_status'] = "-";
              }

              if($values['case_name'] == ""){
                $values['case_name'] = "-";
              }

              if($values['reg_case_number'] == ""){
                $values['reg_case_number'] = "-";
              }

              if($values['stage_name'] == ""){
                $values['stage_name'] = "-";
              }

              if($values['pessi_order_sheet'] == ""){
                $values['pessi_order_sheet'] = "-";
              }

              if($values['pessi_choose_type'] == ""){
                $values['pessi_choose_type'] = "-";
              }


              $sheet->row($i+1, array($sl_number , $values['case_name'] , $values['reg_case_number'], $title ,$values['stage_name'],wordwrap($values['pessi_order_sheet'],50,"\n"), $values['pessi_further_date'], $values['pessi_choose_type'], $values['pessi_sms_status'] , ''));
              $sheet->row($i+1, function($row) {$row->setAlignment('left');});


              $i++;

            }


          $i++;


         } }

         $add = $i+3;

            $sheet->row($i+3, array('Address :- '.$admin_details[0]->admin_address ));
            $sheet->row($i+3, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
            $sheet->mergeCells('A'.$add.':'.'J'.$add);

            $mob = $i+4;
            $sheet->row($i+4, array('Mobile No. :- '.$admin_details[0]->admin_number ));
            $sheet->row($i+4, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
            $sheet->mergeCells('A'.$mob.':'.'J'.$mob);

            $email = $i+5;
            $sheet->row($i+5, array('Email Id :- '.$admin_details[0]->admin_email ));
            $sheet->row($i+5, function($row) {$row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
            $sheet->mergeCells('A'.$email.':'.'J'.$email);
     
         });

      })->download('xlsx');

    }
 
    }



    public function print_daily_cause_list_old(Request $request ,$court_type , $further_date = false){

      $request['further_date'] =  $further_date;
      $request['court_type'] =  $court_type;

      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      $query = Pessi::leftJoin('adv_case_reg', function($join) {$join->on('pessi_case_reg', '=', 'reg_id');})->leftJoin('adv_judge', function($join) {$join->on('honaurable_justice', '=', 'judge_id');})->leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->leftJoin('adv_court', function($join) {$join->on('court_id', '=', 'reg_court_id');});

      // if($request['further_date'] != ""){
      //   $query = $query->where('pessi_further_date', '=', date('Y-m-d',strtotime($request['further_date'])) );
      // }

      // if($request['court_type'] != ""){
      //   $query = $query = $query->where('reg_court', '=', $request['court_type']);
      // }


      if($request['court_type'] == 1){
        $data = $query->where('pessi_user_id', $admin_details[0]->admin_id)->where('pessi_status',1)->orderBy('court_number', 'ASC')->get()->toArray();
      } else {
        $data = $query->where('pessi_user_id', $admin_details[0]->admin_id)->where('pessi_status',1)->orderBy('court_number', 'ASC')->get();
     }
      

      // print_r($request['further_date']);
      // die;
     $day  = date('l',strtotime($request['further_date']));
     $date = date('d/m/Y',strtotime($request['further_date']));

     if($request['court_type'] == 1){

        Excel::create('Daily Diary', function($excel) use ($data,$admin_details,$date,$day) {
        $excel->sheet('Sheet', function ($sheet) use ($data,$admin_details,$date,$day) {

          $sheet->setOrientation('landscape');
          $sheet->mergeCells('A1:E1');
          $sheet->mergeCells('A2:E2');
          $sheet->mergeCells('A3:E3');
          $sheet->mergeCells('A4:E4');
          $sheet->mergeCells('A5:E5');
          $sheet->mergeCells('A6:B6');
          $sheet->mergeCells('C6:D6');
          $sheet->setHeight(array(
            1     =>  20,
            2     =>  20,
          ));
          $sheet->setSize('A8', 15, 18);
          $sheet->setSize('B8', 15, 18);
          $sheet->setSize('C8', 20, 18);
          $sheet->setSize('D8', 40, 18);
          $sheet->setSize('E8', 15, 18);
          $sheet->row(1, array( 'OFFICE OF' ));
          $sheet->row(1, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          $sheet->row(2, array( $admin_details[0]->admin_firmname ));
          $sheet->row(2, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          $sheet->row(3, array( $admin_details[0]->admin_advocates ));
          $sheet->row(3, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          $sheet->row(4, array( '(Advocates)' ));
          $sheet->row(4, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          

          $sheet->row(6, array( 'Date : '.$date, '','Daily Diary', '' , $day));
          $sheet->row(6, function($row) { $row->setAlignment('center');} );
          $sheet->mergeCells('A7:E7');
          $sheet->row(8, array( 'Prev. Date','Type', 'Case No', 'NCV No', 'Title', 'Remark' , 'Next Date'));
          $sheet->row(8, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
          $sheet->row(9, array('', '', '', '', ''));
          $sheet->mergeCells('A9:E9');
          // $sheet->setAutoSize(true);
          
          $i=10;

            foreach ($data as $keys => $values) {

              $title = $values['reg_petitioner'].'v/s'.$values['reg_respondent'];

              $pre_date  = date('d/m/Y',strtotime($values['pessi_prev_date']));
              if($pre_date == "01/01/1970"){
                $pre_date = "-----";
              }

              $next_date = date('d/m/Y',strtotime($values['pessi_further_date']));
              if($next_date == "01/01/1970"){
                $next_date = "-----";
              }
              
              $sheet->row($i, array($pre_date, $values['court_name'], $values['reg_case_number'], $values['court_name'], $title,$values['court_name'], $next_date));
              $sheet->row($i, function($row) {$row->setAlignment('left');});

              $i++;

            }

            $add = $i+3;

            $sheet->row($i+3, array('Address :- '.$admin_details[0]->admin_address ));
            $sheet->row($i+3, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
            $sheet->mergeCells('A'.$add.':'.'E'.$add);

            $mob = $i+4;
            $sheet->row($i+4, array('Mobile No. :- '.$admin_details[0]->admin_number ));
            $sheet->row($i+4, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
            $sheet->mergeCells('A'.$mob.':'.'E'.$mob);

            $email = $i+5;
            $sheet->row($i+5, array('Email Id :- '.$admin_details[0]->admin_email ));
            $sheet->row($i+5, function($row) {$row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
            $sheet->mergeCells('A'.$email.':'.'E'.$email);

     //    die;

         });

      })->download('xlsx');








     } else {

        $arr_court_data =[];
        foreach ($data as $key => $court_data)
        {
          
            if (!in_array($court_data->court_number, $keys))
            {
                $court_data_detail = [];
            }

            $court_data_detail[]   = array(
              'pessi_id'          => $court_data->pessi_id,
              'page_number'       => $court_data->page_number,
              'case_name'         => $court_data->case_name, 
              'reg_case_number'   => $court_data->reg_case_number,
              'reg_petitioner'    => $court_data->reg_petitioner,
              'reg_respondent'    => $court_data->reg_respondent,
              'reg_power'         => $court_data->reg_power,
              'judge_name'        => $court_data->judge_name,
              'court_number'      => $court_data->court_number,
            );
            //$court_data;

            $arr_court_data[$court_data->court_number] = $court_data_detail;
           $keys = array_keys($arr_court_data);
        }

//$keys = array_keys($arr_court_data);
      // print_r($data );
      // die;

      Excel::create('Daily Diary', function($excel) use ($arr_court_data,$admin_details,$date,$day) {
        $excel->sheet('Sheet', function ($sheet) use ($arr_court_data,$admin_details,$date,$day) {

          $sheet->setOrientation('landscape');
          $sheet->mergeCells('A1:E1');
          $sheet->mergeCells('A2:E2');
          $sheet->mergeCells('A3:E3');
          $sheet->mergeCells('A4:E4');
          $sheet->mergeCells('A5:E5');
          $sheet->mergeCells('A6:B6');
          $sheet->mergeCells('C6:D6');

          $sheet->setHeight(array(
            1     =>  20,
            2     =>  20,
          ));

          $sheet->setSize('A8', 15, 18);
          $sheet->setSize('B8', 15, 18);
          $sheet->setSize('C8', 20, 18);
          $sheet->setSize('D8', 40, 18);
          $sheet->setSize('E8', 15, 18);

          $sheet->row(1, array( 'OFFICE OF' ));
          $sheet->row(1, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          $sheet->row(2, array( $admin_details[0]->admin_firmname ));
          $sheet->row(2, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          $sheet->row(3, array( $admin_details[0]->admin_advocates ));
          $sheet->row(3, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          $sheet->row(4, array( '(Advocates)' ));
          $sheet->row(4, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          

          // $day = date("l");
          // $date = date("d/m/Y");

          $sheet->row(6, array( 'Daily Diary', '', $date, '' , $day));
          $sheet->row(6, function($row) { $row->setAlignment('center');} );
          $sheet->mergeCells('A7:E7');
          $sheet->row(8, array( 'PAGE NO', 'TYPE', 'CASE NO', 'TITLE' , 'REMARK'));
          $sheet->row(8, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
          $sheet->row(9, array('', '', '', '', ''));
          $sheet->mergeCells('A9:E9');
          // $sheet->setAutoSize(true);
           
          $i=10;

          foreach ($arr_court_data as $value) {
            
            if($value[0]['judge_name'] != "" && $value[0]['court_number'] != ""){
             $court =  $value[0]['judge_name'].'( Court No. - '.$value[0]['court_number'].')';
            }else if($value[0]['judge_name'] == ""){
             $court =  '( Court No. - '.$value[0]['court_number'].')';
            }else if($value[0]['court_number'] == ""){
             $court =  $value[0]['judge_name'];
            }
              
            // print_r($court);
            // die;

              $sheet->row($i, array( $court ));
              $sheet->mergeCells('A'.$i.':'.'E'.$i);
              $sheet->row($i, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
              $sheet->getStyle('A'.$i)->getAlignment()->setWrapText(true);
              $sheet->setHeight(array(
                $i     =>  30,
              ));
              

              // foreach ($value[0]['court_number'] as $key => $value) {
              //    $sheet->row($i, array( "" ));
              //    $i++;
              // }

            foreach ($value as $keys => $values) {

              $title = $values['reg_petitioner'].'v/s'.$values['reg_respondent'];

              if($values['reg_power'] == 1){
                $power = (P);
              } else if($values['reg_power'] == 2){
                $power = (R);
              } else if($values['reg_power'] == 3){
                $power = (C);
              } else {
                $power = (N);
              }
              
              $sheet->row($i+1, array($values['page_number'], $values['case_name'], $values['reg_case_number'], $title, $power));
              $sheet->row($i+1, function($row) {$row->setAlignment('left');});


              $i++;

            }


          $i++;


         }

         $add = $i+3;

            $sheet->row($i+3, array('Address :- '.$admin_details[0]->admin_address ));
            $sheet->row($i+3, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
            $sheet->mergeCells('A'.$add.':'.'E'.$add);

            $mob = $i+4;
            $sheet->row($i+4, array('Mobile No. :- '.$admin_details[0]->admin_number ));
            $sheet->row($i+4, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
            $sheet->mergeCells('A'.$mob.':'.'E'.$mob);

            $email = $i+5;
            $sheet->row($i+5, array('Email Id :- '.$admin_details[0]->admin_email ));
            $sheet->row($i+5, function($row) {$row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
            $sheet->mergeCells('A'.$email.':'.'E'.$email);
     
         });

      })->download('xlsx');

    }
 
    }








}
