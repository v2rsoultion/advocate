<?php

namespace App\Http\Controllers\Compliance;

use Illuminate\Http\Request;
use App\Model\Case_Registration\Case_Registration; // model name
use App\Model\Compliance\Compliance; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Admin\Account; // model name
use App\Model\Case_Type_Entry\Case_Type_Entry; // model name
use App\Model\Court\Court; // model name
use App\Http\Controllers\Controller;
use DB;
use Excel;

class ComplianceController extends Controller
{

    public function __construct()
      {
          $this->middleware('Validation');
      }
      
    //add Compliance

    public function add_compliance($compliance_id = false){
      
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if(isset(json_decode($admin_details[0]->special_permission)->complaince)){
      } else {
        return Redirect()->back();
      }

      $admin_id = session('admin_id');

      if($compliance_id !=''){
        $get_record = DB::select('select * from adv_compliance where sha1(compliance_id) = "'.$compliance_id.'" and user_admin_id = "'.$admin_id.'" ');

        if(count($get_record) == 0){
          return Redirect()->back();
        }

      }else{
          $get_record = "";
      }   

      $get_case_regestered = Case_Registration::where('user_admin_id','=',$admin_details[0]->admin_id )->where(['reg_case_type_category' => 1])->get();

      $case_type_entry = Case_Registration::leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->where('adv_case_reg.user_admin_id','=',session('admin_id'))->where('case_name' ,'!=',"")->groupBy('case_id')->get();

      $get_case_regestered_all = Case_Registration::where('user_admin_id','=',$admin_details[0]->admin_id )->get();

      $get_case_number_all = Case_Registration::where('user_admin_id','=',$admin_details[0]->admin_id )->where('reg_case_number', '!=','')->get();

      $court_all  = Court::where([ 'court_status' => 1 , 'user_admin_id' => $admin_details[0]->admin_id ])->where('court_name' ,'!=', '')->get();

      // print_r($get_case_number_all);
      // die;

      $case_type_entry_all     = Case_Type_Entry::where(['user_admin_id' => session('admin_id') , 'case_status' => 1 ])->where('case_name' ,'!=',"")->get();

      $title = 'Add Compliance';
      $data  = compact('title','admin_details','get_record','get_case_regestered','case_type_entry','get_case_regestered_all','case_type_entry_all','court_all','get_case_number_all');
      return view('advocate_admin/add_compliance',$data);	
    }


    

    public function get_type_case(Request $request , $compliance_category){
      
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();
      
      if($compliance_category == 0){
        $case_type_entry = Case_Registration::leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->where('adv_case_reg.user_admin_id','=',session('admin_id'))->where('case_name' ,'!=',"")->groupBy('case_id')->get();
      } else {

        $case_type_entry = Case_Registration::leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->where('adv_case_reg.user_admin_id','=',session('admin_id'))->where(['case_category' => $compliance_category])->where('case_name' ,'!=',"")->groupBy('case_id')->get();
      }
      

      // echo "<pre>";
      // print_r($case_type_entry);
      // die;

      $data  = compact('title','admin_details','get_record','case_type_entry');
      return view('advocate_admin/get_type_case',$data);
    }


    // get court 

    public function compliance_court_type(Request $request , $court_type =false){
      
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();
      
      if($court_type == ""){
        $court_all  = Court::where([ 'court_status' => 1 , 'user_admin_id' => $admin_details[0]->admin_id ])->where('court_name' ,'!=', '')->get();
      } else {
        $court_all  = Court::where([ 'court_status' => 1 , 'user_admin_id' => $admin_details[0]->admin_id , 'court_type' => $court_type  ])->where('court_name' ,'!=', '')->get();
      }
      
      $data  = compact('title','admin_details','get_record','case_type_entry','court_all');
      return view('advocate_admin/compliance_court_type',$data);
    }

    // get_case_registeration_category

    public function get_case_registeration_category(Request $request , $compliance_category){
      
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      $explode_category = explode(',', $compliance_category);

      // print_r($compliance_category);
      // die;

      if($compliance_category != "null"){
      
      $get_case_regestered = Case_Registration::where('user_admin_id','=',$admin_details[0]->admin_id )->whereIn('reg_case_type_id' , $explode_category)->where('reg_case_number','!=','')->get();

      } else {

        $get_case_regestered = Case_Registration::where('user_admin_id','=',$admin_details[0]->admin_id )->where('reg_case_number','!=','')->get();

      }

      $data  = compact('title','admin_details','get_record','get_case_regestered');
      return view('advocate_admin/get_case_registeration_category',$data);
    }


    // compliance case type

    public function compliance_case_type(Request $request , $compliance_category , $court_name = false , $court_type = false){
      
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      
      if($court_name == "" && $court_type == "" ){
        $case_type_entry = Case_Registration::leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->where('adv_case_reg.user_admin_id','=',session('admin_id'))->where(['case_category' => $compliance_category])->where('case_name' ,'!=',"")->get();
      
      } else if($court_name == ""){
        $case_type_entry = Case_Registration::leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->where('adv_case_reg.user_admin_id','=',session('admin_id'))->where(['case_category' => $compliance_category])->where('case_name' ,'!=',"")->get();
      
      } else if($court_type == "" ){
        $case_type_entry = Case_Registration::leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->where('adv_case_reg.user_admin_id','=',session('admin_id'))->where(['case_category' => $compliance_category , 'reg_court_id' => $court_name ])->where('case_name' ,'!=',"")->get();
      
      } else if($court_name != "" && $court_type != "" ){
        $case_type_entry = Case_Registration::leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->where('adv_case_reg.user_admin_id','=',session('admin_id'))->where(['case_category' => $compliance_category , 'reg_court' => $court_type , 'reg_court_id' => $court_name ])->where('case_name' ,'!=',"")->get();
      
      } 
      
      $data  = compact('title','admin_details','get_record','case_type_entry');
      return view('advocate_admin/get_type_case',$data);
    }


    // get_case_registeration_file

    public function get_case_registeration_file(Request $request , $compliance_category){
      
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

       $explode_category = explode(',', $compliance_category);

       if($compliance_category != "null"){
      
        $get_case_regestered = Case_Registration::where('user_admin_id','=',$admin_details[0]->admin_id )->whereIn('reg_case_type_id' , $explode_category)->where('reg_file_no','!=','')->get();

      } else {

        $get_case_regestered = Case_Registration::where('user_admin_id','=',$admin_details[0]->admin_id )->where('reg_file_no','!=','')->get();

      }
      
      

      $data  = compact('title','admin_details','get_record','get_case_regestered');
      return view('advocate_admin/get_case_registeration_file',$data);
    }


    // compliance view

    public function view_compliance(Request $request, $show_type=false){
      
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if(isset(json_decode($admin_details[0]->special_permission)->complaince)){

      } else {
        return Redirect()->back();
      }
      
      // print_r($request->input());
      // die;

      if($request['check'] != ""){
        Compliance::whereIn('compliance_id', $request['check'])->delete();
        return Redirect()->back();
      }

      $query = Compliance::leftJoin('adv_case_reg', function($join) {$join->on('compliance_case_id', '=', 'reg_id');})->leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'compliance_case_type');})->leftJoin('adv_court', function($join) {$join->on('court_id', '=', 'compliance_court_name');})->where('adv_compliance.user_admin_id','=',session('admin_id'));

      if($request['court_type'] != ""){
        $query = $query->where('compliance_court_type', '=', $request['court_type']);
      }

      if($request['court_name'] != ""){
        $query = $query->where('compliance_court_name', '=', $request['court_name']);
      }

      if($request['type'] != ""){
        $query = $query->where('compliance_case_type', '=', $request['type']);
      }
      if($request['case_no'] != ""){
        $query = $query->where('compliance_case_id', '=', $request['case_no']);
      }

      if($request['reg_file_no'] != ""){
        $query = $query->where('compliance_case_id', '=', $request['reg_file_no']);
      }

      if($request['petitioner_name'] != ""){
        $query = $query->where('reg_petitioner', 'LIKE', '%'.$request['petitioner_name'].'%');
      }
      if($request['respondent'] != ""){
        $query = $query->where('reg_respondent', 'LIKE', '%'.$request['respondent'].'%');
      }

      if($request['compliance_type'] == "1"){
        $query = $query->where('compliance_type_notice', '=', 1);

        if($request['compliance_from'] != ""){
          $query = $query->whereDate('notice_issue_date', '>=', date('Y-m-d',strtotime($request['compliance_from'])) );
        }

        if($request['compliance_to'] != ""){
          $query = $query->whereDate('notice_issue_date', '<=', date('Y-m-d',strtotime($request['compliance_to'])) );
        }

      }

      if($request['compliance_type'] == "2"){
        $query = $query->where('compliance_type_date', '=', 1);

        if($request['compliance_from'] != ""){
          $query = $query->whereDate('compliance_date', '>=', date('Y-m-d',strtotime($request['compliance_from'])) );
        }

        if($request['compliance_to'] != ""){
          $query = $query->whereDate('compliance_date', '<=', date('Y-m-d',strtotime($request['compliance_to'])) );
        }
      }

      if($request['compliance_type'] == "3"){
        $query = $query->where('compliance_type_defect_case', '=', 1);

        if($request['compliance_from'] != ""){
          $query = $query->whereDate('compliance_defect_date_status', '>=', date('Y-m-d',strtotime($request['compliance_from'])) );
        }

        if($request['compliance_to'] != ""){
          $query = $query->whereDate('compliance_defect_date_status', '<=', date('Y-m-d',strtotime($request['compliance_to'])) );
        }

      }

      if($request['compliance_type'] == "4"){
        $query = $query->where('compliance_type_reply', '=', 1);

        if($request['compliance_from'] != ""){
          $query = $query->whereDate('compliance_reply_date_status', '>=', date('Y-m-d',strtotime($request['compliance_from'])) );
        }

        if($request['compliance_to'] != ""){
          $query = $query->whereDate('compliance_reply_date_status', '<=', date('Y-m-d',strtotime($request['compliance_to'])) );
        }

      }

      if($request['compliance_type'] == "5"){
        $query = $query->where('compliance_type_certified', '=', 1);

        if($request['compliance_from'] != ""){
          $query = $query->whereDate('certified_copy_date_status', '>=', date('Y-m-d',strtotime($request['compliance_from'])) );
        }

        if($request['compliance_to'] != ""){
          $query = $query->whereDate('certified_copy_date_status', '<=', date('Y-m-d',strtotime($request['compliance_to'])) );
        }
      }

      if($request['compliance_type'] == "6"){
        $query = $query->where('compliance_type_other', '=', 1);

        if($request['compliance_from'] != ""){
          $query = $query->whereDate('compliance_other_date_status', '>=', date('Y-m-d',strtotime($request['compliance_from'])) );
        }

        if($request['compliance_to'] != ""){
          $query = $query->whereDate('compliance_other_date_status', '<=', date('Y-m-d',strtotime($request['compliance_to'])) );
        }

        
      } else {

        if($request['compliance_from'] != ""){
          $query = $query->whereDate('compliance_created', '>=', date('Y-m-d',strtotime($request['compliance_from'])) );
        }

        if($request['compliance_to'] != ""){
          $query = $query->whereDate('compliance_created', '<=', date('Y-m-d',strtotime($request['compliance_to'])) );
        }


      }

      if($show_type == "all"){

      } else {

        if($request['compliance_status'] == ""){ $request['compliance_status'] = 1;}

        $compliance_status = $request['compliance_status'];

        if($request['compliance_status'] == "1"){
          $query = $query->where(function($query) use ($compliance_status) { $query->orWhere('notice_status' ,1)->orWhere('compliance_status' ,1)->orWhere('compliance_defect_status' ,1)->orWhere('compliance_reply_status' ,1)->orWhere('certified_copy_status' ,1)->orWhere('compliance_other_status' ,1) ;});
        }

        if($request['compliance_status'] == "2"){
          $query = $query->where(function($query) use ($compliance_status) { $query->orWhere('notice_status' ,2)->orWhere('compliance_status' ,2)->orWhere('compliance_defect_status' ,2)->orWhere('compliance_reply_status' ,2)->orWhere('certified_copy_status' ,2)->orWhere('compliance_other_status' ,2) ;});
        }
      }

      $app_data =  array('court_type' => $request['court_type'],'court_name' => $request['court_name'],'type' => $request['type'],'case_no' => $request['case_no'],'reg_file_no' => $request['reg_file_no'],'petitioner_name' => $request['petitioner_name'],'respondent' => $request['respondent'],'compliance_type' => $request['compliance_type'],'compliance_status' => $request['compliance_status'],'compliance_from' => $request['compliance_from'],'compliance_to' => $request['compliance_to'] );

      
      // print_r($type);
      // die;

      //  print_r(date('Y-m-d',strtotime($request['compliance_to'])));
      // die;

      if($show_type == "all"){

        $perpage    = $query->count();
        $get_record = $query->orderBy('compliance_id', 'desc')->paginate($perpage)->appends($app_data);
      } else {
        $get_record = $query->orderBy('compliance_id', 'desc')->paginate(25)->appends($app_data);
      }


      $get_case_regestered = Case_Registration::where('user_admin_id','=',$admin_details[0]->admin_id )->get();

      $case_type_entry     = Case_Type_Entry::where(['user_admin_id' => session('admin_id') ])->where('case_name','!=',"")->get();

      $court_all  = Court::where([ 'court_status' => 1 , 'user_admin_id' => $admin_details[0]->admin_id ])->where('court_name' ,'!=', '')->get();

      $court_type = $request['court_type'];
      $court_name = $request['court_name'];
      $case_no = $request['case_no'];
      $file_no = $request['reg_file_no'];
      $reg_case_type_id = $request['type'];
      $compliance_type_notice = $request['compliance_type'];
      $compliance_type_date = $request['compliance_type'];
      $compliance_type_defect_case = $request['compliance_type'];
      $compliance_type_reply = $request['compliance_type'];
      $compliance_type_certified = $request['compliance_type'];
      $compliance_type_other = $request['compliance_type'];
      $respondent = $request['respondent'];
      $petitioner_name = $request['petitioner_name'];
      $compliance_status = $request['compliance_status'];
      $compliance_from = $request['compliance_from'];
      $compliance_to = $request['compliance_to'];

      $title = 'View Compliance';
      $data  = compact('title','admin_details','get_record','case_no','file_no','get_case_regestered','case_type_entry','reg_case_type_id','compliance_type_notice','compliance_type_date','compliance_type_defect_case','compliance_type_reply','compliance_type_certified','compliance_type_other','respondent','petitioner_name','compliance_status','compliance_from','compliance_to','court_name','court_type','court_all');
      return view('advocate_admin/view_compliance',$data);
    }


    //insert function

    public function insert_compliance(Request $request, $compliance_id = false){

      // echo '<pre>'; print_r($request->input());
      // die;

      $current_date = date('Y-m-d');

      if($request['notice_status'] == "2"){ 
        if($request['notice_status_date'] == ""){ $request['notice_status_date'] = $current_date; }
      }

      if($request['compliance_status'] == "2"){ 
        if($request['compliance_date_status'] == ""){ $request['compliance_date_status'] = $current_date; }
      }

      if($request['compliance_defect_status'] == "2"){ 
        if($request['compliance_defect_date_status'] == ""){ $request['compliance_defect_date_status'] = $current_date; }
      }

      if($request['compliance_reply_status'] == "2"){ 
        if($request['compliance_reply_date_status'] == ""){ $request['compliance_reply_date_status'] = $current_date; }
      }

      if($request['certified_copy_status'] == "2"){      
        if($request['certified_copy_date_status'] == ""){ $request['certified_copy_date_status'] = $current_date; }
      }

      if($request['compliance_other_status'] == "2"){ 
        if($request['compliance_other_date_status'] == ""){ $request['compliance_other_date_status'] = $current_date; }
      }

      if($compliance_id==''){

        if($request['choose_case_type'] == "" && $request['case_no'] == ""){
          return redirect('advocate-panel/add-compliance');
        }

        $Add = new Compliance;  
      //  $Add->compliance_category           = $request['compliance_category'];
        $Add->compliance_case_type          = $request['choose_case_type'];
        $Add->compliance_court_name         = $request['court_name'];
        $Add->compliance_case_id            = $request['case_no'];
        $Add->compliance_court_type         = $request['court_type'];
        $Add->compliance_type_notice        = $request['compliance_type_notice'];
        $Add->compliance_type_date          = $request['compliance_type_date'];
        $Add->compliance_type_defect_case   = $request['compliance_type_defect_case'];
        $Add->compliance_type_reply         = $request['compliance_type_reply'];
        $Add->compliance_type_certified     = $request['compliance_type_certified'];
        $Add->compliance_type_other         = $request['compliance_type_other'];
        $Add->notice_issue_date             = date('Y-m-d',strtotime($request['notice_issue_date']));
        $Add->notice_status                 = $request['notice_status'];
        $Add->compliance_date_type          = $request['compliance_date_type'];
        $Add->compliance_date               = date('Y-m-d',strtotime($request['compliance_date']));
        $Add->compliance_status             = $request['compliance_status'];
        $Add->compliance_act_reason         = $request['compliance_act_reason'];
        $Add->compliance_defect_status      = $request['compliance_defect_status'];
        $Add->compliance_reply              = $request['compliance_reply'];
        $Add->compliance_reply_status       = $request['compliance_reply_status'];
        $Add->certified_copy_date           = date('Y-m-d',strtotime($request['certified_copy_date']));
        $Add->certified_copy_status         = $request['certified_copy_status'];
        $Add->compliance_other              = $request['compliance_other'];
        $Add->compliance_other_status       = $request['compliance_other_status'];
        $Add->user_admin_id                 = session('admin_id');

        $Add->notice_status_date            = date('Y-m-d',strtotime($request['notice_status_date']));
        $Add->compliance_date_status        = date('Y-m-d',strtotime($request['compliance_date_status']));
        $Add->compliance_defect_date_status = date('Y-m-d',strtotime($request['compliance_defect_date_status']));
        $Add->compliance_reply_date_status  = date('Y-m-d',strtotime($request['compliance_reply_date_status']));
        $Add->certified_copy_date_status    = date('Y-m-d',strtotime($request['certified_copy_date_status']));
        $Add->compliance_other_date_status  = date('Y-m-d',strtotime($request['compliance_other_date_status']));
        $Add->save();

        return redirect()->back()->with('success', 'Your Data has been added successfully');
      
      } else {

        $get_complaince = Compliance::where('compliance_id','=',$compliance_id)->first();

        // $current_date = date('Y-m-d');

        // if($get_complaince['notice_status'] != $request['notice_status']){ $notice_status_date = $current_date; } else { $notice_status_date = $get_complaince['notice_status_date']; }

        // if($get_complaince['compliance_status'] != $request['compliance_status']){ $compliance_date_status = $current_date; } else { $compliance_date_status = $get_complaince['compliance_date_status']; }

        // if($get_complaince['compliance_defect_status'] != $request['compliance_defect_status']){ $compliance_defect_date_status = $current_date; } else { $compliance_defect_date_status = $get_complaince['compliance_defect_date_status']; }

        // if($get_complaince['compliance_reply_status'] != $request['compliance_reply_status']){ $compliance_reply_date_status = $current_date; } else { $compliance_reply_date_status = $get_complaince['compliance_reply_date_status']; }

        // if($get_complaince['certified_copy_status'] != $request['certified_copy_status']){ $certified_copy_date_status = $current_date; } else { $certified_copy_date_status = $get_complaince['certified_copy_date_status']; }

        // if($get_complaince['compliance_other_status'] != $request['compliance_other_status']){ $compliance_other_date_status = $current_date; } else { $compliance_other_date_status = $get_complaince['compliance_other_date_status']; }

        $values  = array(
          'compliance_type_notice'       =>  $request['compliance_type_notice'],
          'compliance_type_date'         =>  $request['compliance_type_date'],
          'compliance_type_defect_case'  =>  $request['compliance_type_defect_case'],
          'compliance_type_reply'        =>  $request['compliance_type_reply'],
          'compliance_type_certified'    =>  $request['compliance_type_certified'],
          'compliance_type_other'        =>  $request['compliance_type_other'],
          'notice_issue_date'            =>  date('Y-m-d',strtotime($request['notice_issue_date'])),
          'notice_status'                =>  $request['notice_status'],
          'notice_status_date'           =>  date('Y-m-d',strtotime($request['notice_status_date'])),
          'compliance_date_type'         =>  $request['compliance_date_type'],
          'compliance_date'              =>  date('Y-m-d',strtotime($request['compliance_date'])),
          'compliance_status'            =>  $request['compliance_status'],
          'compliance_date_status'       =>  date('Y-m-d',strtotime($request['compliance_date_status'])),
          'compliance_act_reason'        =>  $request['compliance_act_reason'],
          'compliance_defect_status'     =>  $request['compliance_defect_status'],
          'compliance_defect_date_status'=>  date('Y-m-d',strtotime($request['compliance_defect_date_status'])),
          'compliance_reply'             =>  $request['compliance_reply'],
          'compliance_reply_status'      =>  $request['compliance_reply_status'],
          'compliance_reply_date_status' =>  date('Y-m-d',strtotime($request['compliance_reply_date_status'])),
          'certified_copy_date'          =>  date('Y-m-d',strtotime($request['certified_copy_date'])),
          'certified_copy_status'        =>  $request['certified_copy_status'],
          'certified_copy_date_status'   =>  date('Y-m-d',strtotime($request['certified_copy_date_status'])),
          'compliance_other'             =>  $request['compliance_other'],
          'compliance_other_status'      =>  $request['compliance_other_status'],
          'compliance_other_date_status' =>  date('Y-m-d',strtotime($request['compliance_other_date_status'])),
        );

        Compliance::where('compliance_id','=',$compliance_id)->update($values);

        return redirect()->back()->with('success', 'Your Data has been updated successfully');
      }

      //return redirect('advocate-panel/view-compliance');
      

    }

    //compliance status

    public function compliance_status($id,$value){

      $admin_id = session('admin_id');
      $get_record = DB::select('select * from adv_compliance where sha1(compliance_id) = "'.$id.'" and user_admin_id = "'.$admin_id.'" ');

      if(count($get_record) == 0){
        return Redirect()->back();
      }

      Compliance::where('compliance_id', $get_record[0]->compliance_id)->update(['compliance_status' => $value]);
      return Redirect()->back();
    }


    // download compliance

    public function download_compliance(Request $request){


      // print_r($request->input());
      // die;

      
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      $query = Compliance::leftJoin('adv_case_reg', function($join) {$join->on('compliance_case_id', '=', 'reg_id');})->leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'compliance_case_type');})->leftJoin('adv_court', function($join) {$join->on('court_id', '=', 'compliance_court_name');})->where('adv_compliance.user_admin_id','=',session('admin_id'));


      if($request['court_type'] != ""){
        $query = $query->where('compliance_court_type', '=', $request['court_type']);
      }

      if($request['court_name'] != ""){
        $query = $query->where('compliance_court_name', '=', $request['court_name']);
      }

      if($request['type'] != ""){
        $query = $query->where('compliance_case_type', '=', $request['type']);
      }

      if($request['regg_case_type_id'] != ""){
        $query = $query->where('compliance_case_type', '=', $request['regg_case_type_id']);
      }
      if($request['case_no'] != ""){
        $query = $query->where('compliance_case_id', '=', $request['case_no']);
      }

      if($request['file_no'] != ""){
        $query = $query->where('compliance_case_id', '=', $request['file_no']);
      }

      if($request['petitioner_name'] != ""){
        $query = $query->where('reg_petitioner', 'LIKE', '%'.$request['petitioner_name'].'%');
      }
      if($request['respondent'] != ""){
        $query = $query->where('reg_respondent', 'LIKE', '%'.$request['respondent'].'%');
      }

      if($request['compliance_type_notice'] == "1"){
        $query = $query->where('compliance_type_notice', '=', 1);
        $compliance_type_notice = 1;
      }
      if($request['compliance_type_date'] == "2"){
        $query = $query->where('compliance_type_date', '=', 1);
        $compliance_type_notice = 2;
      }
      if($request['compliance_type_defect_case'] == "3"){
        $query = $query->where('compliance_type_defect_case', '=', 1);
        $compliance_type_notice = 3;
      }
      if($request['compliance_type_reply'] == "4"){
        $query = $query->where('compliance_type_reply', '=', 1);
        $compliance_type_notice = 4;
      }
      if($request['compliance_type_certified'] == "5"){
        $query = $query->where('compliance_type_certified', '=', 1);
        $compliance_type_notice = 5;
      }
      if($request['compliance_type_other'] == "6"){
        $query = $query->where('compliance_type_other', '=', 1);
        $compliance_type_notice = 6;
      }

      $compliance_status = $request['compliance_status'];

      if($request['compliance_status'] == "1"){
        $query = $query->where(function($query) use ($compliance_status) { $query->orWhere('notice_status' ,1)->orWhere('compliance_status' ,1)->orWhere('compliance_defect_status' ,1)->orWhere('compliance_reply_status' ,1)->orWhere('certified_copy_status' ,1)->orWhere('compliance_other_status' ,1) ;});
      }

      if($request['compliance_status'] == "2"){
        $query = $query->where(function($query) use ($compliance_status) { $query->orWhere('notice_status' ,2)->orWhere('compliance_status' ,2)->orWhere('compliance_defect_status' ,2)->orWhere('compliance_reply_status' ,2)->orWhere('certified_copy_status' ,2)->orWhere('compliance_other_status' ,2) ;});
      }

      if($request['compliance_from'] != ""){
        $query = $query->where('compliance_created', '>=', date('Y-m-d',strtotime($request['compliance_from'])) );
        $date = "From:- ".$request['compliance_from'];
      }

      if($request['compliance_to'] != ""){
        $query = $query->where('compliance_created', '<=', date('Y-m-d',strtotime($request['compliance_to'])) );
        $date = "To:- ".$request['compliance_to'];
      }


      if($request['compliance_type_notice'] == 1){
        $data = $query->orderBy('notice_issue_date', 'asc')->get()->toArray();
      } else if($request['compliance_type_notice'] == 2){
        $data = $query->orderBy('compliance_date', 'asc')->get()->toArray();
      } else if($request['compliance_type_notice'] == 3){
        $data = $query->orderBy('compliance_id', 'asc')->get()->toArray();
      } else if($request['compliance_type_notice'] == 4){
        $data = $query->orderBy('compliance_id', 'asc')->get()->toArray();
      } else if($request['compliance_type_notice'] == 5){
        $data = $query->orderBy('certified_copy_date', 'asc')->get()->toArray();
      } else if($request['compliance_type_notice'] == 6){
        $data = $query->orderBy('compliance_id', 'asc')->get()->toArray();
      } else {
        $data = $query->orderBy('compliance_id', 'desc')->get()->toArray();
      }


      $compliance_type_notice = $request['compliance_type_notice'];

      if($request['compliance_from'] == "" && $request['compliance_to'] == "" ){
        $date = "All Records";
      } else if($request['compliance_from'] != "" && $request['compliance_to'] != "" ){
        $date = "From:- ".$request['compliance_from']. " To:- ".$request['compliance_to'];
      }

      // print_r($data);
      // die;

      if($request['court_type'] == 1){
        $court_name = "Trail Court";
      } else if($request['court_type'] == 2){
        $court_name = "High Court";
      } else {
        $court_name = "Both";
      }

      Excel::create('Compliance', function($excel) use ($data,$admin_details,$compliance_type_notice,$date, $court_name ) {
        $excel->sheet('Sheet', function ($sheet) use ($data,$admin_details,$compliance_type_notice,$date ,$court_name) {

          $sheet->setOrientation('landscape');
          $sheet->mergeCells('A1:E1');
          $sheet->mergeCells('A2:E2');
          $sheet->mergeCells('A3:E3');
          $sheet->mergeCells('A4:E4');
          $sheet->mergeCells('A5:E5');
          $sheet->mergeCells('A6:B6');
          $sheet->mergeCells('C6:D6');
          $sheet->setHeight(array(
            1     =>  20,
            2     =>  20,
          ));
          $sheet->setSize('A8', 10, 18);
          $sheet->setSize('B8', 25, 18);
          $sheet->setSize('C8', 20, 18);
          $sheet->setSize('D8', 15, 18);
          $sheet->setSize('E8', 30, 18);
          $sheet->setSize('F8', 15, 18);
          $sheet->setSize('G8', 15, 18);
          $sheet->setSize('H8', 15, 18);
          $sheet->setSize('I8', 15, 18);
          $sheet->setSize('J8', 15, 18);
          $sheet->setSize('K8', 25, 18);
          $sheet->setSize('L8', 15, 18);
          $sheet->setSize('M8', 15, 18);
          $sheet->setSize('N8', 15, 18);
          $sheet->setSize('O8', 20, 18);
          $sheet->setSize('P8', 15, 18);
          $sheet->setSize('Q8', 15, 18);
          $sheet->row(1, array( 'OFFICE OF' ));
          $sheet->row(1, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          $sheet->row(2, array( $admin_details[0]->admin_firmname ));
          $sheet->row(2, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          $sheet->row(3, array( $admin_details[0]->admin_advocates ));
          $sheet->row(3, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          $sheet->row(4, array( '(Advocates)' ));
          $sheet->row(4, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          $day = date("l");
        //  $date = date("d/m/Y");
          $sheet->row(6, array( $date, '','Compliance', '' , $court_name));
          $sheet->row(6, function($row) { $row->setAlignment('center');} );
          $sheet->mergeCells('A7:E7');

          if($compliance_type_notice == "" || $compliance_type_notice == "7"){
           
            $sheet->row(8, array( 'S.No.', 'Court Name' , 'Type of Case', 'Case No.' , 'Title' , 'Issue date', 'Notice Status' ,'Date Type','Date','Status','Defect Case Status','Reply','Reply Status','Apply Date','Apply Date Status','Other','Other Status'));
             
            $sheet->row(8, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
            $sheet->row(9, array('', '', '', ''));
            $sheet->mergeCells('A9:E9');
            // $sheet->setAutoSize(true);
             
            $i=10;

            if(empty($data) ){

              $sheet->mergeCells('A10:G10');
              $sheet->mergeCells('A11:G11');
              $sheet->mergeCells('A12:G12');
              $sheet->row(11, array('No Record Found'));
              $sheet->row(11, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
            } else {

             foreach ($data as $value) {

             if($value['compliance_type_notice'] == 1){
                if($value['notice_issue_date'] != "1970-01-01"){
                  $notice_issue_date  =  date('d M Y',strtotime($value['notice_issue_date']));
                } else {
                  $notice_issue_date = "-";
                }
                if($value['notice_status'] == 1) { $notice_status = "Pending"; } else { $notice_status = "Complete"; }
              } else {
                $notice_issue_date  =  "-";
                $notice_status      =  "-";
              }

              if($value['compliance_type_date'] == 1){
                if($value['compliance_date_type'] == 1) { $compliance_date_type = "Short Date"; } else { $compliance_date_type = "Long Date"; }
                if($value['compliance_date'] != "1970-01-01"){
                  $compliance_date       =  date('d M Y',strtotime($value['compliance_date']));
                } else {
                  $compliance_date = "-";
                }
                
                if($value['compliance_status'] == 1) { $compliance_status = "Pending"; } else { $compliance_status = "Complete"; }
              } else {
                $compliance_date_type   =  "-";
                $compliance_date        =  "-";
                $compliance_status      =  "-";
              }

              if($value['compliance_type_defect_case'] == 1){
                $compliance_act_reason      =  $value['compliance_act_reason'];
                if($value['compliance_defect_status'] == 1) { $compliance_defect_status = "Pending"; } else { $compliance_defect_status = "Complete"; }
              } else {
                $compliance_act_reason       =  "-";
                $compliance_defect_status    =  "-";
              }

              if($value['compliance_type_reply'] == 1){
                $compliance_reply           =  $value['compliance_reply'];
                if($value['compliance_reply_status'] == 1) { $compliance_reply_status = "Pending"; } else { $compliance_reply_status = "Complete"; }
              } else {
                $compliance_reply           =  "-";
                $compliance_reply_status    =  "-";
              }

              if($value['compliance_type_certified'] == 1){
                
                if($value['certified_copy_date'] != "1970-01-01"){
                  $certified_copy_date        =  date('d M Y',strtotime($value['certified_copy_date']));
                } else {
                  $certified_copy_date = "-";
                }

                if($value['certified_copy_status'] == 1) { $certified_copy_status = "Pending"; } else { $certified_copy_status = "Complete"; }
              } else {
                $certified_copy_date        =  "-";
                $certified_copy_status      =  "-"; 
              }

              if($value['compliance_type_other'] == 1){
                $compliance_other           =  $value['compliance_other'];
                if($value['compliance_other_status'] == 1) { $compliance_other_status = "Pending"; } else { $compliance_other_status = "Complete"; }
              } else {
                $compliance_other           =  "-";
                $compliance_other_status    =  "-";
              }

              $title_case = $value['reg_petitioner'].' v/s '.$value['reg_respondent'];

              if($value['court_name'] == ""){
                $value['court_name'] = "-";
              }

              if($value['case_name'] == ""){
                $value['case_name'] = "-";
              }

              if($value['reg_case_number'] == ""){
                $value['reg_case_number'] = "-";
              }

              $sheet->row($i, array($i-9,$value['court_name'],$value['case_name'],$value['reg_case_number'], $title_case, $notice_issue_date, $notice_status, $compliance_date_type, $compliance_date, $compliance_status, $compliance_defect_status, $compliance_reply, $compliance_reply_status, $certified_copy_date, $certified_copy_status, $compliance_other, $compliance_other_status  ));
              $sheet->row($i, function($row) {$row->setAlignment('left');});

              $i++;
             }  }
          }


          if($compliance_type_notice == "1"){
           
            $sheet->row(8, array( 'S.No.','Court Name','Type of Case','Case No.' , 'Title of Case' ,'Issue date','Notice Status'));
            $sheet->row(8, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
            $sheet->row(9, array('', '', '', ''));
            $sheet->mergeCells('A9:E9');
            // $sheet->setAutoSize(true);
             
             $i=10;
             if(empty($data) ){

              $sheet->mergeCells('A10:G10');
              $sheet->mergeCells('A11:G11');
              $sheet->mergeCells('A12:G12');
              $sheet->row(11, array('No Record Found'));
              $sheet->row(11, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
            } else {

             foreach ($data as $value) {

             if($value['compliance_type_notice'] == 1){

                if($value['notice_issue_date'] != "1970-01-01"){
                  $notice_issue_date  =  date('d M Y',strtotime($value['notice_issue_date']));
                } else {
                  $notice_issue_date = "-";
                }

                if($value['notice_status'] == 1) { $notice_status = "Pending"; } else { $notice_status = "Complete"; }
              } else {
                $notice_issue_date  =  "-";
                $notice_status      =  "-";
              }

              $title_case = $value['reg_petitioner'].' v/s '.$value['reg_respondent'];

              if($value['court_name'] == ""){
                $value['court_name'] = "-";
              }

              if($value['case_name'] == ""){
                $value['case_name'] = "-";
              }

              if($value['reg_case_number'] == ""){
                $value['reg_case_number'] = "-";
              }

              $sheet->row($i, array($i-9,$value['court_name'],$value['case_name'],$value['reg_case_number'], $title_case, $notice_issue_date, $notice_status  ));
              $sheet->row($i, function($row) {$row->setAlignment('left');});

              $i++;
             }  }
          }


          if($compliance_type_notice == "2"){
           
            $sheet->row(8, array( 'S.No.','Court Name', 'Type of Case', 'Case No.' , 'Title of Case' ,'Date Type','Date','Status'));
             
            $sheet->row(8, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
            $sheet->row(9, array('', '', '', ''));
            $sheet->mergeCells('A9:E9');
            // $sheet->setAutoSize(true);
             
             $i=10;

            if(empty($data) ){

              $sheet->mergeCells('A10:G10');
              $sheet->mergeCells('A11:G11');
              $sheet->mergeCells('A12:G12');
              $sheet->row(11, array('No Record Found'));
              $sheet->row(11, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
            } else {

             foreach ($data as $value) {

              if($value['compliance_type_date'] == 1){
                if($value['compliance_date_type'] == 1) { $compliance_date_type = "Short Date"; } else { $compliance_date_type = "Long Date"; }
                

                if($value['compliance_date'] != "1970-01-01"){
                  $compliance_date       =  date('d M Y',strtotime($value['compliance_date']));
                } else {
                  $compliance_date = "-";
                }


                if($value['compliance_status'] == 1) { $compliance_status = "Pending"; } else { $compliance_status = "Complete"; }
              } else {
                $compliance_date_type   =  "-";
                $compliance_date        =  "-";
                $compliance_status      =  "-";
              }

              $title_case = $value['reg_petitioner'].' v/s '.$value['reg_respondent'];

              if($value['court_name'] == ""){
                $value['court_name'] = "-";
              }

              if($value['case_name'] == ""){
                $value['case_name'] = "-";
              }

              if($value['reg_case_number'] == ""){
                $value['reg_case_number'] = "-";
              }

              $sheet->row($i, array($i-9,$value['court_name'],$value['case_name'],$value['reg_case_number'], $title_case, $compliance_date_type, $compliance_date, $compliance_status ));
              $sheet->row($i, function($row) {$row->setAlignment('left');});

              $i++;
             }  }
          }


          if($compliance_type_notice == "3"){
           
            $sheet->row(8, array( 'S.No.', 'Court Name', 'Type of Case', 'Case No.' , 'Title of Case' ,'Defect Case Status'));
             
            $sheet->row(8, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
            $sheet->row(9, array('', '', '', ''));
            $sheet->mergeCells('A9:E9');
            // $sheet->setAutoSize(true);
             
             $i=10;

            if(empty($data) ){

              $sheet->mergeCells('A10:G10');
              $sheet->mergeCells('A11:G11');
              $sheet->mergeCells('A12:G12');
              $sheet->row(11, array('No Record Found'));
              $sheet->row(11, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
            } else {

             foreach ($data as $value) {

              if($value['compliance_type_defect_case'] == 1){
                $compliance_act_reason      =  $value['compliance_act_reason'];
                if($value['compliance_defect_status'] == 1) { $compliance_defect_status = "Pending"; } else { $compliance_defect_status = "Complete"; }
              } else {
                $compliance_act_reason       =  "-";
                $compliance_defect_status    =  "-";
              }

              $title_case = $value['reg_petitioner'].' v/s '.$value['reg_respondent'];

              if($value['court_name'] == ""){
                $value['court_name'] = "-";
              }

              if($value['case_name'] == ""){
                $value['case_name'] = "-";
              }

              if($value['reg_case_number'] == ""){
                $value['reg_case_number'] = "-";
              }

              $sheet->row($i, array($i-9,$value['court_name'],$value['case_name'],$value['reg_case_number'], $title_case, $compliance_defect_status ));
              $sheet->row($i, function($row) {$row->setAlignment('left');});

              $i++;
             }
          }  }


          if($compliance_type_notice == "4"){
           
            $sheet->row(8, array( 'S.No.', 'Court Name', 'Type of Case', 'Case No.' , 'Title of Case' , 'Reply','Reply Status'));
             
            $sheet->row(8, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
            $sheet->row(9, array('', '', '', ''));
            $sheet->mergeCells('A9:E9');
            // $sheet->setAutoSize(true);
             
             $i=10;

            if(empty($data) ){

              $sheet->mergeCells('A10:G10');
              $sheet->mergeCells('A11:G11');
              $sheet->mergeCells('A12:G12');
              $sheet->row(11, array('No Record Found'));
              $sheet->row(11, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
            } else {

             foreach ($data as $value) {

              if($value['compliance_type_reply'] == 1){
                $compliance_reply           =  $value['compliance_reply'];
                if($value['compliance_reply_status'] == 1) { $compliance_reply_status = "Pending"; } else { $compliance_reply_status = "Complete"; }
              } else {
                $compliance_reply           =  "-";
                $compliance_reply_status    =  "-";
              }

              $title_case = $value['reg_petitioner'].' v/s '.$value['reg_respondent'];

              if($value['court_name'] == ""){
                $value['court_name'] = "-";
              }

              if($value['case_name'] == ""){
                $value['case_name'] = "-";
              }

              if($value['reg_case_number'] == ""){
                $value['reg_case_number'] = "-";
              }

              $sheet->row($i, array($i-9,$value['court_name'],$value['case_name'],$value['reg_case_number'], $title_case, $compliance_reply, $compliance_reply_status  ));
              $sheet->row($i, function($row) {$row->setAlignment('left');});

              $i++;
             }  }
          }


          if($compliance_type_notice == "5"){
           
            $sheet->row(8, array( 'S.No.','Court Name',  'Type of Case', 'Case No.' , 'Title of Case' , 'Apply Date','Apply Date Status'));
             
            $sheet->row(8, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
            $sheet->row(9, array('', '', '', ''));
            $sheet->mergeCells('A9:E9');
            // $sheet->setAutoSize(true);
             
             $i=10;

            if(empty($data) ){

              $sheet->mergeCells('A10:G10');
              $sheet->mergeCells('A11:G11');
              $sheet->mergeCells('A12:G12');
              $sheet->row(11, array('No Record Found'));
              $sheet->row(11, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
            } else {

             foreach ($data as $value) {

              if($value['compliance_type_certified'] == 1){
                

                if($value['certified_copy_date'] != "1970-01-01"){
                  $certified_copy_date        =  date('d M Y',strtotime($value['certified_copy_date']));
                } else {
                  $certified_copy_date = "-";
                }

                if($value['certified_copy_status'] == 1) { $certified_copy_status = "Pending"; } else { $certified_copy_status = "Complete"; }
              } else {
                $certified_copy_date        =  "-";
                $certified_copy_status      =  "-"; 
              }

              $title_case = $value['reg_petitioner'].' v/s '.$value['reg_respondent'];

              if($value['court_name'] == ""){
                $value['court_name'] = "-";
              }

              if($value['case_name'] == ""){
                $value['case_name'] = "-";
              }

              if($value['reg_case_number'] == ""){
                $value['reg_case_number'] = "-";
              }

              $sheet->row($i, array($i-9,$value['court_name'],$value['case_name'],$value['reg_case_number'], $title_case, $certified_copy_date, $certified_copy_status ));
              $sheet->row($i, function($row) {$row->setAlignment('left');});

              $i++;
             }  }
          }



          if($compliance_type_notice == "6"){
           
            $sheet->row(8, array( 'S.No.','Court Name',  'Type of Case', 'Case No.' , 'Title of Case' , 'Other','Other Status'));
             
            $sheet->row(8, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
            $sheet->row(9, array('', '', '', ''));
            $sheet->mergeCells('A9:E9');
            // $sheet->setAutoSize(true);
             
             $i=10;

            if(empty($data) ){

              $sheet->mergeCells('A10:G10');
              $sheet->mergeCells('A11:G11');
              $sheet->mergeCells('A12:G12');
              $sheet->row(11, array('No Record Found'));
              $sheet->row(11, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
            } else {

             foreach ($data as $value) {

              if($value['compliance_type_other'] == 1){
                $compliance_other           =  $value['compliance_other'];
                if($value['compliance_other_status'] == 1) { $compliance_other_status = "Pending"; } else { $compliance_other_status = "Complete"; }
              } else {
                $compliance_other           =  "-";
                $compliance_other_status    =  "-";
              }


              $title_case = $value['reg_petitioner'].' v/s '.$value['reg_respondent'];

              if($value['court_name'] == ""){
                $value['court_name'] = "-";
              }

              if($value['case_name'] == ""){
                $value['case_name'] = "-";
              }

              if($value['reg_case_number'] == ""){
                $value['reg_case_number'] = "-";
              }

              $sheet->row($i, array($i-9,$value['court_name'],$value['case_name'],$value['reg_case_number'], $title_case, $compliance_other, $compliance_other_status  ));
              $sheet->row($i, function($row) {$row->setAlignment('left');});

              $i++;
             }  }
          }



          $add = $i+3;

          $sheet->row($i+3, array('Address :- '.$admin_details[0]->admin_address ));
          $sheet->row($i+3, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
          $sheet->mergeCells('A'.$add.':'.'E'.$add);

          $mob = $i+4;
          $sheet->row($i+4, array('Mobile No. :- '.$admin_details[0]->admin_number ));
          $sheet->row($i+4, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
          $sheet->mergeCells('A'.$mob.':'.'E'.$mob);

          $email = $i+5;
          $sheet->row($i+5, array('Email Id :- '.$admin_details[0]->admin_email ));
          $sheet->row($i+5, function($row) {$row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
          $sheet->mergeCells('A'.$email.':'.'E'.$email);

         });

      })->download('xlsx');


    }



}





