<?php

namespace App\Http\Controllers\Certified_Copy_Date;

use Illuminate\Http\Request;
use App\Model\Certified_Copy_Date\Certified_Copy_Date; // model name
use App\Model\Case_Registration\Case_Registration; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Admin\Account; // model name
use App\Http\Controllers\Controller;
use DB;
class CertifiedcopydateController extends Controller
{

    public function __construct()
      {
          $this->middleware('Validation');
      }
      
    //add certified copy date

    public function add_certified_copy_date($ccd_id = false){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();
      $admin_id = session('admin_id');

      if($ccd_id!=''){

          $get_record = DB::select('select * from adv_certified_copy_date where sha1(ccd_id) = "'.$ccd_id.'" and user_admin_id = "'.$admin_id.'" ');

          if(count($get_record) == 0){
            return Redirect()->back();
          }

          $defect_case_no = Case_Registration::where('reg_status','=',1)->get();          
          $title = 'Add Certified Copy Date';
          $data  = compact('title','admin_details','get_record','defect_case_no');
          return view('advocate_admin/add_certified_copy_date',$data);
      }else{
          $get_record = "";
      }
      $defect_case_no = Case_Registration::where('reg_status','=',1)->get();          
      $title = 'Add Certified Copy Date';
      $data  = compact('title','admin_details','get_record','defect_case_no');
      return view('advocate_admin/add_certified_copy_date',$data);	
    }


    // certified case no ajax function
    public function certified_case_no_ajax($certified_caseno_id){
      $arrayName = Case_Registration::where('reg_id','=',$certified_caseno_id)->first();
      return response()->json(['petitioner'=>$arrayName['reg_petitioner'],'respondent'=>$arrayName['reg_respondent']]);
    }


    //view certified copy date

    public function view_certified_copy_date(Request $request){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();
      
      if($request['check'] != ""){
        Certified_Copy_Date::whereIn('ccd_id', $request['check'])->delete();
        return Redirect()->back();
      }

      $query = Certified_Copy_Date::leftJoin('adv_case_reg', function($join) {$join->on('reg_id','=','ccd_case_no');})->where('adv_certified_copy_date.user_admin_id','=',session('admin_id'));

      if($request['ccd_case_no'] != ""){
        $query = $query->where('ccd_case_no', 'LIKE', '%'.$request['ccd_case_no'].'%');
      }
      if($request['ccd_title'] != ""){
        $query = $query->where('ccd_title', 'LIKE', '%'.$request['ccd_title'].'%');
      }
      if($request['ccd_date'] != ""){
        $query = $query->where('ccd_date', 'LIKE', '%'.$request['ccd_date'].'%');
      }

      $app_data =  array('ccd_case_no' => $request['ccd_case_no'],'ccd_title' => $request['ccd_title'],'ccd_date' => $request['ccd_date']);
      $get_record = $query->orderBy('ccd_id', 'desc')->paginate(10)->appends($app_data);      
      $title = 'View certified copy date';
      $data  = compact('title','admin_details','get_record');
      return view('advocate_admin/view_certified_copy_date',$data);	
    }

    //insert certified copy date function

    public function insert_certified_copy_date(Request $request, $ccd_id = false){

      $myDate = date('Y-m-d',strtotime($request['date_picker']));
      if($ccd_id==''){
      $Add = new Certified_Copy_Date;  
      $Add->ccd_title             = $request['title'];
      $Add->ccd_case_no           = $request['case_no'];
      $Add->ccd_status_date       = $request['status_date_s'];
      $Add->user_admin_id         = session('admin_id');      
      $Add->ccd_date              = $myDate;
      $Add->ccd_status            = 1;
      $Add->save();
      return redirect('advocate-panel/view-certified-copy-date');
    }
    else
    {
      $values  = array(
        'ccd_title'               =>  $request['title'],
        'ccd_case_no'             =>  $request['case_no'],
        'ccd_status_date'         =>  $request['status_date_s'],
        'ccd_date'                =>  $myDate
        );

      Certified_Copy_Date::where('ccd_id','=',$ccd_id)->update($values);
      return redirect('advocate-panel/view-certified-copy-date');
    }

  }

    //certified copy date status

    public function certified_copy_date_status($id,$value){
      Certified_Copy_Date::where('ccd_id', $id)->update(['ccd_status' => $value]);
      return Redirect()->back();
    }
}
