<?php

namespace App\Http\Controllers\Class_Code;

use Illuminate\Http\Request;
use App\Model\Class_Code\Class_Code; // model name
use App\Model\Case_Type_Entry\Case_Type_Entry; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Admin\Account; // model name
use App\Http\Controllers\Controller;
use DB;
class ClasscodeController extends Controller
{

    public function __construct()
      {
          $this->middleware('Validation');
      }
      
    //add case sub type

    public function add_class_code($class_code_id = false){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if(isset(json_decode($admin_details[0]->special_permission)->master_modules)){

      } else {
        return Redirect()->back();
      }

      $admin_id = session('admin_id');
      
      if($class_code_id!=''){
        $get_record = DB::select('select * from adv_class_code where sha1(classcode_id) = "'.$class_code_id.'" and user_admin_id = "'.$admin_id.'" ');

          if(count($get_record) == 0){
            return Redirect()->back();
          }
          $case_type_entry = Case_Type_Entry::where('case_status','=',1)->where('case_name','!=',"")->where('user_admin_id','=',session('admin_id'))->get();      

          $title = 'Add Class Code';
          $data  = compact('title','admin_details','get_record','case_type_entry');
          return view('advocate_admin/add_class_code',$data);

      }else{
          $get_record = "";
      }
      $case_type_entry = Case_Type_Entry::where('case_status','=',1)->where('case_name','!=',"")->where('user_admin_id','=',session('admin_id'))->get();      
      $title = 'Add Class Code';
      $data  = compact('title','admin_details','get_record','case_type_entry');
      return view('advocate_admin/add_class_code',$data);	
    }

    //view case sub type

    public function view_class_code(Request $request,$type=false){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if(isset(json_decode($admin_details[0]->special_permission)->master_modules)){

      } else {
        return Redirect()->back();
      }

      if($request['check'] != ""){
        Class_Code::whereIn('classcode_id', $request['check'])->delete();
        return Redirect()->back();
      }
      
      $query = Class_Code::leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'subtype_case_id');})->where('adv_class_code.user_admin_id','=',session('admin_id'));

      if($request['subtype_case'] != ""){
        $query = $query->where('subtype_case_id', '=', $request['subtype_case']);
      }
      if($request['sub_type_name'] != ""){
        $query = $query->where('classcode_name', 'LIKE', '%'.$request['sub_type_name'].'%');
      }
      if($request['subtype_code'] != ""){
        $query = $query->where('classcode_code', 'LIKE', '%'.$request['subtype_code'].'%');
      }
      if($request['subtype_short_name'] != ""){
        $query = $query->where('classcode_short_name', 'LIKE', '%'.$request['subtype_short_name'].'%');
      }

      $app_data =  array('sub_type_name' => $request['sub_type_name'],'subtype_case' => $request['subtype_case'],'subtype_code' => $request['subtype_code'],'subtype_short_name' => $request['subtype_short_name']);

      if($type == "all"){
        $perpage    = $query->count();
        $get_record = $query->orderBy('classcode_id', 'desc')->paginate($perpage)->appends($app_data); 
      } else {
        $get_record = $query->orderBy('classcode_id', 'desc')->paginate(25)->appends($app_data);  
      }

      

      $case_type_entry = Case_Type_Entry::where('case_status','=',1)->where('user_admin_id','=',session('admin_id'))->where('case_name','!=',"")->get();
      $subtype_case = $request['subtype_case'];
      $title = 'View Class Code';
      $data  = compact('title','admin_details','get_record','case_type_entry','subtype_case');
      return view('advocate_admin/view_class_code',$data);	
    }

    //insert case sub type function

    public function insert_class_code(Request $request, $class_code_id = false){

    if($class_code_id==''){

      if($request['choose_case_type'] == "" || $request['sub_type_name'] == ""){
          return redirect('advocate-panel/add-class-code');
      }

      $get_class_code = Class_Code::where('user_admin_id','=',session('admin_id'))->where('subtype_case_id','=',$request['choose_case_type'])->where('classcode_name','=',$request['sub_type_name'])->count();

      if($get_class_code != 0){
        return redirect()->back()->with('danger', 'Class code with same name already exit !!');
      }

      
      $Add = new Class_Code;  
      $Add->subtype_case_id           = $request['choose_case_type'];
      $Add->classcode_name            = $request['sub_type_name'];
      $Add->classcode_short_name      = $request['short_name'];
      $Add->classcode_code            = $request['code'];
      $Add->user_admin_id             = session('admin_id');
      $Add->classcode_description     = $request['description'];
      $Add->classcode_status          = 1;
      $Add->save();
      //return redirect('advocate-panel/view-class-code');
       return redirect()->back()->with('success', 'Your Data has been added successfully');
    }
    else
    {

      $get_class_code = Class_Code::where('user_admin_id','=',session('admin_id'))->where('classcode_id','!=',$class_code_id)->where('subtype_case_id','=',$request['choose_case_type'])->where('classcode_name','=',$request['sub_type_name'])->count();

      if($get_class_code != 0){
        return redirect()->back()->with('danger', 'Class code with same name already exit !!');
      }


      $values  = array(
        'subtype_case_id'           =>  $request['choose_case_type'],
        'classcode_name'            =>  $request['sub_type_name'],
        'classcode_short_name'      =>  $request['short_name'],
        'classcode_code'            =>  $request['code'],
        'classcode_description'     =>  $request['description']
        );
      Class_Code::where('classcode_id','=',$class_code_id)->update($values);
      //return redirect('advocate-panel/view-class-code');
       return redirect()->back()->with('success', 'Your Data has been updated successfully');
    }

  }

    //court sub type status

    public function class_code_status($id,$value){

      $admin_id = session('admin_id');
      $get_record = DB::select('select * from adv_class_code where sha1(classcode_id) = "'.$id.'" and user_admin_id = "'.$admin_id.'" ');

      if(count($get_record) == 0){
        return Redirect()->back();
      }

      Class_Code::where('classcode_id', $get_record[0]->classcode_id)->update(['classcode_status' => $value]);
      return Redirect()->back();
    }
}
