<?php

namespace App\Http\Controllers\Date;

use Illuminate\Http\Request;
use App\Model\Date\Date; // model name
use App\Model\Case_Registration\Case_Registration; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Admin\Account; // model name
use App\Http\Controllers\Controller;
use DB;
class DateController extends Controller
{
    public function __construct()
      {
          $this->middleware('Validation');
      }
      
    //add date

    public function add_date($cdate_id = false){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();
      $admin_id = session('admin_id');

      if($cdate_id!=''){

          $get_record = DB::select('select * from adv_comp_date where sha1(cdate_id) = "'.$cdate_id.'" and user_admin_id = "'.$admin_id.'" ');

          if(count($get_record) == 0){
            return Redirect()->back();
          }

          $date_case_no = Case_Registration::where('reg_status','=',1)->get();       
          $title        = 'Add Date';
          $data         = compact('title','admin_details','get_record','date_case_no');
          return view('advocate_admin/add_date',$data);
      }else{
          $get_record = "";
      }
          $date_case_no = Case_Registration::where('reg_status','=',1)->get();       
          $title        = 'Add Date';
          $data         = compact('title','admin_details','get_record','date_case_no');
          return view('advocate_admin/add_date',$data);	
    }

    

    public function date_case_no_ajax($id_date_change){
      $arraydate = Case_Registration::where('reg_id','=',$id_date_change)->first();
      return response()->json(['petitioner'=>$arraydate['reg_petitioner'],'respondent'=>$arraydate['reg_respondent']]);
    }


    //view date

    public function view_date(Request $request){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if($request['check'] != ""){
        Date::whereIn('cdate_id', $request['check'])->delete();
        return Redirect()->back();
      }
      
      $query = Date::leftJoin('adv_case_reg', function($join) {$join->on('reg_id', '=', 'cdate_case_no');})->where('adv_comp_date.user_admin_id','=',session('admin_id'));

      if($request['cdate_case_no'] != ""){
        $query = $query->where('cdate_case_no', 'LIKE', '%'.$request['cdate_case_no'].'%');
      }
      if($request['cdate_title'] != ""){
        $query = $query->where('cdate_title', 'LIKE', '%'.$request['cdate_title'].'%');
      }
      if($request['cdate_type'] != ""){
        $query = $query->where('cdate_type', 'LIKE', '%'.$request['cdate_type'].'%');
      }
      if($request['cdate_date'] != ""){
        $query = $query->where('cdate_date', 'LIKE', '%'.$request['cdate_date'].'%');
      }

      $app_data =  array('cdate_case_no' => $request['cdate_case_no'],'cdate_title' => $request['cdate_title'],'cdate_type' => $request['cdate_type'],'cdate_date' => $request['cdate_date']);
      $get_record = $query->orderBy('cdate_id', 'desc')->paginate(10)->appends($app_data);      
      $title = 'View Date';
      $data  = compact('title','admin_details','get_record');
      return view('advocate_admin/view_date',$data);	
    }

    //insert date function

    public function insert_date(Request $request, $cdate_id = false){

      $myDate = date('Y-m-d',strtotime($request['date_date']));
      if($cdate_id==''){
      $Add = new Date;  
      $Add->cdate_case_no         = $request['case_no'];
      $Add->cdate_type            = $request['date'];
      $Add->cstatus_date          = $request['status_date_s'];
      // if($request['status_date_s'] == 2){
      // $Add->cdate_pic        = date('Y-m-d',strtotime($request['status_date']));
      // }
      $Add->user_admin_id         = session('admin_id');
      $Add->cdate_date            = $myDate;
      $Add->cdate_status          = 1;
      $Add->save();
      return redirect('advocate-panel/view-date');
    }
    else
    {
      // if($request['status_date_s'] == 2){
      //   $date_status = date('Y-m-d',strtotime($request['status_date']));
      // }else{
      //   $date_status = '0000-00-00';
      // }
      $values  = array(
        'cdate_case_no'         =>  $request['case_no'],
        'cdate_type'            =>  $request['date'],
        'cstatus_date'          =>  $request['status_date_s'],
        // 'cdate_pic'             =>  $date_status,
        'cdate_date'            =>  $myDate
        );

      Date::where('cdate_id','=',$cdate_id)->update($values);
      return redirect('advocate-panel/view-date');
    }

  }

    //date status

    public function date_status($id,$value){
      Date::where('cdate_id', $id)->update(['cdate_status' => $value]);
      return Redirect()->back();
    }
}
