<?php

namespace App\Http\Controllers\Judge;

use Illuminate\Http\Request;
use App\Model\Judge\Judge; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Admin\Account; // model name
use App\Http\Controllers\Controller;
use DB;
class JudgeController extends Controller
{

    public function __construct()
      {
          $this->middleware('Validation');
      }
      
    //add judge

    public function add_judge($judge_id = false){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if(isset(json_decode($admin_details[0]->special_permission)->master_modules)){

      } else {
        return Redirect()->back();
      }

      $admin_id = session('admin_id');
      
      if($judge_id!=''){

          $get_record = DB::select('select * from adv_judge where sha1(judge_id) = "'.$judge_id.'" and user_admin_id = "'.$admin_id.'" ');

          if(count($get_record) == 0){
            return Redirect()->back();
          }

          $title = 'Add Judge';
          $data  = compact('title','admin_details','get_record');
          return view('advocate_admin/add_judge',$data);
        
        }else{
          $get_record = "";
        }       
          $title = 'Add Judge';
          $data  = compact('title','admin_details','get_record');
          return view('advocate_admin/add_judge',$data);
        }

    //view judge

    public function view_judge(Request $request,$type=false){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if(isset(json_decode($admin_details[0]->special_permission)->master_modules)){

      } else {
        return Redirect()->back();
      }
      
      $query = Judge::where('user_admin_id','=',session('admin_id'));

      if($request['check'] != ""){
        Judge::whereIn('judge_id', $request['check'])->delete();
        return Redirect()->back();
      }

      if($request['judge_name'] != ""){
        $query = $query->where('judge_name', 'LIKE', '%'.$request['judge_name'].'%');
      }

      $app_data =  array('judge_name' => $request['judge_name']);

      if($type == "all"){
        $perpage    = $query->count();
        $get_record = $query->orderBy('judge_id', 'desc')->paginate($perpage)->appends($app_data);
      } else {
        $get_record = $query->orderBy('judge_id', 'desc')->paginate(25)->appends($app_data);
      }

            
      $title = 'View Judge';
      $data  = compact('title','admin_details','get_record');
      return view('advocate_admin/view_judge',$data);	
    }

    //insert judge function

    public function insert_judge(Request $request, $judge_id = false){

      if($judge_id==''){

        if($request['judge_name'] == ""){
          return redirect('advocate-panel/add-judge');
        }

        $Add = new Judge;  
        $Add->judge_name            = $request['judge_name'];
        $Add->user_admin_id         = session('admin_id');
        $Add->judge_status          = 1;
        $Add->save();
        //return redirect('advocate-panel/view-judge');
        return redirect()->back()->with('success', 'Your Data has been added successfully');
    }
    else
    {
      $values  = array(
        'judge_name'             =>  $request['judge_name']
        );
      Judge::where('judge_id','=',$judge_id)->update($values);
      //return redirect('advocate-panel/view-judge');
      return redirect()->back()->with('success', 'Your Data has been updated successfully');
    }

  }

    //judge status

    public function judge_status($id,$value){

      $admin_id = session('admin_id');
      $get_record = DB::select('select * from adv_judge where sha1(judge_id) = "'.$id.'" and user_admin_id = "'.$admin_id.'" ');

      if(count($get_record) == 0){
        return Redirect()->back();
      }

      Judge::where('judge_id', $get_record[0]->judge_id)->update(['judge_status' => $value]);
      return Redirect()->back();
    }
}
