<?php

namespace App\Http\Controllers\Client_Sub_Type_ajax;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Case_Sub_Type\Case_Sub_Type; // model name
use App\Model\Case_Type_Entry\Case_Type_Entry; // model name
use App\Model\Case_Registration\Case_Registration; // model name
use App\Model\Class_Code\Class_Code; // model name
use App\Model\Section\Section; // model name
use App\Model\Notice_Issue\Notice_Issue; // model name
use App\Model\Client\Client; // model name
use App\Model\Sub_Client\Sub_Client; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Court\Court; // model name
use DB;

class ClientsubtypeajaxController extends Controller{

    public function __construct()
    {
        $this->middleware('Validation');
    }
    
    //client sub type change ajax function

    public function client_sub_type_ajax($id = false){

      $admin_id = session('admin_id');

      if($id == ""){
        $class_code_ajax = Class_Code::where(['classcode_status' => 1 , 'user_admin_id' => $admin_id ])->where('classcode_name' ,'!=', '')->get();
      } else {
        $class_code_ajax = Class_Code::where('subtype_case_id','=',$id)->where(['classcode_status' => 1 , 'user_admin_id' => $admin_id ])->where('classcode_name' ,'!=', '')->get();  
      }

      
      $data  = compact('class_code_ajax');
      return view('advocate_admin/client_sub_type_ajax',$data);	
    
    }

    // get_case_category

    public function get_case_category($id = false){

      $admin_id = session('admin_id');

      $class_code_ajax = Case_Type_Entry::where('case_id','=',$id)->where(['case_status' => 1 , 'user_admin_id' => $admin_id ])->first();

      if($class_code_ajax->case_category == 1 ){
        return response()->json(['status'=>true,'message'=>'Invalid Token']);
      } else {
        return response()->json(['status'=>false,'message'=>'Invalid Token']);
      }
          
    }

    // check_case_number
    
    public function check_case_number($case_number = false , $case_type = false , $category_type = false){

      $admin_id = session('admin_id');

      $get_record = Case_Registration::where(['reg_case_number' => $case_number, 'reg_case_type_category' => $category_type , 'reg_case_type_id' => $case_type , 'user_admin_id' => $admin_id ])->count();

      if($get_record == 1 ){
        return response()->json(['status'=>true,'message'=>'Invalid Token']);
      } else {
        return response()->json(['status'=>false,'message'=>'Invalid Token']);
      }
          
    }


    //act and section ajax function

    public function act_sub_type_section($id = false){
      $admin_id = session('admin_id');

      $act_type_change = Section::where('section_act_id','=',$id)->where(['section_status' => 1 , 'user_admin_id' => $admin_id ])->where('section_name' ,'!=', '')->get();
      $data  = compact('act_type_change');
      return view('advocate_admin/act_type_change_ajax',$data);	
    }


    //client sub group change type ajax function

    public function sub_group_change_ajax($id = false){
      $admin_id = session('admin_id');

      $get_client = Client::where('cl_id','=',$id)->where(['cl_status' => 1 , 'user_admin_id' => $admin_id ])->first();

      if($get_client->cl_group_type == 2){

        $sub_group_change = Sub_Client::where('client_id','=',$id)->where(['sub_client_status' => 1 , 'user_admin_id' => $admin_id ])->where('sub_client_name' ,'!=', '')->get();
        $data  = compact('sub_group_change');
        return view('advocate_admin/client_sub_group_ajax',$data);	
      } else {
        return response()->json(['status'=>true,'message'=>'Invalid Token']); 
      }
    }


    //notice issue case no ajax function

    public function notice_case_no_change($id = false){

      $notice_case_no_change = Notice_Issue::where('notice_id','=',$id)->get();
      $data  = compact('notice_case_no_change');
      return view('advocate_admin/notice_case_no_ajax',$data);  
    }


    // get all court 

    public function get_all_court($court_type = false){

      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();
      $admin_id = session('admin_id');
      

      if($court_type == 3) {
        $high_court        = Court::where([ 'court_status' => 1 , 'user_admin_id' => $admin_id ])->where('court_name' ,'!=', '')->get();
      } else {
        $high_court        = Court::where([ 'court_status' => 1 , 'user_admin_id' => $admin_id , 'court_type' => $court_type ])->where('court_name' ,'!=', '')->get();  
      }
      


      // print_r($high_court);
      // die;

      $data  = compact('high_court');
      return view('advocate_admin/court_ajax',$data);  
    }


    public function get_all_clients(Request $request){
      $admin_id = session('admin_id');

      // if($request['client_type'] == ""){
      //   $get_client = Client::where('cl_group_type','=',1)->where('cl_group_name','!=',"")->where('cl_group_name','LIKE','%'.$request['client_name'].'%')->where(['cl_status' => 1 , 'user_admin_id' => $admin_id ])->get();
      // } else {
        $get_client = Client::where('cl_group_type','=',$request['client_type'])->where('cl_group_name','!=',"")->where('cl_group_name','LIKE','%'.$request['client_name'].'%')->where(['cl_status' => 1 , 'user_admin_id' => $admin_id ])->get();
      //}

      // print_r($request->input());
      // die;
      $data  = compact('get_client');
      
      return view('advocate_admin/get_all_client_ajax',$data);  
      
    }


}
