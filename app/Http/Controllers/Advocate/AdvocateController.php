<?php

namespace App\Http\Controllers\Advocate;

use Illuminate\Http\Request;
use App\Model\Admin\Admin; // model name
use App\Model\Admin\Account; // model name
use App\Http\Controllers\Controller;
use Mail;

class AdvocateController extends Controller
{

  public function __construct(){
      $this->middleware('Validation');
  }
    //add advocate

    public function add_advocate($advocate_id = false){
      
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();


      if($admin_details[0]->admin_type == 2){
        return Redirect()->back();
      }
      
      if($advocate_id!=''){
          $get_record = Admin::where('admin_id','=',$advocate_id)->first();
      }else{
          $get_record = "";
      }

      $title = 'Add Advocate';
      $data  = compact('title','admin_details','get_record');
      return view('advocate_admin/add_advocate',$data);	
    }

    //view advocate

    public function view_advocate(Request $request,$type = false){
      
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if($admin_details[0]->admin_type == 2){
        return Redirect()->back();
      }

      if($request['check'] != ""){
        Admin::whereIn('admin_id', $request['check'])->delete();
        return Redirect()->back();
      }

      $query = Admin::query();

      if($request['admin_name'] != ""){
        $query = $query->where('admin_name', 'LIKE', '%'.$request['admin_name'].'%');
      }
      if($request['admin_email'] != ""){
        $query = $query->where('admin_email', 'LIKE', '%'.$request['admin_email'].'%');
      }
      if($request['admin_number'] != ""){
        $query = $query->where('admin_number', 'LIKE', '%'.$request['admin_number'].'%');
      }

      $app_data =  array('admin_name' => $request['admin_name'],'admin_email' => $request['admin_email'],'admin_number' => $request['admin_number']);
         

      if($type == "all"){
        $perpage    = $query->count();
        $get_record = $query->where(['admin_type' => 2])->orderBy('admin_id', 'desc')->paginate($perpage)->appends($app_data);
      } else {
        $get_record = $query->where(['admin_type' => 2])->orderBy('admin_id', 'desc')->paginate(25)->appends($app_data);
      }


      $title = 'View Advocate';
      $data  = compact('title','admin_details','get_record');
      return view('advocate_admin/view_advocate',$data);	
    }

    //insert advocate function

    public function insert_advocate(Request $request, $advocate_id = false){
 
      if($advocate_id==''){

        $get_record = Admin::where('admin_email','=', $request['email'] )->count();

        if($get_record != 0){
          return redirect()->back()->with('success', 'An account for the specified email address already exists.')->withInput();
        }


        // print_r($request->input());
        // die;

        $input = array(
          'case_registration'       => $request['case_registration'], 
          'pessi_cause_list'        => $request['pessi_cause_list'], 
          'daily_cause_list'        => $request['daily_cause_list'], 
          'sms_to_clients'          => $request['sms_to_clients'], 
          'order_judgement_upload'  => $request['order_judgement_upload'], 
          'complaince'              => $request['complaince'], 
          'calendar'                => $request['calendar'], 
          'undated_case'            => $request['undated_case'], 
          'reporting'               => $request['reporting'], 
          'due_course'              => $request['due_course'], 
          'master_modules'          => $request['master_modules']
        );

        $special_permission = json_encode(array_filter($input));

        $Add = new Admin;  
        $Add->admin_name            = $request['name'];
        $Add->admin_email           = $request['email'];
        $Add->admin_number          = $request['number'];
        $Add->admin_address         = $request['address'];
        $Add->start_date            = date('Y-m-d',strtotime($request['start_date']));
        $Add->end_date              = date('Y-m-d',strtotime($request['end_date']));
        $Add->admin_password        = md5($request['password']);
        $Add->admin_type            = 2;
        $Add->special_permission    = $special_permission;
        $Add->save();
        $admin_id = $Add->id;

        $file = $request->file('admin_image');
        if($file != ""){
          $destinationPath = 'uploads/admin/';
          $imageName = 'uploads/admin/'.time().'.'.$file->getClientOriginalExtension();
          $file->move($destinationPath,$imageName);
          $image=array(
              'admin_image'         => $imageName
          );
          Admin::where('admin_id', $admin_id)->update($image);
        }


        $data=['name'=> $request->name , 'email'=> $request->email, 'password'=> $request->password, 'start_date'=> date('d M Y',strtotime($request->start_date)), 'end_date'=> date('d M Y',strtotime($request->end_date)) ];

        $_REQUEST['name'] = $request->name;
        $_REQUEST['email'] = $request->email;

        Mail::send(['html' => 'advocate_admin/registration_mail'], $data , function ($message) {
          $message->to($_REQUEST['email'], $_REQUEST['name'])->subject('Registration');
        });

          //return view('advocate_admin/add_advocate');
        return redirect()->back()->with('success', 'Your Data has been added successfully');
        //return redirect('advocate-panel/view-advocate');

      }  else  {

        $input = array(
          'case_registration'       => $request['case_registration'], 
          'pessi_cause_list'        => $request['pessi_cause_list'], 
          'daily_cause_list'        => $request['daily_cause_list'], 
          'sms_to_clients'          => $request['sms_to_clients'], 
          'order_judgement_upload'  => $request['order_judgement_upload'], 
          'complaince'              => $request['complaince'], 
          'calendar'                => $request['calendar'], 
          'undated_case'            => $request['undated_case'], 
          'reporting'               => $request['reporting'], 
          'due_course'              => $request['due_course'], 
          'master_modules'          => $request['master_modules']
        );

        $special_permission = json_encode(array_filter($input));

        $values  = array(
          'admin_name'             =>  $request['name'],
          'admin_number'           =>  $request['number'],
          'admin_address'          =>  $request['address'],
          'start_date'             =>  date('Y-m-d',strtotime($request['start_date'])),
          'end_date'               =>  date('Y-m-d',strtotime($request['end_date'])),
          'special_permission'     =>  $special_permission,
        );

        Admin::where('admin_id','=',$advocate_id)->update($values);

        $file = $request->file('admin_image');
        if($file != ""){
          $destinationPath = 'uploads/admin/';
          $imageName = 'uploads/admin/'.time().'.'.$file->getClientOriginalExtension();
          $file->move($destinationPath,$imageName);
          $image=array(
              'admin_image'         => $imageName
          );
          Admin::where('admin_id', $advocate_id)->update($image);
        }

      //return redirect('advocate-panel/view-advocate');
        return redirect()->back()->with('success', 'Your Data has been updated successfully');

    }
  }

    //advocate status

    public function advocate_status($id,$value){
      Admin::where('admin_id', $id)->update(['admin_status' => $value]);
      return Redirect()->back();
    }


    //advocate login

    public function advocate_login($advocate_id = false){
      
      $get_record = Admin::where('admin_id','=',$advocate_id)->first();

      // print_r($get_record->admin_id);
      // die;
      
      session(['admin_id' => $get_record->admin_id]);
      $admin_id = session('admin_id');
      return redirect('advocate-panel/dashboard');
 
    }




}
