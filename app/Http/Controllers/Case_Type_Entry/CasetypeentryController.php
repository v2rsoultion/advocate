<?php

namespace App\Http\Controllers\Case_Type_Entry;

use Illuminate\Http\Request;
use App\Model\Case_Type_Entry\Case_Type_Entry; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Admin\Account; // model name
use App\Http\Controllers\Controller;
use DB;
class CasetypeentryController extends Controller
{

    public function __construct()
      {
          $this->middleware('Validation');
      }
      
    //add case type entry

    public function add_case($court_type_id = false){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if(isset(json_decode($admin_details[0]->special_permission)->master_modules)){

      } else {
        return Redirect()->back();
      }

      $admin_id = session('admin_id');

      if($court_type_id!=''){
        $get_record = DB::select('select * from adv_case_type where sha1(case_id) = "'.$court_type_id.'" and user_admin_id = "'.$admin_id.'" ');

          if(count($get_record) == 0){
            return Redirect()->back();
          }

          $title = 'Add Case Type';
          $data  = compact('title','admin_details','get_record');
      }else{
          $get_record = "";
      }      
      $title = 'Add Case Type';
      $data  = compact('title','admin_details','get_record');
      return view('advocate_admin/add_case_type',$data);	
    }

    //view case type entry

    public function view_case(Request $request,$type=false){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if(isset(json_decode($admin_details[0]->special_permission)->master_modules)){

      } else {
        return Redirect()->back();
      }
      
      if($request['check'] != ""){
        Case_Type_Entry::whereIn('case_id', $request['check'])->delete();
        return Redirect()->back();
      }

      $query = Case_Type_Entry::where('user_admin_id','=',session('admin_id'));

      if($request['case_name'] != ""){
        $query = $query->where('case_name', 'LIKE', '%'.$request['case_name'].'%');
      }
      if($request['case_short_name'] != ""){
        $query = $query->where('case_short_name', 'LIKE', '%'.$request['case_short_name'].'%');
      }
      if($request['case_category'] != ""){
        $query = $query->where('case_category', 'LIKE', '%'.$request['case_category'].'%');
      }

      $app_data =  array('case_name' => $request['case_name'],'case_short_name' => $request['case_short_name'],'case_category' => $request['case_category']);

      if($type == "all"){
        $perpage    = $query->count();
        $get_record = $query->orderBy('case_id', 'desc')->paginate($perpage)->appends($app_data); 
      } else {
        $get_record = $query->orderBy('case_id', 'desc')->paginate(25)->appends($app_data);  
      }
      

      $case_category = $request['case_category'];     
      $title = 'View Case Type';
      $data  = compact('title','admin_details','get_record','case_category');
      return view('advocate_admin/view_case_type',$data);	
    }


    public function insert_case_type(Request $request,$court_type_id = false){

      if($court_type_id==''){

      if($request['type_name'] == ""){
        return redirect('advocate-panel/add-case-type');
      }  

        $Add = new Case_Type_Entry;  
        $Add->case_name            = $request['type_name'];
        $Add->case_short_name      = $request['short_name'];
        $Add->user_admin_id        = session('admin_id');
        $Add->case_category        = $request['c_name'];
        $Add->case_status          = 1;
        $Add->save();
        //return redirect('advocate-panel/view-case-type');
        return redirect()->back()->with('success', 'Your Data has been added successfully');
    }
    else
    {

      // if($request['type_name'] == ""){
      //   return redirect('advocate-panel/add-case-type');
      // }
      
      $values  = array(
        'case_name'           =>  $request['type_name'],
        'case_short_name'     =>  $request['short_name'],
        'case_category'       =>  $request['c_name']
        );
      Case_Type_Entry::where('case_id','=',$court_type_id)->update($values);
      //return redirect('advocate-panel/view-case-type');
      return redirect()->back()->with('success', 'Your Data has been updated successfully');
    }

  }

    //user status

    public function case_type_status($id,$value){

      $admin_id = session('admin_id');
      $get_record = DB::select('select * from adv_case_type where sha1(case_id) = "'.$id.'" and user_admin_id = "'.$admin_id.'" ');

      if(count($get_record) == 0){
        return Redirect()->back();
      }

      Case_Type_Entry::where('case_id', $get_record[0]->case_id)->update(['case_status' => $value]);
      return Redirect()->back();
    }



    public function check_case_type_name(Request $request)
      {
          $case_name          = $request->input('type_name');
          $case_category      = $request->input('case_category');
          $case_id            = $request->input('case_id');

          // print_r($request->input());
          // die;

          $check_case_type_name  = Case_Type_Entry::where(function($query) use ($request,$case_name,$case_id,$case_category)
          {
              
              if (!empty($case_name) ) 
              {
                  $query->where(['case_name' => $case_name]);
              }
              if (!empty($case_category) ) 
              {
                  $query->where(['case_category' => $case_category]);
              }
              if (!empty($case_id) ) 
              {
                  $query->where('case_id' ,'!=', $case_id);
              }
              
          })->first();

          if (!empty($check_case_type_name->case_name))
          {
              return response()->json(false);
          }
          else
          {
              return response()->json(true);
          }
      }



}
