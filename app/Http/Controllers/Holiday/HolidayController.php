<?php

namespace App\Http\Controllers\Holiday;

use Illuminate\Http\Request;
use App\Model\Admin\Admin; // model name
use App\Model\Admin\Account; // model name
use App\Model\Holiday\Holiday; // model name
use App\Http\Controllers\Controller;

class HolidayController extends Controller
{

  public function __construct(){
      $this->middleware('Validation');
  }
    //add Holiday

    public function add_holiday($holiday_id = false){
      
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if($admin_details[0]->admin_type == 2){
        return Redirect()->back();
      }
      
      if($holiday_id!=''){
          $get_record = Holiday::where('holiday_id','=',$holiday_id)->first();
      }else{
          $get_record = "";
      }

      $title = 'Add Holiday';
      $data  = compact('title','admin_details','get_record');
      return view('advocate_admin/add_holiday',$data);	
    }

    //view holiday

    public function view_holiday(Request $request,$type = false){
      
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if($admin_details[0]->admin_type == 2){
        return Redirect()->back();
      }

      if($request['check'] != ""){
        Holiday::whereIn('holiday_id', $request['check'])->delete();
        return Redirect()->back();
      }

      $query = Holiday::query();

      if($request['holiday_title'] != ""){
        $query = $query->where('holiday_title', 'LIKE', '%'.$request['holiday_title'].'%');
      }

      if($request['holiday_description'] != ""){
        $query = $query->where('holiday_description', 'LIKE', '%'.$request['holiday_description'].'%');
      }

      if($request['holiday_from'] != ""){
        $query = $query->where('holiday_date', '>=', date('Y-m-d',strtotime($request['holiday_from'])) );
      }

      if($request['holiday_to'] != ""){
        $query = $query->where('holiday_date', '<=', date('Y-m-d',strtotime($request['holiday_to'])) );
      }

      $app_data =  array('holiday_title' => $request['holiday_title'],'holiday_description' => $request['holiday_description'],'holiday_from' => $request['holiday_from'] ,'holiday_to' => $request['holiday_to'] ); 

      if($type == "all"){
        $perpage    = $query->count();
        $get_record = $query->orderBy('holiday_id', 'desc')->paginate($perpage)->appends($app_data);
      } else {
        $get_record = $query->orderBy('holiday_id', 'desc')->paginate(25)->appends($app_data);
      }

      $title = 'View Holiday';
      $data  = compact('title','admin_details','get_record');
      return view('advocate_admin/view_holiday',$data);	
    }

    //insert holiday function

    public function insert_holiday(Request $request, $holiday_id = false){
      
      if($holiday_id==''){

        if($request->holiday_title == "" || $request->holiday_date == ""){
          return redirect()->back()->with('danger', 'Please insert correct data!');
          //return redirect('advocate-panel/view-holiday');
        }
        
        $Add = new Holiday;  
        $Add->holiday_title            = $request['holiday_title'];
        $Add->holiday_description      = $request['holiday_description'];
        $Add->holiday_date             = date('Y-m-d',strtotime($request['holiday_date']));
        $Add->save();
        $holiday_id = $Add->id;

        return redirect()->back()->with('success', 'Your Data has been added successfully');

      }  else  {

        if($request->holiday_title == ""){
          return redirect('advocate-panel/view-holiday');
        }

        $values  = array(
          'holiday_title'             =>  $request['holiday_title'],
          'holiday_description'       =>  $request['holiday_description'],
          'holiday_date'              =>  date('Y-m-d',strtotime($request['holiday_date'])),
        );

        Holiday::where('holiday_id','=',$holiday_id)->update($values);

      //return redirect('advocate-panel/view-holiday');
        return redirect()->back()->with('success', 'Your Data has been updated successfully');

    }
  }

    //holiday status

    public function holiday_status($id,$value){
      Holiday::where('holiday_id', $id)->update(['holiday_status' => $value]);
      return Redirect()->back();
    }




}
