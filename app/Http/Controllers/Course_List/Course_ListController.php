<?php

namespace App\Http\Controllers\Course_List;

use Illuminate\Http\Request;
use App\Model\Case_Registration\Case_Registration; // model name
use App\Model\Case_Type_Entry\Case_Type_Entry; // model name
use App\Model\Case_Sub_Type\Case_Sub_Type; // model name
use App\Model\Assigned\Assigned; // model name
use App\Model\Reffered\Reffered; // model name
use App\Model\Section\Section; // model name
use App\Model\Client\Client; // model name
use App\Model\Admin\Account; // model name
use App\Model\Stage\Stage; // model name
use App\Model\Sub_Client\Sub_Client; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Judge\Judge; // model name
use App\Model\Act\Act; // model name
use App\Model\Fir\Fir; // model name
use App\Model\Pessi\Pessi; // model name
use App\Model\Course\Course; // model name
use App\Http\Controllers\Controller;

class Course_ListController extends Controller
{
    //add pessi entry form

    public function add_course_list($reg_id = false){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();
      
      $get_case_regestered = Case_Registration::where('user_admin_id','=',$admin_details[0]->admin_id )->get();

      $get_judge           = Judge::where(['user_admin_id' => $admin_details[0]->admin_id , 'judge_status' => 1 ])->get();
      $stage               = Stage::where(['user_admin_id' => $admin_details[0]->admin_id , 'stage_status' => 1 ])->get();
      
      $title                = 'Add Course List';
      $data                 = compact('title','edit_id','sub_client','admin_details','get_case_regestered','get_record','fir','get_judge','case_sub_type','stage','assigned','reffered','section','client');
      return view('advocate_admin/add_course_list',$data);	
    }


    // get case registration

    public function get_case_registration_course($reg_id){

      $query = Case_Registration::leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->where('reg_id', $reg_id)->first();

      $course = Course::where('course_case_reg', $reg_id)->orderBy('list_id', DESC)->first();

      $pessi = Pessi::where('pessi_case_reg', $reg_id)->where('pessi_choose_type' ,0)->orderBy('pessi_id', DESC)->first();

      if($course == ""){
        $course->list_reg_stage = $query->reg_stage_id;
      }

      if($pessi->pessi_further_date == ""){
        $pre_date = "";
      } else {
        $pre_date = date('d F Y',strtotime($pessi->pessi_further_date));
      }


      return response()->json(['status'=>true,'reg_respondent'=>$query->reg_respondent, 'reg_petitioner'=>$query->reg_petitioner, 'case_name'=>$query->case_name, 'previous_date'=> $pre_date , 'pessi_statge_id'=> $course->list_reg_stage , 'reg_date'=> date('d F Y',strtotime($query->reg_date)) ]);

    }

    //view pessi form

    public function view_course_list(Request $request){
      
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();
      
      if($request['check'] != ""){
        Course::whereIn('list_id', $request['check'])->delete();
        return Redirect()->back();
      }      

      $query = Course::leftJoin('adv_case_reg', function($join) {$join->on('course_case_reg', '=', 'reg_id');})->leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->leftJoin('adv_stage', function($join) {$join->on('stage_id', '=', 'list_reg_stage');})->leftJoin('adv_judge', function($join) {$join->on('list_justice_id', '=', 'judge_id');});

      if($request['case_no'] != "0" && $request['case_no'] != ""){
        $query = $query->where('course_case_reg', '=', $request['case_no']);
      }
      if($request['case_name'] != "0" && $request['case_name'] != ""){
        $query = $query->where('reg_case_type_id', '=', $request['case_name']);
      }
      if($request['stage_name'] != "0" && $request['stage_name'] != ""){
        $query = $query->where('list_reg_stage', '=', $request['stage_name']);
      }
      if($request['honarable_justice'] != "0" && $request['honarable_justice'] != ""){
        $query = $query->where('list_justice_id', '=', $request['honarable_justice']);
      }

      if($request['previous_date'] != "" ){
        $query = $query->where('reg_date', '=', date('Y-m-d',strtotime($request['previous_date'])));
      }



      $app_data =  array('case_no' => $request['case_no'],'case_name' => $request['case_name'],'stage_name' => $request['stage_name'],'honarable_justice' => $request['honarable_justice']);
      $get_record = $query->where(['list_user_id' => $admin_details[0]->admin_id ])->orderBy('list_id', 'desc')->paginate(10)->appends($app_data); 


      $get_case_regestered = Case_Registration::where('user_admin_id','=',$admin_details[0]->admin_id )->get();
      $get_stage           = Stage::where(['user_admin_id' => $admin_details[0]->admin_id , 'stage_status' => 1 ])->get(); 
      $type_case           = Case_Type_Entry::where(['user_admin_id' => $admin_details[0]->admin_id , 'case_status' => 1 ])->get();
      $get_judge           = Judge::where(['user_admin_id' => $admin_details[0]->admin_id , 'judge_status' => 1 ])->get();    
      
      // print_r($get_record);

      $case_no    = $request['case_no'];
      $case_name  = $request['case_name'];
      $stage_name = $request['stage_name'];
      $honarable_justice = $request['honarable_justice'];
      // die;

      $title = 'View Course List';
      $data  = compact('title','admin_details','get_record','get_case_regestered','get_stage','type_case','case_no','case_name','stage_name','get_judge','honarable_justice');
      return view('advocate_admin/view_course_list',$data);	
    }


    //insert case registration function

    public function insert_course_list(Request $request, $list_id = false){

      // echo "<pre>";
      // print_r($request->input());
      // echo "</pre>";die;
      
      $previous_date = date('Y-m-d',strtotime($request['previous_date']));

      if($list_id==''){
      
        $Add = new Course;  
        $Add->course_case_reg        = $request['case_no'];
        $Add->list_previous_date     = $previous_date;
        $Add->list_reg_stage         = $request['reg_stage'];
        $Add->list_justice_id        = $request['honarable_justice'];
        $Add->list_court_no          = $request['court_no'];
        $Add->list_serial_no         = $request['serial_no'];
        $Add->list_page_no           = $request['page_no'];;
        $Add->save();
        $id = $Add->id;
          
        return redirect('advocate-panel/view-course-list');
        
      }

  }


}
