<?php

namespace App\Http\Controllers\Case_Sub_Type;

use Illuminate\Http\Request;
use App\Model\Case_Sub_Type\Case_Sub_Type; // model name
use App\Model\Case_Type_Entry\Case_Type_Entry; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Admin\Account; // model name
use App\Http\Controllers\Controller;
use DB;
class CasesubtypeController extends Controller
{

    public function __construct()
      {
          $this->middleware('Validation');
      }
      
    //add case sub type

    public function add_case_sub_type($court_sub_type_id = false){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();
      $admin_id = session('admin_id');
      
      if($court_sub_type_id!=''){
        $get_record = DB::select('select * from adv_case_subtype where sha1(subtype_id) = "'.$court_sub_type_id.'" and user_admin_id = "'.$admin_id.'" ');

          if(count($get_record) == 0){
            return Redirect()->back();
          }
          $case_type_entry = Case_Type_Entry::get();      
          $title = 'Add Case';
          $data  = compact('title','admin_details','get_record','case_type_entry');
          return view('advocate_admin/add_case_sub_type',$data);

      }else{
          $get_record = "";
      }
      $case_type_entry = Case_Type_Entry::get();      
      $title = 'Add Case';
      $data  = compact('title','admin_details','get_record','case_type_entry');
      return view('advocate_admin/add_case_sub_type',$data);	
    }

    //view case sub type

    public function view_case_sub_type(Request $request){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if($request['check'] != ""){
        Case_Sub_Type::whereIn('subtype_id', $request['check'])->delete();
        return Redirect()->back();
      }
      
      $query = Case_Sub_Type::leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'subtype_case_id');})->where('adv_case_subtype.user_admin_id','=',session('admin_id'));

      if($request['subtype_case'] != ""){
        $query = $query->where('case_name', 'LIKE', '%'.$request['subtype_case'].'%');
      }
      if($request['sub_type_name'] != ""){
        $query = $query->where('subtype_name', 'LIKE', '%'.$request['sub_type_name'].'%');
      }
      if($request['subtype_code'] != ""){
        $query = $query->where('subtype_code', 'LIKE', '%'.$request['subtype_code'].'%');
      }
      if($request['subtype_short_name'] != ""){
        $query = $query->where('subtype_short_name', 'LIKE', '%'.$request['subtype_short_name'].'%');
      }

      $app_data =  array('sub_type_name' => $request['sub_type_name'],'subtype_case' => $request['subtype_case'],'subtype_code' => $request['subtype_code'],'subtype_short_name' => $request['subtype_short_name']);
      $get_record = $query->orderBy('subtype_id', 'desc')->paginate(10)->appends($app_data);      
      $title = 'View Case';
      $data  = compact('title','admin_details','get_record');
      return view('advocate_admin/view_case_sub_type',$data);	
    }

    //insert case sub type function

    public function insert_case_sub_type(Request $request, $court_sub_type_id = false){

      if($court_sub_type_id==''){
      $Add = new Case_Sub_Type;  
      $Add->subtype_case_id         = $request['choose_case_type'];
      $Add->subtype_name            = $request['sub_type_name'];
      $Add->subtype_short_name      = $request['short_name'];
      $Add->subtype_code            = $request['code'];
      $Add->user_admin_id           = session('admin_id');
      $Add->subtype_description     = $request['description'];
      $Add->subtype_status          = 1;
      $Add->save();
      return redirect('advocate-panel/view-case-sub-type');
    }
    else
    {
      $values  = array(
        'subtype_case_id'         =>  $request['choose_case_type'],
        'subtype_name'            =>  $request['sub_type_name'],
        'subtype_short_name'      =>  $request['short_name'],
        'subtype_code'            =>  $request['code'],
        'subtype_description'     =>  $request['description']
        );
      Case_Sub_Type::where('subtype_id','=',$court_sub_type_id)->update($values);
      return redirect('advocate-panel/view-case-sub-type');
    }

  }

    //court sub type status

    public function case_sub_type_status($id,$value){
      Case_Sub_Type::where('subtype_id', $id)->update(['subtype_status' => $value]);
      return Redirect()->back();
    }
}
