<?php

namespace App\Http\Controllers\Reffered;

use Illuminate\Http\Request;
use App\Model\Reffered\Reffered; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Admin\Account; // model name
use App\Http\Controllers\Controller;
use DB;
class RefferedController extends Controller
{

    public function __construct()
      {
          $this->middleware('Validation');
      }
      
    //add referred

    public function add_reffered($ref_id = false){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if(isset(json_decode($admin_details[0]->special_permission)->master_modules)){

      } else {
        return Redirect()->back();
      }

      $admin_id = session('admin_id');

      if($ref_id!=''){

          $get_record = DB::select('select * from adv_referred where sha1(ref_id) = "'.$ref_id.'" and user_admin_id = "'.$admin_id.'" ');

          if(count($get_record) == 0){
            return Redirect()->back();
          }

          $title = 'Add Referred';
          $data  = compact('title','admin_details','get_record');
          return view('advocate_admin/add_reffered',$data);
          
      }else{
          $get_record = "";
      }       
      $title = 'Add Referred';
      $data  = compact('title','admin_details','get_record');
      return view('advocate_admin/add_reffered',$data);	
    }

    //view referred

    public function view_reffered(Request $request,$type = false){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if(isset(json_decode($admin_details[0]->special_permission)->master_modules)){

      } else {
        return Redirect()->back();
      }

      if($request['check'] != ""){
        Reffered::whereIn('ref_id', $request['check'])->delete();
        return Redirect()->back();
      }
      
      $query = Reffered::where('user_admin_id','=',session('admin_id'));

      if($request['ref_advocate_name'] != ""){
        $query = $query->where('ref_advocate_name', 'LIKE', '%'.$request['ref_advocate_name'].'%');
      }
      if($request['ref_address'] != ""){
        $query = $query->where('ref_address', 'LIKE', '%'.$request['ref_address'].'%');
      }
      if($request['ref_mobile_number'] != ""){
        $query = $query->where('ref_mobile_number', 'LIKE', '%'.$request['ref_mobile_number'].'%');
      }
      if($request['ref_place'] != ""){
        $query = $query->where('ref_place', 'LIKE', '%'.$request['ref_place'].'%');
      }

      $app_data =  array('ref_advocate_name' => $request['ref_advocate_name'],'ref_address' => $request['ref_address'],'ref_mobile_number' => $request['ref_mobile_number'],'ref_place' => $request['ref_place']);

      if($type == "all"){
        $perpage    = $query->count();
        $get_record = $query->orderBy('ref_id', 'desc')->paginate($perpage)->appends($app_data);   
      } else {
        $get_record = $query->orderBy('ref_id', 'desc')->paginate(25)->appends($app_data);   
      }


         
      $title = 'View Referred';
      $data  = compact('title','admin_details','get_record');
      return view('advocate_admin/view_reffered',$data);	
    }


    //insert referred function

    public function insert_reffered(Request $request, $ref_id = false){

      if($ref_id==''){

        if($request['advocate_name'] == ""){
          return redirect('advocate-panel/add-referred');
        }

      $Add = new Reffered;  
      $Add->ref_advocate_name         = $request['advocate_name'];
      $Add->ref_advocate_email        = $request['email'];
      $Add->ref_address               = $request['address'];
      $Add->ref_mobile_number         = $request['mobile'];
      $Add->user_admin_id             = session('admin_id');
      $Add->ref_place                 = $request['place'];
      $Add->ref_status                = 1;
      $Add->save();
      //return redirect('advocate-panel/view-referred');
      return redirect()->back()->with('success', 'Your Data has been added successfully');
    }
    else
    {
      $values  = array(
        'ref_advocate_name'         =>  $request['advocate_name'],
        'ref_advocate_email'        =>  $request['email'],
        'ref_address'               =>  $request['address'],
        'ref_mobile_number'         =>  $request['mobile'],
        'ref_place'                 =>  $request['place']
        );
      Reffered::where('ref_id','=',$ref_id)->update($values);
      //return redirect('advocate-panel/view-referred');
      return redirect()->back()->with('success', 'Your Data has been updated successfully');
    }

  }

    //court reffred status

    public function reffered_status($id,$value){

      $admin_id = session('admin_id');
      $get_record = DB::select('select * from adv_referred where sha1(ref_id) = "'.$id.'" and user_admin_id = "'.$admin_id.'" ');

      if(count($get_record) == 0){
        return Redirect()->back();
      }

      Reffered::where('ref_id', $get_record[0]->ref_id)->update(['ref_status' => $value]);
      return Redirect()->back();
    }
}
