<?php

namespace App\Http\Controllers\Reporting;

use Illuminate\Http\Request;
use App\Model\Case_Registration\Case_Registration; // model name
use App\Model\Case_Type_Entry\Case_Type_Entry; // model name
use App\Model\Class_Code\Class_Code; // model name
use App\Model\Assigned\Assigned; // model name
use App\Model\Reffered\Reffered; // model name
use App\Model\Section\Section; // model name
use App\Model\Client\Client; // model name
use App\Model\Admin\Account; // model name
use App\Model\Stage\Stage; // model name
use App\Model\Compliance\Compliance; // model name
use App\Model\Sub_Client\Sub_Client; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Act\Act; // model name
use App\Model\Upload_Document\Upload_Document; // model name
use App\Model\Judge\Judge; // model name
use App\Model\Fir\Fir; // model name
use App\Model\Pessi\Pessi; // model name
use App\Model\Court\Court; // model name
use App\Http\Controllers\Controller;
use DB;
use Excel;

class ReportingController extends Controller
{

    public function __construct()
      {
          $this->middleware('Validation');
      }
      
    //filter reporting


    public function filter_reporting($reg_id = false){
      
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if(isset(json_decode($admin_details[0]->special_permission)->reporting)){

      } else {
        return Redirect()->back();
      }
      
      $admin_id = session('admin_id');
          
      $case_type_entry      = Case_Type_Entry::where([ 'case_status' => 1 , 'user_admin_id' => $admin_id ])->where('case_name','!=','')->get();
      $get_case_regestered  = Case_Registration::where('user_admin_id','=',$admin_details[0]->admin_id )->where('reg_case_number','!=','')->get();
      $get_court            = Court::where([ 'court_status' => 1 , 'user_admin_id' => $admin_id ])->where('court_name','!=','')->get();


      $case_sub_type     = Class_Code::where([ 'classcode_status' => 1 , 'user_admin_id' => $admin_id ])->get();
      $fir               = Fir::where([ 'fir_status' => 1 , 'user_admin_id' => $admin_id ])->get();
      $stage             = Stage::where([ 'stage_status' => 1 , 'user_admin_id' => $admin_id ])->where('stage_name','!=','')->get();
      $assigned          = Assigned::where([ 'assign_status' => 1 , 'user_admin_id' => $admin_id ])->where('assign_advocate_name','!=','')->get();
      $act               = Act::where([ 'act_status' => 1 , 'user_admin_id' => $admin_id ])->where('act_name','!=','')->get();
      $section           = Section::where([ 'section_status' => 1 , 'user_admin_id' => $admin_id ])->where('section_name','!=','')->get();
      $reffered          = Reffered::where([ 'ref_status' => 1 , 'user_admin_id' => $admin_id ])->where('ref_advocate_name','!=','')->get();
      $client            = Client::where([ 'cl_status' => 1 , 'user_admin_id' => $admin_id ])->where('cl_group_name','!=','')->get();
      $sub_client        = Sub_Client::where([ 'sub_client_status' => 1 , 'user_admin_id' => $admin_id ])->where('sub_client_name','!=','')->get();

      $place             = Client::where([ 'cl_status' => 1 , 'user_admin_id' => $admin_id ])->where('cl_group_place','!=','')->groupby('cl_group_place')->get();       
      

      // echo "<pre>";
      // print_r($get_case_regestered);
      // die;


      $title             = 'Reporting';
      $data              = compact('title','edit_id','sub_client','admin_details','act','get_record','fir','case_type_entry','case_sub_type','stage','assigned','reffered','section','client','get_case_regestered','get_court','place');
      return view('advocate_admin/filter_reporting',$data);	
    }


    // court type

    public function court_type(Request $request , $court_type){
      
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();
      
      $get_court_all = Court::where('user_admin_id','=',$admin_details[0]->admin_id )->where(['court_type' => $court_type])->get();

      $data  = compact('title','admin_details','get_record','get_court_all');
      return view('advocate_admin/court_type_ajax',$data);
    }


    // change_group

    public function change_group(Request $request , $group_type){
      
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();
      
      $get_group_all = Client::where('user_admin_id','=',$admin_details[0]->admin_id )->where(['cl_group_type' => $group_type])->where('cl_group_name','!=','')->get();


      $data  = compact('title','admin_details','get_record','get_group_all');
      return view('advocate_admin/client_ajax',$data);
    }



    //view filter reporting

    public function view_filter_reporting(Request $request){


      // print_r($request->input());
      // die;

      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      $query = Case_Registration::leftJoin('adv_pessi', function($join) {$join->on('pessi_case_reg', '=', 'reg_id');})->leftJoin('adv_court', function($join) {$join->on('court_type', '=', 'reg_court');})->leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->leftJoin('adv_class_code', function($join) {$join->on('classcode_id', '=', 'reg_case_subtype_id');})->leftJoin('adv_fir', function($join) {$join->on('fir_id', '=', 'reg_fir_id');})->leftJoin('adv_stage', function($join) {$join->on('stage_id', '=', 'reg_stage_id');})->leftJoin('adv_assigned', function($join) {$join->on('assign_id', '=', 'reg_assigend_id');})->leftJoin('adv_act', function($join) {$join->on('act_id', '=', 'reg_act_id');})->leftJoin('adv_section', function($join) {$join->on('section_id', '=', 'reg_section_id');})->leftJoin('adv_referred', function($join) {$join->on('ref_id', '=', 'reg_reffeerd_by_id');})->leftJoin('adv_client', function($join) {$join->on('cl_id', '=', 'reg_client_group_id');})->leftJoin('adv_sub_client', function($join) {$join->on('sub_client_id', '=', 'reg_client_subgroup_id');})->where('adv_case_reg.user_admin_id','=',session('admin_id'))->where('pessi_status',1);   


      if($request['case_category'] != ""){
        if($request['case_category'] != "0"){
          $query = $query->where('reg_case_type_category', '=', $request['case_category'] );
        }
      }

      if($request['choose_case_type'] != ""){
        $query = $query->whereIn('reg_case_type_id' , $request['choose_case_type'] );
      }

      if($request['case_no'] != "" && $request['case_no'] != "0"){
        $query = $query->where('reg_id', '=', $request['case_no'] );
      }

      if($request['file_no'] != "" && $request['file_no'] != "0" ){
        $query = $query->where('reg_id', '=', $request['file_no'] );
      }

      if($request['case_status'] == "1"){
        //$query = $query->where('reg_status', '=', $request['case_status'] );
        $query = $query->where('pessi_choose_type', '=', 0);
      }

      if($request['case_status'] == "2"){
        //$query = $query->where('reg_status', '=', $request['case_status'] );
        $query = $query->where('pessi_choose_type', '=', 1);
      }

      if($request['court_type'] != ""){
        $query = $query->where('reg_court', '=', $request['court_type'] );
      }

      if($request['court_name'] != ""){
        $query = $query->where('reg_court_id', '=', $request['court_name'] );
      }

      if($request['reg_stage'] != ""){
        $query = $query->where('reg_stage_id', '=', $request['reg_stage'] );
      }

      if($request['power'] != ""){
        $query = $query->where('reg_power', '=', $request['power'] );
      }

      if($request['assigned'] != ""){
        $query = $query->where('reg_assigend_id', '=', $request['assigned'] );
      }

      if($request['act'] != ""){
        $query = $query->where('reg_act_id', '=', $request['act'] );
      }

      if($request['section'] != ""){
      //  $query = $query->whereIn('reg_section_id', '=', $request['section'] );

        $section = $request['section'];
        $review_count = 0;

          $review_count++; 
              $query = $query->where(function ($query) use($section) { 
                $query->whereRaw("find_in_set(".$section[0].",reg_section_id)");
                if(isset($section[0]))
                {
                  unset($section[0]);
                }
                if(!empty($section)) {
                  foreach ($section as $reg_section_id) {
                   $query->orwhereRaw("find_in_set(".$reg_section_id.",reg_section_id)") ;
                  }
                }
              
              })
              ;

              
      }

      if($request['reffered'] != ""){
        $query = $query->where('reg_reffeerd_by_id', '=', $request['reffered'] );
      }

      if($request['group_type'] != ""){
        $query = $query->where('cl_group_type', '=', $request['group_type'] );
      }

      if($request['client_group'] != ""){
        $query = $query->where('reg_client_group_id', '=', $request['client_group'] );
      }

      if($request['client_place'] != ""){
        $query = $query->where('cl_group_place', '=', $request['client_place'] );
      }

      if($request['client_sub_group'] != ""){
        $query = $query->where('reg_client_subgroup_id', '=', $request['client_sub_group'] );
      }

      if($request['reg_date_from'] != ""){
        $query = $query->where('reg_date', '>=', date('Y-m-d',strtotime($request['reg_date_from'])) );
      }

      if($request['reg_date_to'] != ""){
        $query = $query->where('reg_date', '<=', date('Y-m-d',strtotime($request['reg_date_to'])) );
      }

      if($request['disposal_from'] != ""){
        $query = $query->where('pessi_choose_type','=',0)->where('pessi_further_date','<=',date('Y-m-d'));
        $query = $query->where('reg_date', '>=', date('Y-m-d',strtotime($request['disposal_from'])) );
      }

      if($request['disposal_to'] != ""){
        // if($request['from_date'] != ""){
        //   $query = $query->orwhere('reg_date', '>', date('Y-m-d',strtotime($request['to_date'])) );
        // } else {
          $query = $query->where('reg_date', '<=', date('Y-m-d',strtotime($request['disposal_to'])) );
        //  }
      }

      if($request['from_date'] != ""){
        $query = $query->where('pessi_choose_type','=',1)->where('pessi_further_date','<=',date('Y-m-d'));
        $query = $query->where('reg_date', '>=', date('Y-m-d',strtotime($request['from_date'])) );
      }

      if($request['to_date'] != ""){
          $query = $query->where('reg_date', '<=', date('Y-m-d',strtotime($request['to_date'])) );
      }

      if($request['next_date_from'] != ""){
        $query = $query->where('pessi_further_date', '>=', date('Y-m-d',strtotime($request['next_date_from'])) );
      }

      if($request['next_date_to'] != ""){
        $query = $query->where('pessi_further_date', '<=', date('Y-m-d',strtotime($request['next_date_to'])) );
      }

      // if($request['disposal_from'] != ""){
      //   $query = $query->where('reg_disposal_date', '>=', date('Y-m-d',strtotime($request['disposal_from'])) );
      // }

      // if($request['disposal_to'] != ""){
      //   $query = $query->where('reg_disposal_date', '<=', date('Y-m-d',strtotime($request['disposal_to'])) );
      // }

    // [disposal_from] => 1-1-2015
    // [disposal_to] => 1-1-2015
    // [from_date] => 1-1-2015
    // [to_date] => 1-1-2015

    
    // [disposal_from] => 
    // [disposal_to] =>


      $get_record = $query->orderBy('reg_id', 'desc')->groupBy('reg_id')->get();

      // print_r(implode(',', $request->choose_case_type));
      // die;

      // print_r($get_record);
      // die;
      

      $case_category      = $request->case_category;
      $choose_case_type   = implode(',', $request->choose_case_type);
      $case_no            = $request->case_no;
      $file_no            = $request->file_no;
      $case_status        = $request->case_status;
      $court_type         = $request->court_type;
      $court_name         = $request->court_name;
      $reg_stage          = $request->reg_stage;
      $power              = $request->power;
      $assigned           = $request->assigned;
      $act                = $request->act;
      $section            = implode(',', $request->section);
      $reffered           = $request->reffered;
      $group_type         = $request->group_type;
      $client_group       = $request->client_group;
      $client_sub_group   = $request->client_sub_group;
      $client_place       = $request->client_place;
      $reg_date_from      = $request->reg_date_from;
      $reg_date_to        = $request->reg_date_to;
      $disposal_from      = $request->disposal_from;
      $disposal_to        = $request->disposal_to;
      $next_date_from     = $request->next_date_from;
      $next_date_to       = $request->next_date_to;
      $from_date          = $request->from_date;
      $to_date            = $request->to_date;
      $reporting_heading  = $request->reporting_heading;


      $title = 'Reporting';
      $data  = compact('title','admin_details','get_record','case_category','choose_case_type','case_no','file_no','case_status','court_type','court_name','reg_stage','power','assigned','act','section','reffered','group_type','client_group','client_sub_group','reg_date_from','reg_date_to','disposal_from','disposal_to','client_place','next_date_from','next_date_to','from_date','to_date','reporting_heading' );
      return view('advocate_admin/view_reporting',$data);	
    }



    //download reporting function


    public function download_filter_reporting(Request $request, $reg_id = false){


      $case_category      = $request->case_category;
      $choose_case_type   = explode(',', $request->choose_case_type);
      $case_no            = $request->case_no;
      $file_no            = $request->file_no;
      $case_status        = $request->case_status;
      $court_type         = $request->court_type;
      $court_name         = $request->court_name;
      $reg_stage          = $request->reg_stage;
      $power              = $request->power;
      $assigned           = $request->assigned;
      $act                = $request->act;
      $section            = explode(',', $request->section);
      $reffered           = $request->reffered;
      $group_type         = $request->group_type;
      $client_group       = $request->client_group;
      $client_sub_group   = $request->client_sub_group;
      $client_place       = $request->client_place;
      $reg_date_from      = $request->reg_date_from;
      $reg_date_to        = $request->reg_date_to;
      $disposal_from      = $request->disposal_from;
      $disposal_to        = $request->disposal_to;
      $next_date_from     = $request->next_date_from;
      $next_date_to       = $request->next_date_to;
      $from_date          = $request->from_date;
      $to_date            = $request->to_date;
      $reporting_heading  = $request->reporting_heading;

      $admin_details = Admin::where(['admin_id' => session('admin_id')])->first();

      $query = Case_Registration::leftJoin('adv_pessi', function($join) {$join->on('pessi_case_reg', '=', 'reg_id');})->leftJoin('adv_court', function($join) {$join->on('court_type', '=', 'reg_court');})->leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->leftJoin('adv_class_code', function($join) {$join->on('classcode_id', '=', 'reg_case_subtype_id');})->leftJoin('adv_fir', function($join) {$join->on('fir_id', '=', 'reg_fir_id');})->leftJoin('adv_stage', function($join) {$join->on('stage_id', '=', 'reg_stage_id');})->leftJoin('adv_assigned', function($join) {$join->on('assign_id', '=', 'reg_assigend_id');})->leftJoin('adv_act', function($join) {$join->on('act_id', '=', 'reg_act_id');})->leftJoin('adv_section', function($join) {$join->on('section_id', '=', 'reg_section_id');})->leftJoin('adv_referred', function($join) {$join->on('ref_id', '=', 'reg_reffeerd_by_id');})->leftJoin('adv_client', function($join) {$join->on('cl_id', '=', 'reg_client_group_id');})->leftJoin('adv_sub_client', function($join) {$join->on('sub_client_id', '=', 'reg_client_subgroup_id');})->where('adv_case_reg.user_admin_id','=',session('admin_id'))->where('pessi_status',1);   


      if($request['case_category'] != ""){
        if($request['case_category'] != "0"){
          $query = $query->where('reg_case_type_category', '=', $request['case_category'] );
        }
      }

      if($request['choose_case_type'] != ""){
        $query = $query->whereIn('reg_case_type_id' , $choose_case_type );
      }

      if($request['case_no'] != "" && $request['case_no'] != "0"){
        $query = $query->where('reg_id', '=', $request['case_no'] );
      }

      if($request['file_no'] != "" && $request['file_no'] != "0" ){
        $query = $query->where('reg_id', '=', $request['file_no'] );
      }

      if($request['case_status'] == "1"){
        //$query = $query->where('reg_status', '=', $request['case_status'] );
        $query = $query->where('pessi_choose_type', '=', 0);
      }

      if($request['case_status'] == "2"){
        //$query = $query->where('reg_status', '=', $request['case_status'] );
        $query = $query->where('pessi_choose_type', '=', 1);
      }

      if($request['court_type'] != ""){
        $query = $query->where('court_type', '=', $request['court_type'] );
      }

      if($request['court_name'] != ""){
        $query = $query->where('reg_court_id', '=', $request['court_name'] );
      }

      if($request['reg_stage'] != ""){
        $query = $query->where('reg_stage_id', '=', $request['reg_stage'] );
      }

      if($request['power'] != ""){
        $query = $query->where('reg_power', '=', $request['power'] );
      }

      if($request['assigned'] != ""){
        $query = $query->where('reg_assigend_id', '=', $request['assigned'] );
      }

      if($request['act'] != ""){
        $query = $query->where('reg_act_id', '=', $request['act'] );
      }

      if($section[0] != ""){
      //  $query = $query->whereIn('reg_section_id', '=', $request['section'] );

    //    $section = $request['section'];
        $review_count = 0;

          $review_count++; 
              $query = $query->where(function ($query) use($section) { 
                $query->whereRaw("find_in_set(".$section[0].",reg_section_id)");
                if(isset($section[0]))
                {
                  unset($section[0]);
                }
                if(!empty($section)) {
                  foreach ($section as $reg_section_id) {
                   $query->orwhereRaw("find_in_set(".$reg_section_id.",reg_section_id)") ;
                  }
                }
              
              })
              ;
      }

      if($request['reffered'] != ""){
        $query = $query->where('reg_reffeerd_by_id', '=', $request['reffered'] );
      }

      if($request['group_type'] != ""){
        $query = $query->where('cl_group_type', '=', $request['group_type'] );
      }

      if($request['client_group'] != ""){
        $query = $query->where('reg_client_group_id', '=', $request['client_group'] );
      }

      if($request['client_place'] != ""){
        $query = $query->where('cl_group_place', '=', $request['client_place'] );
      }

      if($request['client_sub_group'] != ""){
        $query = $query->where('reg_client_subgroup_id', '=', $request['client_sub_group'] );
      }

      if($request['reg_date_from'] != ""){
        $query = $query->where('reg_date', '>=', date('Y-m-d',strtotime($request['reg_date_from'])) );
      }

      if($request['reg_date_to'] != ""){
        $query = $query->where('reg_date', '<=', date('Y-m-d',strtotime($request['reg_date_to'])) );
      }

      if($request['disposal_from'] != ""){
        $query = $query->where('pessi_choose_type','=',0)->where('pessi_further_date','<=',date('Y-m-d'));
        $query = $query->where('reg_date', '>=', date('Y-m-d',strtotime($request['disposal_from'])) );
      }

      if($request['disposal_to'] != ""){
        // if($request['from_date'] != ""){
        //   $query = $query->orwhere('reg_date', '>', date('Y-m-d',strtotime($request['to_date'])) );
        // } else {
          $query = $query->where('reg_date', '<=', date('Y-m-d',strtotime($request['disposal_to'])) );
        //  }
      }

      if($request['from_date'] != ""){
        $query = $query->where('pessi_choose_type','=',1)->where('pessi_further_date','<=',date('Y-m-d'));
        $query = $query->where('reg_date', '>=', date('Y-m-d',strtotime($request['from_date'])) );
      }

      if($request['to_date'] != ""){
          $query = $query->where('reg_date', '<=', date('Y-m-d',strtotime($request['to_date'])) );
      }

      if($request['next_date_from'] != ""){
        $query = $query->where('pessi_further_date', '>=', date('Y-m-d',strtotime($request['next_date_from'])) );
      }

      if($request['next_date_to'] != ""){
        $query = $query->where('pessi_further_date', '<=', date('Y-m-d',strtotime($request['next_date_to'])) );
      }

      if($request['check'] != ""){
        $query = $query->whereIn('reg_id', $request['check']);
      }



      if($request['reg_date_from'] != ""){
        $query = $query->orderBy('reg_date', 'asc');
      } else if($request['disposal_from'] != ""){
        $query = $query->orderBy('reg_date', 'asc');
      } else if($request['from_date'] != ""){
        $query = $query->orderBy('reg_date', 'asc');
      } else if($request['next_date_from'] != ""){
        $query = $query->orderBy('reg_date', 'asc');
      } else {
        //$query = $query->orderBy('reg_id', 'desc');
        $query = $query->orderBy('court_type', 'desc');
        $query = $query->orderBy('reg_date', 'desc');
      }

      
      if($reporting_heading == ""){
        $reporting_heading = "Reporting";
      } else {
        $reporting_heading = $reporting_heading;
      }

      // echo "<pre>";
      // print_r($request->filter_option);
      // die;


      if($request->filter_option == ""){
        $data = $query->groupBy('reg_id')->get([ 'court_type as Court Type','court_name as Court Name', 'reg_date as Registration Date' ,'reg_file_no as File Number','case_name as Case Type','reg_case_number as Case Number','classcode_name as Class Code','reg_vcn_number as NCV Number','reg_petitioner as Petitioner Name','reg_respondent as Respondent Name','cl_group_name as Client Group/Clients','cl_name_prefix as Prefix','cl_father_name as Guardian Name','sub_client_name as Client Sub Group','cl_group_address as Address','cl_group_mobile_no as Mobile Number','act_name as Act','reg_section_id as Section','reg_power as Power','reg_fir_id as FIR Number','pessi_further_date as Next Date','stage_name as Stage Name','pessi_choose_type as Result','ref_advocate_name as Referred By','reg_opp_council as Opposite Counsel' ,'cl_group_place as Place','assign_advocate_name as Assigned' ])->toArray();
      } else {
        $data = $query->groupBy('reg_id')->get($request->filter_option)->toArray();
      }


      $get_section = Section::query();      
      foreach($section as $reg_section_id) {
        $get_section->orwhereRaw("find_in_set(".$reg_section_id.",section_id)") ;
      }
      $get_section = $get_section->get()->toArray();

      foreach ($get_section as $get_sections) {
        $sections_name .= $get_sections['section_name'].",";
      }

      $sections_name = substr($sections_name,0,-1);
      // echo "<pre>";
      // print_r($sections_name);
      // die;


      if($request['court_type'] == 1){
        $court_name = "Trail Court";
      } else if($request['court_type'] == 2){
        $court_name = "High Court";
      } else {
        $court_name = "Both";
      }

      
      Excel::create('Reporting', function($excel) use ($data,$admin_details,$reporting_heading,$court_name,$sections_name) {
        $excel->sheet('Sheet', function ($sheet) use ($data,$admin_details,$reporting_heading,$court_name,$sections_name) {

          $sheet->setOrientation('landscape');
          $sheet->mergeCells('A1:G1');
          $sheet->mergeCells('A2:G2');
          $sheet->mergeCells('A3:G3');
          $sheet->mergeCells('A4:G4');
          $sheet->mergeCells('A5:G5');
          $sheet->mergeCells('A6:B6');
          $sheet->mergeCells('C6:E6');
          $sheet->mergeCells('F6:G6');
          $sheet->setHeight(array(
            1     =>  20,
            2     =>  20,
          ));
          $sheet->setSize('A8', 15, 18);
          $sheet->setSize('B8', 20, 18);
          $sheet->setSize('C8', 20, 18);
          $sheet->setSize('D8', 18, 18);
          $sheet->setSize('E8', 20, 18);
          $sheet->setSize('F8', 20, 18);
          $sheet->setSize('G8', 25, 18);
          $sheet->setSize('H8', 15, 18);
          $sheet->setSize('I8', 25, 18);
          $sheet->setSize('J8', 25, 18);
          $sheet->setSize('K8', 25, 18);
          $sheet->setSize('L8', 20, 18);
          $sheet->setSize('M8', 20, 18);
          $sheet->setSize('N8', 20, 18);
          $sheet->setSize('O8', 30, 18);
          $sheet->setSize('P8', 20, 18);
          $sheet->setSize('Q8', 20, 18);
          $sheet->setSize('R8', 20, 18);
          $sheet->setSize('S8', 8, 18);
          $sheet->setSize('T8', 15, 18);
          $sheet->setSize('U8', 15, 18);
          $sheet->setSize('V8', 20, 18);
          $sheet->setSize('W8', 15, 18);
          $sheet->setSize('X8', 20, 18);
          $sheet->setSize('Y8', 20, 18);
          $sheet->setSize('Z8', 20, 18);
          $sheet->setSize('AA8', 20, 18);

          $sheet->row(1, array( 'OFFICE OF' ));
          $sheet->row(1, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          $sheet->row(2, array( $admin_details->admin_firmname ));
          $sheet->row(2, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          $sheet->row(3, array( $admin_details->admin_advocates ));
          $sheet->row(3, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          $sheet->row(4, array( '(Advocates)' ));
          $sheet->row(4, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
        //  $day = date("l");
          $date = date("d/m/Y");
          $sheet->row(6, array( 'Date : '.$date, '',$reporting_heading, '','' , $court_name));
          $sheet->row(6, function($row) { $row->setAlignment('center');} );
          $sheet->mergeCells('A7:F7');

          $sheet->row(8, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
          $i=10;

          if(empty($data) ){

              $sheet->mergeCells('A10:G10');
              $sheet->mergeCells('A11:G11');
              $sheet->mergeCells('A12:G12');
              $sheet->row(11, array('No Record Found'));
              $sheet->row(11, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
            } else {

          foreach ($data as $key => $value) {

            if($value['Registration Date'] != ""){
              $data[$key]['Registration Date'] = date('d/m/Y',strtotime($value['Registration Date']));  
            } 

            if($data[$key]['Registration Date'] == '01/01/1970'){
              $data[$key]['Registration Date'] = "-----";
            }
          
            if($value['Next Date'] != ""){
              $data[$key]['Next Date'] = date('d/m/Y',strtotime($value['Next Date']));   
            }           


            if($data[$key]['Next Date'] == '01/01/1970'){
              $data[$key]['Next Date'] = "-----";
            }

            if($value['Court Type'] == 1) {
              $data[$key]['Court Type'] = "Trail Court";
            } else if($value['Court Type'] == 2) {
              $data[$key]['Court Type'] = "High Court";
            }

            //if(exit($value['Result'])){

            if(array_key_exists( 'File Number', $value ) ){
            if($value['Result'] != ""){
              if($value['Result'] == 0){
                $data[$key]['Result'] = "Pending";
              } else if($value['Result'] == 1){
                $data[$key]['Result'] = "Disposal";
              }  else if($value['Result'] == 2){
                $data[$key]['Result'] = "Due Course";
              }
            } else {
              $data[$key]['Result'] = "Pending";
            }
            }



            
            if(array_key_exists( 'File Number', $value ) ){
              if($data[$key]['File Number'] == ""){  $data[$key]['File Number'] = "-----";  }
            }

            if(array_key_exists( 'Case Number', $value ) ){
              if($value['Case Number'] == ""){  $data[$key]['Case Number'] = "-----";  }
            }

            if(array_key_exists( 'Case Type', $value ) ){
              if($value['Case Type'] == ""){  $data[$key]['Case Type'] = "-----";  }
            }

            if(array_key_exists( 'Class Code', $value ) ){
              if($value['Class Code'] == ""){  $data[$key]['Class Code'] = "-----";  }
            }

            if(array_key_exists( 'NCV Number', $value ) ){
              if($value['NCV Number'] == ""){  $data[$key]['NCV Number'] = "-----";  }
            }

            if(array_key_exists( 'Petitioner Name', $value ) ){
              if($value['Petitioner Name'] == ""){  $data[$key]['Petitioner Name'] = "-----";  }
            }

            if(array_key_exists( 'Respondent Name', $value ) ){
              if($value['Respondent Name'] == ""){  $data[$key]['Respondent Name'] = "-----";  }
            }

            if(array_key_exists( 'Client Group/Clients', $value ) ){
              if($value['Client Group/Clients'] == ""){  $data[$key]['Client Group/Clients'] = "-----";  }
            }

            if(array_key_exists( 'Client Sub Group', $value ) ){
              if($value['Client Sub Group'] == ""){  $data[$key]['Client Sub Group'] = "-----";  }
            }

            if(array_key_exists( 'Prefix', $value ) ){
              if($value['Prefix'] == ""){  $data[$key]['Prefix'] = "-----";  }
            }

            if(array_key_exists( 'Guardian Name', $value ) ){
              if($value['Guardian Name'] == ""){  $data[$key]['Guardian Name'] = "-----";  }
            }

            if(array_key_exists( 'Address', $value ) ){
              if($value['Address'] == ""){  $data[$key]['Address'] = "-----";  }
            }

            if(array_key_exists( 'Mobile Number', $value ) ){
              if($value['Mobile Number'] == ""){  $data[$key]['Mobile Number'] = "-----";  }
            }
            
            if(array_key_exists( 'Act', $value ) ){
              if($value['Act'] == ""){  $data[$key]['Act'] = "-----";  }
            }

            if(array_key_exists( 'Section', $value ) ){
              if($value['Section'] == ""){  $data[$key]['Section'] = "-----";  } else { $data[$key]['Section'] = $sections_name; }
            }

            if(array_key_exists( 'Power', $value ) ){

              if($value['Power'] == 1){
                $data[$key]['Power'] = "P";
              } elseif($value['Power'] == 2){
                $data[$key]['Power'] = "R";
              } elseif($value['Power'] == 3){
                $data[$key]['Power'] = "C";
              } elseif($value['Power'] == 4){
                $data[$key]['Power'] = "N";    
              } else {
                $data[$key]['Power'] = "";
              }

              //if($value['Power'] == ""){  $data[$key]['Power'] = "-----";  }
            }

            if(array_key_exists( 'FIR Number', $value ) ){
              if($value['FIR Number'] == ""){  $data[$key]['FIR Number'] = "-----";  }
            }

            if(array_key_exists( 'Stage Name', $value ) ){
              if($value['Stage Name'] == ""){  $data[$key]['Stage Name'] = "-----";  }
            }

            if(array_key_exists( 'Referred By', $value ) ){
              if($value['Referred By'] == ""){  $data[$key]['Referred By'] = "-----";  }
            }

            if(array_key_exists( 'Opposite Counsel', $value ) ){
              if($value['Opposite Counsel'] == ""){  $data[$key]['Opposite Counsel'] = "-----";  }
            }

            if(array_key_exists( 'Place', $value ) ){
              if($value['Place'] == ""){  $data[$key]['Place'] = "-----";  }
            }
            

            // echo "<pre>";
            // print_r($data);
            // die;

           $i++;
          }

        }

          $sheet->fromArray($data, null, 'A8', false, true);

          $add = $i+3;

            $sheet->row($i+3, array('Address :- '.$admin_details->admin_address ));
            $sheet->row($i+3, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
            $sheet->mergeCells('A'.$add.':'.'F'.$add);

            $mob = $i+4;
            $sheet->row($i+4, array('Mobile No. :- '.$admin_details->admin_number ));
            $sheet->row($i+4, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
            $sheet->mergeCells('A'.$mob.':'.'F'.$mob);

            $email = $i+5;
            $sheet->row($i+5, array('Email Id :- '.$admin_details->admin_email ));
            $sheet->row($i+5, function($row) {$row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
            $sheet->mergeCells('A'.$email.':'.'F'.$email);

        
        });

      })->download('xlsx');


      
      return redirect('advocate-panel/view-case-registration-form');
    
    }


    

}
