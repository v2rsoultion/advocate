<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Model\Admin\Admin; // model name
use App\Model\Admin\Account; // model name
use App\Model\SmsText\SmsText; // model name
use App\Http\Controllers\Controller;
use DB;
use Mail;
class HomeController extends Controller
{

	public function __construct()
    {
        $this->send_message = new \App\Http\Controllers\Admin\AdminController;
    }


    public function home(){

        if(session('admin_id') != ""){
            $get_record = Admin::where('admin_id','=', session('admin_id') )->first();
        } else {
            $get_record = "";
        }


      $title         = 'Ideal Lawyer';
      $data          = compact('title','get_record');
      return view('index',$data);	
    }


    public function sign_up(){

    	if(session('admin_id') != ""){
    		return redirect('advocate-panel/dashboard');
    	}

      $title         = 'Sign Up | Ideal Lawyer';
      $data          = compact('title');
      return view('signup',$data);	
    }


    public function submit_signup(Request $request){

    	$get_record = Admin::where('admin_email','=', $request['email'] )->count();

    	$account = Account::first();

    	if($get_record == 0){

    		if($request->otp == ""){

    			$customer_otp = substr(str_shuffle('1234567890'),0,6);

    			$get_otp =  DB::table('otp')->where(['phone_number' => $request->phone_number])->count();

    			if($get_otp == 0){
    				DB::table('otp')->insert(['phone_number' => $request->phone_number , 'otp' => $customer_otp ]);
    			} else {
    				DB::table('otp')->where(['phone_number' => $request->phone_number])->update([ 'otp' => $customer_otp ]);
    			}


    			$case_registration =  SmsText::where(['sms_content_id' => 1 ])->first();

			    // if($case_registration->sms_content_before != ""){
			    //   $message = $case_registration->sms_content_before;
			    // }

			    $message = "Your one time OTP : ".$customer_otp;

			    // if($case_registration->sms_content_after != ""){
			    //   $message = $message.' '.$case_registration->sms_content_after;
			    // }

	            $mobile_no  = $request->phone_number;
	            $this->send_message->send_message($mobile_no,$message);

	            return response()->json(['status'=>true,'message'=>'Invalid Token']); 
    		
    		} else {

    			$get_otp =  DB::table('otp')->where(['phone_number' => $request->phone_number , 'otp' => $request->otp ])->count();

    			if($get_otp == 0){
    				return response()->json(['status'=>'2','message'=>'Invalid Token']); 
    			}

    			$input = array(
		          'case_registration'       => 1, 
		          'pessi_cause_list'        => 1, 
		          'daily_cause_list'        => 1, 
		          'sms_to_clients'          => 1, 
		          'order_judgement_upload'  => 1, 
		          'complaince'              => 1, 
		          'calendar'                => 1, 
		          'undated_case'            => 1, 
		          'reporting'               => 1, 
		          'due_course'              => 1, 
		          'master_modules'          => 1
		        );

		        $special_permission = json_encode(array_filter($input));

		        $Add = new Admin;  
		        $Add->admin_name            = $request['name'];
		        $Add->admin_email           = $request['email'];
		        $Add->admin_number          = $request['phone_number'];
		        $Add->admin_address         = $request['address'];
		        $Add->start_date            = date('Y-m-d');
		        $Add->end_date              = date('Y-m-d',strtotime("+".$account->account_free_trail." days"));
		        $Add->admin_password        = md5($request['password']);
		        $Add->admin_type            = 2;
		        $Add->special_permission    = $special_permission;
		        $Add->save();
		        $admin_id = $Add->id;

                $data=['name'=> $request->name , 'email'=> $request->email, 'password'=> $request->password, 'start_date'=> date('d M Y'), 'end_date'=> date('d M Y',strtotime("+".$account->account_free_trail." days")) ];

                $_REQUEST['name'] = $request->name;
                $_REQUEST['email'] = $request->email;

                Mail::send(['html' => 'advocate_admin/registration_mail'], $data , function ($message) {
                  $message->to($_REQUEST['email'], $_REQUEST['name'])->subject('Registration');
                });
		    	
		    	return response()->json(['status'=>'3','message'=>'Your account has been successfully created for '.$account->account_free_trail.' days.']); 


    		} 

    	 
    	} else {
    		return response()->json(['status'=>false,'message'=>'Invalid Token']);  
    	}

    }



    public function resend_otp(Request $request , $mobile_number){


        $customer_otp = substr(str_shuffle('1234567890'),0,6);

        $get_otp =  DB::table('otp')->where(['phone_number' => $mobile_number])->count();

        if($get_otp == 0){
            DB::table('otp')->insert(['phone_number' => $mobile_number , 'otp' => $customer_otp ]);
        } else {
            DB::table('otp')->where(['phone_number' => $mobile_number])->update([ 'otp' => $customer_otp ]);
        }


        $case_registration =  SmsText::where(['sms_content_id' => 1 ])->first();

        if($case_registration->sms_content_before != ""){
          $message = $case_registration->sms_content_before;
        }

        $message = "Your one time OTP : ".$customer_otp;

        if($case_registration->sms_content_after != ""){
          $message = $message.' '.$case_registration->sms_content_after;
        }

        $mobile_no  = $mobile_number;
        $this->send_message->send_message($mobile_no,$message);

        return response()->json(['status'=>true,'message'=>'Invalid Token']); 

    }




    public function login(){

    	if(session('admin_id') != ""){
    		return redirect('advocate-panel/dashboard');
    	}

      $title         = 'Sign In | Ideal Lawyer';
      $data          = compact('title');
      return view('login',$data);	
    }


    public function submit_login(Request $request){

    	$get_record = Admin::where(['admin_email' => $request['email'], 'admin_password' => md5($request['password']) ])->first();

    	if(count($get_record) == 0){
	        return response()->json(['status'=>0,'message'=>'Invalid Token']); 	
    	} else {	

    		if($get_record->admin_status == 0){
                return response()->json(['status'=>1,'message'=>'Invalid Token']); 	
            }


            if($get_record->admin_type == 2){

                $today_date = date("Y-m-d");

                if($get_record->start_date <= $today_date && $get_record->end_date >= $today_date){

                    session(['admin_id' => $get_record->admin_id]);
                    $admin_id = session('admin_id');
                    return response()->json(['status'=>2,'message'=>'Invalid Token']);

                }

                if($today_date < $get_record->start_date ){
                	return response()->json(['status'=>3,'message'=>"You will be able to login on " .date('d F Y',strtotime($get_record->start_date))."."]);
                }

                if($today_date > $get_record->end_date){

                    return response()->json(['status'=>3,'message'=>"You account has been expired on " .date('d F Y',strtotime($get_record->end_date))."."]);

                } 

            //    your account will be activated from 
            } else {

            	session(['admin_id' => $get_record->admin_id]);
				$admin_id = session('admin_id');
				return response()->json(['status'=>2,'message'=>'Invalid Token']); 	

            }


		    // return response()->json(['status'=>'3','message'=>'Your account has been successfully created for '.$account->account_free_trail.' days.']); 
    	} 
    }


    public function forgot_password(){

    	if(session('admin_id') != ""){
    		return redirect('advocate-panel/dashboard');
    	}

      $title         = 'Forgot Password | Ideal Lawyer';
      $data          = compact('title');
      return view('forgot_password',$data);	
    }


    public function submit_forgot_password(Request $request){

    	$get_record = Admin::where(['admin_email' => $request['email'] ])->first();

    	if(count($get_record) == 0){
	        return response()->json(['status'=>0,'message'=>'Invalid Tffoken']); 	
    	} else {	

    		if($get_record->admin_status == 0){
                return response()->json(['status'=>1,'message'=>'Invalid Token']); 	
            }

            $_REQUEST['admin_name']  = $get_record['admin_name'];
            $_REQUEST['admin_email'] = $get_record['admin_email'];

            if($get_record->admin_type == 2){

                $today_date = date("Y-m-d");

                if($get_record->start_date <= $today_date && $get_record->end_date >= $today_date){

                	$data=['admin_id'=> sha1($get_record['admin_id']) ];
                    Mail::send(['html' => 'forgot_mail'], $data , function ($message) {
                    $message->to($_REQUEST['admin_email'], $_REQUEST['admin_name'])->subject('Password Change Request');
                    });

                    $values=array( 'reset_password_status' => 0 );
                    Admin::where('admin_email', $request['email'])->update($values);
                    
                    // session(['admin_id' => $get_record->admin_id]);
                    // $admin_id = session('admin_id');
                    return response()->json(['status'=>2,'message'=>'Invalid Token']);

                }

                if($today_date < $get_record->start_date ){
                	return response()->json(['status'=>3,'message'=>"You will be able to login on " .date('d F Y',strtotime($get_record->start_date))."."]);
                }

                if($today_date > $get_record->end_date){

                    return response()->json(['status'=>3,'message'=>"You account has been expired on " .date('d F Y',strtotime($get_record->end_date))."."]);

                } 

            //    your account will be activated from 
            } else {

            	$data=['admin_id'=> sha1($get_record['admin_id']) ];
                Mail::send(['html' => 'forgot_mail'], $data , function ($message) {
                $message->to($_REQUEST['admin_email'], $_REQUEST['admin_name'])->subject('Password Change Request');
                });

                $values=array( 'reset_password_status' => 0 );
                Admin::where('admin_email', $request['admin_email'])->update($values);

    //         	session(['admin_id' => $get_record->admin_id]);
				// $admin_id = session('admin_id');
				return response()->json(['status'=>2,'message'=>'Invalid Token']); 	

            }

    	} 

    }


    public function reset_password(Request $request, $id) {

      $get_record   = DB::select('select * from adv_admin where sha1(admin_id) = "'.$id.'" ');
      
      $account     = Account::get();
      if(empty(count($get_record))){
        return redirect()->back()->withSuccess('IT WORKS!');
      } else {
		if($request['admin_password'] != ""){

			if($request['admin_password'] == $request['admin_cpassword']){
			
				$values=array(
				  'admin_password'          => md5($request['admin_password']),
				);
				Admin::where('admin_id', $get_record[0]->admin_id)->update($values);
				$success_msg = "Password Reset Successfully";

				return response()->json(['status'=>2,'message'=>'Invalid Token']); 	
			} else {
				$error_msg  = "Password and Confirm Password doesn't match !!";
			}
		}

        $title       = "Reset Password";
        $admin_id  = $get_record[0]->admin_id;
        $data         = compact('title','admin_id','account','error_msg','success_msg','get_record' );
        return view('reset_password',$data);
      }
    
    }


    public function submit_reset_password(Request $request) {     

    	// print_r($request->input());
    	// die; 

			$values=array(
			  'admin_password'          => md5($request['password']),
			  'reset_password_status'     => 1, 
			);
			Admin::where('admin_id', $request['admin_id'])->update($values);
			$success_msg = "Password Reset Successfully";

			return response()->json(['status'=>2,'message'=>'Invalid Token']); 	
		
    
    }

    public function term_services(){

      $title         = 'Terms of Services';
      $data          = compact('title');
      return view('term-of-service',$data);   
    }

    public function privacy_policy(){

      $title         = 'Privacy Policy';
      $data          = compact('title');
      return view('privacy-policy',$data);   
    }





}
