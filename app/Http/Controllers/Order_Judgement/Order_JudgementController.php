<?php
namespace App\Http\Controllers\Order_Judgement;

use Illuminate\Http\Request;
use App\Model\Case_Registration\Case_Registration; // model name
use App\Model\Case_Type_Entry\Case_Type_Entry; // model name
use App\Model\Class_Code\Class_Code; // model name
use App\Model\Assigned\Assigned; // model name
use App\Model\Reffered\Reffered; // model name
use App\Model\Section\Section; // model name
use App\Model\Client\Client; // model name
use App\Model\Admin\Account; // model name
use App\Model\Stage\Stage; // model name
use App\Model\Court\Court; // model name
use App\Model\Compliance\Compliance; // model name
use App\Model\Sub_Client\Sub_Client; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Act\Act; // model name
use App\Model\Upload_Document\Upload_Document; // model name
use App\Model\Judge\Judge; // model name
use App\Model\Fir\Fir; // model name
use App\Model\Pessi\Pessi; // model name
use App\Http\Controllers\Controller;
use DB;
use Excel;

class Order_JudgementController extends Controller
{

    public function __construct()
      {
          $this->middleware('Validation');
          $this->send_message = new \App\Http\Controllers\Admin\AdminController;
      }
      
    //add order judgement form

    public function add_order_judgment($order_judgment_id = false){
    //  $this->pessi->hello($pa1,);
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if(isset(json_decode($admin_details[0]->special_permission)->order_judgement_upload)){

      } else {
        return Redirect()->back();
      }

      $admin_id = session('admin_id');

      if($order_judgment_id !=''){

        $get_record = DB::select('select * from adv_document_uploads where sha1(upload_id) = "'.$order_judgment_id.'" and user_admin_id = "'.$admin_id.'" ');

        if(count($get_record) == 0){
          return Redirect()->back();
        }

      } else {
        $get_record   = "";
      }
    
      $case_type_entry    = Case_Type_Entry::where([ 'case_status' => 1 , 'user_admin_id' => $admin_id ])->where('case_name' ,'!=', '')->get();

      // echo "<pre>";
      // print_r($case_type_entry);
      // die;

      $case_registration  = Case_Registration::where(['user_admin_id' => $admin_id ])->where('reg_case_number' ,'!=', '')->get();
      
      $court_all  = Court::where([ 'court_status' => 1 , 'user_admin_id' => $admin_details[0]->admin_id ])->where('court_name' ,'!=', '')->get();
      
      $title     = 'Add Order Judgment';
      $data      = compact('title','edit_id','admin_details','get_record','case_type_entry','case_registration','court_all');
      return view('advocate_admin/add_order_judgement',$data);	
    }


    // order judgement case type

    public function order_judgement_case_type(Request $request , $court_name = false , $court_type = false){
      
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      
      if($court_name == "" && $court_type == "" ){
        $case_type_entry = Case_Registration::leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->where('adv_case_reg.user_admin_id','=',session('admin_id'))->where('case_name' ,'!=',"")->groupBy('case_id')->get();
      
      } else if($court_name == ""){
        $case_type_entry = Case_Registration::leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->where('adv_case_reg.user_admin_id','=',session('admin_id'))->where('case_name' ,'!=',"")->groupBy('case_id')->get();
      
      } else if($court_type == "" ){
        $case_type_entry = Case_Registration::leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->where('adv_case_reg.user_admin_id','=',session('admin_id'))->where([ 'reg_court_id' => $court_name ])->where('case_name' ,'!=',"")->groupBy('case_id')->get();
      
      } else if($court_name != "" && $court_type != "" ){
        $case_type_entry = Case_Registration::leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->where('adv_case_reg.user_admin_id','=',session('admin_id'))->where(['reg_court' => $court_type , 'reg_court_id' => $court_name ])->where('case_name' ,'!=',"")->groupBy('case_id')->get();
      
      } 
      
      $data  = compact('title','admin_details','get_record','case_type_entry');
      return view('advocate_admin/get_type_case',$data);
    }


    
    public function get_all_caption(Request $request){

      $admin_id = session('admin_id');

      $get_caption = Upload_Document::where('upload_caption','LIKE','%'.$request['caption'].'%')->where(['user_admin_id' => $admin_id ])->get();
      
      $data  = compact('get_caption');
      
      return view('advocate_admin/get_all_caption_ajax',$data); 

    }    
    //view order judgement form

    public function view_order_judgment(Request $request,$search_type=false){
      
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if(isset(json_decode($admin_details[0]->special_permission)->order_judgement_upload)){

      } else {
        return Redirect()->back();
      }

      // print_r($request->input());
      // die;
      
      if($request['check'] != ""){
        Upload_Document::whereIn('upload_case_id', $request['check'])->delete();
        return Redirect()->back();
      }

      $query = Upload_Document::leftJoin('adv_case_type', function($join) {$join->on('upload_case_type', '=', 'case_id');})->leftJoin('adv_case_reg', function($join) {$join->on('reg_id', '=', 'upload_case_id');})->leftJoin('adv_court', function($join) {$join->on('court_id', '=', 'reg_court_id');})->where('adv_document_uploads.user_admin_id','=',session('admin_id'));      

      if($request['court_type'] != ""){
        $query = $query->where('reg_court', '=', $request['court_type']);
      }

      if($request['court_name'] != ""){
        $query = $query->where('reg_court_id', '=', $request['court_name']);
      }

      if($request['type'] != ""){
        $query = $query->where('upload_case_type', '=', $request['type']);
      }

      if($request['case_year'] != ""){
        $query = $query->where('upload_case_year', '=', $request['case_year']);
      }

      if($request['case_no'] != ""){
        $query = $query->where('upload_case_id', '=', $request['case_no']);
      }

      if($request['file_no'] != ""){
        $query = $query->where('upload_case_id', '=', $request['file_no']);
      }

      if($request['petitioner_name'] != ""){
        $query = $query->where('reg_petitioner', 'LIKE', '%'.$request['petitioner_name'].'%');
      }
      if($request['respondent'] != ""){
        $query = $query->where('reg_respondent', 'LIKE', '%'.$request['respondent'].'%');
      }

      if($request['caption'] != ""){
        $query = $query->where('upload_caption', 'LIKE', '%'.$request['caption'].'%' );
      }

      if($request['from_date'] != ""){
        $query = $query->where('upload_date', '>=', date('Y-m-d',strtotime($request['from_date'])) );
      }

      if($request['to_date'] != ""){
        $query = $query->where('upload_date', '<=', date('Y-m-d',strtotime($request['to_date'])) );
      }


    //   [court_type] => 
    // [court_name] => 
    // [type] => 
    // [case_no] => 
    // [file_no] => 
    // [respondent] => 
    // [petitioner_name] => 
    // [caption] => 
    // [from_date] => 
    // [to_date] => 
    // [search] => 


      
      $app_data =  array('court_type' => $request['court_type'],'court_name' => $request['court_name'],'type' => $request['type'],'case_year' => $request['case_year'],'case_no' => $request['case_no'],'file_no' => $request['file_no'],'respondent' => $request['respondent'],'petitioner_name' => $request['petitioner_name'],'caption' => $request['caption'],'from_date' => $request['from_date'] ,'to_date' => $request['to_date'] );

      if($search_type == "all"){
        $perpage    = $query->count();
        $get_record = $query->orderBy('upload_id', 'desc')->groupBy('reg_id')->paginate($perpage)->appends($app_data);
      } else {
        $get_record = $query->orderBy('upload_id', 'desc')->groupBy('reg_id')->paginate(25)->appends($app_data);
      }

      
      // print_r($get_record);
      // die;

      $case_type_entry    = Case_Type_Entry::where(['user_admin_id' => session('admin_id') ])->where('case_name','!=',"")->get();
      $case_registration  = Case_Registration::where(['user_admin_id' => session('admin_id') ])->where('reg_case_number' ,'!=', '')->get();

      $court_all  = Court::where([ 'court_status' => 1 , 'user_admin_id' => $admin_details[0]->admin_id ])->where('court_name' ,'!=', '')->get();

      $court_type               = $request['court_type'];
      $court_name               = $request['court_name'];
      $reg_case_type_id         = $request['type'];
      $case_year                = $request['case_year'];
      $order_judgment_case_no   = $request['case_no'];
      $order_judgment_file_no   = $request['file_no'];
      $from_date                = $request['from_date'];
      $to_date                  = $request['to_date'];
      $caption                  = $request['caption'];
      $pet_name                 = $request['petitioner_name'];
      $res_name                 = $request['respondent'];

      $title = 'View Order Judgment';
      $data  = compact('title','admin_details','get_record','case_type_entry','case_registration','reg_case_type_id','case_year','order_judgment_case_no','order_judgment_file_no','from_date','to_date','caption','court_all','court_type','court_name','pet_name','res_name');
      return view('advocate_admin/view_order_judgement',$data);	
    }


    // function validateDate($check_date, $format = 'd-m-Y')
    // {
    //     $d = DateTime::createFromFormat($format, $check_date);
    //     return $d && $d->format($format) === $check_date;
    // }

    //insert order judgement function

    public function insert_order_judgment(Request $request, $order_judgment_id = false){  
      
      if($order_judgment_id==''){

        foreach ($request->document_upload as $document_upload) {

          $upload_date = date('Y-m-d',strtotime($document_upload['upload_date']));

        //  $check_date = $document_upload['upload_date'];
          if($upload_date == "1970-01-01"){
            return redirect()->back()->with('error', 'Enter date in dd-mm-yyyy format.')->withInput();
          }
          // print_r($document_upload['upload_images']->getClientOriginalExtension());
           

          $Add = new Upload_Document;  
          $Add->upload_case_type        = $request['order_judgment_case_type'];
          $Add->upload_case_year        = $request['order_judgment_case_year'];
          $Add->upload_court_type       = $request['court_type'];
        //  $Add->upload_file_no          = $request['order_judgment_case_year'];
          $Add->upload_case_id          = $request['order_judgment_file_no'];
          $Add->user_admin_id           = session('admin_id');
          $Add->upload_caption          = $document_upload['upload_caption'];
          $Add->upload_date             = $upload_date;
          $Add->save();
          $image_id = $Add->id;
               
          if($document_upload['upload_images'] != ""){

            if($document_upload['upload_images']->getClientOriginalExtension() == "jpg" || $document_upload['upload_images']->getClientOriginalExtension() == "jpeg" || $document_upload['upload_images']->getClientOriginalExtension() == "png" || $document_upload['upload_images']->getClientOriginalExtension() == "gif" || $document_upload['upload_images']->getClientOriginalExtension() == "docx" || $document_upload['upload_images']->getClientOriginalExtension() == "doc" || $document_upload['upload_images']->getClientOriginalExtension() == "txt" || $document_upload['upload_images']->getClientOriginalExtension() == "xls" || $document_upload['upload_images']->getClientOriginalExtension() == "xlsx" || $document_upload['upload_images']->getClientOriginalExtension() == "pdf" ){

              $destinationPath = 'uploads/case_registration';
              $imageName = 'uploads/case_registration/'.time().$image_id.'.'.$document_upload['upload_images']->getClientOriginalExtension();
              $document_upload['upload_images']->move($destinationPath,$imageName);
              $image=array(
                'upload_images'         => $imageName
              );
              Upload_Document::where(['upload_id'=> $image_id])->update($image);
            
            }

          }
        }

      //  die;

        //return redirect('advocate-panel/view-order-judgment');
        return redirect()->back()->with('success', 'Your Data has been added successfully');
    
      } else {

        foreach ($request->document_upload as $document_upload) {

          $upload_date = date('Y-m-d',strtotime($document_upload['upload_date']));

        //   print_r($document_upload['upload_id']);
        // die;



          if($document_upload['upload_id'] != ""){
            
            $update_documents = array(
              'upload_court_type'     => $request['court_type'],
              'upload_case_type'      => $request['order_judgment_case_type'],
              'upload_file_no'        => $request['order_judgment_file_no'],
              'upload_case_id'        => $request['order_judgment_file_no'],
              'upload_caption'        => $document_upload['upload_caption'],
              'upload_date'           => $upload_date
            );


            Upload_Document::where(['upload_id'=> $document_upload['upload_id']])->update($update_documents);


            if($document_upload['upload_images'] != ""){

              if($document_upload['upload_images']->getClientOriginalExtension() == "jpg" || $document_upload['upload_images']->getClientOriginalExtension() == "jpeg" || $document_upload['upload_images']->getClientOriginalExtension() == "png" || $document_upload['upload_images']->getClientOriginalExtension() == "gif" || $document_upload['upload_images']->getClientOriginalExtension() == "docx" || $document_upload['upload_images']->getClientOriginalExtension() == "doc" || $document_upload['upload_images']->getClientOriginalExtension() == "txt" || $document_upload['upload_images']->getClientOriginalExtension() == "xls" || $document_upload['upload_images']->getClientOriginalExtension() == "xlsx" || $document_upload['upload_images']->getClientOriginalExtension() == "pdf" ){

                $destinationPath = 'uploads/case_registration';
                $imageName = 'uploads/case_registration/'.time().$document_upload['upload_id'].'.'.$document_upload['upload_images']->getClientOriginalExtension();
                $document_upload['upload_images']->move($destinationPath,$imageName);
                $image=array(
                  'upload_images'         => $imageName
                );
                Upload_Document::where(['upload_id'=> $document_upload['upload_id'] ])->update($image);

              }
              
            }


          } else {

            // if($document_upload['upload_caption'] == "" && $document_upload['upload_date'] == "" && $document_upload['upload_caption'] == ""){

            // } else {

              $Add = new Upload_Document;  
              $Add->upload_case_type        = $request['order_judgment_case_type'];
              $Add->upload_case_year        = $request['order_judgment_case_year'];
              $Add->upload_case_id          = $request['order_judgment_file_no'];
              $Add->user_admin_id           = session('admin_id');
              $Add->upload_caption          = $document_upload['upload_caption'];
              $Add->upload_date             = $upload_date;
              $Add->save();
              $image_id = $Add->id;

              if($document_upload['upload_images'] != ""){

                if($document_upload['upload_images']->getClientOriginalExtension() == "jpg" || $document_upload['upload_images']->getClientOriginalExtension() == "jpeg" || $document_upload['upload_images']->getClientOriginalExtension() == "png" || $document_upload['upload_images']->getClientOriginalExtension() == "gif" || $document_upload['upload_images']->getClientOriginalExtension() == "docx" || $document_upload['upload_images']->getClientOriginalExtension() == "doc" || $document_upload['upload_images']->getClientOriginalExtension() == "txt" || $document_upload['upload_images']->getClientOriginalExtension() == "xls" || $document_upload['upload_images']->getClientOriginalExtension() == "xlsx" || $document_upload['upload_images']->getClientOriginalExtension() == "pdf" ){

                  $destinationPath = 'uploads/case_registration';
                  $imageName = 'uploads/case_registration/'.time().$image_id.'.'.$document_upload['upload_images']->getClientOriginalExtension();
                  $document_upload['upload_images']->move($destinationPath,$imageName);
                  $image=array(
                    'upload_images'         => $imageName
                  );
                  Upload_Document::where(['upload_id'=> $image_id])->update($image);
                }

             // }

            }

          }       
        }

        //return redirect('advocate-panel/view-order-judgment');
        return redirect()->back()->with('success', 'Your Data has been updated successfully');
      }

    }

    // ajax_case_type

    public function ajax_case_type($case_type = false , $court_type = false){

      $query  = Case_Registration::where(['user_admin_id' => session('admin_id')])->where('reg_case_number' ,'!=', '');
      
      if($case_type != ""){
        $query = $query->where('reg_case_type_id', $case_type);
      }

      if($court_type != ""){
        $query = $query->where('reg_court', $court_type);
      }

      $case_registration = $query->get();

      $data  = compact('case_registration');
      return view('advocate_admin/ajax_case_type',$data); 

    }

    // ajax_case_type

    public function ajax_case_year($case_year = false,$case_type = false){

      $query  = Case_Registration::where(['user_admin_id' => session('admin_id') ])->where('reg_case_number' ,'!=', '');
      
      if($case_type != ""){
        $query = $query->where('reg_case_type_id', $case_type);
      }

      if($case_year != ""){
        $query = $query->whereYear('reg_date', $case_year);
      }

      $case_registration = $query->get();


      $data  = compact('case_registration');
      return view('advocate_admin/ajax_case_type',$data); 

    }

    // ajax_case_no

    public function ajax_case_no($case_no = false){

      if($case_no != ""){
        $case_registration  = Case_Registration::where(['user_admin_id' => session('admin_id') , 'reg_id' => $case_no ])->where('reg_file_no' ,'!=', '')->get();
      } else {
        $case_registration  = Case_Registration::where(['user_admin_id' => session('admin_id') ])->where('reg_file_no' ,'!=', '')->get();
      }

      $data  = compact('case_registration');
      return view('advocate_admin/ajax_case_no',$data); 

    }


    // delete document

    public function delete_document($upload_id){
      Upload_Document::where(['upload_id'=> $upload_id])->delete();
    }

    //case registration status

    public function case_registration_status($id,$value){

      $admin_id = session('admin_id');
      $get_record = DB::select('select * from adv_case_reg where sha1(reg_id) = "'.$id.'" and user_admin_id = "'.$admin_id.'" ');

      if(count($get_record) == 0){
        return Redirect()->back();
      }

      Case_Registration::where('reg_id', $get_record[0]->reg_id)->update(['reg_status' => $value , 'reg_disposal_date' => date('Y-m-d') ]);
      return Redirect()->back();
    }


    //view case registration form

    public function case_registration_details(Request $request, $registration_id){

      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      $get_case_registration = DB::select('select * from adv_case_reg where sha1(reg_id) = "'.$registration_id.'" and user_admin_id = "'.$admin_details[0]->admin_id.'" ');

      $get_case_details = Case_Registration::leftJoin('adv_pessi', function($join) {$join->on('pessi_case_reg', '=', 'reg_id');})->leftJoin('adv_stage', function($join) {$join->on('stage_id', '=', 'reg_stage_id');})->leftJoin('adv_court', function($join) {$join->on('court_id', '=', 'reg_court');})->leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->leftJoin('adv_class_code', function($join) {$join->on('classcode_id', '=', 'reg_case_subtype_id');})->leftJoin('adv_fir', function($join) {$join->on('fir_id', '=', 'reg_fir_id');})->leftJoin('adv_assigned', function($join) {$join->on('assign_id', '=', 'reg_assigend_id');})->leftJoin('adv_act', function($join) {$join->on('act_id', '=', 'reg_act_id');})->leftJoin('adv_section', function($join) {$join->on('section_id', '=', 'reg_section_id');})->leftJoin('adv_referred', function($join) {$join->on('ref_id', '=', 'reg_reffeerd_by_id');})->leftJoin('adv_client', function($join) {$join->on('cl_id', '=', 'reg_client_group_id');})->leftJoin('adv_sub_client', function($join) {$join->on('sub_client_id', '=', 'reg_client_subgroup_id');})->where(['adv_case_reg.reg_id' => $get_case_registration[0]->reg_id , 'pessi_status' => '1' ])->first();



      $pessi_detail = Pessi::leftJoin('adv_case_reg', function($join) {$join->on('pessi_case_reg', '=', 'reg_id');})->leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->leftJoin('adv_stage', function($join) {$join->on('stage_id', '=', 'pessi_statge_id');})->leftJoin('adv_judge', function($join) {$join->on('judge_id', '=', 'honaurable_justice');})->where('pessi_user_id', $admin_details[0]->admin_id)->where('pessi_case_reg', $get_case_details->reg_id)->orderBy('pessi_id', 'desc')->get(); 


      $compliance_detail = Compliance::leftJoin('adv_case_reg', function($join) {$join->on('compliance_case_id', '=', 'reg_id');})->leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'compliance_case_type');})->where('adv_compliance.user_admin_id','=',session('admin_id'))->where('compliance_case_id', $get_case_details->reg_id)->orderBy('compliance_id', 'desc')->get();

      // ->->where('adv_case_reg.user_admin_id','=',session('admin_id'))->first();
      
      // print_r($get_case_details);
      // die;


    //  $case_type_entry      = Case_Type_Entry::get();      
      
      $title = 'Case Details';
      $data  = compact('title','admin_details','get_case_details','case_type_entry','pessi_detail','compliance_detail');
      return view('advocate_admin/case_registration_detail',$data); 
    }


  // download_order_judgement

    public function download_order_judgement(Request $request){

      // print_r($request->input());
      // die;

      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      $query = Upload_Document::leftJoin('adv_case_type', function($join) {$join->on('upload_case_type', '=', 'case_id');})->leftJoin('adv_case_reg', function($join) {$join->on('reg_id', '=', 'upload_case_id');})->leftJoin('adv_court', function($join) {$join->on('court_id', '=', 'reg_court_id');})->where('adv_document_uploads.user_admin_id','=',session('admin_id'));      

      if($request['court_type'] != ""){
        $query = $query->where('reg_court', '=', $request['court_type']);
      }

      if($request['court_name'] != ""){
        $query = $query->where('reg_court_id', '=', $request['court_name']);
      }

      if($request['reg_case_type_id'] != ""){
        $query = $query->where('upload_case_type', '=', $request['reg_case_type_id']);
      }

      if($request['order_judgment_case_no'] != ""){
        $query = $query->where('upload_case_id', '=', $request['order_judgment_case_no']);
      }

      if($request['order_judgment_file_no'] != ""){
        $query = $query->where('upload_case_id', '=', $request['order_judgment_file_no']);
      }

      if($request['pet_name'] != ""){
        $query = $query->where('reg_petitioner', 'LIKE', '%'.$request['pet_name'].'%');
      }
      if($request['res_name'] != ""){
        $query = $query->where('reg_respondent', 'LIKE', '%'.$request['res_name'].'%');
      }

      if($request['caption'] != ""){
        $query = $query->where('upload_caption', 'LIKE', '%'.$request['caption'].'%' );
      }

      if($request['from_date'] != ""){
        $query = $query->where('upload_date', '>=', date('Y-m-d',strtotime($request['from_date'])) );
        $date = "From:- ".$request['from_date'];
      }

      if($request['to_date'] != ""){
        $query = $query->where('upload_date', '<=', date('Y-m-d',strtotime($request['to_date'])) );
        $date = "To:- ".$request['to_date'];
      }

      $data = $query->orderBy('upload_date', 'ASC')->get()->toArray();

      if($request['from_date'] == "" && $request['to_date'] == "" ){
        $date = "All Records";
      } else if($request['from_date'] != "" && $request['to_date'] != "" ){
        $date = "From:- ".$request['from_date']. " To:- ".$request['to_date'];
      }
//      $date = date("d/m/Y");

      if($request['court_type'] == 1){
        $court_name = "Trail Court";
      } else if($request['court_type'] == 2){
        $court_name = "High Court";
      } else {
        $court_name = "Both";
      }

        Excel::create('Order Judgement Upload', function($excel) use ($data,$admin_details,$date,$court_name) {
        $excel->sheet('Sheet', function ($sheet) use ($data,$admin_details,$date,$court_name) {


//          $sheet->fromArray($data, null, 'F14', false, false)->getStyle('F14')->getAlignment()->setWrapText(true);


          $sheet->setOrientation('landscape');
          $sheet->mergeCells('A1:F1');
          $sheet->mergeCells('A2:F2');
          $sheet->mergeCells('A3:F3');
          $sheet->mergeCells('A4:F4');
          $sheet->mergeCells('A5:F5');
          $sheet->mergeCells('A6:B6');
          $sheet->mergeCells('C6:E6');
          $sheet->setHeight(array(
            1     =>  20,
            2     =>  20,
          ));
          $sheet->setSize('A8', 10, 18);
          $sheet->setSize('B8', 20, 18);
          $sheet->setSize('C8', 15, 18);
          $sheet->setSize('D8', 20, 18);
          $sheet->setSize('E8', 40, 18);
          $sheet->setSize('F8', 40, 18);
          $sheet->setSize('G8', 15, 18);
          $sheet->row(1, array( 'OFFICE OF' ));
          $sheet->row(1, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          $sheet->row(2, array( $admin_details[0]->admin_firmname ));
          $sheet->row(2, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          $sheet->row(3, array( $admin_details[0]->admin_advocates ));
          $sheet->row(3, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          $sheet->row(4, array( '(Advocates)' ));
          $sheet->row(4, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          $day = date("l");
          
          $sheet->row(6, array( $date, '','              Order Judgement Upload', '','' , $court_name));
          $sheet->row(6, function($row) { $row->setAlignment('center');} );
          $sheet->mergeCells('A7:F7');
          $sheet->row(8, array( 'S.l. No.','Court Name', 'Type of Case', 'Case No.', 'Title' , 'Caption' , 'Date'));
          $sheet->row(8, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
          $sheet->row(9, array('', '', '', '', ''));
          $sheet->mergeCells('A9:F9');
          // $sheet->setAutoSize(true);
           
          $i=10;

          if(empty($data) ){

            $sheet->mergeCells('A10:G10');
            $sheet->mergeCells('A11:G11');
            $sheet->mergeCells('A12:G12');
            $sheet->row(11, array('No Record Found'));
            $sheet->row(11, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('center');});
          } else {

            foreach ($data as $keys => $values) {

              $title = $values['reg_petitioner'].' v/s '.$values['reg_respondent'];

              $pre_date  = date('d/m/Y',strtotime($values['pessi_prev_date']));

              $upload_date = date('d/m/Y',strtotime($values['upload_date']));

              if($upload_date == "01/01/1970"){
                $upload_date = "-----";
              }
              
              if($value['court_name'] == ""){
                $value['court_name'] = "-";
              }

              if($value['case_name'] == ""){
                $value['case_name'] = "-";
              }

              if($value['reg_case_number'] == ""){
                $value['reg_case_number'] = "-";
              }
              if($value['upload_caption'] == ""){
                $value['upload_caption'] = "-";
              }
              

              $sheet->row($i, array($i-9,$values['court_name'], $values['case_name'], $values['reg_case_number'], $title,$values['upload_caption'], $upload_date));
              $sheet->row($i, function($row) {$row->setAlignment('left');});

              $i++;

            }
          }

            $add = $i+3;

            $sheet->row($i+3, array('Address :- '.$admin_details[0]->admin_address ));
            $sheet->row($i+3, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
            $sheet->mergeCells('A'.$add.':'.'F'.$add);

            $mob = $i+4;
            $sheet->row($i+4, array('Mobile No. :- '.$admin_details[0]->admin_number ));
            $sheet->row($i+4, function($row) { $row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
            $sheet->mergeCells('A'.$mob.':'.'F'.$mob);

            $email = $i+5;
            $sheet->row($i+5, array('Email Id :- '.$admin_details[0]->admin_email ));
            $sheet->row($i+5, function($row) {$row->setFontSize(12); $row->setFontWeight('bold'); $row->setAlignment('left');});
            $sheet->mergeCells('A'.$email.':'.'F'.$email);

     //    die;

         });

      })->download('xlsx');
 
    }

    

}
