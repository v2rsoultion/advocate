<?php

namespace App\Http\Controllers\Calendar;

use Illuminate\Http\Request;
use App\Model\Case_Registration\Case_Registration; // model name
use App\Model\Case_Type_Entry\Case_Type_Entry; // model name
use App\Model\Class_Code\Class_Code; // model name
use App\Model\Assigned\Assigned; // model name
use App\Model\Reffered\Reffered; // model name
use App\Model\Section\Section; // model name
use App\Model\Client\Client; // model name
use App\Model\Admin\Account; // model name
use App\Model\Notes\Notes; // model name
use App\Model\Sub_Client\Sub_Client; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Act\Act; // model name
use App\Model\Upload_Document\Upload_Document; // model name
use App\Model\Judge\Judge; // model name
use App\Model\Fir\Fir; // model name
use App\Model\Pessi\Pessi; // model name
use App\Model\Holiday\Holiday; // model name
use App\Http\Controllers\Controller;
use DB;

class CalendarController extends Controller
{

    public function __construct()
      {
          $this->middleware('Validation');
      }


    // view calendar

    public function view_calendar($reg_id = false){
      
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if(isset(json_decode($admin_details[0]->special_permission)->calendar)){

      } else {
        return Redirect()->back();
      }

      
      $admin_id = session('admin_id');

      $progress_case_high = DB::table("adv_case_reg")->select("adv_case_reg.reg_id", "adv_case_reg.reg_date", DB::raw("COUNT(*) as count_pessi"),"adv_pessi.pessi_further_date")->join("adv_pessi","adv_pessi.pessi_case_reg","=","adv_case_reg.reg_id")->groupBy("adv_pessi.pessi_further_date")->where('adv_case_reg.user_admin_id','=',session('admin_id'))->where('pessi_status',1)->where('reg_court',2)->where('pessi_choose_type',0)->get();

      $progress_case_trail = DB::table("adv_case_reg")->select("adv_case_reg.reg_id", "adv_case_reg.reg_date", DB::raw("COUNT(*) as count_pessi"),"adv_pessi.pessi_further_date")->join("adv_pessi","adv_pessi.pessi_case_reg","=","adv_case_reg.reg_id")->groupBy("adv_pessi.pessi_further_date")->where('adv_case_reg.user_admin_id','=',session('admin_id'))->where('pessi_status',1)->where('reg_court',1)->where('pessi_choose_type',0)->get();

      $notes = Notes::where('notes_user_id','=',session('admin_id'))->get();

      $holiday = Holiday::where('holiday_status','=',1)->get();

      // print_r($progress_case_trail);
      // die;
      
      $title             = 'Calendar';
      $data              = compact('title','admin_details','progress_case_high','progress_case_trail','notes','holiday');
      return view('advocate_admin/view_calendar',$data);  
    }


    // insert note function

    public function insert_note(Request $request, $reg_id = false){

      if($request['comment'] != "" && $request['further_date'] != ""){

        $Add = new Notes;  
        $Add->notes_description     = preg_replace( "/\r|\n/", " ", $request['comment'] );
        $Add->notes_date            = date('Y-m-d',strtotime($request['further_date']));
        $Add->notes_user_id         = session('admin_id');
        $Add->save();
        $id = $Add->id;

      }

        return redirect('advocate-panel/view-calendar');
    }

    // delete document

    public function delete_document($upload_id){
      Upload_Document::where(['upload_id'=> $upload_id])->delete();
    }

    //case registration status

    public function case_registration_status($id,$value){

      $admin_id = session('admin_id');
      $get_record = DB::select('select * from adv_case_reg where sha1(reg_id) = "'.$id.'" and user_admin_id = "'.$admin_id.'" ');

      if(count($get_record) == 0){
        return Redirect()->back();
      }

      Case_Registration::where('reg_id', $get_record[0]->reg_id)->update(['reg_status' => $value]);
      return Redirect()->back();
    }


    //view case registration form

    public function case_registration_details(Request $request, $registration_id){

      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      $get_case_registration = DB::select('select * from adv_case_reg where sha1(reg_id) = "'.$registration_id.'" and user_admin_id = "'.$admin_details[0]->admin_id.'" ');

      $get_case_details = Case_Registration::leftJoin('adv_stage', function($join) {$join->on('stage_id', '=', 'reg_stage_id');})->leftJoin('adv_court', function($join) {$join->on('court_id', '=', 'reg_court');})->leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->leftJoin('adv_class_code', function($join) {$join->on('classcode_id', '=', 'reg_case_subtype_id');})->leftJoin('adv_fir', function($join) {$join->on('fir_id', '=', 'reg_fir_id');})->leftJoin('adv_assigned', function($join) {$join->on('assign_id', '=', 'reg_assigend_id');})->leftJoin('adv_act', function($join) {$join->on('act_id', '=', 'reg_act_id');})->leftJoin('adv_section', function($join) {$join->on('section_id', '=', 'reg_section_id');})->leftJoin('adv_referred', function($join) {$join->on('ref_id', '=', 'reg_reffeerd_by_id');})->leftJoin('adv_client', function($join) {$join->on('cl_id', '=', 'reg_client_group_id');})->leftJoin('adv_sub_client', function($join) {$join->on('sub_client_id', '=', 'reg_client_subgroup_id');})->where(['adv_case_reg.reg_id' => $get_case_registration[0]->reg_id ])->first();


      // ->->where('adv_case_reg.user_admin_id','=',session('admin_id'))->first();
      
      // print_r($get_case_details);
      // die;


    //  $case_type_entry      = Case_Type_Entry::get();      
      
      $title = 'Case Registration Details';
      $data  = compact('title','admin_details','get_case_details','case_type_entry');
      return view('advocate_admin/case_registration_detail',$data); 
    }

    

}
