<?php

namespace App\Http\Controllers\Stage;

use Illuminate\Http\Request;
use App\Model\Stage\Stage; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Admin\Account; // model name
use App\Http\Controllers\Controller;
use DB;
class StageController extends Controller
{

    public function __construct()
      {
          $this->middleware('Validation');
      }
      
    //add stage

    public function add_stage($stage_id = false){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if(isset(json_decode($admin_details[0]->special_permission)->master_modules)){

      } else {
        return Redirect()->back();
      }

      $admin_id = session('admin_id');

      if($stage_id!=''){

        $get_record = DB::select('select * from adv_stage where sha1(stage_id) = "'.$stage_id.'" and user_admin_id = "'.$admin_id.'" ');

          if(count($get_record) == 0){
            return Redirect()->back();
          }

          $title = 'Add Stage';
          $data  = compact('title','admin_details','get_record');
          return view('advocate_admin/add_stage',$data);
      }else{
          $get_record = "";
      }      
          $title = 'Add Stage';
          $data  = compact('title','admin_details','get_record');
          return view('advocate_admin/add_stage',$data);
        }

    //view stage

    public function view_stage(Request $request,$type=false){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if(isset(json_decode($admin_details[0]->special_permission)->master_modules)){

      } else {
        return Redirect()->back();
      }
      
      $query = Stage::where('user_admin_id','=',session('admin_id'));

      if($request['check'] != ""){
        Stage::whereIn('stage_id', $request['check'])->delete();
        return Redirect()->back();
      }

      if($request['stage_name'] != ""){
        $query = $query->where('stage_name', 'LIKE', '%'.$request['stage_name'].'%');
      }
      if($request['stage_short_name'] != ""){
        $query = $query->where('stage_short_name', 'LIKE', '%'.$request['stage_short_name'].'%');
      }

      $app_data =  array('stage_name' => $request['stage_name'],'stage_short_name' => $request['stage_short_name']);

      if($type == "all"){
        $perpage    = $query->count();
        $get_record = $query->orderBy('stage_id', 'desc')->paginate($perpage)->appends($app_data); 
      } else {
        $get_record = $query->orderBy('stage_id', 'desc')->paginate(25)->appends($app_data); 
      }

           
      $title = 'View stage';
      $data  = compact('title','admin_details','get_record');
      return view('advocate_admin/view_stage',$data);	
    }


    //insert stage function

    public function insert_stage(Request $request, $stage_id = false){

      if($stage_id==''){

        if($request['stage_name'] == ""){
          return redirect('advocate-panel/add-stage');
        }

        $Add = new Stage;  
        $Add->stage_name           = $request['stage_name'];
        $Add->stage_short_name     = $request['short_name'];
        $Add->user_admin_id        = session('admin_id');
        $Add->stage_status         = 1;
        $Add->save();
        //return redirect('advocate-panel/view-stage');
         return redirect()->back()->with('success', 'Your Data has been added successfully');
      }
    else
    {
      $values  = array(
        'stage_name'             =>  $request['stage_name'],
        'stage_short_name'       =>  $request['short_name']
        );
      Stage::where('stage_id','=',$stage_id)->update($values);
      //return redirect('advocate-panel/view-stage');
       return redirect()->back()->with('success', 'Your Data has been updated successfully');
    }

  }

    //court stage status

    public function stage_status($id,$value){

      $admin_id = session('admin_id');
      $get_record = DB::select('select * from adv_stage where sha1(stage_id) = "'.$id.'" and user_admin_id = "'.$admin_id.'" ');

      if(count($get_record) == 0){
        return Redirect()->back();
      }

      Stage::where('stage_id', $get_record[0]->stage_id)->update(['stage_status' => $value]);
      return Redirect()->back();
    }
}
