<?php

namespace App\Http\Controllers\Other;

use Illuminate\Http\Request;
use App\Model\Other\Other; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Case_Registration\Case_Registration; // model name
use App\Model\Admin\Account; // model name
use App\Http\Controllers\Controller;
use DB;
class OtherController extends Controller
{

    public function __construct()
      {
          $this->middleware('Validation');
      }
      
    //add other

    public function add_other($other_id = false){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();
      $admin_id = session('admin_id');
      
      if($other_id!=''){

          $get_record = DB::select('select * from reg_comp_other where sha1(other_id) = "'.$other_id.'" and user_admin_id = "'.$admin_id.'" ');

          if(count($get_record) == 0){
            return Redirect()->back();
          }

          $other_case_no = Case_Registration::where('reg_status','=',1)->get();      
          $title = 'Add Other';
          $data  = compact('title','admin_details','get_record','other_case_no');
          return view('advocate_admin/add_other',$data);
        }else{
          $get_record = "";
        }
          $other_case_no = Case_Registration::where('reg_status','=',1)->get();      
          $title = 'Add Other';
          $data  = compact('title','admin_details','get_record','other_case_no');
          return view('advocate_admin/add_other',$data);
        }


    // other case no ajax function
    public function other_case_no_ajax($other_caseno_id){
      $arrayName = Case_Registration::where('reg_id','=',$other_caseno_id)->first();
      return response()->json(['petitioner'=>$arrayName['reg_petitioner'],'respondent'=>$arrayName['reg_respondent']]);
    }

    //view other

    public function view_other(Request $request){
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();
      
      if($request['check'] != ""){
        Other::whereIn('other_id', $request['check'])->delete();
        return Redirect()->back();
      }
      
      $query = Other::leftJoin('adv_case_reg', function($join) {$join->on('reg_id', '=', 'other_case_no');})->where('reg_comp_other.user_admin_id','=',session('admin_id'));

      if($request['other_case_no'] != ""){
        $query = $query->where('other_case_no', 'LIKE', '%'.$request['other_case_no'].'%');
      }
      if($request['other_title'] != ""){
        $query = $query->where('other_title', 'LIKE', '%'.$request['other_title'].'%');
      }
      if($request['other_compliance'] != ""){
        $query = $query->where('other_compliance', 'LIKE', '%'.$request['other_compliance'].'%');
      }

      $app_data =  array('other_case_no' => $request['other_case_no'],'other_title' => $request['other_title'],'other_compliance' => $request['other_compliance']);
      $get_record = $query->orderBy('other_id', 'desc')->paginate(10)->appends($app_data);      
      $title = 'View Other';
      $data  = compact('title','admin_details','get_record');
      return view('advocate_admin/view_other',$data);	
    }

    //insert other function

    public function insert_other(Request $request, $other_id = false){
      
      if($other_id==''){
      $Add = new Other;  
      $Add->other_case_no         = $request['case_no'];
      $Add->other_title           = $request['title'];
      $Add->other_compliance      = $request['other'];
      $Add->other_status_date     = $request['status_date_s'];
      $Add->user_admin_id         = session('admin_id');      
      $Add->other_status          = 1;
      $Add->save();
      return redirect('advocate-panel/view-other');
    }
    else
    {
      $values  = array(
        'other_case_no'         =>  $request['case_no'],
        'other_title'           =>  $request['title'],
        'other_compliance'      =>  $request['other'],
        'other_status_date'     =>  $request['status_date_s']
        );

      Other::where('other_id','=',$other_id)->update($values);
      return redirect('advocate-panel/view-other');
    }

  }

    //other status

    public function other_status($id,$value){
      Other::where('other_id', $id)->update(['other_status' => $value]);
      return Redirect()->back();
    }
}
