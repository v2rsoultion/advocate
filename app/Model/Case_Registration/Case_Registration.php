<?php

namespace App\Model\Case_Registration;

use Illuminate\Database\Eloquent\Model;

class Case_Registration extends Model
{
    public $timestamps = false;
	protected $table = 'adv_case_reg';	
}
