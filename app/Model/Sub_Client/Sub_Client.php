<?php

namespace App\Model\Sub_Client;

use Illuminate\Database\Eloquent\Model;

class Sub_Client extends Model
{
    public $timestamps = false;
	protected $table = 'adv_sub_client';	
}
