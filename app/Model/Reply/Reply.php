<?php

namespace App\Model\Reply;

use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    public $timestamps = false;
	protected $table = 'adv_comp_reply';	
}
