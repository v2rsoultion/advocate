<?php

namespace App\Model\Client;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    public $timestamps = false;
	protected $table = 'adv_client';	
}
