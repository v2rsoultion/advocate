<?php

namespace App\Model\Defect_Case;

use Illuminate\Database\Eloquent\Model;

class Defect_Case extends Model
{
    public $timestamps = false;
	protected $table = 'adv_comp_defect_case';	
}
