<?php

namespace App\Model\Compliance;

use Illuminate\Database\Eloquent\Model;

class Compliance extends Model
{
    public $timestamps = false;
	protected $table = 'adv_compliance';	
}
