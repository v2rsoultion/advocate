<?php

namespace App\Model\Pages;

use Illuminate\Database\Eloquent\Model;

class Pages extends Model
{
    public $timestamps = false;
	protected $table = 'adv_master_pages';	
}
