<?php

namespace App\Model\Holiday;

use Illuminate\Database\Eloquent\Model;

class Holiday extends Model
{
    public $timestamps = false;
	protected $table = 'adv_holiday';	
}
