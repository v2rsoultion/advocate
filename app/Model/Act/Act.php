<?php

namespace App\Model\Act;

use Illuminate\Database\Eloquent\Model;

class Act extends Model
{
    public $timestamps = false;
	protected $table = 'adv_act';	
	protected $primaryKey = 'act_id';

	public function getAct()
    {
        return $this->hasMany('App\Model\Section\Section','section_act_id');
    }	
}
