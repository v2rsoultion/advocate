<?php

namespace App\Model\Class_Code;

use Illuminate\Database\Eloquent\Model;

class Class_Code extends Model
{
    public $timestamps = false;
	protected $table = 'adv_class_code';	
}
