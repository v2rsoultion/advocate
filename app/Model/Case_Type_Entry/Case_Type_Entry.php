<?php

namespace App\Model\Case_Type_Entry;

use Illuminate\Database\Eloquent\Model;

class Case_Type_Entry extends Model
{
    public $timestamps = false;
	protected $table = 'adv_case_type';	
}
