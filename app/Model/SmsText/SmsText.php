<?php

namespace App\Model\SmsText;

use Illuminate\Database\Eloquent\Model;

class SmsText extends Model
{
    public $timestamps = false;
	protected $table = 'adv_sms_content';	
}
