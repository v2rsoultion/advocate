<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    public $timestamps = false;
	protected $table = 'adv_account_settings';	
}
