<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    public $timestamps = false;
	protected $table = 'adv_admin';	
}
