<?php

namespace App\Model\Stage;

use Illuminate\Database\Eloquent\Model;

class Stage extends Model
{
    public $timestamps = false;
	protected $table = 'adv_stage';	
}
