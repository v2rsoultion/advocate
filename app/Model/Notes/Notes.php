<?php

namespace App\Model\Notes;

use Illuminate\Database\Eloquent\Model;

class Notes extends Model
{
    public $timestamps = false;
	protected $table = 'adv_notes';	
}
