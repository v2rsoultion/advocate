<?php

namespace App\Model\Fir;

use Illuminate\Database\Eloquent\Model;

class Fir extends Model
{
    public $timestamps = false;
	protected $table = 'adv_fir';	
}
