<?php

namespace App\Model\Course;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    public $timestamps = false;
	protected $table = 'adv_courselist_in_high_court';	
}
