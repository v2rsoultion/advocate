<?php

namespace App\Model\Section;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    public $timestamps = false;
	protected $table = 'adv_section';
	protected $primaryKey = 'section_id';
}
