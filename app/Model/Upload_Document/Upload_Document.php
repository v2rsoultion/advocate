<?php

namespace App\Model\Upload_Document;

use Illuminate\Database\Eloquent\Model;

class Upload_Document extends Model
{
    public $timestamps = false;
	protected $table = 'adv_document_uploads';	
}
