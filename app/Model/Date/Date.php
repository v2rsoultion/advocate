<?php

namespace App\Model\Date;

use Illuminate\Database\Eloquent\Model;

class Date extends Model
{
    public $timestamps = false;
	protected $table = 'adv_comp_date';	
}
