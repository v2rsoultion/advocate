<?php

namespace App\Model\Notice_Issue;

use Illuminate\Database\Eloquent\Model;

class Notice_Issue extends Model
{
    public $timestamps = false;
	protected $table = 'adv_comp_notice';	
}
