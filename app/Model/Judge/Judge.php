<?php

namespace App\Model\Judge;

use Illuminate\Database\Eloquent\Model;

class Judge extends Model
{
    public $timestamps = false;
	protected $table = 'adv_judge';	
}
