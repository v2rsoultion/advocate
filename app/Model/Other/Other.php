<?php

namespace App\Model\Other;

use Illuminate\Database\Eloquent\Model;

class Other extends Model
{
    public $timestamps = false;
	protected $table = 'reg_comp_other';	
}
