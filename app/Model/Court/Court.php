<?php

namespace App\Model\Court;

use Illuminate\Database\Eloquent\Model;

class Court extends Model
{
    public $timestamps = false;
	protected $table = 'adv_court';	
}
