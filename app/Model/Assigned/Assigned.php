<?php

namespace App\Model\Assigned;

use Illuminate\Database\Eloquent\Model;

class Assigned extends Model
{
    public $timestamps = false;
	protected $table = 'adv_assigned';	
}
