<?php

namespace App\Model\Case_Sub_Type;

use Illuminate\Database\Eloquent\Model;

class Case_Sub_Type extends Model
{
    public $timestamps = false;
	protected $table = 'adv_case_subtype';	
}
