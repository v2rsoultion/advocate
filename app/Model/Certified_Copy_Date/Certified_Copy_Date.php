<?php

namespace App\Model\Certified_Copy_Date;

use Illuminate\Database\Eloquent\Model;

class Certified_Copy_Date extends Model
{
    public $timestamps = false;
	protected $table = 'adv_certified_copy_date';	
}
