<?php

/*
|--------------------------------------------------------------------------
| Table Name
|--------------------------------------------------------------------------
|
| Here we define the database table name and timestamp to false.
|
*/

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Common extends Model
{
    public $timestamps = false;
    protected $table = 'country';
}
