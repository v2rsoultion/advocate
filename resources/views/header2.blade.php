<!DOCTYPE html>
<html>
  <head>
    <title>{{ $title }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/png" href="{{url('/')}}/public/images/logo2.png" sizes="100x100"/ >

   <!--     {!!Html::style('public/css/responsive.css') !!} -->
    {!!Html::style('public/css/style.css') !!}
    {!!Html::style('public/css/bootstrap.min.css') !!}
    <!-- {!!Html::style('public/css/owl.theme.default.min.css') !!} -->
    {!!Html::style('public/css/owl.carousel.min.css') !!}
    {!!Html::style('public/css/animate.css') !!}

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans" rel="stylesheet">
  </head>
<style type="text/css">
  .scroll_row1{
    padding-top: 15px;
  }
</style>

  <body>
    <div class="head2">
      <div id="scroll_row">
        <div class="container">
          <div class="row">
            <div class="col-md-4 col-xs-2 col-sm-4 logo">
            <a href="{{ url('/') }}"><img src="public/images/F_logo_01.svg" alt="img logo" title="Ideal Lawyer" class="logo"></a>
            </div>
            <div class="col-md-8 col-xs-10 col-sm-8">
              <nav class="navbar navbar-inverse" role="navigation" id="menu">
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="#"><img src="images/logo.png"></a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-nav">
                    <li><a href="{{ url('/') }}" title="Home">Home</a></li>
                    <li ><a href="{{ url('/') }}#about-us" title="About">About</a></li>
                    <li><a href="{{ url('/') }}#product" title="Product">Product</a></li>
                    <!--  <li class="dropdown"> -->
                    <!-- class="dropdown-toggle" data-toggle="dropdown"  -->
                    <!--   <a href="#" title="Careers">Careers  </a> -->
                    <!--  <b class="caret"></b> -->
                    <!--       <ul class="dropdown-menu">
                      <li><a href="#">Action</a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                      <li class="divider"></li>
                      <li><a href="#">One more separated link</a></li>
                      </ul> -->
                    <!--   </li> -->
                    <li><a href="{{ url('/') }}#contact-us" title="Contact">Contact</a></li>
                    <li><a href="{{ url('sign-in') }}" class="login" title="Sign in">SIGN IN</a></li>
                    <li><a href="{{ url('sign-up') }}" class="signup" title="Sign up">SIGN UP</a></li>
                  </ul>
                  <!-- /.navbar-collapse -->
                </div>
              </nav>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
    <script type="text/javascript">
      $('ul.nav li.dropdown').hover(function() {
      $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
      }, function() {
      $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
      });
    </script>
    <div class="clearfix"></div>