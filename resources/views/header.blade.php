<!DOCTYPE html>
<html>
  <head>
    <title> {{ $title }} </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/png" href="{{url('/')}}/public/images/logo2.png" sizes="100x100"/ >

<!--     {!!Html::style('public/css/responsive.css') !!} -->
    {!!Html::style('public/css/style.css') !!}
    {!!Html::style('public/css/bootstrap.min.css') !!}
    <!-- {!!Html::style('public/css/owl.theme.default.min.css') !!} -->
    {!!Html::style('public/css/owl.carousel.min.css') !!}
    {!!Html::style('public/css/animate.css') !!}

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans" rel="stylesheet">
   
  </head>
  <body>
    <div class="head">
      <div id="scroll_row">
        <div class="container" >
          <div class="row" >
            <div class="col-md-4 col-xs-2 col-sm-4 logo">
              <a href="{{ url('/') }}"><img src="public/images/F_logo_01.svg" alt="img logo" title="Ideal Lawyer" class="logo"></a>
            </div>
            <div class="col-md-8 col-xs-10 col-sm-8">
              <nav class="navbar navbar-inverse" role="navigation" id="menu">
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  </button>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-nav">
                    <li><a href="#" class="flip" title="Home">Home</a></li>
                    <li ><a href="#about-us" class="flip" title="About">About</a></li>
                    <li><a href="#product" class="flip" title="Product">Product</a></li>
                    <li><a href="#contact-us" class="flip" title="Contact">Contact</a></li>

                    @if(session('admin_id') != "")
                    
                    <li class=""><a href="advocate-panel/dashboard" class="login" title="Dashboard">{{ $get_record->admin_name}} </a></li>
                    @else
                    <li><a href="{{ url('sign-in') }}" class="login" title="Sign in">SIGN IN</a></li>
                    <li><a href="{{ url('sign-up') }}" class="signup" title="Sign up">SIGN UP</a></li>

                    @endif
                  </ul>
                </div>
              </nav>
            </div>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-5 col-xs-12 col-sm-5 heading wow fadeInLeft" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
            <h1>ALERTS FOR CASES.</h1>
            <h3>Following cases, simple and fast.</h3>
            <p>An easy and savvy client directory,allowing you to keep your client updated about the proceedings via E-mails, SMS, Whatsapp.</p>
            <a href="https://www.apple.com/in/ios/app-store/" title="Apps"><img src="public/images/app.png" alt="img app"></a>
            <a href="https://play.google.com/store?hl=en" ><img src="public/images/google.png" alt="img google" title="Google play"></a>
          </div>
          <div class="col-md-7 col-xs-12 col-sm-7 headimg wow fadeInRight" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
            <img src="public/images/bannerin.jpg" alt="img bannerin" class="img-responsive">
          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript">
      // $('ul.nav li.dropdown').hover(function() {
      // $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
      // }, function() {
      // $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
      // });

      // $(document).ready(function(){
      //   $("#flip").click(function(){

      //     alert();
      //       $("#bs-example-navbar-collapse-1").slideToggle("slow");
      //   });
      // });

    </script>
    <div class="clearfix"></div>