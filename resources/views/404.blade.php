@include("header_thank")

<div class="site_common">
	<div class="container">
		<div class="row">
			<div class="col-md-12 breadcum text-center wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="0.5s" data-wow-offset="10">
				<h1>404!</h1>
				<p><a href="{{url('/')}}" title="Home">Home</a> / <span> 404! </span></p>
			</div>
		</div>
	</div>
</div>
<div class="main_dashboard " style="padding:80px 0px">
	<div class="container">
		<div class="row text-center">
			
			
			<div class="col-md-12  text-center" style="color:black;" >
				<p style="font-family: 'montserratlight';font-size: 18px;color: #777777;font-weight: bold;letter-spacing: 1px;line-height: 30px; display: block;" >Sorry, the page you are looking for is Unavailable. We would suggest you to visit our home page and start from there.</p>
			</div>
			
		</div>
	</div>
</div>


@include('second_footer')
@include('footer')