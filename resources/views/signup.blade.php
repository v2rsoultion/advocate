<!DOCTYPE html>
<html>
  <head>
    <title> {{ $title }} </title>
    <link rel="shortcut icon" type="image/png" href="{{url('/')}}/public/images/logo2.png" sizes="100x100"/ >
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="public/css/responsive.css" rel="stylesheet">
    <link href="public/css/style.css" rel="stylesheet">
    <link href="public/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    {!!Html::script('public/js/bootstrapvalidator.min.js') !!}
    {!!Html::script('public/js/bootstrap-formhelpers.min.js') !!}
  </head>
  <style type="text/css">
    body{
    background-size: cover;
    background:url(public/images/sign.jpg)center no-repeat fixed;
    }
    .form-group input{
    margin-top: 0px !important;
    margin-bottom: 0px !important;
    }
    /*.help-block{
    margin-top: -15px !important;
    }*/
    .modal-backdrop{
    position: inherit;
    }
  </style>
  <body>
    {!! Form::open(['name' => 'registration' , 'id' => 'contact-form' ,'autocomplete'=>'off']) !!}
    <div class="">
      <div class="container">
        <a href="{{ url('/')}}"><img src="public/images/F_logo_01.svg" height="60px" alt="img logo" title="Home" class="logo"></a>
        <div class="signup-page">
          <div class="row fromin">
            <h2>Sign Up</h2>
            <div class="forms">
              <!--             <form action="" name="registration" id="contact-form"> -->
              <div id="signup_messsage"></div>
              <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="form-group">
                  <label for="FirstName">Name</label>
                  <input type="text" class="form-control" name="name" id="name" autocomplete="off">
                </div>
              </div>
              <div class="col-md-6 col-xs-12 col-sm-12">
                <div class="form-group">
                  <label for="phone_number">Phone Number</label>
                  <input type="text" class="form-control numeric" name="phone_number" id="phone_number" autocomplete="off">
                </div>
              </div>
              <div class="col-md-6 col-xs-12 col-sm-12">
                <div class="form-group">
                  <label for="email">Email</label>
                  <input type="text" class="form-control" name="email" id="email" autocomplete="off">
                </div>
              </div>
              <div class="clearfix"></div>
              <div class="col-md-6 col-xs-12 col-sm-12">
                <div class="form-group">
                  <label for="Password">Password</label>
                  <input type="password" class="form-control" name="password" id="password" autocomplete="off">
                </div>
              </div>
              <div class="col-md-6 col-xs-12 col-sm-12">
                <div class="form-group">
                  <label for="confirm_password">Confirm Password</label>
                  <input type="password" class="form-control blank_pass" name="confirm_password" id="confirm_password" autocomplete="off">
                </div>
                <small id="error_password" class="help-block" style=""></small>
              </div>

              <div id="otp_show"  style="display: none;">
                <div class="col-md-12 col-xs-12 col-sm-12">
                  <div class="form-group">
                    <label for="confirm_password">Enter OTP</label>
                    <input type="text" class="form-control" name="otp" id="otp" autocomplete="off">
                  </div>
                </div>

                <div class="col-md-12 col-xs-12 col-sm-12 log">
                  <h4 style="text-align: left;"> Didn't recieve OTP? <span class="resend_otp" title="Resend OTP" onclick="resend_otp()"> Resend OTP </span></h4>
                </div>
              </div>

             <!--  <div class="col-md-12 col-xs-12 col-sm-12 modal2">
                
                <a  data-toggle="modal" data-target="#sentotp"  data-backdrop="static" data-keyboard="false" class="popup">Resend OTP</a>
              </div> -->
            </div>
            <div class="col-md-12 col-xs-12 col-sm-12 button">
              <button type="submit" class="btn btn-default" title="Sign up" id="sign_up">SIGN UP</button>
            </div>
          </div>
          <div class="col-md-12 col-xs-12 col-sm-12 log">
            <h4>Already have an account? <a href="{{ url('sign-in') }}" title="Sign in">Sign In</a></h4>
            <h5>By signing up and using Ideal Lawyer, you are agreeing to its  <u><a href="{{ url('term-services') }}" title="End-user license agreement">End-user license agreement</a></u> & 
              <u><a href="{{ url('privacy-policy') }}" title="Privacy Policy">Privacy Policy</a></u> 
            </h5>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
    </div>
    </form> 
    <div id="sentotp" class="modal" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Enter OTP </h4>
          </div>
          <div class="modal-body">
            <div class="col-md-12 col-xs-12 col-sm-12" id="">
              <div class="form-group">
                <label for="confirm_password">Enter OTP</label>
                <input type="text" class="form-control" name="otp" id="rotp" autocomplete="off">
              </div>
              <button type="submit" class="btn btn-default" title="Submit" id="">SUBMIT</button>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
<script>
  $(document).ready(function(){

    $(function(){
  $('.blank_pass').on('input', function (event) { 
    $('#error_password').html('');
    $('.blank_pass').css("border-color", "");
  });
});
  
   $('#contact-form').bootstrapValidator({
  
       submitHandler: function (validotor, form, submitButton){

        var  password = $('#password').val();
        var  confirm_password = $('#confirm_password').val();

        if(password != confirm_password){
          $('#error_password').html('New Password and Confirm password should be same.');
          //$('.blank_pass').css("display", "block"); 
          return false;
        }
  
         $("#sign_up").html('Loading...<i class="fa fa-circle-o-notch fa-spin fa-1x fa-fw"></i>');
         $('#sign_up').prop('disabled', true);
  
       BASE_URL = '{{ url('/')}}';
  
       $.ajax({
         type: "POST",
         url: BASE_URL+"/submit-signup",   
           data: $(form).serialize(),
           success: function(result) {
             if(result.status == true){
               //  $('#contact-form').bootstrapValidator('resetForm', true);
                 $("#sign_up").html('submit');
                 $('#sign_up').prop('disabled', false);
                 $("#signup_messsage").html('<div class="alert alert-success" style="padding:10px; margin-bottom:15px;"> <a href="#" class="close close_message" data-dismiss="alert" aria-label="close">&times;</a> OTP has been sent to your mobile number. </div>');
                 $('#otp_show').show();
                 $('#phone_number').prop('readonly', true); 
             } else if(result.status == 3){
                 $('#contact-form').bootstrapValidator('resetForm', true);
                 $("#sign_up").html('submit');
                 $('#sign_up').prop('disabled', false);
                 $('#otp_show').hide(); 
                 $('#otp').val('');
                 $("#signup_messsage").html('<div class="alert alert-success" style="padding:10px; margin-bottom:15px;"> <a href="#" class="close close_message" data-dismiss="alert" aria-label="close">&times;</a>' +result.message+ '</div>');
             } else if( result.status == 2) {
                 $("#sign_up").html('submit');
                 $('#sign_up').prop('disabled', false);
                 $("#signup_messsage").html('<div class="alert alert-danger" style="padding:10px; margin-bottom:15px;"> <a href="#" class="close close_message" data-dismiss="alert" aria-label="close">&times;</a> Incorrect OTP. </div>');
             } else {
                 $("#sign_up").html('submit');
                 $('#sign_up').prop('disabled', false);
                 $("#signup_messsage").html('<div class="alert alert-danger" style="padding:10px; margin-bottom:15px;"> <a href="#" class="close close_message" data-dismiss="alert" aria-label="close">&times;</a> You are already registered with this email id. </div>');
             }
           }
       });
       },
         feedbackIcons: {
             validating: 'glyphicon glyphicon-refresh'
         },
         fields: {
             name: {
               validators: {
                   notEmpty: {
                     message: 'Please enter your name'
                 },
                 regexp: {
                   regexp: /^[a-z A-Z_]+$/i,
                   message: 'This field contains alphabets only'
                 }
               }
             },
             email: {
               validators: {
                   notEmpty: {
                     message: 'Please enter your email'
                 },
                 emailAddress: {
                   message: 'Please enter valid email'
                 },
               }
             },    
             phone_number: {
               validators: {
                   notEmpty: {
                     message: 'Please enter your phone number'
                 },
                 stringLength: {
                     min: 10,
                     max: 13,
                     message: 'Phone number should be minimum 10 digits and maximum 13 digits in length'
                 },
                 // regexp: {
                 //   regexp: /^[0-9]+$/i,
                 //   message: 'This field contains numeric only'
                 // }
               }
             },
             password: {
               validators: {
                 notEmpty: {
                     message: 'Please enter your password'
                 },
                 // stringLength: {
                 //     min: 6,
                 //     message: "The password should be at least 6 digits long."
                 // }
               }
             },
             confirm_password: {
                 validators: {            
                     notEmpty: {
                         message: 'Confirm your password'
                     },
                     identical: {
                       field: 'password',
                       message: 'Password and Confirm password should be same.'
                     }
                 }
             }
         }
     })
   });

  function resend_otp(){

    var  phone_number = $('#phone_number').val();

    BASE_URL = '{{ url('/')}}';
  
       $.ajax({
        // type: "POST",
         url: BASE_URL+"/resend-otp/"+phone_number,   
         //  data: $(form).serialize(),
           success: function(result) {
             if(result.status == true){
               //  $('#contact-form').bootstrapValidator('resetForm', true);
                 $("#sign_up").html('submit');
                 $('#sign_up').prop('disabled', false);
                 $("#signup_messsage").html('<div class="alert alert-success" style="padding:10px; margin-bottom:15px;"> <a href="#" class="close close_message" data-dismiss="alert" aria-label="close">&times;</a> A new OTP has been sent to your mobile number. </div>');
              //   $('#otp_show').show(); 
             }
           }
       });

    
  }

  
  $(function(){
       $('.numeric').on('input', function (event) { 
           this.value = this.value.replace(/[^0-9]/g, '');
       });
     });
  
  // popup box
  
  
  $(document).ready(function(){
     $("p").click(function(){
         $(this).hide();
     });
  });
  
</script>