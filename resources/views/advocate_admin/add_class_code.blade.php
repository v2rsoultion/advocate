@extends('advocate_admin/layout')
@section('content')
<style type="text/css">
  .admin-form .select, .admin-form .gui-input, .admin-form .select > select, .admin-form .select-multiple select{
    height: 28px !important;
  }
  .admin-form a.button, .admin-form span.button, .admin-form label.button
  {
    line-height: 28px !important;
  }
  .admin-form .append-icon .field-icon, .admin-form .prepend-icon .field-icon{
    line-height: 28px !important;
  }
  .admin-form .gui-textarea {
    line-height: 7px !important;
  }
/*  .gui-textarea{
    height: 100px !important;
  }*/
/* .admin-form .gui-input {
  padding: 5px;
 }*/
 /*.admin-form .prepend-icon .field-icon {
  top: 6px;
 }*/
 .form-control {
    height: 28px !important;
    /*padding: 0px 12px !important;*/
  }
 .admin-form .button{
    height: 28px !important;
    line-height: 1px !important;
  }
 .btn {
    height: 29px !important;
    line-height: 1px !important;  }

 .down{
  margin-top: 70px;
 }
.account_setting {
  margin: 0px 20px !important;
  margin-top: 25px !important;
  /*padding: 5px 0px !important;*/
 }
 .select-text{
  padding: 0px 12px;
 }
</style>
  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left down">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="{{ url('/advocate-panel/view-class-code') }}">View Class Code</a>
            </li>
            <li class="crumb-icon">
              <a href="{{ url('advocate-panel/dashboard') }}">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="{{ url('advocate-panel/dashboard') }}">Home</a>
            </li>
            <li class="crumb-trail">Add Class Code</li>
          </ol>
        </div>
      </header>

       <div class="row">
        <div class="col-md-12">
          @if (\Session::has('success'))
          <div class="alert alert-success account_setting" >
            {!! \Session::get('success') !!}
          </div>
          @endif

          @if (\Session::has('danger'))
          <div class="alert alert-danger account_setting" >
            {!! \Session::get('danger') !!}
          </div>
          @endif

        </div>
      </div>
      <!-- Begin: Content -->

      <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center">
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-heading">
                <span class="panel-title">Add Class Code</span>
              </div>
              <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                <!-- IF any error found -->
                <div class="col-md-12">
                @if ($errors->any())
                  <div id="log_error" class="alert alert-danger" style='font-family: josefin_sansregular !important;'>
                  {{$errors->first()}}  </i></div>
                @endif
                </div>
                <!-- IF any error found -->
                  <div id="tab1_1" class="tab-pane active">
                    {!! Form::open(['name'=>'form_add_question','url'=>'advocate-panel/insert-class-code/'.$get_record[0]->classcode_id,'id'=>'form_add_question' ,'autocomplete'=>'off']) !!}
                    <div class="row">
                      <div class="col-md-6">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Choose Case Type : </label>
                              <label for="level" class="field prepend-icon"> 
                              <!-- <label for="artist_state" class="field select"> -->
                                <select class="form-control select-text" id="" name="choose_case_type" style="">
                                  <option value=''>Choose Case Type</option>
                                  @if($get_record != "") 
                                   @foreach($case_type_entry as $case_type_entrys) 
                                     <option value="{{ $case_type_entrys->case_id }}" {{ $case_type_entrys->case_id == $get_record[0]->subtype_case_id ? 'selected="selected"' : '' }} >{{$case_type_entrys->case_name}} </option>
                                   @endforeach
                                   @else
                                   @foreach($case_type_entry as $case_type_entrys)
                                     <option value="{{ $case_type_entrys->case_id }}">{{$case_type_entrys->case_name}} </option>
                                   @endforeach
                                   @endif                               
                                </select>
                                <i class="arrow double"></i>
                              </label>
                          </div>
                        </div>
                        <div class="col-md-6">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;" >Class Code Name :  </label>  
                          <label for="level_name" class="field prepend-icon">
                            {!! Form::text('sub_type_name',$get_record[0]->classcode_name,array('class' => 'gui-input','placeholder' => '' )) !!}
                              <label for="Account Mobile" class="field-icon">
                              <i class="fa fa-pencil"></i>
                            </label>                           
                          </label>
                        </div>
                        </div>
                        <div class="clearfix"></div>
                        <!-- <div class="col-md-6">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;" >Code :  </label>  
                          <label for="level_name" class="field prepend-icon">
                            {!! Form::text('code',$get_record[0]->classcode_code,array('class' => 'gui-input','placeholder' => '' )) !!}
                              <label for="Account Mobile" class="field-icon">
                              <i class="fa fa-pencil"></i>
                            </label>                           
                          </label>
                        </div>
                        </div>
                        <div class="col-md-6">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;" >Short Name :  </label>  
                          <label for="level_name" class="field prepend-icon">
                            {!! Form::text('short_name',$get_record[0]->classcode_short_name,array('class' => 'gui-input','placeholder' => '' )) !!}
                              <label for="Account Mobile" class="field-icon">
                              <i class="fa fa-pencil"></i>
                            </label>                           
                          </label>
                        </div>
                        </div>
                        <div class="col-md-12">
                        <div class="section" id="textasrea">
                          <label for="level_name" class="field-label" style="font-weight:600;" >Description :  </label>  
                          <label for="level_name" class="field prepend-icon">
                            {!! Form::textarea('description',$get_record[0]->classcode_description,array('class' => 'gui-textarea','placeholder' => '' )) !!}
                              <label for="Account Mobile" class="field-icon">
                              <i class="fa fa-pencil"></i>
                            </label>                          
                          </label>
                        </div>
                        </div> -->
                     </div>

                  <div class="panel-footer text-right">
                      {!! Form::submit('Save', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}
                      {!! Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}
                  </div>   
                    {!! Form::close() !!}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

<script type="text/javascript">

  jQuery(document).ready(function() {
  
    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "This field contains alphabets only");

    jQuery.validator.addMethod("checkemail", function(value, element) 
    {
    return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
    }, "Enter a VALID email address"); 

    $.validator.addMethod('minStrict', function (value, el, param) {
     return value > param; 
    });
    $.validator.addMethod('maxStrict', function (value, el, param) {
     return value < param; 
    });


    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#form_add_question").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        // choose_case_type: {
        //   required: true
        // },
        // sub_type_name: {
        //   required: true,
        //   lettersonly: true
        // },
        // code: {
        //   required: true
        // },
        // description: {
        //   required: true
        // },
        // short_name: {
        //   required: true,
        //   lettersonly: true
        // },
      },

      /* @validation error messages 
      ---------------------------------------------- */

      messages: {
        choose_case_type: {
          required: 'Please Fill Required Choose Case Type'
        },
        sub_type_name: {
          required: 'Please Fill Required Sub Type Name'
        },
        code: {
          required: 'Please Fill Required Code'
        },
        description: {
          required: 'Please Fill Required Description'
        },
        short_name: {
          required: 'Please Fill Required Short Name'
        },        
      },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });


  </script>



@endsection
