<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<title>Email</title>
	<style type="text/css">
		#outlook a {padding:0;}
		body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
		.ExternalClass {width:100%;}
		.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
		#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
		img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;}
		a img {border:none;display:inline-block;}
		.image_fix {display:block;}
		h1, h2, h3, h4, h5, h6 { margin: 0px;}
		h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: blue !important;}
		h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {
			color: red !important; 
		}
		h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {
			color: purple !important; 
		}
		table td {border-collapse: collapse;}
		table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
		a {color: #000;}
		@font-face {
		    font-family: 'bernardo_moda_boldregular';
		    src: url('public/fonts/bernardo_moda_bold-webfont.woff2') format('woff2'), url('public/fonts/bernardo_moda_bold-webfont.woff') format('woff');
		    font-weight: normal;
		    font-style: normal;
		}
		@font-face {
		    font-family: 'RobotoLight';
		    src: url('public/fonts/RobotoLight.eot');
		    src: url('public/fonts/RobotoLight.eot') format('embedded-opentype'), url('public/fonts/RobotoLight.woff2') format('woff2'), url('public/fonts/RobotoLight.woff') format('woff'), url('public/fonts/RobotoLight.ttf') format('truetype'), url('public/fonts/RobotoLight.svg#RobotoLight') format('svg');
		}
		.borderbottom{
			padding: 15px;
			background: #67778e;
		}
		.footer {
			padding: 15px 0px;
			text-align: center;
			background: #67778e;
		}
		.mailtemplate{
			margin: 0px auto;
			padding: 15px 15px;
		}
		.logo{
			text-align: center;
		}

		/*.logo img{
			width: 25%;
		}*/

		.footer{
			color: #ffffff;
			text-align: center;
			font-size: 14px; 
			font-family: 'RobotoLight';
		}
		span{
			color: #6ab1d7;
		}
		p{	
			text-align: justify;
			margin: 15px;
			padding: 0px;
			margin: 0px;
		    font-family: 'RobotoLight';
		}
		.tr{
			padding: 50px 0px;
		}
		.socilicons{
			float: right;
		}
		.socilicons p{
			color: #fff;
			padding: 5px 0px;
			font-size: 14px;
		}
		.socilicons a{
			color: #fff;
			text-decoration: none;
			font-size: 14px;
		}
		.socilicons i{
			margin-right: 10px;
			color: #fcb040;
		}
		.middlediv{
			padding: 30px 10px;
		}
		.middlediv h1{
			color: #353439;
			font-family: 'bernardo_moda_boldregular';
			font-size: 24px;
			margin-left: 15px;
			padding-bottom: 20px;
			text-transform: capitalize;
		}
		.middlediv p{
			margin: 20px 0px 0px 20px;
			font-size: 14px;
			line-height: 18px;
		}
		.middlediv1{
			padding: 0px 10px 20px 15px;
		}
		.middlediv1 h1{
			color: #353439;
			font-family: 'bernardo_moda_boldregular';
			font-size: 16px;
			margin-left: 15px;
			text-transform: capitalize;
		}
		.imgmail{
			margin: 0px auto;
			width:200px;
		}
		.imgmail img{
			width: 200px;
		}
		@media (max-width: 461px){ 
			.logo img{
				width: 90%;
			}
		}
	</style>

<script type="colorScheme" class="swatch active">
  {
    "name":"Default",
    "bgBody":"ffffff",
    "link":"f2f2f2",
    "color":"555555",
    "bgItem":"F4A81C",
    "title":"181818"
  }
</script>

</head>
<body>	
    <div class="mailtemplate">
		<div class="main">
			<div class="borderbottom">
				
				@php
					$account  = App\Model\Admin\Account::first();
				@endphp

				<div class="logo">
					<img src="{{ URL::asset('public/images/advocate.png')}}"  alt='Logo' title="Logo" />
				</div>
				
				<div style="clear:both;"></div>
			</div>
			<div class="middlediv">
				<h1>Hello Admin</h1>
				<p>Please visit the following URL and enter the requested info: <b> <a href="{{ url('advocate-panel/reset-password') }}/{{$admin_id}}"> {{ url('advocate-panel/reset-password') }}/{{$admin_id}} </a> </b> </p>
				<p>If you did not specifically request this password change, please disregard this notice.</p>
				<p>If you have any questions, comments, or concerns, please do not hesitate to contact us.</p>
			</div>
			<div class="middlediv1">
				<h1>Thank You <br> </h1>
			</div>

			<div class="footer">
				Copyright &copy; {{ date('Y') }} <span> Advocate </span> .	 All Rights Reserved.
			</div>
		</div>
	</div>
</body>
</html>