@extends('advocate_admin/layout')

@section('content')
  
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

 <script type="text/javascript">

  // pie chart 

    google.charts.load('current', {packages: ['corechart', 'bar']});
    google.charts.setOnLoadCallback(drawBasic);

    function drawBasic() {

      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Task', 'total no of pending case'],
          ['0-2 years',   {{ $zero_two }} ],
          ['2-5 years',   {{ $two_five }} ],
          ['5-10 years',  {{ $five_ten }} ],
          ['> 10 years',  {{ $greater_ten }} ]
        ]);

        var options = {
          title: 'Total no. of Pending Case'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data, options);
      }

      // bar chart

      var data = google.visualization.arrayToDataTable([
         ['Element', 'Count', { role: 'style' }],
         ['Total Cases', {{ $total_case }}, ''],            // RGB value
         ['Civil Cases', {{ $civil_case }}, ''],            // English color name
         ['Criminal Cases', {{ $criminal_case }}, ''],
      ]);

      var options = {
        title: 'Total no. of Cases',
      };

      var chart = new google.visualization.ColumnChart(
        document.getElementById('chart_div'));

      chart.draw(data, options);
    }


</script>



<!-- Start: Content-Wrapper -->
  <section id="content_wrapper">

  	<header id="topbar" style="margin-top: 60px;">
	    <div class="topbar-left">
	      <ol class="breadcrumb">
	        <li class="crumb-active">
	          <a href="{{ url('/advocate-panel/dashboard') }}">Dashboard</a>
	        </li>
	        <li class="crumb-icon">
	          <a href="{{ url('/advocate-panel/dashboard') }}">
	            <span class="glyphicon glyphicon-home"></span>
	          </a>
	        </li>
	        <li class="crumb-link">
	          <a href="{{ url('/advocate-panel/dashboard') }}">Home</a>
	        </li>
	        <li class="crumb-trail">Dashboard</li>
	      </ol>
	    </div>

	    
	</header>

  @if($admin_details[0]->admin_type == 2)
  <header id="topbar" style="margin-top: 1px; padding-top: 15px !important">
    <a href="{{ url('/advocate-panel/view-peshi') }}">  
      <marquee id='scroll_news' style="color: #000"  ><div onMouseOver="document.getElementById('scroll_news').stop();" onMouseOut="document.getElementById('scroll_news').start();"> {{$message}} </div></marquee>
    </a>
  </header>
  @endif

  
     
      <section id="content" class="animated fadeIn">

      	@if($admin_details[0]->admin_type == 2)
      	<!-- Dashboard Tiles -->
        <div class="row mb10">
          <div class="col-sm-6 col-md-3">
            <div class="panel bg-alert light of-h mb10">
              <div class="pn pl20 p5">
                <div class="icon-bg">
                  <i class="fa fa-bar-chart-o"></i>
                </div>
                <h2 class="mt15 lh15">
                  <b> {{ $total_case }}</b>
                </h2>
                <h5 class="text-muted">Total No. of Cases</h5>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-md-3">
            <div class="panel bg-info light of-h mb10">
              <div class="pn pl20 p5">
                <div class="icon-bg">
                  <i class="fa fa-bar-chart-o"></i>
                </div>
                <h2 class="mt15 lh15">
                  <b> {{ count($progress_case) }} </b>
                </h2>
                <h5 class="text-muted">In Progress Cases</h5>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-md-3">
            <div class="panel bg-danger light of-h mb10">
              <div class="pn pl20 p5">
                <div class="icon-bg">
                  <i class="fa fa-bar-chart-o"></i>
                </div>
                <h2 class="mt15 lh15">
                  <b> {{ $completed_case }} </b>
                </h2>
                <h5 class="text-muted">Completed Cases</h5>
              </div>
            </div>
          </div>

          <div class="col-sm-6 col-md-3">
            <div class="panel bg-warning light of-h mb10">
              <div class="pn pl20 p5">
                <div class="icon-bg">
                  <i class="fa fa-bar-chart-o"></i>
                </div>
                <h2 class="mt15 lh15">
                  <b> {{ $due_cause }} </b>
                </h2>
                <h5 class="text-muted">Due Course</h5>
              </div>
            </div>
          </div>

        </div>

        <div class="row mb10">
          
          <div class="col-sm-6 col-md-6">
            <div id="chart_div" style="height: 400px;"></div>
          </div>

          <div class="col-sm-6 col-md-6">
            <div id="piechart" style="height: 400px;"></div>
          </div>

        </div>


        @else 

      		<div class="content-header" style="margin-top:150px;">
      		  <p class="dash_imgss">{!! Html::image($account[0]->account_logo) !!}</p>
      		  <h1 class="dash_wel"> Welcome to <b class="text-primary" style="color:#0095da">Admin Control Panel</b></h1>
      		</div>

        @endif
      </section>
  </section>
<!-- End: Content -->	  
@endsection
