@extends('advocate_admin/layout')
@section('content')
<style type="text/css">
  .admin-form .select, .admin-form .gui-input, .admin-form .select > select, .admin-form .select-multiple select{
    height: 28px !important;
  }
  .admin-form a.button, .admin-form span.button, .admin-form label.button
  {
    line-height: 28px !important;
  }
  .admin-form .append-icon .field-icon, .admin-form .prepend-icon .field-icon{
    line-height: 28px !important;
  }
  .admin-form .gui-textarea {
    /*line-height: 7px !important;*/
  }
/*  .gui-textarea{
    height: 100px !important;
  }*/
/* .admin-form .gui-input {
  padding: 5px;
 }*/
 /*.admin-form .prepend-icon .field-icon {
  top: 6px;
 }*/
 .admin-form .button{
    height: 28px !important;
    line-height: 1px !important;
  }
 .btn {
    height: 29px !important;
    line-height: 1px !important;  }

 .down{
  margin-top: 70px;
 }
 .account_setting {
  margin: 0px 30px !important;
  margin-top: 15px !important;
  /*padding: 5px 0px !important;*/
 }
</style>

  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left down">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="{{ url('/advocate-panel/account-settings') }}">Account Settings</a>
            </li>
            <li class="crumb-icon">
              <a href="{{ url('/advocate-panel/dashboard') }}">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="{{ url('/advocate-panel/dashboard') }}">Home</a>
            </li>
            <li class="crumb-trail">Account Settings</li>
          </ol>
        </div>
      </header>

      <div class="row">
        <div class="col-md-12">
          @if (\Session::has('success'))
          <div class="alert alert-success account_setting" style="margin: 20px 25px;">
            {!! \Session::get('success') !!}
          </div>
          @endif
        </div>
      </div>

      <!-- Begin: Content -->
      {!! Form::open(['url'=>'advocate-panel/account-settings' , 'enctype' => 'multipart/form-data' , 'id' => 'validation' ,'autocomplete'=>'off' ]) !!}
      <section id="content" class="table-layout animated fadeIn" style="padding-top:15px">
        <div class="tray tray-center" style="padding-left: 30px;padding-right: ; padding-top: 0px; width: 95%;  width: 95%;">
          <div class="mw1000 center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-heading">
                <span class="panel-title">Details</span>
              </div>
              <div class="panel-body bg-light dark">
                <div class="admin-form">
                  @foreach($account as $accounts)
                  <div class="section row mb10">
                    <label for="acc_email" class="field-label col-md-4 text-center">Account Email Address:</label>
                    <div class="col-md-8">  
                      <label for="account_email" class="field prepend-icon">
                        {!! Form::text('account_email',"$accounts->account_email", array('class' => 'gui-input','placeholder' => 'Account Email Address' )) !!}
                        <label for="Account Email" class="field-icon">
                          <i class="fa fa-envelope-o"></i>
                        </label>
                      </label>
                    </div>
                  </div>

                  <div class="section row mb10">
                    <label for="acc_email" class="field-label col-md-4 text-center">Account Mobile Number:</label>
                    <div class="col-md-8">  
                      <label for="account_mobile" class="field prepend-icon">   
                        {!! Form::text('account_number',"$accounts->account_number", array('class' => 'gui-input bfh-phone', 'data-format' =>'dddddddddd', 'placeholder' => 'Account Mobile Number' )) !!}
                        <label for="Account Mobile" class="field-icon">
                          <i class="fa fa-phone"></i>
                        </label>
                      </label>
                    </div>
                  </div>

                  <div class="section row mb10">
                    <label for="acc_email" class="field-label col-md-4 text-center">Account Free Trail:</label>
                    <div class="col-md-8">  
                      <label for="account_mobile" class="field prepend-icon">   
                        {!! Form::text('account_free_trail',"$accounts->account_free_trail", array('class' => 'gui-input numeric', 'placeholder' => 'Account Free Trail' )) !!}
                        <label for="Account Mobile" class="field-icon">
                          <i class="fa fa-pencil"></i>
                        </label>
                      </label>
                    </div>
                  </div>

                  <div class="section row mb10">
                    <label for="acc_email" class="field-label col-md-4 text-center">Account Address:</label>
                    <div class="col-md-8">  
                      <label class="field prepend-icon" for="account_address">
                        {!! Form::textarea('account_address',"$accounts->account_address", array('class' => 'gui-textarea accc1','placeholder' => 'Account Address' )) !!}
                        <label for="account_address" class="field-icon">
                          <i class="fa fa-map-marker"></i>
                        </label>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div> 
        </div>
        <aside class="tray tray-right tray290" style="position: ;right: 0; padding: 5px 15px;">
          <div class="admin-form">

            <h4> Website Options</h4>
            <hr class="short" style="margin: 18px 0;">
            <h5><small>Site Name</small></h5>
            <div class="section mb10">
              <label for="store-name" class="field prepend-icon">
                {!! Form::text('account_name',"$accounts->account_name", array('class' => 'gui-input','placeholder' => 'Site Name' )) !!}
                <label for="store-name" class="field-icon">
                  <i class="fa fa-home"></i>
                </label>
              </label>
            </div>

            <hr class="short alt br-light">
            <div class="fileupload fileupload-new admin-form" data-provides="fileupload">
              <div class="fileupload-preview thumbnail mb15" style="line-height: 136px !important;">
                {!! Html::image($accounts->account_logo, '100%x147') !!}
              </div>
              <span class="button btn-system btn-file btn-block ph5" style="margin-bottom:10px">
                <span class="fileupload-new">Change Logo</span>
                <span class="fileupload-exists">Change Logo</span>
                {!! Form::file('account_logo') !!}
              </span>
            </div>
      
            <hr class="short alt br-light">
            <div><input type="submit" id="" class="btn btn-primary btn-gradient dark btn-block acc_submit" value="Update"/></div>
          </div>
        </aside>
        @endforeach
        {!! Form::close() !!}
      </section>
      <!-- End: Content -->
    </section>

<script type="text/javascript">
  jQuery(document).ready(function() {
    
    jQuery.validator.addMethod("checkemail", function(value, element) 
    {
    return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
    }, "Enter a VALID email address"); 

    
    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#validation").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        
        account_number: {
          required: true,
          minlength: 10,
          maxlength: 10,
      //    integer: true,
        },
        account_email: {
          required: true,
          checkemail: true
        },
        account_address: {
          required: true,
        },
        account_facebook: {
          required: true,
        },
        account_twitter: {
          required: true,
        },
        account_google: {
          required: true,
        },
        account_name: {
          required: true,
        },
        happy_homes: {
          required: true,
        },
        hours_reclaimed: {
          required: true,
        },
        total_genie: {
          required: true,
        },
        total_suburbs: {
          required: true,
        },
        working_hours: {
          required: true,
        },
        account_discount_price: {
          required: true,
        },
        account_logo: {
        // //  required: true,
          extension: 'jpeg,jpg,png',
        //   type: 'image/jpeg,image/png',
        // //  maxSize: 2097152,   // 2048 * 1024
       //    message: 'The selected file is not valid'
         },
        
      },

      /* @validation error messages 
      ---------------------------------------------- */

      messages: {
        account_number: {
          required: "Enter your account's mobile number",
          minlength:'Enter at least 10 digit account mobile number',
          maxlength:'Enter 10 digit account mobile number',
        },
        account_email: {
          required: "Enter your account's email address",
          email: 'Enter a VALID account email address'
        },
        account_address: {
          required: "Enter your account's address"
        },
        account_facebook: {
          required: 'Enter facebook link'
        },
        account_google: {
          required: 'Enter Google plus link'
        },
        account_twitter: {
          required: 'Enter twitter link'
        },
        account_name: {
          required: 'Enter Site Name'
        },
        happy_homes: {
          required: 'Enter Happy Hours'
        },
        hours_reclaimed: {
          required: 'Enter Happy Reclaimed'
        },
        total_genie: {
          required: 'Enter Total Genies'
        },
        total_suburbs: {
          required: 'Enter Total Suburbs'
        },
        working_hours: {
          required: 'Enter Working Hours'
        },
        account_discount_price: {
          required: 'Enter Referral Discount Price'
        },
        account_logo: {
          extension: 'Image Should be in .jpg,.jpeg and .png format only'
        },

      },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }
    });

  });
  </script>

@endsection
