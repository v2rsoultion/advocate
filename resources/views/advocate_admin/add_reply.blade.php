@extends('advocate_admin/layout')
@section('content')

  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="{{ url('/advocate-panel/view-reply') }}">View Reply</a>
            </li>
            <li class="crumb-icon">
              <a href="{{ url('advocate-panel/dashboard') }}">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="{{ url('advocate-panel/dashboard') }}">Home</a>
            </li>
            <li class="crumb-trail">Add Reply</li>
          </ol>
        </div>
      </header>

      <!-- Begin: Content -->

      <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center">
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-heading">
                <span class="panel-title">Add Reply</span>
              </div>
              <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                <!-- IF any error found -->
                <div class="col-md-12">
                @if ($errors->any())
                  <div id="log_error" class="alert alert-danger" style='font-family: josefin_sansregular !important;'>
                  {{$errors->first()}}  </i></div>
                @endif
                </div>
                <!-- IF any error found -->
                  <div id="tab1_1" class="tab-pane active">
                    {!! Form::open(['name'=>'form_add_question','url'=>'advocate-panel/insert-reply/'.$get_record[0]->reply_id,'id'=>'form_add_question' ,'autocomplete'=>'off']) !!}
                    <div class="row">
                      <div class="col-md-6">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Case No : </label>
                              <label for="artist_state" class="field select">
                                <select class="select2-single form-control" name="case_no" id="user_role" onchange="reply_caseno_ajax(this.value)">
                                  <option value=''>--Select-Option--</option>
                                  @if($get_record != "") 
                                   @foreach($reply_case_no as $reply_case_nos) 
                                     <option value="{{ $reply_case_nos->reg_id }}" {{ $reply_case_nos->reg_id == $get_record[0]->reply_case_no ? 'selected="selected"' : '' }} >{{$reply_case_nos->reg_case_number}} </option>
                                   @endforeach
                                   @else
                                   @foreach($reply_case_no as $reply_case_nos)
                                     <option value="{{ $reply_case_nos->reg_id }}">{{$reply_case_nos->reg_case_number}} </option>
                                   @endforeach
                                   @endif
                                </select>
                              </label>
                          </div>
                        </div>
                      <div class="col-md-6">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;" >Title :  </label>  
                          <label for="level_name" class="field prepend-icon">
                            {!! Form::text('title',$get_record[0]->reg_petitioner,array('class' => 'gui-input','placeholder' => '','id'=>'reply_caseno_change','disabled'=>'disabled' )) !!}
                              <label for="Account Mobile" class="field-icon">
                              <i class="fa fa-pencil"></i>
                            </label>                           
                          </label>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;" >Reply :  </label>  
                          <label for="level_name" class="field prepend-icon">
                            {!! Form::text('reply',$get_record[0  ]->reply_desc,array('class' => 'gui-input','placeholder' => '','id'=>'' )) !!}
                              <label for="Account Mobile" class="field-icon">
                              <i class="fa fa-pencil"></i>
                            </label>                           
                          </label>
                        </div>
                      </div>
                      <div class="col-md-6">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Status Date : </label>
                              <label for="artist_state" class="field select">
                                <select class="form-control" name="status_date_s">
                                  <option value='' >--Select-Option--</option>
                                  <option value='2' <?php if($get_record[0]->reply_status_date == "2"){ echo "selected";}?>>Filled</option>
                                  <option value='1' <?php if($get_record[0]->reply_status_date == "1"){ echo "selected";}?>>Pending</option>
                                </select>
                                <i class="arrow double"></i>
                              </label>
                          </div>
                        </div>
                    </div> 
                  </div>

                  <div class="panel-footer text-right">
                      {!! Form::submit('Save', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}
                      {!! Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}
                  </div>   
                    {!! Form::close() !!}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

<script type="text/javascript">

function reply_caseno_ajax(reply_caseno_id){

  if (reply_caseno_id != ''){
    BASE_URL = '{{ url('/')}}';
    $.ajax({
      url:BASE_URL+"/advocate-panel/change-reply-no-ajax/"+reply_caseno_id,
      success: function(result){
        $("#reply_caseno_change").val(result.petitioner+' V/S '+result.respondent);
      }
    });
  }

}

</script>

<script>
function showDiv(show_id) {

  if (show_id > 1) {
    document.getElementById('welcomeDiv').style.display = "block";
  }else if (show_id = 1){
      document.getElementById('welcomeDiv').style.display = "none";
  }

}
</script>      

<script type="text/javascript">

  jQuery(document).ready(function() {
  
    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "This field contains alphabets only");

    jQuery.validator.addMethod("checkemail", function(value, element) 
    {
    return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
    }, "Enter a VALID email address"); 

    $.validator.addMethod('minStrict', function (value, el, param) {
     return value > param; 
    });
    $.validator.addMethod('maxStrict', function (value, el, param) {
     return value < param; 
    });


    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#form_add_question").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        status_date_s: {
          required: true
        },
        status_date: {
          required: true
        },
        case_no: {
          required: true
        },
        title: {
          required: true
        },
        reply: {
          required: true
        },
      },

      /* @validation error messages 
      ---------------------------------------------- */

      messages: {
        status_date_s: {
          required: 'Please Fill Required Status Date'
        },
        status_date: {
          required: 'Please Fill Required Status Date'
        },
        case_no: {
          required: 'Please Fill Required Case No'
        },
        title: {
          required: 'Please Fill Required Title'
        },
        reply: {
          required: 'Please Fill Required Reply'
        },        
      },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });


  </script>



@endsection
