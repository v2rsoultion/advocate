@extends('advocate_admin/layout')
@section('content')
<style type="text/css">
  .admin-form .select, .admin-form .gui-input, .admin-form .select > select, .admin-form .select-multiple select{
    height: 28px !important;
  }
  .admin-form a.button, .admin-form span.button, .admin-form label.button
  {
    line-height: 28px !important;
  }
  .admin-form .append-icon .field-icon, .admin-form .prepend-icon .field-icon{
    line-height: 28px !important;
  }
  .admin-form .gui-textarea {
    line-height: 15px !important;
  }

  .col-md-3 {
    height: 75px;
}


  /*.select2-container .select2-selection--single {
    height: 28px !important;
  }
  .select2-container--default .select2-selection--single .select2-selection__rendered {
    line-height: 24px !important;
  }
  .select2-container .select2-selection--multiple {
    min-height: 30px !important;
  }
  .select2-container--default .select2-selection--single .select2-selection__arrow {
    top: -4px !important;
  }*/

  .multiselect-container li a label{
    border: 0px !important;
    background: none !important;
  }
  .multiselect-container {
    margin-top: 5px !important;
  }

  .btn.multiselect .caret{
    display: none;
  }
  .multiselect{
    text-align: left;
  }
  .input-group-addon {
    min-width: 30px;
    padding: 5px 6px;
  }
  .multiselect-search{
    height: 29px !important;
  }

/*  .gui-textarea{
    height: 100px !important;
  }*/
/* .admin-form .gui-input {
  padding: 5px;
 }*/
 /*.admin-form .prepend-icon .field-icon {
  top: 6px;
 }*/
 .form-control {
    height: 28px !important;
    /*padding: 0px 12px !important;*/
  }
 .admin-form .button{
    height: 28px !important;
    padding-top: 1px !important;
    line-height: 1px !important;
  }
 .btn {
    height: 29px !important;
    line-height: 1px !important;  }

 .down{
  margin-top: 70px;
 }
.account_setting {
  margin: 0px 20px !important;
  margin-top: 25px !important;
  /*padding: 5px 0px !important;*/
 }
 .select-text{
  padding: 0px 12px;
 }
</style>
  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left down">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="{{ url('/advocate-panel/view-case-form') }}">View Case</a>
            </li>
            <li class="crumb-icon">
              <a href="{{ url('advocate-panel/dashboard') }}">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="{{ url('advocate-panel/dashboard') }}">Home</a>
            </li>
            <li class="crumb-trail">Add Case</li>
          </ol>
        </div>
      </header>

       <div class="row">
        <div class="col-md-12">
          @if (\Session::has('success'))
          <div class="alert alert-success account_setting" >
            {!! \Session::get('success') !!}
          </div>
          @endif
        </div>
      </div>
      <!-- Begin: Content -->

      <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center">
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-heading">
                <span class="panel-title">Add Case </span>
              </div>
              <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                <!-- IF any error found -->
                <div class="col-md-12">
                @if ($errors->any())
                  <div id="log_error" class="alert alert-danger" style='font-family: josefin_sansregular !important;'>
                  {{$errors->first()}}  </i></div>
                @endif
                </div>
                <!-- IF any error found -->
                  <div id="tab1_1" class="tab-pane active">
                    {!! Form::open(['name'=>'form_add_question','url'=>'advocate-panel/insert-case-form/'.$get_record[0]->reg_id,'id'=>'form_add_question','files'=>'true' ,'autocomplete'=>'off' ]) !!}
                    <div class="row">

                      <div class="col-md-4">
                        <div class="section section_date">
                          <label for="level_name" class="field-label" style="font-weight:600;"> Date </label>  
                          <label for="level_name" class="field prepend-icon datefuthernew">
                            
                            @if($get_record[0]->reg_date == "")

                            {!! Form::text('date_picker','',array('class' => 'gui-input fromdate_notice','placeholder' => '','id'=>'date_picker', 'autocomplete' => 'off' )) !!}

                            @else

                            {!! Form::text('date_picker', date('d-m-Y',strtotime($get_record[0]->reg_date)) ,array('class' => 'gui-input fromdate_notice','placeholder' => '','id'=>'date_picker', 'autocomplete' => 'off' )) !!}

                            @endif

                              <label for="Account Mobile" class="field-icon">
                              <i class="fa fa-calendar"></i>
                            </label>                          
                          </label>
                        </div>
                      </div>
                      
                      <div class="col-md-4">
                        <div class="section" style="margin-bottom: 40px;">
                          <label for="level_name" class="field-label" style="font-weight:600;" >Court Type :  </label>  
                          <div class="col-md-6">
                            <label class="option" style="font-size:12px;">

                              {!! Form::radio('choose_type','1',$get_record[0]->reg_court == 1 ? 'checked' : '', array( 'class' => 'check' ,'onclick'=>'court_type_trail(1)' )) !!}
                              
                              <span class="radio"></span> Trial Court
                            </label>
                          </div>

                          <div class="col-md-6">
                            <label class="option" style="font-size:12px;">

                              @if($get_record[0]->reg_court != "")
                                {!! Form::radio('choose_type','2',$get_record[0]->reg_court == 2 ? 'checked' : '', array( 'class' => 'check' ,'onclick'=>'court_type_high(2)'  )) !!}
                              @else 
                                {!! Form::radio('choose_type','2',checked, array( 'class' => 'check' , 'class' => 'check' ,'onclick'=>'court_type_high(2)' )) !!}
                              @endif
                              
                              <span class="radio"></span> High Court
                            </label>
                          </div>
                        </div>
                        <div class="section"></div>
                        
                      </div>

                      @if($get_record[0]->reg_court == 1)
                      <div class="col-md-4">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;" > Select Court : </label>
                               <label for="level" class="field prepend-icon">
                              <select class="form-control select-text" name="court_id" id="display_high_court" >
                                <option value=''>Select Court</option>                                   
                                
                                  @foreach($trail_court as $trail_courts) 
                                    <option value="{{ $trail_courts->court_id }}" {{ $trail_courts->court_id == $get_record[0]->reg_court_id ? 'selected="selected"' : '' }} > {{$trail_courts->court_name }} </option>
                                  @endforeach

                              </select>
                              <i class="arrow double"></i>
                            </label>
                        </div>
                      </div>

                      @else
                      <div class="col-md-4">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;" > Select Court : </label>
                               <label for="level" class="field prepend-icon">
                              <select class="form-control select-text" name="court_id" id="display_high_court" >
                                <option value=''>Select Court</option>                                   
                                
                                  @foreach($high_court as $high_courts) 
                                    <option value="{{ $high_courts->court_id }}" {{ $high_courts->court_id == $get_record[0]->reg_court_id ? 'selected="selected"' : '' }} > {{$high_courts->court_name }} </option>
                                  @endforeach

                              </select>
                              <i class="arrow double"></i>
                            </label>
                        </div>
                      </div>
                      @endif

                      <div class="clearfix"></div>


                      {!! Form::hidden('reg_case_type_category',$get_record[0]->reg_case_type_category,array('class' => 'gui-input','placeholder' => '','id'=>'reg_case_type_category' )) !!}

                      
                        <!-- <div class="col-md-6">
                        <div class="section">
                                {!! Form::radio('choose_type','0',$get_record[0]->reg_court =='0' ? 'checked' :'',array('class' => 'high_court','placeholder' => '','id'=>'high','onclick'=>'myFunctionsub()' )) !!} <label for="high"><span>High Court</span></label>
                                {!! Form::radio('choose_type','1',$get_record[0]->reg_court=='1' ? 'checked' :'',array('class' => 'trial_court','placeholder' => '','id'=>'trial' )) !!} <label for="trial"><span>Trial Court</span></label>                                                      
                              </label>
                              </div>
                            </div>
                          </div> -->
                        <div id="">    
                          <div class="col-md-3">
                            <div class="section">
                              <label for="level_name" class="field-label" style="font-weight:600;" > Case Type : </label>
                                <!-- <label for="artist_state" class="field select"> -->
                                   <label for="level" class="field prepend-icon">
                                  @if($get_record[0]->reg_case_type_id != "")
                                    <select class="form-control select-text" name="type" onclick="choose_sub_type(this.value)" id="case_type" >
                                  @else 
                                    <select class="form-control select-text" name="type" onchange="choose_sub_type(this.value)" id="case_type" >
                                  @endif

                                    <option value=''>Select Case Type</option> 
                                      
                                      @foreach($case_type_entry as $case_type_entrys) 
                                        <option value="{{ $case_type_entrys->case_id }}" {{ $case_type_entrys->case_id == $get_record[0]->reg_case_type_id ? 'selected="selected"' : '' }} >{{$case_type_entrys->case_name }} </option>
                                      @endforeach

                                  </select>
                                  <i class="arrow double"></i>
                                </label>
                            </div>
                          </div>
                        </div>

                        <div id="hide_show_ncv" style="display: none;">
                          <div class="col-md-3">
                            <div class="section">
                              <label for="level_name" class="field-label" style="font-weight:600;" >NCV No :  </label>  
                              <label for="level_name" class="field prepend-icon">
                                {!! Form::text('ncv',$get_record[0]->reg_vcn_number,array('class' => 'gui-input','placeholder' => '','id'=>'' )) !!}
                                  <label for="Account Mobile" class="field-icon">
                                  <i class="fa fa-pencil"></i>
                                </label>                           
                              </label>
                            </div>
                          </div>
                        </div>

                        @if($get_record[0]->reg_court == 1) 
                          <div id="hide_show_ncv_new">
                            <div class="col-md-3">
                              <div class="section">
                                <label for="level_name" class="field-label" style="font-weight:600;" >NCV No :  </label>  
                                <label for="level_name" class="field prepend-icon">
                                  {!! Form::text('ncv',$get_record[0]->reg_vcn_number,array('class' => 'gui-input','placeholder' => '','id'=>'' )) !!}
                                    <label for="Account Mobile" class="field-icon">
                                    <i class="fa fa-pencil"></i>
                                  </label>                           
                                </label>
                              </div>
                            </div>
                          </div>
                        @endif
                        
                        <div id="hide_show_class_code" @if($get_record[0]->reg_court == 1) style="display: none;" @endif">
                          <div class="col-md-3">
                            <div class="section">
                              <label for="level_name" class="field-label" style="font-weight:600;" >Class Code : </label>
                                <!-- <label for="artist_state" class="field select"> -->
                                   <label for="level" class="field prepend-icon">
                                  <select class="form-control select-text" id="hide_choose_sub_type" name="sub_type">
                                    <option value=''> Select Class Code</option>

                                    @foreach($case_sub_type as $case_sub_types) 
                                      <option value="{{ $case_sub_types->classcode_id }}" {{ $case_sub_types->classcode_id == $get_record[0]->reg_case_subtype_id ? 'selected="selected"' : '' }} >{{$case_sub_types->classcode_name }} </option>
                                    @endforeach
                                  </select>
                                  <i class="arrow double"></i>
                                </label>
                            </div>
                          </div>
                        </div>

                        <div class="col-md-3">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Case No :  </label>  
                            <label for="level_name" class="field prepend-icon case_number_error">
                              {!! Form::text('case_no',$get_record[0]->reg_case_number,array('class' => 'gui-input','autocomplete' => 'off','onkeyup'=>'check_case_no(this.value)' )) !!}
                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-pencil"></i>
                              </label>                           
                            </label>
                            <div id="case_number_error"></div>
                          </div>
                        </div>

                        <div class="col-md-3">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Stage : </label>
                              <!-- <label for="artist_state" class="field select"> -->
                                <label for="level" class="field prepend-icon"> 
                                <select class="form-control select-text" name="reg_stage">
                                  <option value=''>Select Stage</option>
                                  @if($get_record != "") 
                                   @foreach($stage as $stages) 
                                     <option value="{{ $stages->stage_id }}" {{ $stages->stage_id == $get_record[0]->reg_stage_id ? 'selected="selected"' : '' }} >{{$stages->stage_name}} </option>
                                   @endforeach
                                   @else
                                   @foreach($stage as $stages)
                                     <option value="{{ $stages->stage_id }}">{{$stages->stage_name}} </option>
                                   @endforeach
                                   @endif                                 
                                </select>
                                <i class="arrow double"></i>
                              </label>
                          </div>
                        </div>
                        
                        <div class="col-md-8">
                          <div class="section" style="margin-bottom: 40px;">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Power :  </label>  
                            <div class="col-md-3">
                              <label class="option" style="font-size:12px;">

                                @if($get_record[0]->reg_power != "")
                                  {!! Form::radio('power','1',$get_record[0]->reg_power == 1 ? 'checked' : '', array( 'class' => 'check' ,'onclick'=>'reg_power_respondent(1)' )) !!}
                                @else 
                                  {!! Form::radio('power','1',checked, array( 'class' => 'check' , 'class' => 'check' ,'onclick'=>'reg_power_respondent(1)' )) !!}
                                @endif
                                <span class="radio"></span> Petitioner
                              </label>
                            </div>

                            <div class="col-md-3">
                              <label class="option" style="font-size:12px;">
                                {!! Form::radio('power','2',$get_record[0]->reg_power == 2 ? 'checked' : '', array( 'class' => 'check' ,'onclick'=>'reg_power_respondent(2)'  )) !!}
                                <span class="radio"></span> Respondent
                              </label>
                            </div>

                            <div class="col-md-3">
                              <label class="option" style="font-size:12px;">
                                {!! Form::radio('power','3',$get_record[0]->reg_power == 3 ? 'checked' : '', array( 'class' => 'check' ,'onclick'=>'reg_power_respondent(3)'  )) !!}
                                <span class="radio"></span> Caveat
                              </label>
                            </div>

                            <div class="col-md-3">
                              <label class="option" style="font-size:12px;">
                                {!! Form::radio('power','4',$get_record[0]->reg_power == 4 ? 'checked' : '', array( 'class' => 'check' ,'onclick'=>'reg_power_respondent(4)'  )) !!}
                                <span class="radio"></span> None
                              </label>
                            </div>

                          </div>
                          <div class="section"></div>
                        
                        </div>

                        @if($get_record[0]->reg_power == "")

                          <div class="col-md-4" style="display: none;" id="reg_power_respondent">

                        @else 

                          @if($get_record[0]->reg_power == 2)

                            <div class="col-md-4" style="" id="reg_power_respondent">

                          @else
                            <div class="col-md-4" style="display: none;" id="reg_power_respondent">
                          @endif

                          

                        @endif
                        


                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;"> Respondent Number :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              {!! Form::text('power_respondent',$get_record[0]->reg_power_respondent,array('class' => 'gui-input','placeholder' => '','id'=>'' )) !!}
                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-pencil"></i>
                              </label>                           
                            </label>
                          </div>
                        </div>

                        <div class="clearfix"></div>

                        <div class="col-md-6">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Petitioner Name :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              {!! Form::text('petitioner',$get_record[0]->reg_petitioner,array('class' => 'gui-input','placeholder' => '','id'=>'' )) !!}
                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-pencil"></i>
                              </label>                           
                            </label>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Respondent Name :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              {!! Form::text('respondent',$get_record[0]->reg_respondent,array('class' => 'gui-input','placeholder' => '','id'=>'' )) !!}
                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-pencil"></i>
                              </label>                           
                            </label>
                          </div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="row">

                          <div class="col-md-6" style="padding-right: 0px;">

                            <div class="">
                              <span class="sub_group_new"> Petitioner Extra Party Details </span>
                            </div>

                        @php
                        $counters = 0;
                        $reg_petitioner = json_decode($get_record[0]->reg_petitioner_extra,true);
                        @endphp
                          @foreach($reg_petitioner as $reg_petitioners)
                            @php $counters++; @endphp
                            <div id="hide_pedi{{$counters}}">
                            <div class="questionrowss" id="">
                              <div class="questiondivss" id="questiondivss">

                                <div class="col-md-12">
                                  <div class="">
                                    <label for="level_name" class="field-label" style="font-weight:600;" > Petitioner {{$counters}}  </label> 
                                    <label for="level_name" class="field prepend-icon">
                                      <label for="Account Mobile" class="field-icon"></label>
                                    </label>
                                  </div>
                                </div>


                                <div class="col-md-12">
                                  <div class="section">
                                    <label for="level_name" class="field-label" style="font-weight:600;" > Petitioner Name :  </label> 
                                    <label for="level_name" class="field prepend-icon">
                                      {!! Form::text('petitioner_extra['.$counters.'][petitioner_name]',$reg_petitioners[petitioner_name],array('class' => 'gui-input','placeholder' => '','id'=>'' )) !!}
                                      <label for="Account Mobile" class="field-icon">
                                        <i class="fa fa-pencil"></i>
                                      </label>
                                    </label>
                                  </div>
                                </div>

                                <div class="col-md-12">
                                  <div class="section">
                                    <label for="level_name" class="field-label" style="font-weight:600;" >Choose Prefix : </label>
                                      <label for="artist_state" class="field">
                                        <select class="form-control select-text" id="" name="petitioner_extra['.$counters.'][petitioner_prefix]">
                                          <option value="">Select</option>
                                          <option value="S/O" {{ $reg_petitioners[petitioner_prefix] == "S/O" ? 'selected="selected"' : '' }} >S/O</option>
                                          <option value="F/O" {{ $reg_petitioners[petitioner_prefix] == "F/O" ? 'selected="selected"' : '' }} >F/O</option>
                                          <option value="D/O" {{ $reg_petitioners[petitioner_prefix] == "D/O" ? 'selected="selected"' : '' }} >D/O</option>
                                          <option value="C/O" {{ $reg_petitioners[petitioner_prefix] == "C/O" ? 'selected="selected"' : '' }}>C/O</option>
                                          <option value="W/O" {{ $reg_petitioners[petitioner_prefix] == "W/O" ? 'selected="selected"' : '' }} >W/O</option>
                                          <option value="Thr/O" {{ $reg_petitioners[petitioner_prefix] == "Thr/O" ? 'selected="selected"' : '' }} >Thr/O</option>                           
                                        </select>
                                        <i class="arrow"></i>
                                      </label>
                                  </div>
                                </div>
                                
                                <div class="col-md-12">
                                  <div class="section">
                                    <label for="level_name" class="field-label" style="font-weight:600;" > Petitioner Father Name :  </label> 
                                    <label for="level_name" class="field prepend-icon">
                                      {!! Form::text('petitioner_extra['.$counters.'][petitioner_father_name]',$reg_petitioners[petitioner_father_name],array('class' => 'gui-input','placeholder' => '','id'=>'' )) !!}
                                      <label for="Account Mobile" class="field-icon">
                                        <i class="fa fa-pencil"></i>
                                      </label>
                                    </label>
                                  </div>
                                </div>

                                <div class="col-md-12">
                                  <div class="section">
                                    <label for="level_name" class="field-label" style="font-weight:600;" > Petitioner Address :  </label> 
                                    <label for="level_name" class="field prepend-icon">
                                      {!! Form::textarea('petitioner_extra['.$counters.'][petition_address]',$reg_petitioners[petition_address],array('class' => 'gui-textarea','placeholder' => '','id'=>'' )) !!}
                                      <label for="Account Mobile" class="field-icon">
                                        <i class="fa fa-pencil"></i>
                                      </label>
                                    </label>
                                  </div>
                                </div>
                                
                                <div class="section">
                                  <div class="col-md-12" style="margin-left:10px !important;">
                                    <button type="button" onclick="removeRecord('{{$counters}}');" name="add" id="" class="button btn-danger pull-right mb10 mr10"><i class="fa fa-trash-o" aria-hidden="true"></i> &nbsp; Remove Fields
                                    </button></div>
                              </div>
                            </div> <div class="clearfix"> </div>
                          </div></div>

                          
                          @endforeach
                          <div id="questionrowss"></div>

                            @if($edit_id =="")
                            <div class="questionrowss" id="questionrowss">
                              <div class="questiondivss" id="questiondivss">
                                <!-- <div class="col-md-12">
                                  <div class="section" style="margin-left:0px !important;">
                                    <label for="level_name" class="field-label" style="font-weight:600;" >Petitioner Name :  </label>
                                    <label for="level_name" class="field prepend-icon">
                                      {!! Form::text('petitioner_extra[0][petitioner_name]',$get_record[0]->reg_petitioner_extra,array('class' => 'gui-input','placeholder' => '','id'=>'' )) !!}
                                      <label for="Account Mobile" class="field-icon">
                                        <i class="fa fa-pencil"></i>
                                      </label>
                                    </label>
                                  </div>
                                </div>
                                
                                <div class="col-md-12">
                                  <div class="section" style="margin-left:0px !important;">
                                    <label for="level_name" class="field-label" style="font-weight:600;" >Petitioner Father Name :  </label>
                                    <label for="level_name" class="field prepend-icon">
                                      {!! Form::text('petitioner_extra[0][petitioner_father_name]',$get_record[0]->reg_petitioner_extra,array('class' => 'gui-input','placeholder' => '','id'=>'' )) !!}
                                      <label for="Account Mobile" class="field-icon">
                                        <i class="fa fa-pencil"></i>
                                      </label>
                                    </label>
                                  </div>
                                </div>

                                <div class="col-md-12">
                                  <div class="section" style="margin-left:0px !important;">
                                    <label for="level_name" class="field-label" style="font-weight:600;" >Petitioner Address :  </label>
                                    <label for="level_name" class="field prepend-icon">
                                      {!! Form::textarea('petitioner_extra[0][petition_address]',$get_record[0]->reg_petitioner_extra,array('class' => 'gui-textarea','placeholder' => '','id'=>'' )) !!}
                                      <label for="Account Mobile" class="field-icon">
                                        <i class="fa fa-pencil"></i>
                                      </label>
                                    </label>
                                  </div>
                                </div> -->
                                
                                <div class="section" style="margin-top: -52px;">
                                  <div class="col-md-12"> 
                                    <button type="button" onclick="addRecord();" name="add" id="" class="button btn-primary pull-right mr10"> <i class="fa fa-plus" aria-hidden="true"></i> &nbsp; Add Extra Party </button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            @endif
                            @if($edit_id !="")
                            <div class="section" style="margin-top: -52px;">
                              <div class="section">
                                <div class="col-md-" style=""> 
                                  <button type="button" onclick="addRecord();" name="add" id="" class="button btn-primary pull-right mr10"> <i class="fa fa-plus" aria-hidden="true"></i> &nbsp; Add Extra Party </button>
                                </div>
                            </div>
                          </div>
                          @endif

                        <input type="hidden" name="countids1" id="countids1" value="{{count($reg_petitioner)}}"> <br>
                        <input type="hidden" name="countids2" id="countids2" value="0">

                      </div>

                      <div class="col-md-6" style="padding-left: 0px;">

                            <div class="">
                              <span class="sub_group_new"> Respondent Extra Party Details </span>
                            </div>

                        @php
                        $counterss = 0;
                        $respondent_extra = json_decode($get_record[0]->reg_respondent_extra,true);
                        @endphp
                          @foreach($respondent_extra as $respondent_extras)
                            @php $counterss++; @endphp
                            <div id="hide_resp{{$counterss}}">
                            <div class="questionrowsres" id="">
                              <div class="questiondivsres" id="questiondivsres">
                                

                                <div class="col-md-12">
                                  <div class="">
                                    <label for="level_name" class="field-label" style="font-weight:600;" >Respondent {{$counterss}}  </label> 
                                    <label for="level_name" class="field prepend-icon">
                                      <label for="Account Mobile" class="field-icon"></label>
                                    </label>
                                  </div>
                                </div>

                                <div class="col-md-12">
                                  <div class="section">
                                    <label for="level_name" class="field-label" style="font-weight:600;" >Respondent Name :  </label> 
                                    <label for="level_name" class="field prepend-icon">
                                      {!! Form::text('respondent_extra['.$counterss.'][respondent_name]',$respondent_extras[respondent_name],array('class' => 'gui-input','placeholder' => '','id'=>'' )) !!}
                                      <label for="Account Mobile" class="field-icon">
                                        <i class="fa fa-pencil"></i>
                                      </label>
                                    </label>
                                  </div>
                                </div>

                                <div class="col-md-12">
                                  <div class="section">
                                    <label for="level_name" class="field-label" style="font-weight:600;" >Choose Prefix : </label>
                                      <label for="artist_state" class="field">
                                        <select class="form-control select-text" id="" name="respondent_extra['.$counterss.'][respondent_prefix]">
                                          <option value="">Select</option>
                                          <option value="S/O" {{ $respondent_extras[respondent_prefix] == "S/O" ? 'selected="selected"' : '' }} >S/O</option>
                                          <option value="F/O" {{ $respondent_extras[respondent_prefix] == "F/O" ? 'selected="selected"' : '' }} >F/O</option>
                                          <option value="D/O" {{ $respondent_extras[respondent_prefix] == "D/O" ? 'selected="selected"' : '' }} >D/O</option>
                                          <option value="C/O" {{ $respondent_extras[respondent_prefix] == "C/O" ? 'selected="selected"' : '' }}>C/O</option>
                                          <option value="W/O" {{ $respondent_extras[respondent_prefix] == "W/O" ? 'selected="selected"' : '' }} >W/O</option>
                                          <option value="Thr/O" {{ $respondent_extras[respondent_prefix] == "Thr/O" ? 'selected="selected"' : '' }} >Thr/O</option>                           
                                        </select>
                                        <i class="arrow"></i>
                                      </label>
                                  </div>
                                </div>
                                
                                <div class="col-md-12">
                                  <div class="section">
                                    <label for="level_name" class="field-label" style="font-weight:600;" > Respondent Guardian Name :  </label> 
                                    <label for="level_name" class="field prepend-icon">
                                      {!! Form::text('respondent_extra['.$counterss.'][respondent_father_name]',$respondent_extras[respondent_father_name],array('class' => 'gui-input','placeholder' => '','id'=>'' )) !!}
                                      <label for="Account Mobile" class="field-icon">
                                        <i class="fa fa-pencil"></i>
                                      </label>
                                    </label>
                                  </div>
                                </div>

                                <div class="col-md-12">
                                  <div class="section">
                                    <label for="level_name" class="field-label" style="font-weight:600;" > Respondent Address :  </label> 
                                    <label for="level_name" class="field prepend-icon">
                                      {!! Form::textarea('respondent_extra['.$counterss.'][respondent_address]',$respondent_extras[respondent_address],array('class' => 'gui-textarea','placeholder' => '','id'=>'' )) !!}
                                      <label for="Account Mobile" class="field-icon">
                                        <i class="fa fa-pencil"></i>
                                      </label>
                                    </label>
                                  </div>
                                </div>
                                
                                <div class="section">
                                  <div class="col-md-12" style="margin-left:10px !important;">
                                    <button type="button" onclick="removeRecords('{{$counterss}}');" name="add" id="" class="button btn-danger pull-right mr10 mb10"><i class="fa fa-trash-o" aria-hidden="true"></i> &nbsp; Remove Fields
                                    </button>
                                    </div>
                              </div>
                            </div> <div class="clearfix"> </div>
                          </div></div>
                            @endforeach

                            <div id="questionrowsres"></div>

                            @if($edit_id =="")
                            <div class="questionrowsres" id="questionrowsres">
                              <div class="questiondivsres" id="questiondivsres">
                                
                                <!-- <div class="col-md-12">
                                  <div class="section">
                                    <label for="level_name" class="field-label" style="font-weight:600;" >Respondent Name :  </label>
                                    <label for="level_name" class="field prepend-icon">
                                      {!! Form::text('respondent_extra[0][respondent_name]',$get_record[0]->reg_respondent_extra,array('class' => 'gui-input','placeholder' => '','id'=>'' )) !!}
                                      <label for="Account Mobile" class="field-icon">
                                        <i class="fa fa-pencil"></i>
                                      </label>
                                    </label>
                                  </div>
                                </div>
                                
                                <div class="col-md-12">
                                  <div class="section">
                                    <label for="level_name" class="field-label" style="font-weight:600;" >Respondent Father Name :  </label>
                                    <label for="level_name" class="field prepend-icon">
                                      {!! Form::text('respondent_extra[0][respondent_father_name]',$get_record[0]->reg_respondent_extra,array('class' => 'gui-input','placeholder' => '','id'=>'' )) !!}
                                      <label for="Account Mobile" class="field-icon">
                                        <i class="fa fa-pencil"></i>
                                      </label>
                                    </label>
                                  </div>
                                </div>

                                <div class="col-md-12">
                                  <div class="section">
                                    <label for="level_name" class="field-label" style="font-weight:600;" >Respondent Address :  </label>
                                    <label for="level_name" class="field prepend-icon">
                                      {!! Form::textarea('respondent_extra[0][respondent_address]',$get_record[0]->reg_respondent_extra,array('class' => 'gui-textarea','placeholder' => '','id'=>'' )) !!}
                                      <label for="Account Mobile" class="field-icon">
                                        <i class="fa fa-pencil"></i>
                                      </label>
                                    </label>
                                  </div>
                                </div> -->
                                
                                <div class="section" style="margin-top: -52px;">
                                  <div class="col-md-12"> 
                                    <button type="button" onclick="addrecord();" name="add" id="" class="button btn-primary pull-right"> <i class="fa fa-plus" aria-hidden="true"></i> &nbsp; Add Extra Party </button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            @endif
                            @if($edit_id !="")
                            <div class="section" style="margin-top: -52px;">
                              <div class="section">
                                <div class="col-md-12" style=""> 
                                  <button type="button" onclick="addrecord();" name="add" id="" class="button btn-primary pull-right"> <i class="fa fa-plus" aria-hidden="true"></i> &nbsp; Add Extra Party </button>
                                </div>
                            </div>
                          </div>
                          @endif

                        <input type="hidden" name="countidres1" id="countidres1" value="{{count($respondent_extra)}}"> <br>
                        <input type="hidden" name="countidres2" id="countidres2" value="0">

                      </div>

                      </div>
                      <div class="clearfix"><br></div>


                      <div class="col-md-3">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Client Group/Clients : </label>
                              <!-- <label for="artist_state" class="field select"> -->
                                <label for="level" class="field prepend-icon">
                                @if($get_record[0]->reg_client_group_id != "")
                                  <select class="form-control select-text" name="client_group" onclick="sub_group_change_ajax(this.value)">
                                @else
                                  <select class="form-control select-text" name="client_group" onchange="sub_group_change_ajax(this.value)">
                                @endif

                                  <option value=''> Select Client Group/Clients</option>
                                  @if($get_record != "") 
                                   @foreach($client as $clients) 
                                     <option value="{{ $clients->cl_id }}" {{ $clients->cl_id == $get_record[0]->reg_client_group_id ? 'selected="selected"' : '' }} >{{$clients->cl_group_name}} @if($clients->cl_father_name != "") {{ $clients->cl_name_prefix }} {{ $clients->cl_father_name }} @endif </option>
                                   @endforeach
                                   @else
                                   @foreach($client as $clients)
                                     <option value="{{ $clients->cl_id }}">{{$clients->cl_group_name}} @if($clients->cl_father_name != "") {{ $clients->cl_name_prefix }} {{ $clients->cl_father_name }} @endif </option>
                                   @endforeach
                                   @endif                                 
                                </select>
                                <i class="arrow double"></i>
                              </label>
                          </div>
                        </div>

                        @if($get_record[0]->reg_client_subgroup_id != "")

                          <div class="col-md-3" id="show_client_sub_group">
                            <div class="section">
                              <label for="level_name" class="field-label" style="font-weight:600;" >Client Sub Group : </label>
                                <!-- <label for="artist_state" class="field select"> -->
                                  <label for="level" class="field prepend-icon">
                                  <select class="form-control select-text" id="sub_group_change" name="client_sub_group">
                                    <option value=''>Select Client Sub Group</option>  
                                    @foreach($sub_client as $sub_clients) 
                                       <option value="{{ $sub_clients->sub_client_id }}" {{ $sub_clients->sub_client_id == $get_record[0]->reg_client_subgroup_id ? 'selected="selected"' : '' }} >{{$sub_clients->sub_client_name}} </option>
                                     @endforeach                              
                                  </select>
                                  <i class="arrow double"></i>
                                </label>
                            </div>
                          </div>

                        @endif

                        <div class="col-md-3" style="display: none;" id="show_client_sub_group">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Client Sub Group : </label>
                              <!-- <label for="artist_state" class="field select"> -->
                                <label for="level" class="field prepend-icon">
                                <select class="form-control select-text" id="sub_group_change" name="client_sub_group">
                                  <option value=''>Select Client Sub Group</option>  
                                  @foreach($sub_client as $sub_clients) 
                                     <option value="{{ $sub_clients->sub_client_id }}" {{ $sub_clients->sub_client_id == $get_record[0]->reg_client_subgroup_id ? 'selected="selected"' : '' }} >{{$sub_clients->sub_client_name}} </option>
                                   @endforeach                              
                                </select>
                                <i class="arrow double"></i>
                              </label>
                          </div>
                        </div>

                        <div class="col-md-3">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Referred By : </label>
                              <!-- <label for="artist_state" class="field select"> -->
                                <select class="form-control select-text" id="" name="reffered">
                                  <option value=''>Select Referred By</option>
                                  @if($get_record != "") 
                                   @foreach($reffered as $reffereds) 
                                     <option value="{{ $reffereds->ref_id }}" {{ $reffereds->ref_id == $get_record[0]->reg_reffeerd_by_id ? 'selected="selected"' : '' }} >{{$reffereds->ref_advocate_name}} </option>
                                   @endforeach
                                   @else
                                   @foreach($reffered as $reffereds)
                                     <option value="{{ $reffereds->ref_id }}">{{$reffereds->ref_advocate_name}} </option>
                                   @endforeach
                                   @endif                                 
                                </select>
                                <i class="arrow double"></i>
                              </label>
                          </div>
                        </div>

                        
                        <div class="col-md-3">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Assigned : </label>
                              <!-- <label for="artist_state" class="field select"> -->
                                <label for="level" class="field prepend-icon">
                                <select class="form-control select-text" id="" name="assigned">
                                  <option value=''> Select Assigned</option>
                                  @if($get_record != "") 
                                   @foreach($assigned as $assigneds) 
                                     <option value="{{ $assigneds->assign_id }}" {{ $assigneds->assign_id == $get_record[0]->reg_assigend_id ? 'selected="selected"' : '' }} >{{$assigneds->assign_advocate_name}} </option>
                                   @endforeach
                                   @else
                                   @foreach($assigned as $assigneds)
                                     <option value="{{ $assigneds->assign_id }}">{{$assigneds->assign_advocate_name}} </option>
                                   @endforeach
                                   @endif                                 
                                </select>
                                <i class="arrow double"></i>
                              </label>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Act : </label>
                              <!-- <label for="artist_state" class="field select"> -->
                                <label for="level" class="field prepend-icon">
                                @if($get_record != "")
                                  <select class="form-control select-text" name="act" onclick="act_type_change_ajax(this.value)">
                                @else 
                                  <select class="form-control select-text" name="act" onchange="act_type_change_ajax(this.value)">
                                @endif

                                  <option value=''> Select Act</option>
                                  @if($get_record != "") 
                                   @foreach($act as $acts) 
                                     <option value="{{ $acts->act_id }}" {{ $acts->act_id == $get_record[0]->reg_act_id ? 'selected="selected"' : '' }} >{{$acts->act_name}} </option>
                                   @endforeach
                                   @else
                                   @foreach($act as $acts)
                                     <option value="{{ $acts->act_id }}">{{$acts->act_name}} </option>
                                   @endforeach
                                   @endif                                 
                                </select>
                                <i class="arrow double"></i>
                              </label>
                          </div>
                        </div>

                        @php
                          $section =  \App\Model\Section\Section::where(['section_act_id' => $get_record[0]->reg_act_id ])->where('section_name', '!=' ,'')->get();
                        @endphp

                        <div class="col-md-3">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Section : </label>
                            <label for="artist_state" class="field" style="height: 28px;" id="show_act_sub_type_section">
                              <select id="framework_3" name="section[]" multiple class="form-control">
                                  @foreach($section as $sections) 

                                  @php
                                    $arr = explode(',', $get_record[0]->reg_section_id);
                                    $sel1 = '';
                                    if ( in_array( $sections->section_id, $arr) ) {
                                      $sel1 = 'selected="selected"';  
                                    } 
                                  @endphp

                                    <option value="{{ $sections->section_id }}" {{ $sel1 }} >{{$sections->section_name}} </option>
                                  @endforeach    
                              </select>
                              <i class="arrow double"></i>
                            </label>
                            <!-- <label for="level_name" class="field-label" style="font-weight:600;" >Section : </label>
                              <label for="artist_state" class="field" style="height: 28px;">
                                <select id="framework" name="section[]" multiple class="form-control">
                                
                                            
                                  
                                  @if($get_record[0]->reg_section_id != "")
                                    @foreach($section as $sections) 
                                       <option value="{{ $sections->section_id }}" {{ $sections->section_id == $get_record[0]->reg_section_id ? 'selected="selected"' : '' }} >{{$sections->section_name}} </option>
                                     @endforeach                  
                                  @endif
                                </select>
                                <i class="arrow double"></i>
                              </label> -->
                          </div>
                        </div>




                        <div class="col-md-3">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Opposite Counsel :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              {!! Form::text('opposite_council',$get_record[0]->reg_opp_council,array('class' => 'gui-input','placeholder' => '','id'=>'' )) !!}
                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-pencil"></i>
                              </label>                           
                            </label>
                          </div>
                        </div>


                        <div class="col-md-3">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >FIR No :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              {!! Form::text('fir',$get_record[0]->reg_fir_id,array('class' => 'gui-input','placeholder' => '','id'=>'' )) !!}
                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-pencil"></i>
                              </label>                           
                            </label>
                          </div>
                        </div>

                        <div class="col-md-3">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;"> FIR Year : </label>
                              <!-- <label for="artist_state" class="field select"> -->
                                <label for="level" class="field prepend-icon">
                                <select class="form-control select-text" id="reg_fir_year" name="reg_fir_year">
                                  <option value=''>Select FIR Year</option>              
                                  
                                  @php
                                      $currentYear = date('Y');
                                  @endphp

                                  @foreach(range(1950, $currentYear) as $value)
                                      <option value={{$value}} {{ $value == $get_record[0]->reg_fir_year ? 'selected="selected"' : '' }}> {{$value}} </option>
                                  @endforeach

                                </select>
                                <i class="arrow double"></i>
                              </label>
                          </div>
                        </div>

                        <div class="col-md-3">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;"> Police Station :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              {!! Form::text('reg_police_thana',$get_record[0]->reg_police_thana,array('class' => 'gui-input','placeholder' => '','id'=>'' )) !!}
                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-pencil"></i>
                              </label>                           
                            </label>
                          </div>
                        </div>

                        @php
                          $prev_date =  \App\Model\Pessi\Pessi::where(['pessi_case_reg' => $get_record[0]->reg_id , 'pessi_status' => 1 ])->first();
                        @endphp

                        <div class="col-md-3">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Next Further Date </label>  
                            <label for="level_name" class="field prepend-icon">

                              @if($prev_date->pessi_further_date == "" || $prev_date->pessi_further_date == "1970-01-01")

                               {!! Form::text('futher','',array('class' => 'gui-input todate_notice_new','placeholder' => '','autocomplete'=>'off' )) !!}

                              @else

                              {!! Form::text('futher',date('d-m-Y',strtotime($prev_date->pessi_further_date)) ,array('class' => 'gui-input todate_notice_new','placeholder' => '','autocomplete'=>'off' )) !!}

                              @endif
                             
                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-calendar"></i>
                              </label>                           
                              <div></div>
                            </label>
                            <!-- <label for="level_name" class="field-label"> <b>Note :-</b> Date format will be dd-mm-yyyy </label> -->
                          </div>
                        </div>

                        <div class="clearfix"></div>

                        <div class="col-md-6">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;"> Remark :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              {!! Form::textarea('reg_remark',$get_record[0]->reg_remark,array('class' => 'gui-textarea','placeholder' => '','id'=>'' )) !!}
                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-pencil"></i>
                              </label>                           
                            </label>
                          </div>
                        </div>


                        <div class="col-md-6">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;"> Other Field :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              {!! Form::textarea('reg_other_field',$get_record[0]->reg_other_field,array('class' => 'gui-textarea','placeholder' => '','id'=>'' )) !!}
                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-pencil"></i>
                              </label>                           
                            </label>
                          </div>
                        </div>
                        
                        <!-- Send SMS -->

                        <div class="col-md-12">
                          <div class="field" align="left">
                            <label for="documents" class="field-label" style="font-weight:600;" > <span style="float: left;margin-bottom: 10px;"> Send SMS </span> </label>
                          </div>
                        </div>

                        <div class="col-md-12">
                          <div class="section" style="margin-bottom: 40px;">  
                            <div class="col-md-3">
                              <label class="option block mn" style="font-size:12px;"> 
                                {!! Form::checkbox('sms_type[]','1','', array( 'class' => 'check' , 'class' => 'check' )) !!}
                                <span class="checkbox mn"></span> Client
                              </label>
                            </div>

                            <div class="col-md-3">
                              <label class="option block mn" style="font-size:12px;">
                                {!! Form::checkbox('sms_type[]','2', '', array( 'class' => 'check'  )) !!}
                                <span class="checkbox mn"></span> Referred
                              </label>
                            </div>

                            <div class="col-md-3">
                              <label class="option block mn" style="font-size:12px;">
                                {!! Form::checkbox('sms_type[]','3','', array( 'class' => 'check', 'id' => 'sms_type' ,'onclick'=>'reg_sms_type()'  )) !!}
                                <span class="checkbox mn"></span> Other
                              </label>
                            </div>
                          </div>
                        </div>

                        <div class="col-md-4" style="display: none;" id="show_other_mobile">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;"> Other Mobile No. :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              {!! Form::text('reg_other_mobile','',array('class' => 'gui-input','placeholder' => '' )) !!}
                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-pencil"></i>
                              </label>                           
                            </label>
                          </div>
                        </div>

                        <div class="col-md-4">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;"> Text :  </label>
                            <label for="level_name" class="field prepend-icon">
                              {!! Form::text('reg_sms_text', '' ,array('class' => 'gui-input','placeholder' => '' )) !!}
                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-pencil"></i>
                              </label>                           
                            </label>
                          </div>
                        </div>
                        
                        
<!-- 
                        <div class="col-md-12">
                          <div class="field" align="left">
                            <label for="documents" class="field-label" style="font-weight:600;" > <span style="float: left;margin-bottom: 10px;"> Upload Documents</span> <span id="cleardiv" style="float: right;display: none;"><a style="cursor: pointer;" onclick="remove_img()">Clear</a></span> </label>
                            <input type="file" id="registration_document" name="img_reg[]" multiple style="padding: 10px;border: 1px solid #ccc;width: 100%;margin-bottom: 20px;" />
                          </div>
                        </div> -->

                        <!-- <div class="col-md-12">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;" >File Upload :  </label>  
                          <label for="level_name" class="field maker_file_upload">
                            {!! Form::file('img_reg[]',array('class' => 'gui-input image_register','multiple' => 'multiple','id'=>'' )) !!}                          
                          </label>
                        </div>
                        </div> -->

                        <!-- <div>
                          @if($get_record != "")
                            @php
                              $images_cat =  \App\Model\Upload_Document\Upload_Document::where(['upload_case_id' => $get_record[0]->reg_id ])->get();
                            @endphp
                          @endif
                          @if(!empty($images_cat))
                            @foreach($images_cat as $images_cats) 
                              <div class="col-md-3" style="margin-bottom: 22px;position: relative;" id="cat_img{{$images_cats['upload_id']}}">
                              <div onclick="remove_image({{$images_cats['upload_id']}})" style="position: absolute;font-size: 25px;color: #232121;right: 21px;top: 6px;cursor: pointer;"><i class="fa fa-times-circle"></i></div>
                                {!! Html::image($images_cats['upload_images'], '', array('class' => 'media-object mw150 customer_profile', 'width'=>'100%', 'height'=>'250')) !!}
                              </div>
                            @endforeach
                          @endif
                        </div>

                        <div class="document_image">
                          <div id="document_image"></div>
                        </div> -->

                     </div>

                  <div class="panel-footer text-right">
                      {!! Form::submit('Save', array('class' => 'button btn-primary', 'id' => 'maskedKey_submit')) !!}
                      {!! Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}
                  </div>   
                    {!! Form::close() !!}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>


<style type="text/css">

.divider{
  display: none;
}
#form_add_question .field-icon {
    margin-top: 0px !important;
}

</style>

<script type="text/javascript">

  function remove_img() {
    $("#registration_document").val('');
    $('.document_image').html('<div id="document_image"></div>');
  }

  $(document).ready(function() {


  $('#framework_3').multiselect({
    nonSelectedText: 'Select Section',
    enableFiltering: true,
    enableCaseInsensitiveFiltering: true,
    buttonWidth:'400px'
   });



    if (window.File && window.FileList && window.FileReader) {
      $("#registration_document").on("change", function(e) {
        $("#cleardiv").show();  
        $('.document_image').html('<div id="document_image"></div>');
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
        var files = e.target.files,
          filesLength = files.length;
        for (var i = 0; i < filesLength; i++) {
          var f = files[i]
          var fileReader = new FileReader();
          var file = e.target;
          
         if (regex.test(f.name.toLowerCase())) {
  
            fileReader.onload = (function(e) {          
              $("<div id class=\"col-md-3\">" +
                "<img class=\"imageThumb\" width=\"100%\" height=\"250\" style=\"margin-bottom:22px;\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
                "<br/><span class=\"\"></span>" +
                "</div>").insertAfter("#document_image");
            });

          } else {
            alert(f.name + " is not a valid image file.");
            dvPreview.innerHTML = "";
            return false;
          }

          fileReader.readAsDataURL(f);
        }
      });
    } else {
      alert("Your browser doesn't support to File API")
    }
  });


  // remove image 

  function remove_image(imid) {
  
      $('#lodding_image').show();
      $.ajax({
       url:'{{url("advocate-panel/delete-document")}}'+'/'+imid,  
       success:function(response){
      // alert(response);
       $('#lodding_image').hide();
       $('#cat_img'+imid).remove();
       }
      });
    
  }


  // power respondent 

  function reg_power_respondent(reg_power_respondent){

    if(reg_power_respondent == 2){
      $('#reg_power_respondent').show();
    } else {
      $('#reg_power_respondent').hide();
    }

  }

  // sms type

  function reg_sms_type(){

    if ($('#sms_type').is(":checked")){
      $('#show_other_mobile').show();
    } else {
      $('#show_other_mobile').hide();      
    }

  }
  // court_type_trail

  function court_type_trail(court_type) {
    
    document.getElementById("hide_show_class_code").style.display = "none";
    document.getElementById("hide_show_ncv").style.display = "block";
    

    BASE_URL = '{{ url('/')}}';
      $.ajax({
        url:BASE_URL+"/advocate-panel/get-all-court/"+court_type,
        success: function(result){
            $("#display_high_court").html(result);
        }
      });

  }

  // court_type_high

  function court_type_high(court_type) {
    
    

    BASE_URL = '{{ url('/')}}';
      $.ajax({
        url:BASE_URL+"/advocate-panel/get-all-court/"+court_type,
        success: function(result){
            $("#display_high_court").html(result);
        }
      });

    document.getElementById("hide_show_ncv").style.display = "none";
    document.getElementById("hide_show_class_code").style.display = "block";
    document.getElementById("hide_show_ncv_new").style.display = "none";
    

  }

  // choose_sub_type

  function choose_sub_type(client_id){

    if (client_id != ''){
      BASE_URL = '{{ url('/')}}';
      $.ajax({
        url:BASE_URL+"/advocate-panel/change-client-sub-type-ajax/"+client_id,
        success: function(result){
            $("#hide_choose_sub_type").html(result);
        }
      });

      
      $.ajax({
        url:BASE_URL+"/advocate-panel/get-case-category/"+client_id,
        success: function(result){
            if(result.status == true){
              $("#reg_case_type_category").val(1);
              document.getElementById("fir_show").style.display = "block";
            } else {
              $("#reg_case_type_category").val(2);
              document.getElementById("fir_show").style.display = "none";
            }
        }
      });
    }
  }

  // check case no

  function check_case_no(case_number){

  //  alert(case_number);

    BASE_URL = '{{ url('/')}}';
    var reg_case_type_category = $("#reg_case_type_category").val();
    var case_type = $("#case_type").val();
    
    $.ajax({
      url:BASE_URL+"/advocate-panel/check-case-number/"+case_number+"/"+case_type+"/"+reg_case_type_category,
      success: function(result){
        if(result.status == true){
          $("#case_number_error").html('<div class="state-error"></div><em for="services_excerpt" class="state-error"> This case number already exits </em>');
          $(".case_number_error").removeClass("state-success");
          $(".case_number_error").addClass("state-error");
          $('#maskedKey_submit').prop('disabled', true);
          return false;
          
        } else {
          $('#maskedKey_submit').prop('disabled', false);
          $(".case_number_error").removeClass("state-error");
          $(".case_number_error").addClass("state-success");
          $("#case_number_error").html("");
        }
      }
    });
  }

// function type_change_ajax_id(type_id){

//   if (type_id != ''){
//     BASE_URL = '{{ url('/')}}';
//     $.ajax({
//       url:BASE_URL+"/advocate-panel/change-type-fir-change/"+type_id,
//       success: function(result){
//           $("#type_change_ajax").html(result); 
//       }
//     });
//   }

// }


function act_type_change_ajax(act_type_id){

  if (act_type_id != ''){
    BASE_URL = '{{ url('/')}}';
    $.ajax({
      url:BASE_URL+"/advocate-panel/change-act-sub-type-section-ajax/"+act_type_id,
      success: function(result){
          $("#show_act_sub_type_section").html(result); 
      }
    });
  }

}

function sub_group_change_ajax(sub_group_change_id){

  if (sub_group_change_id != ''){
    BASE_URL = '{{ url('/')}}';
    $.ajax({
      url:BASE_URL+"/advocate-panel/change-sub-group-ajax/"+sub_group_change_id,
      success: function(result){
          
          if(result.status == true){
            document.getElementById("show_client_sub_group").style.display = "none";
          } else {
            document.getElementById("show_client_sub_group").style.display = "block";
            $("#sub_group_change").html(result); 
          }

      }
    });

  }

}

</script>


<script>

function myFunction() {
    var x = document.getElementById("myDIV");
    if (x.style.display === "none") {
        x.style.display = "block";
    }else{
        x.style.display = "block";
    }
}
</script>

<style>
#myDIV{
    width: 100%;
    padding: 50px 0;
    text-align: left !important;
    margin-top: 20px;
    display:none !important;
}
</style>
<script>

</script>
<script>

    // function showhide()
    //  {
    // var div = document.getElementById("myDIV");
    // if (div.style.display !== "none") {
    //     div.style.display = "block";
    // }
    //   div.style.display = "none";
    // }
     
</script>

<style>
#subtype {
    width: 100%;
    padding: 50px 0;
    text-align: left !important;
    margin-top: 20px;
    display:none;
}
</style>
<script type="text/javascript">


  


  // $(document).on("keyup",".todate_notice",function(){
  //     $('.mysave').removeAttr('disabled');
  //     var  currentdate = this.value;
  //     var n=$(this);    

  //     var checkdate = /^([0]?[1-9]|[1|2][0-9]|[3][0|1])[-]([0]?[1-9]|[1][0-2])[-]([0-9]{4}|[0-9]{4})$/.test(currentdate);
      
  //     if(checkdate == true) {
  //       n.next().next().html("");
  //       n.next().next().css("color", "#DE888A");
  //       n.parent().addClass("state-success");
  //       $('.mysave').removeAttr('disabled');
  //     } else {
  //       n.next().next().html("Enter date in dd-mm-yyyy format.");
  //       n.next().next().css("color", "#DE888A");
  //       n.parent().addClass("state-error");
  //       n.parent().removeClass("state-success");
  //       $(this).closest('.mysave').html('');
  //       $('.mysave').attr('disabled','disabled');
  //       return false;
  //     }
  // });


  $(document).on("keyup",".todate_notice_new",function(){
      $('#maskedKey_submit').removeAttr('disabled');
      var  currentdate = this.value;
      var n=$(this);    

      if(currentdate != ""){
        var checkdate = /^([0]?[1-9]|[1|2][0-9]|[3][0|1])[-]([0]?[1-9]|[1][0-2])[-]([0-9]{4}|[0-9]{4})$/.test(currentdate);

        if(checkdate == true) {

          value=currentdate.split("-");
          var newDate_notice =value[1]+"/"+value[0]+"/"+value[2];
          var notice_status_date = new Date(newDate_notice).getTime();
          
          var notice_issue_date = $('#date_picker').val();
          notice_issue_date = notice_issue_date.split("-");
          var newDate_notice_issue = notice_issue_date[1]+"/"+notice_issue_date[0]+"/"+notice_issue_date[2];
          var notice_issue_date = new Date(newDate_notice_issue).getTime();


          if(notice_status_date >= notice_issue_date){
            n.next().next().html("");
            n.next().next().css("color", "#DE888A");
            n.parent().addClass("state-success");
            $('#maskedKey_submit').removeAttr('disabled');
          } else {
            n.next().next().html("Next date should be greater than or equal to case registration date.");
            n.next().next().css("color", "#DE888A");
            n.parent().addClass("state-error");
            n.parent().removeClass("state-success");
            $(this).closest('.mysave').html('');
            $('#maskedKey_submit').attr('disabled','disabled');
            return false;
          }

        } else {
          n.next().next().html("Enter date in dd-mm-yyyy format.");
          n.next().next().css("color", "#DE888A");
          n.parent().addClass("state-error");
          n.parent().removeClass("state-success");
          $(this).closest('.mysave').html('');
          $('#maskedKey_submit').attr('disabled','disabled');
          return false;
        }
      } else {
          n.next().next().html("");
            n.next().next().css("color", "#DE888A");
            n.parent().addClass("state-success");
            $('#maskedKey_submit').removeAttr('disabled');
      }
  });




  jQuery(document).ready(function() {
    

    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "This field contains alphabets only");

    jQuery.validator.addMethod("checkemail", function(value, element) 
    {
    return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
    }, "Enter a VALID email address"); 

    $.validator.addMethod('minStrict', function (value, el, param) {
     return value > param; 
    });
    $.validator.addMethod('maxStrict', function (value, el, param) {
     return value < param; 
    });


    jQuery.validator.addMethod("checkdate", function(value, element) 
    {
    return this.optional(element) || /^([0]?[1-9]|[1|2][0-9]|[3][0|1])[-]([0]?[1-9]|[1][0-2])[-]([0-9]{4}|[0-9]{4})$/.test(value);
    }, "Enter date in dd-mm-yyyy format");



    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#form_add_question").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        img_reg: {
          // required: true,
          extension: 'jpeg,jpg,png',
        },
        // opposite_council: {
        //   required: true
        // },
        // client_sub_group: {
        //   required: true
        // },
        // type: {
        //   required: true
        // },
        // sub_type: {
        //   required: true
        // },
        // choose_type: {
        //   required: true
        // },
        date_picker: {
          required: true,
          checkdate: true
        },
        // court: {
        //   required: true
        // },
        // fir: {
        //   required: true
        // },
        petitioner: {
          required: true,
      //    lettersonly: true
        },
        respondent: {
          required: true,
      //    lettersonly: true
        },
        // extra_party: {
        //   required: true
        // },
        // reg_stage: {
        //   required: true
        // },
        // power_respondent: {
        //   minlength: 10,
        //   maxlength: 12,
        // },
        // assigned: {
        //   required: true
        // },
        // act: {
        //   required: true
        // },
        // section: {
        //   required: true
        // },
        // client_group: {
        //   required: true
        // },
        // reffered: {
        //   required: true
        // },
        // sub_group: {
        //   required: true
        // },
        // case_no: {
        //   required: true
        // },
        // ncv: {
        //   required: true
        // },
        // futher: {
        //   checkdate: true,
        //   greaterThan_reg: true,
        // },
      },
      /* @validation error messages 
      ---------------------------------------------- */

      messages: {
        img_reg: {
          extension: 'Image Should be in .jpg,.jpeg and .png format only'
        },
        opposite_council: {
          required: 'Please enter Opposite Counsel'
        },
        client_sub_group: {
          required: 'Please select client sub group'
        },
        type: {
          required: 'Please select case type'
        },
        sub_type: {
          required: 'Please select class code'
        },
        choose_type: {
          required: 'Please Fill Required Choose Type'
        },
        date_picker: {
          required: 'Please select date'
        },
        court: {
          required: 'Please Fill Required Court'
        },
        fir: {
          required: 'Please enter FIR no.'
        },
        petitioner: {
          required: 'Please enter petitioner name'
        },
        respondent: {
          required: 'Please enter respondent name'
        },
        extra_party: {
          required: 'Please Fill Required Extra Party'
        },
        // power_respondent: {
        //   minlength:'Please enter valid mobile number',
        //   maxlength:'Please enter valid mobile number',
        // },
        reg_stage: {
          required: 'Please select stage'
        },        
        assigned: {
          required: 'Please select assigned'
        },
        act: {
          required: 'Please select act'
        },
        section: {
          required: 'Please select section'
        },
        client_group: {
          required: 'Please select client group'
        },
        reffered: {
          required: 'Please select Referred By'
        },
        sub_group: {
          required: 'Please select client sub group'
        },
        case_no: {
          required: 'Please enter case no.'
        },
        ncv: {
          required: 'Please select NCV no.'
        },
        futher: {
          required: 'Please select next further date'
        },
      },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });
  // petitioner party code

  function addRecord(){
    var count1 = $('#countids1').val();
    var count2 = $('#countids2').val();

    var counter1 = parseInt(count1) + 1;
    var counter2 = parseInt(count2) + 1;
    $('#countids1').val(counter1);
    $('#countids2').val(counter2);

    if(count2 >= 0){
      $('#questionrowss').append('<div class="questiondivss" id="questiondivss'+counter1+'"><div class="divider"></div><div class="col-md-12"><div class="" id="peti_counter'+counter2+'"> <label for="level_name" class="field-label" style="font-weight:600;" >Petitioner '+counter2+' </label><label for="level_name" class="field prepend-icon"><label for="Account Mobile" class="field-icon"></label></label></div></div> <div class="col-md-12"><div class="section"><label for="level_name" class="field-label" style="font-weight:600;" >Petitioner Name : </label><label for="level_name" class="field prepend-icon"><input type="text" name="petitioner_extra['+counter1+'][petitioner_name]" value="" class="gui-input" placeholder="" ><label for="Account Mobile" class="field-icon"><i class="fa fa-pencil"></i></label></label></div></div><div class="col-md-12"><div class="section"><label for="level_name" class="field-label" style="font-weight:600;" >Choose Prefix : </label><label for="artist_state" class="field"><select class="form-control select-text" id="" name="petitioner_extra['+counter1+'][petitioner_prefix]"><option value="">Select</option><option value="S/O">S/O</option><option value="F/O">F/O</option><option value="D/O">D/O</option><option value="C/O">C/O</option><option value="W/O">W/O</option><option value="Thr/O">Thr/O</option></select><i class="arrow"></i></label></div></div><div class="col-md-12"><div class="section"><label for="level_name" class="field-label" style="font-weight:600;" >Petitioner Guardian Name : </label><label for="level_name" class="field prepend-icon"><input type="text" name="petitioner_extra['+counter1+'][petitioner_father_name]" value="" class="gui-input" placeholder="" ><label for="Account Mobile" class="field-icon"><i class="fa fa-pencil"></i></label></label></div></div><div class="col-md-12"><div class="section"><label for="level_name" class="field-label" style="font-weight:600;" >Petitioner Address : </label><label for="level_name" class="field prepend-icon"><textarea name="petitioner_extra['+counter1+'][petition_address]" value="" class="gui-textarea" placeholder="" /><label for="Account Mobile" class="field-icon"><i class="fa fa-pencil"></i></label></label></div></div><div class="section"><div class="col-md-12"><button type="button" onclick="removeRecord('+counter1+');" name="add" id="" class="button btn-danger pull-right mb10"> <i class="fa fa-trash-o" aria-hidden="true"></i> &nbsp; Remove Fields </button></div></div></div></div>');
    }
  }


  // <label for="level_name" class="field-label" style="font-weight:600;" >Petitioner '+counter2+' </label><label for="level_name" class="field prepend-icon"><label for="Account Mobile" class="field-icon"></label></label>

  function removeRecord(countnew){

    var count2 = $('#countids2').val();
    var counter2 = parseInt(count2) - 1;

    $( "#peti_counter"+count2+"" ).html('<label for="level_name" class="field-label" style="font-weight:600;" >Petitioner '+counter2+' </label><label for="level_name" class="field prepend-icon"><label for="Account Mobile" class="field-icon"></label></label>');

    $( "#questiondivss"+countnew+"" ).remove();
    $( "#hide_pedi"+countnew+"" ).remove();

    
    $('#countids2').val(counter2);
  }

  // respondence code
  function addrecord(){
    var count1 = $('#countidres1').val();
    var count2 = $('#countidres2').val();

    var counter1 = parseInt(count1) + 1;
    var counter2 = parseInt(count2) + 1;
    $('#countidres1').val(counter1);
    $('#countidres2').val(counter2);

    if(count2 >= 0){
      $('#questionrowsres').append('<div class="questiondivsres" id="questiondivsres'+counter1+'"><div class="divider"></div><div class="col-md-12"><div class="" id="resp_counter'+counter2+'"><label for="level_name" class="field-label" style="font-weight:600;" >Respondent '+counter2+' </label><label for="level_name" class="field prepend-icon"><label for="Account Mobile" class="field-icon"></label></label></div></div> <div class="col-md-12"><div class="section"><label for="level_name" class="field-label" style="font-weight:600;" >Respondent Name : </label><label for="level_name" class="field prepend-icon"><input type="text" name="respondent_extra['+counter1+'][respondent_name]" value="" class="gui-input" placeholder="" ><label for="Account Mobile" class="field-icon"><i class="fa fa-pencil"></i></label></label></div></div><div class="col-md-12"><div class="section"><label for="level_name" class="field-label" style="font-weight:600;" >Choose Prefix : </label><label for="artist_state" class="field"><select class="form-control select-text" id="" name="respondent_extra['+counter1+'][respondent_father_name]"><option value="">Select</option><option value="S/O">S/O</option><option value="F/O">F/O</option><option value="D/O">D/O</option><option value="C/O">C/O</option><option value="W/O">W/O</option><option value="Thr/O">Thr/O</option></select><i class="arrow"></i></label></div></div><div class="col-md-12"><div class="section"><label for="level_name" class="field-label" style="font-weight:600;" >Respondent Guardian Name : </label><label for="level_name" class="field prepend-icon"><input type="text" name="respondent_extra['+counter1+'][respondent_father_name]" value="" class="gui-input" placeholder="" ><label for="Account Mobile" class="field-icon"><i class="fa fa-pencil"></i></label></label></div></div><div class="col-md-12"><div class="section"><label for="level_name" class="field-label" style="font-weight:600;" >Respondent Address : </label><label for="level_name" class="field prepend-icon"><textarea type="textarea" name="respondent_extra['+counter1+'][respondent_address]" value="" class="gui-textarea" placeholder="" /><label for="Account Mobile" class="field-icon"><i class="fa fa-pencil"></i></label></label></div></div><div class="section"><div class="col-md-12"><button type="button" onclick="removerecord('+counter1+');" name="add" id="" class="button btn-danger pull-right mb10"> <i class="fa fa-trash-o" aria-hidden="true"></i> &nbsp; Remove Fields </button></div></div></div></div>');
    }
  }

  function removerecord(countnew){

    var count2 = $('#countidres2').val();
    var counter2 = parseInt(count2) - 1;

    var newcouter = parseInt(count2) + 1;
    
    for (i = count2; i > 1; i--) { 
      var counter2new = parseInt(i) - 1;

      var countnewres = parseInt(i) + 1;

      $( "#resp_counter"+i+"" ).html('<label for="level_name" class="field-label" style="font-weight:600;" >Respondent '+counter2new+' </label><label for="level_name" class="field prepend-icon"><label for="Account Mobile" class="field-icon"></label></label>');



    }

    $( "#questiondivsres"+countnew+"" ).remove();
    $('#countidres2').val(counter2);
  }

  // extra party code

  function addRecords(){
    var count1 = $('#countid1').val();
    var count2 = $('#countid2').val();


    var counter1 = parseInt(count1) + 1;
    var counter2 = parseInt(count2) + 1;
    $('#countid1').val(counter1);
    $('#countid2').val(counter2);

    if(count2 >= 0){
      $('#questionrow').append('<div class="questiondiv" id="questiondiv'+count1+'"><div class="divider"></div><div class="col-md-9"><div class="section"><label for="level_name" class="field-label" style="font-weight:600;" >Extra Party : </label><label for="level_name" class="field prepend-icon"><input type="text" name="reg_extra_party['+count1+'][extra_party]" value="" class="gui-input" placeholder="" ><label for="Account Mobile" class="field-icon"><i class="fa fa-pencil"></i></label></label></div></div><div class="section"><div class="col-md-3"><button type="button" onclick="removeRecords('+count1+');" name="add" id="remove_case_reg" class="button btn-danger pull-right mr30"> <i class="fa fa-trash-o" aria-hidden="true"></i> &nbsp; Remove Fields </button></div></div></div></div>');
       // $('#countid').val(count);
    }
  }

  function removeRecords(countnew){
    $( "#questiondiv"+countnew+"" ).remove();
    $( "#hide_resp"+countnew+"" ).remove();

    var count2 = $('#countid2').val();
    var counter2 = parseInt(count2) - 1;
    $('#countid2').val(counter2);
  }


  </script>



@endsection
