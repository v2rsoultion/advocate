@extends('advocate_admin/layout')
@section('content')

<style type="text/css">
  
  blink, .blink {
      animation: blinker 1s linear infinite;
  }

 @keyframes blinker {  
      50% { opacity: 0; }
 }

  .table > thead > tr > th {
    font-weight: 500;
  }
  b{
    font-weight: 1000;
  }
</style>

  <!-- Start: Content-Wrapper -->
  <section id="content_wrapper">
    <header id="topbar" style="margin-top: 60px;">
      <div class="topbar-left">
        <ol class="breadcrumb">
          <li class="crumb-active">
            <a href="{{ url('/advocate-panel/view-case-form') }}">View Case Registration</a>
          </li>
          <li class="crumb-icon">
            <a href="{{ url('/advocate-panel/dashboard') }}">
              <span class="glyphicon glyphicon-home"></span>
            </a>
          </li>
          <li class="crumb-link">
            <a href="{{ url('/advocate-panel/dashboard') }}">Home</a>
          </li>
          <li class="crumb-trail">Case Registration Details </li>
        </ol>
      </div>
    </header>

    <div class="" style="margin-top:10px;">
      <div class="col-md-12">
        <div class="panel panel-primary panel-border top">  
          <div class="panel-heading">
            <div class="panel-title hidden-xs">
              <span class="glyphicon glyphicon-tasks"></span>Case Registration Details 

<!--               <span class="pull-right"> <a href="#" onclick="printInfo(this)">Print</a>  </span>

<script type="text/javascript">

function printInfo(){


  var prtContent = document.getElementById("content");
var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
WinPrint.document.write(prtContent.innerHTML);
WinPrint.document.close();
WinPrint.focus();
WinPrint.print();
WinPrint.close();
}

</script>
 -->
              <span class="pull-right"> @if($get_case_details->pessi_choose_type == 0) Pending @elseif($get_case_details->pessi_choose_type == 1) Disposal :- {{ date('d F Y',strtotime($get_case_details->created_at)) }} @else Due Course @endif </span>
            </div>

          </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>

      <section id="content" class="animated fadeIn">
        <div class="page-heading" style="background-color: #FFF; margin:-45px -9px 5px;">
            <div class="media clearfix">
              <div class="col-md-12">
                
                <div class="panel-heading" style="background: gray; color: white; font-size: 17px;">
                  <div class="panel-title hidden-xs text-center"> Case Details </div>
                </div>

                <div class="panel-body pn">
                  <table class="table table-striped table-hover" id="" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th class=""> <b> Court Name :- </b> @if( $get_case_details->court_name != "") {{ $get_case_details->court_name }} @else ----- @endif </th>
                      </tr>
                      <tr style="">
                        <th class="" > 
                          <div class="col-md-6" style="margin-left: -10px;"> 
                            <b> Institution Date :- </b> @if( $get_case_details->reg_date != "") {{ date('d/m/Y',strtotime($get_case_details->reg_date))  }} @else ----- @endif <br> 

                            <b> File No :- </b> @if( $get_case_details->reg_file_no != "") {{ $get_case_details->reg_file_no }} @else ----- @endif
                          </div>
                          <div class="col-md-6" style="border-left: 4px solid #eeeeee !important;"> 
                            <b> NCV No :- </b> @if( $get_case_details->reg_vcn_number != "") {{ $get_case_details->reg_vcn_number }} @else ----- @endif <br> 
                            <b> Class Code :- </b> @if( $get_case_details->classcode_name != "") {{ $get_case_details->classcode_name }} @else ----- @endif </div> 
                        </th>  
                      </tr>
                      <tr>
                        <th class=""> 
                          <b> Case Number :- 
                            @if( $get_case_details->case_name != "") {{ $get_case_details->case_name }} @else ---- @endif 
                            @if( $get_case_details->reg_case_number != "") {{ $get_case_details->reg_case_number }} @else ---- @endif
                            @if( $get_case_details->reg_date != "") {{ date('Y',strtotime($get_case_details->reg_date))  }} @else ----- @endif
                          </b></th>
                      </tr>
                      <tr style="">
                        <th class=""> <b> Case Title :- @if( $get_case_details->reg_petitioner != "") {{ $get_case_details->reg_petitioner }} @else ---- @endif v/s @if( $get_case_details->reg_respondent != "") {{ $get_case_details->reg_respondent }} @else ---- @endif </b></th>
                      </tr>
                      <tr>
                        <th class=""> <b> Power :- </b>
                          @if($get_case_details->reg_power == 1) Petitioner @elseif($get_case_details->reg_power == 2 ) @if($get_case_details->reg_power_respondent != "") Respondent - {{ $get_case_details->reg_power_respondent }} @else Respondent @endif @elseif($get_case_details->reg_power == 3 ) Caveat @elseif($get_case_details->reg_power == 4 ) None @else ---- @endif
                        </th>
                      </tr>
                      <tr style="">
                        <th class=""> 
                          <div class="col-md-6" style="margin-left: -10px;"> 
                            <b> FIR No :- </b> @if( $get_case_details->reg_fir_id != "") {{ $get_case_details->reg_fir_id }} @else ---- @endif <br> 
                            <b> FIR Year :- </b> @if( $get_case_details->reg_fir_year != "") {{ $get_case_details->reg_fir_year }} @else ---- @endif </div>
                          <div class="col-md-6" style="border-left: 4px solid #eeeeee !important;"> <b> Police Station :- </b> @if( $get_case_details->reg_police_thana != "") {{ $get_case_details->reg_police_thana }} @else ---- @endif </div> 
                        </th>
                      </tr>
                    </thead>
                  </table>
                </div>

                <br>

                <div class="panel-heading" style="background: gray; color: white; font-size: 17px;">
                  <div class="panel-title hidden-xs text-center"> Case Status </div>
                </div>

                <div class="panel-body pn">
                  <table class="table table-striped table-hover" id="" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th class=""> <b> Status :- </b> @if($get_case_details->pessi_choose_type == 0) Pending @elseif($get_case_details->pessi_choose_type == 1) Disposal ( {{ date('d F Y',strtotime($get_case_details->created_at)) }} ) @else Due Course @endif </th>
                      </tr>
                      <tr style="">
                        <th class=""> <b> Previous Date :- </b>
                          @if($get_case_details->pessi_prev_date == "1970-01-01" || $get_case_details->pessi_prev_date == "")
                            -----
                          @else
                            {{ date('d/m/Y',strtotime($get_case_details->pessi_prev_date)) }}
                          @endif
                        </th>
                      </tr>
                      <tr>
                        <th class=""> <b> Next Date :- </b>
                          @if($get_case_details->reg_nxt_further_date == "1970-01-01")
                            -----
                          @else
                            <blink> {{ date('d/m/Y',strtotime($get_case_details->reg_nxt_further_date)) }} </blink>
                          @endif
                        </th>
                      
                      </tr>
                      <tr style="">
                        <th class=""> <b> Stage :- </b> @if( $get_case_details->stage_name != "") <blink> {{ $get_case_details->stage_name }} </blink> @else ---- @endif </th>
                      </tr>
                      <tr>
                        <th class=""> <b> Extra Party :- </b> 
                          <button type="button" class="btn btn-primary view" style="padding: 0px 10px;"  data-toggle="modal" data-target="#reg_petitioner{{$get_case_details->reg_id}}" > 
                            <a href="#" style="text-transform: capitalize; text-decoration:none; color: white;"> Pet. Details </a> 
                          </button> 

                          <button type="button" class="btn btn-primary view" style="padding: 0px 10px;" data-toggle="modal" data-target="#reg_respondent{{$get_case_details->reg_id}}" >
                            <a href="#" style="text-transform: capitalize; text-decoration:none; color: white;"> Resp. Details </a> 
                          </button> 
                        </th>
                      </tr>
                    </thead>
                  </table>
                </div>
                            
                <div id="reg_petitioner{{$get_case_details->reg_id}}" class="modal fade in" role="dialog">
                  <div class="modal-dialog" style="margin-top:100px;">
                    <div class="modal-content">
                      <div class="modal-header" style="padding-bottom: 35px;">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title pull-left">
                            Petitioner Extra Party
                          </h4>
                      </div>
                      <div class="">
                        <table class="table admin-form theme-warning tc-checkbox-1  fs13" id="datatable">
                          <thead>
                            <tr>
                              <th class="text-left">Name</th>
                              <th class="text-left">Father Name</th>
                              <th class="text-left">Address</th>
                            </tr>
                        </thead>
                        <tbody>
                         @php
                          $reg_petitioner_extra = json_decode($get_case_details->reg_petitioner_extra,true);
                          @endphp
                          @foreach($reg_petitioner_extra as $respondent_extras)
                          <tr>
                            <td class="text-left" style="padding-left:20px"> {{ $respondent_extras[petitioner_name] }}</td>
                            <td class="text-left" style="padding-left:20px"> {{ $respondent_extras[petitioner_father_name] }}</td>
                            <td class="text-left" style="padding-left:20px"> {{ $respondent_extras[petition_address] }}</td>
                          </tr>
                            @endforeach
                          </tbody>
                      </table>
                      </div>
                      <div class="clearfix"></div>
                    </div> 
                  </div>
                </div>
                         
                <div id="reg_respondent{{$get_case_details->reg_id}}" class="modal fade in" role="dialog">
                  <div class="modal-dialog" style="margin-top:100px;">
                    <div class="modal-content">
                      <div class="modal-header" style="padding-bottom: 35px;">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title pull-left">
                            Respondent Extra Party
                          </h4>
                      </div>
                      <div class="">
                        <table class="table admin-form theme-warning tc-checkbox-1  fs13" id="datatable_1">
                          <thead>
                            <tr>
                              <th class="text-left">Name</th>
                              <th class="text-left">Father Name</th>
                              <th class="text-left">Address</th>
                            </tr>
                        </thead>
                        <tbody>
                         @php
                          $respondent_extra = json_decode($get_case_details->reg_respondent_extra,true);
                          @endphp
                          @foreach($respondent_extra as $respondent_extras)
                          <tr class="json_view">
                          <td class="text-left" style="padding-left:20px">{{ $respondent_extras[respondent_name] }}</td>
                          <td class="text-left" style="padding-left:20px">{{ $respondent_extras[respondent_father_name] }}</td>
                          <td class="text-left" style="padding-left:20px">{{ $respondent_extras[respondent_address] }}</td>
                        </tr>
                            @endforeach
                          </tbody>
                      </table>
                      </div>
                      <div class="clearfix"></div>
                    </div> 
                  </div>
                </div>
                        


                <br>

                <div class="panel-heading" style="background: gray; color: white; font-size: 17px;">
                  <div class="panel-title hidden-xs text-center"> Client Details </div>
                </div>

                <div class="panel-body pn">
                  <table class="table table-striped table-hover" id="" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th class=""> <b> Referred By :- </b> @if( $get_case_details->ref_advocate_name != "") {{ $get_case_details->ref_advocate_name }} @else ---- @endif </th>
                      </tr>
                      <tr style="">
                        <th class=""> <div class="col-md-2" style="margin-left: -10px;"> <b> Client Name :- </b> </div> 
                          @if( $get_case_details->reg_client_group_id != "") 

                            @if($get_case_details->cl_group_type == "1") 
                              
                              <div class="col-md-10" style="margin-left: -40px;">
                                {{ $get_case_details->cl_group_name }} <br> {{ $get_case_details->cl_group_address }} {{ $get_case_details->cl_group_place }} <br> {{ $get_case_details->cl_group_mobile_no }} 
                              </div>

                            @else 
                              
                              <div class="col-md-10" style="margin-left: -40px;">
                                {{ $get_case_details->cl_group_name }} <br> {{ $get_case_details->sub_client_name }} <br> {{ $get_case_details->sub_client_address }} {{ $get_case_details->sub_client_place }} <br> {{ $get_case_details->sub_client_mobile_no }} 
                              </div>

                            @endif

                          @else ---- @endif </th>
                      </tr>
                    </thead>
                  </table>
                </div>

                <br>

                <div class="panel-heading" style="background: gray; color: white; font-size: 17px;">
                  <div class="panel-title hidden-xs text-center"> Other Details </div>
                </div>

                <div class="panel-body pn">
                  <table class="table table-striped table-hover" id="" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th class=""> <b> Assigned :- </b> @if( $get_case_details->assign_advocate_name != "") {{ $get_case_details->assign_advocate_name }} @else ---- @endif</th>
                      </tr>
                      <tr style="background: #eee;">
                        <th class=""> <b> Act :- </b> @if( $get_case_details->act_name != "") {{ $get_case_details->act_name }} @else ---- @endif</th>
                      </tr>
                      <tr>
                        <th class=""> <b> Section :- </b> @if( $get_case_details->section_name != "") {{ $get_case_details->section_name }} @else ---- @endif </th>
                      </tr>
                      <tr style="background: #eee;">
                        <th class=""> <b> Opposite Counsel :- </b> @if( $get_case_details->reg_opp_council != "") {{ $get_case_details->reg_opp_council }} @else ---- @endif </th>
                      </tr>
                      <tr>
                        <th class=""> <b> Remark Field :- </b> @if( $get_case_details->reg_remark != "") {{ $get_case_details->reg_remark }} @else ---- @endif </th>
                      </tr>
                    </thead>
                  </table>
                </div>

                <br>

                <div class="panel-heading" style="background: gray; color: white; font-size: 17px;">
                  <div class="panel-title hidden-xs text-center"> History of Case </div>
                </div>

                <div class="panel-body pn">
                  <table class="table table-striped table-hover" id="" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th style="width:100px !important;" class="text-left">
                          <label class="option block mn">
                            Delete
                          </label>
                        </th>
                        <th class=""> <b> Peshi Date </b></th>
                        <th class=""> <b> Judge Name & Court No</b></th>
                        <th class=""> <b> Purpose / Stage </b></th>
                        <th class=""> <b> Order Sheet </b></th>
                      </tr>
                    </thead>

                    <tbody>
                      @foreach($pessi_detail as $pessi_details)
                        
                        <tr>
                          <!-- <td class=""> 
                            <a href="{{ url('advocate-panel/view-peshi')}}" style="text-decoration: none;">
                              @if($pessi_details->pessi_choose_type == 0)
                                Date:-  {{ date('d F Y',strtotime($pessi_details->pessi_further_date)) }}
                              @elseif($pessi_details->pessi_choose_type == 1)
                                Decide :- @if($pessi_details->pessi_decide_id == 1) Case in fever @elseif($pessi_details->pessi_decide_id == 2) Case in against @elseif($pessi_details->pessi_decide_id == 3) Withdraw @elseif($pessi_details->pessi_decide_id == 4) None @endif
                              @else
                                Due Course
                              @endif
                            </a> -->

                          <td class="text-left" style="padding-left: 20px;">
                            <label class="option block mn">
                              <a href="{{ url('advocate-panel/delete-peshi') }}/{{ sha1($pessi_details->pessi_id) }}">
                                <span class="fa fa-trash" style="cursor: pointer;"></span>
                              </a>
                            </label>
                          </td>

                          <td>
                            <a href="#" style="text-transform: capitalize; text-decoration:none;" data-toggle="modal" data-target="#pessi_details{{$pessi_details->pessi_id}}" > 

                              @if($pessi_details->pessi_choose_type == 0)

                                @if($pessi_details->pessi_further_date == "" || $pessi_details->pessi_further_date ==  "1970-01-01")
                                  Date:-  -----
                                @else

                                  Date:-  {{ date('d F Y',strtotime($pessi_details->pessi_further_date)) }}
                                
                                @endif

                                
                              @elseif($pessi_details->pessi_choose_type == 1)
                                Decide :- @if($pessi_details->pessi_decide_id == 1) Case in fever @elseif($pessi_details->pessi_decide_id == 2) Case in against @elseif($pessi_details->pessi_decide_id == 3) Withdraw @elseif($pessi_details->pessi_decide_id == 4) None @endif
                              @else
                                Due Course
                              @endif

                            </a>

                            <div id="pessi_details{{$pessi_details->pessi_id}}" class="modal fade in" role="dialog">
                              <div class="modal-dialog" style="width:700px; margin-top:100px;">
                                <div class="modal-content">
                                  <div class="modal-header" style="padding-bottom: 35px;">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title pull-left"> Peshi Entry </h4>
                                  </div>
                                  <section id="content" class="table-layout animated fadeIn">
                                    <div>
                                      <div class="center-block">
                                        <div class="panel panel-primary panel-border top mb35">
                                          <div class="panel-body bg-light dark">
                                            <div class="tab-content pn br-n admin-form">
                                              <div id="tab1_1" class="tab-pane active">
                                                
                                                {!! Form::open(['name'=>'form_add_pessi'.$pessi_details->pessi_id,'url'=>'advocate-panel/edit-peshi/'.$pessi_details->pessi_id,'id'=>'form_add_pessi']) !!}

                                                <div class="row">                                                
                                                    
                                                    <div class="col-md-12">
                                                      <div class="section">
                                                        <label for="level_name" class="field-label" style="font-weight:600;"> Select Stage : </label>
                                                          <label for="artist_state" class="field select">
                                                            <select class="form-control" name="reg_stage" id="reg_stage">
                                                              <!-- <option value=''>Select Stage</option> -->
                                                                @foreach($get_stage as $stages) 
                                                                  <option value="{{ $stages->stage_id }}" {{ $stages->stage_id == $pessi_details->pessi_statge_id ? 'selected="selected"' : '' }} >{{$stages->stage_name}} </option>
                                                                @endforeach                                
                                                            </select>
                                                            <i class="arrow double"></i>
                                                          </label>
                                                      </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                      <div class="section">
                                                        <label for="level_name" class="field-label" style="font-weight:600;"> Order Sheet :  </label>  
                                                        <label for="level_name" class="field prepend-icon">
                                                          {!! Form::textarea('order_sheet',$pessi_details->pessi_order_sheet,array('class' => 'gui-textarea accc1','placeholder' => '','id'=>'' )) !!}
                                                            <label for="Account Mobile" class="field-icon">
                                                            <i class="fa fa-comment"></i>
                                                          </label                           
                                                        </label>
                                                      </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                      <div class="section" style="margin-bottom: 40px;">
                                                        <div class="col-md-3">
                                                          <label class="option" style="font-size:12px;">
                                                            {!! Form::radio('choose_type','0',$pessi_details['pessi_choose_type'] == 0 ? 'checked' : '', array('onclick' => "show_further('$pessi_details->pessi_id')", 'class' => 'check' )) !!}
                                                            <span class="radio" style="border: 2px solid #4a89dc !important;"></span> Next Date
                                                          </label>
                                                        </div>

                                                        <div class="col-md-3">
                                                          <label class="option" style="font-size:12px;">
                                                            {!! Form::radio('choose_type','1',$pessi_details['pessi_choose_type'] == 1 ? 'checked' : '', array('onclick' => "show_decide('$pessi_details->pessi_id')", 'class' => 'check' )) !!}
                                                            <span class="radio" style="border: 2px solid #4a89dc !important;"></span> Decide
                                                          </label>
                                                        </div>

                                                        <div class="col-md-3">
                                                          <label class="option" style="font-size:12px;">
                                                            {!! Form::radio('choose_type','2',$pessi_details['pessi_choose_type'] == 2 ? 'checked' : '', array('onclick' => "show_due_course('$pessi_details->pessi_id')", 'class' => 'check' )) !!}
                                                            <span class="radio" style="border: 2px solid #4a89dc !important;"></span> Due Course
                                                          </label>
                                                        </div>

                                                      </div>

                                                      <div class="section"></div>
                                                      <div class="clearfix"></div>
                                                    </div>

                                                    <div class="col-md-6" @if($pessi_details['pessi_choose_type'] == 1) style="display:none;" @endif  id="show_further_date{{$pessi_details['pessi_id']}}" >
                                                      <div class="section section_from">
                                                        <label for="level_name" class="field-label" style="font-weight:600;" >Next Date : (dd/mm/yyyy)  </label>  
                                                        <label for="level_name" class="field prepend-icon" id="discount_date_from_lb">

                                                          @if($pessi_details->pessi_further_date == "" || $pessi_details->pessi_further_date ==  "1970-01-01")
                                                            
                                                            {!! Form::date('further_date','',array('class' => 'gui-input bfh-phone','data-format' =>'dd-dd-dddd','placeholder' => '','id'=>''  )) !!}

                                                          @else

                                                            {!! Form::date('further_date',$pessi_details->pessi_further_date,array('class' => 'gui-input bfh-phone','data-format' =>'dd-dd-dddd','placeholder' => '','id'=>''  )) !!}

                                                          @endif

                                                          
                                                            <label for="Account Mobile" class="field-icon">
                                                            <i class="fa fa-calendar"></i>
                                                          </label>                           
                                                        </label>
                                                        <!-- <label for="level_name" class="field-label"> <b>Note :-</b> dd/mm/yyyy </label> -->
                                                      </div>
                                                    </div>

                                                    <div class="col-md-6" @if($pessi_details['pessi_choose_type'] == 1) @else style="display: none;" @endif id="show_decide{{$pessi_details['pessi_id']}}">
                                                      <div class="section">
                                                        <label for="level_name" class="field-label" style="font-weight:600;"> Decide : </label>
                                                          <label for="artist_state" class="field select">
                                                            <select class="form-control" id="decide" name="decide" >
                                                              <option value=''>Select Decide </option>
                                                              <option value='1' {{ $pessi_details->pessi_decide_id == 1 ? 'selected="selected"' : '' }} >Case in fever </option>      
                                                              <option value='2' {{ $pessi_details->pessi_decide_id == 2 ? 'selected="selected"' : '' }} >Case in against </option>
                                                              <option value='3' {{ $pessi_details->pessi_decide_id == 3 ? 'selected="selected"' : '' }} > Withdraw </option>
                                                              <option value='4' {{ $pessi_details->pessi_decide_id == 4 ? 'selected="selected"' : '' }} > None </option>
                                                            </select>
                                                            <i class="arrow double"></i>
                                                          </label>
                                                      </div>
                                                    </div>

                                                    <div class="col-md-2" style="display: none;" id="show_due_course{{$pessi_details['pessi_id']}}">
                                                      <button type="button" class="btn btn-success br2 btn-xs field select form-control" style="margin-top: 23px; margin-bottom: 22px;" > Due Course </button>
                                                    </div>

                            <div class="col-md-12">
                              <div class="field" align="left">
                                <label for="documents" class="field-label" style="font-weight:600;" > <span style="float: left;margin-bottom: 10px;"> Send sms </span> </label>
                              </div>
                            </div>

                            <div class="col-md-12">
                              <div class="section" style="margin-bottom: 40px;">  
                                <div class="col-md-3">
                                  <label class="option block mn" style="font-size:12px;"> 
                                    {!! Form::checkbox('sms_type[]','1','', array( 'class' => 'check' , 'class' => 'check' )) !!}
                                    <span class="checkbox mn" style="border: 2px solid #4a89dc !important;"></span> Client
                                  </label>
                                </div>

                                <div class="col-md-3">
                                  <label class="option block mn" style="font-size:12px;">
                                    {!! Form::checkbox('sms_type[]','2', '', array( 'class' => 'check'  )) !!}
                                    <span class="checkbox mn" style="border: 2px solid #4a89dc !important;"></span> Reffered
                                  </label>
                                </div>

                                <div class="col-md-3">
                                  <label class="option block mn" style="font-size:12px;">
                                    {!! Form::checkbox('sms_type[]','3','', array( 'class' => 'check', 'id' => 'sms_type' ,'onclick'=>'reg_sms_type()'  )) !!}
                                    <span class="checkbox mn" style="border: 2px solid #4a89dc !important;"></span> Other
                                  </label>
                                </div>
                              </div>
                            </div>

                            <div class="col-md-4" style="display: none;" id="show_other_mobile">
                              <div class="section">
                                <label for="level_name" class="field-label" style="font-weight:600;"> Other Mobile No. :  </label>  
                                <label for="level_name" class="field prepend-icon">
                                  {!! Form::text('reg_other_mobile','',array('class' => 'gui-input','placeholder' => '' )) !!}
                                    <label for="Account Mobile" class="field-icon">
                                    <i class="fa fa-pencil"></i>
                                  </label>                           
                                </label>
                              </div>
                            </div>

                            <div class="col-md-8">
                              <div class="section">
                                <label for="level_name" class="field-label" style="font-weight:600;"> Text :  </label>
                                <label for="level_name" class="field prepend-icon">
                                  {!! Form::text('reg_sms_text', '' ,array('class' => 'gui-input','placeholder' => '' )) !!}
                                    <label for="Account Mobile" class="field-icon">
                                    <i class="fa fa-pencil"></i>
                                  </label>                           
                                </label>
                              </div>
                            </div>
                                                  
                                                </div>

                                              <div class="panel-footer text-right">
                                                  {!! Form::button('Save', array('class' => 'button btn-primary mysave', 'onclick' => 'update_pessi_details("'.$pessi_details->pessi_id.'")', 'id' => 'maskedKey')) !!}
                                                  <!-- {!! Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!} -->
                                              </div>   
                                                {!! Form::close() !!}
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </section>
                                  <div class="clearfix"></div>
                                </div> 
                              </div>
                            </div>

                          </td>

                          <script type="text/javascript">
                            function update_pessi_details(pessi_id) {

                              if(confirm("Are you sure to want update records?")) {
                                $("[name=form_add_pessi"+pessi_id+"]").submit();    
                              }
                            }
                          </script>

                          @php
                            $justise_name =  \App\Model\Judge\Judge::where(['judge_id' => $pessi_details->honarable_justice_db ])->first();
                          @endphp

                          <td class=""> 
                            @if($pessi_details->reg_court == 2)
                              @if($pessi_details->judge_name == "" && $pessi_details->court_number == "")
                                -----
                              @else
                                @if($pessi_details->judge_name != "")  {{$pessi_details->judge_name}}  @endif

                                @if($justise_name->judge_name != "" && $pessi_details->judge_name != "") and  @endif
                                @if($justise_name->judge_name != "")  {{$justise_name->judge_name}}  @endif

                                @if( ($pessi_details->judge_name != "" || $justise_name->judge_name != "") && $pessi_details->court_number != "")  &  @endif
                                @if($pessi_details->court_number != "")  {{$pessi_details->court_number}}  @endif
                              @endif
                            @elseif($pessi_details->reg_court == 1)
                                   -------
                            @endif
                          </td>
                          <td class=""> @if($pessi_details->stage_name == "") ----- @else {{ $pessi_details->stage_name }} @endif </td>
                          <td class=""> @if($pessi_details->pessi_order_sheet == "") ----- @else {{ $pessi_details->pessi_order_sheet }} @endif</td>
                        </tr>

                      @endforeach
                    </tbody>
                  </table>
                </div>

                <br>

                <div class="panel-heading" style="background: gray; color: white; font-size: 17px;">
                  <div class="panel-title hidden-xs text-center"> Compliance </div>
                </div>

                <div class="panel-body pn">
                  <table class="table table-striped table-hover" id="" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th class=""> <b> Created </b></th>
                        <th class=""> <b> View Compliance </b></th>
                      </tr>
                    </thead>

                    <tbody>

                      @if(count($compliance_detail) != 0)
                        @foreach($compliance_detail as $compliance_details)
                          <tr>
                            <td class=""> @if($compliance_details->compliance_created != "" || $compliance_details->compliance_created != "1970-01-01") {{ date('d M Y',strtotime($compliance_details->compliance_created)) }} @else ----- @endif </td>
              
                            <td class="text-left">
                              <button type="button" class="btn btn-primary view btn-xs" data-toggle="modal" data-target="#view_compliance{{$compliance_details->compliance_id}}">
                                <a href="#" style="text-transform: capitalize; text-decoration:none; color: white;"> View Compliance</a>
                              </button>

                              <div id="view_compliance{{$compliance_details->compliance_id}}" class="modal fade in" role="dialog">
                                <div class="modal-dialog" style="width:500px; margin-top:100px;">
                                  <div class="modal-content">
                                    <div class="modal-header" style="padding-bottom: 35px;">
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title pull-left" style="color: #626262;">
                                           View Compliance
                                        </h4>
                                    </div>

                                    @if($compliance_details->compliance_type_notice == 1) 
                                      <div class="modal-header" style="padding-bottom: 35px;">
                                          <h4 class="modal-title pull-left" style="color: #626262;">  Notice Details  </h4>
                                      </div>

                                      <div class="modal-body">
                                        <table class="table table-bordered mbn">
                                          <thead>
                                            <tr class="bg-light">
                                              <th class="text-left" style="padding-left: 10px !important; font-weight: 600; color: #626262;">Issue Date</th>
                                              <th class="text-left" style="padding-left: 10px !important; font-weight: 600; color: #626262;">Status</th>
                                            </tr>
                                          </thead>
                                          <tbody>            
                                            <tr class="">
                                              <td class="text-left" style="font-weight: 400; color: #626262;" > @if($compliance_details->notice_issue_date != "1970-01-01") {{ date('d M Y',strtotime($compliance_details->notice_issue_date)) }} @else ----- @endif </td>
                                              <td class="text-left" style="font-weight: 400; color: #626262;"> @if($compliance_details->notice_status == 1) Pending @elseif($compliance_details->notice_status == 2)  Complete ( @if($compliance_details->notice_status_date != "" || $compliance_details->notice_status_date != "1970-01-01") {{ date('d M Y',strtotime($compliance_details->notice_status_date)) }} @endif ) @else ----- @endif </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </div>
                                    @endif
                                    <div class="clearfix"></div>

                                    @if($compliance_details->compliance_type_date == 1) 
                                      <div class="divider" style="margin: 0px;"></div>
                                      <div class="modal-header" style="padding-bottom: 35px;">
                                          <h4 class="modal-title pull-left" style="color: #626262;" >  Date  </h4>
                                      </div>

                                      <div class="modal-body">
                                        <table class="table table-bordered mbn">
                                          <thead>
                                            <tr class="bg-light">
                                              <th class="text-left" style="padding-left: 10px !important; font-weight: 600; color: #626262;">Date Type</th>
                                              <th class="text-left" style="padding-left: 10px !important; font-weight: 600; color: #626262;">Date</th>
                                              <th class="text-left" style="padding-left: 10px !important; font-weight: 600; color: #626262;">Status</th>
                                            </tr>
                                          </thead>
                                          <tbody>            
                                            <tr class="">
                                              <td class="text-left" style="font-weight: 400; color: #626262;"> @if($compliance_details->compliance_date_type == 1) Short Date @else Long Date @endif</td>
                                              <td class="text-left" style="font-weight: 400; color: #626262;"> @if($compliance_details->compliance_date != "1970-01-01") {{ date('d M Y',strtotime($compliance_details->compliance_date)) }} @else ----- @endif</td>
                                              <td class="text-left" style="font-weight: 400; color: #626262;">@if($compliance_details->compliance_status == 1) Pending @elseif($compliance_details->compliance_status == 2) Complete ( {{ date('d M Y',strtotime($compliance_details->compliance_date_status)) }} ) @else ----- @endif </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </div>
                                    @endif

                                    @if($compliance_details->compliance_type_defect_case == 1) 
                                      <div class="divider" style="margin: 0px;"></div>
                                      <div class="modal-header" style="padding-bottom: 35px;">
                                          <h4 class="modal-title pull-left" style="color: #626262;">  Defect Case  </h4>
                                      </div>

                                      <div class="modal-body">
                                        <table class="table table-bordered mbn">
                                          <thead>
                                            <tr class="bg-light">
                                              <!-- <th class="text-left" style="padding-left: 10px !important;">Act Reason</th> -->
                                              <th class="text-left" style="padding-left: 10px !important; font-weight: 600; color: #626262;">Status</th>
                                            </tr>
                                          </thead>
                                          <tbody>            
                                            <tr class="">
                                              <!-- <td class="text-left"> {{ $compliance_details->compliance_act_reason }} </td> -->
                                              <td class="text-left" style="font-weight: 400; color: #626262;">@if($compliance_details->compliance_defect_status == 1) Pending @elseif($compliance_details->compliance_defect_status == 2) Complete ( {{ date('d M Y',strtotime($compliance_details->compliance_defect_date_status)) }} ) @else ----- @endif  </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </div>
                                    @endif

                                    @if($compliance_details->compliance_type_reply == 1) 
                                      <div class="divider" style="margin: 0px;"></div>
                                      <div class="modal-header" style="padding-bottom: 35px;">
                                          <h4 class="modal-title pull-left" style="color: #626262;">  Reply  </h4>
                                      </div>

                                      <div class="modal-body">
                                        <table class="table table-bordered mbn">
                                          <thead>
                                            <tr class="bg-light">
                                              <th class="text-left" style="padding-left: 10px !important;font-weight: 600; color: #626262;">Reply</th>
                                              <th class="text-left" style="padding-left: 10px !important;font-weight: 600; color: #626262;">Status</th>
                                            </tr>
                                          </thead>
                                          <tbody>            
                                            <tr class="">
                                              <td class="text-left" style="font-weight: 400; color: #626262;" > @if($compliance_details->compliance_reply != "") {{ $compliance_details->compliance_reply }} @else ----- @endif </td>
                                              <td class="text-left" style="font-weight: 400; color: #626262;" > @if($compliance_details->compliance_reply_status == 1) Pending @elseif($compliance_details->compliance_reply_status == 2) Complete ( {{ date('d M Y',strtotime($compliance_details->compliance_reply_date_status)) }} ) @else ----- @endif </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </div>
                                    @endif

                                    @if($compliance_details->compliance_type_certified == 1) 
                                      <div class="divider" style="margin: 0px;"></div>
                                      <div class="modal-header" style="padding-bottom: 35px;">
                                          <h4 class="modal-title pull-left" style="color: #626262;">  Certified Copy Date  </h4>
                                      </div>

                                      <div class="modal-body">
                                        <table class="table table-bordered mbn">
                                          <thead>
                                            <tr class="bg-light">
                                              <th class="text-left" style="padding-left: 10px !important;font-weight: 600; color: #626262; ">Certified Copy Date</th>
                                              <th class="text-left" style="padding-left: 10px !important;font-weight: 600; color: #626262; ">Status</th>
                                            </tr>
                                          </thead>
                                          <tbody>            
                                            <tr class="">
                                              <td class="text-left" style="font-weight: 400; color: #626262;">@if($compliance_details->certified_copy_date != "1970-01-01") {{ date('d M Y',strtotime($compliance_details->certified_copy_date)) }} @else ----- @endif</td>
                                              <td class="text-left" style="font-weight: 400; color: #626262;"> @if($compliance_details->certified_copy_status == 1) Pending @elseif($compliance_details->certified_copy_status == 2) Complete ( {{ date('d M Y',strtotime($compliance_details->certified_copy_date_status)) }} ) @else ----- @endif </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </div>
                                    @endif

                                    @if($compliance_details->compliance_type_other == 1) 
                                      <div class="divider" style="margin: 0px;"></div>
                                      <div class="modal-header" style="padding-bottom: 35px;">
                                          <h4 class="modal-title pull-left" style="color: #626262;">  Other  </h4>
                                      </div>

                                      <div class="modal-body">
                                        <table class="table table-bordered mbn">
                                          <thead>
                                            <tr class="bg-light">
                                              <th class="text-left" style="padding-left: 10px !important; font-weight: 600; color: #626262;">Other</th>
                                              <th class="text-left" style="padding-left: 10px !important; font-weight: 600; color: #626262;">Status</th>
                                            </tr>
                                          </thead>
                                          <tbody>            
                                            <tr class="">
                                              <td class="text-left" style="font-weight: 400; color: #626262;"> @if($compliance_details->compliance_other != "") {{ $compliance_details->compliance_other }} @else ----- @endif </td>
                                              <td class="text-left" style="font-weight: 400; color: #626262;"> @if($compliance_details->compliance_other_status == 1) Pending @elseif($compliance_details->compliance_other_status == 2) Complete ( {{ date('d M Y',strtotime($compliance_details->compliance_other_date_status)) }} ) @else ----- @endif </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </div>
                                    @endif

                                  </div> 
                                </div> 
                              </div>
                            </td>
                          </tr>    
                        @endforeach
                      @else 
                        <tr >
                          <td colspan="5" text-center>No Record Found !!</td>
                        </tr>
                      @endif

                    </tbody>
                  </table>
                </div>

                <br>

                <div class="panel-heading" style="background: gray; color: white; font-size: 17px;">
                  <div class="panel-title hidden-xs text-center"> Documents </div>
                </div>

                <div class="panel-body pn">

                  @php
                    $images_cat =  \App\Model\Upload_Document\Upload_Document::where(['upload_case_id' => $get_case_details->reg_id ])->where('upload_images', '!=' ,'')->get();
                  @endphp

                  @if(count($images_cat) != 0)
                    @foreach($images_cat as $images_cats) 


                    @php
                      $doc_image = explode("/",$images_cats['upload_images']);
                      $last =  substr($doc_image[2],-4);
                    @endphp
                    
                      <div class="col-md-4 document_clear" style="margin-bottom: 22px; margin-top: 22px; position: relative;" id="cat_img{{$images_cats['upload_id']}}">
                        
                        @if($last == '.jpg' or $last == 'jpeg' or $last == '.png' or $last == '.gif')
                         
                         {!! Html::image($images_cats['upload_images'], '', array('class' => 'media-object mw150 customer_profile', 'width'=>'100%', 'height'=>'250')) !!}

                          <a target="_blank" href="{{url($images_cats['upload_images'])}}" class="btn btn-xs btn-primary" style="margin-left: 10px; margin-top: 10px;"> View </a>
                        @elseif($last == '.pdf')
                         <i class="fa fa-file-pdf-o"></i> 
                          <a target="_blank" href="{{url($images_cats['upload_images'])}}" class="btn btn-xs btn-primary" style="margin-left: 10px;"> View </a>
                        @elseif($last == 'docx' or $last == '.doc')
                         <i class="fa fa-file-word-o"></i>
                          <a target="_blank" href="{{url($images_cats['upload_images'])}}" class="btn btn-xs btn-primary" style="margin-left: 10px;"> View </a>
                        @elseif($last == '.xls' or $last == 'xlsx')
                         <i class="fa fa-file-excel-o"></i>
                          <a target="_blank" href="{{url($images_cats['upload_images'])}}" class="btn btn-xs btn-primary" style="margin-left: 10px;"> Download </a>
                        @else
                          <a target="_blank" href="{{url($images_cats['upload_images'])}}" class="btn btn-xs btn-primary" style="margin-left: 10px;"> View </a>
                        @endif

                        @if($images_cats['upload_caption'] != "" )
                          <div style="margin-top: 10px;"> Caption :- {{ $images_cats['upload_caption'] }} </div>
                        @endif

                        @if($images_cats['upload_date'] != "" )
                          <div style="margin-top: 10px;"> Date :- 
                            @if($images_cats['upload_date'] == "1970-01-01")
                              -----
                            @else
                              {{ date('d/m/Y',strtotime($images_cats['upload_date'])) }}
                            @endif 
                          </div>
                        @endif


                      </div>
                    @endforeach
                  @else 
                    <div style="margin: 20px auto;" class="text-center" > No Documents Available !! </div>
                  @endif
                  
                </div>
              </div>
            </div>
        </div>  
      </section>

  </section>

<style type="text/css">
  
  .document_clear:nth-child(4){
    clear: both;
  }
  
</style>

<script type="text/javascript">
  
  function show_further(pessi_id){
    document.getElementById("show_further_date"+pessi_id).style.display = "block";
    document.getElementById("show_decide"+pessi_id).style.display = "none";
    document.getElementById("show_due_course"+pessi_id).style.display = "none";
  }


  function show_decide(pessi_id){
    document.getElementById("show_further_date"+pessi_id).style.display = "none";
    document.getElementById("show_decide"+pessi_id).style.display = "block";
    document.getElementById("show_due_course"+pessi_id).style.display = "none";
  }


  function show_due_course(pessi_id){
    document.getElementById("show_further_date"+pessi_id).style.display = "none";
    document.getElementById("show_decide"+pessi_id).style.display = "none";
  //  document.getElementById("show_due_course").style.display = "block";
  }


  // function update_pessi_details() {
     
  //   if(confirm("Are you sure to want update records?")) {
  //     $("[name=form_add_pessi]").submit();    
  //   }
  // }

</script>


@endsection