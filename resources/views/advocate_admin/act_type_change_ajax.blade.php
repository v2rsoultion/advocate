
    <select id="framework_3" name="section[]" multiple class="form-control">
        @foreach($act_type_change as $act_type_changes) 
		 <option value="{{ $act_type_changes->section_id }}" {{ $act_type_changes->section_id == $get_record->reg_section_id ? 'selected="selected"' : '' }} >{{$act_type_changes->section_name}} </option>
		@endforeach
    </select>
    <i class="arrow double arrow_new"></i>


<script type="text/javascript">
	

	$('#framework_3').multiselect({
      nonSelectedText: 'Select Section',
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
      buttonWidth:'400px'
     });
	
</script>
	
