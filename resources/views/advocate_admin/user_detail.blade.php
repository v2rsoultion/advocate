@extends('trivia_admin/layout')
@section('content')

  <!-- Start: Content-Wrapper -->
  <section id="content_wrapper">
    <header id="topbar">
      <div class="topbar-left">
        <ol class="breadcrumb">
          <li class="crumb-active">
            <a href="{{ url('trivialeague-panel/view-user') }}">View User</a>
          </li>
          <li class="crumb-icon">
            <a href="{{ url('/trivialeague-panel/dashboard') }}">
              <span class="glyphicon glyphicon-home"></span>
            </a>
          </li>
          <li class="crumb-link">
            <a href="{{ url('/trivialeague-panel/dashboard') }}">Home</a>
          </li>
          <li class="crumb-trail">User Profile </li>
        </ol>
      </div>
    </header>

    <div class="" style="margin-top:10px;">
      <div class="col-md-12">
        <div class="panel panel-primary panel-border top">  
          <div class="panel-heading">
            <div class="panel-title hidden-xs">
              <span class="glyphicon glyphicon-tasks"></span>User Profile</div>
          </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>

      <section id="content" class="animated fadeIn">
        <div class="page-heading" style="background-color: #FFF; margin:-45px -9px 5px;">
            <div class="media clearfix">
                             <!-- Put Image file here if any available -->
              <div class="media-body va-m">
                <h2 class="media-heading text-capitalize"> {!! $get_record['user_first_name'] !!} {!! $get_record['user_last_name'] !!}
                  <small> - Profile</small>
                </h2>

                <div style="padding:5px;" class="col-md-12" >
                    <span class="fa fa-mobile text-primary fs15 mr15"></span> {!! $get_record['user_phone_no'] !!} 
                </div>
                <div style="padding:5px;" class="col-md-12" >
                    <span class="fa fa-calendar text-primary fs15 mr10"></span> {{date('d F Y',strtotime($get_record['user_created']))}}
                </div>
                <div style="padding:5px;">
                    <span class="glyphicons glyphicons-wallet text-primary fs15 mr15"></span> <i class="fa fa-inr" aria-hidden="true"></i> {!! $get_record['user_wallet'] !!} 
                </div>
                <div style="padding:5px;">
                    <span class="text-primary fs15 mr15">{{ Html::image('/public/admin/icon/coin-small.png', 'a picture',array('class'=>'coin-img')) }}</span>  {!! $coin !!} 
                </div>

              </div>
            </div>
        </div>

        <div class="row">
          <div class="">
            <div class="panel">
              <div class="panel-heading">
                <ul class="nav panel-tabs-border panel-tabs panel-tabs-left">
                  <li class="active">
                    <a href="#tab1" data-toggle="tab" aria-expanded="false">Attempted question</a>
                  </li>
                  <li class="">
                    <a href="#tab2" data-toggle="tab" aria-expanded="false">Gullyy Coins</a>
                  </li>
                  <li class="">
                    <a href="#tab3" data-toggle="tab" aria-expanded="false">Gullyy Cash</a>
                  </li>
                </ul>
              </div>
              <div class="panel-body">
                <div class="tab-content pn br-n">   
                  <div id="tab1" class="tab-pane active">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="panel panel-visible" id="spy2">
                          <div class="panel-heading">
                            <div class="panel-title hidden-xs">
                              <span class="glyphicon glyphicon-tasks"></span>Attempted Questions ( Last 10 questions )</div>
                          </div>
                          <div class="panel-body pn">
                            <table class="table table-striped table-hover" id="datatable" cellspacing="0" width="100%">
                              <thead>
                                <tr>
                                  <th></th>
                                  <th class="text-left" >Question</th>
                                  <th class="text-center" >User's answer</th>
                                  <th class="text-center" >Correct answer</th>
                                  <th class="text-center" >Result</th>
                                  <th class="text-center" >Reward Coins</th>
                                  <th class="text-center" >Date</th>

                                </tr>
                              </thead>
                              <tbody>

                               @foreach($attemted_question as $attemted_questions) 
                                  
                                <tr>
                                  <td></td>
                                  <td class="text-left" style="padding-left: 20px;"> {{$attemted_questions->quiz_question}} </td>
                                  <td class="text-center" > {{$attemted_questions->attempted_quiz_user_ans}} </td>
                                  <td class="text-center" > {{$attemted_questions->attempted_quiz_correct_ans}} </td>
                                  <td class="text-center" > 

                                  @php
                                    $tc = $attemted_questions->attempted_quiz_timer;
                                    $tc1 = explode($attemted_questions->attempted_quiz_tc1,$tc);
                                    $tc2 = explode($attemted_questions->attempted_quiz_tc2,$tc1[1]);

                                      $netim = base64_decode(base64_decode($tc2[0]));
                                      if(!is_numeric($netim)){
                                        $netim = '00';
                                      }
                                  @endphp

                                    
                                    @if($attemted_questions->attempted_quiz_result == 2 )
                                    <span style="color: Green;">Correct</span>
                                    @elseif($attemted_questions->attempted_quiz_result == 1 ) 
                                    <span style="color: red;">Wrong</span>
                                    @elseif(date('Y-m-d',strtotime($attemted_questions->attempted_quiz_date)) == date('Y-m-d') )
                                    
                                    <span style="color: blue;"> @if($netim == '00') Times Up  @else  Alloted @endif </span>
                                    
                                    @else
                                    <span style="color: blue;"> @if($netim == '00') Times Up  @else  Not Attempted @endif </span>
                                    @endif

                                  </td>
                                  <td class="text-center" >
                                       @if($attemted_questions->attempted_quiz_user_ans == $attemted_questions->attempted_quiz_correct_ans)
                                       {{$attemted_questions->attempted_quiz_received_coins}}
                                       @else
                                       0
                                       @endif
                                  </td>
                                  <td class="text-center" > {{date('d F Y',strtotime($attemted_questions->attempted_quiz_date))}} </td>
                                  
                                  
                                </tr>  
                               @endforeach 
                                
                              </tbody>
                            </table>
                          </div>

                          <div class="panel-body pn">
                            <div class="table-responsive">
                              <table class="table admin-form theme-warning tc-checkbox-1 fs13">                                
                                <tbody>
                                  <tr class="">
                                    <th class="text-right">
                                      <a href="{{ url('/trivialeague-panel/attempted-question') }}/{{$get_record['user_id']}}"> View All </a> 
                                    </th>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                          
                        </div>
                      </div>
                    </div>
                  </div>

                  <div id="tab2" class="tab-pane ">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="panel panel-visible" id="spy2">
                          <div class="panel-heading">
                            <div class="panel-title hidden-xs">
                              <span class="glyphicon glyphicon-tasks"></span>Gullyy Coins </div>
                          </div>
                          <div class="panel-body pn">
                            <table class="table table-striped table-hover" id="datatable" cellspacing="0" width="100%">
                              <thead>
                                <tr>
                                  <th></th>
                                  <th class="text-left" >Coins</th>
                                  <th class="text-left" style="padding-left: 20px;">Type</th>
                                  <th class="text-center" >Date</th>

                                </tr>
                              </thead>
                              <tbody>

                               @foreach($coins_detail as $coins_details) 
                                  
                                <tr>
                                  <td></td>
                                  <td class="text-left" style="padding-left: 20px;"> {{$coins_details->coin_coins}} </td>
                                  <td class="text-left" style="padding-left: 20px;">
                                  @if($coins_details->coin_flag == 1)
                                  Quiz
                                  @else
                                  Referral Code
                                  @endif
                                  </td>
                                  
                                  <td class="text-center" > {{date('d F Y',strtotime($coins_details->coin_date))}} </td>
                                  
                                  
                                </tr>  
                               @endforeach 
                                
                              </tbody>
                            </table>
                          </div>

                          <div class="panel-body pn">
                            <div class="table-responsive">
                              <table class="table admin-form theme-warning tc-checkbox-1 fs13">                                
                                <tbody>
                                  <tr class="">
                                    <!-- <th class="text-right">
                                      <a href="{{ url('/trivialeague-panel/attempted-question') }}/{{$get_record['user_id']}}"> View All </a> 
                                    </th> -->
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                          
                        </div>
                      </div>
                    </div>
                  </div>

                  <div id="tab3" class="tab-pane ">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="panel panel-visible" id="spy2">
                          <div class="panel-heading">
                            <div class="panel-title hidden-xs">
                              <span class="glyphicon glyphicon-tasks"></span>Gullyy Cash </div>
                          </div>
                          <div class="panel-body pn">
                            <table class="table table-striped table-hover" id="datatable" cellspacing="0" width="100%">
                              <thead>
                                <tr>
                                  <th></th>
                                  <th class="text-left" >Amount</th>
                                  <th class="text-left" style="padding-left: 20px;">Type</th>
                                  <th class="text-center" >Date</th>

                                </tr>
                              </thead>
                              <tbody>

                               @foreach($trans_detail as $trans_details) 
                                  
                                <tr>
                                  <td></td>
                                  <td class="text-left" style="padding-left: 20px;"> {{$trans_details->trans_amount}} </td>
                                  <td class="text-left" style="padding-left: 20px;">
                                  @if($trans_details->trans_type == 1)
                                  Debit
                                  @else
                                  Credit
                                  @endif
                                  </td>
                                  
                                  <td class="text-center" > {{date('d F Y',strtotime($trans_details->trans_date))}} </td>
                                  
                                </tr>  
                               @endforeach 
                                
                              </tbody>
                            </table>
                          </div>

                          <div class="panel-body pn">
                            <div class="table-responsive">
                              <table class="table admin-form theme-warning tc-checkbox-1 fs13">                                
                                <tbody>
                                  <tr class="">
                                    <!-- <th class="text-right">
                                      <a href="{{ url('/trivialeague-panel/attempted-question') }}/{{$get_record['user_id']}}"> View All </a> 
                                    </th> -->
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                          
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>  
      </section>

  </section>

@endsection