@extends('advocate_admin/layout')
@section('content')

  <!-- Start: Content-Wrapper -->
  <section id="content_wrapper">
    <header id="topbar" style="margin-top: 62px;">
      <div class="topbar-left">
        <ol class="breadcrumb">
          <li class="crumb-active">
            <a href="{{ url('advocate-panel/filter-reporting') }}"> Reporting</a>
          </li>
          <li class="crumb-icon">
            <a href="{{ url('advocate-panel/dashboard') }}">
              <span class="glyphicon glyphicon-home"></span>
            </a>
          </li>
          <li class="crumb-link">
            <a href="{{ url('advocate-panel/dashboard') }}">Home</a>
          </li>
          <li class="crumb-trail"> Reporting </li>
        </ol>
      </div>
    </header>

    <div class="" style="margin-top:10px;">
      <div class="col-md-12 mb30">
        <div class="panel panel-primary panel-border top mb35">  
          <div class="panel-heading">
            <div class="panel-title hidden-xs">
              <span class="glyphicon glyphicon-tasks"></span> Reporting </div>
          </div>

          {!! Form::open(['url'=>'/advocate-panel/download-filter-reporting','id'=>'form_add_question' ,'autocomplete'=>'off']) !!}
          <div class="panel-menu admin-form theme-primary" style="padding: 5px 20px;">
            <div class="row">

                <div class="col-md-6">
                  <div class="section" >
                    <label for="user_name" class="field-label" style="font-weight:600;" > Select Filter Option </label>
                    <label for="artist_state" class="field select">
                      <select class="select2-single form-control "  name="filter_option[]" id="" multiple>
                        <option value="court_type as Court Type"> Court Type </option>
                        <option value="court_name as Court Name"> Court Name </option>
                        <option value="reg_date as Registration Date"> Date </option>
                        <option value="reg_file_no as File Number"> File Number </option>
                        <option value="case_name as Case Type"> Case Type </option>
                        <option value="reg_case_number as Case Number"> Case Number </option>
                        <option value="classcode_name as Class Code"> Class Code </option>
                        <option value="reg_vcn_number as NCV Number"> NCV Number </option>
                        <option value="reg_petitioner as Petitioner Name"> Petitioner Name </option>
                        <option value="reg_respondent as Respondent Name"> Respondent Name </option>
                        <option value="cl_group_name as Client Group/Clients"> Client Group/Clients </option>
                        <option value="sub_name_prefix as Prefix"> Prefix </option>
                        <option value="sub_guardian_name as Guardian Name"> Guardian Name </option>
                        <option value="sub_client_address as Address"> Address </option>
                        <option value="sub_client_name as Client Sub Group"> Client Sub Group </option>
                        <option value="sub_client_mobile_no as Mobile Number"> Mobile Number </option>
                        <option value="act_name as Act"> Act </option>
                        <option value="section_name as Section"> Section </option>
                        <option value="reg_power as Power"> Power </option>
                        <option value="reg_fir_id as FIR Number"> FIR Number </option>
                        <option value="pessi_further_date as Next Date"> Next Date </option>
                        <option value="stage_name as Stage Name"> Stage </option>
                        <option value="pessi_choose_type as Result"> Result </option>
                        <option value="ref_advocate_name as Referred By"> Referred By </option>
                        <option value="reg_opp_council as Opposite Counsel"> Opposite Counsel </option>
                        <option value="cl_group_place as Place"> Place </option>
                        <option value="assign_advocate_name as Assigned"> Assigned </option>
                      </select>
                    </label>
                  </div>
                </div>

                <input type="hidden" name="case_category" value="{{ $case_category }}">
                <input type="hidden" name="choose_case_type" value="{{ $choose_case_type }}">
                <input type="hidden" name="case_no" value="{{ $case_no }}">
                <input type="hidden" name="file_no" value="{{ $file_no }}">
                <input type="hidden" name="case_status" value="{{ $case_status }}">
                <input type="hidden" name="court_type" value="{{ $court_type }}">
                <input type="hidden" name="court_name" value="{{ $court_name }}">
                <input type="hidden" name="reg_stage" value="{{ $reg_stage }}">
                <input type="hidden" name="power" value="{{ $power }}">
                <input type="hidden" name="assigned" value="{{ $assigned }}">
                <input type="hidden" name="act" value="{{ $act }}">
                <input type="hidden" name="section" value="{{ $section }}">
                <input type="hidden" name="reffered" value="{{ $reffered }}">
                <input type="hidden" name="group_type" value="{{ $group_type }}">
                <input type="hidden" name="client_group" value="{{ $client_group }}">
                <input type="hidden" name="client_sub_group" value="{{ $client_sub_group }}">
                <input type="hidden" name="reg_date_from" value="{{ $reg_date_from }}">
                <input type="hidden" name="reg_date_to" value="{{ $reg_date_to }}">
                <input type="hidden" name="disposal_from" value="{{ $disposal_from }}">
                <input type="hidden" name="disposal_to" value="{{ $disposal_to }}">
                <input type="hidden" name="client_place" value="{{ $client_place }}">
                <input type="hidden" name="next_date_from" value="{{ $next_date_from }}">
                <input type="hidden" name="next_date_to" value="{{ $next_date_to }}">
                <input type="hidden" name="from_date" value="{{ $from_date }}">
                <input type="hidden" name="to_date" value="{{ $to_date }}">
                <input type="hidden" name="reporting_heading" value="{{ $reporting_heading }}">

                <input type="hidden" name="all_reg_id[]" name="all_reg_id" value="">
                
                <div class="col-md-2 pull-left" style="margin-top: 22px;">
                  <button type="submit" name="search" class="button btn-primary save_search"> Download Excel </button>
                </div>
                     
                
                <div class="clearfix"></div>     
            </div>
          </div>

          <div class="panel-body pn">
              
              <div class="table-responsive" style="overflow: scroll;">
                <table class="table admin-form table-bordered table-striped theme-warning tc-checkbox-1 fs13 dataTable no-footer" id="datatable">
                  <thead>
                    <tr class="bg-light">
                      <th style="width:90px !important;" class="text-left">
                        <label class="option block mn" style="width:85px !important;">
                          <input type="checkbox" id="check_all"> 
                          <span class="checkbox mn"></span>
                          Select All
                        </label>
                      </th>
                      <th class="">Reg. Date</th>
                      <th class="">File Number</th>
                      <!-- <th class="">Court Type</th> -->
                      <th class="">Case Type</th>
                      <th class="">Case Number</th>
                      <th class="">Petitioner</th>
                      <th class="">Respondent</th>
                      <th class="">Next Date</th>
                      <th class="">Result</th>
                      <th class="">Class Code</th>
                      <th class="">NCV Number</th>
                      <th class="">Stage</th>
                      <th class="">Power</th>
                      <th class="">FIR Number</th>
                      <th class="">Client Group/Clients</th>
                      <th class="">Client Sub Group</th>
                      <th class="">Act</th>
                      <th class="">Section</th>
                      <th class="">Assigned</th>
                      <th class="">Refferd By</th>
                      <th class="">Opposite Counsel</th>    
                    </tr>
                  </thead>
                  <tbody>
                  @foreach($get_record as $get_records) 

                    <tr>
                      <td class="text-left" style="padding-left: 18px;">
                        <label class="option block mn">
                          <input type="checkbox" name="check[]" id="get_all_records" class="check" value="{{$get_records->reg_id}}">
                          <span class="checkbox mn"></span>
                        </label>
                      </td>

                      <td class="text-left" style="padding-left:20px">  
                        @if($get_records->reg_date == "1970-01-01")
                          -----
                        @else
                          {{ date('d/m/Y',strtotime($get_records->reg_date)) }}
                        @endif             
                      </td>
                      <td class="" style="padding-left:20px"><a href="{{ url('/advocate-panel/case-detail') }}/{{sha1($get_records->reg_id) }}"> @if($get_records->reg_file_no == "") ----- @else {{ $get_records->reg_file_no }} @endif </a></td>



                      <!-- <td class="text-left" style="padding-left:20px">
                        @if($get_records->reg_court == 2)
                               High Court
                        @elseif($get_records->reg_court == 1)
                               Trial Court
                        @endif
                      </td> -->
                      
                      <td class="text-left" style="padding-left:20px"> @if($get_records->case_name == "") ----- @else {{ $get_records->case_name }} @endif </td>
                      

                      <td class="text-left" style="padding-left:20px"> @if($get_records->reg_case_number == "") ----- @else {{ $get_records->reg_case_number }} @endif </td>

                      <td class="text-left" style="padding-left:20px"> @if($get_records->reg_petitioner == "") ----- @else {{ $get_records->reg_petitioner }} @endif </td>

                      <td class="text-left" style="padding-left:20px"> @if($get_records->reg_respondent == "") ----- @else {{ $get_records->reg_respondent }} @endif </td>
                      
                      <td class="text-left" style="padding-left:20px">  
                        @if($get_records->pessi_further_date == "1970-01-01")
                          -----
                        @else
                          {{ date('d F Y',strtotime($get_records->pessi_further_date)) }}
                        @endif             
                      </td>


                      <td class="text-left">  
                        
                        @if($get_records->pessi_choose_type == 0)
                          <button type="button" class="btn btn-info br2 btn-xs fs12 dropdown-toggle"> Pending </button> 
                        @elseif($get_records->pessi_choose_type == 1)
                          <button type="button" class="btn btn-success br2 btn-xs fs12 dropdown-toggle"> Disposal </button>
                        @else
                          <button type="button" class="btn btn-success br2 btn-xs fs12 dropdown-toggle"> Due Course </button>
                        @endif

                      </td>

                      <td class="text-left" style="padding-left:20px"> @if($get_records->classcode_name == "") ----- @else {{ $get_records->classcode_name }} @endif </td>

                      <td class="text-left" style="padding-left:20px"> @if($get_records->reg_vcn_number == "") ----- @else {{ $get_records->reg_vcn_number }} @endif </td>

                      <td class="text-left" style="padding-left:20px"> @if($get_records->stage_name == "") ----- @else {{ $get_records->stage_name }} @endif </td>

                      <td class="text-left" style="padding-left:20px"> 
                        @if($get_records->reg_power == 1)
                            P
                            @elseif($get_records->reg_power == 2)
                            R
                            @elseif($get_records->reg_power == 3)
                            C
                            @elseif($get_records->reg_power == 4)
                            N    
                        @endif
                      </td>
                      
                      <td class="text-left" style="padding-left:20px"> @if($get_records->reg_fir_id == "") ----- @else {{ $get_records->reg_fir_id }} @endif </td>

                      <td class="text-left" style="padding-left:20px"> @if($get_records->cl_group_name == "") ----- @else {{ $get_records->cl_group_name }} @if($get_records->cl_father_name != "") {{ $get_records->cl_name_prefix }} {{ $get_records->cl_father_name }} @endif @endif </td>

                      <td class="text-left" style="padding-left:20px"> @if($get_records->sub_client_name == "") ----- @else {{ $get_records->sub_client_name }} @endif </td>
                      
                      <td class="text-left" style="padding-left:20px"> @if($get_records->act_name == "") ----- @else {{ $get_records->act_name }} @endif </td>


                      @php
                        $section_name  = "";
                        $get_records->reg_section_id = explode(',', $get_records->reg_section_id);
                        $section =  \App\Model\Section\Section::whereIn('section_id', $get_records->reg_section_id )->where('section_name', '!=' ,'')->get();

                        foreach($section as $sections){
                          $section_name .= $sections->section_name.' , ';
                        }

                        $section_name = substr($section_name,0,-2);
                      @endphp


                      <td class="text-left" style="padding-left:20px"> @if($section_name == "") ----- @else {{ $section_name }} @endif </td>                 
<!-- 
                      <td class="text-left" style="padding-left:20px"> @if($get_records->reg_status == "1") Pending @else Disposal @endif </td> -->

                      
                      <td class="text-left" style="padding-left:20px"> @if($get_records->assign_advocate_name == "") ----- @else {{ $get_records->assign_advocate_name }} @endif </td>
                      
                      <td class="text-left" style="padding-left:20px"> @if($get_records->ref_advocate_name == "") ----- @else {{ $get_records->ref_advocate_name }} @endif </td>

                      <td class="text-left" style="padding-left:20px"> @if($get_records->reg_opp_council == "") ----- @else {{ $get_records->reg_opp_council }} @endif </td>


                      
                      
                    </tr>
                  @endforeach()
                  </tbody>
                </table>
              </div>
          </div>

          {!! Form::close() !!}      
        </div>
      </div>
    </div>
  </section>

<script type="text/javascript">

function choose_sub_type(client_id){

  if (client_id != ''){

    BASE_URL = '{{ url('/')}}';
    $.ajax({
      url:BASE_URL+"/advocate-panel/change-client-sub-type-ajax/"+client_id,
      success: function(result){
          $("#hide_choose_sub_type").html(result); 
      }
    });
  }

}




</script>
<style type="text/css">
.view a{
  color:#fff !important;
}
</style>
<style type="text/css">
.dt-panelfooter{
  display: none !important;
}
</style>

<!-- <script type="text/javascript">
  
  $(".save_search").on("click", function() {

      var get_all_records = $("#get_all_records").val();

        alert(get_all_records);

        // var flag = ( $(".check:checked").length == $(".check").length ) ? true : false
        // $("#check_all").prop("checked", flag);
    
    });

</script> -->

@endsection