
<!-- Start : Header -->  

    @include('advocate_admin/include/header')

<!-- End : Header -->	


<!-- Start: Menu -->

    @include('advocate_admin/include/sidebar')

<!-- End: Menu -->

@yield('content')

<!-- Start: Footer -->
  
    @include('advocate_admin/include/footer')

<!-- End: Footer -->