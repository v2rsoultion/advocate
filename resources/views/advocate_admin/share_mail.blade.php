<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<title>Email</title>
	<style type="text/css">
		#outlook a {padding:0;}
		body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
		.ExternalClass {width:100%;}
		.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
		#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
		img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;}
		a img {border:none;display:inline-block;}
		.image_fix {display:block;}
		h1, h2, h3, h4, h5, h6 { margin: 0px;}
		h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: blue !important;}
		h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {
			color: red !important; 
		}
		h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {
			color: purple !important; 
		}
		table td {border-collapse: collapse;}
		table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
		a {color: #000;}
		@font-face {
		    font-family: 'bernardo_moda_boldregular';
		    src: url('public/fonts/bernardo_moda_bold-webfont.woff2') format('woff2'), url('public/fonts/bernardo_moda_bold-webfont.woff') format('woff');
		    font-weight: normal;
		    font-style: normal;
		}
		@font-face {
		    font-family: 'RobotoLight';
		    src: url('public/fonts/RobotoLight.eot');
		    src: url('public/fonts/RobotoLight.eot') format('embedded-opentype'), url('public/fonts/RobotoLight.woff2') format('woff2'), url('public/fonts/RobotoLight.woff') format('woff'), url('public/fonts/RobotoLight.ttf') format('truetype'), url('public/fonts/RobotoLight.svg#RobotoLight') format('svg');
		}
		.borderbottom{
			padding: 15px;
			background: #67778e;
		}
		.footer {
			padding: 15px 0px;
			text-align: center;
			background: #67778e;
		}
		.mailtemplate{
			margin: 0px auto;
			padding: 15px 15px;
		}
		.logo{
			text-align: center;
		}

		/*.logo img{
			width: 25%;
		}*/

		.footer{
			color: #ffffff;
			text-align: center;
			font-size: 14px; 
			font-family: 'RobotoLight';
		}
		span{
			color: #6ab1d7;
		}
		p{	
			text-align: justify;
			margin: 15px;
			padding: 0px;
			margin: 0px;
		    font-family: 'RobotoLight';
		}
		.tr{
			padding: 50px 0px;
		}
		.socilicons{
			float: right;
		}
		.socilicons p{
			color: #fff;
			padding: 5px 0px;
			font-size: 14px;
		}
		.socilicons a{
			color: #fff;
			text-decoration: none;
			font-size: 14px;
		}
		.socilicons i{
			margin-right: 10px;
			color: #fcb040;
		}
		.middlediv{
			padding: 30px 10px;
		}
		.middlediv h1{
			color: #353439;
			font-family: 'bernardo_moda_boldregular';
			font-size: 24px;
			margin-left: 15px;
			padding-bottom: 20px;
			text-transform: capitalize;
		}
		.middlediv p{
			margin: 20px 0px 0px 20px;
			font-size: 14px;
			line-height: 18px;
		}
		.middlediv1{
			padding: 0px 10px 20px 15px;
		}
		.middlediv1 h1{
			color: #353439;
			font-family: 'bernardo_moda_boldregular';
			font-size: 16px;
			margin-left: 15px;
			text-transform: capitalize;
		}
		.imgmail{
			margin: 0px auto;
			width:200px;
		}
		.imgmail img{
			width: 200px;
		}
		@media (max-width: 461px){ 
			.logo img{
				width: 90%;
			}
		}
	</style>

<script type="colorScheme" class="swatch active">
  {
    "name":"Default",
    "bgBody":"ffffff",
    "link":"f2f2f2",
    "color":"555555",
    "bgItem":"F4A81C",
    "title":"181818"
  }
</script>

</head>
<body>	
    <div class="mailtemplate">
		<div class="main">
			<div class="borderbottom">
				
				@php
					$account  = App\Model\Admin\Account::first();
				@endphp

				<div class="logo">
					<img src="{{ URL::asset('public/images/advocate.png')}}"  alt='Logo' title="Logo" />
				</div>
				
				<div style="clear:both;"></div>
			</div>
			<div class="middlediv">
				<h1>Hello User</h1>
				<p> {{ $message_share }} </p>
			</div>

			@php

				$get_case_details = App\Model\Case_Registration\Case_Registration::leftJoin('adv_pessi', function($join) {$join->on('pessi_case_reg', '=', 'reg_id');})->leftJoin('adv_stage', function($join) {$join->on('stage_id', '=', 'pessi_statge_id');})->leftJoin('adv_court', function($join) {$join->on('court_id', '=', 'reg_court_id');})->leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->leftJoin('adv_class_code', function($join) {$join->on('classcode_id', '=', 'reg_case_subtype_id');})->leftJoin('adv_fir', function($join) {$join->on('fir_id', '=', 'reg_fir_id');})->leftJoin('adv_assigned', function($join) {$join->on('assign_id', '=', 'reg_assigend_id');})->leftJoin('adv_act', function($join) {$join->on('act_id', '=', 'reg_act_id');})->leftJoin('adv_section', function($join) {$join->on('section_id', '=', 'reg_section_id');})->leftJoin('adv_referred', function($join) {$join->on('ref_id', '=', 'reg_reffeerd_by_id');})->leftJoin('adv_client', function($join) {$join->on('cl_id', '=', 'reg_client_group_id');})->leftJoin('adv_sub_client', function($join) {$join->on('sub_client_id', '=', 'reg_client_subgroup_id');})->where(['adv_case_reg.reg_id' => $reg_id , 'pessi_status' => '1' ])->first();

      			$pessi_detail = App\Model\Pessi\Pessi::leftJoin('adv_case_reg', function($join) {$join->on('pessi_case_reg', '=', 'reg_id');})->leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'reg_case_type_id');})->leftJoin('adv_stage', function($join) {$join->on('stage_id', '=', 'pessi_statge_id');})->leftJoin('adv_judge', function($join) {$join->on('judge_id', '=', 'honaurable_justice');})->where('pessi_case_reg', $get_case_details->reg_id)->orderBy('pessi_id', 'desc')->get(); 

      			$compliance_detail = App\Model\Compliance\Compliance::leftJoin('adv_case_reg', function($join) {$join->on('compliance_case_id', '=', 'reg_id');})->leftJoin('adv_case_type', function($join) {$join->on('case_id', '=', 'compliance_case_type');})->where('compliance_case_id','=' ,$get_case_details->reg_id)->orderBy('compliance_id', 'desc')->get();



      		@endphp


			<div class="panel-heading" style="background: gray; color: white; font-size: 17px; margin-bottom: 30px;">
                  <div class="panel-title hidden-xs text-center" style="padding: 4px 0px; text-align: center;"> 
                    @if($get_case_details->pessi_choose_type == 0) Pending @elseif($get_case_details->pessi_choose_type == 1) {{ date('d F Y',strtotime($get_case_details->created_at)) }} :- Disposal @else Due Course @endif
                  </div>
                </div>

                <div class="panel-heading" style="background: gray; color: white; font-size: 17px;">
                  <div class="panel-title hidden-xs text-center" style="padding: 4px 0px; text-align: center;"> Case Details </div>
                </div>

                <div class="panel-body pn">
                  <table class="table table-striped table-hover" id="" cellspacing="0" width="100%" style="color: #000;">
                    <thead>
                      <tr>
                        <th class="" style="border: 1px solid gray; text-align: left; padding: 7px 7px;"> <b> Court Name :- </b> @if( $get_case_details->court_name != "") {{ $get_case_details->court_name }} @else ----- @endif </th>
                      </tr>
                      <tr>
                        <th class="" style="border: 1px solid gray; padding: 7px 7px; background-color: antiquewhite;">
                         <!-- class= col-md-6 -->
                          <div class="" style="text-align: left; float:left; width: 45%; "> 
                            <b> Institution Date :- </b> @if( $get_case_details->reg_date != "") {{ date('d/m/Y',strtotime($get_case_details->reg_date))  }} @else ----- @endif <br> 

                            <b> File No :- </b> @if( $get_case_details->reg_file_no != "") {{ $get_case_details->reg_file_no }} @else ----- @endif
                          </div>
                          <div class="" style=" text-align: left; float: left; width: 45%;  border-left: 4px solid #eeeeee !important;"> 
                            <b style="margin-left: 10px;"> NCV No :- </b> @if( $get_case_details->reg_vcn_number != "") {{ $get_case_details->reg_vcn_number }} @else ----- @endif <br> 
                            <b style="margin-left: 10px;"> Class Code :- </b> @if( $get_case_details->classcode_name != "") {{ $get_case_details->classcode_name }} @else ----- @endif </div> 
                        </th>  
                      </tr>
                      <tr style="text-align: left;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px;"> 
                          <b> Case Number :- 
                            @if( $get_case_details->case_name != "") {{ $get_case_details->case_name }} @else ---- @endif 
                            @if( $get_case_details->reg_case_number != "") {{ $get_case_details->reg_case_number }} @else ---- @endif
                            <!-- @if( $get_case_details->reg_date != "") {{ date('Y',strtotime($get_case_details->reg_date))  }} @else ----- @endif -->
                          </b></th>
                      </tr>
                      <tr style="text-align: left; background-color: antiquewhite;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px;"> <b> Case Title :- @if( $get_case_details->reg_petitioner != "") {{ $get_case_details->reg_petitioner }} @else ---- @endif v/s @if( $get_case_details->reg_respondent != "") {{ $get_case_details->reg_respondent }} @else ---- @endif </b></th>
                      </tr>
                      <tr style="text-align: left;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px;"> <b> Power :- </b>
                          @if($get_case_details->reg_power == 1) Petitioner @elseif($get_case_details->reg_power == 2 ) @if($get_case_details->reg_power_respondent != "") Respondent - {{ $get_case_details->reg_power_respondent }} @else Respondent @endif @elseif($get_case_details->reg_power == 3 ) Caveat @elseif($get_case_details->reg_power == 4 ) None @else ---- @endif
                        </th>
                      </tr>
                      <tr style="">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px; background-color: antiquewhite;"> 
                          <!-- class= col-md-6 -->
                          <div class="" style="text-align: left; float:left; width: 45%;"> 
                            <b class=""> FIR No :- </b> @if( $get_case_details->reg_fir_id != "") {{ $get_case_details->reg_fir_id }} @else ---- @endif <br> 
                            <b class=""> FIR Year :- </b> @if( $get_case_details->reg_fir_year != "") {{ $get_case_details->reg_fir_year }} @else ---- @endif </div>
                          <div class="" style="float: left; text-align: left; width: 45%; border-left: 4px solid #eeeeee !important;"> <b style="margin-left: 15px;"> Police Station :- </b> @if( $get_case_details->reg_police_thana != "") {{ $get_case_details->reg_police_thana }} @else ---- @endif </div> 
                        </th>
                      </tr>
                    </thead>
                  </table>
                </div>

                <br>

                <div class="panel-heading" style="background: gray; color: white; font-size: 17px;">
                  <div class="panel-title hidden-xs text-center" style="padding: 4px 0px; text-align: center;"> Case Status </div>
                </div>

                <div class="panel-body pn">
                  <table class="table table-striped table-hover" id="" cellspacing="0" width="100%" style="color: #000;">
                    <thead>
                      <tr style="text-align: left;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px;"> <b> Status :- </b> @if($get_case_details->pessi_choose_type == 0) Pending @elseif($get_case_details->pessi_choose_type == 1) Disposal ( {{ date('d F Y',strtotime($get_case_details->created_at)) }} ) @else Due Course @endif </th>
                      </tr>
                      <tr style="text-align: left; background-color: antiquewhite;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px;"> <b> Previous Date :- </b>
                          @if($get_case_details->pessi_prev_date == "1970-01-01" || $get_case_details->pessi_prev_date == "")
                            -----
                          @else
                            {{ date('d/m/Y',strtotime($get_case_details->pessi_prev_date)) }}
                          @endif
                        </th>
                      </tr>
                      <tr style="text-align: left;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px;"> <b> Next Date :- </b>
                          @if($get_case_details->pessi_further_date == "1970-01-01")
                            -----
                          @else
                            <blink> {{ date('d/m/Y',strtotime($get_case_details->pessi_further_date)) }} </blink>
                          @endif
                        </th>
                      
                      </tr>
                      <tr style="text-align: left; background-color: antiquewhite;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px;"> <b> Stage :- </b> @if( $get_case_details->stage_name != "") <blink> {{ $get_case_details->stage_name }} </blink> @else ---- @endif </th>
                      </tr>
                      <tr style="text-align: left;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px;"> <b> Extra Party :- </b> 
                          <button type="button" class="btn btn-primary view" style="padding: 4px 10px; background-color: #4a89dc; border:none;"  data-toggle="modal" data-target="#reg_petitioner{{$get_case_details->reg_id}}" > 
                            <a href="#" style="text-transform: capitalize; text-decoration:none; color: white;"> Pet. Details </a> 
                          </button> 

                          <button type="button" class="btn btn-primary view" style="padding: 4px 10px; background-color: #4a89dc; border:none;" data-toggle="modal" data-target="#reg_respondent{{$get_case_details->reg_id}}" >
                            <a href="#" style="text-transform: capitalize; text-decoration:none; color: white;"> Resp. Details </a> 
                          </button> 
                        </th>
                      </tr>
                    </thead>
                  </table>
                </div>
                            
               
                        


                <br>

                <div class="panel-heading" style="background: gray; color: white; font-size: 17px;">
                  <div class="panel-title hidden-xs text-center" style="padding: 4px 0px; text-align: center;"> Client Details </div>
                </div>

                <div class="panel-body pn">
                  <table class="table table-striped table-hover" id="" cellspacing="0" width="100%" style="color: #000;">
                    <thead>
                      <tr style="text-align: left;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px;"> <b> Referred By :- </b> @if( $get_case_details->ref_advocate_name != "") {{ $get_case_details->ref_advocate_name }} @else ---- @endif </th>
                      </tr>
                      <tr style="text-align: left;">
                        <th class="" style="border: 1px solid gray; padding: 7px 5px; text-align: left; background-color: antiquewhite;"> <div class="col-md-2"> <b> Client Name :- </b> </div> 
                          @if( $get_case_details->reg_client_group_id != "") 

                            @if($get_case_details->cl_group_type == "1") 
                              
                              <div class="col-md-11" >
                                {{ $get_case_details->cl_group_name }} @if($get_case_details->cl_father_name != "") {{ $get_case_details->cl_name_prefix }} {{ $get_case_details->cl_father_name }} @endif <br> {{ $get_case_details->cl_group_address }} {{ $get_case_details->cl_group_place }} <br> {{ $get_case_details->cl_group_mobile_no }} 
                              </div>

                            @else 
                              
                              <div class="col-md-11" >
                                {{ $get_case_details->cl_group_name }} @if($get_case_details->cl_father_name != "") {{ $get_case_details->cl_name_prefix }} {{ $get_case_details->cl_father_name }} @endif <br> {{ $get_case_details->sub_client_name }} <br> {{ $get_case_details->sub_client_address }} {{ $get_case_details->sub_client_place }} <br> {{ $get_case_details->sub_client_mobile_no }} 
                              </div>

                            @endif

                          @else ---- @endif </th>
                      </tr>
                    </thead>
                  </table>
                </div>

                <br>

                <div class="panel-heading" style="background: gray; color: white; font-size: 17px;">
                  <div class="panel-title hidden-xs text-center" style="padding: 4px 0px; text-align: center;"> Other Details </div>
                </div>

                <div class="panel-body pn">
                  <table class="table table-striped table-hover" id="" cellspacing="0" width="100%" style="color: #000;">
                    <thead>
                      <tr style="text-align: left;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px;"> <b> Assigned :- </b> @if( $get_case_details->assign_advocate_name != "") {{ $get_case_details->assign_advocate_name }} @else ---- @endif</th>
                      </tr>
                      <tr style="background-color: antiquewhite; text-align: left;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px;"> <b> Act :- </b> @if( $get_case_details->act_name != "") {{ $get_case_details->act_name }} @else ---- @endif</th>
                      </tr>

                      @php
                        $section_name  = "";
                        $get_case_details->reg_section_id = explode(',', $get_case_details->reg_section_id);
                        $section =  \App\Model\Section\Section::whereIn('section_id', $get_case_details->reg_section_id )->where('section_name', '!=' ,'')->get();

                        foreach($section as $sections){
                          $section_name .= $sections->section_name.' , ';
                        }

                        $section_name = substr($section_name,0,-2);
                      @endphp
                      

                      <tr style="text-align: left;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px;"> <b> Section :- </b> @if( $section_name != "") {{ $section_name }} @else ---- @endif </th>
                      </tr>
                      

                      <tr style="background-color: antiquewhite; text-align: left;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px;"> <b> Opposite Counsel :- </b> @if( $get_case_details->reg_opp_council != "") {{ $get_case_details->reg_opp_council }} @else ---- @endif </th>
                      </tr>
                      <tr style="text-align: left;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px;"> <b> Remark Field :- </b> @if( $get_case_details->reg_remark != "") {{ $get_case_details->reg_remark }} @else ---- @endif </th>
                      </tr>

                      <tr style="text-align: left; background-color: antiquewhite;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px;"> <b> Other Field :- </b> @if( $get_case_details->reg_other_field != "") {{ $get_case_details->reg_other_field }} @else ---- @endif </th>
                      </tr>
                    </thead>
                  </table>
                </div>

                <br>

                <div class="panel-heading" style="background: gray; color: white; font-size: 17px;">
                  <div class="panel-title hidden-xs text-center" style="padding: 4px 0px; text-align: center;"> History of Case </div>
                </div>

                <div class="panel-body pn">
                  <table class="table table-striped table-hover" id="" cellspacing="0" width="100%" style="color: #000;">
                    <thead>
                      <tr>
                        <th class="text-left" style="border: 1px solid gray; padding: 7px 7px;">
                          <!-- <label class="option block mn"> -->
                           <b> Delete</b>
                          <!-- </label> -->
                        </th>
                        <th class="" style="border: 1px solid gray; padding: 7px 7px;"> <b> Peshi Date </b></th>
                        <th class="" style="border: 1px solid gray; padding: 7px 7px;"> <b> Court No: Judge Name </b></th>
                        <th class="" style="border: 1px solid gray; padding: 7px 7px;"> <b> Purpose / Stage </b></th>
                        <th class="" style="border: 1px solid gray; padding: 7px 7px;"> <b> Order Sheet </b></th>
                      </tr>
                    </thead>

                    <tbody>
                      @foreach($pessi_detail as $pessi_details)
                        
                        <tr>
                          <!-- <td class=""> 
                            <a href="{{ url('advocate-panel/view-peshi')}}" style="text-decoration: none;">
                              @if($pessi_details->pessi_choose_type == 0)
                                Date:-  {{ date('d F Y',strtotime($pessi_details->pessi_further_date)) }}
                              @elseif($pessi_details->pessi_choose_type == 1)
                                Decide :- @if($pessi_details->pessi_decide_id == 1) Case In Favour @elseif($pessi_details->pessi_decide_id == 2) Case in against @elseif($pessi_details->pessi_decide_id == 3) Withdraw @elseif($pessi_details->pessi_decide_id == 4) None @endif
                              @else
                                Due Course
                              @endif
                            </a> -->

                          <td class="text-left" style="padding-left: 20px; border: 1px solid gray; padding: 7px 7px;">
                            @if(count($pessi_detail) == 1)

                            @else
                            <label class="option block mn">
                              <a href="{{ url('advocate-panel/delete-pessi') }}/{{ sha1($pessi_details->pessi_id) }}">
                                <span class="fa fa-trash" style="cursor: pointer;"></span>
                              </a>
                            </label>

                            @endif
                          </td>

                          <td style="border: 1px solid gray; padding: 7px 7px;">
                            <a href="#" style="text-transform: capitalize; text-decoration:none;" data-toggle="modal" data-target="#pessi_details{{$pessi_details->pessi_id}}" > 

                              @if($pessi_details->pessi_choose_type == 0)

                                @if($pessi_details->pessi_further_date == "" || $pessi_details->pessi_further_date ==  "1970-01-01")
                                  Date:-  -----
                                @else

                                  Date:-  {{ date('d F Y',strtotime($pessi_details->pessi_further_date)) }}
                                
                                @endif

                                
                              @elseif($pessi_details->pessi_choose_type == 1)

                                Decide :- @if($pessi_details->pessi_decide_id == 1) Case In Favour @elseif($pessi_details->pessi_decide_id == 2) Case in against @elseif($pessi_details->pessi_decide_id == 3) Withdraw @elseif($pessi_details->pessi_decide_id == 4) None @endif
                              @else
                                Due Course
                              @endif

                            </a>

                            

                          </td>

                          <script type="text/javascript">
                            function update_pessi_details(pessi_id) {

                              if(confirm("Are you sure to want update records?")) {
                                $("[name=form_add_pessi"+pessi_id+"]").submit();    
                              }
                            }
                          </script>

                          @php
                            $justise_name =  \App\Model\Judge\Judge::where(['judge_id' => $pessi_details->honarable_justice_db ])->first();
                          @endphp

                          <td class="" style=" border: 1px solid gray; padding: 7px 7px;"> 
                            @if($pessi_details->reg_court == 2)
                              @if($pessi_details->judge_name == "" && $pessi_details->court_number == "")
                                -----
                              @else
                                @if($pessi_details->court_number != "")  C-{{$pessi_details->court_number}}: &nbsp @endif

                                @if($pessi_details->judge_name != "")  {{$pessi_details->judge_name}}  @endif

                                @if($justise_name->judge_name != "" && $pessi_details->judge_name != "") &  @endif
                                @if($justise_name->judge_name != "")  {{$justise_name->judge_name}}  @endif

                                @if( ($pessi_details->judge_name != "" || $justise_name->judge_name != "") && $pessi_details->court_number != "")   @endif
                                
                              @endif
                            @elseif($pessi_details->reg_court == 1)
                                   -------
                            @endif
                          </td>
                          <td class="" style=" border: 1px solid gray; padding: 7px 7px;"> @if($pessi_details->stage_name == "") ----- @else {{ $pessi_details->stage_name }} @endif </td>
                          <td class="" style=" border: 1px solid gray; padding: 7px 7px;"> @if($pessi_details->pessi_order_sheet == "") ----- @else {{ $pessi_details->pessi_order_sheet }} @endif</td>
                        </tr>

                      @endforeach
                    </tbody>
                  </table>
                </div>

                <br>

                <div class="panel-heading" style="background: gray; color: white; font-size: 17px;">
                  <div class="panel-title hidden-xs text-center" style="padding: 4px 0px; text-align: center;"> Compliance </div>
                </div>

                <div class="panel-body pn">
                  <table class="table table-striped table-hover" id="" cellspacing="0" width="100%" style="color: #000;">
                    <thead>
                      <tr>
                        <th class="" style="border: 1px solid gray; padding: 7px 7px;"> <b> Created </b></th>
                        <th class="" style="border: 1px solid gray; padding: 7px 7px;"> <b> View Compliance </b></th>
                      </tr>
                    </thead>

                    <tbody>

                      @if(count($compliance_detail) != 0)
                        @foreach($compliance_detail as $compliance_details)
                          <tr>
                            <td class="" style="border: 1px solid gray; padding: 7px 7px;"> @if($compliance_details->compliance_created != "" || $compliance_details->compliance_created != "1970-01-01") {{ date('d M Y',strtotime($compliance_details->compliance_created)) }} @else ----- @endif </td>
              
                            <td class="text-left" style="border: 1px solid gray; padding: 7px 7px;">
                              <button type="button" class="btn btn-primary view btn-xs" data-toggle="modal" data-target="#view_compliance{{$compliance_details->compliance_id}}" style="padding: 4px 10px; background-color: #4a89dc; border:none;">
                                <a href="#" style="text-transform: capitalize; text-decoration:none; color: white;"> View Compliance</a>
                              </button>
                            </td>
                          </tr>    
                        @endforeach
                      @else 
                        <tr >
                          <td colspan="5" class="text-center" style="border: 1px solid gray; padding: 7px 7px;">No Record Found !!</td>
                        </tr>
                      @endif

                    </tbody>
                  </table>
                </div>

                <br>

                <div class="panel-heading" style="background: gray; color: white; font-size: 17px;">
                  <div class="panel-title hidden-xs text-center" style="padding: 4px 0px; text-align: center;"> Documents </div>
                </div>

                <div style="border: 1px solid gray; padding: 15px 7px;" class="panel-body pn" style="color: #000;">

                  @php
                    $images_cat =  \App\Model\Upload_Document\Upload_Document::where(['upload_case_id' => $get_case_details->reg_id ])->where('upload_images', '!=' ,'')->get();
                  @endphp

                  @if(count($images_cat) != 0)
                    @foreach($images_cat as $images_cats) 


                    @php
                      $doc_image = explode("/",$images_cats['upload_images']);
                      $last =  substr($doc_image[2],-4);
                    @endphp
                    
                      <div class="col-md-4 document_clear" style="margin-bottom: 22px; margin-top: 22px; position: relative;" id="cat_img{{$images_cats['upload_id']}}">
                        
                        @if($last == '.jpg' or $last == 'jpeg' or $last == '.png' or $last == '.gif')
                         
                         <!-- {!! Html::image($images_cats['upload_images'], '', array('class' => 'media-object mw150 customer_profile', 'width'=>'100%', 'height'=>'250')) !!} -->

                          <a target="_blank" href="{{url($images_cats['upload_images'])}}" class="btn btn-xs btn-primary" style="margin-left: 10px; margin-top: 10px;"> View </a>
                        @elseif($last == '.pdf')
                         <i class="fa fa-file-pdf-o"></i> 
                          <a target="_blank" href="{{url($images_cats['upload_images'])}}" class="btn btn-xs btn-primary" style="margin-left: 10px;"> View </a>
                        @elseif($last == 'docx' or $last == '.doc')
                         <i class="fa fa-file-word-o"></i>
                          <a target="_blank" href="{{url($images_cats['upload_images'])}}" class="btn btn-xs btn-primary" style="margin-left: 10px;"> View </a>
                        @elseif($last == '.xls' or $last == 'xlsx')
                         <i class="fa fa-file-excel-o"></i>
                          <a target="_blank" href="{{url($images_cats['upload_images'])}}" class="btn btn-xs btn-primary" style="margin-left: 10px;"> Download </a>
                        @else
                          <a target="_blank" href="{{url($images_cats['upload_images'])}}" class="btn btn-xs btn-primary" style="margin-left: 10px;"> View </a>
                        @endif

                        @if($images_cats['upload_caption'] != "" )
                          <div style="margin-top: 10px;"> Caption :- {{ $images_cats['upload_caption'] }} </div>
                        @endif

                        @if($images_cats['upload_date'] != "" )
                          <div style="margin-top: 10px;"> Date :- 
                            @if($images_cats['upload_date'] == "1970-01-01")
                              -----
                            @else
                              {{ date('d/m/Y',strtotime($images_cats['upload_date'])) }}
                            @endif 
                          </div>
                        @endif


                      </div>
                    @endforeach
                  @else 
                    <div style="text-align: center; border: 1px solid gray; padding: 15px 7px; color: #000;" class="text-center" > No Documents Available !! </div>
                  @endif
                  
                </div>




			<div class="middlediv1">
				<h1>Thank You <br> </h1>
			</div>

			<div class="footer">
				Copyright &copy; {{ date('Y') }} <span> Advocate </span> .	 All Rights Reserved.
			</div>
		</div>
	</div>
</body>
</html>