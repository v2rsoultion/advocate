@extends('trivia_admin/layout')

@section('content')

  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="{{ url('/gullyyquiz-panel/view-question') }}">View Question</a>
            </li>
            <li class="crumb-icon">
              <a href="{{ url('gullyyquiz-panel/dashboard') }}">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="{{ url('gullyyquiz-panel/dashboard') }}">Home</a>
            </li>
            <li class="crumb-trail">Import Excel</li>
          </ol>
        </div>
      </header>

      <!-- Begin: Content -->

      <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center">
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-heading">
                <span class="panel-title"> Import Excel </span>
              </div>
              <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                  <div id="tab1_1" class="tab-pane active">

                    <div class="row" id="my_file_error">
                      
                      </div>
                    </div>
 

                    <div class="col-md-12">
                          <div class="section">
                            <form action="{{ URL::to('trivialeague-panel/importExcel') }}" class="form-horizontal" method="post" enctype="multipart/form-data" name="test" id="test">
                             <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                            <label for="user_email" class="field-label" style="font-weight:600;"> Upload Excel File </label>
                            <!-- <div class="section" id="spy2" style="margin-bottom: 5px;">
                              <label for="file1" class="field file">
                                <span class="button btn-primary"> Choose Excel File </span>
                                <input type="file" class="gui-file flUpload" name="import_file" id="import_file" onchange="document.getElementById('uploader1').value = this.value;">
                                <input type="text" class="gui-input" id="uploader1" placeholder="No statement file selected" readonly="">-->
                              
                               <label for="file1" class="field file"> 
                              <input type="file" id="import_file" name="import_file"  style="padding: 10px;border: 1px solid #ccc;width: 100%;margin-bottom: 20px;" />
                            </label>
                            </div>

                            </label>

                            </div> 


                            <div class="fixclear"></div>
                            <div style="text-align: left;">
                              <a href="../public/admin/sample/sample.xlsx" style="text-decoration: none;color: #626262;    margin-left: 11px;">Download sample xlsx file</a>
                            </div>

                              <button class="btn btn-primary btn-lg pull-right mt20" id="import_button">
                                <!-- <i class="fa fa-spinner fa-spin"></i> Loading -->
                                 Import File
                              </button>

                          </div>
                        </div>
                      </form>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

<script type="text/javascript">

  jQuery(document).ready(function() {
  
    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "This field contains alphabets only");

    jQuery.validator.addMethod("checkemail", function(value, element) 
    {
    return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
    }, "Enter a VALID email address"); 


    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#test").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        import_file: {
          required: true,
          extension: 'xls,xlsx',
        }
      },

      /* @validation error messages 
      ---------------------------------------------- */

      messages: {
        import_file: {
          extension: 'Please select an excel file-".xls" or ".xlsx"',
        }
      },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }


    });



  });


$('#test').submit(function(e) {  
      
    var fileName, fileExtension;
    fileName = $('#import_file').val();
    fileExtension = fileName.replace(/^.*\./, '');

    if (fileExtension == 'xls' || fileExtension == 'xlsx'){
     e.preventDefault();

     $('#import_button').html('<i class="fa fa-spinner fa-spin"></i> Loading ');
     $(this).ajaxSubmit({
          beforeSubmit: function() {
            $("#progress-bar").width('0%');
          },
          uploadProgress: function (event, position, total, percentComplete){ 
            $("#progress-bar").width(percentComplete + '%');
          },
          success:function (response){
              if(response.status == 1){
              $("#progress-bar").html('<div id="progress-status">' + 100 +' %</div>')
              $('#import_button').html('Import File');
              // window.location('/gullyyquiz-panel/view-question');
              window.location.href = "{{ url('trivialeague-panel/view-question') }}";
              }else if(response.status == 2){
              $("#progress-bar").html('<div id="progress-status">' + 100 +' %</div>')
              $('#import_button').html('Import File');
              $('#my_file_error').html('<div id="log_error" class="alert alert-danger"><i>Please use correct format or you can download sample Excel file from below link.</i></div>');
              } else {
              $("#progress-bar").html('<div id="progress-status">' + 100 +' %</div>')
              $('#import_button').html('Import File');
              $('#my_file_error').html('<div id="log_error" class="alert alert-danger"><i>Please use correct format or you can download sample Excel file from below link.</i></div>');
              }
          },
        });
    }
});


</script>

@endsection
