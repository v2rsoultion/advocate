@extends('advocate_admin/layout')
@section('content')

<style type="text/css">
  /*.admin-form .select, .admin-form .gui-input, .admin-form .select > select, .admin-form .select-multiple select{
    height: 28px !important;
  }*/
  .admin-form .append-icon .field-icon, .admin-form .prepend-icon .field-icon{
    line-height: 28px !important;
  }
  .admin-form .gui-textarea {
    line-height: 7px !important;
  }

  .form-control {
    height: 28px !important;
  }
  .admin-form .button{
    height: 28px !important;
    line-height: 1px !important;
  }
  .btn {
    height: 29px !important;
    line-height: 1px !important;  }

 .down{
  margin-top: 70px;
 }

</style>
<style type="text/css">

.select_outer{
  height: 30px !important;
}
.new_select{
  height: 30px !important;
  padding: 0px 10px !important;
  font-size: 12px !important;
}

.arrow_new{
  top: 2px !important;
}

.new_text{
  height: 30px !important;
  font-size: 12px !important;
}

.search_new{
  line-height: 32px !important;
}

.new_button{
  height: 30px !important;
  font-size: 12px !important;
  padding: 5px 10px; 
  line-height: 30px !important;
}

.new_button_2{
  height: 30px !important;
  font-size: 12px !important;
  padding: 5px 10px; 
}

.caretss {
    margin-left: 30% !important;
}

.mutbtnnn button{
  height: 30px !important;
  padding: 3px 10px !important; 
}

.admin-form .checkbox, .admin-form .radio{
  border: 0px solid !important;
  background: none !important;
}

.dataTables_filter{
  float: left;
}
 .close{
  margin-top: -6px !important;
  font-size: 30px !important;
  width: 30px !important;
}
</style>

<section id="content_wrapper">
    <header id="topbar" style="margin-top: 60px;">
      <div class="topbar-left">
        <ol class="breadcrumb">
          <li class="crumb-active">
            <a href="{{url('/advocate-panel/add-case-form')}}">Add Case</a>
          </li>
          <li class="crumb-icon">
            <a href="{{ url('advocate-panel/dashboard') }}">
              <span class="glyphicon glyphicon-home"></span>
            </a>
          </li>
          <li class="crumb-link">
            <a href="{{ url('advocate-panel/dashboard') }}">Home</a>
          </li>
          <li class="crumb-trail">View / Edit Case </li>
        </ol>
      </div>
    </header>

    <div class="" style="margin-top:10px;">
      <div class="col-md-12 mb30">
        <div class="panel panel-primary panel-border top mb35">  
          <div class="panel-heading">
            <div class="panel-title hidden-xs">
              <span class="glyphicon glyphicon-tasks"></span> View / Edit Case </div>
          </div>

          <div class="panel-menu admin-form theme-primary" style="padding: 5px 20px;">
            <div class="row">
              {!! Form::open(['url'=>'/advocate-panel/view-case-form','id'=>'form_add_question' ,'autocomplete'=>'off' ]) !!}

                <div class="col-md-2">
                    <label for="level" class="field prepend-icon select_outer" style="margin-top:10px !important;">
                      <select class=" form-control new_select" name="court_type" id="level" onchange="court_type_high(this.value)" style="color: black;">
                        <!-- <option value="">Court Type</option> -->
                            <option value="1" {{ $court_type == 1 ? 'selected="selected"' : '' }} >Trial Court</option>
                            <option value="2" {{ $court_type == 2 ? 'selected="selected"' : '' }} >High Court</option>
                            <option value="3" {{ $court_type == 3 ? 'selected="selected"' : '' }} >Both</option> 
                      </select>
                    </label>
                </div>



                <div class="col-md-2" id="court_name_hide">
                  <div class="" style="margin-top:10px !important;">
                      <label for="artist_state" class="field select select_outer">

                        <select class="form-control new_select" id="display_high_court" name="reg_court_id" style="color: black;">
                          <option value=''>Select Court</option>
                          @foreach($high_court as $high_courts) 
                            <option value="{{ $high_courts->court_id }}" {{ $high_courts->court_id == $reg_court_id ? 'selected="selected"' : '' }} > {{$high_courts->court_name }} </option>
                          @endforeach
                        </select>
                        <i class="arrow double arrow_new"></i>
                      </label>
                  </div>
                </div>


                <div class="col-md-2">
                    <label for="level" class="field prepend-icon select_outer" style="margin-top:10px !important;">
                      <select class=" form-control new_select" name="case_status" id="level" style="color: black;">
                        <!-- <option value="">Select Status</option> -->
                            <option value="1" {{ $case_status == 1 ? 'selected="selected"' : '' }} > Pending </option>
                            <option value="2" {{ $case_status == 2 ? 'selected="selected"' : '' }} > Disposal</option>
                            <option value="3" {{ $case_status == 3 ? 'selected="selected"' : '' }} >Both</option> 
                      </select>
                    </label>
                </div>

                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 10px;">
                    {!! Form::text('reg_file_no','',array('class' => 'form-control new_text','placeholder' => 'File No', 'autocomplete' => 'off' )) !!}
                    <label for="pincode" class="field-icon search_new">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>

                <div class="col-md-2">
                  <div class="" style="margin-top:10px !important;">
                    <label for="artist_state" class="field select select_outer">
                      <select class="form-control new_select" name="type" onchange="choose_sub_type(this.value)" style="color: black;">
                        <option value=''>Select Case Type</option> 
                        
                        @foreach($case_type_entry as $case_type_entrys) 
                        <option value="{{ $case_type_entrys->case_id }}" {{ $case_type_entrys->case_id == $reg_case_type_id ? 'selected="selected"' : '' }} >{{$case_type_entrys->case_name}} </option>
                        @endforeach
                        
                      </select>
                      <i class="arrow double arrow_new"></i>
                    </label>
                  </div>
                </div>

                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 10px;">
                    {!! Form::text('case_no','',array('class' => 'form-control new_text','placeholder' => 'Case No', 'autocomplete' => 'off' )) !!}
                    <label for="pincode" class="field-icon search_new">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>


                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 10px;">
                    {!! Form::text('petitioner_name','',array('class' => 'form-control new_text','placeholder' => 'Pet. Name', 'autocomplete' => 'off' )) !!}
                    <label for="pincode" class="field-icon search_new">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>

                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 10px;">
                    {!! Form::text('respondent','',array('class' => 'form-control new_text','placeholder' => 'Res. Name', 'autocomplete' => 'off' )) !!}
                    <label for="pincode" class="field-icon search_new">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>

                <div class="col-md-2" style="display: none" id="case_no_hide">
                  <div class="" style="margin-top:10px !important;">
                    <label for="artist_state" class="field select select_outer">
                      <select class="form-control new_select" id="hide_choose_sub_type_" name="reg_case_subtype_id" style="color: black;">
                        <option value=''> Select Class Code</option>

                        @foreach($case_sub_type as $case_sub_types) 
                          <option value="{{ $case_sub_types->classcode_id }}" {{ $case_sub_types->classcode_id == $reg_case_subtype_id ? 'selected="selected"' : '' }} >{{$case_sub_types->classcode_name }} </option>
                        @endforeach

                      </select>
                      <i class="arrow double arrow_new"></i>
                    </label>
                  </div>
                </div>


                <div class="col-md-2" style="display: none" id="year_hide">
                  <div class="" style="margin-top:10px !important;">
                      <label for="artist_state" class="field select select_outer">
                        <select class="form-control new_select" id="" name="case_year" style="color: black;">
                          <option value=''>Select Year</option>
                      
                          @php
                              $currentYear = date('Y');
                          @endphp

                          @foreach(range(1950, $currentYear) as $value)
                              <option value={{$value}} {{ $value == $case_year ? 'selected="selected"' : '' }} >{{$value}}</option>
                          @endforeach

                        </select>
                        <i class="arrow double arrow_new"></i>
                      </label>
                  </div>
                </div>

                <!-- <div class="clearfix"></div> -->
                <div class="col-md-2" style="display: none" id="ncv_no_hide">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 10px;">
                    {!! Form::text('ncv_no','',array('class' => 'form-control new_text','placeholder' => 'NCV No', 'autocomplete' => 'off' )) !!}
                    <label for="pincode" class="field-icon search_new">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>

                <div class="col-md-2" style="display: none" id="stage_hide">
                  <div class="" style="margin-top:10px !important;">
                      <label for="artist_state" class="field select select_outer">
                        <select class="form-control new_select" id="hide_choose_sub_type_" name="reg_stage_id" style="color: black;">
                          <option value=''>Select Stage</option>
                          @foreach($stage as $stages) 
                            <option value="{{ $stages->stage_id }}" {{ $stages->stage_id == $reg_stage_id ? 'selected="selected"' : '' }} >{{$stages->stage_name}} </option>
                          @endforeach
                        </select>
                        <i class="arrow double arrow_new"></i>
                      </label>
                  </div>
                </div>

                <div class="col-md-2" style="display: none" id="power_hide">
                  <div class="" style="margin-top:10px !important;">
                      <label for="artist_state" class="field select select_outer">
                        <select class="form-control new_select" id="" name="reg_power" style="color: black;">
                          <option value=''>Select Power</option>
                          
                          <option value="1" {{ $reg_power == 1 ? 'selected="selected"' : '' }}> Petitioner </option>
                          <option value="2" {{ $reg_power == 2 ? 'selected="selected"' : '' }}> Respondent </option>
                          <option value="3" {{ $reg_power == 3 ? 'selected="selected"' : '' }}> Caveat </option>
                          <option value="4" {{ $reg_power == 4 ? 'selected="selected"' : '' }}> None </option>

                        </select>
                        <i class="arrow double arrow_new"></i>
                      </label>
                  </div>
                </div>

                <div class="col-md-2" style="display: none"  id="client_hide">
                  <div class="" style="margin-top:10px !important;">
                      <label for="artist_state" class="field select select_outer">
                        <select class="form-control new_select" id="hide_choose_sub_type_" name="reg_client_group_id" onchange="sub_group_change_ajax(this.value)" style="color: black;">
                          <option value=''> Client Group/Clients</option>
                            
                            @foreach($client as $clients) 
                              <option value="{{ $clients->cl_id }}" {{ $clients->cl_id == $reg_client_group_id ? 'selected="selected"' : '' }} >{{$clients->cl_group_name}} @if($clients->cl_father_name != "") {{ $clients->cl_name_prefix }} {{ $clients->cl_father_name }} @endif </option>
                            @endforeach
                                   
                        </select>
                        <i class="arrow double arrow_new"></i>
                      </label>
                  </div>
                </div>

                <div class="col-md-2" style="display: none" id="show_client_sub_group">
                  <div class="" style="margin-top:10px !important;">
                      <label for="artist_state" class="field select select_outer">
                        
                        <select class="form-control new_select" id="sub_group_change" name="reg_client_subgroup_id" style="color: black;">
                          <option value=''>Select Client Sub Group</option>  
                          @foreach($sub_client as $sub_clients) 
                             <option value="{{ $sub_clients->sub_client_id }}" {{ $sub_clients->sub_client_id == $get_record[0]->reg_client_subgroup_id ? 'selected="selected"' : '' }}> {{$sub_clients->sub_client_name}} </option>
                           @endforeach                              
                        </select>

                        <i class="arrow double arrow_new"></i>
                      </label>
                  </div>
                </div>




                <div class="col-md-2" style="display: none" id="referred_hide">
                  <div class="" style="margin-top:10px !important;">
                      <label for="artist_state" class="field select select_outer">
                        <select class="form-control new_select" id="" name="reg_reffeerd_by_id" style="color: black;">
                          <option value=''>Select Referred By</option>
                          
                          @foreach($reffered as $reffereds) 
                            <option value="{{ $reffereds->ref_id }}" {{ $reffereds->ref_id == $reg_reffeerd_by_id ? 'selected="selected"' : '' }} >{{$reffereds->ref_advocate_name}} </option>
                          @endforeach
                                                              
                        </select>
                        <i class="arrow double arrow_new"></i>
                      </label>
                  </div>
                </div>

                <div class="col-md-2" style="display: none" id="assigned_hide">
                  <div class="" style="margin-top:10px !important;">
                      <label for="artist_state" class="field select select_outer">
                        <select class="form-control new_select" id="" name="reg_assigend_id" style="color: black;">
                          <option value=''> Select Assigned</option>
                            @foreach($assigned as $assigneds) 
                              <option value="{{ $assigneds->assign_id }}" {{ $assigneds->assign_id == $reg_assigend_id ? 'selected="selected"' : '' }} >{{$assigneds->assign_advocate_name}} </option>
                            @endforeach                        
                        </select>
                        <i class="arrow double arrow_new"></i>
                      </label>
                  </div>
                </div>

                <div class="col-md-2" style="display: none" id="act_hide">
                  <div class="" style="margin-top:10px !important;">
                      <label for="artist_state" class="field select select_outer">
                        <select class="form-control new_select" id="" name="reg_act_id" onchange="act_type_change_ajax(this.value)" style="color: black;">
                          
                          <option value=''> Select Act</option>
                          @foreach($act as $acts) 
                            <option value="{{ $acts->act_id }}" {{ $acts->act_id == $reg_act_id ? 'selected="selected"' : '' }} >{{$acts->act_name}} </option>
                          @endforeach
                        
                        </select>
                        <i class="arrow double arrow_new"></i>
                      </label>
                  </div>
                </div>

                <div class="col-md-2" style="display: none" id="section_hide">
                  <div class="" style="margin-top:10px !important;">
                    <label for="artist_state" class="field select select_outer" id="show_act_sub_type_section">
                      <select id="framework_3" name="section[]" multiple class="form-control">
                          @foreach($section as $sections) 

                          @php
                            $arr = explode(',', $reg_section_id);
                            $sel1 = '';
                            if ( in_array( $sections->section_id, $arr) ) {
                              $sel1 = 'selected="selected"';  
                            } 
                          @endphp

                            <option value="{{ $sections->section_id }}" {{ $sel1 }} >{{$sections->section_name}} </option>
                          @endforeach    
                      </select>
                      <i class="arrow double arrow_new"></i>
                    </label>
                  </div>
                </div>



                <div class="col-md-2" style="display: none" id="opposite_hide">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 10px;">
                    {!! Form::text('reg_opp_council','',array('class' => 'form-control  new_text','placeholder' => 'Opposite Counsel', 'autocomplete' => 'off' )) !!}
                    <label for="pincode" class="field-icon search_new">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>

                <div class="col-md-2" style="display: none" id="fir_no_hide">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 10px;">
                    {!! Form::text('reg_fir_id','',array('class' => 'form-control  new_text','placeholder' => 'Fir No', 'autocomplete' => 'off' )) !!}
                    <label for="pincode" class="field-icon search_new">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>




                <div class="col-md-2" style="display: none" id="fir_year_hide">
                  <div class="" style="margin-top:10px !important;">
                      <label for="artist_state" class="field select select_outer">
                        
                        <select class="form-control new_select" id="" name="reg_fir_year" style="color: black;">
                          <option value=''>Select FIR Year</option>              
                                  
                          @php
                              $currentYear = date('Y');
                          @endphp

                          @foreach(range(1950, $currentYear) as $value)
                              <option value={{$value}} {{ $value == $reg_fir_year ? 'selected="selected"' : '' }}> {{$value}} </option>
                          @endforeach                              
                        </select>

                        <i class="arrow double arrow_new"></i>
                      </label>
                  </div>
                </div>

                <div class="col-md-2" style="display: none" id="station_hide">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 10px;">
                    {!! Form::text('reg_police_thana','',array('class' => 'form-control  new_text','placeholder' => 'Police Station', 'autocomplete' => 'off' )) !!}
                    <label for="pincode" class="field-icon search_new">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>

                <div class="col-md-2" style="display: none" id="remark_hide">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 10px;">
                    {!! Form::text('reg_remark','',array('class' => 'form-control  new_text','placeholder' => 'Remark', 'autocomplete' => 'off' )) !!}
                    <label for="pincode" class="field-icon search_new">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>

                <div class="col-md-2" style="display: none" id="other_hide">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 10px;">
                    {!! Form::text('reg_other_field','',array('class' => 'form-control  new_text','placeholder' => 'Other Field', 'autocomplete' => 'off' )) !!}
                    <label for="pincode" class="field-icon search_new">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>


                <div class="col-md-2" style="display: none" id="from_date_hide">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 10px;">
                    {!! Form::text('from_date','',array('class' => 'form-control fromdate_search new_text','placeholder' => 'Reg. From Date', 'autocomplete' => 'off' )) !!}
                    <label for="pincode" class="field-icon search_new">
                      <i class="fa fa-calendar"></i>
                    </label>
                  </label>
                </div>

                <div class="col-md-2" style="display: none" id="to_date_hide">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 10px;">
                    {!! Form::text('to_date','',array('class' => 'form-control todate new_text','placeholder' => 'Reg. To Date', 'autocomplete' => 'off'  )) !!}
                    <label for="pincode" class="field-icon search_new">
                      <i class="fa fa-calendar"></i>
                    </label>
                  </label>
                </div>
                

                <div class="col-md-2" style="display: none" id="next_date_hide">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 10px;">
                    {!! Form::text('further_date','',array('class' => 'form-control fromdate_search new_text','placeholder' => 'Next Date', 'autocomplete' => 'off', 'id' => '' )) !!}
                    <label for="pincode" class="field-icon search_new">
                      <i class="fa fa-calendar"></i>
                    </label>
                  </label>
                </div>


                <div class="clearfix"></div>

                 

                <div class="col-md-3">
                  <div class="" style="margin-top:10px !important;">
                    <label for="artist_state" class="field select select_outer">
                      <select id="multiselect2" multiple="multiple" name="selected_value[]" onchange="selected_fields(this.value)" style="color: black;">
                          <option value="1">Class Code</option>
                          <option value="2">Case Year</option>
                          <option value="3">NCV No.</option>
                          <option value="4">Select Stage</option>
                          <option value="8">Power</option>
                          <option value="9">Client Group/Clients</option>
                          <option value="10"> Referred By </option>
                          <option value="11"> Assigned </option>
                          <option value="12"> Act </option>
                          <option value="13"> Section </option>
                          <option value="14"> Opposite Counsel </option>
                          <option value="15"> Fir No</option>
                          <option value="16"> Fir Year</option>
                          <option value="17"> Police Station </option>
                          <option value="18"> Remark </option>
                          <option value="19"> Other Field </option>
                          <option value="5">Reg. From Date</option>
                          <option value="6">Reg. To Date</option>
                          <option value="7">Next Date</option>
                        </select>
                    </label>
                  </div>
                </div>


                <div class="clearfix"></div>

                <div class="col-md-1 pull-right" style="margin-top:10px !important;  margin-right: 5px !important;">
                  <button type="submit" name="search" class="button btn-primary new_button"> Search </button>
                </div>
              {!! Form::close() !!}             
                <div class="col-md-1" style="margin-top:10px !important;">
                   <a href="{{ url('/advocate-panel/view-case-form/')}}">{!! Form::submit('Default', array('class' => 'btn btn-primary new_button_2', 'id' => 'maskedKey')) !!}</a>
                </div>

                <div class="col-md-2 mt10">
                   <a href="{{ url('/advocate-panel/view-case-form/all')}}">{!! Form::button('Show All Records', array('class' => 'btn btn-primary', 'id' => 'maskedKey')) !!}</a>
                </div>
                <div class="clearfix"></div>     
            </div>
          </div>


          <div class="panel-body pn">
              {!! Form::open(['url'=>'/advocate-panel/view-case-form','name'=>'form' ,'autocomplete'=>'off']) !!}
              <div class="table-responsive" style="overflow: scroll;">
                <table class="table admin-form table-bordered table-striped theme-warning tc-checkbox-1 fs13" id="datatable2">
                  <thead>
                    <tr class="bg-light">
                      <th style="width:90px !important;" class="text-left">
                        <label class="option block mn" style="width:90px !important;">
                          <input type="checkbox" id="selcheck" onclick="selectall()"> 
                          <span class="checkbox mn" style="border: 2px solid #4a89dc !important; background: #fff !important;"></span>
                          Select All
                        </label>
                      </th>
                      <th class="text-left">Edit </th>
                      <th class="text-left">Reg. Date</th>
                      <th class="text-left">File Number</th>
                      <!-- <th class="text-left">Court Type</th> -->
                      <th class="text-left">Case Type</th>
                      <th class="">Case Number</th>
                      <th class="">Petitioner</th>
                      <th class="">Respondent</th>
                      <th class="">Previous Date</th>
                      <th class="">Next Date</th>
                      <th class="text-right">Status</th>
                      <th class="">Class Code</th>
                      <th class="">NCV No.</th>
                      <th class="">Stage</th>
                      <th class="">Power</th>
                      <th class="">Client Group/Clients</th>
                      <th class="">Client Sub Group</th>
                      <th class="">Extra Petitioner Party</th>
                      <th class="">Extra Respondent Party</th>
                      <th class="">FIR No.</th>
                      <th class="">FIR Year</th>
                      <th class="">Police Station</th>
                      <th class="">Act</th>
                      <th class="">Section</th>
                      <th class="">Assigend</th>
                      <th class="">Refferd By</th>
                      <th class="">Opposite Counsel</th>
                      <th class="">Remark</th>
                      <th class="">Other Field</th>
                      <th class="">Document</th>
                      
                    </tr>
                  </thead>
                  <tbody>
                  @foreach($get_record as $get_records) 

                    <tr>
                      <td class="text-left" style="padding-left: 20px;">
                        <label class="option block mn">
                          <input type="checkbox" name="check[]" class="check" value="{{$get_records->reg_id}}" onclick="checkakk(this.value)">
                          <span class="checkbox mn" style="border: 2px solid #4a89dc !important; background: #fff !important;"></span>
                        </label>
                      </td>

                      <td style="padding-left: 20px;"> <a href="{{ url('/advocate-panel/add-case-form') }}/{{sha1($get_records->reg_id)}}"> <i class="fa fa-edit"></i> </a> </td>

                      <td class="text-left" style="padding-left: 20px;">  
                        @if($get_records->reg_date == "1970-01-01")
                          -----
                        @else
                          {{ date('d/m/Y',strtotime($get_records->reg_date)) }}
                        @endif             
                      </td>

                      <td class="" style="padding-left: 20px;"> <a href="{{ url('/advocate-panel/case-detail') }}/{{sha1($get_records->reg_id) }}"> @if($get_records->reg_file_no == "") ----- @else {{ $get_records->reg_file_no }} @endif </a> </td>

                      <!-- <td class="text-left" style="padding-left: 20px;">
                        @if($get_records->reg_court == 2)
                               High Court
                        @elseif($get_records->reg_court == 1)
                               Trial Court
                        @else 
                          -----
                        @endif

                      </td> -->

                      <td class="text-left" style="padding-left: 20px;"> @if($get_records->case_name == "") ----- @else {{ $get_records->case_name }} @endif </td>

                      <td class="text-left" style="padding-left: 20px;"> @if($get_records->reg_case_number == "") ----- @else {{ $get_records->reg_case_number }} @endif </td>

                      <td class="text-left" style="padding-left: 20px;"> @if($get_records->reg_petitioner == "") ----- @else {{ $get_records->reg_petitioner }} @endif  </td>
                      <td class="text-left" style="padding-left: 20px;"> @if($get_records->reg_respondent == "") ----- @else {{ $get_records->reg_respondent }} @endif  </td>


                      @php
                        $prev_date =  \App\Model\Pessi\Pessi::where(['pessi_case_reg' => $get_records->reg_id , 'pessi_status' => 1 ])->first();
                      @endphp

                      <td class="text-left" style="padding-left: 20px;">  
                        @if($prev_date->pessi_prev_date == "1970-01-01" || $prev_date->pessi_prev_date == "")
                          -----
                        @else
                          {{ date('d F Y',strtotime($prev_date->pessi_prev_date)) }}
                        @endif             
                      </td>

                      <td class="text-left" style="padding-left: 20px;">  
                        @if($get_records->pessi_further_date == "1970-01-01")
                          -----
                        @else
                          {{ date('d F Y',strtotime($get_records->pessi_further_date)) }}
                        @endif             
                      </td>

                      <td class="text-left">  
                        
                        @if($get_records->pessi_choose_type == 0)
                          
                          <a href="{{ url('/advocate-panel/case-detail') }}/{{sha1($get_records->reg_id)}}">
                            <button type="button" class="btn btn-info br2 btn-xs fs12 dropdown-toggle"> Pending </button> 
                          </a>

                        @elseif($get_records->pessi_choose_type == 1)
                          
                          <a href="{{ url('/advocate-panel/case-detail') }}/{{sha1($get_records->reg_id)}}">
                            <button type="button" class="btn btn-success br2 btn-xs fs12 dropdown-toggle"> Disposal </button>
                          </a>

                        @else

                          <a href="{{ url('/advocate-panel/case-detail') }}/{{sha1($get_records->reg_id)}}">
                            <button type="button" class="btn btn-success br2 btn-xs fs12 dropdown-toggle"> Due Course </button>
                          </a>

                        @endif

                      </td>

                      <td class="text-left" style="padding-left: 20px;"> @if($get_records->classcode_name == "") ----- @else {{ $get_records->classcode_name }} @endif </td>

                      <td class="text-left" style="padding-left: 20px;"> @if($get_records->reg_vcn_number == "") ----- @else {{ $get_records->reg_vcn_number }} @endif </td>

                      <td class="text-left" style="padding-left: 20px;"> @if($get_records->stage_name == "") ----- @else {{ $get_records->stage_name }} @endif </td>

                      <td class="text-left" style="padding-left: 20px;"> 
                        @if($get_records->reg_power == 1)
                            P
                            @elseif($get_records->reg_power == 2)
                            R
                            @elseif($get_records->reg_power == 3)
                            C
                            @elseif($get_records->reg_power == 4)
                            N    
                        @endif
                      </td>

                      <td class="text-left" style="padding-left: 20px;"> @if($get_records->cl_group_name == "") ----- @else {{ $get_records->cl_group_name }} @if($get_records->cl_father_name != "") {{ $get_records->cl_name_prefix }} {{ $get_records->cl_father_name }} @endif @endif </td>

                      <td class="text-left" style="padding-left: 20px;"> @if($get_records->sub_client_name == "") ----- @else {{ $get_records->sub_client_name }} @endif </td>
                      
                      <td class="" style="padding-left: 20px;">


                        <button type="button" class="btn btn-primary view" onclick="view_image3({{$get_records->reg_id}})" >
                          <a href="#" style="text-transform: capitalize; text-decoration:none;"  > View </a>
                        </button>


                          <div id="view_image3{{$get_records->reg_id}}" class="modal fade in" role="dialog" style="overflow: scroll;">
                          <div class="modal-dialog" style="margin-top:80px;">
                            <div class="modal-content">
                              <div class="modal-header" style="padding-bottom: 35px;">
                                <button type="button" class="close" data-dismiss="modal" onclick="close_button_image3({{$get_records->reg_id}})" >&times;</button>
                                  <h4 class="modal-title pull-left">
                                     Extra Petitioner Party
                                  </h4>
                              </div>
                              <div class="">
                                <table class="table admin-form theme-warning tc-checkbox-1  fs13" id="datatable">
                                  <thead>
                                    <tr>
                                      <th class="text-left">Name</th>
                                      <th class="text-left">Prefix</th>
                                      <th class="text-left">Guardian Name</th>
                                      <th class="text-left">Address</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    @php
                                      $reg_petitioner = json_decode($get_records->reg_petitioner_extra,true);
                                    @endphp

                                    @if($reg_petitioner != "" )

                                    
                                    @foreach($reg_petitioner as $reg_petitioners)
                                      
                                      <tr>
                                        <td class="text-left" style="padding-left: 20px;"> @if($reg_petitioners[petitioner_name] != "") {{ $reg_petitioners[petitioner_name] }} @else ----- @endif </td>
                                        <td class="text-left" style="padding-left: 20px;"> @if($reg_petitioners[petitioner_prefix] != "") {{ $reg_petitioners[petitioner_prefix] }} @else ----- @endif </td>
                                        <td class="text-left" style="padding-left: 20px;"> @if($reg_petitioners[petitioner_father_name] != "") {{ $reg_petitioners[petitioner_father_name] }} @else ----- @endif </td>
                                        <td class="text-left" style="padding-left: 20px;"> @if($reg_petitioners[petition_address] != "") {{ $reg_petitioners[petition_address] }} @else ----- @endif </td>
                                      </tr>
                                      
                                    @endforeach

                                    @else

                                      <tr>
                                        <td colspan="3">No Record Found !!</td>
                                      </tr>

                                    @endif


                                  </tbody>

                                </table>
                              </div>
                              <div class="clearfix"></div>
                            </div> 
                          </div>
                        </div>
                      </td>

                      <td class="" style="padding-left: 20px;">
                        <button type="button" class="btn btn-primary view" onclick="view_image1({{$get_records->reg_id}})" >
                          <a href="#" style="text-transform: capitalize; text-decoration:none;"  > View </a>
                         </button>
                          <div id="view_image1{{$get_records->reg_id}}" class="modal fade in" role="dialog" style="overflow: scroll;">
                          <div class="modal-dialog" style="margin-top:80px;">
                            <div class="modal-content">
                              <div class="modal-header" style="padding-bottom: 35px;">
                                <button type="button" class="close" data-dismiss="modal" onclick="close_button_image1({{$get_records->reg_id}})" >&times;</button>
                                  <h4 class="modal-title pull-left">
                                     Extra Respondent Party
                                  </h4>
                              </div>
                              <div class="">
                                <table class="table admin-form theme-warning tc-checkbox-1  fs13" id="datatable">
                                  <thead>
                                    <tr>
                                      <th class="text-left">Name</th>
                                      <th class="text-left">Prefix</th>
                                      <th class="text-left">Guardian Name</th>
                                      <th class="text-left">Address</th>
                                    </tr>
                                </thead>
                                <tbody>
                              
                                @php
                                  $respondent_extra = json_decode($get_records->reg_respondent_extra,true);
                                @endphp

                                 @if($respondent_extra != "" )

                                  @foreach($respondent_extra as $respondent_extras)
                                    <tr class="json_view">
                                      <td class="text-left" style="padding-left: 20px;"> @if($respondent_extras[respondent_name] != "") {{ $respondent_extras[respondent_name] }} @else ----- @endif </td>
                                      <td class="text-left" style="padding-left: 20px;"> @if($respondent_extras[respondent_prefix] != "") {{ $respondent_extras[respondent_prefix] }} @else ----- @endif </td>
                                      <td class="text-left" style="padding-left: 20px;"> @if($respondent_extras[respondent_father_name] != "") {{ $respondent_extras[respondent_father_name] }} @else ----- @endif </td>
                                      <td class="text-left" style="padding-left: 20px;"> @if($respondent_extras[respondent_address] != "") {{ $respondent_extras[respondent_address] }} @else ----- @endif </td>
                                    </tr>
                                  @endforeach

                                @else

                                  <tr>
                                    <td colspan="3">No Record Found !!</td>
                                  </tr>

                                @endif

                                  </tbody>
                              </table>
                              </div>
                              <div class="clearfix"></div>
                            </div> 
                          </div>
                        </div>
                      </td>

                      <td class="text-left" style="padding-left: 20px;"> @if($get_records->reg_fir_id == "") ----- @else {{ $get_records->reg_fir_id }} @endif </td>

                      <td class="text-left" style="padding-left: 20px;"> @if($get_records->reg_fir_year == "") ----- @else {{ $get_records->reg_fir_year }} @endif </td>

                      <td class="text-left" style="padding-left: 20px;"> @if($get_records->reg_police_thana   == "") ----- @else {{ $get_records->reg_police_thana  }} @endif </td>

                      <td class="text-left" style="padding-left: 20px;"> @if($get_records->act_name == "") ----- @else {{ $get_records->act_name }} @endif </td>

                      @php
                        $section_name  = "";
                        $get_records->reg_section_id = explode(',', $get_records->reg_section_id);
                        $section =  \App\Model\Section\Section::whereIn('section_id', $get_records->reg_section_id )->where('section_name', '!=' ,'')->get();

                        foreach($section as $sections){
                          $section_name .= $sections->section_name.' , ';
                        }

                        $section_name = substr($section_name,0,-2);
                      @endphp

                      <td class="text-left" style="padding-left: 20px;"> @if($section_name == "") ----- @else {{ $section_name }} @endif </td>

                      <td class="text-left" style="padding-left: 20px;"> @if($get_records->assign_advocate_name == "") ----- @else {{ $get_records->assign_advocate_name }} @endif </td>

                      <td class="text-left" style="padding-left: 20px;"> @if($get_records->ref_advocate_name == "") ----- @else {{ $get_records->ref_advocate_name }} @endif </td>

                      <td class="text-left" style="padding-left: 20px;"> @if($get_records->reg_opp_council == "") ----- @else {{ $get_records->reg_opp_council }} @endif </td>

                      <td class="text-left" style="padding-left: 20px;"> @if($get_records->reg_remark == "") ----- @else {{ substr($get_records->reg_remark, 0,15) }}..... @endif  </td>

                      <td class="text-left" style="padding-left: 20px;"> @if($get_records->reg_other_field == "") ----- @else {{ substr($get_records->reg_other_field, 0,15) }}..... @endif </td>

                      <td class="" style="padding-left: 20px;">
                        <button type="button" class="btn btn-primary view" onclick="document_image3({{$get_records->reg_id}})" >
                          <a href="#" style="text-transform: capitalize; text-decoration:none;"> View </a>
                         </button>
                          <div id="document_image3{{$get_records->reg_id}}" class="modal fade in" role="dialog" style="overflow: scroll;">
                          <div class="modal-dialog" style="margin-top:80px;">
                            <div class="modal-content">
                              <div class="modal-header" style="padding-bottom: 35px;">
                                <button type="button" class="close" data-dismiss="modal" onclick="close_button_document({{$get_records->reg_id}})" >&times;</button>
                                  <h4 class="modal-title pull-left">
                                    View Documents
                                  </h4>
                              </div>
                              <div class="">
                                
                                @php
                                  $images_cat =  \App\Model\Upload_Document\Upload_Document::where(['upload_case_id' => $get_records->reg_id ])->get();
                                @endphp
                              
                                @if(count($images_cat) != 0)
                                  @foreach($images_cat as $images_cats) 
                                    <div class="col-md-3" style="margin-bottom: 22px;position: relative;" id="cat_img{{$images_cats['upload_id']}}">
                                    
                                      {!! Html::image($images_cats['upload_images'], '', array('class' => 'media-object mw150 customer_profile', 'width'=>'100%', 'height'=>'150')) !!}
                                    </div>
                                  @endforeach
                                @else 
                                  <div style="margin: 20px auto;" class="text-center" > No Documents Available </div>
                                @endif

                              </div>
                              <div class="clearfix"></div>
                            </div> 
                          </div>
                        </div>
                      </td>
                      
                      
                      <!-- <td class="text-right">
                        <div class="btn-group text-left">
                          <button type="button" class="btn {{ $get_records->reg_status == 1 ? 'btn-info' : 'btn-success' }}  br2 btn-xs fs12 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> {{ $get_records->reg_status == 1 ? 'In Progress' : 'Disposal' }}
                            <span class="caret ml5"></span>
                          </button>
                          <ul class="dropdown-menu" role="menu" style="min-width:130px; left: {{ $get_records->reg_status == 1 ? '-38px' : '-54px' }} !important;">
                            <li>
                              <a href="{{ url('/advocate-panel/add-case-registration-form') }}/{{sha1($get_records->reg_id)}}">Edit</a>
                            </li>
                            <div class="divider"></div>                            
                            <li class="">
                              <a href="{{ url('/advocate-panel/case-registration-detail') }}/{{sha1($get_records->reg_id)}}">View Details</a>
                            </li>
                            <li class="{{ $get_records->reg_status == 1 ? 'active' : '' }}">
                              <a href="{{ url('/advocate-panel/change-case-registration-form-status') }}/{{sha1($get_records->reg_id)}}/1">In Progress</a>
                            </li>
                            <li class=" {{ $get_records->reg_status == 2 ? 'active' : '' }} ">
                              <a href="{{ url('/advocate-panel/change-case-registration-form-status') }}/{{sha1($get_records->reg_id)}}/2">Disposal</a>
                            </li>
                          </ul>
                        </div>
                      </td>  -->
                    </tr>
                  @endforeach()
                  </tbody>
                </table>
              </div>
              {!! Form::close() !!}
          </div>

          <div class="panel-body pn">
            <div class="table-responsive">
              <table class="table admin-form theme-warning tc-checkbox-1 fs13">                                
                <tbody>
                  <tr class="">
                     <th class="text-left">
                      <button type="button" class="btn btn-primary" onclick="go_delete()"><i class="glyphicon glyphicon-trash"></i> Delete Multiple </button>
                    </th>
                    <th class="text-right">
                      {{ $get_record->links() }}
                    </th>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


          

<style type="text/css">

.multiselect-container.dropdown-menu{
  overflow: scroll;
  max-height: 300px;
}

</style>


<script type="text/javascript">

  // court_type_high

  function court_type_high(court_type) {

    BASE_URL = '{{ url('/')}}';
      $.ajax({
        url:BASE_URL+"/advocate-panel/get-all-court/"+court_type,
        success: function(result){
            $("#display_high_court").html(result);
        }
      });

  }


  // choose_sub_type

  function choose_sub_type(client_id){

    
      BASE_URL = '{{ url('/')}}';
      $.ajax({
        url:BASE_URL+"/advocate-panel/change-client-sub-type-ajax/"+client_id,
        success: function(result){
            $("#hide_choose_sub_type").html(result);
        }
      });

    if (client_id != ''){  
      $.ajax({
        url:BASE_URL+"/advocate-panel/get-case-category/"+client_id,
        success: function(result){
            if(result.status == true){
              $("#reg_case_type_category").val(1);
              document.getElementById("fir_show").style.display = "block";
            } else {
              $("#reg_case_type_category").val(2);
              document.getElementById("fir_show").style.display = "none";
            }
        }
      });
    }
  }


  function act_type_change_ajax(act_type_id){

  if (act_type_id != ''){
    BASE_URL = '{{ url('/')}}';
    $.ajax({
      url:BASE_URL+"/advocate-panel/change-act-sub-type-section-ajax/"+act_type_id,
      success: function(result){
          $("#show_act_sub_type_section").html(result); 
      }
    });
  }

}


  function sub_group_change_ajax(sub_group_change_id){

  if (sub_group_change_id != ''){
    BASE_URL = '{{ url('/')}}';
    $.ajax({
      url:BASE_URL+"/advocate-panel/change-sub-group-ajax/"+sub_group_change_id,
      success: function(result){
          
          if(result.status == true){
            document.getElementById("show_client_sub_group").style.display = "none";
          } else {
            document.getElementById("show_client_sub_group").style.display = "block";
            $("#sub_group_change").html(result); 
          }

      }
    });

  } else {
    document.getElementById("show_client_sub_group").style.display = "none";
  }

}


  function selected_fields(){


    var new_selected = $("#multiselect2").val();

    if(new_selected == null){
      $('#case_no_hide').css('display','none');
      $('#year_hide').css('display','none');      
      $('#ncv_no_hide').css('display','none');
      $('#stage_hide').css('display','none');
      $('#from_date_hide').css('display','none');
      $('#to_date_hide').css('display','none');
      $('#next_date_hide').css('display','none');
      $('#power_hide').css('display','none');
      $('#client_hide').css('display','none');
      $('#referred_hide').css('display','none');
      $('#assigned_hide').css('display','none');
      $('#act_hide').css('display','none');
      $('#section_hide').css('display','none');
      $('#opposite_hide').css('display','none');
      $('#fir_no_hide').css('display','none');
      $('#fir_year_hide').css('display','none');
      $('#station_hide').css('display','none');
      $('#remark_hide').css('display','none');
      $('#other_hide').css('display','none');
    }    

    var selected = $("#multiselect2").val().toString();


    if(selected != null){
      var values   = selected.split(",");      
    }
    

    values.forEach(function(element_new) {

      if(element_new != 1){  
        $('#case_no_hide').css('display','none');  
      }

      if(element_new != 2){
        $('#year_hide').css('display','none');
      }

      if(element_new != 3){
        $('#ncv_no_hide').css('display','none');
      }

      if(element_new != 4){
        $('#stage_hide').css('display','none');
      }

      if(element_new != 5){
        $('#from_date_hide').css('display','none');
      }

      if(element_new != 6){
        $('#to_date_hide').css('display','none');
      }

      if(element_new != 7){
        $('#next_date_hide').css('display','none');
      }

      if(element_new != 8){
        $('#power_hide').css('display','none');
      }

      if(element_new != 9){
        $('#client_hide').css('display','none');
      }

      if(element_new != 10){
        $('#referred_hide').css('display','none');
      }

      if(element_new != 11){
        $('#assigned_hide').css('display','none');
      }

      if(element_new != 12){
        $('#act_hide').css('display','none');
      }

      if(element_new != 13){
        $('#section_hide').css('display','none');
      }

      if(element_new != 14){
        $('#opposite_hide').css('display','none');
      }

      if(element_new != 15){
        $('#fir_no_hide').css('display','none');
      }

      if(element_new != 16){
        $('#fir_year_hide').css('display','none');
      }

      if(element_new != 17){
        $('#station_hide').css('display','none');
      }

      if(element_new != 18){
        $('#remark_hide').css('display','none');
      }

      if(element_new != 19){
        $('#other_hide').css('display','none');
      }

    });  


    values.forEach(function(element) {

      if(element == 1){  
        $('#case_no_hide').css('display','block');  
      }

      if(element == 2){
        $('#year_hide').css('display','block');
      }

      if(element == 3){
        $('#ncv_no_hide').css('display','block');
      }

      if(element == 4){
        $('#stage_hide').css('display','block');
      }

      if(element == 5){
        $('#from_date_hide').css('display','block');
      }

      if(element == 6){
        $('#to_date_hide').css('display','block');
      }

      if(element == 7){
        $('#next_date_hide').css('display','block');
      }

      if(element == 8){
        $('#power_hide').css('display','block');
      }

      if(element == 9){
        $('#client_hide').css('display','block');
      }

      if(element == 10){
        $('#referred_hide').css('display','block');
      }

      if(element == 11){
        $('#assigned_hide').css('display','block');
      }

      if(element == 12){
        $('#act_hide').css('display','block');
      }

      if(element == 13){
        $('#section_hide').css('display','block');
      }

      if(element == 14){
        $('#opposite_hide').css('display','block');
      }

      if(element == 15){
        $('#fir_no_hide').css('display','block');
      }

      if(element == 16){
        $('#fir_year_hide').css('display','block');
      }

      if(element == 17){
        $('#station_hide').css('display','block');
      }

      if(element == 18){
        $('#remark_hide').css('display','block');
      }

      if(element == 19){
        $('#other_hide').css('display','block');
      }

    });
  

  }
  




</script>
<style type="text/css">
.view a{
  color:#fff !important;
}
</style>
<style type="text/css">
.dt-panelfooter{
  display: none !important;
}

.btn.multiselect .caret{
    display: none;
  }
  .multiselect{
    text-align: left;
  }
  .input-group-addon {
    min-width: 30px;
    padding: 5px 6px;
  }
  .multiselect-search{
    height: 29px !important;
  }
  .dt-panelmenu{
    background-color: white;
  }
</style>

 <script type="text/javascript">
  jQuery(document).ready(function() {

    $('#framework_3').multiselect({
      nonSelectedText: 'Select Section',
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
      buttonWidth:'400px'
     });


    "use strict";

    // Init Theme Core    
  //  Core.init();

    // Init Demo JS  
 //   Demo.init();

    $('#datatable2').dataTable({
      "aoColumnDefs": [{
        'bSortable': false,
        'aTargets': [-1]
      }],
      "oLanguage": {
        "oPaginate": {
          "sPrevious": "",
          "sNext": ""
        }
      },
      "iDisplayLength": 25,
      // "aLengthMenu": [
      //   [5, 10, 25, 50, -1],
      //   [5, 10, 25, 50, "All"]
      // ],
      "sDom": '<"dt-panelmenu clearfix"lfr>t<"dt-panelfooter clearfix"ip>',
      "oTableTools": {
        "sSwfPath": "vendor/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
      }
    });

    $('.dataTables_filter input').attr("placeholder", "Enter Terms...");
    $('div.dataTables_filter input').focus();
    $('#datatable2_length').hide();

  });


  function view_image1(reg_id){
    $('body').css('overflow','hidden');
    $("html, body").animate({ scrollTop: 0 }, "slow");
    document.getElementById('view_image1'+reg_id).style.display='block';
  }

  function close_button_image1(reg_id){
    $('body').css('overflow','auto', 'important');
    document.getElementById('view_image1'+reg_id).style.display='none';
  }


  function view_image3(reg_id){
    $('body').css('overflow','hidden');
    $("html, body").animate({ scrollTop: 0 }, "slow");
    document.getElementById('view_image3'+reg_id).style.display='block';
  }

  function close_button_image3(reg_id){
    $('body').css('overflow','auto', 'important');
    document.getElementById('view_image3'+reg_id).style.display='none';
  }


  function document_image3(reg_id){
    $('body').css('overflow','hidden');
    $("html, body").animate({ scrollTop: 0 }, "slow");
    document.getElementById('document_image3'+reg_id).style.display='block';
  }

  function close_button_document(reg_id){
    $('body').css('overflow','auto', 'important');
    document.getElementById('document_image3'+reg_id).style.display='none';
  }

  </script>

@endsection