@extends('advocate_admin/layout')
@section('content')

  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="{{ url('/advocate-panel/view-user') }}">View User</a>
            </li>
            <li class="crumb-icon">
              <a href="{{ url('advocate-panel/dashboard') }}">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="{{ url('advocate-panel/dashboard') }}">Home</a>
            </li>
            <li class="crumb-trail">Add User</li>
          </ol>
        </div>
      </header>

      <!-- Begin: Content -->

      <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center">
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-heading">
                <span class="panel-title"> Add User </span>
              </div>
              <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                
                <!-- IF any error found -->
                <div class="col-md-12">
                  @if (\Session::has('success'))
                    <div class="alert alert-danger">
                      {!! \Session::get('success') !!}
                    </div>
                  @endif
                </div>
                
                <!-- IF any error found -->
                  <div id="tab1_1" class="tab-pane active">
                    {!! Form::open(['url'=>'advocate-panel/insert-user/'.$get_record->admin_id,'id'=>'validation' ,  'enctype' => 'multipart/form-data' ,'autocomplete'=>'off' ]) !!}
                    <div class="row"> 
                      <div class="col-md-8">
                      
                        <div class="col-md-12">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Name :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              {!! Form::text('name',$get_record->admin_name,array('class' => 'gui-input','placeholder' => '' )) !!}
                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-pencil"></i>
                              </label>                            
                            </label>
                          </div>
                          </div>
                        <div class="col-md-12">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Email Id :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              {!! Form::text('email',$get_record->admin_email,array('class' => 'gui-input','placeholder' => '', $get_record  == "" ? '' : 'disabled'  )) !!}
                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-envelope"></i>
                              </label>                           
                            </label>
                          </div>
                          </div>
                          
                          <div class="col-md-12">
                            <div class="section">
                              <label for="level_name" class="field-label" style="font-weight:600;" >Mobile No :  </label>  
                              <label for="level_name" class="field prepend-icon">
                                {!! Form::text('number',$get_record->admin_number,array('class' => 'gui-input numeric','placeholder' => '' )) !!}
                                  <label for="Account Mobile" class="field-icon">
                                  <i class="fa fa-phone"></i>
                                </label>                           
                              </label>
                            </div>
                          </div>
                          

                          @if($get_record == "")
                          <div class="col-md-12">
                            <div class="section">
                              <label for="level_name" class="field-label" style="font-weight:600;" >Password :  </label>  
                              <label for="level_name" class="field prepend-icon">
                                {!! Form::text('password',$get_record->admin_number,array('class' => 'gui-input','placeholder' => '' )) !!}
                                  <label for="Account Mobile" class="field-icon">
                                  <i class="fa fa-lock"></i>
                                </label>                           
                              </label>
                            </div>
                          </div>
                          @endif

                          </div>
                      
                      
                          <div class="col-md-4" style="  margin-top: 25px;">
                            <div class="fileupload fileupload-new admin-form" data-provides="fileupload">

                            @if($get_record['admin_image'] != "")

                              <div class="fileupload-preview thumbnail mb15" style="line-height: 136px !important; height: 200px !important;">
                                {!! Html::image($get_record['admin_image'], '100%x147') !!}
                              </div>
                              <span class="button btn-system btn-file btn-block ph5" style="margin-bottom:10px">
                                <span class="fileupload-new">Update Profile</span>
                                <span class="fileupload-exists">Update Profile</span>
                                {!! Form::file('admin_image') !!}
                              </span>

                            @else

                              <div class="fileupload-preview thumbnail mb15" style="padding: 3px;outline: 1px dashed #d9d9d9;height: 200px !important;">
                                <img data-src="holder.js/100%x200" alt="100%x200" src="" data-holder-rendered="true"  style="height: 200px !important; width: 100%; display: block;">
                              </div>
                              <span class="button btn-system btn-file btn-block ph5" style="margin-bottom:10px">
                                <span class="fileupload-new" >Photo Upload</span>
                                <span class="fileupload-exists">Photo Upload</span>
                                {!! Form::file('admin_image') !!}
                              </span>

                            @endif

                            </div>
                        </div>
                      </div>
              
                  <div class="panel-footer text-right">
                    {!! Form::submit('Save', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}
                    {!! Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}
                  </div>
                  {!! Form::close() !!}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


<script type="text/javascript">

  jQuery(document).ready(function() {
  
    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "This field contains alphabets only");

    jQuery.validator.addMethod("checkemail", function(value, element) 
    {
    return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
    }, "Enter a VALID email address"); 

    $.validator.addMethod('minStrict', function (value, el, param) {
     return value > param; 
    });
    $.validator.addMethod('maxStrict', function (value, el, param) {
     return value < param; 
    });


    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#validation").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        name: {
          required: true,
          lettersonly: true,
        },
        email: {
          required: true,
          checkemail: true
        },
        number: {
          required: true,
          minlength: 10,
          maxlength: 10,
        },
        password: {
          required: true,
          minlength: 6,
        },
        admin_image: {
        // //  required: true,
          extension: 'jpeg,jpg,png',
        //   type: 'image/jpeg,image/png',
        // //  maxSize: 2097152,   // 2048 * 1024
       //    message: 'The selected file is not valid'
         },
      },

      /* @validation error messages 
      ---------------------------------------------- */

      messages: {
        name: {
          required: 'Please enter name'
        },
        email: {
          required: 'Please enter Email Id'
        },
        number: {
          required: 'Please enter mobile no',
          minlength:'Enter at least 10 digit phone numbers',
          maxlength:'Enter 10 digit phone numbers',
        },        
        admin_image: {
          extension: 'Image Should be in .jpg,.jpeg and .png format only'
        },
        password:{
          required: 'Please enter password',
          minlength:'Please enter at least 6 characters'
        },
      },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });

  </script>



@endsection
