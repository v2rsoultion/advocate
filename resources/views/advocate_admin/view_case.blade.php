@extends('advocate_admin/layout')
@section('content')

  <!-- Start: Content-Wrapper -->
  <section id="content_wrapper">
    <header id="topbar">
      <div class="topbar-left">
        <ol class="breadcrumb">
          <li class="crumb-active">
            <a href="{{url('/advocate-panel/add-case')}}">Add Case Type</a>
          </li>
          <li class="crumb-icon">
            <a href="{{ url('advocate-panel/dashboard') }}">
              <span class="glyphicon glyphicon-home"></span>
            </a>
          </li>
          <li class="crumb-link">
            <a href="{{ url('advocate-panel/dashboard') }}">Home</a>
          </li>
          <li class="crumb-trail">View Case Type </li>
        </ol>
      </div>
    </header>

    <div class="" style="margin-top:10px;">
      <div class="col-md-12">
        <div class="panel panel-primary panel-border top mb35">  
          <div class="panel-heading">
            <div class="panel-title hidden-xs">
              <span class="glyphicon glyphicon-tasks"></span> View Case Type</div>
          </div>

          <div class="panel-menu admin-form theme-primary">
            <div class="row">
              {!! Form::open(['url'=>'/advocate-panel/view-case' ,'autocomplete'=>'off']) !!}

                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon">
                    {!! Form::text('case_name','',array('class' => 'form-control ','placeholder' => 'Type Name', 'autocomplete' => 'off' )) !!}
                    <label for="pincode" class="field-icon">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>
                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon">
                    {!! Form::text('case_short_name','',array('class' => 'form-control ','placeholder' => 'Short Name', 'autocomplete' => 'off' )) !!}
                    <label for="pincode" class="field-icon">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>
                <div class="col-md-3">
                    <label for="level" class="field prepend-icon">  
                      <select class=" form-control" name="case_category" id="level" >
                        <option value="">Choose Case Category</option>
                            <option value="1" {{ $case_category == 1 ? 'selected="selected"' : '' }} >Criminal</option>
                            <option value="2" {{ $case_category == 2 ? 'selected="selected"' : '' }} >Civil</option> 
                      </select>
                    </label>
                </div>

                <div class="col-md-1">
                  <button type="submit" name="search" class="button btn-primary"> Search </button>
                </div>
              {!! Form::close() !!}             
                <div class="col-md-1 pull-right">
                   <a href="{{ url('/advocate-panel/view-case/')}}">{!! Form::submit('Default', array('class' => 'btn btn-primary', 'id' => 'maskedKey')) !!}</a>
                </div>     
            </div>
          </div>

          <div class="panel-body pn">
              {!! Form::open(['url'=>'/advocate-panel/view-case','name'=>'form' ,'autocomplete'=>'off']) !!}
              <div class="table-responsive">
                <table class="table admin-form theme-warning tc-checkbox-1 fs13" id="datatable">
                  <thead>
                    <tr class="bg-light">
                      <th style="width:90px !important;" class="text-left">
                        <label class="option block mn">
                          <input type="checkbox" id="check_all"> 
                          <span class="checkbox mn"></span>
                          Select All
                        </label>
                      </th>
                      <th class=""> Case Category</th>
                      <th class=""> Type Name</th>
                      <th class=""> Short Name</th>
                      <th class="text-right"> Status</th>
                    </tr>
                  </thead>
                  <tbody>
                  @foreach($get_record as $get_records)               
                    <tr>
                      <td class="text-left" style="padding-left: 18px;">
                        <label class="option block mn">
                          <input type="checkbox" name="check[]" class="check" value="{{$get_records->case_id}}">
                          <span class="checkbox mn"></span>
                        </label>
                      </td>
                      <td class="text-left" style="padding-left:20px">
                        @if($get_records->case_category == 1)
                               Criminal
                        @elseif($get_records->case_category == 2)
                               Civil
                        @endif
                      </td>
                      <td class="" style="padding-left:20px"> {{$get_records->case_name}} </td>
                      <td class="text-left" style="padding-left:20px"> {{$get_records->case_short_name}} </td>
                      
                      <td class="text-right">
                        <div class="btn-group text-left">
                          <button type="button" class="btn {{ $get_records->case_status == 1 ? 'btn-success' : 'btn-danger' }}  br2 btn-xs fs12 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> {{ $get_records->case_status == 1 ? 'Active' : 'Deactive' }}
                            <span class="caret ml5"></span>
                          </button>
                          <ul class="dropdown-menu" role="menu" style="min-width:130px; left: {{ $get_records->case_status == 1 ? '-67px' : '-54px' }} !important;">
                            <li>
                              <a href="{{ url('/advocate-panel/add-case') }}/{{sha1($get_records->case_id)}}">Edit</a>
                            </li>
                            <div class="divider"></div>                            
                            <li class="{{ $get_records->case_status == 1 ? 'active' : '' }}">
                              <a href="{{ url('/advocate-panel/change-case-type-status') }}/{{sha1($get_records->case_id)}}/1">Active</a>
                            </li>
                            <li class=" {{ $get_records->case_status == 0 ? 'active' : '' }} ">
                              <a href="{{ url('/advocate-panel/change-case-type-status') }}/{{sha1($get_records->case_id)}}/0">Deactive</a>
                            </li>
                          </ul>
                        </div>
                      </td> 
                    </tr>
                    @endforeach                 
                  </tbody>
                </table>
              </div>
              {!! Form::close() !!}
          </div>
          <div class="panel-body pn">
            <div class="table-responsive">
              <table class="table admin-form theme-warning tc-checkbox-1 fs13">                                
                <tbody>
                  <tr class="">
                     <th class="text-left">
                      <button type="button" class="btn btn-primary" onclick="go_delete()"><i class="glyphicon glyphicon-trash"></i> Delete Multiple </button>
                    </th>
                    <th class="text-right">
                      {{ $get_record->links() }}
                    </th>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


<style type="text/css">
.dt-panelfooter{
  display: none !important;
}
</style>

@endsection