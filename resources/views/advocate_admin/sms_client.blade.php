@extends('advocate_admin/layout')
@section('content')
<style type="text/css">
  .admin-form .select, .admin-form .gui-input, .admin-form .select > select, .admin-form .select-multiple select{
    height: 28px !important;
  }
  .admin-form a.button, .admin-form span.button, .admin-form label.button
  {
    line-height: 28px !important;
  }
  .admin-form .append-icon .field-icon, .admin-form .prepend-icon .field-icon{
    line-height: 28px !important;
  }
  .admin-form .gui-textarea {
    line-height: 7px !important;
  }
  .select2-container .select2-selection--single {
    height: 28px !important;
  }
  .select2-container--default .select2-selection--single .select2-selection__rendered {
    line-height: 24px !important;
  }
  .select2-container .select2-selection--multiple {
    min-height: 30px !important;
  }
  .select2-container--default .select2-selection--single .select2-selection__arrow {
    top: -4px !important;
  }
/*  .gui-textarea{
    height: 100px !important;
  }*/
/* .admin-form .gui-input {
  padding: 5px;
 }*/
 /*.admin-form .prepend-icon .field-icon {
  top: 6px;
 }*/
 .form-control {
    height: 28px !important;
    /*padding: 0px 12px !important;*/
  }
 .admin-form .button{
    height: 28px !important;
    line-height: 1px !important;
  }
 .btn {
    height: 29px !important;
    line-height: 1px !important;  }

 .down{
  margin-top: 70px;
 }
.account_setting {
  margin: 0px 20px !important;
  margin-top: 25px !important;
  /*padding: 5px 0px !important;*/
 }
 .select-text{
  padding: 0px 12px;
 }
 .multiselect {
    text-align: left;
}
.btn.multiselect .caret {
    display: none;
}
.multiselect-search{
  height: 36px !important;
}
</style>
  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left down">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="#"> Sms to Client</a>
            </li>
            <li class="crumb-icon">
              <a href="{{ url('advocate-panel/dashboard') }}">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="{{ url('advocate-panel/dashboard') }}">Home</a>
            </li>
            <li class="crumb-trail">Sms to Client</li>
          </ol>
        </div>
      </header>

      <div class="row">
        <div class="col-md-12">
          @if (\Session::has('success'))
          <div class="alert alert-success account_setting" style="margin: 20px 25px;">
            {!! \Session::get('success') !!}
          </div>
          @endif
        </div>
      </div>

      <!-- Begin: Content -->

      <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center">
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-heading">
                <span class="panel-title">Sms to Client</span>
              </div>
              <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                <!-- IF any error found -->
                <div class="col-md-12">
                
                </div>
                <!-- IF any error found -->
                  <div id="tab1_1" class="tab-pane active">
                    {!! Form::open(['name'=>'form_add_question','url'=>'advocate-panel/sms-to-client/' ,'id'=>'form_add_question' ,'files'=>'true' ,'autocomplete'=>'off']) !!}
                    
                    <div class="row">
                        

                        <div class="col-md-12" id="">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Referred : </label>
                              <label for="artist_state" class="field">
                                <select id="referred" name="referred[]" multiple class="form-control">
                                    @foreach($referred as $referreds)
                                      <option value="{{ $referreds->ref_id }}"> {{ $referreds->ref_advocate_name }} ({{ $referreds->ref_mobile_number }}) </option>
                                    @endforeach
                                </select>
                              </label>
                          </div>
                        </div>


                        <div class="col-md-12">
                          <div class="section" style="margin-bottom: 40px;">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Send SMS :  </label>  
                            
                            <div class="col-md-2">
                              <label class="option" style="font-size:12px;">
                                  {!! Form::radio('send_sms','1',true, array( 'class' => 'check' , 'onclick' => 'send_sms_all(1)' )) !!}
                                <span class="radio"></span> All
                              </label>
                            </div>

                            <div class="col-md-2">
                              <label class="option" style="font-size:12px;">
                                  {!! Form::radio('send_sms','2','', array( 'class' => 'check' , 'onclick' => 'send_sms_all(2)' )) !!}
                                <span class="radio"></span> Pending
                              </label>
                            </div>
                          </div>
                          <div class="section"></div>
                          <div class="clearfix"></div>
                        </div>


                        <div class="col-md-12" id="hide_framework">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Client : </label>
                              <label for="artist_state" class="field">
                                <select id="framework" name="client[]" multiple class="form-control" onchange="get_client(this.value)" >
                                    @foreach($client as $clients)
                                      <option value="{{ $clients->cl_id }}"> {{ $clients->cl_group_name }} @if($clients->cl_father_name != "") {{ $clients->cl_name_prefix }} {{ $clients->cl_father_name }} @endif @if($clients->cl_group_mobile_no != "") ({{ $clients->cl_group_mobile_no }}) @endif </option>
                                    @endforeach
                                </select>
                              </label>
                          </div>
                        </div>

                        <div class="col-md-12" id="framework_sms">
                          
                        </div>



                        <div class="col-md-12" id="sub_client_view">
                          
                        </div>



                        <!-- <div class="col-md-12">
                          <div class="section" style="margin-bottom: 40px;">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Sub Client :  </label>  
                            
                            @foreach($sub_client as $sub_clients)
                              <div class="col-md-3" style="margin-top: 20px;">
                                <label for="level_name" class="field-label" style="font-weight:600;" > Client :  </label>
                                <label class="option block mn" style="font-size:12px;">
                                  {!! Form::checkbox('cl_group_name[]',$clients->cl_group_mobile_no, false , array( 'class' => 'check' ,'onclick'=>'group_type({{ $clients->cl_group_type }})'  )) !!}
                                  <span class="checkbox mn"></span> {{ $clients->cl_group_name }}
                                </label>
                              </div>
                            @endforeach

                          </div>
                          <div class="section"></div>
                        </div> -->
                               
                        <div class="col-md-12">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;"> Text :  </label>
                            <label for="level_name" class="field prepend-icon">
                              {!! Form::text('reg_sms_text', '' ,array('class' => 'gui-input','placeholder' => '' )) !!}
                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-pencil"></i>
                              </label>                           
                            </label>
                          </div>
                        </div>

                     </div>

                  <div class="panel-footer text-right">
                      {!! Form::submit('Send', array('class' => 'button btn-primary', 'id' => 'maskedKey_submit')) !!}
                      {!! Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}
                  </div>   
                    {!! Form::close() !!}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>


<style type="text/css">

.divider{
  display: none;
}
#form_add_question .field-icon {
    margin-top: 0px !important;
}

</style>

<script type="text/javascript">

  // sms type

  function reg_sms_type(){

    if ($('#sms_type').is(":checked")){
      $('#show_other_mobile').show();
    } else {
      $('#show_other_mobile').hide();      
    }

  }


  function get_client(client_id){

    var new_selected = $("#framework").val();

    var new_new = $("#framework_3").val()


    if(new_selected == null){
      var new_selected = $("#framework_3").val();
    }

    BASE_URL = '{{ url('/')}}';
    $.ajax({
      url:BASE_URL+"/advocate-panel/get-sub-client-ajax/"+new_selected,
      success: function(result){
          $("#sub_client_view").html(result); 
      }
    });
   
  }


  // send sms

  function send_sms_all(sms){

  //  alert(sms);

    BASE_URL = '{{ url('/')}}';
    $.ajax({
      url:BASE_URL+"/advocate-panel/send-sms-client/"+sms,
      success: function(result){
          $('#hide_framework').hide();  
          $("#framework_sms").html(result); 
      }
    });
   
  }


 


 


</script>


<style>
#subtype {
    width: 100%;
    padding: 50px 0;
    text-align: left !important;
    margin-top: 20px;
    display:none;
}

.mutbtnnn{
  height: 35px;
}

.mutbtnnn button{
  height: 35px !important;
}

.multiselect-container li a label{
  border: 0px !important;
  background: none !important;
}
.multiselect-container {
  margin-top: 5px !important;
}

optgroup{
  padding-left: 25px;
}
</style>
<script type="text/javascript">

  jQuery(document).ready(function() {

    $('#referred').multiselect({
      nonSelectedText: 'Select Referred',
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
      buttonWidth:'400px'
     });

    $('#framework').multiselect({
      nonSelectedText: 'Select Client',
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
      buttonWidth:'400px'
     });

    $('#framework_2').multiselect({
      nonSelectedText: 'Select Sub Client',
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
      buttonWidth:'400px'
     });

    $('#framework_3').multiselect({
      nonSelectedText: 'Select Client',
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
      buttonWidth:'400px'
     });


  
    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "This field contains alphabets only");

    jQuery.validator.addMethod("checkemail", function(value, element) 
    {
    return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
    }, "Enter a VALID email address"); 

    $.validator.addMethod('minStrict', function (value, el, param) {
     return value > param; 
    });
    $.validator.addMethod('maxStrict', function (value, el, param) {
     return value < param; 
    });


    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#form_add_question").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        img_reg: {
          // required: true,
          extension: 'jpeg,jpg,png',
        },
        // opposite_council: {
        //   required: true
        // },
        // client_sub_group: {
        //   required: true
        // },
      },
      /* @validation error messages 
      ---------------------------------------------- */

      messages: {
        img_reg: {
          extension: 'Image Should be in .jpg,.jpeg and .png format only'
        },
        ncv: {
          required: 'Please select NCV no.'
        },
        futher: {
          required: 'Please select next further date'
        },
      },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });
  
  </script>



@endsection
