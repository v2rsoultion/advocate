@extends('advocate_admin/layout')
@section('content')

  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="{{ url('/advocate-panel/view-case') }}">View Case Type</a>
            </li>
            <li class="crumb-icon">
              <a href="{{ url('advocate-panel/dashboard') }}">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="{{ url('advocate-panel/dashboard') }}">Home</a>
            </li>
            <li class="crumb-trail">Add Case Type</li>
          </ol>
        </div>
      </header>

      <!-- Begin: Content -->

      <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center">
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-heading">
                <span class="panel-title">Add Case Type</span>
              </div>
              <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                <!-- IF any error found -->
                <div class="col-md-12">
                @if ($errors->any())
                  <div id="log_error" class="alert alert-danger" style='font-family: josefin_sansregular !important;'>
                  {{$errors->first()}}  </i></div>
                @endif
                </div>
                <!-- IF any error found -->
                <div id="tab1_1" class="tab-pane active">
                    {!! Form::open(['name'=>'form_validation','url'=>'advocate-panel/insert-case-type/'.$get_record[0]->case_id,'id'=>'form_validation','autocomplete'=>'off']) !!}
                    <div class="row">
                      
                      <div class="col-md-12">
                        <div class="section" style="margin-bottom: 40px;">
                          <label for="level_name" class="field-label" style="font-weight:600;" > Category :  </label>  
                          <div class="col-md-2">
                            <label class="option block mn" style="font-size:12px;">

                              @if($get_record[0]->case_category != "")
                                {!! Form::radio('c_name','1',$get_record[0]->case_category == 1 ? 'checked' : '', array( 'class' => 'check' )) !!}
                              @else 
                                {!! Form::radio('c_name','1',checked, array( 'class' => 'check' , 'class' => 'check' )) !!}
                              @endif
                              <span class="checkbox mn"></span> Criminal
                            </label>
                          </div>

                          <div class="col-md-2">
                            <label class="option block mn" style="font-size:12px;">
                              {!! Form::radio('c_name','2',$get_record[0]->case_category == 2 ? 'checked' : '', array( 'class' => 'check'  )) !!}
                              <span class="checkbox mn"></span> Civil
                            </label>
                          </div>
                        </div>

                        <div class="section"></div>
                        <div class="clearfix"></div>
                      </div>




                      <div class="col-md-12">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;" >Type Name :  </label>  
                          <label for="level_name" class="field prepend-icon">
                            {!! Form::text('type_name',$get_record[0]->case_name,array('class' => 'gui-input','placeholder' => '' )) !!}
                              <label for="Account Mobile" class="field-icon">
                              <i class="fa fa-pencil"></i>
                            </label>                           
                          </label>
                        </div>
                        </div>
                        <div class="col-md-12">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;" >short Name :  </label>  
                          <label for="level_name" class="field prepend-icon">
                            {!! Form::text('short_name',$get_record[0]->case_short_name,array('class' => 'gui-input','placeholder' => '' )) !!}
                              <label for="Account Mobile" class="field-icon">
                              <i class="fa fa-pencil"></i>
                            </label>                           
                          </label>
                        </div>
                        </div>
                        
                      </div>
                    </div>
                  <div class="panel-footer text-right">
                      {!! Form::submit('Save', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}
                      {!! Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}
                  </div>   
                    {!! Form::close() !!}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

<script type="text/javascript">

  jQuery(document).ready(function() {
  
    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "This field contains alphabets only");

    jQuery.validator.addMethod("checkemail", function(value, element) 
    {
    return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
    }, "Enter a VALID email address"); 

    $.validator.addMethod('minStrict', function (value, el, param) {
     return value > param; 
    });
    $.validator.addMethod('maxStrict', function (value, el, param) {
     return value < param; 
    });


    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#form_validation").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        type_name: {
          required: true,
          lettersonly: true
        },
        short_name: {
          required: true,
          lettersonly: true
        },
      },

      /* @validation error messages 
      ---------------------------------------------- */

      messages: {
        type_name: {
          required: 'Please Fill Required Type Name'
        },
        c_name: {
          required: 'Please Fill Required Category'
        },
        short_name: {
          required: 'Please Fill Required Short Name'
        },        
      },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });


  </script>



@endsection
