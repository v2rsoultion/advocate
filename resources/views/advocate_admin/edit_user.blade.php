@extends('trivia_admin/layout')

@section('content')

  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="{{ url('/trivialeague-panel/view-user') }}">View User</a>
            </li>
            <li class="crumb-icon">
              <a href="{{ url('/trivialeague-panel/dashboard') }}">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="{{ url('/trivialeague-panel/dashboard') }}">Home</a>
            </li>
            <li class="crumb-trail">Edit User Profile</li>
          </ol>
        </div>
      </header>


      <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center" style="height:100% !important;">
          <div class="mw1000 center-block">
            <div class="content-header">
              <h2> Update  <b class="text-primary">User Profile</b> </h2>
              <p class="lead"></p>
            </div>

            <div class="panel panel-primary panel-border top mt20 mb35">
              <div class="panel-heading">
                <span class="panel-title">Profile</span>
              </div>            

              <div class="panel-body bg-light dark">

                <div class="admin-form">
                  {!! Form::open(['name'=>'userform','id'=>'userform','url'=>'/trivialeague-panel/edit-user/'.$id.'','files' => true ,'autocomplete'=>'off']) !!}
                    <div class="col-md-9">
                      <div class="tab-content pn br-n admin-form">
                        <div id="tab1_1" class="tab-pane active">
                                     

                          <div class="section row" style="  margin-bottom: 17px;">
                            <div class="col-md-12">  
                              <label for="user_name" class="field prepend-icon">
                                {!! Form::text('user_first_name',$get_record['user_first_name'], array('class' => 'gui-input','placeholder' => 'Enter First Name' )) !!}
                                {!! Form::hidden('user_id',$get_record['user_id']) !!}
                                <label for="user_name" class="field-icon">
                                  <i class="fa fa-user"></i>
                                </label>
                              </label>
                            </div>
                          </div>

                          <div class="section row" style="  margin-bottom: 17px;"> 
                            <div class="col-md-12">
                              <label for="user_email" class="field prepend-icon">
                                {!! Form::text('user_last_name',$get_record['user_last_name'],array('class' => 'gui-input mycursor' ,'placeholder' => 'Enter Last Name')) !!}
                                <label for="user_email" class="field-icon">
                                  <i class="fa fa-user"></i>
                                </label>
                              </label>
                            </div>
                          </div>

                          <div class="section row" style="  margin-bottom: 17px;">
                            <div class="col-md-12">
                              <label for="user_number" class="field prepend-icon">
                                {!! Form::text('user_email',$get_record['user_email'],array('class' => 'gui-input','placeholder' => 'Enter Email Address' )) !!}
                                <label for="Mobile Number" class="field-icon">
                                  <i class="fa fa-envelope"></i>
                                </label>
                              </label>
                            </div>
                          </div>
                         </div>   

                        
                      </div>
                      </div>


                      <div class="col-md-3" style="  margin-top: 4px;">
                      <div class="fileupload fileupload-new admin-form" data-provides="fileupload">

                      @if($get_record['user_image'] != "")

                        <div class="fileupload-preview thumbnail mb15" style="line-height: 136px !important;">
                          {!! Html::image($get_record['user_image'], '100%x147') !!}
                        </div>
                        <span class="button btn-system btn-file btn-block ph5" style="margin-bottom:10px">
                          <span class="fileupload-new">Profile Picture</span>
                          <span class="fileupload-exists">Profile Picture</span>
                          {!! Form::file('user_image') !!}
                        </span>

                      @else

                        <div class="fileupload-preview thumbnail mb15" style="padding: 3px;outline: 1px dashed #d9d9d9;height: 150px">
                          <img data-src="holder.js/100%x147" alt="100%x147" src="" data-holder-rendered="true"  style="height: 150px !important; width: 100%; display: block;">
                        </div>
                        <span class="button btn-system btn-file btn-block ph5" style="margin-bottom:10px">
                          <span class="fileupload-new" >Profile Photo</span>
                          <span class="fileupload-exists">Profile Photo</span>
                          {!! Form::file('user_image') !!}
                        </span>

                      @endif

                      </div>
                    </div>


                    

                    <div class="form-group">
                    <div class="col-lg-12">
                      <div class="input-group" style="float: right; width: 220px;  ">
                      {!! Form::submit('Update', array('class' => 'button btn-primary btn-file btn-block ph5', 'id' => 'maskedKey')) !!}

                      </div>
                    </div>
                    </div>
                  {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

<script type="text/javascript">
  jQuery(document).ready(function() {

    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "This field contains alphabets only");

    jQuery.validator.addMethod("checkemail", function(value, element) 
    {
    return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
    }, "Enter a VALID email address"); 
  
    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#userform").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        user_first_name: {
          required: true,
          lettersonly: true,
        },
        user_last_name: {
          required: true,
          lettersonly: true,
        },
        user_email: {
          checkemail: true,
        },
        user_image: {
        // //  required: true,
          extension: 'jpeg,jpg,png',
        //   type: 'image/jpeg,image/png',
        // //  maxSize: 2097152,   // 2048 * 1024
       //    message: 'The selected file is not valid'
         },
      },

      /* @validation error messages 
      ---------------------------------------------- */

      messages: {
        user_first_name: {
          required: 'Enter first name.'
        },
        user_last_name: {
          required: 'Enter last name.'
        },
        user_email: {
          
          email: 'Enter a VALID email address'
        },
        user_image: {
          extension: 'Image Should be in .jpg,.jpeg and .png format only'
        },
      },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });
  </script>





@endsection
