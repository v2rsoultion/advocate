@extends('advocate_admin/layout')
@section('content')

<style type="text/css">

.select_outer{
  height: 30px !important;
}
.new_select{
  height: 30px !important;
  padding: 0px 10px !important;
  font-size: 12px !important;
}

.arrow_new{
  top: 2px !important;
}

.new_text{
  height: 30px !important;
  font-size: 12px !important;
}

.search_new{
  line-height: 32px !important;
}

.new_button{
  height: 30px !important;
  font-size: 12px !important;
  padding: 5px 10px; 
  line-height: 30px !important;
}

.new_button_2{
  height: 30px !important;
  font-size: 12px !important;
  padding: 5px 10px; 
}

.caretss {
    margin-left: 30% !important;
}

.mutbtnnn button{
  height: 30px !important;
  padding: 3px 10px !important; 
}

.admin-form .checkbox, .admin-form .radio{
  border: 0px solid !important;
  background: none !important;
}

 .close{
  margin-top: -6px !important;
  font-size: 30px !important;
  width: 30px !important;
}

 /*.admin-form .select, .admin-form .gui-input, .admin-form .select > select, .admin-form .select-multiple select{
    height: 28px !important;
  }*/
  .admin-form .append-icon .field-icon, .admin-form .prepend-icon .field-icon{
    line-height: 28px !important;
  }
  .admin-form .gui-textarea {
    line-height: 15px !important;
  }
  .admin-form .gui-input {
    height: 28px !important;
  }

  .form-control {
    height: 28px !important;
    /*padding: 0px 12px !important;*/
  }
  .admin-form .button{
    height: 28px !important;
    line-height: 1px !important;
  }
  .btn {
    height: 29px !important;
    line-height: 1px !important;  }

 .down{
  margin-top: 70px;
 }
 .select-text{
  padding: 0px 12px;
 }
</style>

  <!-- Start: Content-Wrapper -->
  <section id="content_wrapper">
    <header id="topbar" style="margin-top: 62px;" >
      <div class="topbar-left">
        <ol class="breadcrumb">
          <li class="crumb-active">
            <a href="{{url('/advocate-panel/undated-case')}}">Undated Case</a>
          </li>
          <li class="crumb-icon">
            <a href="{{ url('advocate-panel/dashboard') }}">
              <span class="glyphicon glyphicon-home"></span>
            </a>
          </li>
          <li class="crumb-link">
            <a href="{{ url('advocate-panel/dashboard') }}">Home</a>
          </li>
          <li class="crumb-trail"> Undated Case </li>
        </ol>
      </div>
    </header>

    <div class="" style="margin-top:10px;">
      <div class="col-md-12">
        <div class="panel panel-primary panel-border top mb70">  
          <div class="panel-heading">
            <div class="panel-title hidden-xs">
              <span class="glyphicon glyphicon-tasks"></span> Undated Case </div>
          </div>

          <div class="panel-menu admin-form theme-primary" style="padding: 5px 20px;">
            <div class="row">
              {!! Form::open(['url'=>'/advocate-panel/undated-case' ,'autocomplete'=>'off' ]) !!}

                <div class="col-md-2">
                    <label for="level" class="field prepend-icon select_outer" style="margin-top:10px !important;">
                      <select class=" form-control new_select" name="court_type" id="level" style="color: black;">
                        <option value="">Court Type</option>
                            <option value="1" {{ $court_type == 1 ? 'selected="selected"' : '' }} >Trial Court</option>
                            <option value="2" {{ $court_type == 2 ? 'selected="selected"' : '' }} >High Court</option>
                            <option value="3" {{ $court_type == 3 ? 'selected="selected"' : '' }} >Both</option> 
                      </select>
                    </label>
                </div>

                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 10px;">
                    {!! Form::text('reg_file_no','',array('class' => 'form-control new_text','placeholder' => 'File No', 'autocomplete' => 'off' )) !!}
                    <label for="pincode" class="field-icon search_new">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>

                <div class="col-md-2">
                  <div class="" style="margin-top:10px !important;">
                    <label for="artist_state" class="field select select_outer">
                      <select class="form-control new_select" name="type" style="color: black;">
                        <option value=''>Case Type</option> 
                        
                        @foreach($case_type_entry as $case_type_entrys) 
                        <option value="{{ $case_type_entrys->case_id }}" {{ $case_type_entrys->case_id == $reg_case_type_id ? 'selected="selected"' : '' }} >{{$case_type_entrys->case_name}} </option>
                        @endforeach
                        
                      </select>
                      <i class="arrow double arrow_new"></i>
                    </label>
                  </div>
                </div>

                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 10px;">
                    {!! Form::text('petitioner_name','',array('class' => 'form-control new_text','placeholder' => 'Pet. Name', 'autocomplete' => 'off' )) !!}
                    <label for="pincode" class="field-icon search_new">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>

                <div class="col-md-2">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 10px;">
                    {!! Form::text('respondent','',array('class' => 'form-control new_text','placeholder' => 'Res. Name', 'autocomplete' => 'off' )) !!}
                    <label for="pincode" class="field-icon search_new">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>

                <div class="col-md-2">
                  <div class="" style="margin-top:10px !important;">
                    <label for="artist_state" class="field select select_outer">
                      <select id="multiselect2" multiple="multiple" name="selected_value[]" onchange="selected_fields(this.value)" style="color: black;">
                          <option value="1">Case No.</option>
                          <!-- <option value="2">Select Year</option> -->
                          <option value="3">NCV No.</option>
                          <option value="4">Select Stage</option>
                          <option value="5">Reg. From Date</option>
                          <option value="6">Reg. To Date</option>
                          <option value="7">Next Date</option>
                          <option value="8">From Date</option>
                          <option value="9">To Date</option>
                        </select>
                    </label>
                  </div>
                </div>

                <div class="col-md-2" style="display: none" id="case_no_hide">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 10px;">
                    {!! Form::text('case_no','',array('class' => 'form-control new_text','placeholder' => 'Case No', 'autocomplete' => 'off' )) !!}
                    <label for="pincode" class="field-icon search_new">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>

                <!-- <div class="col-md-2" style="display: none" id="year_hide">
                  <div class="" style="margin-top:10px !important;">
                      <label for="artist_state" class="field select select_outer">
                        <select class="form-control new_select" id="" name="case_year">
                          <option value=''>Select Year</option>
                      
                          @php
                              $currentYear = date('Y');
                          @endphp

                          @foreach(range(1950, $currentYear) as $value)
                              <option value={{$value}} {{ $value == $case_year ? 'selected="selected"' : '' }} >{{$value}}</option>
                          @endforeach

                        </select>
                        <i class="arrow double arrow_new"></i>
                      </label>
                  </div>
                </div> -->

                <div class="col-md-2" style="display: none" id="ncv_no_hide">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 10px;">
                    {!! Form::text('ncv_no','',array('class' => 'form-control new_text','placeholder' => 'NCV No', 'autocomplete' => 'off' )) !!}
                    <label for="pincode" class="field-icon search_new">
                      <i class="fa fa-search"></i>
                    </label>
                  </label>
                </div>
              
                <div class="col-md-2" style="display: none" id="stage_hide">
                  <div class="" style="margin-top:10px !important;">
                      <label for="artist_state" class="field select select_outer">
                        <select class="form-control new_select" id="hide_choose_sub_type" name="reg_stage_id" style="color: black;">
                          <option value=''>Select Stage</option>
                          @foreach($stage as $stages) 
                            <option value="{{ $stages->stage_id }}" {{ $stages->stage_id == $reg_stage_id ? 'selected="selected"' : '' }} >{{$stages->stage_name}} </option>
                          @endforeach
                        </select>
                        <i class="arrow double arrow_new"></i>
                      </label>
                  </div>
                </div>

                <div class="col-md-2" style="display: none" id="from_date_hide">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 10px;">
                    {!! Form::text('from_date','',array('class' => 'form-control fromdate_search new_text','placeholder' => 'Reg. From Date', 'autocomplete' => 'off' )) !!}
                    <label for="pincode" class="field-icon search_new">
                      <i class="fa fa-calendar"></i>
                    </label>
                  </label>
                </div>
                
                <div class="col-md-2" style="display: none" id="to_date_hide">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 10px;">
                    {!! Form::text('to_date','',array('class' => 'form-control todate new_text','placeholder' => 'Reg. To Date', 'autocomplete' => 'off' )) !!}
                    <label for="pincode" class="field-icon search_new">
                      <i class="fa fa-calendar"></i>
                    </label>
                  </label>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-2" style="display: none" id="next_date_hide">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 10px;">
                    {!! Form::text('further_date','',array('class' => 'form-control fromdate_search new_text','placeholder' => 'Next Date', 'autocomplete' => 'off', 'id' => '' )) !!}
                    <label for="pincode" class="field-icon search_new">
                      <i class="fa fa-calendar"></i>
                    </label>
                  </label>
                </div>

                <div class="col-md-2" style="display: none" id="from_hide">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 10px;">
                    {!! Form::text('from','',array('class' => 'form-control fromdate_certified new_text','placeholder' => 'From Date', 'autocomplete' => 'off', 'id' => '' )) !!}
                    <label for="pincode" class="field-icon search_new">
                      <i class="fa fa-calendar"></i>
                    </label>
                  </label>
                </div>

                <div class="col-md-2" style="display: none" id="to_hide">
                  <label for="pincode" class="field prepend-icon" style="margin-top: 10px;">
                    {!! Form::text('to','',array('class' => 'form-control todate_certified new_text','placeholder' => 'To Date', 'autocomplete' => 'off', 'id' => '' )) !!}
                    <label for="pincode" class="field-icon search_new">
                      <i class="fa fa-calendar"></i>
                    </label>
                  </label>
                </div>

        
                <div class="col-md-1 pull-right" style="margin-top:10px !important;">
                  <button type="submit" name="search" class="button btn-primary new_button"> Search </button>
                </div>

                 <div class="col-md-1" style="margin-top:10px !important;">
                   <a href="{{ url('/advocate-panel/undated-case/')}}">{!! Form::button('Default', array('class' => 'btn btn-primary new_button_2', 'id' => 'maskedKey')) !!}</a>
                </div> 

                <div  class="col-md-2" style="margin-top:10px !important;">
                  <a href="{{ url('advocate-panel/download-undated-case-all/xls')}}?reg_case_type_id={{$reg_case_type_id}}&case_no={{$case_no}}&case_year={{$case_year}}&respondent={{$respondent}}&petitioner_name={{$petitioner_name}}&ncv_no={{$ncv_no}}&file_no={{$reg_file_no}}&stage_id={{$reg_stage_id}}&from_date={{$from_date}}&to_date={{$to_date}}&further_date={{$further_date}}&court_type={{$court_type}}&from={{$from}}&to={{$to}}"><button type="button" class="btn btn-primary pull-right new_button_2" > Download All </button></a>
                </div>

                <div class="col-md-2 mt10">
                   <a href="{{ url('/advocate-panel/undated-case/all')}}">{!! Form::button('Show All Records', array('class' => 'btn btn-primary', 'id' => 'maskedKey')) !!}</a>
                </div>

              {!! Form::close() !!}             
                   
            </div>
          </div>

          <div class="panel-body pn">
              <!-- {!! Form::open(['url'=>'/advocate-panel/view-peshi','name'=>'form']) !!} -->
              <div class="table-responsive">
                <table class="table admin-form table-bordered table-striped theme-warning tc-checkbox-1 fs13" id="datatable">
                  <thead>
                    <tr class="bg-light">
                      <!-- <th style="width:90px !important;" class="text-left">
                        <label class="option block mn">
                          <input type="checkbox" id="check_all"> 
                          <span class="checkbox mn"></span>
                          Select All
                        </label>
                      </th> -->
                      <th> S.No</th>
                      <th class="">Case No.</th>
                      <th class="">File No.</th>
                      <th class="">Court Type</th>
                      <th class="">Type of Case</th>
                      <th class="">Title of Case</th>
                      <th class="">Stage</th>
                      <th class="">Previous Date</th>
                      <th class="">Peshi Entry</th>
                      <th class="">Result/Next Date</th>
                    </tr>
                  </thead>
                  <tbody>

                  @php $sno = 0; @endphp
                  @foreach($get_record as $get_records)              

                  @php $sno = $sno+1; @endphp
                    <tr>
                      <!-- <td class="text-left" style="padding-left: 18px;">
                        <label class="option block mn">
                          <input type="checkbox" name="check[]" class="check" value="{{$get_records->pessi_id}}">
                          <span class="checkbox mn"></span>
                        </label>
                      </td> -->

                      <td> {{ $sno }} </td>
                      <td class="text-left" style="padding-left: 20px;"> @if($get_records->reg_case_number == "") ----- @else {{ $get_records->reg_case_number }} @endif </td>

                      <td class="text-left" style="padding-left: 20px;"> <a href="{{ url('/advocate-panel/case-detail') }}/{{sha1($get_records->reg_id) }}"> @if($get_records->reg_file_no == "") ----- @else {{ $get_records->reg_file_no }} @endif </a> </td>

                      <td class="text-left" style="padding-left: 20px;">
                        @if($get_records->reg_court == 2)
                               High Court
                        @elseif($get_records->reg_court == 1)
                               Trial Court
                        @endif
                      </td>

                      <td class="text-left" style="padding-left: 20px;"> @if($get_records->case_name == "") ----- @else {{ $get_records->case_name }} @endif </td>

                      <td class="text-left" style="padding-left: 20px;">  {{ $get_records->reg_petitioner }} v/s {{ $get_records->reg_respondent }}  </td>


                      <td class="text-left" style="padding-left: 20px;"> @if($get_records->stage_name == "") ----- @else {{ $get_records->stage_name }} @endif </td>
                      
                      <td class="text-left" style="padding-left: 20px;">  
                        @if($get_records->pessi_prev_date == "1970-01-01" || $get_records->pessi_prev_date == "")
                          -----
                        @else
                          {{ date('d F Y',strtotime($get_records->pessi_prev_date)) }}
                        @endif
                      </td>

                      <td class="text-left" style="padding-left: 20px;">
                        <a href="#" style="text-transform: capitalize; text-decoration:none;" onclick="edit_pessi({{$get_records->pessi_id}})" > Peshi Entry </a>
                        <div id="edit_pessi{{$get_records->pessi_id}}" class="modal fade in" role="dialog" style="overflow: scroll;">
                          <div class="modal-dialog" style="width:700px; margin-top:80px;">
                            <div class="modal-content">
                              <div class="modal-header" style="padding-bottom: 35px;">
                                <button type="button" class="close" data-dismiss="modal" onclick="close_button_edit_pessi({{$get_records->pessi_id}})" >&times;</button>
                                <h4 class="modal-title pull-left"> Peshi Entry </h4>
                              </div>
                              <section id="content" class="table-layout animated fadeIn">
                                <div>
                                  <div class="center-block">
                                    <div class="panel panel-primary panel-border top mb35">
                                      <div class="panel-body bg-light dark">
                                        <div class="tab-content pn br-n admin-form">
                                          <div id="tab1_1" class="tab-pane active">
                                            
                                            {!! Form::open(['name'=>'form_add_pessi','url'=>'advocate-panel/insert-peshi/'.$get_records->pessi_id,'id'=>'form_add_pessi' ,'autocomplete'=>'off']) !!}

                                            <div class="row">                                                
                                                

                                                <div class="col-md-12">
                                                  <div class="section" style="margin-bottom: 40px;">
                                                    <div class="col-md-3">
                                                      <label class="option" style="font-size:12px;">
                                                        {!! Form::radio('choose_type','0',$get_records['pessi_choose_type'] == 0 ? 'checked' : '', array('onclick' => "show_further('$get_records->pessi_id')", 'class' => 'check' )) !!}
                                                        <span class="radio" style="border: 2px solid #4a89dc !important;"></span> Next Date
                                                      </label>
                                                    </div>

                                                    <div class="col-md-3">
                                                      <label class="option" style="font-size:12px;">
                                                        {!! Form::radio('choose_type','1',$get_records['pessi_choose_type'] == 1 ? 'checked' : '', array('onclick' => "show_decide('$get_records->pessi_id')", 'class' => 'check' )) !!}
                                                        <span class="radio" style="border: 2px solid #4a89dc !important;"></span> Decide
                                                      </label>
                                                    </div>

                                                    <div class="col-md-3">
                                                      <label class="option" style="font-size:12px;">
                                                        {!! Form::radio('choose_type','2',$get_records['pessi_choose_type'] == 2 ? 'checked' : '', array('onclick' => "show_due_course('$get_records->pessi_id')", 'class' => 'check' )) !!}
                                                        <span class="radio" style="border: 2px solid #4a89dc !important;"></span> Due Course
                                                      </label>
                                                    </div>

                                                  </div>

                                                  <div class="section"></div>
                                                  <div class="clearfix"></div>
                                                </div>

                                                <div class="col-md-6" @if($get_records['pessi_choose_type'] == 1) style="display:none;" @endif  id="show_further_date{{$get_records['pessi_id']}}" >
                                                  <div class="section section_from">
                                                    <label for="level_name" class="field-label" style="font-weight:600;" >Next Further Date </label>  
                                                    <label for="level_name" class="field prepend-icon" id="discount_date_from_lb">

                                                    {!! Form::hidden('current_date',date('d-m-Y'),array('class' => 'gui-input current_date','placeholder' => '','id'=>'current_date'  )) !!}

                                                    @if($get_records->pessi_further_date == "" || $get_records->pessi_further_date == "1970-01-01")

                                                      {!! Form::text('further_date','',array('class' => 'gui-input disposal_to' ,'placeholder' => '','id'=>'' , autofocus )) !!}

                                                    @else
                                                      {!! Form::text('further_date',date('d-m-Y',strtotime($get_records->pessi_further_date)),array('class' => 'gui-input disposal_to' ,'placeholder' => '','id'=>'' , autofocus )) !!}
                                                    @endif

                                                        <label for="Account Mobile" class="field-icon">
                                                        <i class="fa fa-calendar"></i>
                                                      </label>        
                                                      <div></div>                   
                                                    </label>
                                                    <!-- <label for="level_name" class="field-label"> <b>Note :-</b> Date format will be dd/mm/yyyy </label> -->
                                                  </div>
                                                </div>

                                                <div class="col-md-6" @if($get_records['pessi_choose_type'] == 1) @else style="display: none;" @endif id="show_decide{{$get_records['pessi_id']}}">
                                                  <div class="section">
                                                    <label for="level_name" class="field-label" style="font-weight:600;"> Decide : </label>
                                                      <!-- <label for="artist_state" class="field select"> -->
                                                      <label for="level" class="field prepend-icon">
                                                        <select class="form-control select-text" id="decide" name="decide" >
                                                          <option value=''>Select Decide </option>
                                                          <option value='1' {{ $get_records->pessi_decide_id == 1 ? 'selected="selected"' : '' }} >Case In Favour </option>      
                                                          <option value='2' {{ $get_records->pessi_decide_id == 2 ? 'selected="selected"' : '' }} >Case in against </option>
                                                          <option value='3' {{ $get_records->pessi_decide_id == 3 ? 'selected="selected"' : '' }} > Withdraw </option>
                                                          <option value='4' {{ $get_records->pessi_decide_id == 4 ? 'selected="selected"' : '' }} > None </option>
                                                        </select>
                                                        <i class="arrow double"></i>
                                                      </label>
                                                  </div>
                                                </div>

                                                <div class="col-md-2" style="display: none;" id="show_due_course{{$get_records['pessi_id']}}">
                                                  <button type="button" class="btn btn-success br2 btn-xs field select form-control" style="margin-top: 23px; margin-bottom: 22px;" > Due Course </button>
                                                </div>


                                                <div class="col-md-12">
                                                  <div class="section">
                                                    <label for="level_name" class="field-label" style="font-weight:600;"> Select Stage : </label>
                                                      <!-- <label for="artist_state" class="field select"> -->
                                                        <label for="level" class="field prepend-icon">
                                                            <select class="form-control select-text" name="reg_stage" id="reg_stage">
                                                         <!--  <option value=''>Select Stage</option> -->
                                                            @foreach($stage as $stages) 
                                                              <option value="{{ $stages->stage_id }}" {{ $stages->stage_id == $get_records->reg_stage_id ? 'selected="selected"' : '' }} >{{$stages->stage_name}} </option>
                                                            @endforeach                                
                                                        </select>
                                                        <i class="arrow double"></i>
                                                      </label>
                                                  </div>
                                                </div>

                                                <div class="col-md-12">
                                                  <div class="section">
                                                    <label for="level_name" class="field-label" style="font-weight:600;"> Order Sheet :  </label>  
                                                    <label for="level_name" class="field prepend-icon">
                                                      {!! Form::textarea('order_sheet',$get_records->pessi_order_sheet,array('class' => 'gui-textarea accc1','placeholder' => '','id'=>'' )) !!}
                                                        <label for="Account Mobile" class="field-icon">
                                                        <i class="fa fa-comment"></i>
                                                      </label                           
                                                    </label>
                                                  </div>
                                                </div>

                                                
                                              
                                             </div>

                                          <div class="panel-footer text-right">
                                              {!! Form::submit('Save', array('class' => 'button btn-primary mysave', 'id' => 'maskedKey')) !!}
                                              <!-- {!! Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!} -->
                                          </div>   
                                            {!! Form::close() !!}
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </section>
                              <div class="clearfix"></div>
                            </div> 
                          </div>
                        </div>
                      </td>

                      <td style="padding-left: 20px;">
                        @if($get_records->pessi_choose_type == 0)
                        
                        @if($get_records->pessi_further_date == "" || $get_records->pessi_further_date == "1970-01-01")
                          Next Date:-  ------
                        @else 
                          Next Date:-  {{ date('d F Y',strtotime($get_records->pessi_further_date)) }}
                        @endif

                        @elseif($get_records->pessi_choose_type == 1)
                          Decide :- @if($get_records->pessi_decide_id == 1) Case In Favour @elseif($get_records->pessi_decide_id == 2) Case in against @elseif($get_records->pessi_decide_id == 3) Withdraw @elseif($get_records->pessi_decide_id == 4) None @endif
                        @else
                          Due Course
                        @endif
                      </td>

                    </tr>
                  @endforeach()

                  @if(count($get_record) == 0)
                    
                  @endif
                  </tbody>
                </table>
              </div>
              {!! Form::close() !!}
          </div>
          <div class="panel-body pn">
            <div class="table-responsive">
              <table class="table admin-form theme-warning tc-checkbox-1 fs13">                                
                <tbody>
                  <tr class="">
                     <!-- <th class="text-left">
                      <button type="button" class="btn btn-primary" onclick="go_delete()"><i class="glyphicon glyphicon-trash"></i> Delete Multiple </button>
                    </th> -->
                    <th class="text-right">
                      {{ $get_record->links() }}
                    </th>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

<style type="text/css">
.view a{
  color:#fff !important;
}
</style>
<style type="text/css">
.dt-panelfooter{
  display: none !important;
}
</style>

<script type="text/javascript">


$(document).on("keyup",".disposal_to",function(){
      $('.mysave').removeAttr('disabled');
      var  currentdate = this.value;
      var n=$(this);    

      var checkdate = /^([0]?[1-9]|[1|2][0-9]|[3][0|1])[-]([0]?[1-9]|[1][0-2])[-]([0-9]{4}|[0-9]{4})$/.test(currentdate);
      
      if(checkdate == true) {

        value=currentdate.split("-");
        var newDate_notice =value[1]+"/"+value[0]+"/"+value[2];
        var notice_status_date = new Date(newDate_notice).getTime();

        
        var notice_issue_date = $('#current_date').val();
        notice_issue_date = notice_issue_date.split("-");
        var newDate_notice_issue = notice_issue_date[1]+"/"+notice_issue_date[0]+"/"+notice_issue_date[2];
        var notice_issue_date = new Date(newDate_notice_issue).getTime();


        //if(notice_status_date > notice_issue_date){
          n.next().next().html("");
          n.next().next().css("color", "#DE888A");
          n.parent().addClass("state-success");
          $('.mysave').removeAttr('disabled');
        // } else {
        //   n.next().next().html("Next date must be greater than current date.");
        //   n.next().next().css("color", "#DE888A");
        //   n.parent().addClass("state-error");
        //   n.parent().removeClass("state-success");
        //   $(this).closest('.mysave').html('');
        //   $('.mysave').attr('disabled','disabled');
        //   return false;
        // }

        
      } else {
        n.next().next().html("Enter date in dd-mm-yyyy format.");
        n.next().next().css("color", "#DE888A");
        n.parent().addClass("state-error");
        n.parent().removeClass("state-success");
        $(this).closest('.mysave').html('');
        $('.mysave').attr('disabled','disabled');
        return false;
      }
  });



  jQuery(document).ready(function() {

    "use strict";

    $('#datatable').dataTable({
      "aoColumnDefs": [{
        'bSortable': false,
        'aTargets': [-1]
      }],
      "oLanguage": {
        "oPaginate": {
          "sPrevious": "",
          "sNext": ""
        }
      },
      "iDisplayLength": -1,
      
      "sDom": '<"dt-panelmenu clearfix"lfr>t<"dt-panelfooter clearfix"ip>',
      "oTableTools": {
        "sSwfPath": "vendor/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
      }
    });

    $('.dataTables_filter input').attr("placeholder", "Enter Terms...");
    $('div.dataTables_filter input').focus();
    $('#datatable_length').hide();

  });

  
  function selected_fields(){


    var new_selected = $("#multiselect2").val();

    if(new_selected == null){
      $('#case_no_hide').css('display','none');
      $('#year_hide').css('display','none');      
      $('#ncv_no_hide').css('display','none');
      $('#stage_hide').css('display','none');
      $('#from_date_hide').css('display','none');
      $('#to_date_hide').css('display','none');
      $('#next_date_hide').css('display','none');
    }    

    var selected = $("#multiselect2").val().toString();

    if(selected != null){
      var values   = selected.split(",");      
    }


    if(selected.includes("1") == true){  
      $('#case_no_hide').css('display','block');  
    } else {  
      $('#case_no_hide').css('display','none');  
    }

    if(selected.includes("2") == true){
      $('#year_hide').css('display','block');
    } else {
      $('#year_hide').css('display','none');
    }

    if(selected.includes("3") == true){
      $('#ncv_no_hide').css('display','block');
    } else {
      $('#ncv_no_hide').css('display','none');
    }

    if(selected.includes("4") == true){
      $('#stage_hide').css('display','block');
    } else {
      $('#stage_hide').css('display','none');
    }

    if(selected.includes("5") == true){
      $('#from_date_hide').css('display','block');
    } else {
      $('#from_date_hide').css('display','none');
    }

    if(selected.includes("6") == true){
      $('#to_date_hide').css('display','block');
    } else {
      $('#to_date_hide').css('display','none');
    }

    if(selected.includes("7") == true){
      $('#next_date_hide').css('display','block');
    } else {
      $('#next_date_hide').css('display','none');
    }

    if(selected.includes("8") == true){
      $('#from_hide').css('display','block');
    } else {
      $('#from_hide').css('display','none');
    }

    if(selected.includes("9") == true){
      $('#to_hide').css('display','block');
    } else {
      $('#to_hide').css('display','none');
    }

    
  }


  function show_further(pessi_id){
    document.getElementById("show_further_date"+pessi_id).style.display = "block";
    document.getElementById("show_decide"+pessi_id).style.display = "none";
    document.getElementById("show_due_course"+pessi_id).style.display = "none";
  }


  function show_decide(pessi_id){
    document.getElementById("show_further_date"+pessi_id).style.display = "none";
    document.getElementById("show_decide"+pessi_id).style.display = "block";
    document.getElementById("show_due_course"+pessi_id).style.display = "none";
  }


  function show_due_course(pessi_id){
    document.getElementById("show_further_date"+pessi_id).style.display = "none";
    document.getElementById("show_decide"+pessi_id).style.display = "none";
  //  document.getElementById("show_due_course").style.display = "block";
  }

</script>

<script type="text/javascript">

  jQuery(document).ready(function() {

    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "This field contains alphabets only");

    jQuery.validator.addMethod("checkemail", function(value, element) 
    {
    return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
    }, "Enter a VALID email address"); 

    $.validator.addMethod('minStrict', function (value, el, param) {
     return value > param; 
    });
    $.validator.addMethod('maxStrict', function (value, el, param) {
     return value < param; 
    });

    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#form_add_pessi").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        // case_no: {
        //   required: true
        // },
        // reg_stage: {
        //   required: true
        // },
        // order_sheet: {
        //   required: true
        // },
        // choose_type: {
        //   required: true
        // },
        // further_date: {
        //   required: true
        // },
        // decide: {
        //   required: true
        // }
      },
      /* @validation error messages 
      ---------------------------------------------- */

      messages: {
        // reg_stage: {
        //   required: 'Please select stage'
        // },
        // order_sheet: {
        //   required: 'Please enter order sheet'
        // },
        // type: {
        //   required: 'Please Fill Required Type'
        // },
        // further_date: {
        //   required: 'Please select further date'
        // },
        // decide: {
        //   required: 'Please select decide'
        // },
      },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });


  // setInterval(function(){ 
  //   var dt_val = $("#datepicker_further").val();
  //   if (dt_val!="") {
  //       $("#discount_date_from_lb").addClass("state-success");
  //       $("#discount_date_from_lb").removeClass("state-error");
  //       $(".section_from .state-error").html("");
  //   }
  // }, 500);



  jQuery(document).ready(function() {

    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "This field contains alphabets only");

    jQuery.validator.addMethod("letternumeric", function(value, element) 
    {
    return this.optional(element) || /^[a-z,0-9," "]+$/i.test(value);
    }, "This field contains only alphabets and numeric only");

    jQuery.validator.addMethod("checkemail", function(value, element) 
    {
    return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
    }, "Enter a VALID email address"); 

    $.validator.addMethod('minStrict', function (value, el, param) {
     return value > param; 
    });
    $.validator.addMethod('maxStrict', function (value, el, param) {
     return value < param; 
    });


    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#form_add_question").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        // case_no: {
        //   required: true
        // },
        // reg_stage: {
        //   required: true
        // },
        // honarable_justice: {
        //   required: true
        // },
        // court_no: {
        //   required: true,
        //   letternumeric: true
        // },
        // serial_no: {
        //   required: true,
        //   letternumeric: true
        // },
        // page_no: {
        //   required: true,
        //   letternumeric: true
        // }
      },
      /* @validation error messages 
      ---------------------------------------------- */

      messages: {
        // reg_stage: {
        //   required: 'Please select stage'
        // },
        // honarable_justice: {
        //   required: 'Please select honarable justice'
        // },
        // court_no: {
        //   required: 'Please enter court no'
        // },
        // serial_no: {
        //   required: 'Please enter serial no'
        // },
        // page_no: {
        //   required: 'Please enter page no'
        // },
      },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });


  function edit_pessi(pessi_id){
    $('body').css('overflow','hidden');
    $("html, body").animate({ scrollTop: 0 }, "slow");
    document.getElementById('edit_pessi'+pessi_id).style.display='block';
  }

  function close_button_edit_pessi(pessi_id){
    $('body').css('overflow','auto', 'important');
    document.getElementById('edit_pessi'+pessi_id).style.display='none';
  }


  </script>

@endsection