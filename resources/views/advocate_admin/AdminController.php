<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Court\Court; // model name
use App\Model\Admin\Admin; // model name
use App\Model\Admin\Account; // model name
use App\Model\Case_Registration\Case_Registration; // model name
use App\Model\Pessi\Pessi; // model name


class AdminController extends Controller
{
   	public function __construct(){
      	$this->middleware('Validation');
  	}

    public function dashboard(){

    	if(session('admin_id') == ""){
    		return redirect('advocate-panel/dashboard');
    	}      

      $total_case     = Case_Registration::where('adv_case_reg.user_admin_id','=',session('admin_id'))->count();
      
      $civil_case     = Case_Registration::where('adv_case_reg.user_admin_id','=',session('admin_id'))->where('adv_case_reg.reg_case_type_category','=',2)->count();


      $criminal_case  = Case_Registration::where('adv_case_reg.user_admin_id','=',session('admin_id'))->where('adv_case_reg.reg_case_type_category','=',1)->count();

      
      // $progress_case  = Case_Registration::leftJoin('adv_pessi', function($join) {$join->on('reg_id', '=', 'pessi_case_reg');})->where('adv_case_reg.user_admin_id','=',session('admin_id'))
      // ->where('pessi_status',1)
      // ->where('pessi_choose_type','!=',2)
      // ->where('pessi_choose_type','!=',1)
      // ->get();

      $progress_case = Pessi::leftJoin('adv_case_reg', function($join) {$join->on('pessi_case_reg', '=', 'reg_id');})->where('pessi_user_id', session('admin_id'))
      // ->where('pessi_status',1)
      ->where('pessi_choose_type','!=',2)->where('pessi_choose_type', '!=', 1)
      ->groupBy('pessi_case_reg')
      ->orderBy('reg_id', 'desc')->get();
      
      // echo "<pre>";
      // print_r(count($progress_case));
      // die;
 
      $completed_case = Case_Registration::leftJoin('adv_pessi', function($join) {$join->on('reg_id', '=', 'pessi_case_reg');})->where('adv_case_reg.user_admin_id','=',session('admin_id'))
      // ->where('pessi_status',1)
      ->where('pessi_choose_type','=',1)->groupBy('pessi_case_reg')->count();

      $due_cause      = Case_Registration::leftJoin('adv_pessi', function($join) {$join->on('reg_id', '=', 'pessi_case_reg');})->where('adv_case_reg.user_admin_id','=',session('admin_id'))
      // ->where('pessi_status',1)
      ->where('pessi_choose_type','=',2)->count();


      $count_trial_count  = Case_Registration::leftJoin('adv_pessi', function($join) {$join->on('reg_id', '=', 'pessi_case_reg');})->where('adv_case_reg.user_admin_id','=',session('admin_id'))->where('pessi_status',1)->where('pessi_choose_type','=',0)->where('reg_court','=',1)->where('pessi_further_date','=', date("Y-m-d") )->count();

      $count_trial_civil  = Case_Registration::leftJoin('adv_pessi', function($join) {$join->on('reg_id', '=', 'pessi_case_reg');})->where('adv_case_reg.user_admin_id','=',session('admin_id'))->where('pessi_status',1)->where('pessi_choose_type','=',0)->where('reg_court','=',1)->where('adv_case_reg.reg_case_type_category','=',2)->where('pessi_further_date','=', date("Y-m-d") )->count();

      $count_trial_criminal  = Case_Registration::leftJoin('adv_pessi', function($join) {$join->on('reg_id', '=', 'pessi_case_reg');})->where('adv_case_reg.user_admin_id','=',session('admin_id'))->where('pessi_status',1)->where('pessi_choose_type','=',0)->where('reg_court','=',1)->where('adv_case_reg.reg_case_type_category','=',1)->where('pessi_further_date','=', date("Y-m-d") )->count();

      $Count_high_court  = Case_Registration::leftJoin('adv_pessi', function($join) {$join->on('reg_id', '=', 'pessi_case_reg');})->where('adv_case_reg.user_admin_id','=',session('admin_id'))->where('pessi_status',1)->where('pessi_choose_type','=',0)->where('reg_court','=',2)->where('pessi_further_date','=', date("Y-m-d") )->count();

      $Count_high_civil  = Case_Registration::leftJoin('adv_pessi', function($join) {$join->on('reg_id', '=', 'pessi_case_reg');})->where('adv_case_reg.user_admin_id','=',session('admin_id'))->where('pessi_status',1)->where('pessi_choose_type','=',0)->where('reg_court','=',2)->where('adv_case_reg.reg_case_type_category','=',2)->where('pessi_further_date','=', date("Y-m-d") )->count();

      $Count_high_criminal  = Case_Registration::leftJoin('adv_pessi', function($join) {$join->on('reg_id', '=', 'pessi_case_reg');})->where('adv_case_reg.user_admin_id','=',session('admin_id'))->where('pessi_status',1)->where('pessi_choose_type','=',0)->where('reg_court','=',2)->where('adv_case_reg.reg_case_type_category','=',1)->where('pessi_further_date','=', date("Y-m-d") )->count();


      $message = "Today you have $count_trial_count cases in TC: $count_trial_civil civil, $count_trial_criminal Criminal. Today you have $Count_high_court cases in HC: $Count_high_civil civil, $Count_high_criminal Criminal."; 
     
      // print_r($message);
      // die;

      $date1 = date_create(date("Y-m-d"));

      $a = 1;
      $b = 1;
      $c = 1;
      $d = 1;
      $two_five    = 0;
      $zero_two    = 0;
      $five_ten    = 0;
      $greater_ten = 0;

      foreach ($progress_case as $progress_cases ) {
        
        $date2       = date_create($progress_cases->reg_date);
        $diff        = date_diff($date1,$date2);

        if($diff->y >= 0 && $diff->y <= 2) {
            $zero_two = $a++;
        }

        if($diff->y >= 3 && $diff->y <= 5) {
          $two_five = $b++;
        }

        if($diff->y >= 6 && $diff->y <= 10) {
            $five_ten = $c++;
        }

        if($diff->y >= 11 ) {
            $greater_ten = $d++;
        }
  
      }



	    $account         =     Account::all();
	    $admin_details   =     Admin::where(['admin_id' => session('admin_id')])->get();
	 	  $title           =     "Dashboard";
	    $data            =     compact('title','permission','admin_details','account','total_case','progress_case','completed_case','due_cause','civil_case','criminal_case','zero_two','two_five','five_ten','greater_ten','message');
	    return view('advocate_admin/dashboard',$data);

	  }

    public function logout(){
      session()->forget('admin_id');
      return redirect('/');
    }


    public function change_password(Request $request){

      $account     = Account::get();
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();

      if($request['new_password'] != ""){
   
        $admin_id = session('admin_id');
        $admin_login = Admin::where(['admin_password' => md5($request['current_password']), 'admin_id' => $admin_id ])->count();

        if($admin_login == ""){
          $message = "Current password doesn't match";
          $confirm_message = "";
        }else{
          $values=array(
              'admin_password'          => md5($request['new_password']),
          );
          Admin::where('admin_id', $admin_id)->update($values);
          $message = "";
          $confirm_message = "Password Changed successfully";
        }

        $title      = "Change Password";
        $data       = compact('title','message','confirm_message','account','admin_details');
        return view('advocate_admin/change_password',$data);
        
      }else{
        $message = "";
        $confirm_message = "";
        $title      = "Change Password";
        $data       = compact('title','message','confirm_message','account','admin_details');
        return view('advocate_admin/change_password',$data);
      }
    }

    public function account_settings(Request $request){

      if($request['account_name'] != ""){
        $values=array(
          'account_name'        => $request['account_name'],
          'account_number'      => $request['account_number'],
          'account_free_trail'      => $request['account_free_trail'],
          'account_email'       => $request['account_email'],
          'account_address'     => $request['account_address'],
        );
        Account::where('account_id', 1)->update($values);
        $file = $request->file('account_logo');
        if($file != ""){
          $destinationPath = 'uploads/admin/';
          $imageName = 'uploads/admin/'.time().'.'.$file->getClientOriginalExtension();
          $file->move($destinationPath,$imageName);
          $images=array(
              'account_logo'       => $imageName
          );
          Account::where('account_id', 1)->update($images);
        }  
        return redirect()->back()->with('success', 'Your account settings has been updated successfully.');       
      }

      $confirm_upadate = "Your account settings has been updated successfully.";
      $account     = Account::get();
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();
      $id = session('admin_id');
    
      $title      = "Account Settings";
      $data       = compact('title','account','admin_details');
      return view('advocate_admin/account_settings',$data);
    }

    public function admin_profile(Request $request){

      if($request['admin_name'] != ""){
        $values=array(
            'admin_name'       =>   $request['admin_name'],
            'admin_number'     =>   $request['admin_number'],
            'admin_address'    =>   $request['admin_address'],
            'admin_degree'     =>   $request['admin_degree'],
            'admin_court'      =>   $request['admin_court'],
            'admin_firmname'   =>   $request['admin_firmname'],
            'admin_advocates'  =>   $request['admin_advocates'],
        );
        Admin::where('admin_id', session('admin_id'))->update($values);

        $file = $request->file('admin_image');
        if($file != ""){
          $destinationPath = 'uploads/admin/';
          $imageName = 'uploads/admin/'.time().'.'.$file->getClientOriginalExtension();
          $file->move($destinationPath,$imageName);
          $image=array(
              'admin_image'         => $imageName
          );
          Admin::where('admin_id', session('admin_id'))->update($image);
        }     
        return redirect()->back()->with('success', 'Your profile has been updated successfully.');       
      }
      
      $account     = Account::get();
      $id = session('admin_id');
      $get_record = Admin::where('admin_id',$id)->get();
      $admin_details = Admin::where(['admin_id' => session('admin_id')])->get();
      $admin_detail = Admin::where(['admin_id' => session('admin_id')])->first();

      $get_court = Court::where('user_admin_id','=',session('admin_id'))->orderBy('court_id', 'desc')->get();

      $title      = "Profile";
      $data       = compact('title','admin_detail','account','admin_details','get_court');
      return view('advocate_admin/profile',$data);
    }



    public function send_message($mobile_no,$message){

    file_get_contents('http://bulksms.wscubetech.com/api/sendmsg.php?user=DEMACD&pass=demacd@123!&sender=DEMACD&phone='.htmlentities($mobile_no).'&text='.urlencode($message).'&priority=ndnd&stype=normal');
    }



}
