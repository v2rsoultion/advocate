@extends('advocate_admin/layout')
@section('content')

  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="{{ url('/advocate-panel/view-notice-issue') }}">View Notice Issue</a>
            </li>
            <li class="crumb-icon">
              <a href="{{ url('advocate-panel/dashboard') }}">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="{{ url('advocate-panel/dashboard') }}">Home</a>
            </li>
            <li class="crumb-trail">Add Notice Issue</li>
          </ol>
        </div>
      </header>

      <!-- Begin: Content -->

      <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center">
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-heading">
                <span class="panel-title">Add Notice Issue</span>
              </div>
              <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                <!-- IF any error found -->
                <div class="col-md-12">
                @if ($errors->any())
                  <div id="log_error" class="alert alert-danger" style='font-family: josefin_sansregular !important;'>
                  {{$errors->first()}}  </i></div>
                @endif
                </div>
                <!-- IF any error found -->
                  <div id="tab1_1" class="tab-pane active">
                    {!! Form::open(['name'=>'form_add_question','url'=>'advocate-panel/insert-notice-issue/'.$get_record[0]->notice_id,'id'=>'form_add_question' ,'autocomplete'=>'off']) !!}
                    <div class="row"> 
                        <div class="col-md-6">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Case No : </label>
                              <label for="artist_state" class="field select">
                                <select class="select2-single form-control" name="case_no" id="user_role" onchange="notice_case_no_ajax(this.value)">
                                  <option value=''>--Select-Option--</option>
                                  @if($get_record != "") 
                                   @foreach($notice_case_no as $notice_case_nos) 
                                     <option value="{{ $notice_case_nos->reg_id }}" {{ $notice_case_nos->reg_id == $get_record[0]->notice_case_no ? 'selected="selected"' : '' }} >{{$notice_case_nos->reg_case_number}} </option>
                                   @endforeach
                                   @else
                                   @foreach($notice_case_no as $notice_case_nos)
                                     <option value="{{ $notice_case_nos->reg_id }}">{{$notice_case_nos->reg_case_number}} </option>
                                   @endforeach
                                   @endif
                                </select>
                              </label>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Title :  </label>
                            <label for="level_name" class="field prepend-icon" id="">
                              {!! Form::text('title','',array('class' => 'gui-input','placeholder' => '','id'=>'notice_caseno_change','disabled'=>'disabled' )) !!}
                              <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-pencil"></i>
                              </label>
                            </label>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Notice Issue Date :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              {!! Form::text('notice_issue',$get_record[0]->notice_issue_date,array('class' => 'gui-input','placeholder' => '','id'=>'registerdate' )) !!}
                              <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-calendar"></i>
                              </label>
                            </label>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Status : </label>
                              <label for="artist_state" class="field select">
                                <select class="form-control" name="status_date_s">
                                  <option value=''>--Select-Option--</option>
                                  <option value='2' <?php if($get_record[0]->notice_status_date == "2"){ echo "selected";}?>>Filled</option>
                                  <option value='1' <?php if($get_record[0]->notice_status_date == "1"){ echo "selected";}?>>Pending</option>                                
                                </select>
                                <i class="arrow double"></i>
                              </label>
                          </div>
                        </div>
                      </div>

                      <div class="panel-footer text-right">
                          {!! Form::submit('Save', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}
                          {!! Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}
                      </div>   
                      {!! Form::close() !!}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>


<script type="text/javascript">

$(document).on('click','.mysave',function(){

    var case_no = $("#case_no").val();
    
    if(case_no == 0){
      $('#descrption_error').html('<div class="state-error"></div><em for="services_excerpt" class="state-error">Please select case no.</em>');
      $(".select2-selection").css({"border": "1px solid #de888a"});
      $(".select2-selection").css({"background": "#fee9ea"});
      return false;
    }
  })



function notice_case_no_ajax(notice_caseno_id){

  if (notice_caseno_id != ''){
    BASE_URL = '{{ url('/')}}';
    $.ajax({
      url:BASE_URL+"/advocate-panel/change-notice-case-no-ajax/"+notice_caseno_id,
      success: function(result){
        $("#notice_caseno_change").val(result.petitioner+' V/S '+result.respondent);
      }
    });
  }
}

</script>

<script>
function showDiv(show_id) {

  if (show_id > 1) {
    document.getElementById('welcomeDiv').style.display = "block";
  }else if (show_id = 1){
      document.getElementById('welcomeDiv').style.display = "none";
  }

}
</script>

<script type="text/javascript">
    function showhide(){
     
    var div = document.getElementById("myDIV");
    if (div.style.display !== "none") {
        div.style.display = "none";
    }
      div.style.display = "none";
    }
     
</script>

<style>
#subtype {
    width: 100%;
    padding: 50px 0;
    text-align: left !important;
    margin-top: 20px;
    display:none;
}
#myDIV {
    width: 100%;
    padding: 50px 0;
    text-align: left !important;
    margin-top: 20px;
    display:none;
}
</style>
<script>
function myFunctionsub() {
    var x = document.getElementById("subtype");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}
</script>
<script type="text/javascript">

  jQuery(document).ready(function() {
  
    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "This field contains alphabets only");

    jQuery.validator.addMethod("checkemail", function(value, element) 
    {
    return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
    }, "Enter a VALID email address"); 

    $.validator.addMethod('minStrict', function (value, el, param) {
     return value > param; 
    });
    $.validator.addMethod('maxStrict', function (value, el, param) {
     return value < param; 
    });


    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#form_add_question").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        status_date_s: {
          required: true
        },
        case_no: {
          required: true
        },
        title: {
          required: true
        },
        notice_issue: {
          required: true
        },
      },

      /* @validation error messages 
      ---------------------------------------------- */

      messages: {
        status_date_s: {
          required: 'Please Fill Required Choose Status Date'
        },
        case_no: {
          required: 'Please Fill Required Choose No'
        },
        title: {
          required: 'Please Fill Required Title'
        },
        notice_issue: {
          required: 'Please Fill Required Notice Issue'
        },
      },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });


  function addRecords(){
    var count1 = $('#countid1').val();
    var count2 = $('#countid2').val();


    var counter1 = parseInt(count1) + 1;
    var counter2 = parseInt(count2) + 1;
    $('#countid1').val(counter1);
    $('#countid2').val(counter2);

    if(count2 >= 0){
      $('#questionrow').append('<div class="questiondiv" id="questiondiv'+count1+'"><div class="divider"></div><div class="col-md-12"><div class="section"><label for="level_name" class="field-label" style="font-weight:600;" >Extra Party : </label><label for="level_name" class="field prepend-icon"><input type="text" name="extra_party" value="" class="gui-input" placeholder="" required><label for="Account Mobile" class="field-icon"><i class="fa fa-pencil"></i></label></label></div></div><div class="section row mb5 customClasss"><div class=""> <button type="button" onclick="removeRecords('+count1+');" name="add" id="add" class="btn btn-danger pull-right mr30"> <i class="fa fa-trash-o" aria-hidden="true"></i> &nbsp; Remove Fields </button></div></div></div></div>');
       // $('#countid').val(count);
    }
  }

  function removeRecords(countnew){
    $( "#questiondiv"+countnew+"" ).remove();
    var count2 = $('#countid2').val();
    var counter2 = parseInt(count2) - 1;
    $('#countid2').val(counter2);
  }


  </script>



@endsection
