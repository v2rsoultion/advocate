@extends('advocate_admin/layout')
@section('content')
<style type="text/css">
  .admin-form .select, .admin-form .gui-input, .admin-form .select > select, .admin-form .select-multiple select{
    height: 28px !important;
  }
  .admin-form a.button, .admin-form span.button, .admin-form label.button
  {
    line-height: 28px !important;
  }
  .admin-form .append-icon .field-icon, .admin-form .prepend-icon .field-icon{
    line-height: 28px !important;
  }
  .admin-form .gui-textarea {
    line-height: 7px !important;
  }
  .select2-container .select2-selection--single {
    height: 28px !important;
  }
  .select2-container--default .select2-selection--single .select2-selection__rendered {
    line-height: 24px !important;
  }
  .select2-container .select2-selection--multiple {
    min-height: 30px !important;
  }
  .select2-container--default .select2-selection--single .select2-selection__arrow {
    top: -4px !important;
  }
/*  .gui-textarea{
    height: 100px !important;
  }*/
/* .admin-form .gui-input {
  padding: 5px;
 }*/
 /*.admin-form .prepend-icon .field-icon {
  top: 6px;
 }*/
 .form-control {
    height: 28px !important;
    /*padding: 0px 12px !important;*/
  }
 .admin-form .button{
    height: 28px !important;
    line-height: 1px !important;
  }
 .btn {
    height: 29px !important;
    line-height: 1px !important;  }

 .down{
  margin-top: 70px;
 }
.account_setting {
  margin: 0px 20px !important;
  margin-top: 25px !important;
  /*padding: 5px 0px !important;*/
 }
 .select-text{
  padding: 3px 10px !important;
 }
 .admin-form .select .arrow{
  top: 2px !important;
 }
 .ui-datepicker select.ui-datepicker-month, .ui-datepicker select.ui-datepicker-year{
  width: 47%;
  margin-left: 4px;
 }
</style>
  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left down">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="{{ url('/advocate-panel/view-compliance') }}">View Compliance</a>
            </li>
            <li class="crumb-icon">
              <a href="{{ url('advocate-panel/dashboard') }}">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="{{ url('advocate-panel/dashboard') }}">Home</a>
            </li>
            <li class="crumb-trail">Add Compliance</li>
          </ol>
        </div>
      </header>

      <div class="row">
        <div class="col-md-12">
          @if (\Session::has('success'))
          <div class="alert alert-success account_setting" >
            {!! \Session::get('success') !!}
          </div>
          @endif
        </div>
      </div>
      <!-- Begin: Content -->

      <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center">
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-heading">
                <span class="panel-title">Add Compliance</span>
              </div>
              <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                <!-- IF any error found -->
                <div class="col-md-12">
                @if ($errors->any())
                  <div id="log_error" class="alert alert-danger" style='font-family: josefin_sansregular !important;'>
                  {{$errors->first()}}  </i></div>
                @endif
                </div>
                <!-- IF any error found -->
                  <div id="tab1_1" class="tab-pane active">
                    
                    {!! Form::open(['name'=>'form_add_question','url'=>'advocate-panel/insert-compliance/'.$get_record[0]->compliance_id,'id'=>'form_add_question','files'=>'ture' ,'autocomplete'=>'off']) !!}

                    <div class="row">
                        
                      <!-- <div class="col-md-12">
                        <div class="section" style="margin-bottom: 40px;">
                          <label for="level_name" class="field-label" style="font-weight:600;" > Category :  </label>  
                          <div class="col-md-2">
                            <label class="option" style="font-size:12px;">

                              @if($get_record[0]->compliance_category != "")
                                {!! Form::radio('compliance_category','1',$get_record[0]->compliance_category == 1 ? 'checked' : '', array( 'class' => 'check', 'id' => 'check1' ,'onclick' => 'change_case(this.value)', disabled )) !!}
                              @else 
                                {!! Form::radio('compliance_category','1',checked, array( 'class' => 'check' , 'onclick' => 'change_case(this.value)' , 'class' => 'check' ,'id' => 'check1' )) !!}
                              @endif
                              <span class="radio"></span> Criminal
                            </label>
                          </div>

                          <div class="col-md-2">
                            <label class="option" style="font-size:12px;">

                              @if($get_record[0]->compliance_category != "")
                                {!! Form::radio('compliance_category','2',$get_record[0]->compliance_category == 2 ? 'checked' : '', array( 'class' => 'check' ,'id' => 'check2' ,'onclick' => 'change_case(this.value)',disabled )) !!}
                              @else 
                                {!! Form::radio('compliance_category','2',$get_record[0]->compliance_category == 2 ? 'checked' : '', array( 'class' => 'check' ,'id' => 'check2' ,'onclick' => 'change_case(this.value)' )) !!}
                              @endif

                              
                              <span class="radio"></span> Civil
                            </label>
                          </div>
                        </div>

                        <div class="section"></div>
                        <div class="clearfix"></div>
                      </div> -->

                      <div id="">    
                        <div class="col-md-2">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Court Type  : </label>
                              <!-- <label for="artist_state" class="field select"> -->
                                <label for="level" class="field prepend-icon">
                                @if($get_record != "")
                                  <select class="form-control select-text" name="court_type" onclick="choose_court(this.value)" id="court_type" disabled="disabled">
                                @else 
                                  <select class="form-control select-text" name="court_type" onchange="choose_court(this.value)" id="court_type" >
                                @endif

                                  <option value="">Court Type</option>
                                  <option value="1" {{ $get_record[0]->compliance_court_type == 1 ? 'selected="selected"' : '' }} >Trial Court</option>
                                  <option value="2" {{ $get_record[0]->compliance_court_type == 2 ? 'selected="selected"' : '' }} >High Court</option>
                                </select>
                                <i class="arrow"></i>
                              </label>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-3">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Court Name  : </label>
                              <!-- <label for="artist_state" class="field select"> -->
                                <label for="level" class="field prepend-icon">
                                @if($get_record != "")
                                  <select class="form-control select-text" name="court_name" onclick="choose_court_name(this.value)" id="court_name" disabled="disabled">
                                @else 
                                  <select class="form-control select-text" name="court_name" onchange="choose_court_name(this.value)" id="court_name" >
                                @endif
                                  <option value="">Select Court Name</option>
                                    
                                    @foreach($court_all as $court_alls) 
                                      <option value="{{ $court_alls->court_id }}" {{ $court_alls->court_id == $get_record[0]->reg_court_id ? 'selected="selected"' : '' }} > {{$court_alls->court_name }} </option>
                                    @endforeach

                                </select>
                                <i class="arrow"></i>
                              </label>
                          </div>
                        </div>
                        
                        <div class="col-md-3">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Type of Case : </label>
                            <!-- <label for="artist_state" class="field select"> -->
                              <label for="level" class="field prepend-icon">
                              @if($get_record == "") 
                                <select class="form-control select-text" id="choose_case_type" name="choose_case_type" onchange="get_type_of_case(this.value)">
                              @else 
                                <select class=" form-control select-text" id="choose_case_type" name="choose_case_type" onclick="get_type_of_case(this.value)" disabled="disabled">
                              @endif
                                <option value=''>Select Type Of Case</option>
                                
                                @if($get_record != "") 
                                  @foreach($case_type_entry_all as $case_type_entrys) 
                                   <option value="{{ $case_type_entrys->case_id }}" {{ $case_type_entrys->case_id == $get_record[0]->compliance_case_type ? 'selected="selected"' : '' }} >{{$case_type_entrys->case_name}} </option>
                                  @endforeach
                                @else
                                  @foreach($case_type_entry as $case_type_entrys)
                                     <option value="{{ $case_type_entrys->case_id }}">{{$case_type_entrys->case_name}} </option>
                                  @endforeach
                                @endif

                              </select>
                              <i class="arrow double"></i>
                            </label>
                          </div>
                        </div>


                        <div class="col-md-2">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Case No : </label>
                              <!-- <label for="artist_state" class="field"> -->
                                <label for="level" class="field prepend-icon">
                                @if($get_record != "") 
                                  <select class="form-control select-text" id="case_no" name="case_no" onclick="choose_case_no(this.value)"  disabled="disabled">
                                    <option value='0'>Select Case No.</option>    
                                 
                                      @foreach($get_case_regestered_all as $get_case_regestereds)
                                        <option value="{{ $get_case_regestereds->reg_id }}" {{ $get_case_regestereds->reg_id == $get_record[0]->compliance_case_id ? 'selected="selected"' : '' }} > {{ $get_case_regestereds->reg_case_number }}</option>
                                      @endforeach
                                    
                                  </select>
                                @else
                                  <select class="form-control select-text" id="case_no" name="case_no" onchange="choose_case_no(this.value)">
                                    <option value=''>Select Case No.</option> 

                                      @foreach($get_case_number_all as $get_case_numbers)
                                        <option value="{{ $get_case_numbers->reg_id }}" {{ $get_case_numbers->reg_id == $get_record[0]->compliance_case_id ? 'selected="selected"' : '' }} > {{ $get_case_numbers->reg_case_number }}</option>
                                      @endforeach 
  
                                  </select>
                                @endif

                                <i class="arrow double"></i>
                              </label>
                              <div id="descrption_error" class="" ></div>
                          </div>
                        </div>

                        
                        <div class="col-md-2">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;"> File No : </label>
                            <!-- <label for="artist_state" class="field select"> -->
                              <label for="level" class="field prepend-icon">
                              @if($get_record != "")
                                <select class="form-control select-text" name="order_judgment_file_no" onchange="get_detail(this.value)" id="order_judgment_file_no" disabled="disabled">
                              @else 
                                <select class="form-control select-text" name="order_judgment_file_no" onchange="get_detail(this.value)" id="order_judgment_file_no">
                              @endif

                              
                                <option value=''>Select File No </option>
                                @if($get_record != "") 
                                 @foreach($get_case_regestered_all as $get_case_regestereds)
                                        <option value="{{ $get_case_regestereds->reg_id }}" {{ $get_case_regestereds->reg_id == $get_record[0]->compliance_case_id ? 'selected="selected"' : '' }} > {{ $get_case_regestereds->reg_file_no }}</option>
                                      @endforeach
                                @else
                                 @foreach($get_case_regestered_all as $case_registrations)
                                   <option value="{{ $case_registrations->reg_id }}">{{$case_registrations->reg_file_no}} </option>
                                 @endforeach
                                 @endif                                 
                              </select>
                              <i class="arrow"></i>
                            </label>
                        </div>
                      </div>

                        

                        
                        
                        <div class="col-md-3">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;"> Title of Case : </label>
                              <!-- <label for="artist_state" class="field select"> -->
                                <label for="level" class="field select">
                                {!! Form::text('case_title',$get_record->case_title,array('class' => 'gui-input','id' => 'case_title' , disabled )) !!}

                              </label>
                          </div>
                        </div>

                        <div class="col-md-12">
                          <div class="section" style="margin-bottom: 40px;">
                            <div class="col-md-2">
                              <label class="option block mn" style="font-size:12px;">
                                {!! Form::checkbox('compliance_type_notice','1',$get_record[0]->compliance_type_notice == 1 ? 'checked' : '', array('onclick' => 'show_notice()', 'id' => 'compliance_type_notice' ,'class' => 'check' )) !!}
                                <span class="checkbox mn"></span> Notice Issue
                              </label>
                            </div>

                            <div class="col-md-2">
                              <label class="option block mn" style="font-size:12px;">
                                {!! Form::checkbox('compliance_type_date','1',$get_record[0]->compliance_type_date == 1 ? 'checked' : '', array('onclick' => 'show_date()', 'id' => 'compliance_type_date' , 'class' => 'check' )) !!}
                                <span class="checkbox mn"></span> Date
                              </label>
                            </div>

                            <div class="col-md-2">
                              <label class="option block mn" style="font-size:12px;">
                                {!! Form::checkbox('compliance_type_defect_case','1',$get_record[0]->compliance_type_defect_case == 1 ? 'checked' : '', array('onclick' => 'show_defect()', 'id' => 'compliance_type_defect_case' , 'class' => 'check' )) !!}
                                <span class="checkbox mn"></span> Defect Case
                              </label>
                            </div>

                            <div class="col-md-2">
                              <label class="option block mn" style="font-size:12px;">
                                {!! Form::checkbox('compliance_type_reply','1',$get_record[0]->compliance_type_reply == 1 ? 'checked' : '', array('onclick' => 'show_reply()', 'id' => 'compliance_type_reply' , 'class' => 'check' )) !!}
                                <span class="checkbox mn"></span> Reply
                              </label>
                            </div>

                            <div class="col-md-2">
                              <label class="option block mn" style="font-size:12px;">
                                {!! Form::checkbox('compliance_type_certified','1',$get_record[0]->compliance_type_certified == 1 ? 'checked' : '', array('onclick' => 'show_certified()', 'id' => 'compliance_type_certified' , 'class' => 'check' )) !!}
                                <span class="checkbox mn"></span> Apply Date
                              </label>
                            </div>

                            <div class="col-md-2">
                              <label class="option block mn" style="font-size:12px;">
                                {!! Form::checkbox('compliance_type_other','1',$get_record[0]->compliance_type_other == 1 ? 'checked' : '', array('onclick' => 'show_other()', 'id' => 'compliance_type_other' , 'class' => 'check' )) !!}
                                <span class="checkbox mn"></span> Other
                              </label>
                            </div>
                          </div>
                          <div id="compliance_error" class="" ></div>
                          <div class="section"></div>
                          <div class="clearfix"></div>
                        </div>

                        <!-- notice issue section -->

                        <div @if($get_record[0]->compliance_type_notice == 1)  @else style="display: none;" @endif id="show_compliance_type_notice">
                          <section id="content" class="table-layout animated fadeIn" style="margin-bottom: -75px;">
                            <div class="">
                              <div class="center-block">
                                <div class="panel panel-primary panel-border top " style="border: 1px solid lightgray;">
                                  <div class="panel-heading" style="padding: 5px 15px;">
                                    <span class="panel-title" style="font-size: 18px;"> Notice Issue</span>
                                  </div>
                                  <div class="panel-body bg-light dark">
                                    <div class="tab-content pn br-n admin-form">
                                      <div id="tab1_1" class="tab-pane active">
                                        <div class="row"> 
                                          <div class="col-md-4">
                                            <div class="section section_from">
                                              <label for="level_name" class="field-label" style="font-weight:600;" >Issue date :  </label>  
                                              <label for="level_name" class="field prepend-icon" id="discount_date_from_lb">
                                                
                                                @if($get_record[0]->notice_issue_date == "" || $get_record[0]->notice_issue_date == "1970-01-01" )
                                                  {!! Form::text('notice_issue_date','',array('class' => 'gui-input fromdate_notice','placeholder' => '','id'=>'notice_issue_date' )) !!}
                                                @else
                                                  {!! Form::text('notice_issue_date',date('d-m-Y',strtotime($get_record[0]->notice_issue_date)),array('class' => 'gui-input fromdate_notice' , 'placeholder' => '','id'=>'notice_issue_date' )) !!}
                                                @endif
                                                
                                                <label for="Account Mobile" class="field-icon">
                                                  <i class="fa fa-calendar"></i>
                                                </label>
                                              </label>
                                            </div>
                                          </div>
                                          <div class="col-md-4">
                                            <div class="section">
                                              <label for="level_name" class="field-label" style="font-weight:600;"> Status : </label>
                                                <!-- <label for="artist_state" class="field select"> -->
                                              <label for="level" class="field prepend-icon">
                                                <div class="section">
                                                  <label class="field select">
                                                    <select class="form-control select-text" name="notice_status" id="notice_status" onchange="chnage_notice_date(this.value)">
                                                      <option value=''>Select Status</option>
                                                      <option value='1' <?php if($get_record[0]->notice_status == "1"){ echo "selected";}?>>Pending</option>
                                                      <option value='2' <?php if($get_record[0]->notice_status == "2"){ echo "selected";}?>>Complete</option>
                                                                                      
                                                    </select>
                                                    <i class="arrow double"></i>
                                                  </label>
                                                </div>
                                              </label>
                                            </div>
                                          </div>

                                          <div class="col-md-4" style="display: none;" id="show_notice_date">
                                            <div class="section section_from">
                                              <label for="level_name" class="field-label" style="font-weight:600;"> Complete Date :  </label>  
                                              <label for="level_name" class="field prepend-icon" id="discount_date_from_lb">
                                                
                                                @if($get_record[0]->notice_status_date == "" || $get_record[0]->notice_status_date == "1970-01-01" )
                                                  {!! Form::text('notice_status_date','',array('class' => 'gui-input todate_notice','placeholder' => '','id'=>'notice_status_date' )) !!}
                                                @else
                                                  {!! Form::text('notice_status_date',date('d-m-Y',strtotime($get_record[0]->notice_status_date)),array('class' => 'gui-input todate_notice','placeholder' => '','id'=>'notice_status_date' )) !!}
                                                @endif
                                                
                                                <label for="Account Mobile" class="field-icon">
                                                  <i class="fa fa-calendar"></i>
                                                </label>
                                              </label>
                                            </div>
                                          </div>

                                          <script type="text/javascript">
                                            function chnage_notice_date(status_value){
                                              if(status_value == 2){
                                                document.getElementById("show_notice_date").style.display = "block";
                                              } else {
                                                document.getElementById("show_notice_date").style.display = "none";
                                              }
                                            }
                                          </script>

                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </section>
                        </div>

                        <!-- Date section -->

                        <div @if($get_record[0]->compliance_type_date == 1)  @else style="display: none;" @endif id="show_compliance_type_date">
                          <section id="content" class="table-layout animated fadeIn" style="margin-bottom: -75px;">
                            <div class="">
                              <div class="center-block">
                                <div class="panel panel-primary panel-border top " style="border: 1px solid lightgray;">
                                  <div class="panel-heading" style="padding: 5px 15px;">
                                    <span class="panel-title" style="font-size: 18px;"> Date</span>
                                  </div>
                                  <div class="panel-body bg-light dark">
                                    <div class="tab-content pn br-n admin-form">
                                      <div id="tab1_1" class="tab-pane active">
                                        <div class="row"> 

                                          <div class="col-md-12">
                                            <div class="section" style="margin-bottom: 40px;">
                                              <div class="col-md-2">
                                                <label class="option" style="font-size:12px;">
                                                  
                                                  @if($get_record[0]->compliance_date_type == "")
                                                    {!! Form::radio('compliance_date_type','1', 'checked' , array( 'class' => 'check' )) !!}
                                                  @else 
                                                    {!! Form::radio('compliance_date_type','1',$get_record[0]->compliance_date_type == 1 ? 'checked' : '', array( 'class' => 'check' )) !!}
                                                  @endif
                                                  
                                                  <span class="radio"></span> Short Date
                                                </label>
                                              </div>

                                              <div class="col-md-2">
                                                <label class="option" style="font-size:12px;">
                                                  {!! Form::radio('compliance_date_type','2',$get_record[0]->compliance_date_type == 2 ? 'checked' : '', array( 'class' => 'check' )) !!}
                                                  <span class="radio"></span> Long Date
                                                </label>
                                              </div>
                                            </div>

                                            <div class="section"></div>
                                            <div class="clearfix"></div>
                                          </div>

                                          <div class="col-md-4">
                                            <div class="section">
                                              <label for="level_name" class="field-label" style="font-weight:600;"> Status : </label>
                                                <!-- <label for="artist_state" class="field select"> -->
                                              <label for="level" class="field prepend-icon">
                                                <div class="section">
                                                  <label class="field select">
                                                    <select class="form-control select-text" name="compliance_status" id="compliance_status" onchange="chnage_compliance_date(this.value)">
                                                      <option value=''>Select Status</option>
                                                      <option value='1' <?php if($get_record[0]->compliance_status == "1"){ echo "selected";}?>>Pending</option>
                                                      <option value='2' <?php if($get_record[0]->compliance_status == "2"){ echo "selected";}?>>Complete</option>
                                                                                      
                                                    </select>
                                                    <i class="arrow double"></i>
                                                  </label>
                                                </div>
                                              </label>
                                            </div>
                                          </div>

                                          <div class="col-md-4">
                                            <div class="section section_from_2">
                                              <label for="level_name" class="field-label" style="font-weight:600;"> Date :  </label>  
                                              <label for="level_name" class="field prepend-icon" id="discount_date_from_lb_2">
                                                
                                                @if($get_record[0]->compliance_date == "" || $get_record[0]->compliance_date == "1970-01-01")
                                                  {!! Form::text('compliance_date','',array('class' => 'gui-input fromdate_search','placeholder' => '','id'=>'compliance_date' )) !!}
                                                @else
                                                  {!! Form::text('compliance_date',date('d-m-Y',strtotime($get_record[0]->compliance_date)),array('class' => 'gui-input fromdate_search','placeholder' => '','id'=>'compliance_date' )) !!}
                                                @endif

                                                <label for="Account Mobile" class="field-icon">
                                                  <i class="fa fa-calendar"></i>
                                                </label>
                                              </label>
                                            </div>
                                          </div>

                                          <div class="col-md-4" style="display: none;" id="show_compliance_date">
                                            <div class="section section_from">
                                              <label for="level_name" class="field-label" style="font-weight:600;"> Complete Date :  </label>  
                                              <label for="level_name" class="field prepend-icon" id="discount_date_from_lb">
                                                
                                                @if($get_record[0]->compliance_date_status == "" || $get_record[0]->compliance_date_status == "1970-01-01" )
                                                  {!! Form::text('compliance_date_status','',array('class' => 'gui-input todate','placeholder' => '','id'=>'compliance_date_status' )) !!}
                                                @else
                                                  {!! Form::text('compliance_date_status',date('d-m-Y',strtotime($get_record[0]->compliance_date_status)),array('class' => 'gui-input todate','placeholder' => '','id'=>'compliance_date_status' )) !!}
                                                @endif
                                                
                                                <label for="Account Mobile" class="field-icon">
                                                  <i class="fa fa-calendar"></i>
                                                </label>
                                              </label>
                                            </div>
                                          </div>

                                          <script type="text/javascript">
                                            function chnage_compliance_date(status_value){
                                              if(status_value == 2){
                                                document.getElementById("show_compliance_date").style.display = "block";
                                              } else {
                                                document.getElementById("show_compliance_date").style.display = "none";
                                              }
                                            }
                                          </script>
                                          
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </section>
                        </div>


                        <!-- Defect Case -->

                        <div @if($get_record[0]->compliance_type_defect_case == 1)  @else style="display: none;" @endif id="show_compliance_type_defect_case">
                          <section id="content" class="table-layout animated fadeIn" style="margin-bottom: -75px;">
                            <div class="">
                              <div class="center-block">
                                <div class="panel panel-primary panel-border top " style="border: 1px solid lightgray;">
                                  <div class="panel-heading" style="padding: 5px 15px;">
                                    <span class="panel-title" style="font-size: 18px;"> Defect Case </span>
                                  </div>
                                  <div class="panel-body bg-light dark">
                                    <div class="tab-content pn br-n admin-form">
                                      <div id="tab1_1" class="tab-pane active">
                                        <div class="row"> 
                                          <!-- <div class="col-md-6">
                                            <div class="section">
                                              <label for="level_name" class="field-label" style="font-weight:600;"> Act Reason :  </label>  
                                              <label for="level_name" class="field prepend-icon">
                                                {!! Form::text('compliance_act_reason',$get_record[0]->compliance_act_reason,array('class' => 'gui-input','placeholder' => '','id'=>'compliance_act_reason' )) !!}
                                                <label for="Account Mobile" class="field-icon">
                                                  <i class="fa fa-pencil"></i>
                                                </label>
                                              </label>
                                            </div>
                                          </div> -->
                                          <div class="col-md-6">
                                            <div class="section">
                                              <label for="level_name" class="field-label" style="font-weight:600;"> Status : </label>
                                                <!-- <label for="artist_state" class="field select"> -->
                                              <label for="level" class="field prepend-icon">
                                                <div class="section">
                                                  <label class="field select">
                                                    <select class="form-control select-text" name="compliance_defect_status" id="compliance_defect_status" onchange="chnage_defect_date(this.value)">
                                                      <option value=''>Select Status</option>
                                                      
                                                      <option value='1' <?php if($get_record[0]->compliance_defect_status == "1"){ echo "selected";}?>>Pending</option>
                                                      <option value='2' <?php if($get_record[0]->compliance_defect_status == "2"){ echo "selected";}?>>Complete</option>
                                                                                      
                                                    </select>
                                                    <i class="arrow double"></i>
                                                  </label>
                                                </div>
                                              </label>
                                            </div>
                                          </div>

                                          <div class="col-md-4" style="display: none;" id="show_defect_date">
                                            <div class="section section_from">
                                              <label for="level_name" class="field-label" style="font-weight:600;"> Complete Date :  </label>  
                                              <label for="level_name" class="field prepend-icon" id="discount_date_from_lb">
                                                
                                                @if($get_record[0]->compliance_defect_date_status == "" || $get_record[0]->compliance_defect_date_status == "1970-01-01" )
                                                  {!! Form::text('compliance_defect_date_status','',array('class' => 'gui-input disposal_from','placeholder' => '','id'=>'' )) !!}
                                                @else
                                                  {!! Form::text('compliance_defect_date_status',date('d-m-Y',strtotime($get_record[0]->compliance_defect_date_status)),array('class' => 'gui-input disposal_from','placeholder' => '','id'=>'' )) !!}
                                                @endif
                                                
                                                <label for="Account Mobile" class="field-icon">
                                                  <i class="fa fa-calendar"></i>
                                                </label>
                                              </label>
                                            </div>
                                          </div>

                                          <script type="text/javascript">
                                            function chnage_defect_date(status_value){
                                              if(status_value == 2){
                                                document.getElementById("show_defect_date").style.display = "block";
                                              } else {
                                                document.getElementById("show_defect_date").style.display = "none";
                                              }
                                            }
                                          </script>

                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </section>
                        </div>
                        
                        <!-- Reply -->

                        <div @if($get_record[0]->compliance_type_reply == 1)  @else style="display: none;" @endif id="show_compliance_type_reply">
                          <section id="content" class="table-layout animated fadeIn" style="margin-bottom: -75px;">
                            <div class="">
                              <div class="center-block">
                                <div class="panel panel-primary panel-border top " style="border: 1px solid lightgray;">
                                  <div class="panel-heading" style="padding: 5px 15px;">
                                    <span class="panel-title" style="font-size: 18px;"> Reply </span>
                                  </div>
                                  <div class="panel-body bg-light dark">
                                    <div class="tab-content pn br-n admin-form">
                                      <div id="tab1_1" class="tab-pane active">
                                        <div class="row"> 
                                          <div class="col-md-4">
                                            <div class="section">
                                              <label for="level_name" class="field-label" style="font-weight:600;"> Reply :  </label>  
                                              <label for="level_name" class="field prepend-icon">
                                                {!! Form::text('compliance_reply',$get_record[0]->compliance_reply,array('class' => 'gui-input','placeholder' => '','id'=>'compliance_reply' )) !!}
                                                <label for="Account Mobile" class="field-icon">
                                                  <i class="fa fa-pencil"></i>
                                                </label>
                                              </label>
                                            </div>
                                          </div>
                                          <div class="col-md-4">
                                            <div class="section">
                                              <label for="level_name" class="field-label" style="font-weight:600;"> Status : </label>
                                                <!-- <label for="artist_state" class="field select"> -->
                                              <label for="level" class="field prepend-icon">
                                                <div class="section">
                                                  <label class="field select">
                                                    <select class="form-control select-text" name="compliance_reply_status" id="compliance_reply_status" onchange="chnage_reply_date(this.value)">
                                                      <option value=''>Select Status</option>
                                                      
                                                      <option value='1' <?php if($get_record[0]->compliance_reply_status == "1"){ echo "selected";}?>>Pending</option>
                                                      <option value='2' <?php if($get_record[0]->compliance_reply_status == "2"){ echo "selected";}?>>Complete</option>
                                                                                      
                                                    </select>
                                                    <i class="arrow double"></i>
                                                  </label>
                                                </div>
                                              </label>
                                            </div>
                                          </div>

                                          <div class="col-md-4" style="display: none;" id="show_reply_date">
                                            <div class="section section_from">
                                              <label for="level_name" class="field-label" style="font-weight:600;"> Complete Date :  </label>  
                                              <label for="level_name" class="field prepend-icon" id="discount_date_from_lb">
                                                
                                                @if($get_record[0]->compliance_reply_date_status == "" || $get_record[0]->compliance_reply_date_status == "1970-01-01" )
                                                  {!! Form::text('compliance_reply_date_status','',array('class' => 'gui-input disposal_from','placeholder' => '','id'=>'compliance_reply_date_status' )) !!}
                                                @else
                                                  {!! Form::text('compliance_reply_date_status',date('d-m-Y',strtotime($get_record[0]->compliance_reply_date_status)),array('class' => 'gui-input disposal_from','placeholder' => '','id'=>'compliance_reply_date_status' )) !!}
                                                @endif
                                                
                                                <label for="Account Mobile" class="field-icon">
                                                  <i class="fa fa-calendar"></i>
                                                </label>
                                              </label>
                                            </div>
                                          </div>

                                          <script type="text/javascript">
                                            function chnage_reply_date(status_value){
                                              if(status_value == 2){
                                                document.getElementById("show_reply_date").style.display = "block";
                                              } else {
                                                document.getElementById("show_reply_date").style.display = "none";
                                              }
                                            }
                                          </script>

                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </section>
                        </div>

                        <!-- Apply Date section -->

                        <div @if($get_record[0]->compliance_type_certified == 1)  @else style="display: none;" @endif id="show_compliance_type_certified">
                          <section id="content" class="table-layout animated fadeIn" style="margin-bottom: -75px;">
                            <div class="">
                              <div class="center-block">
                                <div class="panel panel-primary panel-border top " style="border: 1px solid lightgray;">
                                  <div class="panel-heading" style="padding: 5px 15px;">
                                    <span class="panel-title" style="font-size: 18px;"> Apply Date</span>
                                  </div>
                                  <div class="panel-body bg-light dark">
                                    <div class="tab-content pn br-n admin-form">
                                      <div id="tab1_1" class="tab-pane active">
                                        <div class="row"> 
                                          <div class="col-md-4">
                                            <div class="section section_from_3">
                                              <label for="level_name" class="field-label" style="font-weight:600;" >Apply Date :  </label>  
                                              <label for="level_name" class="field prepend-icon" id="discount_date_from_lb_3">
                                                
                                                @if($get_record[0]->certified_copy_date == "" || $get_record[0]->certified_copy_date == "1970-01-01")
                                                  {!! Form::text('certified_copy_date','',array('class' => 'gui-input fromdate_certified','placeholder' => '','id'=>'certified_copy_date' )) !!}
                                                @else
                                                  {!! Form::text('certified_copy_date',date('d-m-Y',strtotime($get_record[0]->certified_copy_date)),array('class' => 'gui-input fromdate_certified','placeholder' => '','id'=>'certified_copy_date' )) !!}
                                                @endif

                                                
                                                <label for="Account Mobile" class="field-icon">
                                                  <i class="fa fa-calendar"></i>
                                                </label>
                                              </label>
                                            </div>
                                          </div>
                                          <div class="col-md-4">
                                            <div class="section">
                                              <label for="level_name" class="field-label" style="font-weight:600;"> Status : </label>
                                                <!-- <label for="artist_state" class="field select"> -->
                                              <label for="level" class="field prepend-icon">
                                                <div class="section">
                                                  <label class="field select">
                                                    <select class="form-control select-text" name="certified_copy_status" id="certified_copy_status" onchange="chnage_certified_date(this.value)">
                                                      <option value=''>Select Status</option>
                                                      
                                                      <option value='1' <?php if($get_record[0]->certified_copy_status == "1"){ echo "selected";}?>>Pending</option>
                                                      <option value='2' <?php if($get_record[0]->certified_copy_status == "2"){ echo "selected";}?>>Complete</option>
                                                                                      
                                                    </select>
                                                    <i class="arrow double"></i>
                                                  </label>
                                                </div>
                                              </label>
                                            </div>
                                          </div>

                                          <div class="col-md-4" style="display: none;" id="show_certified_date">
                                            <div class="section section_from">
                                              <label for="level_name" class="field-label" style="font-weight:600;"> Complete Date :  </label>  
                                              <label for="level_name" class="field prepend-icon" id="discount_date_from_lb">
                                                
                                                @if($get_record[0]->certified_copy_date_status == "" || $get_record[0]->certified_copy_date_status == "1970-01-01" )
                                                  {!! Form::text('certified_copy_date_status','',array('class' => 'gui-input todate_certified','placeholder' => '','id'=>'certified_copy_date_status' )) !!}
                                                @else
                                                  {!! Form::text('certified_copy_date_status',date('d-m-Y',strtotime($get_record[0]->certified_copy_date_status)),array('class' => 'gui-input todate_certified','placeholder' => '','id'=>'certified_copy_date_status' )) !!}
                                                @endif
                                                
                                                <label for="Account Mobile" class="field-icon">
                                                  <i class="fa fa-calendar"></i>
                                                </label>
                                              </label>
                                            </div>
                                          </div>

                                          <script type="text/javascript">
                                            function chnage_certified_date(status_value){
                                              if(status_value == 2){
                                                document.getElementById("show_certified_date").style.display = "block";
                                              } else {
                                                document.getElementById("show_certified_date").style.display = "none";
                                              }
                                            }
                                          </script>

                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </section>
                        </div>


                        <!-- Other Compliance -->

                        <div @if($get_record[0]->compliance_type_other == 1)  @else style="display: none;" @endif id="show_compliance_type_other">
                          <section id="content" class="table-layout animated fadeIn" style="margin-bottom: -75px;">
                            <div class="">
                              <div class="center-block">
                                <div class="panel panel-primary panel-border top " style="border: 1px solid lightgray;">
                                  <div class="panel-heading" style="padding: 5px 15px;">
                                    <span class="panel-title" style="font-size: 18px;"> Other Compliance </span>
                                  </div>
                                  <div class="panel-body bg-light dark">
                                    <div class="tab-content pn br-n admin-form">
                                      <div id="tab1_1" class="tab-pane active">
                                        <div class="row"> 
                                          <div class="col-md-4">
                                            <div class="section">
                                              <label for="level_name" class="field-label" style="font-weight:600;"> Other Compliance :  </label>  
                                              <label for="level_name" class="field prepend-icon">
                                                {!! Form::text('compliance_other',$get_record[0]->compliance_other,array('class' => 'gui-input','placeholder' => '','id'=>'compliance_other' )) !!}
                                                <label for="Account Mobile" class="field-icon">
                                                  <i class="fa fa-pencil"></i>
                                                </label>
                                              </label>
                                            </div>
                                          </div>
                                          <div class="col-md-4">
                                            <div class="section">
                                              <label for="level_name" class="field-label" style="font-weight:600;"> Status : </label>
                                                <!-- <label for="artist_state" class="field select"> -->
                                              <label for="level" class="field prepend-icon">
                                                <div class="section">
                                                  <label class="field select">
                                                    <select class="form-control select-text" name="compliance_other_status" id="compliance_other_status" onchange="chnage_other_date(this.value)">
                                                      <option value=''>Select Status</option>
                                                      
                                                      <option value='1' <?php if($get_record[0]->compliance_other_status == "1"){ echo "selected";}?>>Pending</option>
                                                      <option value='2' <?php if($get_record[0]->compliance_other_status == "2"){ echo "selected";}?>>Complete</option>
                                                                                      
                                                    </select>
                                                    <i class="arrow double"></i>
                                                  </label>
                                                </div>
                                              </label>
                                            </div>
                                          </div>

                                          <div class="col-md-4" style="display: none;" id="show_other_date">
                                            <div class="section section_from">
                                              <label for="level_name" class="field-label" style="font-weight:600;"> Complete Date :  </label>  
                                              <label for="level_name" class="field prepend-icon" id="discount_date_from_lb">
                                                
                                                @if($get_record[0]->compliance_other_date_status == "" || $get_record[0]->compliance_other_date_status == "1970-01-01" )
                                                  {!! Form::text('compliance_other_date_status','',array('class' => 'gui-input disposal_from','placeholder' => '','id'=>'' )) !!}
                                                @else
                                                  {!! Form::text('compliance_other_date_status',date('d-m-Y',strtotime($get_record[0]->compliance_other_date_status)),array('class' => 'gui-input disposal_from','placeholder' => '','id'=>'' )) !!}
                                                @endif
                                                
                                                <label for="Account Mobile" class="field-icon">
                                                  <i class="fa fa-calendar"></i>
                                                </label>
                                              </label>
                                            </div>
                                          </div>

                                          <script type="text/javascript">
                                            function chnage_other_date(status_value){
                                              if(status_value == 2){
                                                document.getElementById("show_other_date").style.display = "block";
                                              } else {
                                                document.getElementById("show_other_date").style.display = "none";
                                              }
                                            }
                                          </script>

                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </section>
                        </div>

                        <div class="clearfix"></div>

                        

                      
                     </div>

                  <div class="panel-footer text-right" style="margin-top: 60px;">
                      {!! Form::submit('Save', array('class' => 'button btn-primary mysave', 'id' => 'maskedKey')) !!}
                      <!-- {!! Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!} -->
                  </div>   
                    {!! Form::close() !!}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>


<style type="text/css">
  .admin-form .select > select[disabled]{
    color: black !important;
  }

  .admin-form select[disabled], .admin-form .select > select[disabled] {
   color: black !important; 
  }
</style>

<script type="text/javascript">


   // choose_case_no

  function choose_case_no(case_no) { 

   // if(case_no != ""){

    BASE_URL = '{{ url('/')}}';

      $.ajax({
        url:BASE_URL+"/advocate-panel/ajax-case-no/"+case_no,
        success: function(result){
            $("#order_judgment_file_no").html(result);
        }
      });
   // }
  }


  
  //compliance_type_notice  compliance_type_date  compliance_type_defect_case compliance_type_reply compliance_type_certified compliance_type_other 


  function show_notice(){
    var status = $("input:checkbox[id=compliance_type_notice]").is(":checked");

    if(status == true){
      $('#compliance_error').html('');
      document.getElementById("show_compliance_type_notice").style.display = "block";
    } else {
      $('#notice_issue_date').val('');
      $('#notice_status').val('');
      document.getElementById("show_compliance_type_notice").style.display = "none";
    }
  }

  function show_date(){
    var status = $("input:checkbox[id=compliance_type_date]").is(":checked");

    if(status == true){
      $('#compliance_error').html('');
      document.getElementById("show_compliance_type_date").style.display = "block";
    } else {
      $('#compliance_date').val('');
      $('#compliance_status').val('');
      document.getElementById("show_compliance_type_date").style.display = "none";
    }
  }

  function show_defect(){
    var status = $("input:checkbox[id=compliance_type_defect_case]").is(":checked");

    if(status == true){
      $('#compliance_error').html('');
      document.getElementById("show_compliance_type_defect_case").style.display = "block";
    } else {
      $('#compliance_act_reason').val('');
      $('#compliance_defect_status').val('');
      document.getElementById("show_compliance_type_defect_case").style.display = "none";
    }
  }

  function show_reply(){
    var status = $("input:checkbox[id=compliance_type_reply]").is(":checked");

    if(status == true){
      $('#compliance_error').html('');
      document.getElementById("show_compliance_type_reply").style.display = "block";
    } else {
      $('#compliance_reply').val('');
      $('#compliance_reply_status').val('');
      document.getElementById("show_compliance_type_reply").style.display = "none";
    }
  }

  function show_certified(){
    var status = $("input:checkbox[id=compliance_type_certified]").is(":checked");

    if(status == true){
      $('#compliance_error').html('');
      document.getElementById("show_compliance_type_certified").style.display = "block";
    } else {
      $('#certified_copy_date').val('');
      $('#certified_copy_status').val('');
      document.getElementById("show_compliance_type_certified").style.display = "none";
    }
  }
  

  function show_other(){
    var status = $("input:checkbox[id=compliance_type_other]").is(":checked");

    if(status == true){
      $('#compliance_error').html('');
      document.getElementById("show_compliance_type_other").style.display = "block";
    } else {
      $('#compliance_other').val('');
      $('#compliance_other_status').val('');
      document.getElementById("show_compliance_type_other").style.display = "none";
    }
  }


  function change_case(compliance_category){

    BASE_URL = '{{ url('/')}}';
      $.ajax({
        url:BASE_URL+"/advocate-panel/get-type-case/"+compliance_category,
        success: function(result){
            $("#choose_case_type").html(result);
        }
      });
  }

  // function get_type_of_case(compliance_category){

  //   BASE_URL = '{{ url('/')}}';
  //     $.ajax({
  //       url:BASE_URL+"/advocate-panel/get-case-registeration-category/"+compliance_category,
  //       success: function(result){
  //           $("#case_no").html(result);
  //       }
  //     });
  // }


   // choose_case

  function get_type_of_case(case_type) { 

    var court_type = $("#court_type").val();
    BASE_URL = '{{ url('/')}}';
      $.ajax({
        url:BASE_URL+"/advocate-panel/ajax-case-type/"+case_type+"/"+court_type,
        success: function(result){
            $("#case_no").html(result);
        }
      });
  }


  function choose_court_name(court_name) { 

    if (document.getElementById('check1').checked) {
      compliance_category = document.getElementById('check1').value;
    }

    if (document.getElementById('check2').checked) {
      compliance_category = document.getElementById('check2').value;
    }


    var court_type = $("#court_type").val();

    BASE_URL = '{{ url('/')}}';
      $.ajax({
        url:BASE_URL+"/advocate-panel/compliance-case-type/"+compliance_category+"/"+court_name+"/"+court_type,
        success: function(result){
            $("#choose_case_type").html(result);
        }
      });
  }

  
  // choose court  case_type

  // function choose_court(court_type) { 
  //   var case_type = $("#choose_case_type").val();
  //   BASE_URL = '{{ url('/')}}';
  //     $.ajax({
  //       url:BASE_URL+"/advocate-panel/ajax-case-type/"+case_type+"/"+court_type,
  //       success: function(result){
  //           $("#case_no").html(result);
  //       }
  //     });
  // }

  function choose_court(court_type) { 
    BASE_URL = '{{ url('/')}}';
      $.ajax({
        url:BASE_URL+"/advocate-panel/compliance-court-type/"+court_type,
        success: function(result){
            $("#court_name").html(result);
        }
      });
  }

  // get title and type by case

  function get_detail(reg_id){

    if(reg_id != 0){

      $('#descrption_error').html('');
      $('#descrption_error_file').html('');
      $('#descrption_error_ncv').html('');
      $(".select2-selection").css({"border": "1px solid #A5D491"});
      $(".select2-selection").css({"background": "#F0FEE9"});

      BASE_URL = '{{ url('/')}}';
      $.ajax({
        url:BASE_URL+"/advocate-panel/get-case-registeration/"+reg_id,
        success: function(result){
            $("#case_type").val(result.case_name);
            $("#case_title").val(result.reg_petitioner+' v/s '+result.reg_respondent);
            $("#previous_date").val(result.previous_date);
            $("#reg_stage").val(result.pessi_statge_id);
            $("#case_no").val(result.reg_case_no);
            $("#file_no").val(result.reg_file_no);
            $("#ncv_no").val(result.reg_vcn_number);
            $("#court_type").val(result.reg_court);
            $("#court_name").val(result.reg_court_id);
            $("#choose_case_type").val(result.reg_case_type_id);
        }
      });
    } 
  }

</script>

<script type="text/javascript">

  jQuery(document).ready(function() {

    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "This field contains alphabets only");

    jQuery.validator.addMethod("checkemail", function(value, element) 
    {
    return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
    }, "Enter a VALID email address"); 

    $.validator.addMethod('minStrict', function (value, el, param) {
     return value > param; 
    });
    $.validator.addMethod('maxStrict', function (value, el, param) {
     return value < param; 
    });

    jQuery.validator.addMethod("checkdate", function(value, element) 
    {
    return this.optional(element) || /^([0]?[1-9]|[1|2][0-9]|[3][0|1])[-]([0]?[1-9]|[1][0-2])[-]([0-9]{4}|[0-9]{4})$/.test(value);
    }, "Enter date in dd-mm-yyyy format");




    jQuery.validator.addMethod("greaterThan_notice", function(value, element, params) {
      if (!/Invalid|NaN/.test(value)) {

      //var myDate_notice = value;
      value=value.split("-");
      var newDate_notice =value[1]+"/"+value[0]+"/"+value[2];
      var notice_status_date = new Date(newDate_notice).getTime();

      
      var notice_issue_date = $('#notice_issue_date').val();
      notice_issue_date=notice_issue_date.split("-");
      var newDate_notice_issue =notice_issue_date[1]+"/"+notice_issue_date[0]+"/"+notice_issue_date[2];
      var notice_issue_date = new Date(newDate_notice_issue).getTime();


      return notice_status_date >= notice_issue_date;
     
      }
      return isNaN(notice_status_date) && isNaN($(params).val()) 
      || (Number(notice_status_date) > Number($(params).val())); 
    },'Complete date must be greater than issue date.');



    jQuery.validator.addMethod("greaterThan_date", function(value, element, params) {
      if (!/Invalid|NaN/.test(value)) {

      //var myDate_notice = value;
      value=value.split("-");
      var newDate_date =value[1]+"/"+value[0]+"/"+value[2];
      var compliance_date_status = new Date(newDate_date).getTime();
      

      var compliance_date = $('#compliance_date').val();
      compliance_date=compliance_date.split("-");
      var newDate_com_issue =compliance_date[1]+"/"+compliance_date[0]+"/"+compliance_date[2];
      var compliance_date = new Date(newDate_com_issue).getTime();


      return compliance_date_status >= compliance_date;
     
      }
      return isNaN(compliance_date_status) && isNaN($(params).val()) 
      || (Number(compliance_date_status) > Number($(params).val())); 
    },'Complete date must be greater than issue date.');



    jQuery.validator.addMethod("greaterThan_certified", function(value, element, params) {
      if (!/Invalid|NaN/.test(value)) {

      //var myDate_notice = value;
      value=value.split("-");
      var newDate_certified =value[1]+"/"+value[0]+"/"+value[2];
      var certified_copy_date_status = new Date(newDate_certified).getTime();

      
      var certified_copy_date = $('#certified_copy_date').val();
      certified_copy_date=certified_copy_date.split("-");
      var newDate_certified_issue =certified_copy_date[1]+"/"+certified_copy_date[0]+"/"+certified_copy_date[2];
      var certified_copy_date = new Date(newDate_certified_issue).getTime();


      return certified_copy_date_status >= certified_copy_date;
     
      }
      return isNaN(certified_copy_date_status) && isNaN($(params).val()) 
      || (Number(certified_copy_date_status) > Number($(params).val())); 
    },'Complete date must be greater than apply date.');




    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#form_add_question").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        notice_issue_date: {
          required: true,
          checkdate: true,
        },
        notice_status: {
          required: true
        },
        notice_status_date: {
          checkdate: true,
          greaterThan_notice: true,
        },
        compliance_date: {
          required: true,
          checkdate: true,
        },
        compliance_status: {
          required: true
        },
        compliance_date_status: {
          checkdate: true,
          greaterThan_date: true,
        },
        // compliance_act_reason: {
        //   required: true
        // },
        compliance_defect_status: {
          required: true
        },
        compliance_defect_date_status: {
          checkdate: true,
        },
        compliance_reply: {
          required: true
        },
        compliance_reply_status: {
          required: true
        },
        compliance_reply_date_status: {
          checkdate: true,
        },
        certified_copy_date: {
          required: true,
          checkdate: true
        },
        certified_copy_status: {
          required: true
        },
        certified_copy_date_status: {
          checkdate: true,
          greaterThan_certified: true
        },
        compliance_other: {
          required: true
        },
        compliance_other_status: {
          required: true
        },
        compliance_other_date_status: {
          checkdate: true,
        },
        // choose_case_type: {
        //   required:true
        // }
      },
      /* @validation error messages 
      ---------------------------------------------- */
      messages: {
        choose_case_type: {
          required: 'Please select type of case'
        },
        notice_issue_date: {
          required: 'Please select date'
        },
        notice_status: {
          required: 'Please select status '
        },
        compliance_date: {
          required: 'Please select date'
        },
        compliance_status: {
          required: 'Please select status '
        },
        compliance_act_reason: {
          required: 'Please enter act reason'
        },
        compliance_defect_status: {
          required: 'Please select status '
        },
        compliance_reply: {
          required: 'Please enter reply'
        },
        compliance_reply_status: {
          required: 'Please select status '
        },
        certified_copy_date: {
          required: 'Please select date'
        },
        certified_copy_status: {
          required: 'Please select status '
        },
        compliance_other: {
          required: 'Please enter other compliance'
        },
        compliance_other_status: {
          required: 'Please select status'
        }
      },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });


  $(document).on('click','.mysave',function(){

    var case_no = $("#case_no").val();

    var choose_case_type = $("#choose_case_type").val();

    var compliance_type_notice      = $("input:checkbox[id=compliance_type_notice]").is(":checked");
    var compliance_type_date        = $("input:checkbox[id=compliance_type_date]").is(":checked");
    var compliance_type_defect_case = $("input:checkbox[id=compliance_type_defect_case]").is(":checked");
    var compliance_type_reply       = $("input:checkbox[id=compliance_type_reply]").is(":checked");
    var compliance_type_certified   = $("input:checkbox[id=compliance_type_certified]").is(":checked");
    var compliance_type_other       = $("input:checkbox[id=compliance_type_other]").is(":checked");

//     alert(case_no);
    if(choose_case_type != ""){

      if(case_no == 0){
        $('#descrption_error').html('<div class="state-error"></div><em for="services_excerpt" class="state-error">Please select case no.</em>');
        $('#descrption_error_file').html('<div class="state-error"></div><em for="services_excerpt" class="state-error">Please select file no.</em>');
        $('#descrption_error_ncv').html('<div class="state-error"></div><em for="services_excerpt" class="state-error">Please select NCV</em>');
        $(".select2-selection").css({"border": "1px solid #de888a"});
        $(".select2-selection").css({"background": "#fee9ea"});
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
      } 

      // if(compliance_type_notice == false && compliance_type_date == false && compliance_type_defect_case == false && compliance_type_reply == false && compliance_type_certified == false && compliance_type_other == false) {

      //   $('#compliance_error').html('<div class="state-error"></div><em for="services_excerpt" class="state-error">Please select atleast one checkbox. </em>');
        
      //   return false;
      // }
    }

  })



  // setInterval(function(){ 

  //   var checkdate = /^([0]?[1-9]|[1|2][0-9]|[3][0|1])[-]([0]?[1-9]|[1][0-2])[-]([0-9]{4}|[0-9]{4})$/;

  //   var dt_val = $("#notice_issue_date").val();
    
  //   if(!checkdate.test(dt_val)) {

  //   } else {
  //     $("#discount_date_from_lb").addClass("state-success");
  //     $("#discount_date_from_lb").removeClass("state-error");
  //     $(".section_from .state-error").html("");
  //   }


  //   var dt_val_2 = $("#compliance_date").val();
  //   if(!checkdate.test(dt_val_2)) {

  //   } else {
  //     $("#discount_date_from_lb_2").addClass("state-success");
  //     $("#discount_date_from_lb_2").removeClass("state-error");
  //     $(".section_from_2 .state-error").html("");
  //   }

  //   var dt_val_3 = $("#certified_copy_date").val();
  //   if(!checkdate.test(dt_val_3)) {

  //   } else {
  //     $("#discount_date_from_lb_3").addClass("state-success");
  //     $("#discount_date_from_lb_3").removeClass("state-error");
  //     $(".section_from_3 .state-error").html("");
  //   }


  // }, 500);


  </script>



@endsection
