@extends('trivia_admin/layout')

@section('content')

<style type="text/css">
.mytexthieght{
  height: 400px !important;
}
</style>

  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="{{ url('/trivialeague-panel/view-level') }}">Write Message</a>
            </li>
            <li class="crumb-icon">
              <a href="{{ url('trivialeague-panel/dashboard') }}">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="{{ url('trivialeague-panel/dashboard') }}">Home</a>
            </li>
            <li class="crumb-trail">Write Message</li>
          </ol>
        </div>
      </header>

      <!-- Begin: Content -->

      <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center">
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-heading">
                <span class="panel-title"> Write Message </span>
              </div>
              <div class="panel-body bg-light dark">
              <div class="col-md-12">
              @if ($errors->any())
                <div id="log_error" class="alert alert-danger" style='font-family: josefin_sansregular !important;'>
               {{$errors->first()}}  </i></div> @endif </div>
                <div class="tab-content pn br-n admin-form">
                  <div id="tab1_1" class="tab-pane active">
                    {!! Form::open(['name'=>'mailmatterform','id'=>'mailmatterform','url'=>'/trivialeague-panel/send-email' ,'autocomplete'=>'off']) !!}
                    <div class="row">
                      
                        <div class="section row mb10">
                          <label for="acc_email" class="field-label col-md-2 text-center">Subject:</label>
                          <div class="col-md-10 ">  
                            <label for="account_email" class="field prepend-icon">
                              {!! Form::text('mail_subject','', array('class' => 'gui-input','placeholder' => 'Subject' )) !!}
                              {!! Form::hidden('register_ids',$register_ids, array('class' => 'gui-input')) !!}
                              <label for="Account Email" class="field-icon">
                                <i class="fa fa-pencil-square" aria-hidden="true"></i>
                              </label>
                            </label>
                          </div>
                        </div>
                        <div class="section row mb10">
                            <label for="acc_email" class="field-label col-md-2 text-center">Message:</label>
                            <div class="col-md-10">  
                              <label class="field prepend-icon" for="account_address">
                                {!! Form::textarea('mail_message','', array('class' => 'gui-textarea accc1 mytexthieght','placeholder' => 'Message' )) !!}
                                <label for="account_address" class="field-icon">
                                  <i class="fa fa-envelope-o"></i>
                                </label>
                              </label>
                            </div>
                          </div>
                        </div>
                    </div>

                    <div class="panel-footer text-right">
                      {!! Form::submit('Send', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}
                      @if($get_record == '')
                      {!! Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}
                      @endif
                    </div>
                    
                    {!! Form::close() !!}

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

<script type="text/javascript">

  jQuery(document).ready(function() {
  
    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "This field contains alphabets only");

    jQuery.validator.addMethod("checkemail", function(value, element) 
    {
    return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
    }, "Enter a VALID email address"); 


    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#mailmatterform").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        mail_subject: {
          required: true
        },
        mail_message: {
          required: true
        },
      },

      /* @validation error messages 
      ---------------------------------------------- */

      messages: {
        mail_subject: {
          required: 'Please enter subject.'
        },
        mail_message: {
          required: 'Please enter message.'
        },        
      },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });

  </script>



@endsection
