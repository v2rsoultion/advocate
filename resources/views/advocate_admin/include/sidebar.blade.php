<!-- menu selected -->
@php

error_reporting(0);
$path = Request::path();

if($path == "advocate-panel/dashboard"){ $dashboard= 'active';} else { $dashboard = '';};

if($path == "advocate-panel/configuration"){ $configuration= 'active';} else { $configuration = '';};

if($path == "advocate-panel/account-settings"){ $accountsettings= 'active';} else { $accountsettings = '';};

if($path == "advocate-panel/sms-text"){ $sms_text= 'active';} else { $sms_text = '';};

if($path == "advocate-panel/change-password"){ $changepassword= 'active';} else { $changepassword = '';};

if($path == "advocate-panel/leader-board"){ $user= 'active'; $user_menu = 'menu-open'; } else { $user = ''; $user_menu = ''; };

if($path == "advocate-panel/add-client" || $path == "advocate-panel/view-client"){ $client= 'active'; $menu_open = 'menu-open'; } else { $client = ''; $client_menu = ''; };

if($path == "advocate-panel/add-court" || $path == "advocate-panel/view-court"){ $court= 'active'; $menu_open = 'menu-open'; } else { $court = ''; $court_menu = ''; };

if($path == "advocate-panel/add-case-type" || $path == "advocate-panel/view-case-type"){ $case= 'active'; $menu_open = 'menu-open'; } else { $case = ''; $case_menu = ''; };

if($path == "advocate-panel/add-class-code" || $path == "advocate-panel/view-class-code"){ $case_sub= 'active'; $menu_open = 'menu-open'; } else { $case_sub = ''; $case_sub_type = ''; };

if($path == "advocate-panel/add-stage" || $path == "advocate-panel/view-stage"){ $stage= 'active'; $menu_open = 'menu-open'; } else { $stage = ''; $stage_menu = ''; };

if($path == "advocate-panel/add-act" || $path == "advocate-panel/view-act"){ $act= 'active'; $menu_open = 'menu-open'; } else { $act = ''; $act_menu = ''; };

if($path == "advocate-panel/add-section" || $path == "advocate-panel/view-section"){ $section= 'active'; $menu_open = 'menu-open'; } else { $section = ''; $section_menu = ''; };

if($path == "advocate-panel/add-judge" || $path == "advocate-panel/view-judge"){ $judge= 'active'; $menu_open = 'menu-open'; } else { $judge = ''; $judge_menu = ''; };

if($path == "advocate-panel/add-referred" || $path == "advocate-panel/view-referred"){ $reffered= 'active'; $menu_open = 'menu-open'; } else { $reffered = ''; $reffered_menu = ''; };

if($path == "advocate-panel/add-assign" || $path == "advocate-panel/view-assign"){ $assign= 'active'; $menu_open = 'menu-open'; } else { $assign = ''; $assign_menu = ''; };

if($path == "advocate-panel/add-fir" || $path == "advocate-panel/view-fir"){ $fir= 'active'; $fir_menu = 'menu-open'; } else { $fir = ''; $fir_menu = ''; };

if($path == "advocate-panel/add-notice-issue" || $path == "advocate-panel/view-notice-issue"){ $notice= 'active'; $notice_issue_menu = 'menu-open'; } else { $notice = ''; $notice_issue_menu = ''; };

if($path == "advocate-panel/add-date" || $path == "advocate-panel/view-date"){ $date= 'active'; $date_menu = 'menu-open'; } else { $date = ''; $date_menu = ''; };

if($path == "advocate-panel/add-defect-case" || $path == "advocate-panel/view-defect-case"){ $defect= 'active'; $defect_case_menu = 'menu-open'; } else { $defect = ''; $defect_case_menu = ''; };

if($path == "advocate-panel/add-reply" || $path == "advocate-panel/view-reply"){ $reply= 'active'; $reply_menu = 'menu-open'; } else { $reply = ''; $reply_menu = ''; };

if($path == "advocate-panel/add-certified-copy-date" || $path == "advocate-panel/view-certified-copy-date"){ $certified= 'active'; $certified_copy_date_menu = 'menu-open'; } else { $certified = ''; $certified_copy_date_menu = ''; };

if($path == "advocate-panel/add-other" || $path == "advocate-panel/view-other"){ $other= 'active'; $other_menu = 'menu-open'; } else { $other = ''; $other_menu = ''; };

if($path == "advocate-panel/add-case-form" || $path == "advocate-panel/view-case-form"){ $case_registration= 'active'; $case_registration_form_menu = 'menu-open'; } else { $case_registration = ''; $case_registration_form_menu = ''; };

if($path == "advocate-panel/add-advocate" || $path == "advocate-panel/view-advocate"){ $advocate = 'active'; $advocate_menu = 'menu-open'; } else { $advocate = ''; $advocate_menu = ''; };

if($path == "advocate-panel/add-holiday" || $path == "advocate-panel/view-holiday"){ $holiday = 'active'; $holiday_menu = 'menu-open'; } else { $holiday = ''; $holiday_menu = ''; };

if($path == "advocate-panel/add-peshi" || $path == "advocate-panel/view-peshi"){ $pessi_entry = 'active'; $pessi_entry_menu = 'menu-open'; } else { $pessi_entry = ''; $pessi_entry_menu = ''; };

if($path == "advocate-panel/add-course-list" || $path == "advocate-panel/view-course-list"){ $course_list = 'active'; $course_list_menu = 'menu-open'; } else { $course_list = ''; $course_list_menu = ''; };

if($path == "advocate-panel/add-compliance" || $path == "advocate-panel/view-compliance"){ $compliance = 'active'; $compliance_menu = 'menu-open'; } else { $compliance = ''; $compliance_menu = ''; };

if($path == "advocate-panel/add-order-judgment" || $path == "advocate-panel/view-order-judgment"){ $order_judgment = 'active'; $order_judgment_menu = 'menu-open'; } else { $order_judgment = ''; $order_judgment_menu = ''; };

if($path == "advocate-panel/view-calendar"){ $view_calendar = 'active'; } else { $view_calendar = ''; };

if($path == "advocate-panel/filter-reporting"){ $filter_reporting = 'active'; } else { $filter_reporting = ''; };

@endphp
<!-- menu selected -->
<!-- sub menu selected -->
@php


if($path == "advocate-panel/sms-client"){ $sms_client = 'active'; } else { $sms_client = ''; };


if($path == "advocate-panel/add-order-judgment"){ $add_order_judgment = 'active'; } else { $add_order_judgment = ''; };
if($path == "advocate-panel/view-order-judgment"){ $view_order_judgment = 'active'; } else { $view_order_judgment = ''; };

if($path == "advocate-panel/add-compliance"){ $add_compliance = 'active'; } else { $add_compliance = ''; };
if($path == "advocate-panel/view-compliance"){ $view_compliance = 'active'; } else { $view_compliance = ''; };

if($path == "advocate-panel/due-course"){ $due_course = 'active'; } else { $due_course = ''; };

if($path == "advocate-panel/undated-case"){ $udated_case = 'active'; } else { $udated_case = ''; };

if($path == "advocate-panel/add-advocate"){ $add_advocate = 'active'; } else { $add_advocate = ''; };
if($path == "advocate-panel/view-advocate"){ $view_advocate = 'active'; } else { $view_advocate = ''; };

if($path == "advocate-panel/add-holiday"){ $add_holiday = 'active'; } else { $add_holiday = ''; };
if($path == "advocate-panel/view-holiday"){ $view_holiday = 'active'; } else { $view_holiday = ''; };

if($path == "advocate-panel/add-peshi"){ $add_pessi_entry = 'active'; } else { $add_pessi_entry = ''; };
if($path == "advocate-panel/view-peshi"){ $view_pessi_entry = 'active'; } else { $view_pessi_entry = ''; };


if($path == "advocate-panel/add-course-list"){ $add_course_list = 'active'; } else { $add_course_list = ''; };
if($path == "advocate-panel/view-course-list"){ $view_course_list = 'active'; } else { $view_course_list = ''; };


if($path == "advocate-panel/add-client"){ $addclient = 'active'; } else { $addclient = ''; };
if($path == "advocate-panel/view-client"){ $viewclient = 'active'; } else { $viewclient = ''; };

if($path == "advocate-panel/add-court"){ $addcourt = 'active'; } else { $addcourt = ''; };
if($path == "advocate-panel/view-court"){ $viewcourt = 'active'; } else { $viewcourt = ''; };

if($path == "advocate-panel/add-case-type"){ $addcase = 'active'; } else { $addcase = ''; };
if($path == "advocate-panel/view-case-type"){ $viewcase = 'active'; } else { $viewcase = ''; };

if($path == "advocate-panel/add-class-code"){ $addcasesubtype = 'active'; } else { $addcasesubtype = ''; };
if($path == "advocate-panel/view-class-code"){ $viewcasesubtype = 'active'; } else { $viewcasesubtype = ''; };

if($path == "advocate-panel/add-stage"){ $addstage = 'active'; } else { $addstage = ''; };
if($path == "advocate-panel/view-stage"){ $viewstage = 'active'; } else { $viewstage = ''; };

if($path == "advocate-panel/add-act"){ $addact = 'active'; } else { $addact = ''; };
if($path == "advocate-panel/view-act"){ $viewact = 'active'; } else { $viewact = ''; };

if($path == "advocate-panel/add-section"){ $addsection = 'active'; } else { $addsection = ''; };
if($path == "advocate-panel/view-section"){ $viewsection = 'active'; } else { $viewsection = ''; };

if($path == "advocate-panel/add-judge"){ $addjudge = 'active'; } else { $addjudge = ''; };
if($path == "advocate-panel/view-judge"){ $viewjudge = 'active'; } else { $viewjudge = ''; };

if($path == "advocate-panel/add-referred"){ $addreffered = 'active'; } else { $addreffered = ''; };
if($path == "advocate-panel/view-referred"){ $viewreffered = 'active'; } else { $viewreffered = ''; };

if($path == "advocate-panel/add-assign"){ $addassign = 'active'; } else { $addassign = ''; };
if($path == "advocate-panel/view-assign"){ $viewassign = 'active'; } else { $viewassign = ''; };

if($path == "advocate-panel/add-fir"){ $addfir = 'active'; } else { $addfir = ''; };
if($path == "advocate-panel/view-fir"){ $viewfir = 'active'; } else { $viewfir = ''; };

if($path == "advocate-panel/add-notice-issue"){ $addnoticeissue = 'active'; } else { $addnoticeissue = ''; };
if($path == "advocate-panel/view-notice-issue"){ $viewnoticeissue = 'active'; } else { $viewnoticeissue = ''; };

if($path == "advocate-panel/add-date"){ $adddate = 'active'; } else { $adddate = ''; };
if($path == "advocate-panel/view-date"){ $viewdate = 'active'; } else { $viewdate = ''; };

if($path == "advocate-panel/add-defect-case"){ $adddefectcase = 'active'; } else { $adddefectcase = ''; };
if($path == "advocate-panel/view-defect-case"){ $viewdefectcase = 'active'; } else { $viewdefectcase = ''; };

if($path == "advocate-panel/add-reply"){ $addreply = 'active'; } else { $addreply = ''; };
if($path == "advocate-panel/view-reply"){ $viewreply = 'active'; } else { $viewreply = ''; };

if($path == "advocate-panel/add-certified-copy-date"){ $addcertifiedcopydate = 'active'; } else { $addcertifiedcopydate = ''; };
if($path == "advocate-panel/view-certified-copy-date"){ $viewcertifiedcopydate = 'active'; } else { $viewcertifiedcopydate = ''; };

if($path == "advocate-panel/add-other"){ $addother = 'active'; } else { $addother = ''; };
if($path == "advocate-panel/view-other"){ $viewother = 'active'; } else { $viewother = ''; };

if($path == "advocate-panel/add-case-form"){ $addcaseregistrationform = 'active'; } else { $addcaseregistrationform = ''; };
if($path == "advocate-panel/view-case-form"){ $viewcaseregistrationform = 'active'; } else { $viewcaseregistrationform = ''; };

if($path == "advocate-panel/about-software"){ $about_software = 'active'; } else { $about_software = ''; };

if($path == "advocate-panel/guideline-software"){ $guideline_software = 'active'; } else { $guideline_software = ''; };

if($path == "advocate-panel/update-about-software"){ $update_about_software = 'active'; } else { $update_about_software = ''; };

if($path == "advocate-panel/update-guideline-software"){ $update_guideline_software = 'active'; } else { $update_guideline_software = ''; };

if($path == "advocate-panel/daily-diary"){ $daily_cause_list = 'active'; } else { $daily_cause_list = ''; };



@endphp

<!-- sub menu selected -->

@if($admin_details[0]->admin_status == 0)

<script type="text/javascript">
    window.location = "{{ url('advocate-panel/logout') }}";//here double curly bracket
</script>

@endif

<aside id="sidebar_left" class="nano nano-primary affix">
    <div class="sidebar-left-content nano-content">
        <ul class="nav sidebar-menu">
          <li class="sidebar-label pt20"></li>
          
          <li class="{{$dashboard}}" >
            <a href="{{ url('advocate-panel/dashboard') }}">
              <span class="glyphicons glyphicons-dashboard"></span>
              <span class="sidebar-title">Dashboard</span>
            </a>
          </li>
        <!-- Advocate Management -->

        @if($admin_details[0]->admin_type == 1)
          <li class="sidebar-label pt20"></li> 
          <li class="{{ $advocate }}">
            <a class="accordion-toggle {{$advocate_menu}}"  href="#">
              <span class="fa fa-male"></span>
              <span class="sidebar-title">Advocate</span>
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              <li class="{{$add_advocate}}">
                <a href="{{url('/advocate-panel/add-advocate')}}">Add Advocate</a>
              </li>
              <li class="{{$view_advocate}}">
                <a href="{{url('/advocate-panel/view-advocate')}}">View Advocate</a>
              </li>
            </ul>
          </li>

          <li class="{{ $holiday }}">
            <a class="accordion-toggle {{$holiday_menu}}"  href="#">
              <span class="fa fa-calendar"></span>
              <span class="sidebar-title"> Holiday </span>
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              <li class="{{$add_holiday}}">
                <a href="{{url('/advocate-panel/add-holiday')}}">Add Holiday</a>
              </li>
              <li class="{{$view_holiday}}">
                <a href="{{url('/advocate-panel/view-holiday')}}">View Holiday</a>
              </li>
            </ul>
          </li>
        @endif

         <!-- advocate Management -->

         <!-- Master -->
        @if($admin_details[0]->admin_type == 2)
          <!-- Select Case Registration Form -->

          @if(isset(json_decode($admin_details[0]->special_permission)->case_registration))
          <li class="sidebar-label pt10 pb10" style="font-weight: bold;" >Case Information</li>
          <li class="{{ $case_registration }}">
            <a class="accordion-toggle {{$case_registration_form_menu}}"  href="#">
              <span class="fa fa-briefcase" aria-hidden="true"></span>
              <span class="sidebar-title">Case Registration</span>
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              <li class="{{$addcaseregistrationform}}">
                <a href="{{ url('advocate-panel/add-case-form') }}">Add Case </a>
              </li>
              <li class="{{$viewcaseregistrationform}}">
                <a href="{{ url('advocate-panel/view-case-form') }}">View / Edit Case </a>
              </li>
            </ul>
          </li>
          @endif

          <!-- Select Pessi Form -->
<!-- 
          <li class="{{ $pessi_entry }}">
            <a class="accordion-toggle {{$pessi_entry_menu}}"  href="#">
              <span class="fa fa-folder" aria-hidden="true"></span>
              <span class="sidebar-title"> Pessi Entry</span>
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              <li class="{{$add_pessi_entry}}">
                <a href="{{ url('advocate-panel/add-peshi') }}">Add Pessi</a>
              </li>
              <li class="{{$view_pessi_entry}}">
                <a href="{{ url('advocate-panel/view-peshi') }}">View Pessi</a>
              </li>
            </ul>
          </li> -->

          @if(isset(json_decode($admin_details[0]->special_permission)->pessi_cause_list))
          <li class="{{$view_pessi_entry}}">
            <a href="{{ url('advocate-panel/view-peshi') }}">
              <span class="glyphicons glyphicons-notes_2"></span>
              <span class="sidebar-title">Peshi /Cause List Entry </span>
            </a>
          </li>
          @endif

          @if(isset(json_decode($admin_details[0]->special_permission)->daily_cause_list))
          <li class="{{$daily_cause_list}}">
            <a href="{{ url('advocate-panel/daily-diary') }}">
              <span class="fa fa-book"></span>
              <span class="sidebar-title"> Daily Diary </span>
            </a>
          </li>
          @endif

          @if(isset(json_decode($admin_details[0]->special_permission)->sms_to_clients))
          <li class="{{$sms_client}}">
            <a href="{{ url('advocate-panel/sms-client') }}">
              <span class="fa fa-envelope-o"></span>
              <span class="sidebar-title"> SMS to Clients </span>
            </a>
          </li>
          @endif

          @if(isset(json_decode($admin_details[0]->special_permission)->order_judgement_upload))
          <li class="{{ $order_judgment }}">
            <a class="accordion-toggle {{$order_judgment_menu}}"  href="#">
              <span class="fa fa-cloud-upload" aria-hidden="true"></span>
              <span class="sidebar-title">Order/ Judgment upload</span>
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              <li class="{{$add_order_judgment}}">
                <a href="{{ url('advocate-panel/add-order-judgment') }}">Add Order/ Judgment upload</a>
              </li>
              <li class="{{$view_order_judgment}}">
                <a href="{{ url('advocate-panel/view-order-judgment') }}">View Order/ Judgment upload</a>
              </li>
            </ul>
          </li>
          @endif

          @if(isset(json_decode($admin_details[0]->special_permission)->complaince))
          <li class="{{ $compliance }}">
            <a class="accordion-toggle {{$compliance_menu}}"  href="#">
              <span class="fa fa-copy" aria-hidden="true"></span>
              <span class="sidebar-title">Compliance</span>
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              <li class="{{$add_compliance}}">
                <a href="{{ url('advocate-panel/add-compliance') }}">Add Compliance</a>
              </li>
              <li class="{{$view_compliance}}">
                <a href="{{ url('advocate-panel/view-compliance') }}">View Compliance</a>
              </li>
            </ul>
          </li>
          @endif
<!-- 
          <li class="{{ $course_list }}">
            <a class="accordion-toggle {{$course_list_menu}}"  href="#">
              <span class="fa fa-folder" aria-hidden="true"></span>
              <span class="sidebar-title"> Course List</span>
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              <li class="{{$add_course_list}}">
                <a href="{{ url('advocate-panel/add-course-list') }}">Add Course List</a>
              </li>
              <li class="{{$view_course_list}}">
                <a href="{{ url('advocate-panel/view-course-list') }}">View Course List</a>
              </li>
            </ul>
          </li> -->

          @if(isset(json_decode($admin_details[0]->special_permission)->calendar))
          <li class="{{$view_calendar}}">
            <a href="{{ url('advocate-panel/view-calendar') }}">
              <span class="fa fa-calendar"></span>
              <span class="sidebar-title"> Calendar </span>
            </a>
          </li>
          @endif

          @if(isset(json_decode($admin_details[0]->special_permission)->undated_case))
          <li class="{{$udated_case}}">
            <a href="{{ url('advocate-panel/undated-case') }}">
              <span class="fa fa-paperclip"></span>
              <span class="sidebar-title">Undated Case</span>
            </a>
          </li>
          @endif

          @if(isset(json_decode($admin_details[0]->special_permission)->due_course))
          <li class="{{$due_course}}">
            <a href="{{ url('advocate-panel/due-course') }}">
              <span class="fa fa-trash"></span>
              <span class="sidebar-title">Due Course</span>
            </a>
          </li>
          @endif

          <!-- Reporting -->
          @if(isset(json_decode($admin_details[0]->special_permission)->reporting))
          <li class="sidebar-label pt10 pb10" style="font-weight: bold;">Reporting</li>
          <li class="{{$filter_reporting}}">
            <a href="{{ url('advocate-panel/filter-reporting') }}">
              <span class="fa fa-bar-chart-o"></span>
              <span class="sidebar-title"> Reporting </span>
            </a>
          </li>
          @endif

          <!-- master modules -->
          @if(isset(json_decode($admin_details[0]->special_permission)->master_modules))
          <li class="sidebar-label pt10 pb10" style="font-weight: bold;"> Master Module</li>


          <li class="{{ $client }}">
            <a class="accordion-toggle {{$menu_open}}" href="#">
              <span class="fa fa-columns"></span>
              <span class="sidebar-title">Master Module</span>
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              <li class="{{ $client }}">
                <a class="accordion-toggle"  href="#">
                  <span class="fa fa-male"></span>
                  <span class="sidebar-title">Client</span>
                  <span class="caret"></span>
                </a>
                <ul class="nav sub-nav">
                  <li class="{{$addclient}}">
                    <a href="{{url('/advocate-panel/add-client')}}">Add Client</a>
                  </li>
                  <li class="{{$viewclient}}">
                    <a href="{{url('/advocate-panel/view-client')}}">View Client</a>
                  </li>
                </ul>
              </li>

              <li class="{{ $court }}">
                <a class="accordion-toggle"  href="#">
                  <span class="fa fa-edit"></span>
                  <span class="sidebar-title">Court</span>
                  <span class="caret"></span>
                </a>
                <ul class="nav sub-nav">
                  <li class="{{$addcourt}}">
                    <a href="{{url('/advocate-panel/add-court')}}">Add Court</a>
                  </li>
                  <li class="{{$viewcourt}}">
                    <a href="{{url('/advocate-panel/view-court')}}">View Court</a>
                  </li>
                </ul>
              </li>

              <li class="{{ $case }}">
                <a class="accordion-toggle"  href="#">
                  <span class="fa fa-edit"></span>
                  <span class="sidebar-title">Case Type</span>
                  <span class="caret"></span>
                </a>
                <ul class="nav sub-nav">
                  <li class="{{$addcase}}">
                    <a href="{{url('/advocate-panel/add-case-type')}}">Add Case Type</a>
                  </li>
                  <li class="{{$viewcase}}">
                    <a href="{{url('/advocate-panel/view-case-type')}}">View Case Type</a>
                  </li>
                </ul>
              </li>
              
              <li class="{{ $case_sub }}">
                <a class="accordion-toggle"  href="#">
                  <span class="fa fa-edit"></span>
                  <span class="sidebar-title">Class Code</span>
                  <span class="caret"></span>
                </a>
                <ul class="nav sub-nav">
                  <li class="{{$addcasesubtype}}">
                    <a href="{{url('/advocate-panel/add-class-code')}}">Add Class Code</a>
                  </li>
                  <li class="{{$viewcasesubtype}}">
                    <a href="{{url('/advocate-panel/view-class-code')}}">View Class Code</a>
                  </li>
                </ul>
              </li>

              <li class="{{ $stage }}">
                <a class="accordion-toggle"  href="#">
                  <span class="fa fa-align-justify" aria-hidden="true"></span>
                  <span class="sidebar-title">Stage</span>
                  <span class="caret"></span>
                </a>
                <ul class="nav sub-nav">
                  <li class="{{$addstage}}">
                    <a href="{{url('/advocate-panel/add-stage')}}">Add Stage</a>
                  </li>
                  <li class="{{$viewstage}}">
                    <a href="{{url('/advocate-panel/view-stage')}}">View Stage</a>
                  </li>
                </ul>
              </li>
              <li class="{{ $act }}">
                <a class="accordion-toggle"  href="#">
                  <span class="fa fa-child" aria-hidden="true"></span>
                  <span class="sidebar-title">Acts</span>
                  <span class="caret"></span>
                </a>
                <ul class="nav sub-nav">
                  <li class="{{$addact}}">
                    <a href="{{url('/advocate-panel/add-act')}}">Add Acts</a>
                  </li>
                  <li class="{{$viewact}}">
                    <a href="{{url('/advocate-panel/view-act')}}">View Acts</a>
                  </li>
                </ul>
              </li>
              <!-- <li class="{{ $section }}">
                <a class="accordion-toggle"  href="#">
                  <span class="fa fa-puzzle-piece" aria-hidden="true"></span>
                  <span class="sidebar-title">Section</span>
                  <span class="caret"></span>
                </a>
                <ul class="nav sub-nav">
                  <li class="{{$addsection}}">
                    <a href="{{url('/advocate-panel/add-section')}}">Add Section</a>
                  </li>
                  <li class="{{$viewsection}}">
                    <a href="{{url('/advocate-panel/view-section')}}">View Section</a>
                  </li>
                </ul>
              </li> -->

              <li class="{{ $judge }}">
                <a class="accordion-toggle"  href="#">
                  <span class="fa fa-gavel" aria-hidden="true"></span>
                  <span class="sidebar-title">Judge</span>
                  <span class="caret"></span>
                </a>
                <ul class="nav sub-nav">
                  <li class="{{$addjudge}}">
                    <a href="{{url('/advocate-panel/add-judge')}}">Add Judge</a>
                  </li>
                  <li class="{{$viewjudge}}">
                    <a href="{{url('/advocate-panel/view-judge')}}">View Judge</a>
                  </li>
                </ul>
              </li>
              <li class="{{ $reffered }}">
                <a class="accordion-toggle"  href="#">
                  <span class="fa fa-users" aria-hidden="true"></span>
                  <span class="sidebar-title">Referred</span>
                  <span class="caret"></span>
                </a>
                <ul class="nav sub-nav">
                  <li class="{{$addreffered}}">
                    <a href="{{url('/advocate-panel/add-referred')}}">Add Referred</a>
                  </li>
                  <li class="{{$viewreffered}}">
                    <a href="{{url('/advocate-panel/view-referred')}}">View Referred</a>
                  </li>
                </ul>
              </li>
              <li class="{{ $assign }}">
                <a class="accordion-toggle"  href="#">
                  <span class="fa fa-file-code-o" aria-hidden="true"></span>
                  <span class="sidebar-title">Assigned</span>
                  <span class="caret"></span>
                </a>
                <ul class="nav sub-nav">
                  <li class="{{$addassign}}">
                    <a href="{{url('/advocate-panel/add-assign')}}">Add Assigned</a>
                  </li>
                  <li class="{{$viewassign}}">
                    <a href="{{url('/advocate-panel/view-assign')}}">View Assigned</a>
                  </li>
                </ul>
              </li>

              <!-- <li class="{{ $fir }}">
                <a class="accordion-toggle {{$fir_menu}}"  href="#">
                  <span class="fa fa-folder" aria-hidden="true"></span>
                  <span class="sidebar-title">FIR</span>
                  <span class="caret"></span>
                </a>
                <ul class="nav sub-nav">
                  <li class="{{$addfir}}">
                    <a href="{{url('/advocate-panel/add-fir')}}">Add FIR</a>
                  </li>
                  <li class="{{$viewfir}}">
                    <a href="{{url('/advocate-panel/view-fir')}}">View FIR</a>
                  </li>
                </ul>
              </li> -->


            </ul>
          </li>
          @endif

          
          
          
          
          


          <!-- Complaince -->
    
          <!-- <li class="sidebar-label pt20">Complaince</li>
          <li class="{{ $notice }}">
            <a class="accordion-toggle {{$notice_issue_menu}}"  href="#">
              <span class="fa fa-file-word-o" aria-hidden="true"></span>
              <span class="sidebar-title">Notice Issue</span>
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              <li class="{{$addnoticeissue}}">
                <a href="{{url('/advocate-panel/add-notice-issue')}}">Add Notice Issue</a>
              </li>
              <li class="{{$viewnoticeissue}}">
                <a href="{{url('/advocate-panel/view-notice-issue')}}">View Notice Issue</a>
              </li>
            </ul>
          </li>
          <li class="{{ $date }}">
            <a class="accordion-toggle {{$date_menu}}"  href="#">
              <span class="fa fa-calendar" aria-hidden="true"></span>
              <span class="sidebar-title">Date</span>
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              <li class="{{$adddate}}">
                <a href="{{ url('advocate-panel/add-date') }}">Add Date</a>
              </li>
              <li class="{{$viewdate}}">
                <a href="{{ url('advocate-panel/view-date') }}">View Date</a>
              </li>
            </ul>
          </li>
          <li class="{{ $defect }}">
            <a class="accordion-toggle {{$defect_case_menu}}"  href="#">
              <span class="fa fa-bug" aria-hidden="true"></span>
              <span class="sidebar-title">Defect Case</span>
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              <li class="{{$adddefectcase}}">
                <a href="{{ url('advocate-panel/add-defect-case') }}">Add Defect Case</a>
              </li>
              <li class="{{$viewdefectcase}}">
                <a href="{{ url('advocate-panel/view-defect-case') }}">View Defect Case</a>
              </li>
            </ul>
          </li>
          <li class="{{ $reply }}">
            <a class="accordion-toggle {{$reply_menu}}"  href="#">
              <span class="fa fa-reply" aria-hidden="true"></span>
              <span class="sidebar-title">Reply</span>
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              <li class="{{$addreply}}">
                <a href="{{ url('advocate-panel/add-reply') }}">Add Reply</a>
              </li>
              <li class="{{$viewreply}}">
                <a href="{{ url('advocate-panel/view-reply') }}">View Reply</a>
              </li>
            </ul>
          </li>
          <li class="{{ $certified }}">
            <a class="accordion-toggle {{$certified_copy_date_menu}}"  href="#">
              <span class="fa fa-reply" aria-hidden="true"></span>
              <span class="sidebar-title">Apply Date Date</span>
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              <li class="{{$addcertifiedcopydate}}">
                <a href="{{ url('advocate-panel/add-certified-copy-date') }}">Add Apply Date Date</a>
              </li>
              <li class="{{$viewcertifiedcopydate}}">
                <a href="{{ url('advocate-panel/view-certified-copy-date') }}">View Apply Date Date</a>
              </li>
            </ul>
          </li>
          <li class="{{ $other }}">
            <a class="accordion-toggle {{$other_menu}}"  href="#">
              <span class="fa fa-reply" aria-hidden="true"></span>
              <span class="sidebar-title">Other</span>
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              <li class="{{$addother}}">
                <a href="{{ url('advocate-panel/add-other') }}">Add Other</a>
              </li>
              <li class="{{$viewother}}">
                <a href="{{ url('advocate-panel/view-other') }}">View Other</a>
              </li>
            </ul>
          </li> -->
        @endif
            <!--- user admin -->

          <li class="sidebar-label pt10 pb10" style="font-weight: bold;">Systems</li> 
          
          @if($admin_details[0]->admin_type == 1)
          
          <li class="{{$accountsettings}}">
            <a href="{{ url('advocate-panel/account-settings') }}">
              <span class="fa fa-cogs"></span>
              <span class="sidebar-title">Account Settings</span>
            </a>
          </li>

          @endif

          @if($admin_details[0]->admin_type == 2)

          <!-- <li class="{{$about_software}}">
            <a href="{{ url('advocate-panel/about-software') }}">
              <span class="glyphicon glyphicon-info-sign"></span>
              <span class="sidebar-title">About Software</span>
            </a>
          </li> -->

          <li class="{{$sms_text}}">
            <a href="{{ url('advocate-panel/sms-text') }}">
              <span class="fa fa-cogs"></span>
              <span class="sidebar-title">SMS Text</span>
            </a>
          </li>


          <li class="">
            <a href="#">
              <span class="glyphicon glyphicon-info-sign"></span>
              <span class="sidebar-title">Notice (Coming Soon)</span>
            </a>
          </li>

          <li class="">
            <a href="#">
              <span class="glyphicon glyphicon-info-sign"></span>
              <span class="sidebar-title">Library (Coming Soon)</span>
            </a>
          </li>

          <li class="">
            <a href="#">
              <span class="glyphicon glyphicon-info-sign"></span>
              <span class="sidebar-title">Accounts/ billing (Coming Soon)</span>
            </a>
          </li>

          <li class="">
            <a href="#">
              <span class="glyphicon glyphicon-info-sign"></span>
              <span class="sidebar-title">Correspondence (Coming Soon)</span>
            </a>
          </li>

          <li class="{{$guideline_software}}">
            <a href="{{ url('advocate-panel/guideline-software') }}">
              <span class="glyphicon glyphicon-info-sign"></span>
              <span class="sidebar-title">Guidelines to use Software</span>
            </a>
          </li>

          @endif

          @if($admin_details[0]->admin_type == 1)
<!-- 
          <li class="{{$update_about_software}}">
            <a href="{{ url('advocate-panel/update-about-software') }}">
              <span class="glyphicon glyphicon-info-sign"></span>
              <span class="sidebar-title">About Software</span>
            </a>
          </li> -->

          <li class="{{$update_guideline_software}}">
            <a href="{{ url('advocate-panel/update-guideline-software') }}">
              <span class="glyphicon glyphicon-info-sign"></span>
              <span class="sidebar-title">Guideline Software</span>
            </a>
          </li>

          @endif
          
          <li class="{{$changepassword}}">
            <a href="{{ url('advocate-panel/change-password') }}">
              <span class="glyphicons glyphicons-keys"></span>
              <span class="sidebar-title">Change Password</span>
            </a>
          </li>
          
        </ul>

        <!--- user admin -->
        
        <div class="sidebar-toggle-mini">
          <a href="#">
            <span class="fa fa-sign-out"></span>
          </a>
        </div>
        
    </div>
</aside>

<style type="text/css">

.sidebar-menu{
  background-color: #f1f2f5; 
}
.sidebar-menu > li{
  background-color: #f1f2f5;
  color: black;
}
.sidebar-menu > li {
  background-color: #dfe4e8;
  margin: 5px 0px; 
}
.sidebar-menu > li > a {
    color: black;
}
.sidebar-menu > li > ul > li {
  background-color: #dfe4e8;
}
.sidebar-menu > li > ul > li > a {
  background-color: #f1f2f5;
  color: black;
}
.sidebar-menu > li.active > a{
  background-color: #f1f2f5;
  color: black; 
}
.sidebar-menu > li > ul > li.active > a{
  background-color: #dfe4e8;
  color: black;  
}

.sidebar-menu > li > ul > li > ul > li {
  background-color: #dfe4e8;
}
.sidebar-menu > li > ul > li > ul > li > a{
  background-color: #f1f2f5;
  color: black;
}

/*


#dee2e5


*/

</style>
