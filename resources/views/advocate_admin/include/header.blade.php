<!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8">
        <title> {{ $title }} | AdminPanel</title>
        <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Expires" content="0" />
        <meta name="keywords" content="AdminPanel" />
        <meta name="description" content="AdminPanel">
        <meta name="author" content="AdminPanel">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="csrf-token" content="{{ csrf_token() }}">
  
        {!!Html::style('public/admin/css/css.css') !!}
        <link rel="stylesheet" media="print" href="{{ url('/')}}/public/admin/css/print.css">
        {!!Html::style('public/admin/css/bootstrap-formhelpers.min.css') !!}
        {!!Html::style('public/admin/js/plugins/datatables/media/css/dataTables.bootstrap.css') !!}
        {!!Html::style('public/admin/js/plugins/fullcalendar/fullcalendar.min.css') !!}
        {!!Html::style('public/admin/js/plugins/datatables/media/css/dataTables.plugins.css') !!}
        {!!Html::style('public/admin/js/plugins/magnific/magnific-popup.css') !!}
        {!!Html::style('public/admin/css/admin-forms.css') !!}
        {!!Html::style('public/admin/js/plugins/select2/css/core.css') !!}
        <!-- {!!Html::style('public/admin/js/plugins/selecuploads/admin/1513679344.pngt2/css/core.css') !!} -->
        {!!Html::style('public/admin/css/theme.css') !!}
        {!!Html::style('public/admin/css/fonts/glyphicons-pro/glyphicons-pro.css') !!}
        {!!Html::style('public/admin/css/fonts/iconsweets/iconsweets.css') !!}
        {!!Html::style('public/admin/css/jquery-te-1.4.0.css') !!}
        {!!Html::style('public/admin/skin/default_skin/css/theme.css') !!}

          <!-- jQuery Validate Plugin-->
      {!!Html::script('public/admin/js/jquery/jquery-1.11.1.min.js') !!}
      {!!Html::script('public/admin/js/admin-forms/js/jquery.validate.min.js') !!}
      {!!Html::script('public/admin/js/admin-forms/js/additional-methods.min.js') !!}

<style type="text/css">
  
  .bg-light{
    background-color: #4B77BE !important;
    color: black !important;
  }

  .bg-light th label{
    color: black !important;
  }

  .table-striped > tbody > tr:nth-child(odd) > td, .table-striped > tbody > tr:nth-child(odd) > th{
    background-color: antiquewhite;
    color: black !important;
  }

  .table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th{
    color: black !important;
  }
  .table-bordered{
    border: 1px solid black !important;
  }
  .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td{
    border: 1px solid black !important; 
  }

  .bg-light.dark {
    background-color: #F2F2F2 !important;
}
  .admin-form .gui-input, .admin-form{
    padding: 4px;
  }

  #level{
    padding: 0;
  }
  .case_button{
    margin-top: -6px;
    padding: 7px 12px;
  }
  .ideal_logo{
    width: 200px;
  }
</style>
        
        <script>
            var BASE_URL = "{{ url('/') }}";
        </script>
  
      <link rel="shortcut icon" href="{{{ asset('public/admin/img/1.png') }}}">
    </head>

<body class="">
    <div id="main" style="min-height:0">
      <header class="navbar navbar-fixed-top">
        <div class="navbar-branding" style="background-color: black;"> 
          <a class="navbar-brand" href="{{ url('/advocate-panel/dashboard') }}"><img src="{{ asset('public/images/F_logo_01.svg') }}" alt="img logo" title="Ideal Lawyer" class="ideal_logo"></a>
          
        </div>
        
        
        <ul class="nav navbar-nav navbar-left">  
          <li class="hidden-xs">
            <span id="toggle_sidemenu_l" class="ad ad-lines"></span>
          </li>
        </ul>

        <ul class="nav navbar-nav navbar-left">  
          <li class="hidden-xs">
            <a class="request-fullscreen toggle-active" href="#">
              <span class="ad ad-screen-full fs18"></span>
            </a>
          </li>
        </ul>

<!--         <ul class="nav navbar-nav navbar-left">  
          <li class="hidden-xs">
            <a href="#" class="pl5" title="New Customers">
              <i class="fa fa-user fs18"></i>
              <span class="badge badge-hero badge-danger">3</span>
            </a>
          </li>
        </ul>

        <ul class="nav navbar-nav navbar-left">  
          <li class="hidden-xs">
            <a href="#" class="pl5" title="New Messages">
              <i class="fa fa-envelope-o fs18"></i>
              <span class="badge badge-hero badge-danger">4</span>
            </a>
          </li>
        </ul>

        <ul class="nav navbar-nav navbar-left">  
          <li class="hidden-xs">
            <a href="#" class="pl5" title="New Feedbacks">
              <i class="fa fa-comments-o fs18"></i>
              <span class="badge badge-hero badge-danger">5</span>
            </a>
          </li>
        </ul> -->

<!--         <ul class="nav navbar-nav navbar-left">  
          <li class="hidden-xs">
            <a href="#" class="pl5" title="New Services">
              {!! HTML::image('public/images/order.png','avatar', array('class' => 'fa fa-comment-o fs22 text-primary')) !!}
              <span class="badge badge-hero badge-danger">6</span>
            </a>
          </li>
        </ul> -->

        @if($admin_details[0]->admin_type == 2)
        
          <ul class="nav navbar-nav navbar-left">  
            <li class="hidden-xs">
              <a href="{{ url('/advocate-panel/view-case-form/')}}"> 
                <button type="button" name="Case Status" id="" class="btn btn-primary case_button"> <i class="fa fa-search" aria-hidden="true"></i> &nbsp; Case Status </button>
              </a>
            </li>
          </ul>

        @endif

        <ul class="nav navbar-nav navbar-left" style="width: 45%">  
          <li class="hidden-xs" style="width: 100%; text-align: center;">
            <div style="color: #666;font-size: 13px;font-weight: 600;padding-top: 20px;padding-bottom: 20px;height: 59px;max-height: 59px;">
              {{ $admin_details[0]->admin_name }}
            </div>
          </li>
        </ul>

        <ul class="nav navbar-nav navbar-right">
          <li class="dropdown" style="height: 50px;">
            <a href="#" class="dropdown-toggle fw600 p15" data-toggle="dropdown" style="text-transform: capitalize; height: 60px !important;">
              
              @if($admin_details[0]->admin_image == "")
                {!! HTML::image('public/images/dummy.png','avatar', array('class' => 'mw30 br64 mr15')) !!}
              @else
                {!! HTML::image($admin_details[0]->admin_image,'avatar', array('class' => 'mw30 br64 mr15')) !!}
              @endif

              @php $strlen = strlen($admin_details[0]->admin_name) @endphp
             {{ substr($admin_details[0]->admin_name,0,7) }} @if($strlen > 7) ..... @endif  <span class="caret caret-tp hidden-xs"></span>
            </a>
            <ul class="dropdown-menu list-group dropdown-persist w250" role="menu">
              <li class="list-group-item">
                <a href="{{ url('/advocate-panel/account-profile') }}" class="animated animated-short fadeInUp">
                  <span class="fa fa-user"></span> Profile
                </a>
              </li>
              
              @if($admin_details[0]->admin_type == 1)
              <li class="list-group-item">
                <a href="{{ url('advocate-panel/account-settings') }}" class="animated animated-short fadeInUp">
                  <span class="fa fa-gear"></span> Account Settings </a>
              </li>
              @endif
              
              <li class="list-group-item">
                <a href="{{ url('advocate-panel/sms-text') }}" class="animated animated-short fadeInUp">
                  <span class="fa fa-gear"></span> SMS Text </a>
              </li>

              <li class="list-group-item">
                <a href="{{ url('advocate-panel/change-password') }}" class="animated animated-short fadeInUp">
                  <span class="fa fa-key"></span> Change Password
                </a>
              </li>
              
              <li class="list-group-item">
                <a href="{{ url('/advocate-panel/logout') }}" class="animated animated-short fadeInUp">
                  <span class="fa fa-power-off"></span> Logout </a>
              </li>
            </ul>
          </li>
        </ul>

      </header>