@if(count($sub_client) != 0)
<div class="section">
    <label for="level_name" class="field-label" style="font-weight:600;" > Sub Client : </label>
      <label for="artist_state" class="field">
        <select id="framework_2" name="sub_client[]" multiple class="form-control" >
            @foreach($sub_client as $sub_clients)
              <option value="{{ $sub_clients->sub_client_id }}"> {{ $sub_clients->sub_client_name }} @if($sub_clients->sub_guardian_name != "") {{ $sub_clients->sub_name_prefix }} {{ $sub_clients->sub_guardian_name }} @endif  @if($sub_clients->sub_client_email_id != "") ({{ $sub_clients->sub_client_email_id }}) @endif</option>
            @endforeach
        </select>
        <i class="arrow double"></i>
      </label>
  </div>
@endif


<script type="text/javascript">
	

	$('#framework_2').multiselect({
      nonSelectedText: 'Select Sub Client',
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
      buttonWidth:'400px'
     });
	
</script>