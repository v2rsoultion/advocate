@extends('advocate_admin/layout')
@section('content')
<style type="text/css">
  .admin-form .select, .admin-form .gui-input, .admin-form .select > select, .admin-form .select-multiple select{
    height: 28px !important;
  }
  .admin-form a.button, .admin-form span.button, .admin-form label.button
  {
    line-height: 28px !important;
  }
  .admin-form .append-icon .field-icon, .admin-form .prepend-icon .field-icon{
    line-height: 28px !important;
  }
  .admin-form .gui-textarea {
    line-height: 7px !important;
  }
  .select2-container .select2-selection--single {
    height: 28px !important;
  }
  .select2-container--default .select2-selection--single .select2-selection__rendered {
    line-height: 24px !important;
  }
  .select2-container .select2-selection--multiple {
    min-height: 30px !important;
  }
  .select2-container--default .select2-selection--single .select2-selection__arrow {
    top: -4px !important;
  }
  .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control{
    color: black !important;
  }
/*  .gui-textarea{
    height: 100px !important;
  }*/
/* .admin-form .gui-input {
  padding: 5px;
 }*/
 /*.admin-form .prepend-icon .field-icon {
  top: 6px;
 }*/
 .form-control {
    height: 28px !important;
    /*padding: 0px 12px !important;*/
  }
 .admin-form .button{
    height: 28px !important;
    line-height: 1px !important;
  }
 .btn {
    height: 29px !important;
    padding: 4px 12px !important;
    line-height: 1px !important;  }

 .down{
  margin-top: 70px;
 }
.account_setting {
  margin: 0px 20px !important;
  margin-top: 25px !important;
  /*padding: 5px 0px !important;*/
 }
 .select-text{
  padding: 0px 12px;
 }
</style>
  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left down">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="{{ url('/advocate-panel/view-order-judgment') }}">View Order Judgment</a>
            </li>
            <li class="crumb-icon">
              <a href="{{ url('advocate-panel/dashboard') }}">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="{{ url('advocate-panel/dashboard') }}">Home</a>
            </li>
            <li class="crumb-trail">Add Order Judgment</li>
          </ol>
        </div>
      </header>

      <div class="row">
        <div class="col-md-12">
          @if (\Session::has('success'))
          <div class="alert alert-success account_setting" >
            {!! \Session::get('success') !!}
          </div>
          @endif

          @if (\Session::has('error'))
          <div class="alert alert-danger account_setting" >
            {!! \Session::get('error') !!}
          </div>
          @endif
        </div>
      </div>
      <!-- Begin: Content -->

      <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center">
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-heading">
                <span class="panel-title">Add Order Judgment</span>
              </div>
              <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                <!-- IF any error found -->
                <div class="col-md-12">
                @if ($errors->any())
                  <div id="log_error" class="alert alert-danger" style='font-family: josefin_sansregular !important;'>
                  {{$errors->first()}}  </i></div>
                @endif
                </div>
                <!-- IF any error found -->
                  <div id="tab1_1" class="tab-pane active">
                    {!! Form::open(['name'=>'form_add_question','url'=>'advocate-panel/insert-order-judgment/'.$get_record[0]->upload_case_id,'id'=>'form_add_question','files'=>'true' ,'autocomplete'=>'off']) !!}
                    <div class="row">

                      {!! Form::hidden('upload_case_id',$get_record[0]->upload_case_id,array('class' => 'gui-input','placeholder' => '','id'=>'upload_case_id' )) !!}

                      <div id="">    
                        <div class="col-md-2">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Court Type  : </label>
                              <!-- <label for="artist_state" class="field select"> -->
                                <label for="level" class="field prepend-icon">
                                @if($get_record[0]->upload_id != "")
                                  <select class="form-control select-text" name="court_type" onclick="choose_court(this.value)" id="court_type" readonly>
                                @else 
                                  <select class="form-control select-text" name="court_type" onchange="choose_court(this.value)" id="court_type" >
                                @endif

                                  <option value="">Court Type</option>
                                  <option value="1" {{ $get_record[0]->upload_court_type == 1 ? 'selected="selected"' : '' }} >Trial Court</option>
                                  <option value="2" {{ $get_record[0]->upload_court_type == 2 ? 'selected="selected"' : '' }} >High Court</option>
                                </select>
                                <i class="arrow"></i>
                              </label>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;" > Court Name  : </label>
                            <!-- <label for="artist_state" class="field select"> -->
                              <label for="level" class="field prepend-icon">
                              @if($get_record != "")
                                <select class="form-control select-text" name="court_name" onclick="choose_court_name(this.value)" id="court_name" readonly>
                              @else 
                                <select class="form-control select-text" name="court_name" onchange="choose_court_name(this.value)" id="court_name" >
                              @endif
                                <option value="">Select Court Name</option>
                                  
                                  @foreach($court_all as $court_alls) 
                                    <option value="{{ $court_alls->court_id }}" {{ $court_alls->court_id == $get_record[0]->reg_court_id ? 'selected="selected"' : '' }} > {{$court_alls->court_name }} </option>
                                  @endforeach

                              </select>
                              <i class="arrow"></i>
                            </label>
                        </div>
                      </div>

                    
                      <div id="">    
                        <div class="col-md-2">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Type of Case  : </label>
                              <!-- <label for="artist_state" class="field select"> -->
                                <label for="level" class="field prepend-icon">
                                @if($get_record[0]->upload_id != "")
                                  <select class="form-control select-text" name="order_judgment_case_type" onclick="choose_case(this.value)" id="choose_case_type" readonly>
                                @else 
                                  <select class="form-control select-text" name="order_judgment_case_type" onchange="choose_case(this.value)" id="choose_case_type" >
                                @endif

                                  <option value=''>Select Case Type</option> 
                                    
                                    @foreach($case_type_entry as $case_type_entrys) 
                                      <option value="{{ $case_type_entrys->case_id }}" {{ $case_type_entrys->case_id == $get_record[0]->upload_case_type ? 'selected="selected"' : '' }} >{{$case_type_entrys->case_name }} </option>
                                    @endforeach

                                </select>
                                <i class="arrow"></i>
                              </label>
                          </div>
                        </div>
                      </div>


                      <!-- <div class="col-md-3">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;"> Case Year : </label>
                            <label for="artist_state" class="field select">
                              
                              @if($get_record[0]->upload_case_year != "")
                                <select class="form-control" id="order_judgment_case_year" onclick="choose_year(this.value)" name="order_judgment_case_year">
                              @else 
                                <select class="form-control" id="order_judgment_case_year" onchange="choose_year(this.value)" name="order_judgment_case_year">
                              @endif

                                <option value=''>Select Case Year</option>              
                                
                                @php
                                    $currentYear = date('Y');
                                @endphp

                                @foreach(range(1950, $currentYear) as $value)
                                    <option value={{$value}} {{ $value == $get_record[0]->upload_case_year ? 'selected="selected"' : '' }}> {{$value}} </option>
                                @endforeach

                              </select>
                              <i class="arrow"></i>
                            </label>
                        </div>
                      </div> -->

                      <div class="col-md-2">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;"> Case No : </label>
                            <!-- <label for="artist_state" class="field select"> -->
                              <label for="level" class="field prepend-icon">
                              @if($get_record[0]->upload_id != "")
                                <select class="form-control select-text" id="order_judgment_case_no" onclick="choose_case_no(this.value)" name="order_judgment_case_no" readonly>
                              @else 
                                <select class="form-control select-text" id="order_judgment_case_no" onchange="choose_case_no(this.value)" name="order_judgment_case_no">
                              @endif

                                <option value=''>Select Case No</option>
                                @if($get_record != "") 
                                 @foreach($case_registration as $case_registrations) 
                                   <option value="{{ $case_registrations->reg_id }}" {{ $case_registrations->reg_id == $get_record[0]->upload_case_id ? 'selected="selected"' : '' }} >{{$case_registrations->reg_case_number}} </option>
                                 @endforeach
                                 @else
                                 @foreach($case_registration as $case_registrations)
                                   <option value="{{ $case_registrations->reg_id }}">{{$case_registrations->reg_case_number}} </option>
                                 @endforeach
                                 @endif                                 
                              </select>
                              <i class="arrow"></i>
                            </label>
                        </div>
                      </div>

                      <div class="col-md-2">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;"> File No : </label>
                            <!-- <label for="artist_state" class="field select"> -->
                              <label for="level" class="field prepend-icon">
                              @if($get_record[0]->upload_id != "")
                                <select class="form-control select-text" name="order_judgment_file_no" onchange="get_detail(this.value)" id="order_judgment_file_no" readonly>
                              @else 
                                <select class="form-control select-text" name="order_judgment_file_no" onchange="get_detail(this.value)" id="order_judgment_file_no">
                              @endif

                              
                                <option value=''>Select File No </option>
                                @if($get_record != "") 
                                 @foreach($case_registration as $case_registrations) 
                                   <option value="{{ $case_registrations->reg_id }}" {{ $case_registrations->reg_id == $get_record[0]->upload_case_id ? 'selected="selected"' : '' }} >{{$case_registrations->reg_file_no}} </option>
                                 @endforeach
                                 @else
                                 @foreach($case_registration as $case_registrations)
                                   <option value="{{ $case_registrations->reg_id }}">{{$case_registrations->reg_file_no}} </option>
                                 @endforeach
                                 @endif                                 
                              </select>
                              <i class="arrow"></i>
                            </label>
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;"> Title of Case : </label>
                            <label for="artist_state" class="field select">
                              {!! Form::text('case_title',$get_record->case_title,array('class' => 'gui-input','id' => 'case_title' , disabled )) !!}
                            </label>
                        </div>
                      </div>
                    
                      <div class="col-md-12">

                        <div class="">
                          <span class="sub_group_new"> Order Judgment Upload </span>
                          <div id="date_error" style="margin-left: 10px;" ></div>
                        </div>


                        @php
                          $counterss  = 0;
                          $images_cat =  \App\Model\Upload_Document\Upload_Document::where(['upload_case_id' => $get_record[0]->upload_case_id ])->get();
                        @endphp

                        @if(count($images_cat) != 0)
                        @foreach($images_cat as $images_cats)

                        @php $counterss++; @endphp
                        
                        <div id="hide_resp{{$counterss}}">
                          
                          {!! Form::hidden('document_upload['.$counterss.'][upload_id]',$images_cats->upload_id,array('class' => 'gui-input','placeholder' => '','id'=>'' )) !!}

                          <div class="col-md-4" style="margin-top: 10px;">
                            <div class="section">
                              <label for="level_name" class="field-label" style="font-weight:600;"> Caption: </label>
                              <label for="level_name" class="field prepend-icon">
                                {!! Form::text('document_upload['.$counterss.'][upload_caption]',$images_cats->upload_caption,array('class' => 'gui-input cl_group_name_search','placeholder' => '','id'=>'' )) !!}
                                <label for="Account Mobile" class="field-icon">
                                  <i class="fa fa-pencil"></i>
                                </label>
                              </label>
                              <ul class="list-group client_result" id="client_result" style="position: absolute; max-height: 280px; overflow: auto; width: 96%; z-index: 99999999999;"></ul>
                            </div>
                          </div>
                              
                          <div class="col-md-4" style="margin-top: 10px;">
                            <div class="section">
                              <label for="level_name" class="field-label" style="font-weight:600;"> Date  </label>
                              <label for="level_name" class="field prepend-icon">

                                @if($images_cats->upload_date == "1970-01-01" )
                                  
                                  {!! Form::text('document_upload['.$counterss.'][upload_date]','',array('class' => 'fromdate_notice gui-input ','placeholder' => '','id'=>'' ,required)) !!}

                                @else

                                  {!! Form::text('document_upload['.$counterss.'][upload_date]',date('d-m-Y',strtotime($images_cats->upload_date)),array('class' => 'gui-input fromdate_notice','placeholder' => '','id'=>'' ,required)) !!}

                                @endif


                                <label for="Account Mobile" class="field-icon">
                                  <i class="fa fa-pencil"></i>
                                </label>
                              </label>
                            </div>
                          </div>

                          <div class="col-md-4">
                            <div class="field" align="left">
                              <label for="documents" class="field-label" style="font-weight:600;" > 
                                <span style="float: left;margin-bottom: 10px;"> Upload Documents</span> 
                                
                                @if($images_cats->upload_images != "") <span id="cleardiv" style="float: right;"><a style="cursor: pointer;" href="{{asset($images_cats->upload_images) }}" target="_Blank">View Document </a></span> @endif

                                </span> 
                              </label>
                              <input type="file" id="registration_document" name="document_upload[{{$counterss}}][upload_images]" style="padding: 2px;border: 1px solid #ccc;width: 100%;margin-bottom: 20px;" />
                            </div>
                          </div>      
                          <span style="float: left;margin-bottom: 10px; margin-left: 15px; margin-top: -15px;"> <b> Note :- </b> Document Should be in .jpg , .jpeg , .png , .gif , .docx , .doc , .txt , .xls , .xlsx and .pdf format will be accepted.</span>

                          <div class="section">
                            <div class="col-md-12" style="margin-left:10px !important;">
                              <button type="button" onclick="removeRecords('{{$counterss}}','{{ $images_cats->upload_id}}');" name="add" id="" class="btn btn-danger pull-right mr10 mb10"><i class="fa fa-trash-o" aria-hidden="true"></i> &nbsp; Remove Fields
                              </button>
                            </div>
                          </div>

                        </div> <div class="clearfix"> </div>
                        
                        @endforeach
                        @endif

                        <div id="questionrowsres"></div>

                        @if($get_record == "")
                        <!-- 
                        <div class="questionrowsres" id="questionrowsres">
                          <div class="questiondivsres" id="questiondivsres"> -->  
                                
                            <div class="col-md-4" style="margin-top: 10px;">
                              <div class="section">
                                <label for="level_name" class="field-label" style="font-weight:600;"> Caption : </label>
                                <label for="level_name" class="field prepend-icon">
                                  {!! Form::text('document_upload[0][upload_caption]',$get_record[0]->upload_caption,array('class' => 'gui-input cl_group_name_search','placeholder' => '','id'=>'' )) !!}
                                  <label for="Account Mobile" class="field-icon">
                                    <i class="fa fa-pencil"></i>
                                  </label>
                                </label>
                                <ul class="list-group client_result" id="client_result" style="position: absolute; max-height: 280px; overflow: auto; width: 96%; z-index: 99999999999;"></ul>
                              </div>
                            </div>
                            
                            <div class="col-md-4" style="margin-top: 10px;">
                              <div class="section order_section_1">
                                <label for="level_name" class="field-label" style="font-weight:600;"> Date   </label>
                                <label for="level_name" class="field prepend-icon" id="order_judgement_1">
                                  {!! Form::text('document_upload[0][upload_date]','',array('class' => 'gui-input fromdate_notice','placeholder' => '','id'=>'','min'=>'2018-01-01' ,required)) !!}
                                  <label for="Account Mobile" class="field-icon">
                                    <i class="fa fa-pencil"></i>
                                  </label>
                                </label>
                              </div>
                            </div>

                            <div class="col-md-4">
                              <div class="field" align="left">
                                <label for="documents" class="field-label" style="font-weight:600;" > 
                                  <span style="float: left;margin-bottom: 10px;"> Upload Documents</span> 
                                  <span id="cleardiv" style="float: right;display: none;"><a style="cursor: pointer;" onclick="remove_img()">Clear</a></span> 
                                </label>
                                <input type="file" id="registration_document" name="document_upload[0][upload_images]" style="padding: 2px;border: 1px solid #ccc;width: 100%;margin-bottom: 20px;" />
                              </div>
                            </div>

                            <span style="float: left;margin-bottom: 10px; margin-left: 15px; margin-top: -15px;"> <b> Note :- </b> Document Should be in .jpg , .jpeg , .png , .gif , .docx , .doc , .txt , .xls , .xlsx and .pdf format will be accepted. </span>

                            <div id="questionrowsres"></div>
                                
                            <!-- 
                          </div>
                        </div> -->
                        @endif

                        <div class="section">
                          <div class="col-md-12"> 
                            <button type="button" onclick="addrecord();" name="add" id="" class="btn btn-primary pull-right"> <i class="fa fa-plus" aria-hidden="true"></i> &nbsp; Add Other </button>
                          </div>
                        </div>
                        
                        <input type="hidden" name="countidres1" id="countidres1" value="{{count($images_cat)}}"> <br>
                        <input type="hidden" name="countidres2" id="countidres2" value="0">
                      </div>
                      </div>
                      <div class="clearfix"><br></div>


                        
                        
<!-- 
                        <div class="col-md-12">
                          <div class="field" align="left">
                            <label for="documents" class="field-label" style="font-weight:600;" > <span style="float: left;margin-bottom: 10px;"> Upload Documents</span> <span id="cleardiv" style="float: right;display: none;"><a style="cursor: pointer;" onclick="remove_img()">Clear</a></span> </label>
                            <input type="file" id="registration_document" name="img_reg[]" multiple style="padding: 10px;border: 1px solid #ccc;width: 100%;margin-bottom: 20px;" />
                          </div>
                        </div> -->

                        <!-- <div class="col-md-12">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;" >File Upload :  </label>  
                          <label for="level_name" class="field maker_file_upload">
                            {!! Form::file('img_reg[]',array('class' => 'gui-input image_register','multiple' => 'multiple','id'=>'' )) !!}                          
                          </label>
                        </div>
                        </div> -->

                        <!-- <div>
                          @if($get_record != "")
                            @php
                              $images_cat =  \App\Model\Upload_Document\Upload_Document::where(['upload_case_id' => $get_record[0]->reg_id ])->get();
                            @endphp
                          @endif
                          @if(!empty($images_cat))
                            @foreach($images_cat as $images_cats) 
                              <div class="col-md-3" style="margin-bottom: 22px;position: relative;" id="cat_img{{$images_cats['upload_id']}}">
                              <div onclick="remove_image({{$images_cats['upload_id']}})" style="position: absolute;font-size: 25px;color: #232121;right: 21px;top: 6px;cursor: pointer;"><i class="fa fa-times-circle"></i></div>
                                {!! Html::image($images_cats['upload_images'], '', array('class' => 'media-object mw150 customer_profile', 'width'=>'100%', 'height'=>'250')) !!}
                              </div>
                            @endforeach
                          @endif
                        </div>

                        <div class="document_image">
                          <div id="document_image"></div>
                        </div> -->

                     </div>

                  <div class="panel-footer text-right">
                      {!! Form::submit('Save', array('class' => 'button btn-primary', 'id' => 'maskedKey_submit')) !!}
                      {!! Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}
                  </div>   
                    {!! Form::close() !!}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>


<style type="text/css">

.divider{
  display: none;
}
#form_add_question .field-icon {
    margin-top: 0px !important;
}

</style>

<script type="text/javascript">
  

  $(document).on("keyup",".fromdate_notice",function(){
      $('#maskedKey_submit').removeAttr('disabled');
      
      var  currentdate = this.value;
      var n=$(this);    
      var checkdate = /^([0]?[1-9]|[1|2][0-9]|[3][0|1])[-]([0]?[1-9]|[1][0-2])[-]([0-9]{4}|[0-9]{4})$/.test(currentdate);
      
      if(checkdate == true) {

        $('#date_error').html("");
        $('#maskedKey_submit').removeAttr('disabled');
      //   value=currentdate.split("-");
      //   var newDate_notice =value[1]+"/"+value[0]+"/"+value[2];
      //   var notice_status_date = new Date(newDate_notice).getTime();

        
      //   var notice_issue_date = $('#current_date').val();
      //   notice_issue_date = notice_issue_date.split("-");
      //   var newDate_notice_issue = notice_issue_date[1]+"/"+notice_issue_date[0]+"/"+notice_issue_date[2];
      //   var notice_issue_date = new Date(newDate_notice_issue).getTime();


      //   if(notice_status_date > notice_issue_date){
      //     n.next().next().html("");
      //     n.next().next().css("color", "#DE888A");
      //     n.parent().addClass("state-success");
      //     $('.mysave').removeAttr('disabled');
      //   } else {
      //     n.next().next().html("Next date must be greater than current date.");
      //     n.next().next().css("color", "#DE888A");
      //     n.parent().addClass("state-error");
      //     n.parent().removeClass("state-success");
      //     $(this).closest('.mysave').html('');
      //     $('.mysave').attr('disabled','disabled');
      //     return false;
      //   }

        
      } else {
        $('#date_error').html("(Enter date in dd-mm-yyyy format.)");
        $('#date_error').css("color", "#DE888A");
        $('#date_error').addClass("state-error");
        $('#maskedKey_submit').attr('disabled','disabled');
        return false;
      }
  });

  
  // get title and type by case

  function get_detail(reg_id){

  //  if(reg_id != 0){

      BASE_URL = '{{ url('/')}}';
      $.ajax({
        url:BASE_URL+"/advocate-panel/get-case-registeration/"+reg_id,
        success: function(result){ 
          $("#case_title").val(result.reg_petitioner+' v/s '+result.reg_respondent);
          $("#order_judgment_case_type").val(result.case_name);
          $("#order_judgment_case_no").val(result.reg_case_no);
          $("#court_type").val(result.reg_court);
          $("#court_name").val(result.reg_court_id);
          $("#choose_case_type").val(result.reg_case_type_id);


        }
      });
  //  } 
  }


  // choose_case

  function choose_case(case_type) { 

  //  if(case_type != ""){
    var court_type = $("#court_type").val();

  //  alert(court_type);

    BASE_URL = '{{ url('/')}}';
      $.ajax({
        url:BASE_URL+"/advocate-panel/ajax-case-type/"+case_type+"/"+court_type,
        success: function(result){
            $("#order_judgment_case_no").html(result);
        }
      });
  //  }
  }

  // choose court  case_type

  // function choose_court(court_type) { 

  //  // if(court_type != ""){

  //   var case_type = $("#case_type").val();

  //   BASE_URL = '{{ url('/')}}';
  //     $.ajax({
  //       url:BASE_URL+"/advocate-panel/ajax-case-type/"+case_type+"/"+court_type,
  //       success: function(result){
  //           $("#order_judgment_case_no").html(result);
  //       }
  //     });

  //  // }
  // }

  function choose_court(court_type) { 
    BASE_URL = '{{ url('/')}}';
      $.ajax({
        url:BASE_URL+"/advocate-panel/compliance-court-type/"+court_type,
        success: function(result){
            $("#court_name").html(result);
        }
      });
  }



  function choose_court_name(court_name) { 


    var court_type = $("#court_type").val();

    BASE_URL = '{{ url('/')}}';
      $.ajax({
        url:BASE_URL+"/advocate-panel/order-judgement-case-type/"+court_name+"/"+court_type,
        success: function(result){
            $("#choose_case_type").html(result);
        }
      });
  }

  // choose_year

  function choose_year(case_year) { 

    BASE_URL = '{{ url('/')}}';

    var case_type = $("#case_type").val();
      $.ajax({
        url:BASE_URL+"/advocate-panel/ajax-case-year/"+case_year+"/"+case_type,
        success: function(result){
            $("#order_judgment_case_no").html(result);
        }
      });
  }


  // choose_case_no

  function choose_case_no(case_no) { 

   // if(case_no != ""){

    BASE_URL = '{{ url('/')}}';

      $.ajax({
        url:BASE_URL+"/advocate-panel/ajax-case-no/"+case_no,
        success: function(result){
            $("#order_judgment_file_no").html(result);
        }
      });
   // }
  }

  

</script>

<!-- <script type="text/javascript">

  function remove_img() {
    $("#registration_document").val('');
    $('.document_image').html('<div id="document_image"></div>');
  }

  $(document).ready(function() {
    if (window.File && window.FileList && window.FileReader) {
      $("#registration_document").on("change", function(e) {
        $("#cleardiv").show();  
        $('.document_image').html('<div id="document_image"></div>');
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
        var files = e.target.files,
          filesLength = files.length;
        for (var i = 0; i < filesLength; i++) {
          var f = files[i]
          var fileReader = new FileReader();
          var file = e.target;
          
         if (regex.test(f.name.toLowerCase())) {
  
            fileReader.onload = (function(e) {          
              $("<div id class=\"col-md-3\">" +
                "<img class=\"imageThumb\" width=\"100%\" height=\"250\" style=\"margin-bottom:22px;\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
                "<br/><span class=\"\"></span>" +
                "</div>").insertAfter("#document_image");
            });

          } else {
            alert(f.name + " is not a valid image file.");
            dvPreview.innerHTML = "";
            return false;
          }

          fileReader.readAsDataURL(f);
        }
      });
    } else {
      alert("Your browser doesn't support to File API")
    }
  });


  // remove image 

  function remove_image(imid) {
  
      $('#lodding_image').show();
      $.ajax({
       url:'{{url("advocate-panel/delete-document")}}'+'/'+imid,  
       success:function(response){
      // alert(response);
       $('#lodding_image').hide();
       $('#cat_img'+imid).remove();
       }
      });
    
  }


  // power respondent 

  function reg_power_respondent(reg_power_respondent){

    if(reg_power_respondent == 2){
      $('#reg_power_respondent').show();
    } else {
      $('#reg_power_respondent').hide();
    }

  }

  // sms type

  function reg_sms_type(){

    if ($('#sms_type').is(":checked")){
      $('#show_other_mobile').show();
    } else {
      $('#show_other_mobile').hide();      
    }

  }
  // court_type_trail

  function court_type_trail(court_type) {
    
    document.getElementById("hide_show_class_code").style.display = "none";
    document.getElementById("hide_show_ncv").style.display = "block";
    

    BASE_URL = '{{ url('/')}}';
      $.ajax({
        url:BASE_URL+"/advocate-panel/get-all-court/"+court_type,
        success: function(result){
            $("#display_high_court").html(result);
        }
      });

  }

  // court_type_high

  function court_type_high(court_type) {
    
    document.getElementById("hide_show_ncv").style.display = "none";
    document.getElementById("hide_show_class_code").style.display = "block";
    

    BASE_URL = '{{ url('/')}}';
      $.ajax({
        url:BASE_URL+"/advocate-panel/get-all-court/"+court_type,
        success: function(result){
            $("#display_high_court").html(result);
        }
      });

  }

  // choose_sub_type

  function choose_sub_type(client_id){

    if (client_id != ''){
      BASE_URL = '{{ url('/')}}';
      $.ajax({
        url:BASE_URL+"/advocate-panel/change-client-sub-type-ajax/"+client_id,
        success: function(result){
            $("#hide_choose_sub_type").html(result);
        }
      });

      
      $.ajax({
        url:BASE_URL+"/advocate-panel/get-case-category/"+client_id,
        success: function(result){
            if(result.status == true){
              $("#reg_case_type_category").val(1);
              document.getElementById("fir_show").style.display = "block";
            } else {
              $("#reg_case_type_category").val(2);
              document.getElementById("fir_show").style.display = "none";
            }
        }
      });
    }
  }

  // check case no

  function check_case_no(case_number){

  //  alert(case_number);

    BASE_URL = '{{ url('/')}}';
    var reg_case_type_category = $("#reg_case_type_category").val();
    var case_type = $("#case_type").val();
    
    $.ajax({
      url:BASE_URL+"/advocate-panel/check-case-number/"+case_number+"/"+case_type+"/"+reg_case_type_category,
      success: function(result){
        if(result.status == true){
          $("#case_number_error").html('<div class="state-error"></div><em for="services_excerpt" class="state-error"> This case number already exits </em>');
          $(".case_number_error").removeClass("state-success");
          $(".case_number_error").addClass("state-error");
          $('#maskedKey_submit').prop('disabled', true);
          return false;
          
        } else {
          $('#maskedKey_submit').prop('disabled', false);
          $(".case_number_error").removeClass("state-error");
          $(".case_number_error").addClass("state-success");
          $("#case_number_error").html("");
        }
      }
    });
  }

// function type_change_ajax_id(type_id){

//   if (type_id != ''){
//     BASE_URL = '{{ url('/')}}';
//     $.ajax({
//       url:BASE_URL+"/advocate-panel/change-type-fir-change/"+type_id,
//       success: function(result){
//           $("#type_change_ajax").html(result); 
//       }
//     });
//   }

// }


function act_type_change_ajax(act_type_id){

  if (act_type_id != ''){
    BASE_URL = '{{ url('/')}}';
    $.ajax({
      url:BASE_URL+"/advocate-panel/change-act-sub-type-section-ajax/"+act_type_id,
      success: function(result){
          $("#act_sub_type_section").html(result); 
      }
    });
  }

}

function sub_group_change_ajax(sub_group_change_id){

  if (sub_group_change_id != ''){
    BASE_URL = '{{ url('/')}}';
    $.ajax({
      url:BASE_URL+"/advocate-panel/change-sub-group-ajax/"+sub_group_change_id,
      success: function(result){
          
          if(result.status == true){
            document.getElementById("show_client_sub_group").style.display = "none";
          } else {
            document.getElementById("show_client_sub_group").style.display = "block";
            $("#sub_group_change").html(result); 
          }

      }
    });

  }

}

</script>


<script>

function myFunction() {
    var x = document.getElementById("myDIV");
    if (x.style.display === "none") {
        x.style.display = "block";
    }else{
        x.style.display = "block";
    }
}
</script>

<style>
#myDIV{
    width: 100%;
    padding: 50px 0;
    text-align: left !important;
    margin-top: 20px;
    display:none !important;
}
</style> -->


<script>

    // function showhide()
    //  {
    // var div = document.getElementById("myDIV");
    // if (div.style.display !== "none") {
    //     div.style.display = "block";
    // }
    //   div.style.display = "none";
    // }
     
</script>

<style>
#subtype {
    width: 100%;
    padding: 50px 0;
    text-align: left !important;
    margin-top: 20px;
    display:none;
}
</style>
<script type="text/javascript">

//   $(".disposal_from").keyup(function(){
//       var  currentdate = this.value;

//       var checkdate = /^([0]?[1-9]|[1|2][0-9]|[3][0|1])[-]([0]?[1-9]|[1][0-2])[-]([0-9]{4}|[0-9]{4})$/;
      
//       if(checkdate.test(currentdate)) {

//       } else {

//         $("#order_judgement_1").addClass("state-error");
//         $("#order_judgement_1").removeClass("state-success");
// //        $(".order_section_1 .state-error").html("Enter date in dd-mm-yyyy format");

//         return false;
//       }
//   });
  

    $('.cl_group_name_search').keyup(function(){
        var searchField = this.value;
        var n=$(this);
        n.parent().next().html('');
         
        var expression = new RegExp(searchField, "i");

        $.ajax({
            url: BASE_URL+"/advocate-panel/get-all-caption?caption="+searchField,       
            success: function(result) {
              n.parent().next().html(result);
            }
        });
    });
    $('.client_result').on('click', 'li', function() {
        var click_text  = $(this).text();
            // var name_1      = click_text[0];
            // var name_2      = click_text[1];
            // var name_3      = name_2.substr(0,3);
            //$('#cl_group_name').val($.trim(click_text));
        $(".client_result").html('');
    });
  


  jQuery(document).ready(function() {
  
    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "This field contains alphabets only");

    jQuery.validator.addMethod("checkemail", function(value, element) 
    {
    return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
    }, "Enter a VALID email address"); 

    $.validator.addMethod('minStrict', function (value, el, param) {
     return value > param; 
    });
    $.validator.addMethod('maxStrict', function (value, el, param) {
     return value < param; 
    });


    jQuery.validator.addMethod("checkdate", function(value, element) 
    {
    return this.optional(element) || /^([0]?[1-9]|[1|2][0-9]|[3][0|1])[-]([0]?[1-9]|[1][0-2])[-]([0-9]{4}|[0-9]{4})$/.test(value);
    }, "Enter date in dd-mm-yyyy format");


    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#form_add_question").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        img_reg: {
          // required: true,
          extension: 'jpeg,jpg,png',
        },
        order_judgment_file_no: {
          required: true
        },
        document_upload: {
          required: true
        },
        // type: {
        //   required: true
        // },
        // sub_type: {
        //   required: true
        // },
        // choose_type: {
        //   required: true
        // },
        // date_picker: {
        //   required: true
        // },
        // court: {
        //   required: true
        // },
        // fir: {
        //   required: true
        // },
        // petitioner: {
        //   required: true,
        //   lettersonly: true
        // },
        // respondent: {
        //   required: true,
        //   lettersonly: true
        // },
        // extra_party: {
        //   required: true
        // },
        // reg_stage: {
        //   required: true
        // },
      },
      /* @validation error messages 
      ---------------------------------------------- */

      messages: {
        img_reg: {
          extension: 'Image Should be in .jpg,.jpeg and .png format only'
        },
        order_judgment_file_no: {
          required: 'Select file no'
        },
        // document_upload[]: {
        //   required: 'Select file no'
        // },
      },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });
  // petitioner party code

  // function addRecord(){
  //   var count1 = $('#countids1').val();
  //   var count2 = $('#countids2').val();

  //   var counter1 = parseInt(count1) + 1;
  //   var counter2 = parseInt(count2) + 1;
  //   $('#countids1').val(counter1);
  //   $('#countids2').val(counter2);

  //   if(count2 >= 0){
  //     $('#questionrowss').append('<div class="questiondivss" id="questiondivss'+counter1+'"><div class="divider"></div>          <div class="col-md-12"><div class=""><label for="level_name" class="field-label" style="font-weight:600;" >Petitioner '+counter1+' </label><label for="level_name" class="field prepend-icon"><label for="Account Mobile" class="field-icon"></label></label></div></div>            <div class="col-md-12"><div class="section"><label for="level_name" class="field-label" style="font-weight:600;" >Petitioner Name : </label><label for="level_name" class="field prepend-icon"><input type="text" name="petitioner_extra['+counter1+'][petitioner_name]" value="" class="gui-input" placeholder="" required><label for="Account Mobile" class="field-icon"><i class="fa fa-pencil"></i></label></label></div></div><div class="col-md-12"><div class="section"><label for="level_name" class="field-label" style="font-weight:600;" >Petitioner Father Name : </label><label for="level_name" class="field prepend-icon"><input type="text" name="petitioner_extra['+counter1+'][petitioner_father_name]" value="" class="gui-input" placeholder="" required><label for="Account Mobile" class="field-icon"><i class="fa fa-pencil"></i></label></label></div></div><div class="col-md-12"><div class="section"><label for="level_name" class="field-label" style="font-weight:600;" >Petitioner Address : </label><label for="level_name" class="field prepend-icon"><input type="textarea" name="petitioner_extra['+counter1+'][petition_address]" value="" class="gui-textarea" placeholder="" required><label for="Account Mobile" class="field-icon"><i class="fa fa-pencil"></i></label></label></div></div><div class="section"><div class="col-md-12"><button type="button" onclick="removeRecord('+counter1+');" name="add" id="" class="btn btn-danger pull-right mb10"> <i class="fa fa-trash-o" aria-hidden="true"></i> &nbsp; Remove Fields </button></div></div></div></div>');
  //   }
  // }

  // function removeRecord(countnew){
  //   $( "#questiondivss"+countnew+"" ).remove();
  //   $( "#hide_pedi"+countnew+"" ).remove();

  //   var count2 = $('#countids2').val();
  //   var counter2 = parseInt(count2) - 1;
  //   $('#countids2').val(counter2);
  // }

  // respondence code
  function addrecord(){
    var count1 = $('#countidres1').val();
    var count2 = $('#countidres2').val();

    var counter1 = parseInt(count1) + 1;
    var counter2 = parseInt(count2) + 1;
    $('#countidres1').val(counter1);
    $('#countidres2').val(counter2);

    if(count2 >= 0){
      $('#questionrowsres').append('<div class="questiondivsres" id="questiondivsres'+counter1+'"><div class="divider"></div> <div class="col-md-4"><div class="section"><label for="level_name" class="field-label" style="font-weight:600;" > Caption : </label><label for="level_name" class="field prepend-icon"><input type="text" name="document_upload['+counter1+'][upload_caption]" value="" class="gui-input cl_group_name_search" placeholder="" ><label for="Account Mobile" class="field-icon"><i class="fa fa-pencil"></i></label></label></div></div><div class="col-md-4"><div class="section"><label for="level_name" class="field-label" style="font-weight:600;" > Date </label><label for="level_name" class="field prepend-icon"><input type="text" name="document_upload['+counter1+'][upload_date]" value="" class="gui-input fromdate_notice" placeholder="" required="required" ><label for="Account Mobile" class="field-icon"><i class="fa fa-pencil"></i></label></label></div></div>    <div class="col-md-4"><div class="field" align="left"><label for="documents" class="field-label" style="font-weight:600;" > <span style="float: left;margin-bottom: 10px; margin-top: -10px;"> Upload Documents</span> <span id="cleardiv" style="float: right;display: none;"><a style="cursor: pointer;" onclick="remove_img()">Clear</a></span> </label> <input type="file" id="registration_document" name="document_upload['+counter1+'][upload_images]" multiple style="padding: 2px;border: 1px solid #ccc;width: 100%;margin-bottom: 20px;" /> </div> </div> <span style="float: left;margin-bottom: 10px; margin-left: 15px; margin-top: -15px;"> <b> Note :- </b> Document Should be in .jpg , .jpeg , .png , .gif , .docx , .doc , .txt , .xls , .xlsx and .pdf format will be accepted. </span>  <div class="section"><div class="col-md-12"><button type="button" onclick="removerecord('+counter1+');" name="add" id="" class="btn btn-danger pull-right mb10"> <i class="fa fa-trash-o" aria-hidden="true"></i> &nbsp; Remove Fields </button></div></div></div></div>');
    }
  }

  function removerecord(countnew){
    $( "#questiondivsres"+countnew+"" ).remove();
    var count2 = $('#countidres2').val();
    var counter2 = parseInt(count2) - 1;
    $('#countidres2').val(counter2);
  }

  // extra party code

  // function addRecords(){
  //   var count1 = $('#countid1').val();
  //   var count2 = $('#countid2').val();


  //   var counter1 = parseInt(count1) + 1;
  //   var counter2 = parseInt(count2) + 1;
  //   $('#countid1').val(counter1);
  //   $('#countid2').val(counter2);

  //   if(count2 >= 0){
  //     $('#questionrow').append('<div class="questiondiv" id="questiondiv'+count1+'"><div class="divider"></div><div class="col-md-9"><div class="section"><label for="level_name" class="field-label" style="font-weight:600;" >Extra Party : </label><label for="level_name" class="field prepend-icon"><input type="text" name="reg_extra_party['+count1+'][extra_party]" value="" class="gui-input" placeholder="" required><label for="Account Mobile" class="field-icon"><i class="fa fa-pencil"></i></label></label></div></div><div class="section"><div class="col-md-3"><button type="button" onclick="removeRecords('+count1+');" name="add" id="remove_case_reg" class="btn btn-danger pull-right mr30"> <i class="fa fa-trash-o" aria-hidden="true"></i> &nbsp; Remove Fields </button></div></div></div></div>');
  //      // $('#countid').val(count);
  //   }
  // }

  function removeRecords(countnew , upload_id){

    if(confirm("Are you sure to want remove that records?")) {
        

      BASE_URL = '{{ url('/')}}';
      $.ajax({
        url:BASE_URL+"/advocate-panel/delete-document/"+upload_id,
      });


        $( "#questiondiv"+countnew+"" ).remove();
        $( "#hide_resp"+countnew+"" ).remove();
        var count2 = $('#countid2').val();
        var counter2 = parseInt(count2) - 1;
        $('#countid2').val(counter2);

    }

  }


  </script>



@endsection
