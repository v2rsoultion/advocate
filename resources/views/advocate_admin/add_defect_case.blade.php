@extends('advocate_admin/layout')
@section('content')

  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="{{ url('/advocate-panel/view-defect-case') }}">View Defect Case</a>
            </li>
            <li class="crumb-icon">
              <a href="{{ url('advocate-panel/dashboard') }}">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="{{ url('advocate-panel/dashboard') }}">Home</a>
            </li>
            <li class="crumb-trail">Add Defect Case</li>
          </ol>
        </div>
      </header>

      <!-- Begin: Content -->

      <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center">
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-heading">
                <span class="panel-title">Add Defect Case</span>
              </div>
              <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                <!-- IF any error found -->
                <div class="col-md-12">
                @if ($errors->any())
                  <div id="log_error" class="alert alert-danger" style='font-family: josefin_sansregular !important;'>
                  {{$errors->first()}}  </i></div>
                @endif
                </div>
                <!-- IF any error found -->
                  <div id="tab1_1" class="tab-pane active">
                    {!! Form::open(['name'=>'form_add_question','url'=>'advocate-panel/insert-defect-case/'.$get_record[0]->defect_id,'id'=>'form_add_question' ,'autocomplete'=>'off']) !!}
                    <div class="row">
                        <div class="col-md-6">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Case No : </label>
                              <label for="artist_state" class="field select">
                                <select class="select2-single form-control" name="case_no" id="user_role" onchange="defect_caseno_ajax(this.value)">
                                  <option value=''>--Select-Option--</option>
                                  @if($get_record != "") 
                                   @foreach($def_case_no as $def_case_nos) 
                                     <option value="{{ $def_case_nos->reg_id }}" {{ $def_case_nos->reg_id == $get_record[0]->defect_case_no ? 'selected="selected"' : '' }} >{{$def_case_nos->reg_case_number}} </option>
                                   @endforeach
                                   @else
                                   @foreach($def_case_no as $def_case_nos)
                                     <option value="{{ $def_case_nos->reg_id }}">{{$def_case_nos->reg_case_number}} </option>
                                   @endforeach
                                   @endif
                                </select>
                              </label>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Title :  </label>
                            <label for="level_name" class="field prepend-icon">
                              {!! Form::text('title','',array('class' => 'gui-input','placeholder' => '','id'=>'defect_caseno_change','disabled'=>'disabled' )) !!}
                              <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-pencil"></i>
                              </label>
                            </label>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Act Reason :  </label>
                            <label for="level_name" class="field prepend-icon">
                              {!! Form::text('act_reason',$get_record[0]->defect_reason,array('class' => 'gui-input','placeholder' => '','id'=>'' )) !!}
                              <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-pencil"></i>
                              </label>
                            </label>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Status : </label>
                              <label for="artist_state" class="field select">
                                <select class="form-control" name="status_date_s">
                                  <option value=''>--Select-Option--</option>
                                  <option value='2' <?php if($get_record[0]->defect_status_date == "2"){ echo "selected";}?>>Filled</option>
                                  <option value='1' <?php if($get_record[0]->defect_status_date == "1"){ echo "selected";}?>>Pending</option>
                                </select>
                                <i class="arrow double"></i>
                              </label>
                          </div>
                        </div>
                      </div>
                    </div>

                  <div class="panel-footer text-right">
                      {!! Form::submit('Save', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}
                      {!! Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}
                  </div>   
                    {!! Form::close() !!}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>


<script>
function showDiv(show_id) {

  if (show_id > 1){
    document.getElementById('welcomeDiv').style.display = "block";
  }else if (show_id = 1){
      document.getElementById('welcomeDiv').style.display = "none";
  }

}
</script>

<script type="text/javascript">

function defect_caseno_ajax(defect_case_no_id){

  if (defect_case_no_id != ''){
    BASE_URL = '{{ url('/')}}';
    $.ajax({
      url:BASE_URL+"/advocate-panel/change-defect-case-no-ajax/"+defect_case_no_id,
      success: function(result){
        $("#defect_caseno_change").val(result.petitioner+' V/S '+result.respondent);
      }
    });
  }

}

</script>      

<script type="text/javascript">

  jQuery(document).ready(function() {
  
    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "This field contains alphabets only");

    jQuery.validator.addMethod("checkemail", function(value, element) 
    {
    return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
    }, "Enter a VALID email address"); 

    $.validator.addMethod('minStrict', function (value, el, param) {
     return value > param; 
    });
    $.validator.addMethod('maxStrict', function (value, el, param) {
     return value < param; 
    });


    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#form_add_question").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        status_date: {
          required: true
        },
        case_no: {
          required: true
        },
        status_date_s: {
          required: true
        },
        title: {
          required: true
        },
        act_reason: {
          required: true
        },
      },

      /* @validation error messages 
      ---------------------------------------------- */

      messages: {
        status_date: {
          required: 'Please Fill Required Status Date'
        },
        status_date_s: {
          required: 'Please Fill Required Date'
        },
        case_no: {
          required: 'Please Fill Required Case No'
        },
        title: {
          required: 'Please Fill Required Title'
        },
        act_reason: {
          required: 'Please Fill Required Act Reason'
        },        
      },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });


  </script>



@endsection
