@extends('advocate_admin/layout')

@section('content')

  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="{{ url('/advocate-panel/view-fir') }}">View Fir</a>
            </li>
            <li class="crumb-icon">
              <a href="{{ url('advocate-panel/dashboard') }}">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="{{ url('advocate-panel/dashboard') }}">Home</a>
            </li>
            <li class="crumb-trail">Add Fir</li>
          </ol>
        </div>
      </header>

      <!-- Begin: Content -->

      <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center">
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-heading">
                <span class="panel-title">Add Fir</span>
              </div>
              <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                <!-- IF any error found -->
                <div class="col-md-12">
                @if ($errors->any())
                  <div id="log_error" class="alert alert-danger" style='font-family: josefin_sansregular !important;'>
                  {{$errors->first()}}  </i></div>
                @endif
                </div>
                <!-- IF any error found -->
                  <div id="tab1_1" class="tab-pane active">
                    {!! Form::open(['name'=>'form_add_question','url'=>'advocate-panel/insert-fir/'.$get_record[0]->fir_id,'id'=>'form_add_question' ,'autocomplete'=>'off']) !!}
                    <div class="row">
                      <div class="col-md-4">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;" >Fir No :  </label>  
                          <label for="level_name" class="field prepend-icon">
                            {!! Form::text('fir_no',$get_record[0]->fir_number,array('class' => 'gui-input','placeholder' => '' )) !!}
                              <label for="Account Mobile" class="field-icon">
                              <i class="fa fa-pencil"></i>
                            </label>                           
                          </label>
                        </div>
                        </div>
                      <div class="col-md-4">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;" >Fir Type :  </label>  
                          <label for="level_name" class="field prepend-icon">
                            {!! Form::text('fir_type',$get_record[0]->fir_year,array('class' => 'gui-input','placeholder' => '' )) !!}
                              <label for="Account Mobile" class="field-icon">
                              <i class="fa fa-pencil"></i>
                            </label>                           
                          </label>
                        </div>
                        </div>
                        <div class="col-md-4">
                        <div class="section">
                          <label for="level_name" class="field-label" style="font-weight:600;" >Police Thane Name :  </label>  
                          <label for="level_name" class="field prepend-icon">
                            {!! Form::text('police_thane_name',$get_record[0]->fir_pt_name,array('class' => 'gui-input','placeholder' => '' )) !!}
                              <label for="Account Mobile" class="field-icon">
                              <i class="fa fa-pencil"></i>
                            </label>                           
                          </label>
                        </div>
                        </div>
                     </div>
                     </div>

                    <div class="panel-footer text-right">
                      {!! Form::submit('Save', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}
                      {!! Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}
                    </div>   
                    {!! Form::close() !!}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

<script type="text/javascript">

  jQuery(document).ready(function() {
  
    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "This field contains alphabets only");

    jQuery.validator.addMethod("checkemail", function(value, element) 
    {
    return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
    }, "Enter a VALID email address"); 

    $.validator.addMethod('minStrict', function (value, el, param) {
     return value > param; 
    });
    $.validator.addMethod('maxStrict', function (value, el, param) {
     return value < param; 
    });


    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#form_add_question").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        fir_no: {
          required: true
        },
        fir_type: {
          required: true
        },
        police_thane_name: {
          required: true
        },
      },

      /* @validation error messages 
      ---------------------------------------------- */

      messages: {
        fir_no: {
          required: 'Please Fill Requerd Fir No'
        },
        fir_type: {
          required: 'Please Fill Requerd Fir Type'
        },
        police_thane_name: {
          required: 'Please Fill Requerd Police Thane Name'
        },        
      },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });


  </script>



@endsection
