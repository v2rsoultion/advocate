@extends('mygo_admin/layout')

@section('content')

  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active text-capitalize">
              <a href="{{ url('/mygo-panel/master-pages') }}/{{ $get_record['pages_slug'] }}"> {{ $get_record['pages_title'] }} </a>
            </li>
            <li class="crumb-icon">
              <a href="{{ url('/mygo-panel/dashboard') }}">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="{{ url('/mygo-panel/dashboard') }}">Home</a>
            </li>
            <li class="crumb-trail text-capitalize"> {{ $get_record['pages_title'] }} </li>
          </ol>
        </div>
      </header>

      <div class="row">
        <div class="col-md-12">
          @if (\Session::has('success'))
            <div class="alert alert-success" style="padding: 10px; border: 0px; text-align: center; margin: 10px;">
                {!! \Session::get('success') !!}
            </div>
          @endif
        </div>
      </div>

      <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center">
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-heading">
                <span class="panel-title text-capitalize"> {{ $get_record['pages_title'] }} </span>
              </div>
              <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                  <div id="tab1_1" class="tab-pane active">
                    
                  @if ($errors->any())
                    <div id="log_error" class="alert alert-danger"><i class="fa fa-thumbs-o-down"> {{$errors->first()}} </i></div>
                  @endif

                    {!! Form::open(['url'=>'mygo-panel/save-master-pages' , 'name'=>'form', 'enctype' => 'multipart/form-data' , 'id' => 'validation' ,'autocomplete'=>'off' ] ) !!}
                    
                    {!! Form::hidden('pages_id',$get_record['pages_id'], array('class' => 'event-name gui-input br-light light focusss','placeholder' => '' )) !!}

                    <div class="row">
                      <div class="col-md-12">
                        <div class="col-md-12">
                          <div class="section">
                            <label for="pages_title" class="field-label" style="font-weight:600;" > Title </label>
                              <label for="pages_title" class="field prepend-icon">
                                {!! Form::text('pages_title',$get_record['pages_title'], array('class' => 'event-name gui-input br-light light focusss','placeholder' => '', 'disabled' => 'disabled' )) !!}
                                <label for="store-currency" class="field-icon">
                                  <i class="glyphicons glyphicons-edit"></i>
                                </label>
                              </label>
                          </div>
                        </div>

                        <div class="col-md-12">
                          <div class="section">
                            <label for="pages_excerpt" class="field-label" style="font-weight:600;" > Excerpt </label>
                              <label for="pages_excerpt" class="field prepend-icon">
                                {!! Form::textarea('pages_excerpt',$get_record['pages_excerpt'], array('class' => 'gui-textarea accc','placeholder' => '' )) !!}
                                <label for="store-currency" class="field-icon">
                                  <i class="fa fa-comments"></i>
                                </label>
                              </label>
                          </div>
                        </div>

                        <div class="col-md-12">
                          <div class="section">
                            <label for="pages_description" class="field-label" style="font-weight:600;" > Description </label>
                              <label for="pages_description" class="field prepend-icon">
                                {!! Form::textarea('pages_description',$get_record['pages_description'], array('class' => 'gui-textarea accc jqte-test','id' => 'pages_description' )) !!}
                              </label>
                            <div id="descrption_error" class="" ></div>
                          </div>
                        </div>     
                      </div>
                    </div>

                    <div class="panel-footer text-right">
                      <button type="button" class="button btn-primary mysave" onclick="update_record_pages()"> Update </button>
                      {!! Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!}
                    </div>
                    
                    {!! Form::close() !!}

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- End: Content -->
    </section>

<style>
    
  .jqte_editor{
    height: auto !important;
    min-height: 300px!important;
  }
  .jqte_source{
    height: auto !important;
    min-height: 300px!important;
  }
</style>



<script type="text/javascript">
  jQuery(document).ready(function() {
  
    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#validation").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        pages_title: {
          required: true,
        },

        pages_excerpt: {
          required: true,
        },

        pages_description: {
          required: true,
        },
       
      },

      /* @validation error messages 
      ---------------------------------------------- */

      messages: {
        pages_title: {
          required: 'Enter Pages Title'
        },

        pages_excerpt: {
          required: 'Enter pages Excerpt'
        },

        pages_description: {
          required: 'Enter Pages Description'
        },
      },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });

  function update_record_pages() {
     var pages_description = $("#pages_description").val();
     if(pages_description == ""){
      $('#descrption_error').html('<div class="state-error"></div><em for="services_excerpt" class="state-error">Please enter page description.</em>');
      $(".jqte").css({"border": "1px solid #de888a"});
      $(".jqte_editor").css({"background": "#fee9ea"});
      return false;
     } else {
        if(confirm("Are you sure to want update records?")) {
          $("[name=form]").submit();    
        }
      }
   }

  setInterval(function(){ 
    var pages_description = $("#pages_description").val();
    if (pages_description!="") {
      $('#descrption_error').html('');
      $(".jqte").css({"border": "1px solid #ddd"});
      $(".jqte_editor").css({"background": "#fff"});
    }
  }, 500);


  </script>

@endsection
