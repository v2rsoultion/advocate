@extends('advocate_admin/layout')
@section('content')

  <!-- Start: Content-Wrapper -->
  <section id="content_wrapper">
    <header id="topbar">
      <div class="topbar-left">
        <ol class="breadcrumb">
          <li class="crumb-active">
            <a href="{{url('/advocate-panel/add-pessi')}}">Add Pessi</a>
          </li>
          <li class="crumb-icon">
            <a href="{{ url('advocate-panel/dashboard') }}">
              <span class="glyphicon glyphicon-home"></span>
            </a>
          </li>
          <li class="crumb-link">
            <a href="{{ url('advocate-panel/dashboard') }}">Home</a>
          </li>
          <li class="crumb-trail">View Pessi </li>
        </ol>
      </div>
    </header>

    <div class="" style="margin-top:10px;">
      <div class="col-md-12">
        <div class="panel panel-primary panel-border top mb70">  
          <div class="panel-heading">
            <div class="panel-title hidden-xs">
              <span class="glyphicon glyphicon-tasks"></span> View Pessi </div>
          </div>

          <div class="panel-menu admin-form theme-primary">
            <div class="row">
              {!! Form::open(['url'=>'/advocate-panel/view-pessi']) !!}

                <div class="col-md-3 mb10">
                  <label for="pincode" class="field prepend-icon">
                    <select class="select2-single form-control" id="case_no" name="case_no">
                      <option value='0'>Select Case No.</option>    
                      @foreach($get_case_regestered as $get_case_regestereds)
                        <option value="{{ $get_case_regestereds->reg_id }}" {{ $case_no == $get_case_regestereds->reg_id ? 'selected="selected"' : '' }} > {{ $get_case_regestereds->reg_case_number }}</option>
                      @endforeach                           
                    </select>
                    <i class="arrow double"></i>
                  </label>
                </div>

                <div class="col-md-3 mb10">
                  <label for="pincode" class="field prepend-icon">
                    <select class="select2-single form-control" id="file_no" name="file_no">
                      <option value='0'>Select File No.</option>    
                      @foreach($get_case_regestered as $get_case_regestereds)
                        <option value="{{ $get_case_regestereds->reg_id }}" {{ $file_no == $get_case_regestereds->reg_id ? 'selected="selected"' : '' }} > {{ $get_case_regestereds->reg_file_no }}</option>
                      @endforeach                           
                    </select>
                    <i class="arrow double"></i>
                  </label>
                </div>


                <div class="col-md-3 mb10">
                  <label for="pincode" class="field prepend-icon">
                    <select class="select2-single form-control" id="ncv_no" name="ncv_no">
                      <option value='0'>Select NCV</option>    
                      @foreach($get_case_regestered as $get_case_regestereds)
                        <option value="{{ $get_case_regestereds->reg_id }}" {{ $ncv_no == $get_case_regestereds->reg_id ? 'selected="selected"' : '' }} > {{ $get_case_regestereds->reg_vcn_number }}</option>
                      @endforeach                           
                    </select>
                    <i class="arrow double"></i>
                  </label>
                </div>

                <div class="col-md-3 mb10">
                  <label for="pincode" class="field prepend-icon">
                    <select class="select2-single form-control" id="case_name" name="case_name">
                      <option value='0'>Type Of Case</option>    
                      @foreach($type_case as $type_cases)
                        <option value="{{ $type_cases->case_id }}" {{ $case_name == $type_cases->case_id ? 'selected="selected"' : '' }} > {{ $type_cases->case_name }}</option>
                      @endforeach                           
                    </select>
                    <i class="arrow double"></i>
                  </label>
                </div>
                <div class="col-md-3">
                  <label for="pincode" class="field prepend-icon">
                    <select class="select2-single form-control" id="stage_name" name="stage_name">
                      <option value='0'>Select Stage</option>    
                      @foreach($get_stage as $get_stages)
                        <option value="{{ $get_stages->stage_id }}" {{ $stage_name == $get_stages->stage_id ? 'selected="selected"' : '' }} > {{ $get_stages->stage_name }}</option>
                      @endforeach                           
                    </select>
                    <i class="arrow double"></i>
                  </label>
                </div>

                <div class="col-md-3">
                  <label for="pincode" class="field prepend-icon">
                    {!! Form::text('previous_date',$get_record->reg_nxt_further_date,array('class' => 'gui-input','placeholder' => 'Select Previous Date','id'=>'datepicker' , readonly )) !!}
                    <label for="pincode" class="field-icon">
                      <i class="fa fa-calendar"></i>
                    </label>
                  </label>
                </div>


                <div class="col-md-1">
                  <button type="submit" name="search" class="button btn-primary"> Search </button>
                </div>
              {!! Form::close() !!}             
                <div class="col-md-1 pull-right">
                   <a href="{{ url('/advocate-panel/view-pessi/')}}">{!! Form::submit('Default', array('class' => 'btn btn-primary', 'id' => 'maskedKey')) !!}</a>
                </div>     
            </div>
          </div>

          <div class="panel-body pn">
              {!! Form::open(['url'=>'/advocate-panel/view-pessi','name'=>'form']) !!}
              <div class="table-responsive">
                <table class="table admin-form theme-warning tc-checkbox-1 fs13" id="datatable">
                  <thead>
                    <tr class="bg-light">
                      <th style="width:90px !important;" class="text-left">
                        <label class="option block mn">
                          <input type="checkbox" id="check_all"> 
                          <span class="checkbox mn"></span>
                          Select All
                        </label>
                      </th>
                      <th class="">Case Number</th>
                      <th class="">File No.</th>
                      <th class="">NCV No.</th>
                      <th class="">Type of Case</th>
                      <th class="">Title of Case</th>
                      <th class="">Previous Date</th>
                      <th class="">Stage</th>
                      <th class="">Order Sheet</th>
                      <th class="">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  @foreach($get_record as $get_records)              
                    <tr>
                      <td class="text-left" style="padding-left: 18px;">
                        <label class="option block mn">
                          <input type="checkbox" name="check[]" class="check" value="{{$get_records->pessi_id}}">
                          <span class="checkbox mn"></span>
                        </label>
                      </td>
                      <td class="text-left" style="padding-left:20px"> {{ $get_records->reg_case_number }} </td>
                      <td class="text-left" style="padding-left:20px"> {{ $get_records->reg_file_no }} </td>
                      <td class="text-left" style="padding-left:20px"> {{ $get_records->reg_vcn_number }} </td>
                      <td class="text-left" style="padding-left:20px">  {{ $get_records->case_name }}  </td>
                      <td class="text-left" style="padding-left:20px">  {{ $get_records->reg_respondent }} v/s {{ $get_records->reg_petitioner }}  </td>
                      <td class="text-left" style="padding-left:20px">  
                        @if($get_records->pessi_prev_date == "1970-01-01")
                          -----
                        @else
                          {{ date('d F Y',strtotime($get_records->pessi_prev_date)) }}
                        @endif

                      </td>
                      <td class="text-left" style="padding-left:20px">  {{ $get_records->stage_name }}  </td>

                      <td class="" style="padding-left:20px"> 
                        <a href="#" style="text-transform: capitalize; text-decoration:none;" data-toggle="modal" data-target="#view_message{{$get_records->pessi_id}}" > View Order Sheet </a>
                        <!-- Sign In model -->
                        <div id="view_message{{$get_records->pessi_id}}" class="modal fade in" role="dialog">
                          <div class="modal-dialog" style="width:700px; margin-top:100px;">
                            <div class="modal-content">
                              <div class="modal-header" style="padding-bottom: 35px;">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title pull-left"> Order Sheet </h4>
                              </div>
                              <div class="modal-body">
                                <section style="">
                                  <div class="row">
                                    <div class="col-md-12 text-justify">
                                      {{$get_records->pessi_order_sheet}}
                                    </div>
                                  </div>
                                </section>
                              </div>
                              <div class="clearfix"></div>
                            </div> 
                          </div>
                        </div>
                      </td>

                      <td class="text-right">
                        <div class="btn-group text-left">
                          <button type="button" class="btn btn-success br2 btn-xs fs12 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> Action
                            <span class="caret ml5"></span>
                          </button>
                          <ul class="dropdown-menu" role="menu" style="min-width:130px; left: -67px !important;">
                            <!-- <li>
                              <a href="{{ url('/advocate-panel/user-login') }}/{{$get_records->admin_id}}">User Login</a>
                            </li> -->
                            <li>
                              <a href="{{ url('/advocate-panel/edit-pessi') }}/{{$get_records->pessi_id}}">Edit Pessi</a>
                            </li>
                            <div class="divider"></div>                            
                            <li class="">
                              <a href="{{ url('/advocate-panel/case-registration-detail') }}/{{sha1($get_records->reg_id) }}">View Details</a>
                            </li>
                            <!-- <li class="">
                              <a href="{{ url('/advocate-panel/change-advocate-status') }}/{{$get_records->admin_id}}/0">View Complaince</a>
                            </li> -->
                          </ul>
                        </div>
                      </td>

                      <!-- <td>
                        @if($get_records->pessi_choose_type == 0)
                          Further Date:-  {{ date('d F Y',strtotime($get_records->pessi_further_date)) }}
                        @elseif($get_records->pessi_choose_type == 1)
                          Decide :- @if($get_records->pessi_decide_id == 1) Case in fever @elseif($get_records->pessi_decide_id == 2) Case in against @elseif($get_records->pessi_decide_id == 3) Withdraw @elseif($get_records->pessi_decide_id == 4) None @endif
                        @else
                          Due Course
                        @endif
                      </td> -->

                    </tr>
                  @endforeach()
                  </tbody>
                </table>
              </div>
              {!! Form::close() !!}
          </div>
          <div class="panel-body pn">
            <div class="table-responsive">
              <table class="table admin-form theme-warning tc-checkbox-1 fs13">                                
                <tbody>
                  <tr class="">
                     <th class="text-left">
                      <button type="button" class="btn btn-primary" onclick="go_delete()"><i class="glyphicon glyphicon-trash"></i> Delete Multiple </button>
                    </th>
                    <th>
                      {{ $get_record->links() }}
                    </th>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

<style type="text/css">
.view a{
  color:#fff !important;
}
</style>
<style type="text/css">
.dt-panelfooter{
  display: none !important;
}
</style>

@endsection