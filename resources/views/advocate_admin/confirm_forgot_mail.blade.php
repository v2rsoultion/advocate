<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <title>Gullyy Cricket Email</title>
  <style type="text/css">
  *{
    font-family: sans-serif;
  }
    #outlook a {padding:0;}
    body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
    .ExternalClass {width:100%;}
    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
    #backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
    img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;}
    a img {border:none;display:inline-block;}
    .image_fix {display:block;}
    h1, h2, h3, h4, h5, h6 { margin: 0px;}
    h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: blue !important;}
    h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {
      color: red !important; 
    }
    h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {
      color: purple !important; 
    }
    table td {border-collapse: collapse;}
    table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
    a {color: #000;}
    @font-face {
        font-family: 'bernardo_moda_boldregular';
        src: url('public/fonts/bernardo_moda_bold-webfont.woff2') format('woff2'), url('public/fonts/bernardo_moda_bold-webfont.woff') format('woff');
        font-weight: normal;
        font-style: normal;
    }
    @font-face {
        font-family: 'RobotoLight';
        src: url('public/fonts/RobotoLight.eot');
        src: url('public/fonts/RobotoLight.eot') format('embedded-opentype'), url('public/fonts/RobotoLight.woff2') format('woff2'), url('public/fonts/RobotoLight.woff') format('woff'), url('public/fonts/RobotoLight.ttf') format('truetype'), url('public/fonts/RobotoLight.svg#RobotoLight') format('svg');
    }
    .borderbottom{
      padding: 5px 15px;
      /*background: #fbd103;*/
      background: #302C2A; 
    }
    .footer {
      padding: 15px 0px;
      text-align: center;
      /*background: #fbd103 ;*/
      background: #302C2A;
    }
    .mailtemplate{
      width: 1000px;
      margin: 0px auto;
      padding: 15px 15px;
    }
    .logo{
      float: left;
    }
    .logo img{
      width: 208px;
    }
    .footer{
      color: #fff;
      text-align: center;
      font-size: 14px; 
      
    }
    span{
      color: #fff;
    }
    p{  
      text-align: justify;
      margin: 15px;
      padding: 0px;
      margin: 0px;
       
    }
    .tr{
      padding: 50px 0px;
    }
    .socilicons{
      float: right;
    }
    .socilicons p{
      color: #fff;
      padding: 5px 0px;
      font-size: 14px;
    }
    .socilicons a{
      color: #fff;
      text-decoration: none;
      font-size: 14px;
    }
    .socilicons i{
      margin-right: 10px;
      color: #540000;
    }
    .middlediv{
      padding: 30px 10px;
    }
    .middlediv h1{
      color: rgba(0,0,0,0.9);
     
      font-size: 24px;
      margin-left: 19px;
      padding-bottom: 20px;
      text-transform: capitalize;
    }
    .middlediv p{
      margin: 20px 0px 0px 20px;
      font-size: 14px;
      line-height: 20px;
    }
    .middlediv1{
      padding: 0px 10px 20px 15px;
    }
    .middlediv1 h1{
      color:rgba(0,0,0,0.9);
     
      font-size: 16px;
      margin-left: 15px;
      text-transform: capitalize;
    }
    .imgmail{
      margin: 0px auto;
      width:200px;
    }
    .imgmail img{
      width: 200px;
    }
    .middlediv p a{
      background: #302C2A;
      color: #fff;
      text-align: center;
      padding: 5px 20px;
      display: inline-block;
      text-decoration: none;
    }
    .social{
      text-align: center;
    }
    .social img{
      width: 20px;
    }
  @media (max-width: 461px){
      .mailtemplate{
        width: 100%;
        padding: 0px;
      }
      .logo{
        text-align: center;
        float: none;
      }
      .middlediv p{
        margin:20px 10px;
      }
      .middlediv h1{
        padding-bottom: 0px;
        text-align: center;
      }
      .middlediv1{
        text-align: center;
      }
      .middlediv1 br{
        display: none;

      }
       .middlediv p {
        text-align: center;
       }
      .middlediv p a{
        width: 189px !important;
        display: block;
        margin:0px auto !important;
      }
      .social{
        padding: 20px 0px;
      }
      .middlediv1{
        padding-bottom: 0px;
      }
      .middlediv{
        padding-bottom: 0px;
      }
    }
  </style>

<script type="colorScheme" class="swatch active">
  {
    "name":"Default",
    "bgBody":"ffffff",
    "link":"f2f2f2",
    "color":"#540000",
    "bgItem":"F4A81C",
    "title":"181818"
  }
</script>
@php
   $BASE_URL="http://".$_SERVER['SERVER_NAME'].dirname($_SERVER["REQUEST_URI"].'?').'/';
@endphp
</head>
<body>  
    <div class="mailtemplate">
    <div class="main">
      <div class="borderbottom">
        <div class="logo">
          <img src="{{ URL::asset($logo)}}" width="" height='' alt='Logo'  data-default="placeholder" />
        </div>
        <div class="socilicons">
          
        </div> 
        <div style="clear:both;"></div>
      </div>
      <div class="middlediv">
        <h1>Hello {{ $name }} !</h1>
        <p>
          This email confirms that your password has been changed.<br />
        </p>
      </div>
      <div class="middlediv1">
        <h1>Thank You, <br> GullyyT20 </h1>
      </div>
      <div class="social">
          <a href="https://www.facebook.com/GullyyT20/?ref=br_rs"  target="_blank" title="Facebook"><img src="{{ URL::asset('public/images/mail_fb.png')}}" width="" height='' alt='Facebook'  data-default="placeholder" /></a>
          <a href="https://twitter.com/GullyyT20"   target="_blank" title="Twitter"><img src="{{ URL::asset('public/images/twitter.png')}}" width="" height='' alt='twitter'  data-default="placeholder" /></a>

          <a href="http://www.billionblue.com" title="Bluebillion" target="_blank"><img src="{{ URL::asset('public/images/mail_website.png')}}" width="" height='' alt='twitter'  data-default="placeholder" /></a>
        </div> 
      <div class="footer">
        Copyright &copy; <?php echo date("Y"); ?> <span>blue billion tech pvt.ltd</span> .  All Rights Reserved.
      </div>
    </div>
  </div>
</body>
</html>
