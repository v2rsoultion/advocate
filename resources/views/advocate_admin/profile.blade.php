@extends('advocate_admin/layout')
@section('content')

<style type="text/css">
  .admin-form .select, .admin-form .gui-input, .admin-form .select > select, .admin-form .select-multiple select{
    height: 28px !important;
  }
  .admin-form a.button, .admin-form span.button, .admin-form label.button
  {
    line-height: 28px !important;
  }
  .admin-form .append-icon .field-icon, .admin-form .prepend-icon .field-icon{
    line-height: 28px !important;
  }
  .admin-form .gui-textarea {
    /*line-height: 7px !important;*/
  }
/*  .gui-textarea{
    height: 100px !important;
  }*/
/* .admin-form .gui-input {
  padding: 5px;
 }*/
 /*.admin-form .prepend-icon .field-icon {
  top: 6px;
 }*/
 .admin-form .button{
    height: 28px !important;
    line-height: 1px !important;
  }
 .btn {
    height: 29px !important;
    line-height: 1px !important;  }

 .down{
  margin-top: 70px;
 }
.account_setting {
  margin: 0px 0px !important;
  margin-top: 15px !important;
  /*padding: 5px 0px !important;*/
 }
</style>
  <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <header id="topbar">
        <div class="topbar-left down">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="{{ url('/advocate-panel/profile') }}">Profile</a>
            </li>
            <li class="crumb-icon">
              <a href="{{ url('/advocate-panel/dashboard') }}">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-link">
              <a href="{{ url('/advocate-panel/dashboard') }}">Home</a>
            </li>
            <li class="crumb-trail">Profile</li>
          </ol>
        </div>
      </header>
      <section id="content" class="table-layout animated fadeIn">
        <div class="tray tray-center" style="height:100% !important;">
          <div class="mw1000 center-block">
            <div class="content-header">
              <h2> You can update your <b class="text-primary">Profile here</b> </h2>
              <p class="lead"></p>
            </div>
            <div class="row">
            <div class="col-md-12">
              @if (\Session::has('success'))
              <div class="alert alert-success account_setting">
                {!! \Session::get('success') !!}
              </div>
              @endif
            </div>
          </div>
            <div class="panel panel-primary panel-border top mt20 mb35">
              <div class="panel-heading">
                <span class="panel-title">Profile</span>
              </div>            
              <div class="panel-body bg-light dark">
                <div class="admin-form">
                  {!! Form::open(['url'=>'advocate-panel/account-profile' , 'enctype' => 'multipart/form-data' , 'id' => 'admin-form' ,'autocomplete'=>'off' ]) !!}
                    <div class="col-md-9">
                      <div class="tab-content pn br-n admin-form">
                        <div id="tab1_1" class="tab-pane active">
                          <div class="section row" style="  margin-bottom: 17px;">
                            <div class="col-md-12">  
                              <label for="user_name" class="field prepend-icon">
                                {!! Form::text('admin_name',$admin_detail['admin_name'], array('class' => 'gui-input','placeholder' => 'Enter Name' )) !!}
                                <label for="user_name" class="field-icon">
                                  <i class="fa fa-user"></i>
                                </label>
                              </label>
                            </div>
                          </div>
                          <div class="section row" style="  margin-bottom: 17px;"> 
                            <div class="col-md-12">
                              <label for="user_email" class="field prepend-icon">
                                {!! Form::text('admin_email',$admin_detail['admin_email'],array('class' => 'gui-input mycursor' ,'placeholder' => 'Enter Email','disabled'=>'disabled')) !!}
                                <label for="user_email" class="field-icon">
                                  <i class="fa fa-envelope"></i>
                                </label>
                              </label>
                            </div>
                          </div>
                          <div class="section row" style="  margin-bottom: 17px;">
                            <div class="col-md-12">
                              <label for="user_number" class="field prepend-icon">
                                {!! Form::text('admin_number',$admin_detail['admin_number'],array('class' => 'gui-input numeric','placeholder' => 'Enter Mobile Number' )) !!}
                                <label for="Mobile Number" class="field-icon">
                                  <i class="fa fa-phone"></i>
                                </label>
                              </label>
                            </div>
                          </div>

                          <div class="section row" style="  margin-bottom: 17px;">
                            <div class="col-md-12">
                              <label for="user_number" class="field prepend-icon">
                                {!! Form::text('admin_degree',$admin_detail['admin_degree'],array('class' => 'gui-input ','placeholder' => 'Enter Degree' )) !!}
                                <label for="Mobile Number" class="field-icon">
                                  <i class="fa fa-pencil"></i>
                                </label>
                              </label>
                            </div>
                          </div>


                          <div class="section row" style="  margin-bottom: 17px;">
                            <div class="col-md-12">
                              <label for="user_number" class="field prepend-icon">
                                {!! Form::text('admin_court',$admin_detail['admin_court'],array('class' => 'gui-input ','placeholder' => 'Enter Court Name' )) !!}
                                <label for="Mobile Number" class="field-icon">
                                  <i class="fa fa-pencil"></i>
                                </label>
                              </label>
                            </div>
                          </div>

                          <div class="section row" style="  margin-bottom: 17px;">
                            <div class="col-md-12">
                              <label for="user_number" class="field prepend-icon">
                                {!! Form::text('admin_firmname',$admin_detail['admin_firmname'],array('class' => 'gui-input ','placeholder' => 'Enter Firm Name' )) !!}
                                <label for="Mobile Number" class="field-icon">
                                  <i class="fa fa-pencil"></i>
                                </label>
                              </label>
                            </div>
                          </div>


                          <div class="section row" style="  margin-bottom: 17px;">
                            <div class="col-md-12">
                              <label for="user_number" class="field prepend-icon">
                                {!! Form::text('admin_advocates',$admin_detail['admin_advocates'],array('class' => 'gui-input ','placeholder' => 'Enter Advocates Name' )) !!}
                                <label for="Mobile Number" class="field-icon">
                                  <i class="fa fa-pencil"></i>
                                </label>
                              </label>
                            </div>
                          </div>

                          <div class="section row" style="  margin-bottom: 17px;">
                            <div class="col-md-12">
                              <label for="user_number" class="field prepend-icon">
                                {!! Form::textarea('admin_address',$admin_detail['admin_address'],array('class' => 'gui-textarea accc1','placeholder' => 'Enter Address' )) !!}
                                <label for="Mobile Number" class="field-icon">
                                  <i class="fa fa-map-marker"></i>
                                </label>
                              </label>
                            </div>
                          </div>

                         </div>                         
                      </div>
                      </div>
                     <div class="col-md-3" style="  margin-top: 4px;">
                      <div class="fileupload fileupload-new admin-form" data-provides="fileupload">

                      @if($admin_detail['admin_image'] != "")

                        <div class="fileupload-preview thumbnail mb15" style="line-height: 136px !important;">
                          {!! Html::image($admin_detail['admin_image'], '100%x147') !!}
                        </div>
                        <span class="button btn-system btn-file btn-block ph5" style="margin-bottom:10px">
                          <span class="fileupload-new">Profile Picture</span>
                          <span class="fileupload-exists">Profile Picture</span>
                          {!! Form::file('admin_image') !!}
                        </span>

                      @else

                        <div class="fileupload-preview thumbnail mb15" style="padding: 3px;outline: 1px dashed #d9d9d9;height: 150px">
                          <img data-src="holder.js/100%x147" alt="100%x147" src="" data-holder-rendered="true"  style="height: 150px !important; width: 100%; display: block;">
                        </div>
                        <span class="button btn-system btn-file btn-block ph5" style="margin-bottom:10px">
                          <span class="fileupload-new" >Profile Photo</span>
                          <span class="fileupload-exists">Profile Photo</span>
                          {!! Form::file('admin_image') !!}
                        </span>

                      @endif

                      </div>
                    </div>


                    

                    <div class="form-group">
                    <div class="col-lg-12">
                      <div class="input-group" style="float: right; width: 220px;  ">
                      {!! Form::submit('Update', array('class' => 'button btn-primary btn-file btn-block ph5', 'id' => 'maskedKey')) !!}

                      </div>
                    </div>
                    </div>
                  {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

<script type="text/javascript">
  jQuery(document).ready(function() {

    jQuery.validator.addMethod("lettersonly", function(value, element) 
    {
    return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "This field contains alphabets only");

    jQuery.validator.addMethod("checkemail", function(value, element) 
    {
    return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
    }, "Enter a VALID email address"); 
  
    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */
    $("#admin-form").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        admin_name: {
          required: true,
          lettersonly: true,
        },
        // admin_number: {
        //   required: true,
        //   minlength: 10,
        //   maxlength: 10,
          
        // },
        admin_email: {
          required: true,
          checkemail: true,
        },
        // admin_address: {
        //   required: true,
        // },
        // admin_degree: {
        //   required: true,
        // },
        // admin_court: {
        //   required: true,
        //   lettersonly: true
        // },
        admin_image: {
        // //  required: true,
          extension: 'jpeg,jpg,png',
        //   type: 'image/jpeg,image/png',
        // //  maxSize: 2097152,   // 2048 * 1024
       //    message: 'The selected file is not valid'
         },
      },

      /* @validation error messages 
      ---------------------------------------------- */

      messages: {
        admin_name: {
          required: 'Enter your name'
        },
        admin_degree: {
          required: 'Enter your degree'
        },
        admin_court: {
          required: 'Enter your court'
        },
        admin_address: {
          required: 'Enter your address'
        },
        admin_number: {
          required: 'Enter your mobile number',
          minlength:'Enter at least 10 digit phone numbers',
          maxlength:'Enter 10 digit phone numbers',
        },
        admin_email: {
          required: 'Enter email address',
          email: 'Enter a VALID email address'
        },
        admin_image: {
          extension: 'Image Should be in .jpg,.jpeg and .png format only'
        },
      },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });
  </script>





@endsection
