@extends('advocate_admin/layout')
@section('content')

  <!-- Start: Content-Wrapper -->
  <section id="content_wrapper">
    <header id="topbar" style="padding-top: 17px;">
      <div class="topbar-left">
        <ol class="breadcrumb">
          <li class="crumb-icon">
            <a href="{{ url('advocate-panel/dashboard') }}">
              <span class="glyphicon glyphicon-home"></span>
            </a>
          </li>
          <li class="crumb-link">
            <a href="{{ url('advocate-panel/dashboard') }}">Home</a>
          </li>
          <li class="crumb-trail"> Guidelines to use Software</li>
        </ol>
      </div>
    </header>

    <div class="" style="margin-top:10px;">
      <div class="col-md-12">
        <div class="panel panel-primary panel-border top mb35">  
          <div class="panel-heading">
            <div class="panel-title hidden-xs">
              <span class="glyphicon glyphicon-tasks"></span> Guidelines to use Software</div>
          </div>

          <div class="panel-menu admin-form theme-primary" style="padding: 10px 20px;">
            <div class="row">  
                <div class="col-md-2">
                   <a href="{{asset($get_record['pages_english_pdf']) }}" target="_Blank">{!! Form::submit('View Pdf in English', array('class' => 'btn btn-primary', 'id' => 'maskedKey')) !!}</a>
                </div>

                <div class="col-md-2">
                   <a href="{{asset($get_record['pages_hindi_pdf']) }}" target="_Blank">{!! Form::submit('View Pdf in Hindi', array('class' => 'btn btn-primary', 'id' => 'maskedKey')) !!}</a>
                </div>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


<style type="text/css">
.dt-panelfooter{
  display: none !important;
}
</style>

@endsection