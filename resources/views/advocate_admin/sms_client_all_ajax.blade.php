<div class="section">
<label for="level_name" class="field-label" style="font-weight:600;" > Client : </label>
  <label for="artist_state" class="field">
    <select id="framework_3" name="client[]" multiple class="form-control" onchange="get_client(this.value)" >
        @foreach($client as $clients)
          <option value="{{ $clients->cl_id }}"> {{ $clients->cl_group_name }} @if($clients->cl_father_name != "") {{ $clients->cl_name_prefix }} {{ $clients->cl_father_name }} @endif  @if($clients->cl_group_mobile_no != "") ({{ $clients->cl_group_mobile_no }}) @endif </option>
        @endforeach
    </select>
    <i class="arrow double"></i>
  </label>
</div>


<script type="text/javascript">
	

	$('#framework_3').multiselect({
      nonSelectedText: 'Select Client',
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
      buttonWidth:'400px'
     });
	
</script>
