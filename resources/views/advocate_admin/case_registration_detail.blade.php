@extends('advocate_admin/layout')
@section('content')

<style type="text/css">
  
  blink, .blink {
      animation: blinker 1s linear infinite;
  }

 .close{
  margin-top: -6px !important;
  font-size: 30px !important;
  width: 30px !important;
}

 @keyframes blinker {  
      50% { opacity: 0; }
 }

  .table > thead > tr > th {
    font-weight: 500;
  }
  b{
    font-weight: 1000;
  }  
  .admin-form .gui-input, .admin-form{
    padding: 10px;
  }
  .bg-light {
    background-color: #4B77BE !important;
    color: black !important;
    font-weight: bold;
  }
  .pagebreak { page-break-before: always; }
@media print {
    h1 {page-break-before: always;}
}

</style>

<style type="text/css">
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
</style>

  <!-- Start: Content-Wrapper -->
  <section id="content_wrapper">
    <header id="topbar" style="margin-top: 60px;">
      <div class="topbar-left">
        <ol class="breadcrumb">
          <li class="crumb-active">
            <a href="{{ url('/advocate-panel/view-case-form') }}">View Case Registration</a>
          </li>
          <li class="crumb-icon">
            <a href="{{ url('/advocate-panel/dashboard') }}">
              <span class="glyphicon glyphicon-home"></span>
            </a>
          </li>
          <li class="crumb-link">
            <a href="{{ url('/advocate-panel/dashboard') }}">Home</a>
          </li>
          <li class="crumb-trail">Case Registration Details </li>
        </ol>

        <ol class="breadcrumb pull-right">
          <li class="crumb-active">
            <a href="#" onclick="share_email()" > Share via Email</a>
          </li>
        </ol>

      </div>
    </header>

    <div class="row mt20">
        <div class="col-md-12">
          @if (\Session::has('success'))
          <div class="alert alert-success account_setting" >
            {!! \Session::get('success') !!}
          </div>
          @endif

          @if (\Session::has('failure'))
          <div class="alert alert-danger account_setting" >
            {!! \Session::get('failure') !!}
          </div>
          @endif
        </div>
      </div>




    <div class="" style="margin-top:10px;">
      <div class="col-md-12">
        <div class="panel panel-primary panel-border top">  
          <div class="panel-heading">
            <div class="panel-title hidden-xs">
              <span class="glyphicon glyphicon-tasks"></span>Case Registration Details 
              <span class="pull-right"> <a href="#" onclick="printInfo(this)">
                <i class="fa fa-print"></i>
              </a>  </span>

<script type="text/javascript">

function printInfo(){


  var prtContent = document.getElementById("content");
var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
WinPrint.document.write(prtContent.innerHTML);
WinPrint.document.close();
WinPrint.focus();
WinPrint.print();
WinPrint.close();
}

</script>

              <span class="" style="margin-left: 330px;">  </span>
            </div>

          </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>

      <section id="content" class="animated fadeIn">
        <!-- <div class="page-heading" style="background-color: #FFF; margin:-45px -9px 5px;"> -->
          <div class="page-heading" style="background-color: #FFF; ">
            <div class="media clearfix">
              <div class="col-md-12">
                
                <div class="panel-heading" style="background: #4B77BE !important; color: black; font-weight: bold; font-size: 16px; margin-bottom: 30px; font-family: sans-serif;">
                  <div class="panel-title hidden-xs text-center" style="padding: 4px 0px; text-align: center;"> 
                    @if($get_case_details->pessi_choose_type == 0) Pending @elseif($get_case_details->pessi_choose_type == 1) {{ date('d F Y',strtotime($get_case_details->pessi_further_date)) }} :- Disposal @else Due Course @endif
                  </div>
                </div>

                <div class="panel-heading" style="background: #4B77BE !important; color: black; font-weight: bold; font-size: 16px;">
                  <div class="panel-title hidden-xs text-center" style="padding: 4px 0px; text-align: center;"> Case Details </div>
                </div>

                <div class="panel-body pn">
                  <table class="table table-striped table-hover" id="" cellspacing="0" width="100%" style="color: #000; font-size: 12px;">
                    <thead>
                      <tr>
                        <th class="" style="border: 1px solid gray; text-align: left; padding: 7px 7px; font-weight: normal;"> <b> Court Name :- </b>  @if( $get_case_details->court_name != "") {{ $get_case_details->court_name }} @else ----- @endif </th>
                      </tr>
                      <tr>
                        <th class="" style="border: 1px solid gray; padding: 7px 7px; background-color: antiquewhite; font-weight: normal;">
                         <!-- class= col-md-6 -->
                          <div class="" style="text-align: left; float:left; width: 45%; "> 
                            <b> Institution Date :- </b> @if( $get_case_details->reg_date != "") {{ date('d/m/Y',strtotime($get_case_details->reg_date))  }} @else ----- @endif <br> 

                            <b> File No :- </b> @if( $get_case_details->reg_file_no != "") {{ $get_case_details->reg_file_no }} @else ----- @endif
                          </div>
                          <div class="" style=" text-align: left; float: left; width: 45%;  border-left: 4px solid #eeeeee !important;"> 
                            <b style="margin-left: 10px;"> NCV No :- </b> @if( $get_case_details->reg_vcn_number != "") {{ $get_case_details->reg_vcn_number }} @else ----- @endif <br> 
                            <b style="margin-left: 10px;"> Class Code :- </b> @if( $get_case_details->classcode_name != "") {{ $get_case_details->classcode_name }} @else ----- @endif </div> 
                        </th>  
                      </tr>
                      <tr style="text-align: left;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px; font-weight: normal;"> 
                          <b> Case Number :- </b>
                            @if( $get_case_details->case_name != "") {{ $get_case_details->case_name }} @else ---- @endif 
                            @if( $get_case_details->reg_case_number != "") {{ $get_case_details->reg_case_number }} @else ---- @endif
                            <!-- @if( $get_case_details->reg_date != "") {{ date('Y',strtotime($get_case_details->reg_date))  }} @else ----- @endif -->
                          </th>
                      </tr>
                      <tr style="text-align: left; background-color: antiquewhite; font-weight: normal;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px; font-weight: normal;"> <b> Case Title :- </b> @if( $get_case_details->reg_petitioner != "") {{ $get_case_details->reg_petitioner }} @else ---- @endif v/s @if( $get_case_details->reg_respondent != "") {{ $get_case_details->reg_respondent }} @else ---- @endif </th>
                      </tr>
                      <tr style="text-align: left; font-weight: normal;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px; font-weight: normal;"> <b> Power :- </b>
                          @if($get_case_details->reg_power == 1) Petitioner @elseif($get_case_details->reg_power == 2 ) @if($get_case_details->reg_power_respondent != "") Respondent - {{ $get_case_details->reg_power_respondent }} @else Respondent @endif @elseif($get_case_details->reg_power == 3 ) Caveat @elseif($get_case_details->reg_power == 4 ) None @else ---- @endif
                        </th>
                      </tr>
                      <tr style="font-weight: normal;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px; background-color: antiquewhite; font-weight: normal;"> 
                          <!-- class= col-md-6 -->
                          <div class="" style="text-align: left; float:left; width: 45%;"> 
                            <b class=""> FIR No :- </b> @if( $get_case_details->reg_fir_id != "") {{ $get_case_details->reg_fir_id }} @else ---- @endif <br> 
                            <b class=""> FIR Year :- </b> @if( $get_case_details->reg_fir_year != "") {{ $get_case_details->reg_fir_year }} @else ---- @endif </div>
                          <div class="" style="float: left; text-align: left; width: 45%; border-left: 4px solid #eeeeee !important;"> <b style="margin-left: 15px;"> Police Station :- </b> @if( $get_case_details->reg_police_thana != "") {{ $get_case_details->reg_police_thana }} @else ---- @endif </div> 
                        </th>
                      </tr>
                    </thead>
                  </table>
                </div>

                <br>

                <div class="panel-heading" style="background: #4B77BE !important; color: black; font-weight: bold; font-size: 16px;">
                  <div class="panel-title hidden-xs text-center" style="padding: 4px 0px; text-align: center;"> Case Status </div>
                </div>

                <div class="panel-body pn">
                  <table class="table table-striped table-hover" id="" cellspacing="0" width="100%" style="color: #000; font-size: 12px;">
                    <thead>
                      <tr style="text-align: left;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px; font-weight: normal;"> <b> Status :- </b> @if($get_case_details->pessi_choose_type == 0) Pending @elseif($get_case_details->pessi_choose_type == 1) Disposal ( {{ date('d F Y',strtotime($get_case_details->pessi_further_date)) }} ) @else Due Course @if($get_case_details->pessi_further_date == "1970-01-01")
                            @else --- {{ date('d/m/Y',strtotime($get_case_details->pessi_further_date)) }} 
                            @endif  @endif </th>
                      </tr>
                      
                      @if($get_case_details->pessi_choose_type != 2)
                        <tr style="text-align: left; background-color: antiquewhite; font-weight: normal;">
                          <th class="" style="border: 1px solid gray; padding: 7px 7px; font-weight: normal;"> <b> Previous Date :- </b>
                            @if($get_case_details->pessi_prev_date == "1970-01-01" || $get_case_details->pessi_prev_date == "")
                              -----
                            @else
                              {{ date('d/m/Y',strtotime($get_case_details->pessi_prev_date)) }}
                            @endif
                          </th>
                        </tr>
                        <tr style="text-align: left;">
                          <th class="" style="border: 1px solid gray; padding: 7px 7px; font-weight: normal;"> <b> Next Date :- </b>
                            @if($get_case_details->pessi_further_date == "1970-01-01")
                              -----
                            @else
                              <blink> {{ date('d/m/Y',strtotime($get_case_details->pessi_further_date)) }} </blink>
                            @endif
                          </th>
                        </tr>
                      @endif


                      <tr style="text-align: left; background-color: antiquewhite;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px; font-weight: normal;"> <b> Stage :- </b> @if( $get_case_details->stage_name != "") <blink> {{ $get_case_details->stage_name }} </blink> @else ---- @endif </th>
                      </tr>
                      <tr style="text-align: left;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px;"> <b> Extra Party :- </b> 
                          <button type="button" class="btn btn-primary view" style="padding: 4px 10px; background-color: #4a89dc; border:none;"  onclick="reg_petitioner({{$get_case_details->reg_id}})" > 
                            <a href="#" style="text-transform: capitalize; text-decoration:none; color: black;"> Pet. Details </a> 
                          </button> 

                          <button type="button" class="btn btn-primary view" style="padding: 4px 10px; background-color: #4a89dc; border:none;" onclick="reg_respondent({{$get_case_details->reg_id}})" >
                            <a href="#" style="text-transform: capitalize; text-decoration:none; color: black;"> Resp. Details </a> 
                          </button> 
                        </th>
                      </tr>
                    </thead>
                  </table>
                </div>
                            
               
                        


                <br>

                <div class="panel-heading" style="background: #4B77BE !important; color: black; font-weight: bold; font-size: 16px;">
                  <div class="panel-title hidden-xs text-center" style="padding: 4px 0px; text-align: center;"> Client Details </div>
                </div>

                <div class="panel-body pn">
                  <table class="table table-striped table-hover" id="" cellspacing="0" width="100%" style="color: #000;  font-size: 12px;">
                    <thead>
                      <tr style="text-align: left;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px; font-weight: normal;"> <b> Referred By :- </b> @if( $get_case_details->ref_advocate_name != "") {{ $get_case_details->ref_advocate_name }} @else ---- @endif </th>
                      </tr>
                      <tr style="text-align: left;">
                        <th class="" style="border: 1px solid gray; padding: 7px 5px; text-align: left; background-color: antiquewhite; font-weight: normal;"> <div class="col-md-2"> <b> Client Name :- </b> </div> 
                          @if( $get_case_details->reg_client_group_id != "") 

                            @if($get_case_details->cl_group_type == "1") 
                              
                              <div class="col-md-11" >
                                {{ $get_case_details->cl_group_name }} @if($get_case_details->cl_father_name != "") {{ $get_case_details->cl_name_prefix }} {{ $get_case_details->cl_father_name }} @endif <br> {{ $get_case_details->cl_group_address }} {{ $get_case_details->cl_group_place }} <br> {{ $get_case_details->cl_group_mobile_no }} 
                              </div>

                            @else 
                              
                              <div class="col-md-11" >
                                {{ $get_case_details->cl_group_name }} @if($get_case_details->cl_father_name != "") {{ $get_case_details->cl_name_prefix }} {{ $get_case_details->cl_father_name }} @endif <br> {{ $get_case_details->sub_client_name }} <br> {{ $get_case_details->sub_client_address }} {{ $get_case_details->sub_client_place }} <br> {{ $get_case_details->sub_client_mobile_no }} 
                              </div>

                            @endif

                          @else ---- @endif </th>
                      </tr>
                    </thead>
                  </table>
                </div>

                <br>

                <div class="panel-heading" style="background: #4B77BE !important; color: black; font-weight: bold; font-size: 16px;">
                  <div class="panel-title hidden-xs text-center" style="padding: 4px 0px; text-align: center;"> Other Details </div>
                </div>

                <div class="panel-body pn">
                  <table class="table table-striped table-hover" id="" cellspacing="0" width="100%" style="color: #000;  font-size: 12px;">
                    <thead>
                      <tr style="text-align: left;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px; font-weight: normal;"> <b> Assigned :- </b> @if( $get_case_details->assign_advocate_name != "") {{ $get_case_details->assign_advocate_name }} @else ---- @endif</th>
                      </tr>
                      <tr style="background-color: antiquewhite; text-align: left;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px; font-weight: normal;"> <b> Act :- </b> @if( $get_case_details->act_name != "") {{ $get_case_details->act_name }} @else ---- @endif</th>
                      </tr>

                      @php
                        $section_name  = "";
                        $get_case_details->reg_section_id = explode(',', $get_case_details->reg_section_id);
                        $section =  \App\Model\Section\Section::whereIn('section_id', $get_case_details->reg_section_id )->where('section_name', '!=' ,'')->get();

                        foreach($section as $sections){
                          $section_name .= $sections->section_name.' , ';
                        }

                        $section_name = substr($section_name,0,-2);
                      @endphp
                      

                      <tr style="text-align: left;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px; font-weight: normal;"> <b> Section :- </b> @if( $section_name != "") {{ $section_name }} @else ---- @endif </th>
                      </tr>
                      

                      <tr style="background-color: antiquewhite; text-align: left;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px; font-weight: normal;"> <b> Opposite Counsel :- </b> @if( $get_case_details->reg_opp_council != "") {{ $get_case_details->reg_opp_council }} @else ---- @endif </th>
                      </tr>
                      <tr style="text-align: left;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px; font-weight: normal;"> <b> Remark Field :- </b> @if( $get_case_details->reg_remark != "") {{ $get_case_details->reg_remark }} @else ---- @endif </th>
                      </tr>

                      <tr style="text-align: left; background-color: antiquewhite;">
                        <th class="" style="border: 1px solid gray; padding: 7px 7px; font-weight: normal;"> <b> Other Field :- </b> @if( $get_case_details->reg_other_field != "") {{ $get_case_details->reg_other_field }} @else ---- @endif </th>
                      </tr>
                    </thead>
                  </table>
                </div>

                <br>

                <div class="panel-heading" style="background: #4B77BE !important; color: black; font-weight: bold; font-size: 16px;">
                  <div class="panel-title hidden-xs text-center" style="padding: 4px 0px; text-align: center;"> History of Case </div>
                </div>

                <div class="panel-body pn">
                  <table class="table table-striped table-hover" id="" cellspacing="0" width="100%" style="color: #000;  font-size: 12px;">
                    <thead>
                      <tr>
                        <th class="" style="border: 1px solid gray; padding: 7px 7px;"> <b> Peshi Date </b></th>
                        <th class="" style="border: 1px solid gray; padding: 7px 7px;"> <b> Court No: Judge Name </b></th>
                        <th class="" style="border: 1px solid gray; padding: 7px 7px;"> <b> Purpose / Stage </b></th>
                        <th class="" style="border: 1px solid gray; padding: 7px 7px;"> <b> Order Sheet </b></th>
                        <th class="text-left" style="border: 1px solid gray; padding: 7px 7px;">
                          <!-- <label class="option block mn"> -->
                           <b> Delete</b>
                          <!-- </label> -->
                        </th>
                      </tr>
                    </thead>

                    <tbody>
                      @foreach($pessi_detail as $pessi_details)
                        
                        <tr>
                          <!-- <td class=""> 
                            <a href="{{ url('advocate-panel/view-peshi')}}" style="text-decoration: none;">
                              @if($pessi_details->pessi_choose_type == 0)
                                Date:-  {{ date('d F Y',strtotime($pessi_details->pessi_further_date)) }}
                              @elseif($pessi_details->pessi_choose_type == 1)
                                Decide :- @if($pessi_details->pessi_decide_id == 1) Case In Favour @elseif($pessi_details->pessi_decide_id == 2) Case in against @elseif($pessi_details->pessi_decide_id == 3) Withdraw @elseif($pessi_details->pessi_decide_id == 4) None @endif
                              @else
                                Due Course
                              @endif
                            </a> -->

                          <td style="border: 1px solid gray; padding: 7px 7px;">
                            <a href="#" style="text-transform: capitalize; text-decoration:none;" onclick="pessi_details({{$pessi_details->pessi_id}})" > 

                              @if($pessi_details->pessi_choose_type == 0)

                                @if($pessi_details->pessi_further_date == "" || $pessi_details->pessi_further_date ==  "1970-01-01")
                                  Date:-  -----
                                @else

                                  Date:-  {{ date('d F Y',strtotime($pessi_details->pessi_further_date)) }}
                                
                                @endif

                                
                              @elseif($pessi_details->pessi_choose_type == 1)

                                Decide :- @if($pessi_details->pessi_decide_id == 1) Case In Favour @elseif($pessi_details->pessi_decide_id == 2) Case in against @elseif($pessi_details->pessi_decide_id == 3) Withdraw @elseif($pessi_details->pessi_decide_id == 4) None @endif
                              @else
                                Due Course
                              @endif

                            </a>

                            

                          </td>

                          <script type="text/javascript">
                            function update_pessi_details(pessi_id) {

                              if(confirm("Are you sure to want update records?")) {
                                $("[name=form_add_pessi"+pessi_id+"]").submit();    
                              }
                            }
                          </script>

                          @php
                            $justise_name =  \App\Model\Judge\Judge::where(['judge_id' => $pessi_details->honarable_justice_db ])->first();
                          @endphp

                          <td class="" style=" border: 1px solid gray; padding: 7px 7px;"> 
                            @if($pessi_details->reg_court == 2)
                              @if($pessi_details->judge_name == "" && $pessi_details->court_number == "")
                                -----
                              @else
                                @if($pessi_details->court_number != "")  C-{{$pessi_details->court_number}}: &nbsp @endif

                                @if($pessi_details->judge_name != "")  {{$pessi_details->judge_name}}  @endif

                                @if($justise_name->judge_name != "" && $pessi_details->judge_name != "") &  @endif
                                @if($justise_name->judge_name != "")  {{$justise_name->judge_name}}  @endif

                                @if( ($pessi_details->judge_name != "" || $justise_name->judge_name != "") && $pessi_details->court_number != "")   @endif
                                
                              @endif
                            @elseif($pessi_details->reg_court == 1)
                                   -------
                            @endif
                          </td>
                          <td class="" style=" border: 1px solid gray; padding: 7px 7px;"> @if($pessi_details->stage_name == "") ----- @else {{ $pessi_details->stage_name }} @endif </td>
                          <td class="" style=" border: 1px solid gray; padding: 7px 7px;"> @if($pessi_details->pessi_order_sheet == "") ----- @else {{ $pessi_details->pessi_order_sheet }} @endif</td>


                          <td class="text-left" style="padding-left: 20px; border: 1px solid gray; padding: 7px 7px;">
                            @if(count($pessi_detail) == 1)

                            @else
                            <label class="option block mn" onclick="delete_pessi('{{ sha1($pessi_details->pessi_id) }}')">
                              <!-- <a href="{{ url('advocate-panel/delete-pessi') }}/{{ sha1($pessi_details->pessi_id) }}"> -->
                                <span class="fa fa-trash" style="cursor: pointer;"></span>
                              <!-- </a> -->
                            </label>

                            @endif
                          </td>


                        </tr>

                      @endforeach
                    </tbody>
                  </table>
                </div>

                <br>

                <div class="panel-heading" style="background: #4B77BE !important; color: black; font-weight: bold; font-size: 16px;">
                  <div class="panel-title hidden-xs text-center" style="padding: 4px 0px; text-align: center;"> Compliance </div>
                </div>

                <div class="panel-body pn">
                  <table class="table table-striped table-hover" id="" cellspacing="0" width="100%" style="color: #000;  font-size: 12px;">
                    <thead>
                      <tr>
                        <th class="" style="border: 1px solid gray; padding: 7px 7px;"> <b> Created </b></th>
                        <th class="" style="border: 1px solid gray; padding: 7px 7px;"> <b> View Compliance </b></th>
                      </tr>
                    </thead>

                    <tbody>

                      @if(count($compliance_detail) != 0)
                        @foreach($compliance_detail as $compliance_details)
                          <tr>
                            <td class="" style="border: 1px solid gray; padding: 7px 7px;"> @if($compliance_details->compliance_created != "" || $compliance_details->compliance_created != "1970-01-01") {{ date('d M Y',strtotime($compliance_details->compliance_created)) }} @else ----- @endif </td>
              
                            <td class="text-left" style="border: 1px solid gray; padding: 7px 7px;">
                              <button type="button" class="btn btn-primary view btn-xs" onclick="view_compliance({{$compliance_details->compliance_id}})" style="padding: 4px 10px; background-color: #4a89dc; border:none;">
                                <a href="#" style="text-transform: capitalize; text-decoration:none; color: white;"> View Compliance</a>
                              </button>
                            </td>
                          </tr>    
                        @endforeach
                      @else 
                        
                        <tr>
<!--                           <td colspan="5">
                            <div style="text-align: center; border: 1px solid gray; padding: 7px 7px;" class="text-center" > No Record Found !! </div>
                          </td> -->

                          <td colspan="5" class="text-center" style="border: 1px solid gray; text-align: center; padding: 7px 7px;"> No Record Found !! </td>
                        </tr>
                      @endif

                    </tbody>
                  </table>
                </div>

                <br>

                <div class="panel-heading" style="background: #4B77BE !important; color: black; font-weight: bold; font-size: 16px;">
                  <div class="panel-title hidden-xs text-center" style="padding: 4px 0px; text-align: center;"> Documents </div>
                </div>

                <div style="border: 1px solid gray; padding: 15px 7px;" class="panel-body pn" style="color: #000;  font-size: 12px;">

                  @php
                    $images_cat =  \App\Model\Upload_Document\Upload_Document::where(['upload_case_id' => $get_case_details->reg_id ])->where('upload_images', '!=' ,'')->get();
                  @endphp

                  @if(count($images_cat) != 0)
                    @foreach($images_cat as $images_cats) 


                    @php
                      $doc_image = explode("/",$images_cats['upload_images']);
                      $last =  substr($doc_image[2],-4);
                    @endphp
                    
                      <div class="col-md-4 document_clear" style="margin-bottom: 22px; margin-top: 22px; position: relative;" id="cat_img{{$images_cats['upload_id']}}">
                        
                        @if($last == '.jpg' or $last == 'jpeg' or $last == '.png' or $last == '.gif')
                         
                         <!-- {!! Html::image($images_cats['upload_images'], '', array('class' => 'media-object mw150 customer_profile', 'width'=>'100%', 'height'=>'250')) !!} -->

                          <a target="_blank" href="{{url($images_cats['upload_images'])}}" class="btn btn-xs btn-primary" style="margin-left: 10px; margin-top: 10px;"> View </a>
                        @elseif($last == '.pdf')
                         <i class="fa fa-file-pdf-o"></i> 
                          <a target="_blank" href="{{url($images_cats['upload_images'])}}" class="btn btn-xs btn-primary" style="margin-left: 10px;"> View </a>
                        @elseif($last == 'docx' or $last == '.doc')
                         <i class="fa fa-file-word-o"></i>
                          <a target="_blank" href="{{url($images_cats['upload_images'])}}" class="btn btn-xs btn-primary" style="margin-left: 10px;"> View </a>
                        @elseif($last == '.xls' or $last == 'xlsx')
                         <i class="fa fa-file-excel-o"></i>
                          <a target="_blank" href="{{url($images_cats['upload_images'])}}" class="btn btn-xs btn-primary" style="margin-left: 10px;"> Download </a>
                        @else
                          <a target="_blank" href="{{url($images_cats['upload_images'])}}" class="btn btn-xs btn-primary" style="margin-left: 10px;"> View </a>
                        @endif

                        @if($images_cats['upload_caption'] != "" )
                          <div style="margin-top: 10px;"> Caption :- {{ $images_cats['upload_caption'] }} </div>
                        @endif

                        @if($images_cats['upload_date'] != "" )
                          <div style="margin-top: 10px;"> Date :- 
                            @if($images_cats['upload_date'] == "1970-01-01")
                              -----
                            @else
                              {{ date('d/m/Y',strtotime($images_cats['upload_date'])) }}
                            @endif 
                          </div>
                        @endif


                      </div>
                    @endforeach
                  @else 
                    <div style="text-align: center; border: 1px solid gray; padding: 15px 7px; color: #000;" class="text-center" > No Documents Available !! </div>
                  @endif
                  
                </div>
              </div>
            </div>
        </div>  
      </section>

  </section>



  @foreach($compliance_detail as $compliance_details)
                         
    <div id="view_compliance{{$compliance_details->compliance_id}}" class="modal fade in" role="dialog" style="overflow: scroll;">
      <div class="modal-dialog" style="width:500px; margin-top:80px;">
        <div class="modal-content">
          <div class="modal-header" style="padding-bottom: 35px;">
            <button type="button" class="close" data-dismiss="modal" onclick="close_button_comp({{$compliance_details->compliance_id}})" >&times;</button>
              <h4 class="modal-title pull-left" style="color: #626262;">
                 View Compliance
              </h4>
          </div>

          @if($compliance_details->compliance_type_notice == 1) 
            <div class="modal-header" style="padding-bottom: 35px;">
                <h4 class="modal-title pull-left" style="color: #626262;">  Notice Details  </h4>
            </div>

            <div class="modal-body">
              <table class="table table-bordered mbn">
                <thead>
                  <tr class="bg-light">
                    <th class="text-left" style="padding-left: 10px !important; font-weight: 600; color: #000;">Issue Date</th>
                    <th class="text-left" style="padding-left: 10px !important; font-weight: 600; color: #000;">Status</th>
                  </tr>
                </thead>
                <tbody>            
                  <tr class="">
                    <td class="text-left" style="font-weight: 400; color: #000;" > @if($compliance_details->notice_issue_date != "1970-01-01") {{ date('d M Y',strtotime($compliance_details->notice_issue_date)) }} @else ----- @endif </td>
                    <td class="text-left" style="font-weight: 400; color: #000;"> @if($compliance_details->notice_status == 1) Pending @elseif($compliance_details->notice_status == 2)  Complete ( @if($compliance_details->notice_status_date != "" || $compliance_details->notice_status_date != "1970-01-01") {{ date('d M Y',strtotime($compliance_details->notice_status_date)) }} @endif ) @else ----- @endif </td>
                  </tr>
                </tbody>
              </table>
            </div>
          @endif
          <div class="clearfix"></div>

          @if($compliance_details->compliance_type_date == 1) 
            <div class="divider" style="margin: 0px;"></div>
            <div class="modal-header" style="padding-bottom: 35px;">
                <h4 class="modal-title pull-left" style="color: #626262;" >  Date  </h4>
            </div>

            <div class="modal-body">
              <table class="table table-bordered mbn">
                <thead>
                  <tr class="bg-light">
                    <th class="text-left" style="padding-left: 10px !important; font-weight: 600; color: #000;">Date Type</th>
                    <th class="text-left" style="padding-left: 10px !important; font-weight: 600; color: #000;">Date</th>
                    <th class="text-left" style="padding-left: 10px !important; font-weight: 600; color: #000;">Status</th>
                  </tr>
                </thead>
                <tbody>            
                  <tr class="">
                    <td class="text-left" style="font-weight: 400; color: #000;"> @if($compliance_details->compliance_date_type == 1) Short Date @else Long Date @endif</td>
                    <td class="text-left" style="font-weight: 400; color: #000;"> @if($compliance_details->compliance_date != "1970-01-01") {{ date('d M Y',strtotime($compliance_details->compliance_date)) }} @else ----- @endif</td>
                    <td class="text-left" style="font-weight: 400; color: #000;">@if($compliance_details->compliance_status == 1) Pending @elseif($compliance_details->compliance_status == 2) Complete ( {{ date('d M Y',strtotime($compliance_details->compliance_date_status)) }} ) @else ----- @endif </td>
                  </tr>
                </tbody>
              </table>
            </div>
          @endif

          @if($compliance_details->compliance_type_defect_case == 1) 
            <div class="divider" style="margin: 0px;"></div>
            <div class="modal-header" style="padding-bottom: 35px;">
                <h4 class="modal-title pull-left" style="color: #626262;">  Defect Case  </h4>
            </div>

            <div class="modal-body">
              <table class="table table-bordered mbn">
                <thead>
                  <tr class="bg-light">
                    <!-- <th class="text-left" style="padding-left: 10px !important;">Act Reason</th> -->
                    <th class="text-left" style="padding-left: 10px !important; font-weight: 600; color: #000;">Status</th>
                  </tr>
                </thead>
                <tbody>            
                  <tr class="">
                    <!-- <td class="text-left"> {{ $compliance_details->compliance_act_reason }} </td> -->
                    <td class="text-left" style="font-weight: 400; color: #000;">@if($compliance_details->compliance_defect_status == 1) Pending @elseif($compliance_details->compliance_defect_status == 2) Complete ( {{ date('d M Y',strtotime($compliance_details->compliance_defect_date_status)) }} ) @else ----- @endif  </td>
                  </tr>
                </tbody>
              </table>
            </div>
          @endif

          @if($compliance_details->compliance_type_reply == 1) 
            <div class="divider" style="margin: 0px;"></div>
            <div class="modal-header" style="padding-bottom: 35px;">
                <h4 class="modal-title pull-left" style="color: #626262;">  Reply  </h4>
            </div>

            <div class="modal-body">
              <table class="table table-bordered mbn">
                <thead>
                  <tr class="bg-light">
                    <th class="text-left" style="padding-left: 10px !important;font-weight: 600; color: #000;">Reply</th>
                    <th class="text-left" style="padding-left: 10px !important;font-weight: 600; color: #000;">Status</th>
                  </tr>
                </thead>
                <tbody>            
                  <tr class="">
                    <td class="text-left" style="font-weight: 400; color: #000;" > @if($compliance_details->compliance_reply != "") {{ $compliance_details->compliance_reply }} @else ----- @endif </td>
                    <td class="text-left" style="font-weight: 400; color: #000;" > @if($compliance_details->compliance_reply_status == 1) Pending @elseif($compliance_details->compliance_reply_status == 2) Complete ( {{ date('d M Y',strtotime($compliance_details->compliance_reply_date_status)) }} ) @else ----- @endif </td>
                  </tr>
                </tbody>
              </table>
            </div>
          @endif

          @if($compliance_details->compliance_type_certified == 1) 
            <div class="divider" style="margin: 0px;"></div>
            <div class="modal-header" style="padding-bottom: 35px;">
                <h4 class="modal-title pull-left" style="color: #626262;">  Apply Date Date  </h4>
            </div>

            <div class="modal-body">
              <table class="table table-bordered mbn">
                <thead>
                  <tr class="bg-light">
                    <th class="text-left" style="padding-left: 10px !important;font-weight: 600; color: #000; ">Apply Date Date</th>
                    <th class="text-left" style="padding-left: 10px !important;font-weight: 600; color: #000; ">Status</th>
                  </tr>
                </thead>
                <tbody>            
                  <tr class="">
                    <td class="text-left" style="font-weight: 400; color: #000;">@if($compliance_details->certified_copy_date != "1970-01-01") {{ date('d M Y',strtotime($compliance_details->certified_copy_date)) }} @else ----- @endif</td>
                    <td class="text-left" style="font-weight: 400; color: #000;"> @if($compliance_details->certified_copy_status == 1) Pending @elseif($compliance_details->certified_copy_status == 2) Complete ( {{ date('d M Y',strtotime($compliance_details->certified_copy_date_status)) }} ) @else ----- @endif </td>
                  </tr>
                </tbody>
              </table>
            </div>
          @endif

          @if($compliance_details->compliance_type_other == 1) 
            <div class="divider" style="margin: 0px;"></div>
            <div class="modal-header" style="padding-bottom: 35px;">
                <h4 class="modal-title pull-left" style="color: #626262;"> Other </h4>
            </div>

            <div class="modal-body">
              <table class="table table-bordered mbn">
                <thead>
                  <tr class="bg-light">
                    <th class="text-left" style="padding-left: 10px !important; font-weight: 600; color: #000;">Other</th>
                    <th class="text-left" style="padding-left: 10px !important; font-weight: 600; color: #000;">Status</th>
                  </tr>
                </thead>
                <tbody>            
                  <tr class="">
                    <td class="text-left" style="font-weight: 400; color: #000;"> @if($compliance_details->compliance_other != "") {{ $compliance_details->compliance_other }} @else ----- @endif </td>
                    <td class="text-left" style="font-weight: 400; color: #000;"> @if($compliance_details->compliance_other_status == 1) Pending @elseif($compliance_details->compliance_other_status == 2) Complete ( {{ date('d M Y',strtotime($compliance_details->compliance_other_date_status)) }} ) @else ----- @endif </td>
                  </tr>
                </tbody>
              </table>
            </div>
          @endif

        </div> 
      </div> 
    </div>  
  @endforeach





 <div id="reg_petitioner{{$get_case_details->reg_id}}" class="modal fade in " role="dialog" style="overflow: scroll;">
    <div class="modal-dialog" style="margin-top:80px;">
      <div class="modal-content">
        <div class="modal-header" style="padding-bottom: 35px;">
          <button type="button" class="close" data-dismiss="modal" onclick="close_button_petit({{$get_case_details->reg_id}})" >&times;</button>
            <h4 class="modal-title pull-left">
              Petitioner Extra Party
            </h4>
        </div>
        <div class="">
          <table class="table admin-form table-bordered table-striped theme-warning tc-checkbox-1 fs13" id="datatable">
            <thead>
              <tr class="bg-light">
                <th class="text-left" style="font-weight: 600;" >Name</th>
                <th class="text-left" style="font-weight: 600;" >Prefix</th>
                <th class="text-left" style="font-weight: 600;" >Guardian Name</th>
                <th class="text-left" style="font-weight: 600;" >Address</th>
              </tr>
          </thead>
          <tbody>
           @php
            $reg_petitioner_extra = json_decode($get_case_details->reg_petitioner_extra,true);
            @endphp
            @foreach($reg_petitioner_extra as $respondent_extras)

            <tr>
              <td class="text-left" style="padding-left: 20px;"> @if($respondent_extras[petitioner_name] != "") {{ $respondent_extras[petitioner_name] }} @else ----- @endif </td>
              <td class="text-left" style="padding-left: 20px;"> @if($respondent_extras[petitioner_prefix] != "") {{ $respondent_extras[petitioner_prefix] }} @else ----- @endif </td>
              <td class="text-left" style="padding-left: 20px;"> @if($respondent_extras[petitioner_father_name] != "") {{ $respondent_extras[petitioner_father_name] }} @else ----- @endif </td>
              <td class="text-left" style="padding-left: 20px;"> @if($respondent_extras[petition_address] != "") {{ $respondent_extras[petition_address] }} @else ----- @endif </td>
            </tr>
              @endforeach
            </tbody>
        </table>
        </div>
        <div class="clearfix"></div>
      </div> 
    </div>
 </div>
                         
 <div id="reg_respondent{{$get_case_details->reg_id}}" class="modal fade in" role="dialog" style="overflow: scroll;">
    <div class="modal-dialog" style="margin-top:80px;">
      <div class="modal-content">
        <div class="modal-header" style="padding-bottom: 35px;">
          <button type="button" class="close" data-dismiss="modal" onclick="close_button_resp({{$get_case_details->reg_id}})" >&times;</button>
            <h4 class="modal-title pull-left">
              Respondent Extra Party
            </h4>
        </div>
        <div class="">
          <table class="table admin-form table-bordered table-striped theme-warning tc-checkbox-1 fs13" id="datatable_1">
            <thead>
              <tr class="bg-light"> 
                <th class="text-left" style="font-weight: 600;" >Name</th>
                <th class="text-left" style="font-weight: 600;" >Prefix</th>
                <th class="text-left" style="font-weight: 600;" >Guardian Name</th>
                <th class="text-left" style="font-weight: 600;" >Address</th>
              </tr>
          </thead>
          <tbody>
           @php
            $respondent_extra = json_decode($get_case_details->reg_respondent_extra,true);
            @endphp
            @foreach($respondent_extra as $respondent_extras)
            
            <tr class="json_view">
              <td class="text-left" style="padding-left: 20px;"> @if($respondent_extras[respondent_name] != "") {{ $respondent_extras[respondent_name] }} @else ----- @endif </td>
              <td class="text-left" style="padding-left: 20px;"> @if($respondent_extras[respondent_prefix] != "") {{ $respondent_extras[respondent_prefix] }} @else ----- @endif </td>
              <td class="text-left" style="padding-left: 20px;"> @if($respondent_extras[respondent_father_name] != "") {{ $respondent_extras[respondent_father_name] }} @else ----- @endif </td>
              <td class="text-left" style="padding-left: 20px;"> @if($respondent_extras[respondent_address] != "") {{ $respondent_extras[respondent_address] }} @else ----- @endif </td>
            </tr>

              @endforeach
            </tbody>
        </table>
        </div>
        <div class="clearfix"></div>
      </div> 
    </div>
 </div>


@foreach($pessi_detail as $pessi_details)

 <div id="pessi_details{{$pessi_details->pessi_id}}" class="modal fade in" role="dialog" style="overflow: scroll;">
  <div class="modal-dialog" style="width:700px; margin-top:80px;">
    <div class="modal-content">
      <div class="modal-header" style="padding-bottom: 35px;">
        <button type="button" class="close" data-dismiss="modal" onclick="close_button_pessi({{$pessi_details->pessi_id}})" >&times;</button>
        <h4 class="modal-title pull-left"> Peshi Entry </h4>
      </div>
      <section id="content" class="table-layout animated fadeIn">
        <div>
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                  <div id="tab1_1" class="tab-pane active">
                    
                    {!! Form::open(['name'=>'form_add_pessi'.$pessi_details->pessi_id,'url'=>'advocate-panel/edit-peshi/'.$pessi_details->pessi_id,'id'=>'form_add_pessi' ,'autocomplete'=>'off' ]) !!}

                    <div class="row">                                                
                        
                        <!-- <div class="col-md-12">
                          <div class="section" style="margin-bottom: 40px;">
                            <div class="col-md-3">
                              <label class="option" style="font-size:12px;">
                                {!! Form::radio('choose_type','0',$pessi_details['pessi_choose_type'] == 0 ? 'checked' : '', array('onclick' => "show_further('$pessi_details->pessi_id')", 'class' => 'check' )) !!}
                                <span class="radio" style="border: 2px solid #4a89dc !important;"></span> Next Date
                              </label>
                            </div>

                            <div class="col-md-3">
                              <label class="option" style="font-size:12px;">
                                {!! Form::radio('choose_type','1',$pessi_details['pessi_choose_type'] == 1 ? 'checked' : '', array('onclick' => "show_decide('$pessi_details->pessi_id')", 'class' => 'check' )) !!}
                                <span class="radio" style="border: 2px solid #4a89dc !important;"></span> Decide
                              </label>
                            </div>

                            <div class="col-md-3">
                              <label class="option" style="font-size:12px;">
                                {!! Form::radio('choose_type','2',$pessi_details['pessi_choose_type'] == 2 ? 'checked' : '', array('onclick' => "show_due_course('$pessi_details->pessi_id')", 'class' => 'check' )) !!}
                                <span class="radio" style="border: 2px solid #4a89dc !important;"></span> Due Course
                              </label>
                            </div>

                          </div>

                          <div class="section"></div>
                          <div class="clearfix"></div>
                        </div> -->

                        <div class="col-md-6" @if($pessi_details['pessi_choose_type'] == 1 || $pessi_details['pessi_choose_type'] == 2) style="display:none;" @endif  id="show_further_date{{$pessi_details['pessi_id']}}" >
                          <div class="section section_from">
                            <label for="level_name" class="field-label" style="font-weight:600;" >Next Date  </label>  
                            <label for="level_name" class="field prepend-icon" id="discount_date_from_lb">

                              {!! Form::hidden('current_date',date('d-m-Y'),array('class' => 'gui-input current_date','placeholder' => '','id'=>'current_date'  )) !!}

                              @if($pessi_details->pessi_further_date == "" || $pessi_details->pessi_further_date ==  "1970-01-01")
                                
                                @if($pessi_details->pessi_status == 1)
                                  {!! Form::text('further_date','',array('class' => 'gui-input disposal_to','placeholder' => '','id'=>'' )) !!}
                                @else
                                  {!! Form::text('further_date','',array('class' => 'gui-input','placeholder' => '','id'=>'' ,disabled )) !!}
                                @endif
                              @else

                                @if($pessi_details->pessi_status == 1)
                                  {!! Form::text('further_date',date('d-m-Y',strtotime($pessi_details->pessi_further_date)),array('class' => 'gui-input disposal_to','placeholder' => '','id'=>''  )) !!}
                                @else
                                  {!! Form::text('further_date',date('d-m-Y',strtotime($pessi_details->pessi_further_date)),array('class' => 'gui-input disposal_to','placeholder' => '','id'=>'', disabled  )) !!}
                                @endif

                              @endif
                              <!-- // disposal_to -->
                              
                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-calendar"></i>
                              </label>      
                              <div></div>                     
                            </label>
                            <!-- <label for="level_name" class="field-label"> <b>Note :-</b> dd/mm/yyyy </label> -->
                          </div>
                        </div>

                        <div class="col-md-6" @if($pessi_details['pessi_choose_type'] == 1) @else style="display: none;" @endif id="show_decide{{$pessi_details['pessi_id']}}">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;"> Decide : </label>
                              <label for="artist_state" class="field select">
                                <select class="form-control" id="decide" name="decide" >
                                  <option value=''>Select Decide </option>
                                  <option value='1' {{ $pessi_details->pessi_decide_id == 1 ? 'selected="selected"' : '' }} >Case In Favour </option>      
                                  <option value='2' {{ $pessi_details->pessi_decide_id == 2 ? 'selected="selected"' : '' }} >Case in against </option>
                                  <option value='3' {{ $pessi_details->pessi_decide_id == 3 ? 'selected="selected"' : '' }} > Withdraw </option>
                                  <option value='4' {{ $pessi_details->pessi_decide_id == 4 ? 'selected="selected"' : '' }} > None </option>
                                </select>
                                <i class="arrow double"></i>
                              </label>
                          </div>
                        </div>

                        <input type="hidden" name="decide" value="{{ $pessi_details->pessi_decide_id }}">
                        <input type="hidden" name="choose_type" value="{{ $pessi_details->pessi_choose_type }}">

                        <div class="col-md-2" style="display: none;" id="show_due_course{{$pessi_details['pessi_id']}}">
                          <button type="button" class="btn btn-success br2 btn-xs field select form-control" style="margin-top: 23px; margin-bottom: 22px;" > Due Course </button>
                        </div>


                        <div class="col-md-12">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;"> Select Stage : </label>
                              <label for="artist_state" class="field select">
                                <select class="form-control" name="reg_stage" id="reg_stage">
                                  <!-- <option value=''>Select Stage</option> -->
                                    @foreach($get_stage as $stages) 
                                      <option value="{{ $stages->stage_id }}" {{ $stages->stage_id == $pessi_details->pessi_statge_id ? 'selected="selected"' : '' }} >{{$stages->stage_name}} </option>
                                    @endforeach                                
                                </select>
                                <i class="arrow double"></i>
                              </label>
                          </div>
                        </div>

                        <div class="col-md-12">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;"> Order Sheet :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              {!! Form::textarea('order_sheet',$pessi_details->pessi_order_sheet,array('class' => 'gui-textarea accc1','placeholder' => '','id'=>'' )) !!}
                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-comment"></i>
                              </label                           
                            </label>
                          </div>
                        </div>

                        

<div class="col-md-12">
  <div class="field" align="left">
    <label for="documents" class="field-label" style="font-weight:600;" > <span style="float: left;margin-bottom: 10px;"> Send SMS </span> </label>
  </div>
</div>

<div class="col-md-12">
  <div class="section" style="margin-bottom: 40px;">  
    <div class="col-md-3">
      <label class="option block mn" style="font-size:12px;"> 
        {!! Form::checkbox('sms_type[]','1','', array( 'class' => 'check' , 'class' => 'check' )) !!}
        <span class="checkbox mn" style="border: 2px solid #4a89dc !important;"></span> Client
      </label>
    </div>

    <div class="col-md-3">
      <label class="option block mn" style="font-size:12px;">
        {!! Form::checkbox('sms_type[]','2', '', array( 'class' => 'check'  )) !!}
        <span class="checkbox mn" style="border: 2px solid #4a89dc !important;"></span> Referred
      </label>
    </div>

    <div class="col-md-3">
      <label class="option block mn" style="font-size:12px;">
        {!! Form::checkbox('sms_type[]','3','', array( 'class' => 'check', 'id' => 'sms_type' ,'onclick'=>'reg_sms_type()'  )) !!}
        <span class="checkbox mn" style="border: 2px solid #4a89dc !important;"></span> Other
      </label>
    </div>
  </div>
</div>

<div class="col-md-4" style="display: none;" id="show_other_mobile">
  <div class="section">
    <label for="level_name" class="field-label" style="font-weight:600;"> Other Mobile No. :  </label>  
    <label for="level_name" class="field prepend-icon">
      {!! Form::text('reg_other_mobile','',array('class' => 'gui-input','placeholder' => '' )) !!}
        <label for="Account Mobile" class="field-icon">
        <i class="fa fa-pencil"></i>
      </label>                           
    </label>
  </div>
</div>

<div class="col-md-8">
  <div class="section">
    <label for="level_name" class="field-label" style="font-weight:600;"> Text :  </label>
    <label for="level_name" class="field prepend-icon">
      {!! Form::text('reg_sms_text', '' ,array('class' => 'gui-input','placeholder' => '' )) !!}
        <label for="Account Mobile" class="field-icon">
        <i class="fa fa-pencil"></i>
      </label>                           
    </label>
  </div>
</div>
                      
                    </div>

                  <div class="panel-footer text-right">
                      {!! Form::button('Save', array('class' => 'button btn-primary mysave', 'onclick' => 'update_pessi_details("'.$pessi_details->pessi_id.'")', 'id' => 'maskedKey')) !!}
                      <!-- {!! Form::reset('Cancel', array('class' => 'button btn-primary', 'id' => 'maskedKey')) !!} -->
                  </div>   
                    {!! Form::close() !!}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <div class="clearfix"></div>
    </div> 
  </div>
</div>
@endforeach

<script type="text/javascript">
  
  $(document).on("keyup",".disposal_to",function(){
      $('.mysave').removeAttr('disabled');
      var  currentdate = this.value;
      var n=$(this);    

      var checkdate = /^([0]?[1-9]|[1|2][0-9]|[3][0|1])[-]([0]?[1-9]|[1][0-2])[-]([0-9]{4}|[0-9]{4})$/.test(currentdate);
      
      if(checkdate == true) {

        value=currentdate.split("-");
        var newDate_notice =value[1]+"/"+value[0]+"/"+value[2];
        var notice_status_date = new Date(newDate_notice).getTime();

        
        var notice_issue_date = $('#current_date').val();
        notice_issue_date = notice_issue_date.split("-");
        var newDate_notice_issue = notice_issue_date[1]+"/"+notice_issue_date[0]+"/"+notice_issue_date[2];
        var notice_issue_date = new Date(newDate_notice_issue).getTime();


        //if(notice_status_date > notice_issue_date){
          n.next().next().html("");
          n.next().next().css("color", "#DE888A");
          n.parent().addClass("state-success");
          $('.mysave').removeAttr('disabled');
        // } else {
        //   n.next().next().html("Next date must be greater than current date.");
        //   n.next().next().css("color", "#DE888A");
        //   n.parent().addClass("state-error");
        //   n.parent().removeClass("state-success");
        //   $(this).closest('.mysave').html('');
        //   $('.mysave').attr('disabled','disabled');
        //   return false;
        // }

        
      } else {
        n.next().next().html("Enter date in dd-mm-yyyy format.");
        n.next().next().css("color", "#DE888A");
        n.parent().addClass("state-error");
        n.parent().removeClass("state-success");
        $(this).closest('.mysave').html('');
        $('.mysave').attr('disabled','disabled');
        return false;
      }
  });

</script>

<style type="text/css">

.mutbtnnn button{
  height: 36px !important;
}
.multiselect {
    text-align: left;
}
.btn.multiselect .caret {
    display: none;
}
.multiselect-search{
  height: 36px !important;
}

.multiselect-container li a label {
    border: 0px !important;
    background: none !important;
}
.multiselect-container.dropdown-menu{
  max-height: 200px;
  overflow-x: auto;
}
/*.dropdown-toggle{
  height: auto !important;
}*/
</style>

<script type="text/javascript">
  
  function share_email(){
    $('body').css('overflow','hidden');
    $("html, body").animate({ scrollTop: 0 }, "slow");
    document.getElementById('share_email').style.display='block';

  }

</script>

 <div id="share_email" class="modal fade in" role="dialog" style="overflow: scroll;">
  <div class="modal-dialog" style="width:700px; margin-top:80px;">
    <div class="modal-content">
      <div class="modal-header" style="padding-bottom: 35px;">
        <button type="button" class="close" data-dismiss="modal" onclick="close_button()" >&times;</button>
        <h4 class="modal-title pull-left"> Share via Email </h4>
      </div>
      <section id="content" class="table-layout animated fadeIn">
        <div>
          <div class="center-block">
            <div class="panel panel-primary panel-border top mb35">
              <div class="panel-body bg-light dark">
                <div class="tab-content pn br-n admin-form">
                  <div id="tab1_1" class="tab-pane active">      
                    {!! Form::open(['name'=>'form_share_email', 'url'=>'advocate-panel/share-email', 'id'=>'form_share_email' ,'files' => true ,'autocomplete'=>'off']) !!}
                      <div class="row">                                                
                        

                        <!-- <div class="col-md-6" id="">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Referred : </label>
                              <label for="artist_state" class="field">
                                <select id="referred" name="referred[]" multiple class="form-control">
                                    @foreach($referred as $referreds)
                                      <option value="{{ $referreds->ref_id }}"> {{ $referreds->ref_advocate_name }} </option>
                                    @endforeach
                                </select>
                              </label>
                          </div>
                        </div>

                        <div class="col-md-6" id="">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Assigned : </label>
                              <label for="artist_state" class="field">
                                <select id="assigned" name="assigned[]" multiple class="form-control">
                                    @foreach($assigned as $assigneds)
                                      <option value="{{ $assigneds->ref_id }}"> {{ $assigneds->ref_advocate_name }} </option>
                                    @endforeach
                                </select>
                              </label>
                          </div>
                        </div> -->

                        <!-- <div class="col-md-12">
                          <div class="section" style="margin-bottom: 40px;">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Send sms :  </label>  
                            
                            <div class="col-md-3">
                              <label class="option" style="font-size:12px;">
                                  {!! Form::radio('send_sms','1',true, array( 'class' => 'check' , 'onclick' => 'send_sms_all(1)' )) !!}
                                <span class="radio"></span> All
                              </label>
                            </div>

                            <div class="col-md-3">
                              <label class="option" style="font-size:12px;">
                                  {!! Form::radio('send_sms','2','', array( 'class' => 'check' , 'onclick' => 'send_sms_all(2)' )) !!}
                                <span class="radio"></span> Pending
                              </label>
                            </div>
                          </div>
                          <div class="section"></div>
                          <div class="clearfix"></div>
                        </div> -->

                        @if($get_case_details->assign_advocate_email != "")
                        <div class="col-md-12">
                          <div class="section" style="margin-bottom: 40px;">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Assign :  </label>  
                            
                            <div class="col-md-12">
                              <label class="option block mn" style="font-size:12px;">
                                {!! Form::checkbox('assign',$get_case_details->assign_id,'', array( 'class' => 'check', 'id' => 'sms_type'  )) !!}
                                <span class="checkbox mn" style="border: 2px solid #4a89dc !important;"></span> {{ $get_case_details->assign_advocate_name }} ( {{ $get_case_details->assign_advocate_email }})
                              </label>
                            </div>
                          </div>
                          <div class="section"></div>
                          <div class="clearfix"></div>
                        </div>
                        @endif

                        @if($get_case_details->ref_advocate_email != "")
                        <div class="col-md-12">
                          <div class="section" style="margin-bottom: 40px;">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Referred :  </label>  
                            
                            <div class="col-md-12">
                              <label class="option block mn" style="font-size:12px;">
                                {!! Form::checkbox('referred',$get_case_details->ref_id,'', array( 'class' => 'check', 'id' => 'sms_type'  )) !!}
                                <span class="checkbox mn" style="border: 2px solid #4a89dc !important;"></span> {{ $get_case_details->ref_advocate_name }} ( {{ $get_case_details->ref_advocate_email }})
                              </label>
                            </div>
                          </div>
                          <div class="section"></div>
                          <div class="clearfix"></div>
                        </div>
                        @endif


                        @if($get_case_details->cl_group_name != "")
                        <div class="col-md-12">
                          <div class="section" style="margin-bottom: 40px;">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Client :  </label>  
                            
                            <div class="col-md-12">
                              <label class="option block mn" style="font-size:12px;">
                                {!! Form::checkbox('client[]',$get_case_details->cl_id,'', array( 'class' => 'check', 'id' => 'sms_type'  )) !!}
                                <span class="checkbox mn" style="border: 2px solid #4a89dc !important;"></span> {{ $get_case_details->cl_group_name }} ( {{ $get_case_details->cl_group_email_id }})
                              </label>
                            </div>
                          </div>
                          <div class="section"></div>
                          <div class="clearfix"></div>
                        </div>
                        @endif

                        @if($get_case_details->sub_client_name != "")
                        <div class="col-md-12">
                          <div class="section" style="margin-bottom: 40px;">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Sub Client :  </label>  
                            
                            <div class="col-md-12">
                              <label class="option block mn" style="font-size:12px;">
                                {!! Form::checkbox('sub_client[]',$get_case_details->sub_client_id,'', array( 'class' => 'check', 'id' => 'sms_type'  )) !!}
                                <span class="checkbox mn" style="border: 2px solid #4a89dc !important;"></span> {{ $get_case_details->sub_client_name }} ( {{ $get_case_details->sub_client_email_id }})
                              </label>
                            </div>
                          </div>
                          <div class="section"></div>
                          <div class="clearfix"></div>
                        </div>
                        @endif


                        <input type="hidden" name="client_name" value="{{ $get_case_details->cl_group_name}}">
                        <input type="hidden" name="client_email" value="{{ $get_case_details->cl_group_email_id}}">

                        <!-- <div class="col-md-12" id="hide_framework">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;" > Client : </label>
                              <label for="artist_state" class="field">
                                <select id="framework" name="client[]" multiple class="form-control" onchange="get_client(this.value)" >
                                    @foreach($client as $clients)
                                      <option value="{{ $clients->cl_id }}"> {{ $clients->cl_group_name }} @if($clients->cl_father_name != "") {{ $clients->cl_name_prefix }} {{ $clients->cl_father_name }} @endif @if($clients->cl_group_email_id != "") ({{ $clients->cl_group_email_id }}) @endif </option>
                                    @endforeach
                                </select>
                              </label>
                          </div>
                        </div> -->

                        <div class="col-md-12" id="framework_sms">
                        </div>

                        <div class="col-md-12" id="sub_client_view">
                          
                        </div>

                        <div class="col-md-12">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;"> Subject :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              {!! Form::text('subject','',array('class' => 'gui-input','placeholder' => '','id'=>'' )) !!}
                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-pencil"></i>
                              </label>                           
                            </label>
                          </div>
                        </div>

                        <input type="hidden" name="client_name" value="{{ $get_case_details->cl_group_name}}">
                        <input type="hidden" name="client_email" value="{{ $get_case_details->cl_group_email_id}}">

                        <div class="col-md-12">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;"> Message :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              {!! Form::textarea('message','',array('class' => 'gui-textarea accc1','placeholder' => '','id'=>'' )) !!}
                                <label for="Account Mobile" class="field-icon">
                                <i class="fa fa-comment"></i>
                              </label>                           
                            </label>
                          </div>
                        </div>
                        {!! Form::hidden('reg_id',$get_case_details->reg_id,array('class' => 'gui-textarea accc1','placeholder' => '','id'=>'' )) !!}
                        <div class="col-md-12">
                          <div class="section">
                            <label for="level_name" class="field-label" style="font-weight:600;"> Document Upload :  </label>  
                            <label for="level_name" class="field prepend-icon">
                              <input type="file" id="" name="document_upload" style="padding: 10px;border: 1px solid #ccc;width: 100%;margin-bottom: 20px;" />                           
                            </label>
                          </div>
                        </div>

                      </div>

                      <div class="panel-footer text-right">
                        {!! Form::submit('Send', array('class' => 'button btn-primary mysave', 'onclick' => 'send_email()', 'id' => 'maskedKey')) !!}
                      </div>   
                    {!! Form::close() !!}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <div class="clearfix"></div>
    </div> 
  </div>
</div>
<script type="text/javascript">

  // sms type

  function reg_sms_type(){

    if ($('#sms_type').is(":checked")){
      $('#show_other_mobile').show();
    } else {
      $('#show_other_mobile').hide();      
    }

  }


  function get_client(client_id){

    var new_selected = $("#framework").val();
    var new_new = $("#framework_3").val()

    if(new_selected == null){
      var new_selected = $("#framework_3").val();
    }

    BASE_URL = '{{ url('/')}}';
    $.ajax({
      url:BASE_URL+"/advocate-panel/get-sub-client-ajax-email/"+new_selected,
      success: function(result){
          $("#sub_client_view").html(result); 
      }
    });
   
  }


  // send sms

  function send_sms_all(sms){

  //  alert(sms);

    BASE_URL = '{{ url('/')}}';
    $.ajax({
      url:BASE_URL+"/advocate-panel/send-sms-client-email/"+sms,
      success: function(result){
          $('#hide_framework').hide();  
          $("#framework_sms").html(result); 
      }
    });
  }


  jQuery(document).ready(function() {

    $('#referred').multiselect({
      nonSelectedText: 'Select Referred',
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
      buttonWidth:'400px'
     });

    $('#assigned').multiselect({
      nonSelectedText: 'Select Assigned',
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
      buttonWidth:'400px'
     });

    $('#framework').multiselect({
      nonSelectedText: 'Select Client',
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
      buttonWidth:'400px'
     });

    $('#framework_2').multiselect({
      nonSelectedText: 'Select Sub Client',
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
      buttonWidth:'400px'
     });

    $('#framework_3').multiselect({
      nonSelectedText: 'Select Client',
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
      buttonWidth:'400px'
     });

  });

</script>


<style type="text/css">
  
  .document_clear:nth-child(4){
    clear: both;
  }
  
</style>

<script type="text/javascript">
  
  function show_further(pessi_id){
    document.getElementById("show_further_date"+pessi_id).style.display = "block";
    document.getElementById("show_decide"+pessi_id).style.display = "none";
    document.getElementById("show_due_course"+pessi_id).style.display = "none";
  }


  function show_decide(pessi_id){
    document.getElementById("show_further_date"+pessi_id).style.display = "none";
    document.getElementById("show_decide"+pessi_id).style.display = "block";
    document.getElementById("show_due_course"+pessi_id).style.display = "none";
  }


  function show_due_course(pessi_id){
    document.getElementById("show_further_date"+pessi_id).style.display = "none";
    document.getElementById("show_decide"+pessi_id).style.display = "none";
  //  document.getElementById("show_due_course").style.display = "block";
  }


  function view_compliance(compliance_id){
    $('body').css('overflow','hidden');
    $("html, body").animate({ scrollTop: 0 }, "slow");
    document.getElementById('view_compliance'+compliance_id).style.display='block';
  }

  function close_button_comp(compliance_id){
    $('body').css('overflow','auto', 'important');
    document.getElementById('view_compliance'+compliance_id).style.display='none';
  }


  function reg_petitioner(petitioner_id){
    $('body').css('overflow','hidden');
    $("html, body").animate({ scrollTop: 0 }, "slow");
    document.getElementById('reg_petitioner'+petitioner_id).style.display='block';
  }

  function close_button_petit(petitioner_id){
    $('body').css('overflow','auto', 'important');
    document.getElementById('reg_petitioner'+petitioner_id).style.display='none';
  }


  function reg_respondent(reg_id){
    $('body').css('overflow','hidden');
    $("html, body").animate({ scrollTop: 0 }, "slow");
    document.getElementById('reg_respondent'+reg_id).style.display='block';
  }

  function close_button_resp(reg_id){
    $('body').css('overflow','auto', 'important');
    document.getElementById('reg_respondent'+reg_id).style.display='none';
  }


  function pessi_details(pessi_id){
    $('body').css('overflow','hidden');
    $("html, body").animate({ scrollTop: 0 }, "slow");
    document.getElementById('pessi_details'+pessi_id).style.display='block';
  }

  function close_button_pessi(pessi_id){
    $('body').css('overflow','auto', 'important');
    document.getElementById('pessi_details'+pessi_id).style.display='none';
  }





  // function update_pessi_details() {
     
  //   if(confirm("Are you sure to want update records?")) {
  //     $("[name=form_add_pessi]").submit();    
  //   }
  // }

    function delete_pessi(pessi_id) {
     
      if(confirm("Are you sure to want delete records?")) {
        //$("[name=form_add_pessi]").submit();    

        $.ajax({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          url: "{{ url('advocate-panel/delete-pessi-new') }}",
          type: 'GET',
          data: {
              'pessi_id': pessi_id
          },
          success: function (data) {
            location.reload();
          }
      });

      }
    }

</script>


@endsection