<!-- footer section start -->

<div class="footer" id="contact-us">
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-xs-12 col-sm-3">
      </div>
      <div class="col-md-6 col-xs-12 col-sm-6">
        <img src="public/images/F_logo_02.svg" alt="img F_logo_02" title="Ideal Lawyer">
        <div class="mail">
          <i class="fa fa-envelope" title="Mail to"></i>
          <a href="mailto:contact@ideallawyer.in">
            <h2> contact@ideallawyer.in</h2>
          </a>
        </div>
      </div>
      <div class="col-md-3 col-xs-12 col-sm-3">
      </div>
    </div>
    <div class="row">
      <div class="col-md-3 col-xs-12 col-sm-3">
        <p>©{{ date ('Y')}} iDeal Lawyer All Rights Reserved</p>
      </div>
      <div class="col-md-6 col-xs-12 col-sm-6">
        <div class="privacy">
          <a href='privacy-policy' title="Privacy Policy">
            <h3>Privacy Policy</h3>
          </a>
          <b>|</b>
          <a href="term-services" title="Term of Service">
            <h4>Terms of Service</h4>
          </a>
        </div>
      </div>
      <div class="col-md-3 col-xs-12 col-sm-3 counter">
        <div align='center'>
       <img src='http://online-visit-counter.com/cg.php?t=MTQzMDc4NQ==' border='0' alt='online visit count'></a><BR><a href='http://online-visit-counter.com/'>
          <div style="visibility: hidden; display: none;">صميم مواقع السعودية<</div>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

{!!Html::script('public/js/bootstrap.min.js') !!}
{!!Html::script('public/js/wow.js') !!}
{!!Html::script('public/js/owl.carousel.js') !!}
{!!Html::script('public/js/owl.carousel.min.js') !!}

<script type="text/javascript">
  // animation 
    wow = new WOW({
        animateClass: 'animated',
        offset: 100,
    });
    wow.init();
  $(document).ready(function() {
      $(window).bind('scroll', function() {
          if ($(window).scrollTop() > 100) {
              $('#menu').addClass('menu_scroll');
               $('#scroll_row').addClass('scroll_row1');
              
          } else {
              $('#menu').removeClass('menu_scroll');
              $('#scroll_row').removeClass('scroll_row1');
             
          }
      });
  });
  $("#subs").prop("disabled", !$("#noOfRoom").val());
  
  $('#subs').click(function subst() {
    var $rooms = $("#noOfRoom");
    var b = $rooms.val();
    if (b >= 1) {
        b--;
        $rooms.val(b);
    }
    else {
        $("#subs").prop("disabled", true);
    }
  });
    $(document).ready(function() {
      var owl = $('#mainslider');
     owl.owlCarousel({
       margin: 10,
       nav: true,
       loop: true,
        autoplay:true,
  autoplayTimeout:2000,
  autoplayHoverPause:false,
       responsive: {
         0: {
           items: 1
         },
         600: {
           items: 1
         },
         1000: {
           items: 1
         }
        }
     })   
  
    });



      $(document).ready(function(){
        $(".flip").click(function(){
            $("#bs-example-navbar-collapse-1").removeClass("show");;
        });
      });
  
</script>